/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.DataLookUpDao;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;

import com.google.inject.Inject;

/**
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public class DataLookupManagerImpl implements DataLookUpManager {

	DataLookUpDao dataLookUpDao;

	@Inject
	public DataLookupManagerImpl(DataLookUpDao dataLookupDao) {
		this.dataLookUpDao = dataLookupDao;
	}

	@Override
	public int saveLookupData(DataLookup dataLookup) {
		if (dataLookup == null) {
			return -1;
		}
		return dataLookUpDao.saveLookupData(dataLookup);
	}

	@Override
	public List<DataLookup> getAllLookUpdata(LookupInfoEnum lookupInfo) {

		return dataLookUpDao.getAllLookUpdata(lookupInfo);
	}

	@Override
	public DataLookup getLookUpdataById(LookupInfoEnum lookupInfo, Long id) {
		return dataLookUpDao.getLookUpdataById(lookupInfo, id);
	}
	
	@Override
    public DataLookup getLookUpdataByValue(LookupInfoEnum lookupInfo, String  value, boolean withSublevels) {
        return dataLookUpDao.getLookUpdataByValue(lookupInfo, value, withSublevels);
    }

	@Override
	public List<String> flagshipLeaderEmail(String flagshipName) {
		 return dataLookUpDao.flagshipLeaderEmail(flagshipName);
	}

	@Override
	public String getFlagshipByUser(String email) {
		return dataLookUpDao.getFlagshipByUser(email);
	}

}
