/*
 * FILENAME
 *     DonorBudget.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Donor budget information.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class DonorBudget implements Serializable, Difference<DonorBudget>
{
    private static final long serialVersionUID = -6899951977221717893L;
    private String donorName;
    private double w3;
    private double bilateral;
    private double other;

    @JsonIgnore
    private double total;
    
    @Override
   	public DiffReport diff(DonorBudget t) {
   		DiffReport report = new DiffReport();

   		if (w3 != t.w3) {
   			report.append(donorName + ", W3 :", "" + w3, "" + t.w3);
   		}
   		if (bilateral != t.bilateral) {
   			report.append(donorName + ", Bilateral :", "" + bilateral, "" + t.bilateral);
   		}
   		if (other != t.other) {
   			report.append(donorName + ", Other :", "" + other, ""
   					+ t.other);
   		}
   		
   		return report;
   	}

    /**
     * <p>
     * Getter for total.
     * </p>
     * 
     * @return the total
     */
    public double getTotal()
    {
        total = w3 + bilateral + other;
        return total;
    }

    /**
     * <p>
     * Getter for donorName.
     * </p>
     * 
     * @return the donorName
     */
    public String getDonorName()
    {
        return donorName;
    }

    /**
     * <p>
     * Setting value for donorName.
     * </p>
     * 
     * @param donorName
     *            the donorName to set
     */
    public void setDonorName(String donorName)
    {
        this.donorName = donorName;
    }

    /**
     * <p>
     * Getter for w3.
     * </p>
     * 
     * @return the w3
     */
    public double getW3()
    {
        return w3;
    }

    /**
     * <p>
     * Setting value for w3.
     * </p>
     * 
     * @param w3
     *            the w3 to set
     */
    public void setW3(double w3)
    {
        this.w3 = w3;
    }

    /**
     * <p>
     * Getter for bilateral.
     * </p>
     * 
     * @return the bilateral
     */
    public double getBilateral()
    {
        return bilateral;
    }

    /**
     * <p>
     * Setting value for bilateral.
     * </p>
     * 
     * @param bilateral
     *            the bilateral to set
     */
    public void setBilateral(double bilateral)
    {
        this.bilateral = bilateral;
    }

    /**
     * <p>
     * Getter for other.
     * </p>
     * 
     * @return the other
     */
    public double getOther()
    {
        return other;
    }

    /**
     * <p>
     * Setting value for other.
     * </p>
     * 
     * @param other
     *            the other to set
     */
    public void setOther(double other)
    {
        this.other = other;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }
}
