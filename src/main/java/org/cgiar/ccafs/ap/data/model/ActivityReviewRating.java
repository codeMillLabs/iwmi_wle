/*
 * FILENAME
 *     ActivityReviewRating.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.cgiar.ccafs.ap.util.HTMLCharacterEscapes;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityReviewRating implements Serializable {

	private static final long serialVersionUID = -1109938051291975655L;

	public static final String RATING_TYPE_MID_YEAR = "MID-YEAR";
	public static final String RATING_TYPE_END_YEAR = "YEAR-END";
	public static final int RATING_GREEN = 0;
	public static final int RATING_AMBER = 1;
	public static final int STAGE_1 = 1;
	public static final int STAGE_2 = 2;
	public static final int CORRECTIONS = 3;
	public static final int STAGE_FINALIZED = 4;

	private int id;
	private String activityId;
	private int midYearReviewRating= -1;
	private String midYearReviewProgress;
	private String createdBy;
	private String comment;
	private int stage = STAGE_1;
	
	@JsonIgnore
	private String activityCreatedBy;

	@JsonIgnore
	private boolean isEditable;

	@JsonIgnore
	private boolean isViewable;
	
	@JsonIgnore
	private boolean isReviewable;
	
	@JsonIgnore
	private boolean isPublishable;

	@JsonIgnore
	private User loggedInUser;

	@JsonIgnore
	private double totalFunding = 0.00;

	private double totalSpent = 0.00;

	private List<DeliverableReviewRating> deliverableRating = new ArrayList<DeliverableReviewRating>();

	@JsonIgnore
	private List<ActivityReviewRatingComment> comments = new ArrayList<>();

	public static String getJSON(ActivityReviewRating rating) throws Exception {

		rating.setLoggedInUser(null);
		rating.setComments(null);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.getJsonFactory().setCharacterEscapes(new HTMLCharacterEscapes());
		String jsonStr = mapper.writeValueAsString(rating);

		return jsonStr;
	}

	public static ActivityReviewRating populateObject(String jsonString)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.getJsonFactory().setCharacterEscapes(new HTMLCharacterEscapes());
		ActivityReviewRating rating = mapper.readValue(jsonString,
				ActivityReviewRating.class);
		return rating;
	}

	@JsonIgnore
	public boolean isEditable() {

		if ((stage == STAGE_1 || stage == CORRECTIONS)
				&& (createdBy == null || loggedInUser.getEmail().equals(
						createdBy)) && loggedInUser.hasRole(UserRole.PL)) {
			isEditable = true;
		} else {
			isEditable = false;
		}
		return isEditable;
	}

	@JsonIgnore
	public boolean isViewable() {
	
		if(loggedInUser.isAdmin()) {
			isViewable = true;
	    } else if ((stage == STAGE_1 || stage == CORRECTIONS) 
				&& (loggedInUser.getEmail().equals(
						activityCreatedBy)) && loggedInUser.hasRole(UserRole.PL)) {
			isViewable = true;
		} else if ((stage == STAGE_2 || stage == CORRECTIONS)
				&& ((loggedInUser.getEmail().equals(activityCreatedBy) && (loggedInUser
						.hasRole(UserRole.PL))) || loggedInUser
						.hasRole(UserRole.FL))) {
			isViewable = true;
		} else if (stage == STAGE_FINALIZED) {
			isViewable = true;
		} else {
			isViewable = false;
		}
		return isViewable;
	}

	@JsonIgnore
	public boolean isReviewable() {
		if ((stage == STAGE_2 || stage == CORRECTIONS) && loggedInUser.hasRole(UserRole.FL)) {
			isReviewable = true;
		} else {
			isReviewable = false;
		}
		return isReviewable;
	}

	@JsonIgnore
	public boolean isPublishable() {
		if ((stage == CORRECTIONS) && (loggedInUser.getEmail().equals(
						activityCreatedBy)) && loggedInUser.hasRole(UserRole.PL)) {
			isPublishable = true;
		} else {
			isPublishable = false;
		}
		return isPublishable;
	}
	
	@JsonIgnore
	public boolean isPublished() {
		return stage == STAGE_FINALIZED;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	@JsonIgnore
	public User getLoggedInUser() {
		return loggedInUser;
	}

	@JsonIgnore
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public int getMidYearReviewRating() {
		return midYearReviewRating;
	}

	public void setMidYearReviewRating(int midYearReviewRating) {
		this.midYearReviewRating = midYearReviewRating;
	}

	public String getMidYearReviewProgress() {
		return midYearReviewProgress;
	}

	public void setMidYearReviewProgress(String midYearReviewProgress) {
		this.midYearReviewProgress = midYearReviewProgress;
	}

	public double getTotalSpent() {
		return totalSpent;
	}

	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}

	public List<DeliverableReviewRating> getDeliverableRating() {
		return deliverableRating;
	}

	public void setDeliverableRating(
			List<DeliverableReviewRating> deliverableRating) {
		this.deliverableRating = deliverableRating;
	}

	public List<ActivityReviewRatingComment> getComments() {
		return comments;
	}

	public void setComments(List<ActivityReviewRatingComment> comments) {
		this.comments = comments;
	}

	public double getTotalFunding() {
		return totalFunding;
	}

	public void setTotalFunding(double totalFunding) {
		this.totalFunding = totalFunding;
	}

	public void setActivityCreatedBy(String activityCreatedBy) {
		this.activityCreatedBy = activityCreatedBy;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
