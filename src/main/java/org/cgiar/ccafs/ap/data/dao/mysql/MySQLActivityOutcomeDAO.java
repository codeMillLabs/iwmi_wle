/**
 * 
 */
package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityOutcomeDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.model.ActivityOutcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * @author Amila Silva
 *
 */
public class MySQLActivityOutcomeDAO implements ActivityOutcomeDAO {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityOutcomeDAO.class);
	private DAOManager databaseManager;

	@Inject
	public MySQLActivityOutcomeDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	private static final String OUTCOME_INSERT_SQL = "INSERT INTO iwmi_activity_outcome "
			+ " ( activity_id, users, users_role, changes, gen_desc, intermediaries, capacity_building, progress) "
			+ " VALUES ( ?, ?, ?, ?, ?, ?, ?)";

	@Override
	public int saveOutcome(ActivityOutcome activityOutcome) {
		LOG.debug(">> saveOutcome( activityOutcome={})", activityOutcome);

		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[] { activityOutcome.getActivityId(),
					activityOutcome.getResearchUser(),
					activityOutcome.getResearchUserRole(),
					activityOutcome.getResearchOutcomeChnages(),
					activityOutcome.getGenderDescription(),
					activityOutcome.getIntermediaries(),
					activityOutcome.getCapacaityBuilding(),
					activityOutcome.getProgress() };

			int outcomeId = databaseManager.makeChangeSecure(con,
					OUTCOME_INSERT_SQL, values);

			if (outcomeId < 0) {
				LOG.error("There was a problem saving the outcome data.");
			}

			LOG.debug("<< saveOutcome(): activityOutcome Id : {} ", outcomeId);
			return outcomeId;
		} catch (SQLException e) {
			LOG.error("-- saveOutcome() > There was an error saving the outcome data.");
			LOG.error("Error: ", e);
		}
		return -1;
	}

	private static final String OUTCOME_UPDATE_SQL = "UPDATE iwmi_activity_outcome SET "
			+ " users = ?, users_role =?, changes = ?, gen_desc = ?, intermediaries = ?, capacity_building = ?, progress = ? "
			+ " WHERE  activity_id = ? ";

	@Override
	public void updateOutcome(ActivityOutcome activityOutcome) {
		LOG.debug(">> updateOutcome( activityOutcome={})", activityOutcome);

		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[] { activityOutcome.getResearchUser(),
					activityOutcome.getResearchUserRole(),
					activityOutcome.getResearchOutcomeChnages(),
					activityOutcome.getGenderDescription(),
					activityOutcome.getIntermediaries(),
					activityOutcome.getCapacaityBuilding(),
					activityOutcome.getProgress(),
					activityOutcome.getActivityId() };

			int outcomeId = databaseManager.makeChangeSecure(con,
					OUTCOME_UPDATE_SQL, values);

			if (outcomeId < 0) {
				LOG.error("There was a problem updating the outcome data.");
			}

			LOG.debug("<< updateOutcome(): activityOutcome Id : {} ", outcomeId);
		} catch (SQLException e) {
			LOG.error("-- updateOutcome() > There was an error updating the outcome data.");
			LOG.error("Error: ", e);
		}
	}

	@Override
	public List<ActivityOutcome> getActivityOutcomeByActivity(int activityId) {
		LOG.debug(">> getActivityOutcomeByActivity (activityID={})", activityId);
		List<ActivityOutcome> outcomes = new ArrayList<ActivityOutcome>();

		final String FETCH_OUTCOME_SQL = " SELECT "
				+ " id, activity_id, users, users_role, changes, gen_desc, intermediaries, capacity_building, progress  "
				+ " FROM  iwmi_activity_outcome WHERE activity_id = '"
				+ activityId + "' ";

		// Querying outcome record.
		try (Connection con = databaseManager.getConnection();
				ResultSet rs = databaseManager
						.makeQuery(FETCH_OUTCOME_SQL, con)) {
			if (rs.next()) {
				ActivityOutcome outcome = new ActivityOutcome();
				outcome.setId(rs.getInt("id"));
				outcome.setActivityId(rs.getInt("activity_id"));
				outcome.setResearchUser(rs.getString("users"));
				outcome.setResearchUserRole(rs.getString("users_role"));
				outcome.setResearchOutcomeChnages(rs.getString("changes"));
				outcome.setGenderDescription(rs.getString("gen_desc"));
				outcome.setIntermediaries(rs.getString("intermediaries"));
				outcome.setCapacaityBuilding(rs.getString("capacity_building"));
				outcome.setProgress(rs.getString("progress"));

				outcomes.add(outcome);
			}
			rs.close();

		} catch (SQLException e) {
			LOG.error(
					"-- getActivityOutcomeByActivity() > There was an error getting outcome for activity {}",
					activityId, e);
			return null;
		}

		LOG.debug("<< getActivityOutcomeByActivity(): Size : {}",
				outcomes.size());
		return outcomes;
	}

}
