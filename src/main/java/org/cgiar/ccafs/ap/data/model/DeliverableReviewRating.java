/*
 * FILENAME
 *     DeliverableReviewRating.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliverableReviewRating implements Serializable {

	private static final long serialVersionUID = 3582291723039787446L;

	private int deliverableId;
	private int year;
	private String outputType;
	private String comment;

	private int midYearReviewRating= -1;
	private String midYearReviewProgress;

	private String toolType;

	private String databaseOpenAccess;
	private String databaseLink;

	private String dataSetOpenAccess;
	private String dataSetLink;

	private String pubType;
	private String pubReference;
	private String pubWebLink;
	private String pubOpenAccess;
	private String publishedInISI;
	private String journalPeerReviewed;
	private String wasThisInLastYearReport;

	private String typeCapacityDev;
	private String trainingMethod;
	private String trainingCompleted;
	private String titleOfWorkshop;
	private String date;
	private String location;
	private String noOfTrainees;
	private String evalutionOfCourse;
	private String studyPlanned;
	private String typeOfTraining;
	private String countries;
	private String noCgiarFemales;
	private String noCgiarFromDev;
	private String noFemales;
	private String noFromNars;
	private String noFromDev;
	private String level;

	private String nameOfStakeholderPlatform;

	private String techType;
	private String techOther;
	private String stageOfDevelopment;

	private String nameOfPolicyProcedure;
	private String stageOfDevelopement;

	private String typeOfCommunication;
	private String typeOfOutreachedActivity;

	private String otherDesc;

	/**
	 * <p>
	 * Getter for deliverableId.
	 * </p>
	 * 
	 * @return the deliverableId
	 */
	public int getDeliverableId() {
		return deliverableId;
	}

	/**
	 * <p>
	 * Setting value for deliverableId.
	 * </p>
	 * 
	 * @param deliverableId
	 *            the deliverableId to set
	 */
	public void setDeliverableId(int deliverableId) {
		this.deliverableId = deliverableId;
	}

	/**
	 * <p>
	 * Getter for year.
	 * </p>
	 * 
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <p>
	 * Setting value for year.
	 * </p>
	 * 
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * <p>
	 * Getter for outputType.
	 * </p>
	 * 
	 * @return the outputType
	 */
	public String getOutputType() {
		return outputType;
	}

	/**
	 * <p>
	 * Setting value for outputType.
	 * </p>
	 * 
	 * @param outputType
	 *            the outputType to set
	 */
	public void setOutputType(String outputType) {
		this.outputType = outputType;
	}

	/**
	 * <p>
	 * Getter for midYearReviewRating.
	 * </p>
	 * 
	 * @return the midYearReviewRating
	 */
	public int getMidYearReviewRating() {
		return midYearReviewRating;
	}

	/**
	 * <p>
	 * Setting value for midYearReviewRating.
	 * </p>
	 * 
	 * @param midYearReviewRating
	 *            the midYearReviewRating to set
	 */
	public void setMidYearReviewRating(int midYearReviewRating) {
		this.midYearReviewRating = midYearReviewRating;
	}

	/**
	 * <p>
	 * Getter for midYearReviewProgress.
	 * </p>
	 * 
	 * @return the midYearReviewProgress
	 */
	public String getMidYearReviewProgress() {
		return midYearReviewProgress;
	}

	/**
	 * <p>
	 * Setting value for midYearReviewProgress.
	 * </p>
	 * 
	 * @param midYearReviewProgress
	 *            the midYearReviewProgress to set
	 */
	public void setMidYearReviewProgress(String midYearReviewProgress) {
		this.midYearReviewProgress = midYearReviewProgress;
	}

	public String getToolType() {
		return toolType;
	}

	public void setToolType(String toolType) {
		this.toolType = toolType;
	}

	public String getDatabaseOpenAccess() {
		return databaseOpenAccess;
	}

	public void setDatabaseOpenAccess(String databaseOpenAccess) {
		this.databaseOpenAccess = databaseOpenAccess;
	}

	public String getDatabaseLink() {
		return databaseLink;
	}

	public void setDatabaseLink(String databaseLink) {
		this.databaseLink = databaseLink;
	}

	public String getDataSetOpenAccess() {
		return dataSetOpenAccess;
	}

	public void setDataSetOpenAccess(String dataSetOpenAccess) {
		this.dataSetOpenAccess = dataSetOpenAccess;
	}

	public String getDataSetLink() {
		return dataSetLink;
	}

	public void setDataSetLink(String dataSetLink) {
		this.dataSetLink = dataSetLink;
	}

	public String getPubType() {
		return pubType;
	}

	public void setPubType(String pubType) {
		this.pubType = pubType;
	}

	public String getPubReference() {
		return pubReference;
	}

	public void setPubReference(String pubReference) {
		this.pubReference = pubReference;
	}

	public String getPubWebLink() {
		return pubWebLink;
	}

	public void setPubWebLink(String pubWebLink) {
		this.pubWebLink = pubWebLink;
	}

	public String getPubOpenAccess() {
		return pubOpenAccess;
	}

	public void setPubOpenAccess(String pubOpenAccess) {
		this.pubOpenAccess = pubOpenAccess;
	}

	public String getPublishedInISI() {
		return publishedInISI;
	}

	public void setPublishedInISI(String publishedInISI) {
		this.publishedInISI = publishedInISI;
	}

	public String getJournalPeerReviewed() {
		return journalPeerReviewed;
	}

	public void setJournalPeerReviewed(String journalPeerReviewed) {
		this.journalPeerReviewed = journalPeerReviewed;
	}

	public String getWasThisInLastYearReport() {
		return wasThisInLastYearReport;
	}

	public void setWasThisInLastYearReport(String wasThisInLastYearReport) {
		this.wasThisInLastYearReport = wasThisInLastYearReport;
	}

	public String getTypeCapacityDev() {
		return typeCapacityDev;
	}

	public void setTypeCapacityDev(String typeCapacityDev) {
		this.typeCapacityDev = typeCapacityDev;
	}

	public String getTrainingMethod() {
		return trainingMethod;
	}

	public void setTrainingMethod(String trainingMethod) {
		this.trainingMethod = trainingMethod;
	}

	public String getTrainingCompleted() {
		return trainingCompleted;
	}

	public void setTrainingCompleted(String trainingCompleted) {
		this.trainingCompleted = trainingCompleted;
	}

	public String getTitleOfWorkshop() {
		return titleOfWorkshop;
	}

	public void setTitleOfWorkshop(String titleOfWorkshop) {
		this.titleOfWorkshop = titleOfWorkshop;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNoOfTrainees() {
		return noOfTrainees;
	}

	public void setNoOfTrainees(String noOfTrainees) {
		this.noOfTrainees = noOfTrainees;
	}

	public String getEvalutionOfCourse() {
		return evalutionOfCourse;
	}

	public void setEvalutionOfCourse(String evalutionOfCourse) {
		this.evalutionOfCourse = evalutionOfCourse;
	}

	public String getStudyPlanned() {
		return studyPlanned;
	}

	public void setStudyPlanned(String studyPlanned) {
		this.studyPlanned = studyPlanned;
	}

	public String getTypeOfTraining() {
		return typeOfTraining;
	}

	public void setTypeOfTraining(String typeOfTraining) {
		this.typeOfTraining = typeOfTraining;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getNameOfStakeholderPlatform() {
		return nameOfStakeholderPlatform;
	}

	public void setNameOfStakeholderPlatform(String nameOfStakeholderPlatform) {
		this.nameOfStakeholderPlatform = nameOfStakeholderPlatform;
	}

	public String getTechType() {
		return techType;
	}

	public void setTechType(String techType) {
		this.techType = techType;
	}

	public String getStageOfDevelopment() {
		return stageOfDevelopment;
	}

	public void setStageOfDevelopment(String stageOfDevelopment) {
		this.stageOfDevelopment = stageOfDevelopment;
	}

	public String getNameOfPolicyProcedure() {
		return nameOfPolicyProcedure;
	}

	public void setNameOfPolicyProcedure(String nameOfPolicyProcedure) {
		this.nameOfPolicyProcedure = nameOfPolicyProcedure;
	}

	public String getStageOfDevelopement() {
		return stageOfDevelopement;
	}

	public void setStageOfDevelopement(String stageOfDevelopement) {
		this.stageOfDevelopement = stageOfDevelopement;
	}

	public String getTypeOfCommunication() {
		return typeOfCommunication;
	}

	public void setTypeOfCommunication(String typeOfCommunication) {
		this.typeOfCommunication = typeOfCommunication;
	}

	public String getTypeOfOutreachedActivity() {
		return typeOfOutreachedActivity;
	}

	public void setTypeOfOutreachedActivity(String typeOfOutreachedActivity) {
		this.typeOfOutreachedActivity = typeOfOutreachedActivity;
	}

	public String getCountries() {
		return countries;
	}

	public void setCountries(String countries) {
		this.countries = countries;
	}

	public String getNoCgiarFemales() {
		return noCgiarFemales;
	}

	public void setNoCgiarFemales(String noCgiarFemales) {
		this.noCgiarFemales = noCgiarFemales;
	}

	public String getNoCgiarFromDev() {
		return noCgiarFromDev;
	}

	public void setNoCgiarFromDev(String noCgiarFromDev) {
		this.noCgiarFromDev = noCgiarFromDev;
	}

	public String getNoFemales() {
		return noFemales;
	}

	public void setNoFemales(String noFemales) {
		this.noFemales = noFemales;
	}

	public String getNoFromNars() {
		return noFromNars;
	}

	public void setNoFromNars(String noFromNars) {
		this.noFromNars = noFromNars;
	}

	public String getNoFromDev() {
		return noFromDev;
	}

	public void setNoFromDev(String noFromDev) {
		this.noFromDev = noFromDev;
	}

	public String getTechOther() {
		return techOther;
	}

	public void setTechOther(String techOther) {
		this.techOther = techOther;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
