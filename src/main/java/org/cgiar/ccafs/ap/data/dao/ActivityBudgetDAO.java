/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityBudgetDAO;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;

import com.google.inject.ImplementedBy;

/**
 * 
 * @author Amila Silva
 *
 */
@ImplementedBy(MySQLActivityBudgetDAO.class)
public interface ActivityBudgetDAO {

	/**
	 * Get the budget data of a given activity.
	 * 
	 * @param activityID
	 *            - activity identifier.
	 * @return the Map with the budget data or null if there is no budget.
	 */
	public ActivityBudget getBudget(int activityID);

	/**
	 * Save budget information into the DAO
	 * 
	 * @param budgetData
	 *            - data to be saved
	 * @return true if the data was successfully saved, false otherwise.
	 */
	public int saveActivityBudget(ActivityBudget activityBudget);

	/**
	 * update budget information into the DAO
	 * 
	 * @param budgetData
	 *            - data to be updated
	 */
	public void updateActivityBudget(ActivityBudget budget);
	
	/**
	 * <p>
	 * Retrieve budget activities with basic activity info by given criteria.
	 * </p>
	 *
	 * @param year year
	 * @param activityId activity id
	 * @param leadCenter activity lead Center
	 * @param cluster cluster details
	 * @param flagship flagship details
	 * @return list of {@link ActivityBudget}
	 *
	 */
	public List<ActivityBudget> getActivityBudgetsByCriteria(int year, int activityId, String leadCenter, String cluster, String flagship);

}
