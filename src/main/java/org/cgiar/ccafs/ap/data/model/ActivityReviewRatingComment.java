package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityReviewRatingComment implements Serializable, Comparable<ActivityReviewRatingComment> {

	private static final long serialVersionUID = 7970908815544181431L;
	private Date date;
	private String reviewer;
	private String comment;

	public static String getJSON(List<ActivityReviewRatingComment> comments)
			throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(comments);
		return jsonStr;
	}

	public static List<ActivityReviewRatingComment> populateObject(
			String jsonString) throws Exception {
		List<ActivityReviewRatingComment> comments = new ArrayList<ActivityReviewRatingComment>();
		if (jsonString != null) {
			ObjectMapper mapper = new ObjectMapper();
			comments = mapper.readValue(jsonString,
					new TypeReference<List<ActivityReviewRatingComment>>() {
					});
		}
		return comments;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@JsonIgnore
	@Override
	public int compareTo(ActivityReviewRatingComment o) {
		return o.getDate().compareTo(this.getDate());
	}

}
