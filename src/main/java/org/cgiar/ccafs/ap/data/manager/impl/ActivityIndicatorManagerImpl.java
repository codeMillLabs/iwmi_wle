/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityIndicatorDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityIndicatorManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.model.ActivityIndicator;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;

import com.google.inject.Inject;

public class ActivityIndicatorManagerImpl implements ActivityIndicatorManager{
	private ActivityIndicatorDAO activityIndicatorDAO;
	private DataLookUpManager lookUpManager;

	@Inject
	public ActivityIndicatorManagerImpl(
			ActivityIndicatorDAO activityIndicatorDAO, DataLookUpManager lookUpManager) {
		this.activityIndicatorDAO = activityIndicatorDAO;
		this.lookUpManager = lookUpManager;
	}
	
	@Override
	public boolean deleteActivityIndicators(int activityID) {
		return activityIndicatorDAO.deleteActivityIndicators(activityID);
	}

	@Override
	public List<ActivityIndicator> getActivityIndicators(int activityID) {
		List<Map<String, String>> activityPublicationsDataList = activityIndicatorDAO
				.getActivityIndicators(activityID);
		List<ActivityIndicator> activityIndicators = new ArrayList<>();

		for (Map<String, String> ao : activityPublicationsDataList) {
			ActivityIndicator activityIndicator = new ActivityIndicator();
			activityIndicator.setId(Integer.parseInt(ao.get("id")));
			activityIndicator.setItype(lookUpManager.getLookUpdataById(LookupInfoEnum.INDICATOR_MAIN, Long.parseLong(ao.get("itype"))));
			activityIndicator.setSubType(lookUpManager.getLookUpdataById(LookupInfoEnum.INDICATOR_SUB, Long.parseLong(ao.get("sub_type"))));
			activityIndicator.setTarget(ao.get("target"));
			activityIndicator.setActual(ao.get("actual"));
			activityIndicator.setContribution(ao.get("contribution"));
			activityIndicator.setComment(ao.get("comment"));
			activityIndicators.add(activityIndicator);
		}
		return activityIndicators;
	}

	@Override
	public int saveActivityIndicators(List<ActivityIndicator> indicators,
			int activityID) {
		int saved = 1;
		for (ActivityIndicator indicator : indicators) {
			Map<String, String> actObjData = new HashMap<>();
			if (indicator.getId() == -1) {
				actObjData.put("id", null);
			} else {
				actObjData.put("id", String.valueOf(indicator.getId()));
			}
			actObjData.put("itype", String.valueOf(indicator.getItype().getId()));
			actObjData.put("sub_type", String.valueOf(indicator.getSubType().getId()));
			actObjData.put("target", indicator.getTarget());
			actObjData.put("actual", indicator.getActual());
			actObjData.put("contribution", indicator.getContribution());
			actObjData.put("comment", indicator.getComment());

			int id = activityIndicatorDAO.saveActivityIndicators(actObjData,
					activityID);
			saved *= id;
		}
		return saved;
	}
}
