/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

public class FlagshipFocalRegion {

	private int id;
	private String fsFrNumber;
	private String name;
	private String leaderA;
	private int leaderAId;
	private String leaderB;
	private int leaderBId;
	private String longName;
	private String orgName;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the fsFrNumber
	 */
	public String getFsFrNumber() {
		return fsFrNumber;
	}
	
	/**
	 * @param fsFrNumber the fsFrNumber to set
	 */
	public void setFsFrNumber(String fsFrNumber) {
		this.fsFrNumber = fsFrNumber;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the leaderA
	 */
	public String getLeaderA() {
		return leaderA;
	}
	
	/**
	 * @param leaderA the leaderA to set
	 */
	public void setLeaderA(String leaderA) {
		this.leaderA = leaderA;
	}
	
	/**
	 * @return the leaderB
	 */
	public String getLeaderB() {
		return leaderB;
	}
	
	/**
	 * @param leaderB the leaderB to set
	 */
	public void setLeaderB(String leaderB) {
		this.leaderB = leaderB;
	}

	/**
	 * @return the leaderAId
	 */
	public int getLeaderAId() {
		return leaderAId;
	}

	/**
	 * @param leaderAId the leaderAId to set
	 */
	public void setLeaderAId(int leaderAId) {
		this.leaderAId = leaderAId;
	}

	/**
	 * @return the leaderBId
	 */
	public int getLeaderBId() {
		return leaderBId;
	}

	/**
	 * @param leaderBId the leaderBId to set
	 */
	public void setLeaderBId(int leaderBId) {
		this.leaderBId = leaderBId;
	}

	/**
	 * @return the longName
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * @param longName the longName to set
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}
