/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import org.cgiar.ccafs.ap.util.MD5Convert;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class User implements Comparable<User>{

	public enum UserRole {
		PI, //Principal Investigator
		CP, //Contact Point
		TL, //Theme Leader
		RPL, //Regional Program Leader
		PL, //Project Leader
		SP, //Specialists
		CA, //Center Administrator
		Admin, //Center Administrator
		FL, //Flagship Leader
		WLME, // WLE M&E Manager
		WLEM,// WLE Manager
		WLED, // WLE Director
	}

	private int id;
	private String name;
	private String email;
	private String password;
	private List<UserRole> roles;
	private Date lastLogin;
	private Leader leader;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User u = (User) obj;
			return u.getEmail().equals(this.getEmail());
		}
		return false;
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public Leader getLeader() {
		return leader;
	}

	public String getName() {
		return name;
	}

	/**
	 * @return the password previously codified using MD5 algorithm.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the roles
	 */
	public List<UserRole> getRoles() {
		if(this.roles == null) {
			this.roles = new ArrayList<UserRole>();
		}
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	/**
	 * Validate if the current user is an Administrator.
	 * 
	 * @return true if the user is actually an Administrator, or false
	 *         otherwise.
	 */
	public boolean isAdmin() {
		return hasRole(UserRole.Admin);
	}

	/**
	 * Validate if the current user is a Contact Point.
	 * 
	 * @return true if the user is actually a Contact Point, or false otherwise.
	 */
	public boolean isCP() {
		return hasRole(UserRole.CP);
	}

	/**
	 * Validate if the current user is a Principal Investigator.
	 * 
	 * @return true if the user is actually a Principal Investigator, or false
	 *         otherwise.
	 */
	public boolean isPI() {
		return hasRole(UserRole.PI);
	}

	/**
	 * Validate if the current user is a Regional Program Leader.
	 * 
	 * @return true if the user is actually a Regional Program Leader, or false
	 *         otherwise.
	 */
	public boolean isRPL() {
		return hasRole(UserRole.RPL);
	}

	/**
	 * Validate if the current user is a Theme Leader.
	 * 
	 * @return true if the user is actually a Theme Leader, or false otherwise.
	 */
	public boolean isTL() {
		return hasRole(UserRole.TL);
	}
	
	/**
	 * Validate if the current user is a Project Leader.
	 * 
	 * @return true if the user is actually a Project Leader, or false
	 *         otherwise.
	 */
	public boolean isPL() {
		return hasRole(UserRole.PL);
	}
	
	/**
	 * Validate if the current user is a Specialist Leader.
	 * 
	 * @return true if the user is actually a Specialist Leader, or false
	 *         otherwise.
	 */
	public boolean isSP() {
		return hasRole(UserRole.SP);
	}
	
	/**
     * Validate if the current user is a Center Admin.
     * 
     * @return true if the user is actually a Center Admin, or false
     *         otherwise.
     */
    public boolean isCA() {
        return hasRole(UserRole.CA);
    }
    
    /**
     * Validate if the current user is a Flagship Leader.
     * 
     * @return true if the user is actually a Flagship Leader, or false
     *         otherwise.
     */
    public boolean isFL() {
        return hasRole(UserRole.FL);
    }
    
    /**
     * Validate if the current user is a WLE M&E Manager.
     * 
     * @return true if the user is actually a WLE M&E Manager, or false
     *         otherwise.
     */
    public boolean isWLME() {
        return hasRole(UserRole.WLME);
    }
    
    /**
     * Validate if the current user is a WLE Manager.
     * 
     * @return true if the user is actually a WLE Manager, or false
     *         otherwise.
     */
    public boolean isWLEM() {
        return hasRole(UserRole.WLEM);
    }
    
    /**
     * Validate if the current user is a WLE Director.
     * 
     * @return true if the user is actually a WLE Director, or false
     *         otherwise.
     */
    public boolean isWLED() {
        return hasRole(UserRole.WLED);
    }
    
	public boolean hasRole(UserRole userRole) {
		for(UserRole role : this.roles) {
			if(role.name().equals(userRole.name()))
				return true;
		}
		return false;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public void setLeader(Leader leader) {
		this.leader = leader;
	}

	public void setMD5Password(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method will calculate the MD5 of the provided parameter.
	 * 
	 * @param password
	 *            normal String.
	 */
	public void setPassword(String password) {
		if (password != null) {
			this.password = MD5Convert.stringToMD5(password);
		} else {
			this.password = null;
		}
	}

	@JsonIgnore
	public void setRole(String roleString) {

		if(roleString == null) return;
		
		String[] roles = roleString.split(",", -1);
		
		for(String role : roles) {
			
			switch (role) {
			case "Admin":
				getRoles().add(UserRole.Admin);
				break;
			case "CP":
				getRoles().add(UserRole.CP);
				break;
			case "TL":
				getRoles().add(UserRole.TL);
				break;
			case "PL":
				getRoles().add(UserRole.PL);
				break;
			case "RPL":
				getRoles().add(UserRole.RPL);
				break;
			case "PI":
				getRoles().add(UserRole.PI);
				break;
			case "SP":
				getRoles().add(UserRole.SP);
				break;
			case "CA":
                getRoles().add(UserRole.CA);
                break;
			case "FL":
                getRoles().add(UserRole.FL);
                break;
			case "WLME":
                getRoles().add(UserRole.WLME);
                break;
			case "WLEM":
                getRoles().add(UserRole.WLEM);
                break;
			case "WLED":
                getRoles().add(UserRole.WLED);
                break;
			default:
				break;
			}
		}
	}
	
	@Override
	public int compareTo(User o) {
		return this.getName().compareTo(o.getName());
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
