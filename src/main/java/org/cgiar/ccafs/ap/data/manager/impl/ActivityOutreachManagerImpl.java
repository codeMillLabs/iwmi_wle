/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityOutreachDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityOutreachManager;
import org.cgiar.ccafs.ap.data.model.ActivityOutreach;

import com.google.inject.Inject;

public class ActivityOutreachManagerImpl implements ActivityOutreachManager {

	private ActivityOutreachDAO activityOutreachDAO;

	@Inject
	public ActivityOutreachManagerImpl(
			ActivityOutreachDAO activityOutreachDAO) {
		this.activityOutreachDAO = activityOutreachDAO;
	}
	
	@Override
	public boolean deleteActivityOutreachs(int activityID) {
		return activityOutreachDAO.deleteActivityOutreachs(activityID);
	}

	@Override
	public List<ActivityOutreach> getActivityOutreachs(int activityID) {
		List<Map<String, String>> activityPublicationsDataList = activityOutreachDAO
				.getActivityOutreachs(activityID);
		List<ActivityOutreach> activityOutreachs = new ArrayList<>();

		for (Map<String, String> ao : activityPublicationsDataList) {
			ActivityOutreach activityOutreach = new ActivityOutreach();
			activityOutreach.setId(Integer.parseInt(ao.get("id")));
			activityOutreach.setType(ao.get("otype"));
			activityOutreach.setGroups(ao.get("groups"));
			activityOutreach.setParicipants(ao.get("paricipants"));
			activityOutreach.setFemaleParticipants(ao.get("female_participants"));
			activityOutreach.setDateAndLocation(ao.get("date_and_location"));
			activityOutreach.setComments(ao.get("comments"));
			activityOutreachs.add(activityOutreach);
		}
		return activityOutreachs;
	}

	@Override
	public int saveActivityOutreachs(List<ActivityOutreach> outreachs,
			int activityID) {
		int saved = 1;
		for (ActivityOutreach outreach : outreachs) {
			Map<String, String> actObjData = new HashMap<>();
			if (outreach.getId() == -1) {
				actObjData.put("id", null);
			} else {
				actObjData.put("id", String.valueOf(outreach.getId()));
			}
			actObjData.put("otype", outreach.getType());
			actObjData.put("groups", outreach.getGroups());
			actObjData.put("paricipants", String.valueOf(outreach.getParicipants()));
			actObjData.put("female_participants", String.valueOf(outreach.getFemaleParticipants()));
			actObjData.put("date_and_location", outreach.getDateAndLocation());
			actObjData.put("comments", outreach.getComments());

			int id = activityOutreachDAO.saveActivityOutreachs(actObjData,
					activityID);
			saved *= id;
		}
		return saved;
	}

}
