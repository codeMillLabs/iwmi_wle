/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardMainPageFirstSection;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardSecondPage;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageOutput;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageProject;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScorecardMainPageFlagship;
import org.cgiar.ccafs.ap.data.dao.FlagshipFocalRegionDAO;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipReportData;
import org.cgiar.ccafs.ap.data.model.FlagshipRptStatus;

import com.google.inject.Inject;

public class FlagshipFocalRegionManagerImpl implements
		FlagshipFocalRegionManager {

	private FlagshipFocalRegionDAO flagshipFocalRegionDao;

	@Inject
	public FlagshipFocalRegionManagerImpl(
			FlagshipFocalRegionDAO flagshipFocalRegionDao) {
		this.flagshipFocalRegionDao = flagshipFocalRegionDao;
	}

	@Override
	public List<FlagshipFocalRegion> getFlagshipFocalRegions() {
		List<Map<String, String>> flagshipFocalRegionsDataList = flagshipFocalRegionDao
				.getFlagshipFocalRegions();
		List<FlagshipFocalRegion> flagshipFocalRegions = new ArrayList<>();

		for (Map<String, String> flagshipFocalRegion : flagshipFocalRegionsDataList) {
			FlagshipFocalRegion fsfor = new FlagshipFocalRegion();
			fsfor.setId(Integer.parseInt(flagshipFocalRegion.get("id")));
			fsfor.setFsFrNumber(flagshipFocalRegion.get("fs_fr_no"));
			fsfor.setName(flagshipFocalRegion.get("name"));
			fsfor.setLeaderA(flagshipFocalRegion.get("leadera") == null ? ""
					: flagshipFocalRegion.get("leadera"));
			fsfor.setLeaderAId(Integer.parseInt(flagshipFocalRegion
					.get("leadera_id")));
			fsfor.setLeaderB(flagshipFocalRegion.get("leaderb") == null ? ""
					: flagshipFocalRegion.get("leaderb"));
			fsfor.setLeaderBId(Integer.parseInt(flagshipFocalRegion
					.get("leaderb_id")));
			fsfor.setLongName(flagshipFocalRegion.get("long_name"));
			fsfor.setOrgName(flagshipFocalRegion.get("org_name"));
			flagshipFocalRegions.add(fsfor);
		}

		return flagshipFocalRegions;
	}

	@Override
	public List<Integer> getClusterIds(int flagshipId) {
		return flagshipFocalRegionDao.getClusterIds(flagshipId);
	}

	@Override
	public List<FlagshipReportData> getFlagshipReportData(int year,
			int flagshipId, String cluster) {
		List<Map<String, String>> flagshipFocalRegionsDataList = flagshipFocalRegionDao
				.getFlagshipReportData(year, flagshipId, cluster);
		List<FlagshipReportData> data = new ArrayList<>();

		for (Map<String, String> flagshipFocalRegion : flagshipFocalRegionsDataList) {
			FlagshipReportData flagshipReportData = new FlagshipReportData();
			flagshipReportData.setId(Integer.parseInt(flagshipFocalRegion
					.get("id")));
			flagshipReportData
					.setFlagshipFocalRegion(getFlagshipFocalRegion(flagshipId));
			flagshipReportData.setOutputCode(flagshipFocalRegion
					.get("output_code"));
			flagshipReportData.setOutput(flagshipFocalRegion.get("output"));
			flagshipReportData.setCluster(flagshipFocalRegion.get("cluster"));
			flagshipReportData.setProject(flagshipFocalRegion.get("project"));
			flagshipReportData.setActivityId(flagshipFocalRegion
					.get("activity_id"));
			flagshipReportData.setDelCode(flagshipFocalRegion.get("del_code"));
			flagshipReportData.setDeliverable(flagshipFocalRegion.get("del"));
			data.add(flagshipReportData);
		}

		return data;
	}

	private FlagshipFocalRegion getFlagshipFocalRegion(int id) {
		Map<String, String> flagshipFocalRegionsDataMap = flagshipFocalRegionDao
				.getFlagshipFocalRegion(id);
		FlagshipFocalRegion fsfor = new FlagshipFocalRegion();
		fsfor.setId(Integer.parseInt(flagshipFocalRegionsDataMap.get("id")));
		fsfor.setFsFrNumber(flagshipFocalRegionsDataMap.get("fs_fr_no"));
		fsfor.setName(flagshipFocalRegionsDataMap.get("name"));
		fsfor.setLeaderA(flagshipFocalRegionsDataMap.get("leadera") == null ? ""
				: flagshipFocalRegionsDataMap.get("leadera"));
		fsfor.setLeaderAId(Integer.parseInt(flagshipFocalRegionsDataMap
				.get("leadera_id")));
		fsfor.setLeaderB(flagshipFocalRegionsDataMap.get("leaderb") == null ? ""
				: flagshipFocalRegionsDataMap.get("leaderb"));
		fsfor.setLeaderBId(Integer.parseInt(flagshipFocalRegionsDataMap
				.get("leaderb_id")));
		fsfor.setLongName(flagshipFocalRegionsDataMap.get("long_name"));

		return fsfor;
	}

	@Override
	public int saveFlagshipRptStatus(FlagshipRptStatus rptStatus) {
		Map<String, String> rptStatusMap = new HashMap<String, String>();
		rptStatusMap.put("year", String.valueOf(rptStatus.getYear()));
		rptStatusMap.put("flagship_id",
				String.valueOf(rptStatus.getFlagshipId()));
		rptStatusMap.put("period", rptStatus.getPeriod());
		return flagshipFocalRegionDao.saveFlagshipRptStatus(rptStatusMap);
	}

	@Override
	public boolean checkReportAvailable(int year, int flagshipId, String period) {
		return flagshipFocalRegionDao.checkReportAvailable(year, flagshipId,
				period);
	}

	@Override
	public List<Integer> getFlaghipIdsByActivity(String activityId) {
		return flagshipFocalRegionDao.getFlaghipIdsByActivity(activityId);
	}

	@Override
	public int saveMainPageFirstSection(
			ScoreCardMainPageFirstSection scoreCardMainPageFirstSection) {

		Map<String, String> firstSectionMap = new HashMap<String, String>();
		firstSectionMap
				.put("cluster_green", String
						.valueOf(scoreCardMainPageFirstSection
								.getClusterGreen()));
		firstSectionMap.put("cluster_yellow", String
				.valueOf(scoreCardMainPageFirstSection.getClusterYellow()));
		firstSectionMap.put("cluster_red",
				String.valueOf(scoreCardMainPageFirstSection.getClusterRed()));
		firstSectionMap
				.put("project_green", String
						.valueOf(scoreCardMainPageFirstSection
								.getProjectGreen()));
		firstSectionMap.put("project_yellow", String
				.valueOf(scoreCardMainPageFirstSection.getProjectYellow()));
		firstSectionMap.put("project_red",
				String.valueOf(scoreCardMainPageFirstSection.getProjectRed()));
		firstSectionMap.put("output_green",
				String.valueOf(scoreCardMainPageFirstSection.getOutputGreen()));
		firstSectionMap
				.put("output_yellow", String
						.valueOf(scoreCardMainPageFirstSection
								.getOutputYellow()));
		firstSectionMap.put("output_red",
				String.valueOf(scoreCardMainPageFirstSection.getOutputRed()));
		firstSectionMap.put("spent",
				String.valueOf(scoreCardMainPageFirstSection.getSpent()));
		firstSectionMap.put("not_spent",
				String.valueOf(scoreCardMainPageFirstSection.getNotSpent()));
		firstSectionMap
				.put("cluster_count", String
						.valueOf(scoreCardMainPageFirstSection
								.getClusterCount()));
		firstSectionMap
				.put("project_count", String
						.valueOf(scoreCardMainPageFirstSection
								.getProjectCount()));
		firstSectionMap.put("output_count",
				String.valueOf(scoreCardMainPageFirstSection.getOutputCount()));
		firstSectionMap
				.put("cluster_output_count", String
						.valueOf(scoreCardMainPageFirstSection
								.getClusterOutputCount()));
		return flagshipFocalRegionDao.saveMainPageFirstSection(firstSectionMap);
	}

	@Override
	public ScoreCardMainPageFirstSection getScoreCardMainPageFirstSection() {
		Map<String, String> mainPageFirstSectionMap = flagshipFocalRegionDao
				.getScoreCardMainPageFirstSection();
		ScoreCardMainPageFirstSection scoreCardMainPageFirstSection = new ScoreCardMainPageFirstSection();
		scoreCardMainPageFirstSection.setId(Integer
				.parseInt(mainPageFirstSectionMap.get("id")));
		scoreCardMainPageFirstSection.setClusterGreen(Integer
				.parseInt(mainPageFirstSectionMap.get("cluster_green")));
		scoreCardMainPageFirstSection.setClusterYellow(Integer
				.parseInt(mainPageFirstSectionMap.get("cluster_yellow")));
		scoreCardMainPageFirstSection.setClusterRed(Integer
				.parseInt(mainPageFirstSectionMap.get("cluster_red")));
		scoreCardMainPageFirstSection.setProjectGreen(Integer
				.parseInt(mainPageFirstSectionMap.get("project_green")));
		scoreCardMainPageFirstSection.setProjectYellow(Integer
				.parseInt(mainPageFirstSectionMap.get("project_yellow")));
		scoreCardMainPageFirstSection.setProjectRed(Integer
				.parseInt(mainPageFirstSectionMap.get("project_red")));
		scoreCardMainPageFirstSection.setOutputGreen(Integer
				.parseInt(mainPageFirstSectionMap.get("output_green")));
		scoreCardMainPageFirstSection.setOutputYellow(Integer
				.parseInt(mainPageFirstSectionMap.get("output_yellow")));
		scoreCardMainPageFirstSection.setOutputRed(Integer
				.parseInt(mainPageFirstSectionMap.get("output_red")));
		scoreCardMainPageFirstSection.setSpent(Double
				.parseDouble(mainPageFirstSectionMap.get("spent")));
		scoreCardMainPageFirstSection.setNotSpent(Double
				.parseDouble(mainPageFirstSectionMap.get("not_spent")));
		scoreCardMainPageFirstSection.setClusterCount(Integer
				.parseInt(mainPageFirstSectionMap.get("cluster_count")));
		scoreCardMainPageFirstSection.setProjectCount(Integer
				.parseInt(mainPageFirstSectionMap.get("project_count")));
		scoreCardMainPageFirstSection.setOutputCount(Integer
				.parseInt(mainPageFirstSectionMap.get("output_count")));
		scoreCardMainPageFirstSection.setClusterOutputCount(Integer
				.parseInt(mainPageFirstSectionMap.get("cluster_output_count")));
		return scoreCardMainPageFirstSection;
	}

	@Override
	public int saveScoreCardMainPageFlagship(
			ScorecardMainPageFlagship scorecardMainPageFlagship) {
		Map<String, String> scorecardMainPageFlagshipKeyValues = new HashMap<String, String>();
		scorecardMainPageFlagshipKeyValues.put("green",
				String.valueOf(scorecardMainPageFlagship.getGreen()));
		scorecardMainPageFlagshipKeyValues.put("yellow",
				String.valueOf(scorecardMainPageFlagship.getYellow()));
		scorecardMainPageFlagshipKeyValues.put("red",
				String.valueOf(scorecardMainPageFlagship.getRed()));
		scorecardMainPageFlagshipKeyValues.put("name",
				String.valueOf(scorecardMainPageFlagship.getName()));
		scorecardMainPageFlagshipKeyValues.put("long_name",
				String.valueOf(scorecardMainPageFlagship.getLongName()));
		scorecardMainPageFlagshipKeyValues.put("tooltip",
				String.valueOf(scorecardMainPageFlagship.getToolTip()));
		scorecardMainPageFlagshipKeyValues.put("spent",
				String.valueOf(scorecardMainPageFlagship.getSpent()));
		scorecardMainPageFlagshipKeyValues.put("not_spent",
				String.valueOf(scorecardMainPageFlagship.getNotSpent()));
		return flagshipFocalRegionDao.saveScoreCardMainPageFlagship(scorecardMainPageFlagshipKeyValues);
	}

	@Override
	public List<ScorecardMainPageFlagship> getScorecardMainPageFlagships() {
		List<ScorecardMainPageFlagship> scorecardMainPageFlagships = new ArrayList<>();
		List<Map<String, String>> scorecardMainPageFlagshipDataList = flagshipFocalRegionDao
				.getScoreCardMainPageFlagships();
		for (Map<String, String> valueMap : scorecardMainPageFlagshipDataList) {
			ScorecardMainPageFlagship scorecardMainPageFlagship = new ScorecardMainPageFlagship();
			scorecardMainPageFlagship
					.setId(Integer.parseInt(valueMap.get("id")));
			scorecardMainPageFlagship.setGreen(Integer.parseInt(valueMap
					.get("green")));
			scorecardMainPageFlagship.setYellow(Integer.parseInt(valueMap
					.get("yellow")));
			scorecardMainPageFlagship.setRed(Integer.parseInt(valueMap
					.get("red")));
			scorecardMainPageFlagship.setName(valueMap.get("name"));
			scorecardMainPageFlagship.setLongName(valueMap.get("long_name"));
			scorecardMainPageFlagship.setToolTip(valueMap.get("tooltip"));
			scorecardMainPageFlagship.setSpent(Double.parseDouble(valueMap
					.get("spent")));
			scorecardMainPageFlagship.setNotSpent(Double.parseDouble(valueMap
					.get("not_spent")));
			scorecardMainPageFlagships.add(scorecardMainPageFlagship);
		}

		return scorecardMainPageFlagships;
	}
	
	@Override
	public ScorecardMainPageFlagship getScorecardMainPageFlagships(int id) {
		Map<String, String> valueMap = flagshipFocalRegionDao
				.getScoreCardMainPageFlagship(id);
		ScorecardMainPageFlagship scorecardMainPageFlagship = new ScorecardMainPageFlagship();
		scorecardMainPageFlagship.setId(Integer.parseInt(valueMap.get("id")));
		scorecardMainPageFlagship.setGreen(Integer.parseInt(valueMap
				.get("green")));
		scorecardMainPageFlagship.setYellow(Integer.parseInt(valueMap
				.get("yellow")));
		scorecardMainPageFlagship.setRed(Integer.parseInt(valueMap.get("red")));
		scorecardMainPageFlagship.setName(valueMap.get("name"));
		scorecardMainPageFlagship.setLongName(valueMap.get("long_name"));
		scorecardMainPageFlagship.setToolTip(valueMap.get("tooltip"));
		scorecardMainPageFlagship.setSpent(Double.parseDouble(valueMap
				.get("spent")));
		scorecardMainPageFlagship.setNotSpent(Double.parseDouble(valueMap
				.get("not_spent")));
		return scorecardMainPageFlagship;
	}

	@Override
	public boolean deleteScoreCardCache() {
		return flagshipFocalRegionDao.deleteScoreCardCache();
	}

	@Override
	public int saveScoreCardSecondPage(ScoreCardSecondPage scoreCardSecondPage) {
		Map<String, String> scoreCardSecondPageKeyValues = new HashMap<String, String>();
		scoreCardSecondPageKeyValues.put("flagship_id",
				String.valueOf(scoreCardSecondPage.getFlagshipId()));
		scoreCardSecondPageKeyValues.put("project_green",
				String.valueOf(scoreCardSecondPage.getProjectGreen()));
		scoreCardSecondPageKeyValues.put("project_yellow",
				String.valueOf(scoreCardSecondPage.getProjectYellow()));
		scoreCardSecondPageKeyValues.put("project_red",
				String.valueOf(scoreCardSecondPage.getProjectRed()));
		scoreCardSecondPageKeyValues.put("output_green",
				String.valueOf(scoreCardSecondPage.getOutputGreen()));
		scoreCardSecondPageKeyValues.put("output_yellow",
				String.valueOf(scoreCardSecondPage.getOutputYellow()));
		scoreCardSecondPageKeyValues.put("output_red",
				String.valueOf(scoreCardSecondPage.getOutputRed()));
		scoreCardSecondPageKeyValues.put("rating",
				String.valueOf(scoreCardSecondPage.getRating()));
		scoreCardSecondPageKeyValues.put("name",
				String.valueOf(scoreCardSecondPage.getName()));
		scoreCardSecondPageKeyValues.put("spent",
				String.valueOf(scoreCardSecondPage.getSpent()));
		scoreCardSecondPageKeyValues.put("not_spent",
				String.valueOf(scoreCardSecondPage.getNotSpent()));
		scoreCardSecondPageKeyValues.put("comment",
				scoreCardSecondPage.getComment());
		return flagshipFocalRegionDao.saveScoreCardSecondPage(scoreCardSecondPageKeyValues);
	}

	@Override
	public List<ScoreCardSecondPage> getScoreCardSecondPages(int flagshipId) {
		List<Map<String, String>> scoreCardSecondPageList = flagshipFocalRegionDao
				.getScoreCardSecondPages(flagshipId);
		List<ScoreCardSecondPage> scoreCardSecondPages = new ArrayList<>();
		for (Map<String, String> scoreCardSecondPageData : scoreCardSecondPageList) {
			ScoreCardSecondPage scoreCardSecondPage = new ScoreCardSecondPage();
			scoreCardSecondPage.setId(Integer.parseInt(scoreCardSecondPageData
					.get("id")));
			scoreCardSecondPage.setProjectGreen(Integer
					.parseInt(scoreCardSecondPageData.get("project_green")));
			scoreCardSecondPage.setProjectYellow(Integer
					.parseInt(scoreCardSecondPageData.get("project_yellow")));
			scoreCardSecondPage.setProjectRed(Integer
					.parseInt(scoreCardSecondPageData.get("project_red")));
			scoreCardSecondPage.setOutputGreen(Integer
					.parseInt(scoreCardSecondPageData.get("output_green")));
			scoreCardSecondPage.setOutputYellow(Integer
					.parseInt(scoreCardSecondPageData.get("output_yellow")));
			scoreCardSecondPage.setOutputRed(Integer
					.parseInt(scoreCardSecondPageData.get("output_red")));
			scoreCardSecondPage
					.setRating(scoreCardSecondPageData.get("rating"));
			scoreCardSecondPage.setName(scoreCardSecondPageData.get("name"));
			scoreCardSecondPage.setSpent(Double
					.parseDouble(scoreCardSecondPageData.get("spent")));
			scoreCardSecondPage.setNotSpent(Double
					.parseDouble(scoreCardSecondPageData.get("not_spent")));
			scoreCardSecondPage.setComment(scoreCardSecondPageData.get("comment"));
			scoreCardSecondPages.add(scoreCardSecondPage);
		}
		return scoreCardSecondPages;
	}

	@Override
	public int saveScoreCardThirdPageOutput(ScoreCardThirdPageOutput scoreCardThirdPageOutput) {
		Map<String, String> scoreCardThirdPageOutputKeyValues = new HashMap<String, String>();
		scoreCardThirdPageOutputKeyValues.put("project_id",
				scoreCardThirdPageOutput.getProjectId());
		scoreCardThirdPageOutputKeyValues.put("output_green",
				String.valueOf(scoreCardThirdPageOutput.getOutputGreen()));
		scoreCardThirdPageOutputKeyValues.put("output_yellow",
				String.valueOf(scoreCardThirdPageOutput.getOutputYellow()));
		scoreCardThirdPageOutputKeyValues.put("output_red",
				String.valueOf(scoreCardThirdPageOutput.getOutputRed()));
		scoreCardThirdPageOutputKeyValues.put("name",
				String.valueOf(scoreCardThirdPageOutput.getName()));
		scoreCardThirdPageOutputKeyValues.put("comment",
				scoreCardThirdPageOutput.getComment());
		return flagshipFocalRegionDao.saveScoreCardThirdPageOutput(scoreCardThirdPageOutputKeyValues);
	}

	@Override
	public List<ScoreCardThirdPageOutput> getScoreCardThirdPageOutputs(String projectId) {
		List<Map<String, String>> scoreCardThirdPageList = flagshipFocalRegionDao
				.getScoreCardThirdPageOutputs(projectId);
		List<ScoreCardThirdPageOutput> scoreCardSecondPageOutputs = new ArrayList<>();
		for (Map<String, String> scoreCardSecondPageData : scoreCardThirdPageList) {
			ScoreCardThirdPageOutput scoreCardSecondPage = new ScoreCardThirdPageOutput();
			scoreCardSecondPage.setId(Integer.parseInt(scoreCardSecondPageData
					.get("id")));
			scoreCardSecondPage.setOutputGreen(Integer
					.parseInt(scoreCardSecondPageData.get("output_green")));
			scoreCardSecondPage.setOutputYellow(Integer
					.parseInt(scoreCardSecondPageData.get("output_yellow")));
			scoreCardSecondPage.setOutputRed(Integer
					.parseInt(scoreCardSecondPageData.get("output_red")));
			scoreCardSecondPage.setName(scoreCardSecondPageData.get("name"));
			scoreCardSecondPage.setComment(scoreCardSecondPageData.get("comment"));
			scoreCardSecondPageOutputs.add(scoreCardSecondPage);
		}
		return scoreCardSecondPageOutputs;
	}

	@Override
	public int saveScoreCardThirdPageProject(
			ScoreCardThirdPageProject scoreCardThirdPageProject) {
		Map<String, String> scoreCardThirdPageProjectKeyValues = new HashMap<String, String>();
		scoreCardThirdPageProjectKeyValues.put("flagship_id",
				String.valueOf(scoreCardThirdPageProject.getFlagshipId()));
		scoreCardThirdPageProjectKeyValues.put("project_id",
				scoreCardThirdPageProject.getProjectId());
		scoreCardThirdPageProjectKeyValues.put("title",scoreCardThirdPageProject.getTitle());
		scoreCardThirdPageProjectKeyValues.put("fl_short_name",scoreCardThirdPageProject.getFlagshipShortName());
		return flagshipFocalRegionDao.saveScoreCardThirdPageProject(scoreCardThirdPageProjectKeyValues);
	}

	@Override
	public List<ScoreCardThirdPageProject> getScoreCardThirdPageProjects(
			int flagshipId) {
		List<Map<String, String>> scoreCardThirdPageList = flagshipFocalRegionDao
				.getScoreCardThirdPageProjects(flagshipId);
		List<ScoreCardThirdPageProject> scoreCardSecondPageProjects = new ArrayList<>();
		for (Map<String, String> scoreCardSecondPageData : scoreCardThirdPageList) {
			ScoreCardThirdPageProject scoreCardSecondPage = new ScoreCardThirdPageProject();
			scoreCardSecondPage.setId(Integer.parseInt(scoreCardSecondPageData
					.get("id")));
			scoreCardSecondPage.setProjectId(scoreCardSecondPageData.get("project_id"));
			scoreCardSecondPage.setTitle(scoreCardSecondPageData.get("title"));
			scoreCardSecondPage.setFlagshipShortName(scoreCardSecondPageData.get("fl_short_name"));
			scoreCardSecondPageProjects.add(scoreCardSecondPage);
		}
		return scoreCardSecondPageProjects;
	}

	@Override
	public ScoreCardThirdPageProject getScoreCardThirdPageProject(
			String projectId) {
		Map<String, String> scoreCardSecondPageData = flagshipFocalRegionDao
				.getScoreCardThirdPageProject(projectId);
		ScoreCardThirdPageProject scoreCardSecondPage = new ScoreCardThirdPageProject();
		scoreCardSecondPage.setId(Integer.parseInt(scoreCardSecondPageData
				.get("id")));
		scoreCardSecondPage.setFlagshipId(Integer.parseInt(scoreCardSecondPageData
				.get("flagship_id")));
		scoreCardSecondPage.setTitle(scoreCardSecondPageData.get("title"));
		scoreCardSecondPage.setFlagshipShortName(scoreCardSecondPageData.get("fl_short_name"));
		return scoreCardSecondPage;
	}
}