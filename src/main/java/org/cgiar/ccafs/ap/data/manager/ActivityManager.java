/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.ActivityManagerImpl;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRatingComment;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.Leader;
import org.cgiar.ccafs.ap.data.model.Review;
import org.cgiar.ccafs.ap.data.model.SectionReview;
import org.cgiar.ccafs.ap.data.model.SectionReview.Category;
import org.cgiar.ccafs.ap.data.model.User;

import com.google.inject.ImplementedBy;

@ImplementedBy(ActivityManagerImpl.class)
public interface ActivityManager
{

    /**
     * Get a list of activities that belongs to the given user in the given year.
     * 
     * @param year
     * @param user
     * @return A list of activities. If the user is an administrator, the method will return all the activities of the
     *         given year.
     */
    public Activity[] getActivities(int year, User user, String createdBy, String flagshipValue, ActivityStatus... activityStatus);

    /**
     * Get a list of activities matching the parameters given in order to fill the Activities detail summary.
     * 
     * @param year
     *            - Year when activity was carried out or 0 to indicate no value.
     * @param activityID
     *            - Activity identifier or 0 to indicate no value.
     * @param activityLeader
     *            - Activity leader or 0 to indicate no value.
     * @return
     */
    public Activity[] getActivitiesForDetailedSummary(int year, int activityID, int activityLeader);

    /**
     * Get a list of activities matching the parameters given in order to fill the Activities status summary.
     * 
     * @param year
     *            - Year when activity was carried out or 0 to indicate no value.
     * @param activityID
     *            - Activity identifier or 0 to indicate no value.
     * @param activityLeader
     *            - Activity leader or 0 to indicate no value.
     * @return
     */
    public Activity[] getActivitiesForStatusSummary(int year, int activityID, int activityLeader);

    /**
     * Get a list of activities that belong to a specific year.
     * 
     * @param year
     *            - (Integer) Year
     * @param limit
     *            - Number of activities that will be returned.
     * @return a list of activities ordered by date added.
     */
    public Activity[] getActivitiesForXML(int year, int limit);

    /**
     * Get a list of activities from the given year populated only with the id and the title.
     * 
     * @param year
     * @return a list of activity objects.
     */
    public Activity[] getActivitiesTitle(int year);

    /**
     * Get a list of activities from the given year populated only with the id and the title.
     * 
     * @param year
     * @param leader
     * @return a list of activity objects.
     */
    public Activity[] getActivitiesTitle(int year, Leader leader);

    /**
     * Get a list of activities from the given year that can be continued.
     * 
     * @param year
     * @return an array of Activity objects.
     */
    public Activity[] getActivitiesToContinue(int year);

    /**
     * Get a list of activities from the given year that belongs to the given leader that can be continued.
     * 
     * @param year
     * @param leader
     * @return an array of Activity objects.
     */
    public Activity[] getActivitiesToContinue(int year, int leader);

    /**
     * Get an activity identified with the given id.
     * 
     * @param id
     *            - Activity ID
     * @return an Activity object or null if nothing found.
     */
    public Activity getActivity(int id);
    
    /**
     * Get an activity identified with the given id.
     * 
     * @param id
     *            - Activity ID
     * @return an Activity object or null if nothing found.
     */
    public Activity getActivity(String id);

    /**
     * Get a list of activities to show in the activity table.
     * 
     * @param year
     *            - An integer representing the year in which the activities belong.
     * @return A List of Activity objects that belong to a specific year and user.
     */
    public Activity[] getActivityListByYear(int year);

    /**
     * Get an activity identified with the given id populated only with the data showed in the status reporting
     * interface.
     * 
     * @param id
     * @return an Activity object or null if no activity was found.
     */
    public Activity getActivityStatusInfo(int id);

    /**
     * Get a list of activities that is going to be used in the planning section.
     * 
     * @param year
     *            - An integer representing the year in which the activities belong.
     * @param user
     *            - An User object representing the leader in which the activities belong.
     * @return A List of Activity objects that belong to a specific year and user.
     */
    public Activity[] getPlanningActivityList(int year, User user, ActivityStatus... activityStatus);

    /**
     * Get an activity with its basic information (ID, Title, Leader)
     * 
     * @param id
     *            - identifier
     * @return an Activity object or null if no activity was found with the given id.
     */
    public Activity getSimpleActivity(int id);

    /**
     * Get array of years that having activities.
     * 
     * @return array of years
     */
    public List<Integer> getYearList();

    /**
     * Get the value of the attribute hasPartners
     * 
     * @param activityId
     * @return true if the activity has partners. False otherwise.
     */
    public boolean hasPartners(int activityId);

    /**
     * Validate if the activity is active for current year
     * 
     * @param activityID
     *            - Activity identifier
     * @param year
     *            - Current year
     * @return true if activity is active, false otherwise.
     */
    public boolean isActiveActivity(int activityID, int year);

    /**
     * Validate if the activity given have been validated.
     * 
     * @param activityID
     *            - activity identifier.
     * @return true if the activity was validated. False otherwise.
     */
    public boolean isValidatedActivity(int activityID, String revisionId);

    /**
     * Validate if the given id actually exist in the current list of activities.
     * 
     * @param id
     *            - activity identifier.
     * @return true if the activity exists or false otherwise.
     */
    public boolean isValidId(int id);

    /**
     * Save the information from Planning section into the DAO.
     * 
     * @param activity
     *            - Activity object with the information of an activity.
     * @return true if the information was successfully saved, or false otherwise.
     */
    public boolean saveActivity(Activity activity);
    
    /**
     * Save the information from Planning section into the DAO.
     * 
     * @param activity
     *            - Activity object with the information of an activity.
     * @return true if the information was successfully saved, or false otherwise.
     */
    public int saveNewActivity(Activity activity);

    /**
     * Save the activity attribute hasPartners into the DAO.
     * 
     * @param activity
     * @return
     */
    public boolean saveHasPartners(Activity activity);

    /**
     * Save the activity status information into the DAO.
     * 
     * @param activity
     *            - Activity object with the information populated on it.
     * @return true if the information was saved, or false otherwise.
     */
    public boolean saveStatus(Activity activity);

    /**
     * Set the value of attribute isGlobal into the DAO
     * 
     * @param activity
     *            - Activity object with the information on it
     * @return true if the information was updated successfully, false otherwise.
     */
    public boolean updateGlobalAttribute(Activity activity);

    /**
     * Delete the activity
     * 
     * @param activityId
     *            - Activity id
     */
    public boolean deleteActivity(Long activityId);

    /**
     * Update the main information (Title, description, milestone, budget, start and end date, gender) into the DAO
     * 
     * @param activity
     *            - Activity object with the information populated on it
     * @return true if the information was updated successfully, false otherwise
     */
    public boolean updateMainInformation(Activity activity);

    public boolean updateActivitStatus(int activityId, ActivityStatus activityStatus, String revisionId);
    
    public int updateActivityRevisionId(int activityId);

    /**
     * This method set the activity attribute isValidated to true into the database.
     * 
     * @param activity
     *            - The activity to validate
     * @return true if the process was successful. False, otherwise.
     */
    public boolean validateActivity(Activity activity);

    /**
     * <p>
     * Add activity reviewers.
     * </p>
     *
     * @param activity
     *            {@link Activity}
     *
     */
    public void addActivtyReviewers(Activity activity);

    public List<ActivityReview> getReviewers(int activityId, String revisionId, String type);
    
    
    /**
	 * Fetch specified Review for given activity
	 * 
	 * @param activityId
	 * @param revisionId
	 * @param reviewType
	 * @return
	 */
	public SectionReview fetchReview(int activityId, String revisionId,
			SectionReview.Category reviewType);

	/**
	 * Fetch all Reviews for given activity
	 * 
	 * @param activityId
	 * @param revisionId
	 * @param reviewType
	 * @return
	 */
	public List<SectionReview> fetchAllReviews(int activityId,
			String revisionId, SectionReview.Category reviewType);

	/**
	 * Add review.
	 * 
	 * @param activityId
	 * @param revisionId
	 * @param reviewType
	 * @param review
	 * @return
	 */
	public boolean addReview(int activityId, String revisionId,
			SectionReview.Category reviewType, Review review);

	boolean updateReview(int activityId, String revisionId,
			Category reviewType, SectionReview sectionReview);
	
    
    public boolean saveOrUdatetMidYearRating(String activityId, ActivityReviewRating rating);
	
	public ActivityReviewRating getMidYearRating(String activityId);
	
	public boolean saveMidYearReviewRatingComment(int reviewRatingId, ActivityReviewRatingComment ratingComment);
	
	public List<ActivityReviewRatingComment> getMidYearReviewRatingComment(int reviewRatingId);
	
	int getActivityCount(String cluster);
}
