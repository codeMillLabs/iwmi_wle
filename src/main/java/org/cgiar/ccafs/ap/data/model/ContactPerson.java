/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.cgiar.ccafs.ap.util.WleAppUtil;

public class ContactPerson implements Difference<ContactPerson> {
	
	private static final String EMAIL = "Email";
	private static final String NAME = "Name";

	private int id;
	private String name;
	private String email;

	public ContactPerson() {
	}

	public ContactPerson(int id, String name, String email) {
		this.id = id;
		this.name = name;
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public DiffReport diff(ContactPerson t) {
		DiffReport report = new DiffReport();
		
		if(WleAppUtil.isNotEqual(this.name, t.name)) {
			report.append(NAME, this.name, t.name);
		}
		if(WleAppUtil.isNotEqual(this.email, t.email)) {
			report.append(EMAIL, this.email, t.email);
		}
		return report;
	}

}
