/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
public class DataLookup {

	private long id;
	private LookupInfoEnum lookupInfoEnum;
	private String displayName;
	private String value;
	private List<DataLookup> subLevel;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LookupInfoEnum getLookupInfoEnum() {
		return lookupInfoEnum;
	}

	public void setLookupInfoEnum(LookupInfoEnum lookupInfoEnum) {
		this.lookupInfoEnum = lookupInfoEnum;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<DataLookup> getSubLevel() {
		if(subLevel == null) {
			subLevel = new ArrayList<DataLookup>();
		}
		return subLevel;
	}

	public void setSubLevel(List<DataLookup> subLevel) {
		this.subLevel = subLevel;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
