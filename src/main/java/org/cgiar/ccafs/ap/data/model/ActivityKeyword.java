/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class ActivityKeyword implements Difference<ActivityKeyword>{

	private int id;
	private String other;
	private Keyword keyword;

	@Override
	public DiffReport diff(ActivityKeyword t) {
		DiffReport report = new DiffReport();
		if(isNotEqual(other, t.other)) {
			report.append("Other keywords", other, t.other);
		}
		report.append(keyword.diff(t.getKeyword()));
		
		return report;
	}

	public int getId() {
		return id;
	}

	public Keyword getKeyword() {
		return keyword;
	}

	public String getOther() {
		return other;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setKeyword(Keyword keyword) {
		this.keyword = keyword;
	}

	public void setOther(String other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}