package org.cgiar.ccafs.ap.data.model;

public class FlagshipRptStatus {

	private int id;
	private int year;
	private int flagshipId;
	private String period;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	
	/**
	 * @return the flagshipId
	 */
	public int getFlagshipId() {
		return flagshipId;
	}
	
	/**
	 * @param flagshipId the flagshipId to set
	 */
	public void setFlagshipId(int flagshipId) {
		this.flagshipId = flagshipId;
	}
	
	/**
	 * @return the period
	 */
	public String getPeriod() {
		return period;
	}
	
	/**
	 * @param period the period to set
	 */
	public void setPeriod(String period) {
		this.period = period;
	}
}
