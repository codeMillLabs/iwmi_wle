/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityOutreachDAO;

import com.google.inject.ImplementedBy;

@ImplementedBy(MySQLActivityOutreachDAO.class)
public interface ActivityOutreachDAO {
	/**
	   * Delete all the outreachs related to the activity given.
	   * 
	   * @param activityID - Activity identifier
	   * @return true if was successfully deleted. False otherwise
	   */
	  public boolean deleteActivityOutreachs(int activityID);

	  /**
	   * Get the outreachs related to the activity given
	   * 
	   * @param activityID - the activity identifier
	   * @return a list of activityOutreach objects with the information
	   */
	  public List<Map<String, String>> getActivityOutreachs(int activityID);

	  /**
	   * Save the outreachs corresponding to the activity given
	   * 
	   * @param outreachs - The data to save into the DAO
	   * @param activityID - Activity identifier
	   * @return true if was successfully saved. False otherwise
	   */
	  public int saveActivityOutreachs(Map<String, String> outreachs, int activityID);
}
