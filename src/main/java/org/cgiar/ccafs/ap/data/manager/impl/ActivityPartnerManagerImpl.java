/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import org.cgiar.ccafs.ap.data.dao.ActivityPartnerDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityPartnerManager;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.Country;
import org.cgiar.ccafs.ap.data.model.Partner;
import org.cgiar.ccafs.ap.data.model.PartnerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ActivityPartnerManagerImpl implements ActivityPartnerManager {

  // Logger
  private static final Logger LOG = LoggerFactory.getLogger(ActivityPartnerManagerImpl.class);
  private ActivityPartnerDAO activityPartnerDAO;

  @Inject
  public ActivityPartnerManagerImpl(ActivityPartnerDAO activityPartnerDAO) {
    this.activityPartnerDAO = activityPartnerDAO;
  }

  @Override
  public List<ActivityPartner> getActivityPartners(int activityID) {
    List<Map<String, String>> activityPartnerDataList = activityPartnerDAO.getActivityPartnersList(activityID);
    Map<String, String> activityPartnerData;
    ArrayList<ActivityPartner> activityPartnerList = new ArrayList<ActivityPartner>();
    for (int c = 0; c < activityPartnerDataList.size(); c++) {
      activityPartnerData = activityPartnerDataList.get(c);
      ActivityPartner activityPartner = new ActivityPartner();
      activityPartner.setId(Integer.parseInt(activityPartnerData.get("id")));
      activityPartner.setContactName(activityPartnerData.get("contact_name"));
      activityPartner.setContactEmail(activityPartnerData.get("contact_email"));
      activityPartner.setClasification(activityPartnerData.get("clasification"));
      activityPartner.setBudget((activityPartnerData.get("budget") == null) ? new Double(0.00) : 
    		  Double.valueOf(activityPartnerData.get("budget")));
      // Partner
      Partner partner = new Partner();
      partner.setId(Integer.parseInt(activityPartnerData.get("partner_id")));
      partner.setAcronym(activityPartnerData.get("partner_acronym"));
      partner.setName(activityPartnerData.get("partner_name"));
      // Partner type
      PartnerType partnerType = new PartnerType();
      partnerType.setId(Integer.parseInt(activityPartnerData.get("partner_type_id")));
      partnerType.setName(activityPartnerData.get("partner_type_name"));
      partner.setType(partnerType);
      // Country
      Country country = new Country();
      country.setId(activityPartnerData.get("country_iso2"));
      country.setName(activityPartnerData.get("country_name"));
      partner.setCountry(country);

      activityPartner.setPartner(partner);
      activityPartnerList.add(activityPartner);
    }


    // LOG.debug("The activity partners for the activity {} loaded.", activityID);

    return activityPartnerList;
  }

  @Override
  public boolean removeActivityPartners(int activityID) {
    return activityPartnerDAO.removeActivityPartners(activityID);
  }

  @Override
  public boolean saveActivityPartners(List<ActivityPartner> activityPartners, int activityID) {
    boolean problem = false;
    List<Map<String, Object>> activityPartnersData = new ArrayList<>();
    for (ActivityPartner cp : activityPartners) {
      if (cp.getPartner() == null) {
        // If the partner is null, continue with the next one
        continue;
      }
      Map<String, Object> cpData = new HashMap<>();
      if (cp.getId() != -1) {
        cpData.put("id", cp.getId());
      } else {
        cpData.put("id", null);
      }
      cpData.put("partner_id", cp.getPartner().getId());
      cpData.put("activity_id", activityID);
      if (cp.getContactName() == null || cp.getContactName().isEmpty()) {
        cpData.put("contact_name", null);
      } else {
        cpData.put("contact_name", cp.getContactName());
      }
      if (cp.getContactEmail() == null || cp.getContactEmail().isEmpty()) {
        cpData.put("contact_email", null);
      } else {
        cpData.put("contact_email", cp.getContactEmail());
      }
      if (cp.getClasification() == null || cp.getClasification().isEmpty()) {
    	  cpData.put("clasification", null);
      } else {
    	  cpData.put("clasification", cp.getClasification());
      }
      cpData.put("budget", cp.getBudget());
      
      activityPartnersData.add(cpData);
    }

    // LOG.debug("The activity partners information for activity {} was send to the DAO to save it", activityID);
    problem = !activityPartnerDAO.saveActivityPartnerList(activityPartnersData);
    return !problem;
  }
}
