/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import org.cgiar.ccafs.ap.data.dao.ActivityObjectiveDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.model.ActivityObjective;
import org.cgiar.ccafs.ap.util.WleAppUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

public class ActivityObjectiveManagerImpl implements ActivityObjectiveManager {

	// Logger
	ActivityObjectiveDAO activityObjectiveDAO;

	@Inject
	public ActivityObjectiveManagerImpl(
			ActivityObjectiveDAO activityObjectiveDAO) {
		this.activityObjectiveDAO = activityObjectiveDAO;
	}

	@Override
	public boolean deleteActivityObjectives(int activityId) {
		return activityObjectiveDAO.deleteActivityObjectives(activityId);
	}

	@Override
	public List<ActivityObjective> getActivityObjectives(int activityID) {
		List<Map<String, String>> activityObjectivesDataList = activityObjectiveDAO
				.getActivityObjectives(activityID);
		List<ActivityObjective> activityObjectives = new ArrayList<>();

		for (Map<String, String> ao : activityObjectivesDataList) {
			ActivityObjective activityObjective = new ActivityObjective();
			activityObjective.setId(Integer.parseInt(ao.get("id")));
			activityObjective.setDescription(ao.get("description"));
			activityObjective.setOutcomeType(ao.get("outcome_type"));
			activityObjective.setResearchUser(ao.get("users"));
			activityObjective.setResearchUserRole(WleAppUtil.generateListFromCommaSeperatedString(ao.get("roles")));
			activityObjective.setResearchOutcomeChanges(ao.get("changes"));
			activityObjective.setGenderDescription(ao.get("gender_desc"));
			activityObjective.setIntermediaries(ao.get("intermediaries"));
			activityObjective.setCapacityBuilding(ao.get("capacity"));
			activityObjective.setProgress(ao.get("progress"));
			activityObjectives.add(activityObjective);
		}
		return activityObjectives;
	}

	@Override
	public int saveActivityObjectives(List<ActivityObjective> objectives,
			int activityID) {
		int saved = 1;
		for (ActivityObjective objective : objectives) {
			Map<String, String> actObjData = new HashMap<>();
			if (objective.getId() == -1) {
				actObjData.put("id", null);
			} else {
				actObjData.put("id", String.valueOf(objective.getId()));
			}
			actObjData.put("description", objective.getDescription());
			actObjData.put("outcome_type", objective.getOutcomeType());
			actObjData.put("users", objective.getResearchUser());
			actObjData.put("roles", WleAppUtil.generateCommaSeperatedStringForDB(objective.getResearchUserRole()));
			actObjData.put("changes", objective.getResearchOutcomeChanges());
			actObjData.put("gender_desc", objective.getGenderDescription());
			actObjData.put("intermediaries", objective.getIntermediaries());
			actObjData.put("capacity", objective.getCapacityBuilding());
			actObjData.put("progress", objective.getProgress());

			int id = activityObjectiveDAO.saveActivityObjectives(actObjData,
					activityID);
			saved *= id;
		}
		return saved;
	}
}
