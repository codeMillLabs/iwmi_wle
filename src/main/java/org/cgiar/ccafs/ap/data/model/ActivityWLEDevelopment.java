package org.cgiar.ccafs.ap.data.model;

public class ActivityWLEDevelopment {

	private int id;
	private int activityId;
	private DataLookup wle;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public DataLookup getWle() {
		return wle;
	}

	public void setWle(DataLookup wle) {
		this.wle = wle;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
}
