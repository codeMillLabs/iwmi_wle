/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityMaterialDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityMaterialManager;
import org.cgiar.ccafs.ap.data.model.ActivityMaterial;

import com.google.inject.Inject;

public class ActivityMaterialManagerImpl implements ActivityMaterialManager{

	private ActivityMaterialDAO activityMaterialDAO;

	@Inject
	public ActivityMaterialManagerImpl(
			ActivityMaterialDAO activityMaterialDAO) {
		this.activityMaterialDAO = activityMaterialDAO;
	}
	
	@Override
	public boolean deleteActivityMaterials(int activityID) {
		return activityMaterialDAO.deleteActivityMaterials(activityID);
	}

	@Override
	public List<ActivityMaterial> getActivityMaterials(int activityID) {
		List<Map<String, String>> activityPublicationsDataList = activityMaterialDAO
				.getActivityMaterials(activityID);
		List<ActivityMaterial> activityMaterials = new ArrayList<>();

		for (Map<String, String> ao : activityPublicationsDataList) {
			ActivityMaterial activityMaterial = new ActivityMaterial();
			activityMaterial.setId(Integer.parseInt(ao.get("id")));
			activityMaterial.setType(ao.get("mtype"));
			activityMaterial.setOtherDescription(ao.get("other_description"));
			activityMaterial.setBriefDescription(ao.get("brief_description"));
			activityMaterial.setTargetAudience(ao.get("target_audience"));
			activityMaterial.setWebLink(ao.get("web_link"));
			activityMaterials.add(activityMaterial);
		}
		return activityMaterials;
	}

	@Override
	public int saveActivityMaterials(List<ActivityMaterial> materials,
			int activityID) {
		int saved = 1;
		for (ActivityMaterial material : materials) {
			Map<String, String> actObjData = new HashMap<>();
			if (material.getId() == -1) {
				actObjData.put("id", null);
			} else {
				actObjData.put("id", String.valueOf(material.getId()));
			}
			actObjData.put("mtype", material.getType());
			actObjData.put("other_description", material.getOtherDescription());
			actObjData.put("brief_description", material.getBriefDescription());
			actObjData.put("target_audience", material.getTargetAudience());
			actObjData.put("web_link", material.getWebLink());

			int id = activityMaterialDAO.saveActivityMaterials(actObjData,
					activityID);
			saved *= id;
		}
		return saved;
	}

}
