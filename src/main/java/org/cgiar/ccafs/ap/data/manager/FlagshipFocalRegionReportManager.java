/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.FlagshipFocalRegionReportManagerImpl;
import org.cgiar.ccafs.ap.data.model.FSFLRReportComment;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegionReport;

import com.google.inject.ImplementedBy;

@ImplementedBy(FlagshipFocalRegionReportManagerImpl.class)
public interface FlagshipFocalRegionReportManager {
	
	/**
	 * Get flagship report.
	 * 
	 * @param flagshipId flagship id
	 * @param year - year
	 * @param param period (mid/end)
	 * 
	 * @return flagship / focal regions report
	 */
	FlagshipFocalRegionReport getReport(int flagshipId, int year, String period);
	
	/**
	 * Get flagship report.
	 * 
	 * @param reportId report id
	 * 
	 * @return flagship / focal regions report
	 */
	FlagshipFocalRegionReport getReport(int reportId);
	
	/**
	 * Save flagship focal region report.
	 * 
	 * @param flagshipFocalRegionReport report instance
	 * 
	 * @return report id
	 */
	int saveReport(FlagshipFocalRegionReport flagshipFocalRegionReport);
	
	/**
	 * Update the state of the report.
	 * 
	 * @param reportId report id
	 * @param newState new state
	 * 
	 * @return updated or not boolean value
	 */
	boolean updateState(int reportId, String newState);
	
	/**
	 * Update the revision of the report.
	 * 
	 * @param reportId report id
	 * 
	 * @return updated or not boolean value
	 */
	boolean updateRevision(int reportId);
	
	/**
	 * Get flagship report comment list.
	 * 
	 * @param reportId report id
	 * 
	 * @return list of comments
	 */
	List<FSFLRReportComment> getComments(int reportId);
	
	/**
	 * Get flagship report comment list.
	 * 
	 * @param reportId report id
	 * @param revision revision
	 * 
	 * @return list of comments
	 */
	List<FSFLRReportComment> getComments(int reportId, int revision);
	
	/**
	 * Save comment.
	 * 
	 * @param reportId report id
	 * @param comment
	 * 
	 * @return comment id
	 */
	int saveComment(int reportId, FSFLRReportComment comment);
}
