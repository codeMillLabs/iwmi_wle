/*
 * FILENAME
 *     ReviewerDAO.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLReviewerDAO;
import org.cgiar.ccafs.ap.data.model.Reviewer;

import com.google.inject.ImplementedBy;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
/**
 * 
 * <p>
 * ReviewerDAO.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 *
 */
@ImplementedBy(MySQLReviewerDAO.class)
public interface ReviewerDAO
{

    public List<Reviewer> getReviewersByCluster(String clusterName);

    public Reviewer getReviewersById(long id);

    public List<Reviewer> getAllReviwers();

    public void save(Reviewer reviwer);

    public void update(Reviewer reviwer);

}
