/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

/**
 * @author Manuja
 */
public class ActivityPublication {

	private int id;
	private String publicationReference;
	private String peerReviewed;
	private String isiJournal;
	private String webLink;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the publicationReference
	 */
	public String getPublicationReference() {
		return publicationReference;
	}
	
	/**
	 * @param publicationReference the publicationReference to set
	 */
	public void setPublicationReference(String publicationReference) {
		this.publicationReference = publicationReference;
	}
	
	/**
	 * @return the peerReviewed
	 */
	public String getPeerReviewed() {
		return peerReviewed;
	}

	/**
	 * @param peerReviewed the peerReviewed to set
	 */
	public void setPeerReviewed(String peerReviewed) {
		this.peerReviewed = peerReviewed;
	}

	/**
	 * @return the isiJournal
	 */
	public String getIsiJournal() {
		return isiJournal;
	}

	/**
	 * @param isiJournal the isiJournal to set
	 */
	public void setIsiJournal(String isiJournal) {
		this.isiJournal = isiJournal;
	}

	/**
	 * @return the webLink
	 */
	public String getWebLink() {
		return webLink;
	}
	
	/**
	 * @param webLink the webLink to set
	 */
	public void setWebLink(String webLink) {
		this.webLink = webLink;
	}
}
