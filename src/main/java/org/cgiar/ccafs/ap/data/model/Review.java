package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.Date;

public class Review implements Serializable, Comparable<Review> {

	private static final long serialVersionUID = 8892146097902005183L;

	public enum Type {
		INTERNAL, EXTERNAL;
	}

	private int id;
	private String revisionId;
	private long reviewerId;
	private String reviewerName;
	private String comment;
	private Type type;
	private Date date;
	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the revisionId
	 */
	public String getRevisionId() {
		return revisionId;
	}

	/**
	 * @param revisionId
	 *            the revisionId to set
	 */
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}

	/**
	 * @return the reviewerName
	 */
	public String getReviewerName() {
		return reviewerName;
	}

	/**
	 * @param reviewerName
	 *            the reviewerName to set
	 */
	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the reviewerId
	 */
	public long getReviewerId() {
		return reviewerId;
	}

	/**
	 * @param reviewerId
	 *            the reviewerId to set
	 */
	public void setReviewerId(long reviewerId) {
		this.reviewerId = reviewerId;
	}

	@Override
	public int compareTo(Review o) {

		return o.getDate().compareTo(this.getDate());
	}

}
