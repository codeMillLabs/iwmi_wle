/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityPublicationDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityPublicationManager;
import org.cgiar.ccafs.ap.data.model.ActivityPublication;

import com.google.inject.Inject;

public class ActivityPublicationManagerImpl implements ActivityPublicationManager{
	
	private ActivityPublicationDAO activityPublicationDao;

	@Inject
	public ActivityPublicationManagerImpl(
			ActivityPublicationDAO activityPublicationDao) {
		this.activityPublicationDao = activityPublicationDao;
	}
	
	@Override
	public boolean deleteActivityPublications(int activityId) {
		return activityPublicationDao.deleteActivityPublications(activityId);
	}

	@Override
	public List<ActivityPublication> getActivityPublications(int activityID) {
		
		List<Map<String, String>> activityPublicationsDataList = activityPublicationDao
				.getActivityPublications(activityID);
		List<ActivityPublication> activityPublications = new ArrayList<>();

		for (Map<String, String> ao : activityPublicationsDataList) {
			ActivityPublication activityObjective = new ActivityPublication();
			activityObjective.setId(Integer.parseInt(ao.get("id")));
			activityObjective.setPublicationReference(ao.get("publication_reference"));
			activityObjective.setIsiJournal(ao.get("isi_journal"));
			activityObjective.setPeerReviewed(ao.get("peer_reviewed"));
			activityObjective.setWebLink(ao.get("web_link"));
			activityPublications.add(activityObjective);
		}
		return activityPublications;
	}

	@Override
	public int saveActivityPublications(List<ActivityPublication> publications,
			int activityID) {
		int saved = 1;
		for (ActivityPublication publication : publications) {
			Map<String, String> actObjData = new HashMap<>();
			if (publication.getId() == -1) {
				actObjData.put("id", null);
			} else {
				actObjData.put("id", String.valueOf(publication.getId()));
			}
			actObjData.put("publication_reference", publication.getPublicationReference());
			actObjData.put("isi_journal", publication.getIsiJournal());
			actObjData.put("peer_reviewed", publication.getPeerReviewed());
			actObjData.put("web_link", publication.getWebLink());

			int id = activityPublicationDao.saveActivityPublications(actObjData,
					activityID);
			saved *= id;
		}
		return saved;
	}
}