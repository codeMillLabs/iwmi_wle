/*
 * FILENAME
 *     MySQLActivityReviewerDAO.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * ActivityReviewer Data access
 * </p>
 *
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
public class MySQLActivityReviewerDAO implements ActivityReviewerDAO
{
    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(MySQLActivityReviewerDAO.class);
    private DAOManager databaseManager;

    @Inject
    public MySQLActivityReviewerDAO(DAOManager databaseManager)
    {
        this.databaseManager = databaseManager;
    }

    private static final String REVIEWERS_INSERT_SQL =
        "INSERT INTO iwmi_activity_reviewes "
            + " (activity_id, activity_revision, reviewer_id, reviewer_name, reviewer_type, satisfied, round_completed) VALUES (?, ?, ?, ?, ?, ?, ?)";

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO#saveActivityReviewers(java.util.List)
     */
    @Override
    public void saveActivityReviewers(int activityId, String revisionId, List<ActivityReview> reviewers)
    {
        try (Connection con = databaseManager.getConnection())
        {
            for (ActivityReview reviewer : reviewers)
            {
                Object[] values =
                    new Object[] {
                        activityId,
                        revisionId,
                        reviewer.getReviewerId(),
                        reviewer.getReviewerName(),
                        reviewer.getType(),
                        reviewer.isSatisfied() ? "1" : "0",
                        reviewer.isRoundCompleted() ? "1" : "0"
                    };

                int reviewId = databaseManager.makeChangeSecure(con, REVIEWERS_INSERT_SQL, values);

                if (reviewId < 0)
                {
                    LOG.error("There was a problem saving the reviewer data.");
                }
            }

            LOG.debug("<< saveActivityReviewers(): saved.");
        }
        catch (Exception e)
        {
            LOG.error("-- saveActivityReviewers() > There was an error saving the activity reviewer's data.");
            LOG.error("Error: ", e);
        }

    }

    private static final String REVIEWERS_DELETE_SQL =
        "DELETE FROM iwmi_activity_reviewes  WHERE activity_id = ? and activity_revision = ? and reviewer_type = ? ";

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO#updateActivityReviewers(java.util.List)
     */
    @Override
    public void addActivityReviewers(int activityId, String revisionId, String type, List<ActivityReview> reviewers)
    {

//        try (Connection con = databaseManager.getConnection())
//        {
//            databaseManager.makeChangeSecure(con, REVIEWERS_DELETE_SQL, new Object[] {
//                activityId, revisionId, type
//            });
//
//            LOG.debug("<< updateActivityReviewers(): activity reviewers removed.");
//        }
//        catch (Exception e)
//        {
//            LOG.error("-- saveActivityReviewers() > There was an error saving the activity reviewer's data.");
//            LOG.error("Error: ", e);
//        }
        saveActivityReviewers(activityId, revisionId, reviewers);
    }

    @Override
    public void updateActivityReviewerStatus(int activityId, String revisionId, String type, User user,
        ActivityReview reviewer)
    {
        String REVIEW_UPDATE =
            "UPDATE iwmi_activity_reviewes set satisfied = ?, round_completed = ? WHERE activity_id = ? "
                + " AND activity_revision = ? AND reviewer_type = ? AND reviewer_name = ? ";

        try (Connection con = databaseManager.getConnection())
        {
            LOG.debug("<< updateActivityReviewerStatus() Parameters : Id :{}, Flags : {}", activityId,
                reviewer.isSatisfied() + ":" + reviewer.isRoundCompleted());
            int reviewId =
                databaseManager.makeChangeSecure(con, REVIEW_UPDATE, new Object[] {
                    (reviewer.isSatisfied() ? "1" : "0"),
                    (reviewer.isRoundCompleted() ? "1" : "0"),
                    activityId,
                    revisionId,
                    type,
                    user.getName()
                });

            if (reviewId < 0)
            {
                LOG.error("There was a problem updating the budget data.");
            }
            else
            {
                LOG.debug("<< updateActivityReviewerStatus(): activity reviewers updated. ");
            }
        }
        catch (Exception e)
        {
            LOG.error("-- updateActivityReviewerStatus() > There was an error saving the activity reviewer's data.");
            LOG.error("Error: ", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO#getReviewers(int, java.lang.String, java.lang.String)
     */
    @Override
    public List<ActivityReview> getReviewers(int activityId, String revisionId, String type)
    {
        LOG.debug(">> getReviewers(), Activity Id= {}, Revision Id= {}, Type = {} ", new Object[] {
            activityId, revisionId, type
        });

        String REVIWERS_BY_ACTIVITY_ID_SQL =
            "SELECT id, activity_id, activity_revision, reviewer_id, reviewer_name, satisfied "
                + " FROM iwmi_activity_reviewes WHERE activity_id = " + activityId + " ";
        if (revisionId != null && !revisionId.isEmpty())
        {
            REVIWERS_BY_ACTIVITY_ID_SQL += "  AND activity_revision = '" + revisionId + "' ";
        }

        if (type != null && !type.isEmpty())
        {
            REVIWERS_BY_ACTIVITY_ID_SQL += "  AND  reviewer_type = '" + type + "' ";
        }

        LOG.debug(">> getReviewers() --> QUERY : {}", REVIWERS_BY_ACTIVITY_ID_SQL);
        List<ActivityReview> reviewers = new ArrayList<ActivityReview>();

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_BY_ACTIVITY_ID_SQL, con);
            while (rs.next())
            {
                ActivityReview reviewer = new ActivityReview();
                reviewer.setId(rs.getLong("id"));
                reviewer.setActivityId(rs.getInt("activity_id"));
                reviewer.setActivityRevision(rs.getString("activity_revision"));
                reviewer.setReviewerId(rs.getInt("reviewer_id"));
                reviewer.setReviewerName(rs.getString("reviewer_name"));
                reviewer.setSatisfied(rs.getString("satisfied").equals("1") ? true : false);
                reviewer.setType(type);
                reviewers.add(reviewer);
            }
            rs.close();
        }
        catch (SQLException e)
        {
            LOG.error("-- getReviewers() > There was an exception trying to load the reviewer By Id.", e);
        }
        LOG.debug("<< getReviewers(): reviewer size :{}", reviewers.size());
        return reviewers;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO#hasAnyPendingReviews(int, java.lang.String)
     */
    @Override
    public List<ActivityReview> hasAnyPendingReviews(int activityId, String revisionId, String type)
    {
        LOG.debug(">> getReviewers()");
        final String REVIWERS_BY_ACTIVITY_ID_SQL =
            "SELECT id, activity_id, activity_revision, reviewer_id, reviewer_name, reviewer_type, satisfied, round_completed "
                + " FROM iwmi_activity_reviewes  WHERE activity_id = " + activityId + "  "
                + " AND activity_revision = '" + revisionId + "' " + " AND reviewer_type = '" + type + "' "
                + " AND round_completed = '0' ";

        LOG.debug(">> getReviewers() -> Query : {}", REVIWERS_BY_ACTIVITY_ID_SQL);
        List<ActivityReview> reviewers = new ArrayList<ActivityReview>();

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_BY_ACTIVITY_ID_SQL, con);
            while (rs.next())
            {
                ActivityReview reviewer = new ActivityReview();
                reviewer.setId(rs.getLong("id"));
                reviewer.setActivityId(rs.getInt("activity_id"));
                reviewer.setActivityRevision(rs.getString("activity_revision"));
                reviewer.setReviewerId(rs.getInt("reviewer_id"));
                reviewer.setReviewerName(rs.getString("reviewer_name"));
                reviewer.setSatisfied(rs.getString("satisfied").equals("1") ? true : false);
                reviewer.setRoundCompleted(rs.getString("round_completed").equals("1") ? true : false);
                reviewer.setType(type);
                reviewers.add(reviewer);
            }
            rs.close();
        }
        catch (Exception e)
        {
            LOG.error("-- getReviewers() > There was an exception trying to load the reviewer By Id.", e);
        }
        LOG.debug("<< getReviewers(): reviewer size {}", reviewers.size());
        LOG.debug(">> getReviewers()");
        return reviewers;
    }

    @Override
    public List<ActivityReview> hasPendingReviewsForUser(int activityId, String revisionId, String type, User user)
    {

        LOG.debug(">> hasPendingReviewsForUser()");
        final String REVIWERS_BY_ACTIVITY_ID_SQL =
            "SELECT id, activity_id, activity_revision, reviewer_id, reviewer_name, satisfied, round_completed "
                + " FROM iwmi_activity_reviewes  WHERE activity_id = " + activityId + "  "
                + " AND activity_revision = '" + revisionId + "' " + " AND reviewer_type = '" + type + "' "
                + " AND satisfied = '0' AND reviewer_name = '" + user.getName() + "'";

        LOG.debug(">> hasPendingReviewsForUser() -> Query : {}", REVIWERS_BY_ACTIVITY_ID_SQL);
        List<ActivityReview> reviewers = new ArrayList<ActivityReview>();

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_BY_ACTIVITY_ID_SQL, con);
            while (rs.next())
            {
                ActivityReview reviewer = new ActivityReview();
                reviewer.setId(rs.getLong("id"));
                reviewer.setActivityId(rs.getInt("activity_id"));
                reviewer.setActivityRevision(rs.getString("activity_revision"));
                reviewer.setReviewerId(rs.getInt("reviewer_id"));
                reviewer.setReviewerName(rs.getString("reviewer_name"));
                reviewer.setSatisfied(rs.getString("satisfied").equals("1") ? true : false);
                reviewer.setRoundCompleted(rs.getString("round_completed").equals("1") ? true : false);
                reviewer.setType(type);
                reviewers.add(reviewer);
            }
            rs.close();
        }
        catch (Exception e)
        {
            LOG.error("-- hasPendingReviewsForUser() > There was an exception trying to load the reviewer By Id.", e);
        }
        LOG.debug("<< hasPendingReviewsForUser(): reviewer size {}", reviewers.size());
        LOG.debug(">> hasPendingReviewsForUser()");
        return reviewers;
    }

}
