package org.cgiar.ccafs.ap.data.model;

/**
 * <p>
 * Get the Difference of object attributes
 * </p>
 * 
 * @author Amila Silva
 *
 */
public interface Difference<T> {
	
	/**
	 * Find the differences on given two objects and return the detail report.
	 * 
	 * @param t
	 * @return
	 */
	DiffReport diff(T t);

}
