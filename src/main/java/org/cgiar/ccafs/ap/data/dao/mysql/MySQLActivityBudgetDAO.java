/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityBudgetDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
public class MySQLActivityBudgetDAO implements ActivityBudgetDAO
{

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(MySQLActivityBudgetDAO.class);
    private DAOManager databaseManager;

    @Inject
    public MySQLActivityBudgetDAO(DAOManager databaseManager)
    {
        this.databaseManager = databaseManager;
    }

    @Override
    public ActivityBudget getBudget(int activityID)
    {
        LOG.debug(">> getBudget(activityID={})", activityID);

        final String FETCH_ACTIVITY_BUDGET_SQL =
            "SELECT id, activity_id, budget_data_json" + " FROM  activity_budgets WHERE activity_id = '" + activityID
                + "' ";

        ActivityBudget budget = new ActivityBudget();
        // Querying budget record.
        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(FETCH_ACTIVITY_BUDGET_SQL, con);
            if (rs.next())
            {
                budget.setId(rs.getInt("id"));
                budget.setActivityId(rs.getInt("activity_id"));
                String budgetJSON = rs.getString("budget_data_json");
                ActivityBudget savedBudget = ActivityBudget.getBudget(budgetJSON);
                budget.setMainBudgets(savedBudget.getMainBudgets());
                budget.setRegBudgets(savedBudget.getRegBudgets());
                budget.setDonorsBudget(savedBudget.getDonorsBudget());
            }
            rs.close();

        }
        catch (Exception e)
        {
            LOG.error("-- getBudget() > There was an error getting budget for activity {}", activityID, e);
            return null;
        }

        LOG.debug("<< getBudget():{}", budget);
        return budget;
    }

    private static final String MAIN_BUDGET_INSERT_SQL = "INSERT INTO activity_budgets "
        + " (activity_id, budget_data_json) " + " VALUES (?, ?)";

    @Override
    public int saveActivityBudget(ActivityBudget budget)
    {
        LOG.debug(">> saveBudget( budgetData={})", budget);

        try (Connection con = databaseManager.getConnection())
        {
            String budgetDataJson = ActivityBudget.getJSON(budget);
            Object[] values = new Object[] {
                budget.getActivityId(), budgetDataJson
            };

            int budgetId = databaseManager.makeChangeSecure(con, MAIN_BUDGET_INSERT_SQL, values);

            if (budgetId < 0)
            {
                LOG.error("There was a problem saving the budget data.");
            }

            LOG.debug("<< saveBudget(): Budget Id : {},", budgetId);
            return budgetId;
        }
        catch (Exception e)
        {
            LOG.error("-- saveBudget() > There was an error saving the budget data.");
            LOG.error("Error: ", e);
        }
        return -1;
    }

    private static final String MAIN_BUDGET_UPDATE_SQL = "UPDATE activity_budgets SET budget_data_json = ? "
        + " WHERE activity_id = ? ";

    @Override
    public void updateActivityBudget(ActivityBudget budget)
    {
        LOG.debug(">> updateActivityBudget( budgetData={})", budget);

        try (Connection con = databaseManager.getConnection())
        {
            String budgetDataJson = ActivityBudget.getJSON(budget);
            Object[] values = new Object[] {
                budgetDataJson, budget.getActivityId()
            };

            int budgetId = databaseManager.makeChangeSecure(con, MAIN_BUDGET_UPDATE_SQL, values);

            if (budgetId < 0)
            {
                LOG.error("There was a problem updating the budget data.");
            }

            LOG.debug("<< updateActivityBudget(): Budget Id : {} ", budget);
        }
        catch (Exception e)
        {
            LOG.error("-- updateActivityBudget() > There was an error updating the budget data.");
            LOG.error("Error: ", e);
        }
    }

    @Override
    public List<ActivityBudget> getActivityBudgetsByCriteria(int year, int activityId, String leadCenter, String cluster, String flagship)
    {
        LOG.debug(">> getActivityBudgetsByCriteria( year={}, activity id={},  leadCenter={}, cluster={}, flagship={} )", new Object[] {
            year, activityId, leadCenter, cluster, flagship
        });

        List<ActivityBudget> budgetList = new ArrayList<ActivityBudget>();

        try (Connection con = databaseManager.getConnection())
        {
            StringBuffer budgetSQL = new StringBuffer();
            budgetSQL.append("SELECT * FROM activity_budgets b INNER JOIN activities a ON (b.activity_id = a.id) ");
            budgetSQL.append(" WHERE a.is_deleted = 0 ");

            if (activityId > 0)
            {
                budgetSQL.append(" AND a.id = " + activityId + " ");
            }
            else
            {
                budgetSQL.append(" AND a.year = " + year + " ");
                budgetSQL.append(" AND a.lead_center LIKE "
                        + ((leadCenter != null && !leadCenter.isEmpty()) ? " '" +  leadCenter + "' " : " '%' "));
                budgetSQL.append("  AND a.flagship LIKE  "
                    + ((flagship != null && !flagship.isEmpty()) ? " '" + flagship + "' " : " '%' "));
                budgetSQL.append(" AND a.activity_cluster LIKE "
                    + ((cluster != null && !cluster.isEmpty()) ? " '" +  cluster + "' " : " '%' "));
            }
            budgetSQL.append(" ORDER BY a.id ");
            LOG.debug(">> getActivityBudgetsByCriteria, SQL ={}", budgetSQL.toString());

            ResultSet results = databaseManager.makeQuery(budgetSQL.toString(), con);

            while (results.next())
            {
                ActivityBudget budget = new ActivityBudget();
                budget.setId(results.getInt("id"));
                budget.setActivityId(results.getInt("activity_id"));
                String budgetJSON = results.getString("budget_data_json");

                Activity activity = new Activity();
                activity.setTitle(results.getString("title"));
                activity.setYear(results.getInt("year"));
                activity.setFlagShipSRP(results.getString("flagship"));
                activity.setActivityCluster(results.getString("activity_cluster"));
                activity.setLeadCenter(results.getString("lead_center"));

                ActivityBudget savedBudget = ActivityBudget.getBudget(budgetJSON);
                budget.setMainBudgets(savedBudget.getMainBudgets());
                budget.setRegBudgets(savedBudget.getRegBudgets());
                budget.setDonorsBudget(savedBudget.getDonorsBudget());
                budget.setActivity(activity);

                budgetList.add(budget);
            }
            results.close();

            if (budgetList.size() < 0)
            {
                LOG.error("No Activity Budgets found for given criteria");
            }

            LOG.debug("<< getActivityBudgetsByCriteria(): Budget List: {} ", budgetList.size());
        }
        catch (Exception e)
        {
            LOG.error("-- getActivityBudgetsByCriteria() > There was an error getActivityBudgetsByCriteria");
            LOG.error("Error: ", e);
        }
        return budgetList;
    }

}
