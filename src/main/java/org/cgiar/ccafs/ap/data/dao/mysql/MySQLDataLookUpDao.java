/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.DataLookUpDao;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * <p>
 * MySQL Implementation of @ DataLookUpDao}
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public class MySQLDataLookUpDao implements DataLookUpDao {

	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLDataLookUpDao.class);
	private DAOManager databaseManager;

	@Inject
	public MySQLDataLookUpDao(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public int saveLookupData(DataLookup dataLookup) {
		throw new RuntimeException("Not yet implemented");
	}

	@Override
	public List<DataLookup> getAllLookUpdata(LookupInfoEnum lookupInfo) {
		LOG.debug(">> getAllLookUpdata(lookupInfo={})", lookupInfo);
		List<DataLookup> dataLookups = new ArrayList<DataLookup>();

		final String GET_ALL_LOOK_UP_SQL = " SELECT m.id, m.display_name, m.value FROM "
				+ lookupInfo.getTableName()
				+ " m"
				+ " ORDER BY  m.display_name ";

		try (Connection con = databaseManager.getConnection()) {

			ResultSet results = databaseManager.makeQuery(GET_ALL_LOOK_UP_SQL,
					con);
			while (results.next()) {

				DataLookup mainLookup = new DataLookup();
				mainLookup.setId(results.getInt("m.id"));
				mainLookup.setLookupInfoEnum(lookupInfo);
				mainLookup.setDisplayName(results.getString("m.display_name"));
				mainLookup.setValue(results.getString("m.value"));
				dataLookups.add(mainLookup);
			}
			results.close();

			if (lookupInfo.hasSubLevel() && !dataLookups.isEmpty()) {

				for (DataLookup lookup : dataLookups) {

					final String GET_ALL_SUB_LEVELS_SQL = " SELECT s.id, s.display_name, s.value FROM "
							+ lookupInfo.getSubLevel().getTableName()
							+ " s "
							+ " WHERE s.parent_id = "
							+ lookup.getId()
							+ " "
							+ " ORDER BY  s.display_name ";
					results = databaseManager.makeQuery(GET_ALL_SUB_LEVELS_SQL,
							con);

					while (results.next()) {

						DataLookup subLookup = new DataLookup();
						subLookup.setId(results.getInt("s.id"));
						subLookup.setLookupInfoEnum(lookupInfo);
						subLookup.setDisplayName(results
								.getString("s.display_name"));
						subLookup.setValue(results.getString("s.value"));
						lookup.getSubLevel().add(subLookup);
					}
				}
			}

			results.close();

		} catch (SQLException e) {
			LOG.error(
					"-- getAllLookUpdata() > There was an error getting Lookup data",
					e);
			return null;
		}
		LOG.debug(">> getAllLookUpdata >  Data size :{}", dataLookups.size());
		return dataLookups;
	}

	@Override
	public DataLookup getLookUpdataById(LookupInfoEnum lookupInfo, Long id) {
		LOG.debug(">> getLookUpdataById(lookupInfo={})", lookupInfo);

		DataLookup mainLookup = null;

		final String GET_ALL_LOOK_UP_SQL = " SELECT m.id, m.display_name, m.value FROM "
				+ lookupInfo.getTableName()
				+ " m "
				+ " WHERE m.id = "
				+ id
				+ " ORDER BY  m.display_name ";

		try (Connection con = databaseManager.getConnection()) {

			ResultSet results = databaseManager.makeQuery(GET_ALL_LOOK_UP_SQL,
					con);
			while (results.next()) {

				mainLookup = new DataLookup();
				mainLookup.setId(results.getInt("m.id"));
				mainLookup.setLookupInfoEnum(lookupInfo);
				mainLookup.setDisplayName(results.getString("m.display_name"));
				mainLookup.setValue(results.getString("m.value"));
			}
			results.close();

			if (lookupInfo.hasSubLevel() && (mainLookup != null)) {

				final String GET_ALL_SUB_LEVELS_SQL = " SELECT s.id, s.display_name, s.value FROM "
						+ lookupInfo.getSubLevel().getTableName()
						+ " s "
						+ " WHERE s.parent_id = "
						+ mainLookup.getId()
						+ " "
						+ " ORDER BY  s.display_name ";
				results = databaseManager
						.makeQuery(GET_ALL_SUB_LEVELS_SQL, con);

				while (results.next()) {

					DataLookup subLookup = new DataLookup();
					subLookup.setId(results.getInt("s.id"));
					subLookup.setLookupInfoEnum(lookupInfo);
					subLookup.setDisplayName(results
							.getString("s.display_name"));
					subLookup.setValue(results.getString("s.value"));
					mainLookup.getSubLevel().add(subLookup);
				}
			}
			results.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getLookUpdataById() > There was an error getting Lookup data",
					e);
			return null;
		}
		LOG.debug(">> getLookUpdataById >  Lookup Data :{}", mainLookup);
		return mainLookup;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.cgiar.ccafs.ap.data.dao.DataLookUpDao#getLookUpdataByValue(org.cgiar.ccafs.ap.data.model.LookupInfoEnum,
	 *      java.lang.String)
	 */
	@Override
	public DataLookup getLookUpdataByValue(LookupInfoEnum lookupInfo,
			String value, boolean withSublevels) {
		LOG.debug(">> getLookUpdataByValue(lookupInfo={})", lookupInfo);

		DataLookup mainLookup = null;

		final String GET_ALL_LOOK_UP_SQL = " SELECT m.id, m.display_name, m.value FROM "
				+ lookupInfo.getTableName()
				+ " m "
				+ " WHERE m.value = '"
				+ value + "' ORDER BY  m.display_name ";

		try (Connection con = databaseManager.getConnection()) {

			ResultSet results = databaseManager.makeQuery(GET_ALL_LOOK_UP_SQL,
					con);
			while (results.next()) {

				mainLookup = new DataLookup();
				mainLookup.setId(results.getInt("m.id"));
				mainLookup.setLookupInfoEnum(lookupInfo);
				mainLookup.setDisplayName(results.getString("m.display_name"));
				mainLookup.setValue(results.getString("m.value"));
			}
			results.close();

			if (lookupInfo.hasSubLevel() && (mainLookup != null)
					&& withSublevels) {

				final String GET_ALL_SUB_LEVELS_SQL = " SELECT s.id, s.display_name, s.value FROM "
						+ lookupInfo.getSubLevel().getTableName()
						+ " s "
						+ " WHERE s.parent_id = "
						+ mainLookup.getId()
						+ " "
						+ " ORDER BY  s.display_name ";
				results = databaseManager
						.makeQuery(GET_ALL_SUB_LEVELS_SQL, con);

				while (results.next()) {

					DataLookup subLookup = new DataLookup();
					subLookup.setId(results.getInt("s.id"));
					subLookup.setLookupInfoEnum(lookupInfo);
					subLookup.setDisplayName(results
							.getString("s.display_name"));
					subLookup.setValue(results.getString("s.value"));
					mainLookup.getSubLevel().add(subLookup);
				}
			}
			results.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getLookUpdataByValue() > There was an error getting Lookup data",
					e);
			return null;
		}
		LOG.debug(">> getLookUpdataByValue >  Lookup Data :{}", mainLookup);
		return mainLookup;
	}

	public List<String> flagshipLeaderEmail(String flagshipName) {
		LOG.debug(">> flagshipLeaderEmail (flagshipName={})", flagshipName);

		List<String> emails = new ArrayList<String>();

		final String GET_FLAGSHIP_LEADER_EMAIL_SQL = " SELECT f.leader_email, f.leader_b_email FROM iwmi_flagship_srp f  WHERE f.value = '"
				+ flagshipName + "' ";

		try (Connection con = databaseManager.getConnection()) {

			ResultSet results = databaseManager.makeQuery(
					GET_FLAGSHIP_LEADER_EMAIL_SQL, con);
			while (results.next()) {
				emails.add(results.getString("f.leader_email"));
				emails.add(results.getString("f.leader_b_email"));
			}
			results.close();
		} catch (SQLException e) {
			LOG.error(
					"-- flagshipLeaderEmail() > There was an error getting Lookup data",
					e);
			return emails;
		}
		LOG.debug(">> flagshipLeaderEmail >  Flagship :{},  leader email :{}",
				flagshipName, emails);
		return emails;
	}

	@Override
	public String getFlagshipByUser(String email) {
		LOG.debug(">> getFlagshipByUser (flagshipName={})", email);

		final String GET_FLAGSHIP_LEADER_EMAIL_SQL = " SELECT f.value FROM iwmi_flagship_srp f  WHERE f.leader_email = '"
				+ email + "' or f.leader_b_email = '" + email + "' ";

		String flagshipValue = null;
		try (Connection con = databaseManager.getConnection()) {

			ResultSet results = databaseManager.makeQuery(
					GET_FLAGSHIP_LEADER_EMAIL_SQL, con);
			while (results.next()) {
				flagshipValue = results.getString("f.value");
			}
			results.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipByUser() > There was an error getting Lookup data",
					e);
			return null;
		}
		LOG.debug(">> getFlagshipByUser >  Flagship :{},  value email :{}",
				flagshipValue, email);
		return flagshipValue;
	}

}
