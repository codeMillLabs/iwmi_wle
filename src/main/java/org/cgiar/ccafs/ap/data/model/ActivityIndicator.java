/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

/**
 * @author Manuja
 */
public class ActivityIndicator {

	private int id;
	private DataLookup itype;
	private DataLookup subType;
	private String target;
	private String actual;
	private String contribution;
	private String comment;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the itype
	 */
	public DataLookup getItype() {
		return itype;
	}

	/**
	 * @param itype
	 *            the itype to set
	 */
	public void setItype(DataLookup itype) {
		this.itype = itype;
	}

	/**
	 * @return the subType
	 */
	public DataLookup getSubType() {
		return subType;
	}

	/**
	 * @param inSubType
	 *            the subType to set
	 */
	public void setSubType(final DataLookup inSubType) {
		this.subType = inSubType;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the actual
	 */
	public String getActual() {
		return actual;
	}

	/**
	 * @param actual
	 *            the actual to set
	 */
	public void setActual(String actual) {
		this.actual = actual;
	}

	/**
	 * @return the contribution
	 */
	public String getContribution() {
		return contribution;
	}

	/**
	 * @param contribution
	 *            the contribution to set
	 */
	public void setContribution(String contribution) {
		this.contribution = contribution;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}