/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.UserManagerImp;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;

import com.google.inject.ImplementedBy;

@ImplementedBy(UserManagerImp.class)
public interface UserManager
{

    /**
     * Get the user identified by the specified email parameter.
     * 
     * @param email
     *            of the user.
     * @return User object representing the user identified by the email provided or Null in the user doesn't exist in
     *         the database.
     */
    public User getUser(String email);

    /**
     * Authenticate a user.
     * 
     * @param email
     *            of the user.
     * @param password
     *            of the user.
     * @return a User object representing the user identified by the email provided or Null if login failed.
     */
    public User login(String email, String password);

    /**
     * Save in the database the date and time that the user made its last login.
     * 
     * @param user
     *            - User information
     * @return - True if the information was succesfully saved, false otherwise.
     */
    public boolean saveLastLogin(User user);

    /**
     * Create a new user in the system by saving the user data in the database.
     * 
     * @param user
     *            - The user information
     * @return true if it was successfully saved. False otherwise.
     */
    public boolean saveUser(User user);

    /**
     * <p>
     * Get users by roles
     * </p>
     *
     * @param role
     * @return list of users
     *
     */
    public List<User> getUsersByRole(UserRole role);
    
    /**
     * <p>
     * Get user by id
     * </p>
     *
     * @param role
     * @return list of users
     *
     */
    public User getById(int id);
    
    /**
     * <p>
     * Get users by ids
     * </p>
     *
     * @param role
     * @return list of users
     *
     */
    public List<User> getUsersById(List<Integer> ids);
}
