/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.DataLookupManagerImpl;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;

import com.google.inject.ImplementedBy;

/**
 * <p>
 * Data lookup manager.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
@ImplementedBy(DataLookupManagerImpl.class)
public interface DataLookUpManager {

	/**
	 * <p>
	 * Save Lookup data using Dao.
	 * </p>
	 * 
	 * @param lookupData
	 *            data to be saved
	 * @return true if the data was successfully saved, false otherwise.
	 */
	public int saveLookupData(DataLookup dataLookup);

	/**
	 * <p>
	 * Fetch all lookup data related to given lookup information.
	 * </p>
	 * 
	 * @param lookupInfo
	 *            lookup meta information
	 * @return list of lookup data
	 */
	public List<DataLookup> getAllLookUpdata(LookupInfoEnum lookupInfo);

	/**
	 * <p>
	 * Get Lookup data by given id.
	 * </p>
	 * 
	 * @param lookupInfo
	 *            lookup meta information
	 * @param id
	 *            id of the lookup data
	 * @return matching lookup data
	 */
	public DataLookup getLookUpdataById(LookupInfoEnum lookupInfo, Long id);

	/**
	 * <p>
	 * Get Lookup data by given value.
	 * </p>
	 * 
	 * @param lookupInfo
	 *            lookup meta information
	 * @param id
	 *            id of the lookup data
	 * @return matching lookup data
	 */
	public DataLookup getLookUpdataByValue(LookupInfoEnum lookupInfo,
			String value, boolean withSublevels);

	/**
	 * <p>
	 * Get Flagship leader email
	 * </p>
	 * 
	 * @param flagshipName
	 * @return
	 */
	public List<String> flagshipLeaderEmail(String flagshipName);

	/**
	 * <p>
	 * Get Flagship leader email
	 * </p>
	 * 
	 * @param flagshipName
	 * @return
	 */
	public String getFlagshipByUser(String email);

}
