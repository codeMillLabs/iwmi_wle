/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLKeywordDAO;

import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;

@ImplementedBy(MySQLKeywordDAO.class)
public interface KeywordDAO {

	/**
	 * Get the keyword information of the given keyword identifier.
	 * 
	 * @param id
	 *            - identifier
	 * @return
	 */
	public Map<String, String> getKeywordInformation(String id);

	/**
	 * Get the list with all the keywords from the DAO
	 * 
	 * @return a list of maps with the data
	 */
	public List<Map<String, String>> getKeywordList();

	/**
	 * save the keywords from the DAO
	 * 
	 */
	public int saveKeyword(Map<String, String> keyValues);

	/**
	 * update keywords
	 * 
	 * @param keyValues
	 */
	public void updateKeyword(Map<String, String> keyValues);
}
