package org.cgiar.ccafs.ap.data.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ClusterOutputDetail 
{
	private int id;
	private String code;
	private String description;
	private String comment;
	private String managerComment;
	private String directorComment;
	
	private Map<String, ActivityClusterOutput> activityMap = new TreeMap<>();
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the activityMap
	 */
	public Map<String, ActivityClusterOutput> getActivityMap() {
		return activityMap;
	}
	
	/**
	 * @param activityMap the activityMap to set
	 */
	public void setActivityMap(Map<String, ActivityClusterOutput> activityMap) {
		this.activityMap = activityMap;
	}
	
	/**
	 * @param activityMap the activityMap to set
	 */
	public void addActivityCluster(ActivityClusterOutput activityClusterOutput) {
		this.activityMap.put(activityClusterOutput.getActivityId(), activityClusterOutput);
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the managerComment
	 */
	public String getManagerComment() {
		return managerComment;
	}

	/**
	 * @param managerComment the managerComment to set
	 */
	public void setManagerComment(String managerComment) {
		this.managerComment = managerComment;
	}

	/**
	 * @return the directorComment
	 */
	public String getDirectorComment() {
		return directorComment;
	}

	/**
	 * @param directorComment the directorComment to set
	 */
	public void setDirectorComment(String directorComment) {
		this.directorComment = directorComment;
	}
}