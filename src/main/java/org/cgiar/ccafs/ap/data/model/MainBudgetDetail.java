/*
 * FILENAME
 *     MainBudgetDetail.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Main Budget details.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainBudgetDetail implements Serializable,
		Difference<MainBudgetDetail> {
	private static final long serialVersionUID = 7789926975675989480L;
	public static final String BUDGET_TYPE = "Budget";
	public static final String GENDER_PRECNT_TYPE = "Gender %";
	public static final String GENDER_BUDGET_TYPE = "Gender Budget";

	private String type = "";
	private double w1w2;
	private double w3;
	private double bilateral;
	private double other;

	@Override
	public DiffReport diff(MainBudgetDetail t) {
		DiffReport report = new DiffReport();

		if (w1w2 != t.w1w2) {
			report.append(type + ", W1/W2 :", "" + w1w2, "" + t.w1w2);
		}
		if (w3 != t.w3) {
			report.append(type + ", W3 :", "" + w3, "" + t.w3);
		}
		if (bilateral != t.bilateral) {
			report.append(type + ", Bilateral :", "" + bilateral, ""
					+ t.bilateral);
		}
		if (other != t.other) {
			report.append(type + ", Other :", "" + other, "" + t.other);
		}

		return report;
	}

	/**
	 * <p>
	 * Get Total value
	 * </p>
	 *
	 * @return total value
	 *
	 */
	@JsonIgnore
	public double getTotal() {
		if (this.type.equals(GENDER_PRECNT_TYPE)) {
			return -1;
		} else {
			return w1w2 + w3 + bilateral + other;
		}
	}

	/**
	 * <p>
	 * Getter for type.
	 * </p>
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * <p>
	 * Setting value for type.
	 * </p>
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * <p>
	 * Getter for w1w2.
	 * </p>
	 * 
	 * @return the w1w2
	 */
	public double getW1w2() {
		return Double.isNaN(w1w2) ? 0.0 : w1w2;
	}

	/**
	 * <p>
	 * Setting value for w1w2.
	 * </p>
	 * 
	 * @param w1w2
	 *            the w1w2 to set
	 */
	public void setW1w2(double w1w2) {
		this.w1w2 = w1w2;
	}

	/**
	 * <p>
	 * Getter for w3.
	 * </p>
	 * 
	 * @return the w3
	 */
	public double getW3() {
		return Double.isNaN(w3) ? 0.0 : w3;
	}

	/**
	 * <p>
	 * Setting value for w3.
	 * </p>
	 * 
	 * @param w3
	 *            the w3 to set
	 */
	public void setW3(double w3) {
		this.w3 = w3;
	}

	/**
	 * <p>
	 * Getter for bilateral.
	 * </p>
	 * 
	 * @return the bilateral
	 */
	public double getBilateral() {
		return Double.isNaN(bilateral) ? 0.0 : bilateral;
	}

	/**
	 * <p>
	 * Setting value for bilateral.
	 * </p>
	 * 
	 * @param bilateral
	 *            the bilateral to set
	 */
	public void setBilateral(double bilateral) {
		this.bilateral = bilateral;
	}

	/**
	 * <p>
	 * Getter for other.
	 * </p>
	 * 
	 * @return the other
	 */
	public double getOther() {
		return Double.isNaN(other) ? 0.0 : other;
	}

	/**
	 * <p>
	 * Setting value for other.
	 * </p>
	 * 
	 * @param other
	 *            the other to set
	 */
	public void setOther(double other) {
		this.other = other;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
