/**
 * 
 */
package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.ActivityWLEManagerImpl;
import org.cgiar.ccafs.ap.data.model.ActivityWLEDevelopment;

import com.google.inject.ImplementedBy;

/**
 * @author Amila Silva
 *
 */
@ImplementedBy(ActivityWLEManagerImpl.class)
public interface ActivityWLEManager {

	/**
	 * Get the wles related to the activity given
	 * 
	 * @param activityID
	 *            - the activity identifier
	 * @return a list o wle objects with the information
	 */
	public List<ActivityWLEDevelopment> getWLEList(int activityID);

	/**
	 * Delete all the wles related to the activity given from the database.
	 * 
	 * @param activityID
	 *            - Activity identifier
	 * @return true if the records were successfully removed. False otherwise.
	 */
	public boolean removeActivityWLEs(int activityID);

	/**
	 * Save a list of activity wles into the database
	 * 
	 * @param wles
	 *            - The information to be saved
	 * @param activityID
	 *            - activity identifier
	 */
	public boolean saveWLEList(List<ActivityWLEDevelopment> wles, int activityID);
}
