/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import static org.cgiar.ccafs.ap.data.model.ActivityReview.EXTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.INTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.populateObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityDAO;
import org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityBenchmarkSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityCountryManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.manager.ActivityOtherSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityPartnerManager;
import org.cgiar.ccafs.ap.data.manager.ContactPersonManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.DeliverableManager;
import org.cgiar.ccafs.ap.data.manager.LeaderManager;
import org.cgiar.ccafs.ap.data.manager.MilestoneManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRatingComment;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.ContactPerson;
import org.cgiar.ccafs.ap.data.model.Leader;
import org.cgiar.ccafs.ap.data.model.Milestone;
import org.cgiar.ccafs.ap.data.model.Objective;
import org.cgiar.ccafs.ap.data.model.Output;
import org.cgiar.ccafs.ap.data.model.Review;
import org.cgiar.ccafs.ap.data.model.SectionReview;
import org.cgiar.ccafs.ap.data.model.SectionReview.Category;
import org.cgiar.ccafs.ap.data.model.Status;
import org.cgiar.ccafs.ap.data.model.Theme;
import org.cgiar.ccafs.ap.data.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class ActivityManagerImpl implements ActivityManager {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityManagerImpl.class);
	private ActivityDAO activityDAO;

	// Managers
	private DeliverableManager deliverableManager;
	private ActivityPartnerManager activityPartnerManager;
	private ContactPersonManager contactPersonManager;
	private MilestoneManager milestoneManager;
	private ActivityObjectiveManager activityObjectiveManager;
	private LeaderManager leaderManager;
	private ActivityCountryManager activityCountryManager;
	private ActivityBenchmarkSiteManager activityBenchmarkSiteManager;
	private ActivityOtherSiteManager activityOtherSiteManager;
	private ActivityReviewerDAO activityReviewerDAO;
	private DataLookUpManager dataLookupManager;

	@Inject
	public ActivityManagerImpl(ActivityDAO activityDAO,
			DeliverableManager deliverableManager,
			ActivityPartnerManager activityPartnerManager,
			ContactPersonManager contactPersonManager,
			MilestoneManager milestoneManager,
			ActivityObjectiveManager activityObjectiveManager,
			LeaderManager leaderManager,
			ActivityCountryManager activityCountryManager,
			ActivityBenchmarkSiteManager activityBenchmarkSiteManager,
			ActivityOtherSiteManager activityOtherSiteManager,
			ActivityReviewerDAO activityReviewerDAO) {
		this.activityDAO = activityDAO;
		this.deliverableManager = deliverableManager;
		this.activityPartnerManager = activityPartnerManager;
		this.contactPersonManager = contactPersonManager;
		this.milestoneManager = milestoneManager;
		this.activityObjectiveManager = activityObjectiveManager;
		this.leaderManager = leaderManager;
		this.activityCountryManager = activityCountryManager;
		this.activityBenchmarkSiteManager = activityBenchmarkSiteManager;
		this.activityOtherSiteManager = activityOtherSiteManager;
		this.activityReviewerDAO = activityReviewerDAO;
	}

	@Override
	public Activity[] getActivities(int year, User user, String createdBy, String flagshipValue, 
			ActivityStatus... activityStatus) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		List<Map<String, String>> activitiesDO;

		// if leader is null the user must be an admin.
		if (user.isAdmin()) {
			activitiesDO = activityDAO.getActivities(year);
		} else {
			// get all activities added by the specified leader.
			activitiesDO = activityDAO.getActivities(year, user.getLeader()
					.getId(), createdBy, flagshipValue, activityStatus);
		}
		Activity[] activities = new Activity[activitiesDO.size()];
		for (int c = 0; c < activitiesDO.size(); c++) {
			Activity activity = new Activity();
			/* --- MAIN INFORMATION --- */
			activity.setId(Integer.parseInt(activitiesDO.get(c).get("id")));
			activity.setActivityId(activitiesDO.get(c).get("activity_id"));
			activity.setRevisionId(activitiesDO.get(c).get("revision_id"));
			activity.setActivityStatus(ActivityStatus.getValue(activitiesDO
					.get(c).get("status")));
			activity.setTitle(activitiesDO.get(c).get("title"));
			activity.setYear(Integer.parseInt(activitiesDO.get(c).get("year")));
			activity.setDescription(activitiesDO.get(c).get("description"));

			activity.setProjectName(activitiesDO.get(c).get("project_name"));
			activity.setGenderResearchDesc(activitiesDO.get(c).get(
					"gender_research"));
			activity.setEcoSystemsDesc(activitiesDO.get(c)
					.get("echo_sys_desc"));
			activity.setFlagShipSRP(activitiesDO.get(c).get("flagship"));
			activity.setActivityCluster(activitiesDO.get(c).get(
					"activity_cluster"));
			activity.setLeadCenter(activitiesDO.get(c).get("lead_center"));
			activity.setCreatedUser(activitiesDO.get(c).get("created_by"));

			try {
				if (activitiesDO.get(c).get("start_date") != null) {
					activity.setStartDate(dateFormat.parse(activitiesDO.get(c)
							.get("start_date")));
				}
			} catch (ParseException e) {
				String msg = "There was an error parsing start date '"
						+ activitiesDO.get(c).get("start_date")
						+ "' for the activity " + activity.getId() + ".";
				LOG.error(msg, e);
			}
			try {
				if (activitiesDO.get(c).get("end_date") != null) {
					activity.setEndDate(dateFormat.parse(activitiesDO.get(c)
							.get("end_date")));
				}
			} catch (ParseException e) {
				String msg = "There was an error parsing end date '"
						+ activitiesDO.get(c).get("end_date")
						+ "' for the activity " + activity.getId() + ".";
				LOG.error(msg, e);
			}
			// Contact Persons
			activity.setContactPersons(contactPersonManager
					.getContactPersons(activity.getId()));
			// Is validated
			activity.setValidated(activitiesDO.get(c).get("is_validated")
					.equals("1"));
			
           if(activitiesDO.get(c).get("mid_yr_rating_json") != null) {
				try {
					ActivityReviewRating rating = populateObject(activitiesDO.get(c).get(
									"mid_yr_rating_json"));
					rating.setActivityId(activity.getActivityId());
					activity.setMidYearReviews(rating);
				} catch (Exception e) {
					LOG.error("Error in populating activity review rating", e);
				}
           }

			Leader activityLeader = new Leader();
			activityLeader.setId(Integer.parseInt(activitiesDO.get(c).get(
					"leader_id")));
			activityLeader.setAcronym(activitiesDO.get(c)
					.get("leader_acronym"));
			activityLeader.setName(activitiesDO.get(c).get("leader_name"));
			activity.setLeader(activityLeader);

			/* --- ACTIVITY STATUS --- */
			Map<String, String> statusInfo = activityDAO
					.getActivityStatusInfo(activity.getId());

			if (statusInfo.get("status_id") != null) {
				Status status = new Status();
				status.setId(Integer.parseInt(statusInfo.get("status_id")));
				status.setName(statusInfo.get("status_name"));
				activity.setStatus(status);
			}
			
			// Status Description
			activity.setStatusDescription(statusInfo.get("status_description"));
			// Gender Integration
			activity.setGenderIntegrationsDescription(statusInfo
					.get("gender_description"));

			activities[c] = activity;
		}
		return activities;
	}

	@Override
	public Activity[] getActivitiesForDetailedSummary(int year, int activityID,
			int activityLeader) {
		Activity[] activities;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// Getting the list of activities according on the parameters received
		// and filling them with basic information (id, title).

		if (activityID != 0) {
			// If the activity identifier is defined just is needed one activity
			activities = new Activity[1];
			Map<String, String> activityData = activityDAO
					.getActivityStatusInfo(activityID);

			Activity activity = new Activity();
			activity.setId(activityID);
			activity.setActivityId(activityData.get("activity_id"));
			activity.setTitle(activityData.get("title"));
			activity.setRevisionId(activityData.get("revision_id"));
			activity.setActivityStatus(ActivityStatus.getValue(activityData
					.get("status")));
			activity.setYear(Integer.parseInt(activityData.get("year")));

			try {
				activity.setStartDate(dateFormat.parse(activityData
						.get("start_date")));
				activity.setEndDate(dateFormat.parse(activityData
						.get("end_date")));
			} catch (ParseException e) {
				String msg = "-- getActivitiesForDetailedSummary() > There was a problem "
						+ "parsing the start date ({}) and end date ({}) for activity {}";
				LOG.error(msg, new Object[] { activityData.get("start_date"),
						activityData.get("start_date"), activityID });
			}

			Status status = new Status();
			status.setId(Integer.parseInt(activityData.get("status_id")));
			status.setName(activityData.get("status_name"));
			activity.setStatus(status);

			Milestone milestone = new Milestone();
			milestone.setId(Integer.parseInt(activityData.get("milestone_id")));
			milestone.setCode(activityData.get("milestone_code"));
			activity.setMilestone(milestone);

			activity.setProjectName(activityData.get("project_name"));
			activity.setGenderResearchDesc(activityData.get("gender_research"));
			activity.setEcoSystemsDesc(activityData.get("echo_sys_desc"));
			activity.setFlagShipSRP(activityData.get("flagship"));
			activity.setActivityCluster(activityData.get("activity_cluster"));
			activity.setLeadCenter(activityData.get("lead_center"));
			activity.setCreatedUser(activityData.get("created_by"));

			activities[0] = activity;

		} else {
			// If the activity identifier was not specified, search for
			// activities that fills the parameters received.

			User user = new User();
			user.setLeader(new Leader(activityLeader));
			List<Map<String, String>> activityList = activityDAO.getActivities(
					year, activityLeader, null, null);
			activities = new Activity[activityList.size()];

			for (int c = 0; c < activityList.size(); c++) {
				Activity activity = new Activity();
				activity.setId(Integer.parseInt(activityList.get(c).get("id")));
				activity.setActivityId(activityList.get(c).get("activity_id"));
				activity.setRevisionId(activityList.get(c).get("revision_id"));
				activity.setActivityStatus(ActivityStatus.getValue(activityList
						.get(c).get("status")));
				activity.setTitle(activityList.get(c).get("title"));
				activity.setYear(Integer.parseInt(activityList.get(c).get(
						"year")));

				try {
					activity.setStartDate(dateFormat.parse(activityList.get(c)
							.get("start_date")));
					activity.setEndDate(dateFormat.parse(activityList.get(c)
							.get("end_date")));
				} catch (ParseException e) {
					String msg = "-- getActivitiesForDetailedSummary() > There was a problem "
							+ "parsing the start date ({}) and end date ({}) for activity {}";
					LOG.error(msg,
							new Object[] {
									activityList.get(c).get("start_date"),
									activityList.get(c).get("start_date"),
									activityID });
				}

				Status status = new Status();
				status.setId(Integer.parseInt(activityList.get(c).get(
						"status_id")));
				status.setName(activityList.get(c).get("status_name"));
				activity.setStatus(status);

				Milestone milestone = new Milestone();
				milestone.setId(Integer.parseInt(activityList.get(c).get(
						"milestone_id")));
				milestone.setCode(activityList.get(c).get("milestone_code"));
				activity.setMilestone(milestone);

				activities[c].setProjectName(activityList.get(c).get(
						"project_name"));
				activities[c].setGenderResearchDesc(activityList.get(c).get(
						"gender_research"));
				activities[c].setEcoSystemsDesc(activityList.get(c).get(
						"echo_sys_desc"));
				activities[c].setFlagShipSRP(activityList.get(c)
						.get("flagship"));
				activities[c].setActivityCluster(activityList.get(c).get(
						"activity_cluster"));
				activities[c].setLeadCenter(activityList.get(c).get(
						"lead_center"));
				activities[c].setCreatedUser(activityList.get(c).get(
						"created_by"));

				activities[c] = activity;
			}
		}

		// After have the list of activities, we will fill all other needed
		// information.

		for (Activity activity : activities) {
			// Contact persons
			activity.setContactPersons(contactPersonManager
					.getContactPersons(activity.getId()));

			// Leader
			activity.setLeader(leaderManager.getActivityLeader(activity.getId()));

			// Country Locations
			activity.setCountries(activityCountryManager
					.getActvitiyCountries(activity.getId()));

			// CCAFS Locations
			activity.setBsLocations(activityBenchmarkSiteManager
					.getActivityBenchmarkSites(activity.getId()));

			// Other locations
			activity.setOtherLocations(activityOtherSiteManager
					.getActivityOtherSites(activity.getId()));

			// Partners
			activity.setActivityPartners(activityPartnerManager
					.getActivityPartners(activity.getId()));

			// Deliverables
			activity.setDeliverables(deliverableManager
					.getDeliverables(activity.getId()));
		}

		return activities;
	}

	@Override
	public Activity[] getActivitiesForStatusSummary(int year, int activityID,
			int activityLeader) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Activity[] getActivitiesForXML(int year, int limit) {
		List<Map<String, String>> activitiesDB = activityDAO
				.getActivitiesForRSS(year, limit);
		if (activitiesDB.size() > 0) {
			Activity[] activities = new Activity[activitiesDB.size()];
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
			int c = 0;
			for (Map<String, String> activityDB : activitiesDB) {
				Activity activity = new Activity();
				activity.setId(Integer.parseInt(activityDB.get("id")));
				activity.setActivityId(activityDB.get("activity_id"));
				activity.setRevisionId(activityDB.get("revision_id"));
				activity.setActivityStatus(ActivityStatus.getValue(activityDB
						.get("status")));
				activity.setTitle(activityDB.get("title"));
				activity.setYear(Integer.parseInt(activityDB.get("year")));

				if (activityDB.get("is_global") != null) {
					activity.setGlobal(Integer.parseInt(activityDB
							.get("is_global")) == 1);
				} else {
					activity.setGlobal(false);
				}

				if (activityDB.get("is_commissioned") != null) {
					activity.setCommissioned(Integer.parseInt(activityDB
							.get("is_commissioned")) == 1);
				} else {
					activity.setCommissioned(false);
				}

				try {
					if (activityDB.get("start_date") != null) {
						activity.setStartDate(dateFormat.parse(activityDB
								.get("start_date")));
					}
				} catch (ParseException e) {
					String msg = "There was an error parsing start date '"
							+ activityDB.get("start_date")
							+ "' for the activity " + activity.getId() + ".";
					LOG.error(msg, e);
				}
				try {
					if (activityDB.get("end_date") != null) {
						activity.setEndDate(dateFormat.parse(activityDB
								.get("end_date")));
					}
				} catch (ParseException e) {
					String msg = "There was an error parsing end date '"
							+ activityDB.get("end_date")
							+ "' for the activity " + activity.getId() + ".";
					LOG.error(msg, e);
				}
				activity.setDescription(activityDB.get("description"));
				try {
					if (activityDB.get("date_added") != null) {
						activity.setDateAdded(dateFormat.parse(activityDB
								.get("date_added")));
					}
				} catch (ParseException e) {
					String msg = "There was an error parsing date_added '"
							+ activityDB.get("date_added")
							+ "' for the activity " + activity.getId() + ".";
					LOG.error(msg, e);
				}

				activity.setProjectName(activityDB.get("project_name"));
				activity.setGenderResearchDesc(activityDB
						.get("gender_research"));
				activity.setEcoSystemsDesc(activityDB.get("echo_sys_desc"));
				activity.setFlagShipSRP(activityDB.get("flagship"));
				activity.setActivityCluster(activityDB.get("activity_cluster"));
				activity.setLeadCenter(activityDB.get("lead_center"));
				activity.setCreatedUser(activityDB.get("created_by"));

				Milestone milestone = new Milestone();
				milestone
						.setId(Integer.parseInt(activityDB.get("milestone_id")));
				milestone.setCode(activityDB.get("milestone_code"));

				activity.setMilestone(milestone);

				activities[c] = activity;
				c++;
			}
			return activities;
		}
		return null;
	}

	@Override
	public Activity[] getActivitiesTitle(int year) {
		List<Map<String, String>> activityData = activityDAO.getTitles(year);
		if (activityData.size() > 0) {
			Activity[] activities = new Activity[activityData.size()];
			for (int c = 0; c < activities.length; c++) {
				activities[c] = new Activity();
				activities[c].setId(Integer.parseInt(activityData.get(c).get(
						"id")));
				activities[c].setId(Integer.parseInt(activityData.get(c).get(
						"activity_id")));
				activities[c].setTitle(activityData.get(c).get("title"));
				activities[c].setCreatedUser(activityData.get(c).get(
						"created_by"));
			}
			return activities;
		}
		return null;
	}

	@Override
	public Activity[] getActivitiesTitle(int year, Leader leader) {
		List<Map<String, String>> activityData = activityDAO.getTitles(year,
				leader.getId());
		if (activityData.size() > 0) {
			Activity[] activities = new Activity[activityData.size()];
			for (int c = 0; c < activities.length; c++) {
				activities[c] = new Activity();
				activities[c].setId(Integer.parseInt(activityData.get(c).get(
						"id")));
				activities[c].setActivityId(activityData.get(c).get(
						"activity_id"));
				activities[c].setTitle(activityData.get(c).get("title"));
				activities[c].setCreatedUser(activityData.get(c).get(
						"created_by"));
			}
			return activities;
		}
		return null;
	}

	@Override
	public Activity[] getActivitiesToContinue(int year) {
		List<Map<String, String>> activitiesDataList = activityDAO
				.getActivitiesToContinue(year);
		Activity[] activities = new Activity[activitiesDataList.size()];

		for (int c = 0; c < activitiesDataList.size(); c++) {
			Activity activity = new Activity();
			activity.setId(Integer
					.parseInt(activitiesDataList.get(c).get("id")));
			activity.setActivityId(activitiesDataList.get(c).get("activity_id"));
			activity.setTitle(activitiesDataList.get(c).get("title"));
			activity.setCreatedUser(activitiesDataList.get(c).get("created_by"));

			activities[c] = activity;
		}
		return activities;
	}

	@Override
	public Activity[] getActivitiesToContinue(int year, int leader) {
		List<Map<String, String>> activitiesDataList = activityDAO
				.getActivitiesToContinue(year, leader);
		Activity[] activities = new Activity[activitiesDataList.size()];

		for (int c = 0; c < activitiesDataList.size(); c++) {
			Activity activity = new Activity();
			activity.setId(Integer
					.parseInt(activitiesDataList.get(c).get("id")));
			activity.setActivityId(activitiesDataList.get(c).get("activity_id"));
			activity.setTitle(activitiesDataList.get(c).get("title"));
			activity.setCreatedUser(activitiesDataList.get(c).get("created_by"));

			activities[c] = activity;
		}
		return activities;
	}

	@Override
	public Activity getActivity(int id) {
		// Activity DATA
		Map<String, String> activityData = activityDAO
				.getActivityStatusInfo(id);

		Activity activity = new Activity();
		activity.setId(id);
		return setActivityInformation(activityData, activity);
	}

	@Override
	public Activity getActivity(String id) {
		// Activity DATA
		Map<String, String> activityData = activityDAO
				.getActivityStatusInfo(id);

		Activity activity = new Activity();
		activity.setId(Integer.parseInt(activityData.get("id")));
		return setActivityInformation(activityData, activity);
	}
	
	private Activity setActivityInformation(Map<String, String> activityData,
			Activity activity) {
		// Leader
		Leader leader = new Leader();
		leader.setId(Integer.parseInt(activityData.get("leader_id")));
		leader.setAcronym(activityData.get("leader_acronym"));
		leader.setName(activityData.get("leader_name"));
		activity.setLeader(leader);
		// Main information
		activity.setActivityId(activityData.get("activity_id"));
		activity.setLeadCenterBasedId(Integer.parseInt(activityData
				.get("lead_center_based_id")));
		activity.setTitle(activityData.get("title"));
		activity.setRevisionId(activityData.get("revision_id"));
		activity.setActivityStatus(ActivityStatus.getValue(activityData
				.get("status")));
		activity.setYear(Integer.parseInt(activityData.get("year")));
		activity.setCreatedUser(activityData.get("created_by"));
		activity.setDescription(activityData.get("description"));
		activity.setHasPartners(activityData.get("has_partners").equals("1"));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		if (activityData.get("start_date") == null) {
			activity.setStartDate(null);
		} else {
			try {
				activity.setStartDate(dateFormat.parse(activityData
						.get("start_date")));
			} catch (ParseException e) {
				String msg = "There was an error parsing the start date '"
						+ activityData.get("start_date")
						+ "' for the activity " + activity.getId() + ".";
				LOG.error(msg, e);
			}
		}

		if (activityData.get("end_date") == null) {
			activity.setEndDate(null);
		} else {
			try {
				activity.setEndDate(dateFormat.parse(activityData
						.get("end_date")));
			} catch (ParseException e) {
				String msg = "There was an error parsing the end date '"
						+ activityData.get("end_date") + "' for the activity "
						+ activity.getId() + ".";
				LOG.error(msg, e);
			}
		}

		activity.setProjectName(activityData.get("project_name"));
		activity.setGenderResearchDesc(activityData.get("gender_research"));
		activity.setEcoSystemsDesc(activityData.get("echo_sys_desc"));
		activity.setFlagShipSRP(activityData.get("flagship"));
		activity.setActivityCluster(activityData.get("activity_cluster"));
		activity.setLeadCenter(activityData.get("lead_center"));
		activity.setCreatedUser(activityData.get("created_by"));

		// Objectives
		activity.setObjectives(activityObjectiveManager
				.getActivityObjectives(activity.getId()));
		// Contact Persons
		activity.setContactPersons(contactPersonManager
				.getContactPersons(activity.getId()));
		if (activityData.get("milestone_id") != null) {
			Milestone milestone = milestoneManager.getMilestone(Integer
					.parseInt(activityData.get("milestone_id")));
			activity.setMilestone(milestone);
		} else {
			activity.setMilestone(null);
		}
		// Deliverables
		activity.setDeliverables(deliverableManager.getDeliverables(activity
				.getId()));
		// Partners
		activity.setActivityPartners(activityPartnerManager
				.getActivityPartners(activity.getId()));
		// Gender Integrations
		activity.setGenderIntegrationsDescription(activityData
				.get("gender_description"));
		
		// Country Locations
        activity.setCountries(activityCountryManager
                .getActvitiyCountries(activity.getId()));

        // CCAFS Locations
        activity.setBsLocations(activityBenchmarkSiteManager
                .getActivityBenchmarkSites(activity.getId()));

        // Other locations
        activity.setOtherLocations(activityOtherSiteManager
                .getActivityOtherSites(activity.getId()));

		return activity;
	}

	@Override
	public Activity[] getActivityListByYear(int year) {
		List<Map<String, String>> activityData;
		activityData = activityDAO.getActivityListByYear(year);
		Activity[] activities = new Activity[activityData.size()];

		if (activityData.size() > 0) {
			for (int c = 0; c < activities.length; c++) {
				activities[c] = new Activity();
				activities[c].setId(Integer.parseInt(activityData.get(c).get(
						"id")));
				activities[c].setActivityId(activityData.get(c).get(
						"activity_id"));
				activities[c].setTitle(activityData.get(c).get("title"));
				activities[c].setRevisionId(activityData.get(c).get(
						"revision_id"));
				activities[c].setActivityStatus(ActivityStatus
						.getValue(activityData.get(c).get("status")));
				activities[c].setYear(Integer.parseInt(activityData.get(c).get(
						"year")));
				activities[c].setValidated(activityData.get(c)
						.get("is_validated").equals("1"));

				activities[c].setProjectName(activityData.get(c).get(
						"project_name"));
				activities[c].setGenderResearchDesc(activityData.get(c).get(
						"gender_research"));
				activities[c].setEcoSystemsDesc(activityData.get(c).get(
						"echo_sys_desc"));
				activities[c].setFlagShipSRP(activityData.get(c)
						.get("flagship"));
				activities[c].setActivityCluster(activityData.get(c).get(
						"activity_cluster"));
				activities[c].setLeadCenter(activityData.get(c).get(
						"lead_center"));
				activities[c].setCreatedUser(activityData.get(c).get(
						"created_by"));

				// Milestone
				Milestone milestone = new Milestone();
				milestone.setCode(activityData.get(c).get("milestone_code"));
				activities[c].setMilestone(milestone);

				// Leader
				Leader leader = new Leader();
				leader.setId(Integer.parseInt(activityData.get(c).get(
						"leader_id")));
				leader.setAcronym(activityData.get(c).get("leader_acronym"));
				leader.setName(activityData.get(c).get("leader_name"));
				activities[c].setLeader(leader);

				if (activityData.get(c).get("contact_person_names") != null) {
					String[] contactPersonNames = activityData.get(c)
							.get("contact_person_names").split("::");
					String[] contactPersonEmails = activityData.get(c)
							.get("contact_person_emails").split("::");

					List<ContactPerson> contactPersons = new ArrayList<>();
					for (int i = 0; i < contactPersonNames.length; i++) {
						ContactPerson contactPerson = new ContactPerson();
						contactPerson.setName(contactPersonNames[i]);

						// Contact person may not have email
						if (i < contactPersonEmails.length) {
							contactPerson.setEmail(contactPersonEmails[i]);
						}

						contactPersons.add(contactPerson);
					}
					activities[c].setContactPersons(contactPersons);
				}

			}
		}
		return activities;
	}

	@Override
	public Activity getActivityStatusInfo(int id) {
		Map<String, String> activityDB = activityDAO.getActivityStatusInfo(id);
		if (activityDB != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Activity activity = new Activity();
			activity.setId(id);
			activity.setActivityId(activityDB.get("activity_id"));
			activity.setTitle(activityDB.get("title"));
			activity.setRevisionId(activityDB.get("revision_id"));
			activity.setActivityStatus(ActivityStatus.getValue(activityDB
					.get("status")));
			activity.setYear(Integer.parseInt(activityDB.get("year")));

			try {
				if (activityDB.get("start_date") == null) {
					activity.setStartDate(null);
				} else {
					activity.setStartDate(dateFormat.parse(activityDB
							.get("start_date")));
				}
			} catch (ParseException e) {
				String msg = "There was an error parsing start date '"
						+ activityDB.get("start_date") + "' for the activity "
						+ id + ".";
				LOG.error(msg, e);
			}
			try {
				if (activityDB.get("end_date") == null) {
					activity.setEndDate(null);
				} else {
					activity.setEndDate(dateFormat.parse(activityDB
							.get("end_date")));
				}
			} catch (ParseException e) {
				String msg = "There was an error parsing end date '"
						+ activityDB.get("end_date") + "' for the activity "
						+ id + ".";
				LOG.error(msg, e);
			}
			activity.setDescription(activityDB.get("description"));

			if (activityDB.get("is_global") == null) {
				activity.setGlobal(false);
			} else {
				activity.setGlobal(activityDB.get("is_global").equals("1"));
			}

			if (activityDB.get("is_commissioned") == null) {
				activity.setCommissioned(false);
			} else {
				activity.setCommissioned(activityDB.get("is_commissioned")
						.equals("1"));
			}

			if (activityDB.get("has_partners") == null) {
				activity.setHasPartners(false);
			} else {
				activity.setHasPartners(activityDB.get("has_partners").equals(
						"1"));
			}

			if (activityDB.get("continuous_activity_id") != null) {
				Activity activityTemp = new Activity();
				activityTemp.setId(Integer.parseInt(activityDB
						.get("continuous_activity_id")));
				activityTemp.setActivityId(activityDB
						.get("countinuos_activity_activityID"));
				activity.setContinuousActivity(activityTemp);
			}

			// Status
			Status status = new Status();
			if (activityDB.get("status_id") != null) {
				status.setId(Integer.parseInt(activityDB.get("status_id")));
			}

			if (activityDB.get("status_name") != null) {
				status.setName(activityDB.get("status_name"));
			}
			activity.setStatus(status);

			// Status Description
			activity.setStatusDescription(activityDB.get("status_description"));

			activity.setProjectName(activityDB.get("project_name"));
			activity.setGenderResearchDesc(activityDB.get("gender_research"));
			activity.setEcoSystemsDesc(activityDB.get("echo_sys_desc"));
			activity.setFlagShipSRP(activityDB.get("flagship"));
			activity.setActivityCluster(activityDB.get("activity_cluster"));
			activity.setLeadCenter(activityDB.get("lead_center"));
			activity.setCreatedUser(activityDB.get("created_by"));

			// Milestone
			Milestone milestone = new Milestone();
			if (activityDB.get("milestone_id") != null) {
				milestone
						.setId(Integer.parseInt(activityDB.get("milestone_id")));
			}

			if (activityDB.get("milestone_code") != null) {
				milestone.setCode(activityDB.get("milestone_code"));
			}
			activity.setMilestone(milestone);

			// Activity leader
			Leader activityLeader = new Leader();
			if (activityDB.get("leader_id") != null) {
				activityLeader.setId(Integer.parseInt(activityDB
						.get("leader_id")));
			}
			activityLeader.setAcronym(activityDB.get("leader_acronym"));
			activityLeader.setName(activityDB.get("leader_name"));

			activity.setLeader(activityLeader);

			// Gender Integration
			activity.setGenderIntegrationsDescription(activityDB
					.get("gender_description"));

			return activity;
		}
		return null;
	}

	@Override
	public Activity[] getPlanningActivityList(int year, User user,
			ActivityStatus... activityStatus) {
		List<Map<String, String>> activityData;

		int leaderId = user.getLeader().getId();
		if (user.isRPL()) {
			// If the user is a regional leader also should see the activities
			// of centers located in its region.
			int regionId = user.getLeader().getRegion().getId();
			activityData = activityDAO.getPlanningActivityListForRPL(year,
					leaderId, regionId, activityStatus);
		} else if (user.isTL()) {
			// If the user is a theme leader also should see the activities of
			// centers and rpls
			// under its theme
			int themeCode = Integer.parseInt(user.getLeader().getTheme()
					.getCode());
			activityData = activityDAO.getPlanningActivityListForTL(year,
					leaderId, themeCode, activityStatus);
		} else if (user.isPI()) {
			activityData = activityDAO.getPlanningActivityList(year, leaderId,
					activityStatus);
		} else {
			// Get the list of activities corresponding to the role of the user
			activityData = activityDAO.getPlanningActivityList(year, leaderId,
					activityStatus);
		}

		Activity[] activities = new Activity[activityData.size()];
		for (int c = 0; c < activities.length; c++) {
			int id = Integer.parseInt(activityData.get(c).get("id"));
			String revisionId = activityData.get(c).get("revision_id");
			activities[c] = new Activity();
			activities[c].setLoggedInUser(user);
			activities[c].setId(id);
			activities[c].setActivityId(activityData.get(c).get("activity_id"));
			activities[c].setTitle(activityData.get(c).get("title"));
			activities[c].setRevisionId(revisionId);
			activities[c].setActivityStatus(ActivityStatus
					.getValue(activityData.get(c).get("status")));
			activities[c].setYear(Integer.parseInt(activityData.get(c).get(
					"year")));
			activities[c].setProjectName(activityData.get(c)
					.get("project_name"));
			activities[c].setGenderResearchDesc(activityData.get(c).get(
					"gender_research"));
			activities[c].setEcoSystemsDesc(activityData.get(c).get(
					"echo_sys_desc"));
			activities[c].setFlagShipSRP(activityData.get(c).get("flagship"));
			activities[c].setActivityCluster(activityData.get(c).get(
					"activity_cluster"));
			activities[c].setLeadCenter(activityData.get(c).get("lead_center"));
			activities[c].setCreatedUser(activityData.get(c).get("created_by"));

			// Activity validate status
			activities[c].setValidated(activityDAO.isValidatedActivity(id,
					revisionId));

			// Reviews
			List<ActivityReview> intReviews = activityReviewerDAO.getReviewers(id, null, ActivityReview.INTERNAL_TYPE);
			List<ActivityReview> extReviews = activityReviewerDAO.getReviewers(id, null, ActivityReview.EXTERNAL_TYPE);
			
			activities[c].setInternalReviewers(intReviews);
			activities[c].setExternalReviewers(extReviews);

			// Milestone
			Milestone milestone = new Milestone();
			milestone.setCode(activityData.get(c).get("milestone_code"));
			activities[c].setMilestone(milestone);

			// Leader
			Leader leader = new Leader();
			leader.setId(Integer.parseInt(activityData.get(c).get("leader_id")));
			leader.setAcronym(activityData.get(c).get("leader_acronym"));
			leader.setName(activityData.get(c).get("leader_name"));
			activities[c].setLeader(leader);

			if (activityData.get(c).get("contact_person_names") != null) {
				String[] contactPersonNames = activityData.get(c)
						.get("contact_person_names").split("::");
				String[] contactPersonEmails = activityData.get(c)
						.get("contact_person_emails").split("::");

				List<ContactPerson> contactPersons = new ArrayList<>();
				for (int i = 0; i < contactPersonNames.length; i++) {
					ContactPerson contactPerson = new ContactPerson();
					contactPerson.setName(contactPersonNames[i]);

					// Contact person may not have email
					if (i < contactPersonEmails.length) {
						contactPerson.setEmail(contactPersonEmails[i]);
					}

					contactPersons.add(contactPerson);
				}
				activities[c].setContactPersons(contactPersons);
			}

		}
		return activities;
	}

	@Override
	public Activity getSimpleActivity(int id) {
		Map<String, String> activityDB = activityDAO.getSimpleActivity(id);
		if (activityDB != null) {
			Activity activity = new Activity();
			activity.setId(id);
			activity.setActivityId(activityDB.get("activity_id"));
			activity.setTitle(activityDB.get("title"));
			activity.setYear(Integer.parseInt(activityDB.get("year")));
			activity.setCreatedUser(activityDB.get("created_by"));
			activity.setRevisionId(activityDB.get("revision_id"));
			activity.setActivityStatus(ActivityStatus.getValue(activityDB.get("status")));

			Leader activityLeader = new Leader();
			activityLeader.setId(Integer.parseInt(activityDB.get("leader_id")));
			activityLeader.setName(activityDB.get("leader_name"));
			activityLeader.setAcronym(activityDB.get("leader_acronym"));
			activity.setLeader(activityLeader);

			return activity;
		}
		return null;
	}

	@Override
	public List<Integer> getYearList() {
		return activityDAO.getYearList();
	}

	@Override
	public boolean hasPartners(int activityId) {
		return activityDAO.hasPartners(activityId);
	}

	@Override
	public boolean isActiveActivity(int activityID, int year) {
		int activityYear = activityDAO.getActivityYear(activityID);
		return activityYear >= year;
	}

	@Override
	public boolean isValidatedActivity(int activityID, String revisionId) {
		return activityDAO.isValidatedActivity(activityID, revisionId);
	}

	@Override
	public boolean isValidId(int id) {
		return activityDAO.isValidId(id);
	}

	@Override
	public boolean saveActivity(Activity activity) {
		Map<String, Object> activityData = new HashMap<>();
		activityData.put("title", activity.getTitle());
		activityData.put("activity_leader_id", activity.getLeader().getId());
		activityData.put("is_commissioned", activity.isCommissioned());
		activityData.put("year", activity.getYear());
		activityData.put("has_partners", activity.isHasPartners());

		if (activity.getMilestone() != null) {
			activityData.put("milestone_id", activity.getMilestone().getId());
		}
		if (activity.getDescription() != null) {
			activityData.put("description", activity.getDescription());
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (activity.getStartDate() != null) {
			activityData.put("start_date",
					dateFormat.format(activity.getStartDate()));
		}
		if (activity.getEndDate() != null) {
			activityData.put("end_date",
					dateFormat.format(activity.getEndDate()));
		}
		if (activity.getContinuousActivity() != null) {
			activityData.put("continuous_activity_id", activity
					.getContinuousActivity().getId() + "");
		}
		if (activity.getProjectName() != null) {
			activityData.put("project_name", activity.getProjectName());
		}
		if (activity.getGenderResearchDesc() != null) {
			activityData.put("gender_research",
					activity.getGenderResearchDesc());
		}
		if (activity.getEcoSystemsDesc() != null) {
			activityData.put("echo_sys_desc", activity.getEcoSystemsDesc());
		}
		if (activity.getFlagShipSRP() != null) {
			activityData.put("flagship", activity.getFlagShipSRP());
		}
		if (activity.getActivityCluster() != null) {
			activityData.put("activity_cluster", activity.getActivityCluster());
		}

		if (activity.getCreatedUser() != null) {
			activityData.put("created_by", activity.getCreatedUser());
		}

		if (activity.getLeadCenter() != null) {
			activityData.put("lead_center", activity.getLeadCenter());
		}

		int activityID = activityDAO.saveSimpleActivity(activityData);
		if (activityID != -1) {
			// Objectives
			if (activity.getObjectives() != null
					&& activity.getObjectives().size() > 0) {
				activityObjectiveManager.saveActivityObjectives(
						activity.getObjectives(), activityID);
			}

			// Contact Persons
			if (activity.getContactPersons() != null
					&& activity.getContactPersons().size() > 0) {
				contactPersonManager.saveContactPersons(
						activity.getContactPersons(), activityID);
			}

			// Partners
			if (activity.getActivityPartners() != null
					&& activity.getActivityPartners().size() > 0) {
				activityPartnerManager.saveActivityPartners(
						activity.getActivityPartners(), activityID);
			}

			// Deliverables
			if (activity.getDeliverables() != null
					&& activity.getDeliverables().size() > 0) {
				deliverableManager.saveDeliverables(activity.getDeliverables(),
						activityID);
			}

			if (activity.getInternalReviewers() != null
					&& activity.getInternalReviewers().size() > 0) {

				activityReviewerDAO.saveActivityReviewers(activity.getId(),
						activity.getRevisionId(), activity.getInternalReviewers());
			}

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.cgiar.ccafs.ap.data.manager.ActivityManager#saveNewActivity(org.cgiar.ccafs.ap.data.model.Activity)
	 */
	@Override
	public int saveNewActivity(Activity activity) {
		int activityID = -1;
		Map<String, Object> activityData = new HashMap<>();
		activityData.put("title", activity.getTitle());
		activityData.put("activity_id", activity.getActivityId());
		activityData.put("lead_center_based_id",
				activity.getLeadCenterBasedId());
		activityData.put("activity_leader_id", activity.getLeader().getId());
		activityData.put("is_commissioned", activity.isCommissioned());
		activityData.put("year", activity.getYear());
		activityData.put("has_partners", activity.isHasPartners());

		if (activity.getMilestone() != null) {
			activityData.put("milestone_id", activity.getMilestone().getId());
		}
		if (activity.getDescription() != null) {
			activityData.put("description", activity.getDescription());
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (activity.getStartDate() != null) {
			activityData.put("start_date",
					dateFormat.format(activity.getStartDate()));
		}
		if (activity.getEndDate() != null) {
			activityData.put("end_date",
					dateFormat.format(activity.getEndDate()));
		}
		if (activity.getContinuousActivity() != null) {
			activityData.put("continuous_activity_id", activity
					.getContinuousActivity().getId() + "");
		}
		if (activity.getProjectName() != null) {
			activityData.put("project_name", activity.getProjectName());
		}
		if (activity.getGenderResearchDesc() != null) {
			activityData.put("gender_research",
					activity.getGenderResearchDesc());
		}
		if (activity.getEcoSystemsDesc() != null) {
			activityData.put("echo_sys_desc", activity.getEcoSystemsDesc());
		}
		if (activity.getFlagShipSRP() != null) {
			activityData.put("flagship", activity.getFlagShipSRP());
		}
		if (activity.getActivityCluster() != null) {
			activityData.put("activity_cluster", activity.getActivityCluster());
		}

		if (activity.getCreatedUser() != null) {
			activityData.put("created_by", activity.getCreatedUser());
		}

		if (activity.getLeadCenter() != null) {
			activityData.put("lead_center", activity.getLeadCenter());
		}

		activityID = activityDAO.saveSimpleActivity(activityData);
		return activityID;
	}

	@Override
	public boolean saveHasPartners(Activity activity) {
		return activityDAO.saveHasPartners(activity.getId(),
				activity.isHasPartners());
	}

	@Override
	public boolean saveStatus(Activity activity) {
		Map<String, String> activityData = new HashMap<>();
		activityData.put("activity_id", "" + activity.getId());
		activityData.put("activity_status_id", ""
				+ activity.getStatus().getId());
		activityData.put("status_description", activity.getStatusDescription());
		activityData.put("gender_integrations_description",
				activity.getGenderIntegrationsDescription());
		return activityDAO.saveStatus(activityData);
	}

	@Override
	public boolean updateGlobalAttribute(Activity activity) {
		return activityDAO.updateGlobalAttribute(activity.getId(),
				activity.isGlobal());
	}

	@Override
	public boolean deleteActivity(Long activityId) {
		return activityDAO.deleteActivity(activityId);
	}

	@Override
	public boolean updateMainInformation(Activity activity) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Map<String, String> activityData = new HashMap<>();
		activityData.put("id", String.valueOf(activity.getId()));
		activityData.put("title", activity.getTitle());
		activityData.put("year", String.valueOf(activity.getYear()));
		activityData.put("description", activity.getDescription());

		if (activity.getEndDate() != null) {
			activityData.put("end_date", sdf.format(activity.getEndDate()));
		}
		if (activity.getStartDate() != null) {
			activityData.put("start_date", sdf.format(activity.getStartDate()));
		}
		activityData.put("milestone_id",
				String.valueOf(activity.getMilestone().getId()));
		if (activity.getGenderIntegrationsDescription().isEmpty()) {
			activityData.put("genderDescription", null);
		} else {
			activityData.put("genderDescription",
					activity.getGenderIntegrationsDescription());
		}

		if (activity.getCreatedUser() != null) {
			activityData.put("created_by", activity.getCreatedUser());
		}

		if (activity.getProjectName() != null) {
			activityData.put("project_name", activity.getProjectName());
		}
		if (activity.getGenderResearchDesc() != null) {
			activityData.put("gender_research",
					activity.getGenderResearchDesc());
		}
		if (activity.getEcoSystemsDesc() != null) {
			activityData.put("echo_sys_desc", activity.getEcoSystemsDesc());
		}
		if (activity.getFlagShipSRP() != null) {
			activityData.put("flagship", activity.getFlagShipSRP());
		}
		if (activity.getActivityCluster() != null) {
			activityData.put("activity_cluster", activity.getActivityCluster());
		}
		if (activity.getLeadCenter() != null) {
			activityData.put("lead_center", activity.getLeadCenter());
		}

		return activityDAO.updateMainInformation(activityData);
	}

	public boolean updateActivitStatus(int activityId,
			ActivityStatus activityStatus, String revisionId) {
		return activityDAO.updateActivitStatus(activityId, activityStatus,
				revisionId);
	}

	@Override
	public int updateActivityRevisionId(int activityId) {
		synchronized (this) {
			Activity activity = getSimpleActivity(activityId);

			int revisionId = (activity.getRevisionId() != null) ? Integer
					.valueOf(activity.getRevisionId()) + 1 : 0;
			boolean updated = activityDAO.updateActivityRevisionId(activityId,
					String.valueOf(revisionId));
			return updated ? revisionId : -1;
		}
	}

	@Override
	public void addActivtyReviewers(Activity activity) {
		activityReviewerDAO.addActivityReviewers(activity.getId(),
				activity.getRevisionId(), INTERNAL_TYPE, activity.getInternalReviewers());
		
		activityReviewerDAO.addActivityReviewers(activity.getId(),
				activity.getRevisionId(), EXTERNAL_TYPE, activity.getExternalReviewers());
	}

	@Override
	public List<ActivityReview> getReviewers(int activityId, String revisionId, String type) {
		return activityReviewerDAO.getReviewers(activityId, revisionId, type);
	}

	@Override
	public boolean validateActivity(Activity activity) {
		return activityDAO.validateActivity(activity.getId(),
				activity.getRevisionId(), activity.isValidated());
	}

	@Override
	public SectionReview fetchReview(int activityId, String revisionId,
			Category reviewType) {
		return activityDAO.fetchReview(activityId, revisionId, reviewType);
	}

	@Override
	public List<SectionReview> fetchAllReviews(int activityId,
			String revisionId, Category reviewType) {
		return activityDAO.fetchAllReviews(activityId, revisionId, reviewType);
	}

	@Override
	public boolean addReview(int activityId, String revisionId,
			Category reviewType, Review review) {

		SectionReview sectionReview = activityDAO.fetchReview(activityId,
				revisionId, reviewType);
		sectionReview.addReview(review);

		return activityDAO.updateReview(activityId, revisionId, reviewType,
				sectionReview);
	}
	
	@Override
	public boolean updateReview(int activityId, String revisionId,
			Category reviewType, SectionReview sectionReview) {
		
		return activityDAO.updateReview(activityId, revisionId, reviewType,
				sectionReview);
	}

	@Override
	public ActivityReviewRating getMidYearRating(String activityId) {
		return activityDAO.getMidYearRating(activityId);
	}

	@Override
	public boolean saveOrUdatetMidYearRating(String activityId,
			ActivityReviewRating rating) {
		return activityDAO.saveOrUdatetMidYearRating(activityId, rating);
	}

	@Override
	public boolean saveMidYearReviewRatingComment(int reviewRatingId,
			ActivityReviewRatingComment ratingComment) {
		return activityDAO.saveMidYearReviewRatingComment(reviewRatingId, ratingComment);
	}

	@Override
	public List<ActivityReviewRatingComment> getMidYearReviewRatingComment(
			int reviewRatingId) {
		return activityDAO.getMidYearReviewRatingComment(reviewRatingId);
	}

	@Override
	public int getActivityCount(String cluster) {
		return activityDAO.getActivityCount(cluster);
	}

}
