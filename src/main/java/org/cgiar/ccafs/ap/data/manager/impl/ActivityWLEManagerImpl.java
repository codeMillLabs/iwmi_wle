/**
 * 
 */
package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityWLEDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityWLEManager;
import org.cgiar.ccafs.ap.data.model.ActivityWLEDevelopment;

import com.google.inject.Inject;

/**
 * @author Amila Silva
 *
 */
public class ActivityWLEManagerImpl implements ActivityWLEManager {

	private ActivityWLEDAO activityWLEDAO;

	@Inject
	public ActivityWLEManagerImpl(ActivityWLEDAO activityWLEDAO) {
		this.activityWLEDAO = activityWLEDAO;
	}

	@Override
	public List<ActivityWLEDevelopment> getWLEList(int activityID) {
		return activityWLEDAO.getWLEList(activityID);
	}

	@Override
	public boolean removeActivityWLEs(int activityID) {
		return activityWLEDAO.removeActivityWLE(activityID);
	}

	@Override
	public boolean saveWLEList(List<ActivityWLEDevelopment> wles, int activityID) {
		boolean save = false;
		for(ActivityWLEDevelopment wleDev : wles) {
			save &= activityWLEDAO.saveWLE(wleDev, activityID);
		}
		return save;
	}

}
