/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardMainPageFirstSection;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardSecondPage;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageOutput;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageProject;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScorecardMainPageFlagship;
import org.cgiar.ccafs.ap.data.manager.impl.FlagshipFocalRegionManagerImpl;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipReportData;
import org.cgiar.ccafs.ap.data.model.FlagshipRptStatus;

import com.google.inject.ImplementedBy;

@ImplementedBy(FlagshipFocalRegionManagerImpl.class)
public interface FlagshipFocalRegionManager {

	/**
	 * Get the flagship/focal regions
	 * 
	 * @return a list of flagship/focal regions
	 */
	public List<FlagshipFocalRegion> getFlagshipFocalRegions();
	
	/**
	 * Get cluster id list for given flagship id.
	 * 
	 * @param flagshipId flag ship id
	 * 
	 * @return list of cluster ids
	 */
	public List<Integer> getClusterIds(int flagshipId);
	
	/**
	 * Get flagship report data for given cluster code.
	 *
	 * @param year year
	 * @param flagshipId
	 * @param cluster cluster code
	 * 
	 * @return list of flagship report data
	 */
	public List<FlagshipReportData> getFlagshipReportData(int year, int flagshipId, String cluster);
	
	/**
	 * Save flagship rpt status.
	 * 
	 * @param rptStatus
	 * 
	 * @return integer value (id of the saved rpt status)
	 */
	int saveFlagshipRptStatus(FlagshipRptStatus rptStatus);
	
	/**
	 * Check report availability.
	 * 
	 * @param year year
	 * @param flagshipId flagship id
	 * @param period period (mid,end)
	 * 
	 * @return boolean value whether available or not
	 */
	boolean checkReportAvailable(int year, int flagshipId, String period);
	
	/**
	 * Get flagship ids for given activity id.
	 * 
	 * @param activityId activity id
	 * 
	 * @return flagship ids
	 */
	public List<Integer> getFlaghipIdsByActivity(String activityId);
	
	/**
	 * Save score card main page first section.
	 * 
	 * @param scoreCardMainPageFirstSection instance
	 * 
	 * @return saved status
	 */
	int saveMainPageFirstSection(ScoreCardMainPageFirstSection scoreCardMainPageFirstSection);
	
	/**
	 * Get score card main first section.
	 * 
	 * @return score card main first section instance
	 */
	ScoreCardMainPageFirstSection getScoreCardMainPageFirstSection();
	
	
	/**
	 * Save score card main page flagship.
	 * 
	 * @param scorecardMainPageFlagship instance
	 * 
	 * @return saved status
	 */
	int saveScoreCardMainPageFlagship(ScorecardMainPageFlagship scorecardMainPageFlagship);
	
	/**
	 * @return list of score card main page flagship instances.
	 */
	List<ScorecardMainPageFlagship> getScorecardMainPageFlagships();
	
	/**
	 * @return score card main page flagship instance.
	 */
	ScorecardMainPageFlagship getScorecardMainPageFlagships(int id);
	
	/**
	 * @return boolean whether deleted or not
	 */
	public boolean deleteScoreCardCache();
	
	/**
	 * @param scoreCardSecondPage score card second page instance.
	 * 
	 * @return saved id
	 */
	int saveScoreCardSecondPage(ScoreCardSecondPage scoreCardSecondPage);
	
	/**
	 * @param flagshipId flagship id
	 * 
	 * @return list of score card second pages.
	 */
	List<ScoreCardSecondPage> getScoreCardSecondPages(int flagshipId);
	
	/**
	 * @param scoreCardThirdPageOutput score card third page output instance
	 * @return saved id
	 */
	int saveScoreCardThirdPageOutput(ScoreCardThirdPageOutput scoreCardThirdPageOutput);
	
	/**
	 * @param projectId project id
	 * 
	 * @return list of score card third page outputs.
	 */
	List<ScoreCardThirdPageOutput> getScoreCardThirdPageOutputs(String projectId);
	
	
	/**
	 * @param scoreCardThirdPageProject score card third page project instance
	 * @return saved id
	 */
	int saveScoreCardThirdPageProject(ScoreCardThirdPageProject scoreCardThirdPageProject);
	
	/**
	 * @param flagshipId flagship id
	 * 
	 * @return list of score card third page projects.
	 */
	List<ScoreCardThirdPageProject> getScoreCardThirdPageProjects(int flagshipId);
	
	/**
	 * @param projectId project id
	 * 
	 * @return list of score card third page projects.
	 */
	ScoreCardThirdPageProject getScoreCardThirdPageProject(String projectId);
}
