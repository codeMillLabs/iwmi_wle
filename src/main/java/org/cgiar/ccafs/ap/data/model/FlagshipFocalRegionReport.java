/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import java.util.ArrayList;
import java.util.List;

public class FlagshipFocalRegionReport {
	
	public static final String INITIAL_STATE = "initial";
	public static final String PENDING_STATE = "pending";
	public static final String REVIEWED_STATE = "reviewed";
	public static final String APPROVED_STATE = "approved";
	
	
	private int id;
	private int year;
	private int flagshipId;
	private String period;
	private String overallDescription;
	private List<FlagshipCluster> flagshipClusters = new ArrayList<>();
	private String state = INITIAL_STATE;
	private int revision = 0;
	private String managerComment;
	private String directorComment;
	
	private int managerReviewDone;
	private int directorReviewDone;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	
	/**
	 * @return the flagshipId
	 */
	public int getFlagshipId() {
		return flagshipId;
	}
	
	/**
	 * @param flagshipId the flagshipId to set
	 */
	public void setFlagshipId(int flagshipId) {
		this.flagshipId = flagshipId;
	}
	
	/**
	 * @return the period
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(String period) {
		this.period = period;
	}

	/**
	 * @return the overallDescription
	 */
	public String getOverallDescription() {
		return overallDescription;
	}
	
	/**
	 * @param overallDescription the overallDescription to set
	 */
	public void setOverallDescription(String overallDescription) {
		this.overallDescription = overallDescription;
	}
	
	/**
	 * @return the flagshipClusters
	 */
	public List<FlagshipCluster> getFlagshipClusters() {
		return flagshipClusters;
	}
	
	/**
	 * @param flagshipClusters the flagshipClusters to set
	 */
	public void setFlagshipClusters(List<FlagshipCluster> flagshipClusters) {
		this.flagshipClusters = flagshipClusters;
	}
	
	/**
	 * @param flagshipCluster the flagshipCluster to add
	 */
	public void addFlagshipCluster(FlagshipCluster flagshipCluster) {
		this.flagshipClusters.add(flagshipCluster);
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the revision
	 */
	public int getRevision() {
		return revision;
	}

	/**
	 * @param revision the revision to set
	 */
	public void setRevision(int revision) {
		this.revision = revision;
	}

	/**
	 * @return the managerComment
	 */
	public String getManagerComment() {
		return managerComment;
	}

	/**
	 * @param managerComment the managerComment to set
	 */
	public void setManagerComment(String managerComment) {
		this.managerComment = managerComment;
	}

	/**
	 * @return the directorComment
	 */
	public String getDirectorComment() {
		return directorComment;
	}

	/**
	 * @param directorComment the directorComment to set
	 */
	public void setDirectorComment(String directorComment) {
		this.directorComment = directorComment;
	}
	
	public String getStatus()
	{
		if(this.state.equals(INITIAL_STATE))
		{
			return "Report not yet submitted";
		}
		else if (this.state.equals(PENDING_STATE))
		{
			return "Draft report ready for review ";
		}
		else if (this.state.equals(REVIEWED_STATE))
		{
			return "Draft report reviewed ";
		}
		else if (this.state.equals(APPROVED_STATE))
		{
			return "Final report published ";
		}
		
		return "-";
	}

	/**
	 * @return the managerReviewDone
	 */
	public int getManagerReviewDone() {
		return managerReviewDone;
	}

	/**
	 * @param managerReviewDone the managerReviewDone to set
	 */
	public void setManagerReviewDone(int managerReviewDone) {
		this.managerReviewDone = managerReviewDone;
	}

	/**
	 * @return the directorReviewDone
	 */
	public int getDirectorReviewDone() {
		return directorReviewDone;
	}

	/**
	 * @param directorReviewDone the directorReviewDone to set
	 */
	public void setDirectorReviewDone(int directorReviewDone) {
		this.directorReviewDone = directorReviewDone;
	}
}