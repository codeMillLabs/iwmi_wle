/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ActivityPartner implements Difference<ActivityPartner> {

	private int id;
	private String contactName;
	private String contactEmail;
	private Partner partner;
	private PartnerRole role;
	private String clasification;
	private double budget;

	public ActivityPartner() {
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public String getContactName() {
		return contactName;
	}

	public int getId() {
		return id;
	}

	public Partner getPartner() {
		return partner;
	}

	public PartnerRole getRole() {
		return role;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public void setRole(PartnerRole role) {
		this.role = role;
	}

	public String getClasification() {
		return clasification;
	}

	public void setClasification(String clasification) {
		this.clasification = clasification;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	private static String NAME = "Name";
	private static String CONTACT_NAME = "Contact name";
	private static String CONTACT_EMAIL = "Contact email";
	private static String CLASSIFICATION = "Classification";
	private static String BUDGET = "Budget to partner";
	
	
	@Override
	public DiffReport diff(ActivityPartner t) {
		DiffReport report = new DiffReport();
		
         if(this.partner != null && t.partner != null) {
        	 if(this.partner.getName().equals(t.partner.getName())) {
        		 report.append(NAME, this.partner.getName(), t.partner.getName()); 
        	 }
         }
         
         if(isNotEqual(this.contactName, t.contactName)) {
        	 report.append(CONTACT_NAME, this.contactName, t.contactName); 
         }
         
         if(isNotEqual(this.contactEmail, t.contactEmail)) {
        	 report.append(CONTACT_EMAIL, this.contactEmail, t.contactEmail); 
         }
         
         if(isNotEqual(this.clasification, t.clasification)) {
        	 report.append(CLASSIFICATION, this.clasification, t.clasification); 
         }

         if(this.budget != t.budget) {
        	 report.append(BUDGET, "" + this.budget, "" + t.budget); 
         }
		return report;
	}

}
