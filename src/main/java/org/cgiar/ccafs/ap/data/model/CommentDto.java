package org.cgiar.ccafs.ap.data.model;

public class CommentDto 
{
	private String code;
	private String comment;
	private String managerComment;
	private String directorComment;
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the managerComment
	 */
	public String getManagerComment() {
		return managerComment;
	}

	/**
	 * @param managerComment the managerComment to set
	 */
	public void setManagerComment(String managerComment) {
		this.managerComment = managerComment;
	}

	/**
	 * @return the directorComment
	 */
	public String getDirectorComment() {
		return directorComment;
	}

	/**
	 * @param directorComment the directorComment to set
	 */
	public void setDirectorComment(String directorComment) {
		this.directorComment = directorComment;
	}
}