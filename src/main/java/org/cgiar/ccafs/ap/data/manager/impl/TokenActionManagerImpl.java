/**
 * 
 */
package org.cgiar.ccafs.ap.data.manager.impl;

import org.cgiar.ccafs.ap.data.dao.ActivityPartnerDAO;
import org.cgiar.ccafs.ap.data.dao.PartnerDAO;
import org.cgiar.ccafs.ap.data.dao.TokenActionDAO;
import org.cgiar.ccafs.ap.data.manager.TokenActionManager;
import org.cgiar.ccafs.ap.data.model.TokenAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * @author Amila Silva
 *
 */
public class TokenActionManagerImpl implements TokenActionManager {
	
	// Logger
		private static final Logger LOG = LoggerFactory
				.getLogger(TokenActionManagerImpl.class);
		private TokenActionDAO tokenActionDAO;
		
		
	@Inject	
	public TokenActionManagerImpl(TokenActionDAO tokenActionDAO) {
		this.tokenActionDAO = tokenActionDAO;
	}

	@Override
	public int saveToken(TokenAction tokenAction) {
		return tokenActionDAO.saveToken(tokenAction);
	}

	@Override
	public void updateToken(TokenAction tokenAction) {
		tokenActionDAO.updateToken(tokenAction);
	}

	@Override
	public TokenAction getTokenAction(String tokenId) {
		return tokenActionDAO.getTokenAction(tokenId);
	}

}
