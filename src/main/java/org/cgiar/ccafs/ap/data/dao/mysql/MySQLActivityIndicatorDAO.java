/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityIndicatorDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLActivityIndicatorDAO implements ActivityIndicatorDAO {

	// Looging
		private static final Logger LOG = LoggerFactory
				.getLogger(MySQLActivityIndicatorDAO.class);
		DAOManager databaseManager;

		@Inject
		public MySQLActivityIndicatorDAO(DAOManager databaseManager) {
			this.databaseManager = databaseManager;
		}

		@Override
		public boolean deleteActivityIndicators(int activityID) {
			LOG.debug(">> deleteActivityIndicators(activityID={})", activityID);
			boolean deleted = false;
			String query = "DELETE FROM activity_indicators WHERE activity_id = ?";
			Object[] values = new Object[1];
			values[0] = activityID;
			try (Connection con = databaseManager.getConnection()) {
				int rows = databaseManager.makeChangeSecure(con, query, values);
				if (rows < 0) {
					LOG.warn(
							"-- deleteActivityIndicators() > There was a problem deleting the indicators related to the activity {}. \n{}",
							activityID, query);
				} else {
					deleted = true;
				}
			} catch (SQLException e) {
				LOG.error(
						"-- deleteActivityIndicators() > There was an error deleting the indicators related to the activity {}",
						activityID, e);
			}
			LOG.debug("<< deleteActivityIndicators():{}", deleted);
			return deleted;
		}

		@Override
		public List<Map<String, String>> getActivityIndicators(int activityID) {
			LOG.debug(">> getActivityIndicators(activityID={})", activityID);
			List<Map<String, String>> activityIndicatorsDataList = new ArrayList<>();
			String query = " SELECT id, itype, sub_type, target, actual, contribution, comment"
					+ "  FROM activity_indicators WHERE activity_id = " + activityID;
			try (Connection con = databaseManager.getConnection()) {
				ResultSet rs = databaseManager.makeQuery(query, con);
				while (rs.next()) {
					Map<String, String> activityIndicatorsData = new HashMap<>();
					activityIndicatorsData.put("id", rs.getString("id"));
					activityIndicatorsData.put("itype", rs.getString("itype"));
					activityIndicatorsData.put("sub_type", rs.getString("sub_type"));
					activityIndicatorsData.put("target",	rs.getString("target"));
					activityIndicatorsData.put("actual",	rs.getString("actual"));
					activityIndicatorsData.put("contribution",	rs.getString("contribution"));
					activityIndicatorsData.put("comment",	rs.getString("comment"));

					activityIndicatorsDataList.add(activityIndicatorsData);
				}
				rs.close();
			} catch (SQLException e) {
				LOG.error(
						"-- getActivityIndicators() > There was an error getting the activity indicators for activity {}",
						activityID, e);
			}
			LOG.debug(
					"<< getActivityIndicators():activityIndicatorsDataList.size={}",
					activityIndicatorsDataList.size());
			return activityIndicatorsDataList;
		}

		@Override
		public int saveActivityIndicators(Map<String, String> indicators,
				int activityID) {
			LOG.debug(">> saveActivityIndicators(indicators={}, activityID={})",
					indicators, activityID);
			boolean saved = false;
			String query = " INSERT INTO activity_indicators "
					+ "( id,  activity_id, itype, sub_type, target, actual, contribution, comment)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
					+ " ON DUPLICATE KEY UPDATE itype = VALUES(itype), sub_type = VAlUES(sub_type), "
					+ " activity_id = VALUES(activity_id), target = VALUES(target), actual = VALUES(actual),"
					+ " contribution = VALUES(contribution), comment = VALUES(comment)";

			Object[] values = new Object[8];
			values[0] = indicators.get("id");
			values[1] = activityID;
			values[2] = indicators.get("itype");
			values[3] = indicators.get("sub_type");
			values[4] = indicators.get("target");
			values[5] = indicators.get("actual");
			values[6] = indicators.get("contribution");
			values[7] = indicators.get("comment");

			try (Connection con = databaseManager.getConnection()) {
				int rows = databaseManager.makeChangeSecure(con, query, values);

				return rows;
			} catch (SQLException e) {
				LOG.error(
						"-- saveActivityIndicators() > There was an error saving indicators for activity {}.",
						activityID);
			}

			LOG.debug("<< saveActivityIndicators():{}", saved);
			return -1;
		}
}
