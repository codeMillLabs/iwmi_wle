/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.TokenActionDAO;
import org.cgiar.ccafs.ap.data.model.TokenAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * <p>
 * 
 * </p>
 * 
 * @author Amila Silva
 *
 */
public class MYSQLTokenActionDAO implements TokenActionDAO {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(MYSQLTokenActionDAO.class);
	private DAOManager databaseManager;


	@Inject
	public MYSQLTokenActionDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}
	
	private static final String INSERT_SQL = "INSERT INTO iwmi_token_activity( token_id, action, is_used ) "
			+ " VALUE (?, ?, ?) ";

	@Override
	public int saveToken(TokenAction tokenAction) {
		LOG.debug(">> saveToken()");
		try (Connection connection = databaseManager.getConnection()) {
			Object[] values = new Object[3];
			values[0] = tokenAction.getTokenId();
			values[1] = tokenAction.getAction();
			values[2] = (tokenAction.isUsed()) ? "1" : "0";

			int id = databaseManager.makeChangeSecure(connection, INSERT_SQL,
					values);

			LOG.info(">> Token saved successfully, [ Id : {}]", id);
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveToken() > There was an error getting the token saving.",
					e);
		}
		return -1;
	}

	private static final String UPDATE_SQL = "UPDATE iwmi_token_activity SET is_used = ? "
			+ " WHERE id = ? ";

	@Override
	public void updateToken(TokenAction tokenAction) {
		LOG.debug(">> updateToken()");
		try (Connection connection = databaseManager.getConnection()) {
			Object[] values = new Object[2];
			values[0] = (tokenAction.isUsed()) ? "1" : "0";
			values[1] = tokenAction.getId();

			int id = databaseManager.makeChangeSecure(connection, UPDATE_SQL,
					values);
			LOG.info(">> Token update successfully, [ Id : {}]", id);
		} catch (SQLException e) {
			LOG.error(
					"-- udpateToken() > There was an error getting the token updated.",
					e);
		}
	}

	@Override
	public TokenAction getTokenAction(String tokenId) {
		LOG.debug(">> getTokenAction()");
		String selectSQL = "SELECT id, token_id, action, is_used FROM iwmi_token_activity WHERE token_id = '"
				+ tokenId + "'" + " AND is_used = '0' ";
		try (Connection connection = databaseManager.getConnection()) {

			ResultSet results = databaseManager
					.makeQuery(selectSQL, connection);

			TokenAction tokenAction = null;
			if (results.next()) {
				tokenAction = new TokenAction();
				tokenAction.setId(results.getInt("id"));
				tokenAction.setTokenId(results.getString("token_id"));
				tokenAction.setAction(results.getString("action"));
				tokenAction
						.setUsed(results.getString("is_used").equals("1") ? true
								: false);
				LOG.info(">> Token Found successfully, [ Id : {}]",
						tokenAction.getId());
			}
			
			return tokenAction;
		} catch (SQLException e) {
			LOG.error(
					"-- getTokenAction() > There was an error getting the fetch token.",
					e);
		}
		return null;
	}

}
