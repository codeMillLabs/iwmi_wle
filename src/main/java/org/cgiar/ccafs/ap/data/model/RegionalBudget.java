/*
 * FILENAME
 *     RegionalBudget.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Regional Budget informations.
 * </p>
 * 
 *
 * @see TODO Related Information
 *
 * @author Amila Silva
 *
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegionalBudget implements Serializable, Comparable<RegionalBudget>, Difference<RegionalBudget>
{
    private static final long serialVersionUID = -4561756538026635853L;

    private String id;
    private String regionName;
    private String regionId;

    private List<RegionalBudgetDetails> details = new ArrayList<RegionalBudgetDetails>();

    @JsonIgnore
    private double personnelTotal = 0;

    @JsonIgnore
    private double operationsTotal = 0;

    @JsonIgnore
    private double indirectCostsTotal = 0;

    @JsonIgnore
    private double totalMainBudgetTotal = 0;

    @JsonIgnore
    private double totalRegionSum = 0;
    
    @Override
 	public DiffReport diff(RegionalBudget t) {
 		DiffReport report = new DiffReport();
 		
 		for(int i = 0; i < t.getDetails().size(); i++) {
 			RegionalBudgetDetails prev = this.details.get(i);			
 			RegionalBudgetDetails current = t.details.get(i);
 			DiffReport bDiff = prev.diff(current);
 			
 			if( bDiff.hasDifferent()) {
 				report.getDifferentReport().append("Regional Budget, Region :" + regionName + ",\n\t\t\t").append(bDiff.getDifferentReport()).append("\n");
 			}
 		}
 		return report;
 	}
    

    /**
     * <p>
     * Calculate the Total of the main budget.
     * </p>
     */
    @JsonIgnore
    public void calculateTotals()
    {
        for (RegionalBudgetDetails budgetDetails : details)
        {
            this.personnelTotal += budgetDetails.getPersonnel();
            this.operationsTotal += budgetDetails.getOperations();
            this.indirectCostsTotal += budgetDetails.getIndirectCosts();
            this.totalMainBudgetTotal += budgetDetails.getMainBudgetTotal();

            totalRegionSum += budgetDetails.getTotal();
        }
    }

    /**
     * <p>
     * Getter for totalRegionSum.
     * </p>
     * 
     * @return the totalRegionSum
     */
    public double getTotalRegionSum()
    {
        return totalRegionSum;
    }

    /**
     * <p>
     * Getter for totalMainBudgetTotal.
     * </p>
     * 
     * @return the totalMainBudgetTotal
     */
    public double getTotalMainBudgetTotal()
    {
        return totalMainBudgetTotal;
    }

    /**
     * <p>
     * Getter for personnelTotal.
     * </p>
     * 
     * @return the personnelTotal
     */
    public double getPersonnelTotal()
    {
        return personnelTotal;
    }

    /**
     * <p>
     * Getter for operationsTotal.
     * </p>
     * 
     * @return the operationsTotal
     */
    public double getOperationsTotal()
    {
        return operationsTotal;
    }

    /**
     * <p>
     * Getter for indirectCostsTotal.
     * </p>
     * 
     * @return the indirectCostsTotal
     */
    public double getIndirectCostsTotal()
    {
        return indirectCostsTotal;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for regionName.
     * </p>
     * 
     * @return the regionName
     */
    public String getRegionName()
    {
        return regionName;
    }

    /**
     * <p>
     * Setting value for regionName.
     * </p>
     * 
     * @param regionName
     *            the regionName to set
     */
    public void setRegionName(String regionName)
    {
        this.regionName = regionName;
    }

    /**
     * <p>
     * Getter for regionId.
     * </p>
     * 
     * @return the regionId
     */
    public String getRegionId()
    {
        return regionId;
    }

    /**
     * <p>
     * Setting value for regionId.
     * </p>
     * 
     * @param regionId
     *            the regionId to set
     */
    public void setRegionId(String regionId)
    {
        this.regionId = regionId;
    }

    /**
     * <p>
     * Getter for details.
     * </p>
     * 
     * @return the details
     */
    public List<RegionalBudgetDetails> getDetails()
    {
        return details;
    }

    /**
     * <p>
     * Setting value for details.
     * </p>
     * 
     * @param details
     *            the details to set
     */
    public void setDetails(List<RegionalBudgetDetails> details)
    {
        this.details = details;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(RegionalBudget o)
    {
        return this.regionName.compareToIgnoreCase(o.regionName);
    }

}
