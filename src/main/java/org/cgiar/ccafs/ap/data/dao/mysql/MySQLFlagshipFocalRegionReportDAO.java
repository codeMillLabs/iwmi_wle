package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.FlagshipFocalRegionReportDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLFlagshipFocalRegionReportDAO implements
		FlagshipFocalRegionReportDAO {

	// Looging
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLFlagshipFocalRegionReportDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLFlagshipFocalRegionReportDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}
	
	@Override
	public boolean deleteFlagshipFocalRegionReport(final int id) {
		LOG.debug(">> deleteFlagshipFocalRegionReport(id={})", id);
		boolean deleted = false;
		String query = "DELETE FROM fl_fr_rpt WHERE id = ?";
		Object[] values = new Object[1];
		values[0] = id;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteFlagshipFocalRegionReport() > There was a problem deleting the flagship / focal regions report {}. \n{}",
						id, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteFlagshipFocalRegionReport() > There was an error deleting the flagship / focal regions report {}",
					id, e);
		}
		LOG.debug("<< deleteFlagshipFocalRegionReport():{}", deleted);
		return deleted;
	}
	
	@Override
	public boolean deleteFlagshipClusters(int reportId) {
		LOG.debug(">> deleteFlagshipClusters(reportId={})", reportId);
		boolean deleted = false;
		String query = "DELETE FROM flfr_cluster WHERE rpt_id = ?";
		Object[] values = new Object[1];
		values[0] = reportId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteFlagshipClusters() > There was a problem deleting flagship clusters of report {}. \n{}",
						reportId, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteFlagshipClusters() > There was an error deleting the flagship clusters of report {}",
					reportId, e);
		}
		LOG.debug("<< deleteFlagshipClusters():{}", deleted);
		return deleted;
	}
	
	@Override
	public boolean deleteClusterDetails(int clusterId) {
		LOG.debug(">> deleteClusterDetails(outputActivityId={})", clusterId);
		boolean deleted = false;
		String query = "DELETE FROM flfr_output_detail WHERE cluster_id = ?";
		Object[] values = new Object[1];
		values[0] = clusterId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteClusterDetails() > There was a problem deleting the cluster details of cluster {}. \n{}",
						clusterId, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteClusterDetails() > There was an error deleting the cluster details of cluster {}",
					clusterId, e);
		}
		LOG.debug("<< deleteClusterDetails():{}", deleted);
		return deleted;
	}

	@Override
	public boolean deleteOutputActivities(int detailId) {
		LOG.debug(">> deleteOutputActivities(detailId={})", detailId);
		boolean deleted = false;
		String query = "DELETE FROM flfr_output_activity WHERE detail_id = ?";
		Object[] values = new Object[1];
		values[0] = detailId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteOutputActivities() > There was a problem deleting the ouput activities of cluster {}. \n{}",
						detailId, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteOutputActivities() > There was an error deleting the cluster ouput activities of cluster {}",
					detailId, e);
		}
		LOG.debug("<< deleteOutputActivities():{}", deleted);
		return deleted;
	}


	@Override
	public boolean deleteClusterOutputs(int outputActivityId) {
		LOG.debug(">> deleteClusterOutputs(outputActivityId={})", outputActivityId);
		boolean deleted = false;
		String query = "DELETE FROM flfr_output WHERE oact_id = ?";
		Object[] values = new Object[1];
		values[0] = outputActivityId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteClusterOutputs() > There was a problem deleting the cluster outputs of cluster {}. \n{}",
						outputActivityId, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteClusterOutputs() > There was an error deleting the cluster output of cluster {}",
					outputActivityId, e);
		}
		LOG.debug("<< deleteClusterOutputs():{}", deleted);
		return deleted;
	}

	@Override
	public Map<String, String> getFlagshipFocalRegionReport(
			final int flagshipId, final int year, final String period) {
		LOG.debug(">> getFlagshipFocalRegionReport(flagshipId={},year={})", flagshipId, year);
		Map<String, String> flagshipFocalRegionsRptMap = new HashMap<>();
		String query = " SELECT id, description, state, revision, mngr_comment, dir_comment, mngr_done, dir_done"
				+ "  FROM fl_fr_rpt WHERE flagship_id=" + flagshipId
				+ " AND year=" + year + " AND period='" + period
				+ "' ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				flagshipFocalRegionsRptMap.put("id", rs.getString("id"));
				flagshipFocalRegionsRptMap.put("description",
						rs.getString("description"));
				flagshipFocalRegionsRptMap.put("state",
						rs.getString("state"));
				flagshipFocalRegionsRptMap.put("revision",
						rs.getString("revision"));
				flagshipFocalRegionsRptMap.put("mngr_comment",
						rs.getString("mngr_comment"));
				flagshipFocalRegionsRptMap.put("dir_comment",
						rs.getString("dir_comment"));
				flagshipFocalRegionsRptMap.put("mngr_done",
						rs.getString("mngr_done"));
				flagshipFocalRegionsRptMap.put("dir_done",
						rs.getString("dir_done"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipFocalRegionReport() > There was an error getting flagship / focal regions report",
					e);
		}
		LOG.debug(
				"<< getFlagshipFocalRegionReport():flagshipFocalRegionsRptMap.size={}",
				flagshipFocalRegionsRptMap.size());
		return flagshipFocalRegionsRptMap;
	}
	
	@Override
	public Map<String, String> getFlagshipFocalRegionReport(
			final int reportId) {
		LOG.debug(">> getFlagshipFocalRegionReport(reportId={})", reportId);
		Map<String, String> flagshipFocalRegionsRptMap = new HashMap<>();
		String query = " SELECT id, description, state, revision, mngr_comment, dir_comment, mngr_done, dir_done, flagship_id, year, period"
				+ "  FROM fl_fr_rpt WHERE id=" + reportId
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				flagshipFocalRegionsRptMap.put("id", rs.getString("id"));
				flagshipFocalRegionsRptMap.put("description",
						rs.getString("description"));
				flagshipFocalRegionsRptMap.put("state",
						rs.getString("state"));
				flagshipFocalRegionsRptMap.put("revision",
						rs.getString("revision"));
				flagshipFocalRegionsRptMap.put("mngr_comment",
						rs.getString("mngr_comment"));
				flagshipFocalRegionsRptMap.put("dir_comment",
						rs.getString("dir_comment"));
				flagshipFocalRegionsRptMap.put("mngr_done",
						rs.getString("mngr_done"));
				flagshipFocalRegionsRptMap.put("dir_done",
						rs.getString("dir_done"));
				flagshipFocalRegionsRptMap.put("flagship_id",
						rs.getString("flagship_id"));
				flagshipFocalRegionsRptMap.put("year",
						rs.getString("year"));
				flagshipFocalRegionsRptMap.put("period",
						rs.getString("period"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipFocalRegionReport() > There was an error getting flagship / focal regions report",
					e);
		}
		LOG.debug(
				"<< getFlagshipFocalRegionReport():flagshipFocalRegionsRptMap.size={}",
				flagshipFocalRegionsRptMap.size());
		return flagshipFocalRegionsRptMap;
	}

	@Override
	public List<Map<String, String>> getFlagshipClusters(int reportId) {
		LOG.debug(">> getFlagshipClusters(reportId={})", reportId);
		List<Map<String, String>> flagshipClusterDataList = new ArrayList<>();
		String query = " SELECT id, spent, status, rating, name, act_count, budget, mngr_comment, dir_comment, exp, clus_id"
				+ "  FROM flfr_cluster WHERE rpt_id=" + reportId
				+ " ORDER BY clus_id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> flagshipClusterData = new HashMap<>();
				flagshipClusterData.put("id", rs.getString("id"));
				flagshipClusterData.put("spent", rs.getString("spent"));
				flagshipClusterData.put("status", rs.getString("status"));
				flagshipClusterData.put("rating", rs.getString("rating"));
				flagshipClusterData.put("name", rs.getString("name"));
				flagshipClusterData.put("act_count", rs.getString("act_count"));
				flagshipClusterData.put("budget", rs.getString("budget"));
				flagshipClusterData.put("mngr_comment", rs.getString("mngr_comment"));
				flagshipClusterData.put("dir_comment", rs.getString("dir_comment"));
				flagshipClusterData.put("exp", rs.getString("exp"));
				flagshipClusterData.put("clus_id", rs.getString("clus_id"));

				flagshipClusterDataList.add(flagshipClusterData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipClusters() > There was an error getting flagship clusters",
					e);
		}
		LOG.debug("<< getFlagshipClusters():flagshipClusterDataList.size={}",
				flagshipClusterDataList.size());
		return flagshipClusterDataList;
	}
	
	@Override
	public List<Map<String, String>> getClusterDetails(int clusterId) {
		LOG.debug(">> getClusterDetails(clusterId={})", clusterId);
		List<Map<String, String>> clusterOutputDetailDataList = new ArrayList<>();
		String query = " SELECT id, code, description, comment, mngr_comment, dir_comment"
				+ "  FROM flfr_output_detail WHERE cluster_id=" + clusterId
				+ " ORDER BY code";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> clusterOutputDetailData = new HashMap<>();
				clusterOutputDetailData.put("id", rs.getString("id"));
				clusterOutputDetailData.put("code", rs.getString("code"));
				clusterOutputDetailData.put("description", rs.getString("description"));
				clusterOutputDetailData.put("comment", rs.getString("comment"));
				clusterOutputDetailData.put("mngr_comment", rs.getString("mngr_comment"));
				clusterOutputDetailData.put("dir_comment", rs.getString("dir_comment"));
				clusterOutputDetailDataList.add(clusterOutputDetailData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getClusterDetails() > There was an error getting cluster output details",
					e);
		}
		LOG.debug("<< getClusterDetails():clusterOutputDetailDataList.size={}",
				clusterOutputDetailDataList.size());
		return clusterOutputDetailDataList;
	}
	
	@Override
	public List<Map<String, String>> getOutputActivities(int detailId) {
		LOG.debug(">> getOutputActivities(detailId={})", detailId);
		List<Map<String, String>> outputActivityDetailDataList = new ArrayList<>();
		String query = " SELECT id, activity_id, activity_name"
				+ "  FROM flfr_output_activity WHERE detail_id=" + detailId
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> clusterOutputDetailData = new HashMap<>();
				clusterOutputDetailData.put("id", rs.getString("id"));
				clusterOutputDetailData.put("activity_id", rs.getString("activity_id"));
				clusterOutputDetailData.put("activity_name", rs.getString("activity_name"));
				outputActivityDetailDataList.add(clusterOutputDetailData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getClusterDetails() > There was an error getting cluster output details",
					e);
		}
		LOG.debug("<< getClusterDetails():clusterOutputDetailDataList.size={}",
				outputActivityDetailDataList.size());
		return outputActivityDetailDataList;
	}

	@Override
	public List<Map<String, String>> getClusterOutputs(int outputActivityId) {
		LOG.debug(">> getClusterOutputs(detailId={})", outputActivityId);
		List<Map<String, String>> clusterOutputsDataList = new ArrayList<>();
		String query = " SELECT id, oact_id, activity_id, activity_name, project_id, project_name, progress, status, rating"
				+ "  FROM flfr_output WHERE oact_id=" + outputActivityId
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> clusterOutputsData = new HashMap<>();
				clusterOutputsData.put("id", rs.getString("id"));
				clusterOutputsData
						.put("oact_id", rs.getString("oact_id"));
				clusterOutputsData.put("activity_id", rs.getString("activity_id"));
				clusterOutputsData.put("activity_name", rs.getString("activity_name"));
				clusterOutputsData.put("project_id", rs.getString("project_id"));
				clusterOutputsData.put("project_name", rs.getString("project_name"));
				clusterOutputsData.put("progress", rs.getString("progress"));
				clusterOutputsData.put("status", rs.getString("status"));
				clusterOutputsData.put("rating", rs.getString("rating"));
				clusterOutputsDataList.add(clusterOutputsData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getClusterOutputs() > There was an error getting cluster outputs",
					e);
		}
		LOG.debug("<< getClusterOutputs():flagshipClusterDataList.size={}",
				clusterOutputsDataList.size());
		return clusterOutputsDataList;
	}

	@Override
	public int saveFlagshipFocalRegionReport(Map<String, String> keyValues) {
		LOG.debug(">> saveFlagshipFocalRegionReport( Flagship / Focal Regions report : {})", keyValues);

		String query = "INSERT INTO fl_fr_rpt ( year, flagship_id, period, description, state, revision, mngr_comment, dir_comment, mngr_done, dir_done) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[10];
			values[0] = keyValues.get("year");
			values[1] = keyValues.get("flagship_id");
			values[2] = keyValues.get("period");
			values[3] = keyValues.get("description");
			values[4] = keyValues.get("state");
			values[5] = keyValues.get("revision");
			values[6] = keyValues.get("mngr_comment");
			values[7] = keyValues.get("dir_comment");
			values[8] = keyValues.get("mngr_done");
			values[9] = keyValues.get("dir_done");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Flagship / Focal Regions report saved ( Flagship / Focal Regions report : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveFlagshipFocalRegionReport() > There was an error trying to add new Flagship / Focal Region : '{}'",
					keyValues.get("flagship_id"), e);
		}
		return -1;
	}
	
	
	@Override
	public boolean updateFlagshipFocalRegionReport(int id, Map<String, String> keyValues) {
		LOG.debug(">> updateFlagshipFocalRegionReport( Flagship / Focal Regions report : {}), reportId = {}", id, keyValues);
		boolean updated = false;
		String query = "UPDATE fl_fr_rpt SET year=?, flagship_id=?, period=?, description=?, state=?, revision=?, mngr_comment=?, dir_comment=?, mngr_done=?, dir_done=? WHERE id=?";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[11];
			values[0] = keyValues.get("year");
			values[1] = keyValues.get("flagship_id");
			values[2] = keyValues.get("period");
			values[3] = keyValues.get("description");
			values[4] = keyValues.get("state");
			values[5] = keyValues.get("revision");
			values[6] = keyValues.get("mngr_comment");
			values[7] = keyValues.get("dir_comment");
			values[8] = keyValues.get("mngr_done");
			values[9] = keyValues.get("dir_done");
			values[10] = id;
			

			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- updateFlagshipFocalRegionReport() > There was a problem updating report {}",
						id, query);
			} else {
				updated = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- updateFlagshipFocalRegionReport() > There was an error trying update Flagship / Focal Region report : '{}'",
					id, e);
		}
		return updated;
	}

	@Override
	public int saveFlagshipCluster(Map<String, String> keyValues, int reportId) {
		LOG.debug(">> saveFlagshipCluster( Flagship cluster : {})", keyValues);

		String query = "INSERT INTO flfr_cluster (rpt_id, spent, status, rating, name, act_count, budget, mngr_comment, dir_comment, exp, clus_id) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[11];
			values[0] = reportId;
			values[1] = keyValues.get("spent");
			values[2] = keyValues.get("status");
			values[3] = keyValues.get("rating");
			values[4] = keyValues.get("name");
			values[5] = keyValues.get("act_count");
			values[6] = keyValues.get("budget");
			values[7] = keyValues.get("mngr_comment");
			values[8] = keyValues.get("dir_comment");
			values[9] = keyValues.get("exp");
			values[10] = keyValues.get("clus_id");


			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Flagship cluster saved ( Flagship cluster : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveFlagshipCluster() > There was an error trying to add new Flagship cluster of report : '{}'",
					reportId, e);
		}
		return -1;
	}
	

	@Override
	public int saveClusterDetails(Map<String, String> keyValues, int clusterId) {
		LOG.debug(">> saveClusterDetails( Cluster Details : {})", keyValues);

		String query = "INSERT INTO flfr_output_detail (cluster_id, code, description, comment, mngr_comment, dir_comment) VALUES ( ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[6];
			values[0] = clusterId;
			values[1] = keyValues.get("code");
			values[2] = keyValues.get("description");
			values[3] = keyValues.get("comment");
			values[4] = keyValues.get("mngr_comment");
			values[5] = keyValues.get("dir_comment");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Flagship cluster saved ( Flagship cluster details : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveClusterDetails() > There was an error trying to add new cluster details of report : '{}'",
					clusterId, e);
		}
		return -1;
	}
	
	@Override
	public int saveOutputActivities(Map<String, String> keyValues, int detailId) {
		LOG.debug(">> saveOutputActivities( Output Activity Details : {})", keyValues);

		String query = "INSERT INTO flfr_output_activity (detail_id, activity_id, activity_name) VALUES ( ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[3];
			values[0] = detailId;
			values[1] = keyValues.get("activity_id");
			values[2] = keyValues.get("activity_name");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Flagship cluster saved ( Output activity : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveOutputActivities() > There was an error trying to add new out activity of report : '{}'",
					detailId, e);
		}
		return -1;
	}

	@Override
	public int saveClusterOutputs(Map<String, String> keyValues, int outputActivityId) {
		LOG.debug(">> saveClusterOutputs( Cluster output : {})", keyValues);

		String query = "INSERT INTO flfr_output ( oact_id, activity_id, activity_name, project_id, project_name, progress, status, rating) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[8];
			values[0] = outputActivityId;
			values[1] = keyValues.get("activity_id");
			values[2] = keyValues.get("activity_name");
			values[3] = keyValues.get("project_id");
			values[4] = keyValues.get("project_name");
			values[5] = keyValues.get("progress");
			values[6] = keyValues.get("status");
			values[7] = keyValues.get("rating");


			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Cluster output saved ( Cluster output : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveClusterOutputs() > There was an error trying to add new cluster output of cluster : '{}'",
					keyValues.get("cluster_id"), e);
		}
		return -1;
	}

	@Override
	public boolean updateState(int reportId, String newState) {
		LOG.debug(">> updateState(reportId={}, newState={})", reportId,newState);
		boolean updated = false;
		String query = "UPDATE fl_fr_rpt SET state=? WHERE id = ?";
		Object[] values = new Object[2];
		values[0] = newState;
		values[1] = reportId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- updateState() > There was a problem updating report state {}",
						reportId, query);
			} else {
				updated = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- updateState() > There was an error updating report state",
					reportId, e);
		}
		LOG.debug("<< updateState():{}", updated);
		return updated;
	}
	
	@Override
	public boolean updateRevision(int reportId) {
		LOG.debug(">> updateRevision(reportId={})", reportId);
		boolean updated = false;
		String query = "UPDATE fl_fr_rpt SET revision=revision+1 WHERE id = ?";
		Object[] values = new Object[1];
		values[0] = reportId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- updateRevision() > There was a problem updating report revision {}",
						reportId, query);
			} else {
				updated = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- updateRevision() > There was an error updating report revision",
					reportId, e);
		}
		LOG.debug("<< updateRevision():{}", updated);
		return updated;
	}
	
	
	@Override
	public List<Map<String, String>> getComments(int reportId) {
		LOG.debug(">> getComments(reportId={})", reportId);
		List<Map<String, String>> flagshipReportCommentDataList = new ArrayList<>();
		String query = " SELECT id, user_id, comment, revision"
				+ "  FROM flfr_comment WHERE rpt_id=" + reportId
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> flagshipReportData = new HashMap<>();
				flagshipReportData.put("id", rs.getString("id"));
				flagshipReportData.put("user_id", rs.getString("user_id"));
				flagshipReportData.put("comment", rs.getString("comment"));
				flagshipReportData.put("revision", rs.getString("revision"));
				flagshipReportCommentDataList.add(flagshipReportData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getComments() > There was an error getting flagship report coments",
					e);
		}
		LOG.debug("<< getComments():flagshipReportCommentDataList.size={}",
				flagshipReportCommentDataList.size());
		return flagshipReportCommentDataList;
	}
	
	@Override
	public List<Map<String, String>> getComments(int reportId, int revision) {
		LOG.debug(">> getComments(reportId={},revision={})", reportId, revision);
		List<Map<String, String>> flagshipReportCommentDataList = new ArrayList<>();
		String query = " SELECT id, user_id, comment, revision"
				+ "  FROM flfr_comment WHERE rpt_id=" + reportId
				+ " AND revision=" + revision
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> flagshipReportData = new HashMap<>();
				flagshipReportData.put("id", rs.getString("id"));
				flagshipReportData.put("user_id", rs.getString("user_id"));
				flagshipReportData.put("comment", rs.getString("comment"));
				flagshipReportData.put("revision", rs.getString("revision"));
				flagshipReportCommentDataList.add(flagshipReportData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getComments() > There was an error getting flagship report coments",
					e);
		}
		LOG.debug("<< getComments():flagshipReportCommentDataList.size={}",
				flagshipReportCommentDataList.size());
		return flagshipReportCommentDataList;
	}
	
	
	@Override
	public int saveComment(int reportId, Map<String, String> keyValues) {
		LOG.debug(">> saveComment( Comment : {})", keyValues);

		String query = "INSERT INTO flfr_comment ( rpt_id, user_id, comment, revision) VALUES ( ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[4];
			values[0] = reportId;
			values[1] = keyValues.get("user_id");
			values[2] = keyValues.get("comment");
			values[3] = keyValues.get("revision");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Cluster output saved ( Cluster output : {})", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveComment() > There was an error trying to add new comment to flagship report : '{}'",
					reportId, e);
		}
		return -1;
	}
}