/**
 * 
 */
package org.cgiar.ccafs.ap.data.model;

/**
 * @author asilva
 *
 */
public class DiffReport {

	private boolean hasDifferent = false;
	private String title;
	private StringBuilder differentReport = new StringBuilder();

	public void append(String fieldName, String prevValue, String currentValue) {
		hasDifferent = true;
		differentReport.append("\t\t Field :" + fieldName
				+ ", Previsous Value :" + filter(prevValue)
				+ ", Current Value :" + filter(currentValue) + "\n");
	}

	public void append(DiffReport report) {
		if (report.hasDifferent) {
			hasDifferent = true;
			differentReport.append(report.getDifferentReport()).append("\n");
		}
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public void append(String str) {
		hasDifferent = true;
		differentReport.append(str).append("\n");
	}

	/**
	 * @return the differentReport
	 */
	public StringBuilder getDifferentReport() {
		return differentReport;
	}

	/**
	 * @param differentReport
	 *            the differentReport to set
	 */
	public void setDifferentReport(StringBuilder differentReport) {
		this.differentReport = differentReport;
	}

	/**
	 * @return the hasDifferent
	 */
	public boolean hasDifferent() {
		return hasDifferent;
	}

	/**
	 * @param hasDifferent
	 *            the hasDifferent to set
	 */
	public void setHasDifferent(boolean hasDifferent) {
		this.hasDifferent = hasDifferent;
	}

	private String filter(String str) {
		if (str == null || str.isEmpty())
			return "";
		else
			return str;
	}

}
