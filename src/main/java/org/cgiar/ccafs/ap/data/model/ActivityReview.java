/*
 * FILENAME
 *     ActivityReviewer.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
/**
 * 
 * <p>
 * ActivityReviewer
 * </p>
 * 
 *
 * @author Amila Silva
 *
 *
 */
public class ActivityReview implements Serializable
{
    private static final long serialVersionUID = 644255125487490483L;
    public static final String INTERNAL_TYPE = "INTERNAL";
    public static final String EXTERNAL_TYPE = "EXTERNAL";

    private long id;
    private int activityId;
    private String activityRevision;
    private long reviewerId;
    private String reviewerName;
    private String type; // INTERNAL, EXTERNAL
    private boolean satisfied;
    private boolean roundCompleted;

    private boolean editable;

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + activityId;
        result = prime * result + ((activityRevision == null) ? 0 : activityRevision.hashCode());
        result = prime * result + (int) (reviewerId ^ (reviewerId >>> 32));
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActivityReview other = (ActivityReview) obj;
        if (activityId == other.activityId && activityRevision.equals(other.activityRevision)
            && type.equals(other.type) && reviewerId == other.reviewerId)
        {
            return true;
        }
        return false;
    }

    public String getActivityRevision()
    {
        return activityRevision;
    }

    public void setActivityRevision(String activityRevision)
    {
        this.activityRevision = activityRevision;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public int getActivityId()
    {
        return activityId;
    }

    public void setActivityId(int activityId)
    {
        this.activityId = activityId;
    }

    public long getReviewerId()
    {
        return reviewerId;
    }

    public void setReviewerId(long reviewerId)
    {
        this.reviewerId = reviewerId;
    }

    public String getReviewerName()
    {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName)
    {
        this.reviewerName = reviewerName;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public boolean isSatisfied()
    {
        return satisfied;
    }

    public void setSatisfied(boolean satisfied)
    {
        this.satisfied = satisfied;
    }

    public boolean isRoundCompleted()
    {
        return roundCompleted;
    }

    public void setRoundCompleted(boolean roundCompleted)
    {
        this.roundCompleted = roundCompleted;
    }

}
