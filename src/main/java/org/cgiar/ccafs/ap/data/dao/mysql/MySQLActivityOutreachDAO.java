/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityOutreachDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLActivityOutreachDAO implements ActivityOutreachDAO {

	// Looging
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityOutreachDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLActivityOutreachDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public boolean deleteActivityOutreachs(int activityID) {
		LOG.debug(">> deleteActivityOutreachs(activityID={})", activityID);
		boolean deleted = false;
		String query = "DELETE FROM activity_outreachs WHERE activity_id = ?";
		Object[] values = new Object[1];
		values[0] = activityID;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteActivityOutreachs() > There was a problem deleting the outreachs related to the activity {}. \n{}",
						activityID, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteActivityOutreachs() > There was an error deleting the outreachs related to the activity {}",
					activityID, e);
		}
		LOG.debug("<< deleteActivityOutreachs():{}", deleted);
		return deleted;
	}

	@Override
	public List<Map<String, String>> getActivityOutreachs(int activityID) {
		LOG.debug(">> getActivityOutreachs(activityID={})", activityID);
		List<Map<String, String>> activityOutreachsDataList = new ArrayList<>();
		String query = " SELECT id, otype, groups, paricipants, female_participants, date_and_location, comments"
				+ "  FROM activity_outreachs WHERE activity_id = " + activityID;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> activityPublicationsData = new HashMap<>();
				activityPublicationsData.put("id", rs.getString("id"));
				activityPublicationsData.put("otype", rs.getString("otype"));
				activityPublicationsData.put("groups",
						rs.getString("groups"));
				activityPublicationsData.put("paricipants",
						rs.getString("paricipants"));
				activityPublicationsData.put("female_participants",
						rs.getString("female_participants"));
				activityPublicationsData.put("date_and_location",
						rs.getString("date_and_location"));
				activityPublicationsData.put("comments",
						rs.getString("comments"));

				activityOutreachsDataList.add(activityPublicationsData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivityOutreachs() > There was an error getting the activity outreachs for activity {}",
					activityID, e);
		}
		LOG.debug(
				"<< getActivityOutreachs():activityOutreachsDataList.size={}",
				activityOutreachsDataList.size());
		return activityOutreachsDataList;
	}

	@Override
	public int saveActivityOutreachs(Map<String, String> outreachs,
			int activityID) {
		LOG.debug(">> saveActivityOutreachs(outreachs={}, activityID={})",
				outreachs, activityID);
		boolean saved = false;
		String query = " INSERT INTO activity_outreachs "
				+ "( id,  activity_id, otype, groups, paricipants, female_participants, date_and_location, comments)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
				+ " ON DUPLICATE KEY UPDATE otype = VALUES(otype), groups = VAlUES(groups), "
				+ " activity_id = VALUES(activity_id), paricipants = VALUES(paricipants),"
				+ " female_participants = VALUES(female_participants), date_and_location = VALUES(date_and_location),"
				+ " comments = VALUES(comments)";

		Object[] values = new Object[8];
		values[0] = outreachs.get("id");
		values[1] = activityID;
		values[2] = outreachs.get("otype");
		values[3] = outreachs.get("groups");
		values[4] = outreachs.get("paricipants");
		values[5] = outreachs.get("female_participants");
		values[6] = outreachs.get("date_and_location");
		values[7] = outreachs.get("comments");

		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);

			return rows;
		} catch (SQLException e) {
			LOG.error(
					"-- saveActivityOutreachs() > There was an error saving outreachs for activity {}.",
					activityID);
		}

		LOG.debug("<< saveActivityOutreachs():{}", saved);
		return -1;
	}

}
