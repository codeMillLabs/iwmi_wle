/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityBudgetDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
public class ActivityBudgetManagerImpl implements ActivityBudgetManager {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityBudgetManagerImpl.class);
	private ActivityBudgetDAO budgetDAO;

	@Inject
	public ActivityBudgetManagerImpl(ActivityBudgetDAO budgetDAO) {
		this.budgetDAO = budgetDAO;
	}

	@Override
	public ActivityBudget getBudget(int activityID) {
		ActivityBudget budget = budgetDAO.getBudget(activityID);

		LOG.debug("The budget for the activity {} was loaded successfully.",
				activityID);
		return budget;
	}

	@Override
	public int saveBudget(ActivityBudget budget) {

		return budgetDAO.saveActivityBudget(budget);
	}

	@Override
	public void updateBudget(ActivityBudget budget) {

		budgetDAO.updateActivityBudget(budget);
	}

	@Override
	public List<ActivityBudget> getActivityBudgetsByCriteria(int year,
			int activityId, String leadCenter, String cluster, String flagship) {
		return budgetDAO.getActivityBudgetsByCriteria(year, activityId, leadCenter, cluster, flagship);
	}
}
