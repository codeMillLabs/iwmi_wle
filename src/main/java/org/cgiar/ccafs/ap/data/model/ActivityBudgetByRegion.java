/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 
 * @author Amila Silva
 * 
 *
 */
public class ActivityBudgetByRegion {

	private int id;
	private int activityId;
	private double eastAfricaBudget;
	private double westAfricaBudget;
	private double southernAfricaBudget;
	private double centralAsiaBudget;
	private double middleEastBudget;
	private double southAsiaBudget;
	private double southEastAsiaBudget;
	private double latinAmericaBudget;
	private double globalBudget;
	private double otherBudget;
	
	private double regionTotalBudget;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public double getEastAfricaBudget() {
		return eastAfricaBudget;
	}

	public void setEastAfricaBudget(double eastAfricaBudget) {
		this.eastAfricaBudget = eastAfricaBudget;
	}

	public double getWestAfricaBudget() {
		return westAfricaBudget;
	}

	public void setWestAfricaBudget(double westAfricaBudget) {
		this.westAfricaBudget = westAfricaBudget;
	}

	public double getSouthernAfricaBudget() {
		return southernAfricaBudget;
	}

	public void setSouthernAfricaBudget(double southernAfricaBudget) {
		this.southernAfricaBudget = southernAfricaBudget;
	}

	public double getCentralAsiaBudget() {
		return centralAsiaBudget;
	}

	public void setCentralAsiaBudget(double centralAsiaBudget) {
		this.centralAsiaBudget = centralAsiaBudget;
	}

	public double getMiddleEastBudget() {
		return middleEastBudget;
	}

	public void setMiddleEastBudget(double middleEastBudget) {
		this.middleEastBudget = middleEastBudget;
	}

	public double getSouthAsiaBudget() {
		return southAsiaBudget;
	}

	public void setSouthAsiaBudget(double southAsiaBudget) {
		this.southAsiaBudget = southAsiaBudget;
	}

	public double getSouthEastAsiaBudget() {
		return southEastAsiaBudget;
	}

	public void setSouthEastAsiaBudget(double southEastAsiaBudget) {
		this.southEastAsiaBudget = southEastAsiaBudget;
	}

	public double getLatinAmericaBudget() {
		return latinAmericaBudget;
	}

	public void setLatinAmericaBudget(double latinAmericaBudget) {
		this.latinAmericaBudget = latinAmericaBudget;
	}

	public double getGlobalBudget() {
		return globalBudget;
	}

	public void setGlobalBudget(double globalBudget) {
		this.globalBudget = globalBudget;
	}

	public double getOtherBudget() {
		return otherBudget;
	}

	public void setOtherBudget(double otherBudget) {
		this.otherBudget = otherBudget;
	}

	public double getRegionTotalBudget() {
		return regionTotalBudget;
	}

	public void setRegionTotalBudget(double regionTotalBudget) {
		this.regionTotalBudget = regionTotalBudget;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
