/*
 * FILENAME
 *     ActivityBudgetTotals.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

public class ActivityBudgetTotals
{

    private static final Logger LOG = LoggerFactory.getLogger(ActivityBudgetTotals.class);

    private ActivityBudget activityBudget;

    // Main budget totals
    private double w1w2BudgetTotal = 0;
    private double w1w2GenderPrecTotal = 0;
    private double w1w2GenderBudgetTotal = 0;

    private double w3BudgetTotal = 0;
    private double w3GenderPrecTotal = 0;
    private double w3GenderBudgetTotal = 0;

    private double bilateralBudgetTotal = 0;
    private double bilateralGenderPrecTotal = 0;
    private double bilateralGenderBudgetTotal = 0;

    private double otherBudgetTotal = 0;
    private double otherGenderPrecTotal = 0;
    private double otherGenderBudgetTotal = 0;

    // Regional budget totals
    private double personnelTotal = 0;
    private double operationsTotal = 0;
    private double indirectCostsTotal = 0;

    // Donor budget totals
    private double w3DonorTotal = 0;
    private double bilateralDonorTotal = 0;
    private double otherDonorTotal = 0;

    public ActivityBudgetTotals(ActivityBudget activityBudget)
    {
        this.activityBudget = activityBudget;
    }

    /**
     * <p>
     * Sum Activity budget categories ( Main/ Regional/ Donor's).
     * </p>
     *
     */
    public void sumActivityBudget()
    {
        sumMainBudgetTotal();
        sumRegionalBudgetTotal();
        sumDonorBudgetTotal();

        StringBuilder builder = new StringBuilder();

        builder.append("\nActivity Budget Calculation : \n");
        builder.append(" -> Main Budget : \n");
        builder.append("         w1w2BudgetTotal : " + w1w2BudgetTotal + ", w1w2GenderPrecTotal :"
            + w1w2GenderPrecTotal + ", w1w2GenderBudgetTotal:" + w1w2GenderBudgetTotal + "\n");
        builder.append("         w3BudgetTotal : " + w3BudgetTotal + ", w3GenderPrecTotal :" + w3GenderPrecTotal
            + ", w3GenderBudgetTotal:" + w3GenderBudgetTotal + "\n");
        builder.append("         bilateralBudgetTotal : " + bilateralBudgetTotal + ", bilateralGenderPrecTotal :"
            + bilateralGenderPrecTotal + ", bilateralGenderBudgetTotal:" + bilateralGenderBudgetTotal + "\n");
        builder.append("         otherBudgetTotal : " + otherBudgetTotal + ", otherGenderPrecTotal :"
            + otherGenderPrecTotal + ", otherGenderBudgetTotal:" + otherGenderBudgetTotal + "\n");

        builder.append(" -> Regional Budget : \n");
        builder.append("         personnelTotal : " + personnelTotal + ", operationsTotal :" + operationsTotal
            + ", indirectCostsTotal:" + indirectCostsTotal + "\n");

        builder.append(" -> Donor Budget : \n");
        builder.append("         w3DonorTotal : " + w3DonorTotal + ", bilateralDonorTotal :" + bilateralDonorTotal
            + ", otherDonorTotal:" + otherDonorTotal + "\n");

        LOG.debug(builder.toString());
    }

    public double getGrandTotalOfFunding()
    {
        return w1w2BudgetTotal + w3BudgetTotal + bilateralBudgetTotal + otherBudgetTotal;
    }

    public double getTotalGenderPrecentage()
    {
        return roundUp(getGrandTotalGenderBudget() / getGrandTotalOfFunding() * 100);
    }

    public double getGrandTotalGenderBudget()
    {
        return w1w2GenderBudgetTotal + w3GenderBudgetTotal + bilateralGenderBudgetTotal + otherGenderBudgetTotal;
    }

    public void sumMainBudgetTotal()
    {
        for (MainBudget mainBudget : this.activityBudget.getMainBudgets())
        {
            mainBudget.calculateTotals();
            w1w2BudgetTotal += mainBudget.getW1w2BudgetTotal();
            w3BudgetTotal += mainBudget.getW3BudgetTotal();
            bilateralBudgetTotal += mainBudget.getBilateralBudgetTotal();
            otherBudgetTotal += mainBudget.getOtherBudgetTotal();

            w1w2GenderPrecTotal += mainBudget.getW1w2GenderPrecTotal();
            w3GenderPrecTotal += mainBudget.getW3GenderPrecTotal();
            bilateralGenderPrecTotal += mainBudget.getBilateralGenderPrecTotal();
            otherGenderPrecTotal += mainBudget.getOtherGenderPrecTotal();

            w1w2GenderBudgetTotal += mainBudget.getW1w2GenderBudgetTotal();
            w3GenderBudgetTotal += mainBudget.getW3GenderBudgetTotal();
            bilateralGenderBudgetTotal += mainBudget.getBilateralGenderBudgetTotal();
            otherGenderBudgetTotal += mainBudget.getOtherGenderBudgetTotal();
        }

        w1w2GenderPrecTotal = roundUp((w1w2GenderBudgetTotal / w1w2BudgetTotal) * 100);
        w3GenderPrecTotal = roundUp((w3GenderBudgetTotal / w3BudgetTotal) * 100);
        bilateralGenderPrecTotal = roundUp((bilateralGenderBudgetTotal / bilateralBudgetTotal) * 100);
        otherGenderPrecTotal = roundUp((otherGenderBudgetTotal / otherBudgetTotal) * 100);
    }

    public static double roundUp(double value)
    {
        value = Math.round(value * 100);
        value = value / 100;
        return value;
    }

    public void sumRegionalBudgetTotal()
    {
        for (RegionalBudget regionalBudget : this.activityBudget.getRegBudgets())
        {
            regionalBudget.calculateTotals();
            personnelTotal += regionalBudget.getPersonnelTotal();
            operationsTotal += regionalBudget.getOperationsTotal();
            indirectCostsTotal += regionalBudget.getIndirectCostsTotal();
        }
    }

    public void sumDonorBudgetTotal()
    {
        for (DonorBudget donorBudget : this.activityBudget.getDonorsBudget())
        {
            w3DonorTotal += donorBudget.getW3();
            bilateralDonorTotal += donorBudget.getBilateral();
            otherDonorTotal += donorBudget.getOther();
        }
    }

    /**
     * <p>
     * Getter for grandDonorTotal.
     * </p>
     * 
     * @return the grandDonorTotal
     */
    public double getGrandDonorTotal()
    {
        return getW3DonorTotal() + getBilateralDonorTotal() + getOtherDonorTotal();
    }

    /**
     * <p>
     * Getter for w1w2BudgetTotal.
     * </p>
     * 
     * @return the w1w2BudgetTotal
     */
    public double getW1w2BudgetTotal()
    {
        return w1w2BudgetTotal;
    }

    /**
     * <p>
     * Getter for w1w2GenderPrecTotal.
     * </p>
     * 
     * @return the w1w2GenderPrecTotal
     */
    public double getW1w2GenderPrecTotal()
    {
        return w1w2GenderPrecTotal;
    }

    /**
     * <p>
     * Getter for w1w2GenderBudgetTotal.
     * </p>
     * 
     * @return the w1w2GenderBudgetTotal
     */
    public double getW1w2GenderBudgetTotal()
    {
        return w1w2GenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for w3BudgetTotal.
     * </p>
     * 
     * @return the w3BudgetTotal
     */
    public double getW3BudgetTotal()
    {
        return w3BudgetTotal;
    }

    /**
     * <p>
     * Getter for w3GenderPrecTotal.
     * </p>
     * 
     * @return the w3GenderPrecTotal
     */
    public double getW3GenderPrecTotal()
    {
        return w3GenderPrecTotal;
    }

    /**
     * <p>
     * Getter for w3GenderBudgetTotal.
     * </p>
     * 
     * @return the w3GenderBudgetTotal
     */
    public double getW3GenderBudgetTotal()
    {
        return w3GenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for bilateralBudgetTotal.
     * </p>
     * 
     * @return the bilateralBudgetTotal
     */
    public double getBilateralBudgetTotal()
    {
        return bilateralBudgetTotal;
    }

    /**
     * <p>
     * Getter for bilateralGenderPrecTotal.
     * </p>
     * 
     * @return the bilateralGenderPrecTotal
     */
    public double getBilateralGenderPrecTotal()
    {
        return bilateralGenderPrecTotal;
    }

    /**
     * <p>
     * Getter for bilateralGenderBudgetTotal.
     * </p>
     * 
     * @return the bilateralGenderBudgetTotal
     */
    public double getBilateralGenderBudgetTotal()
    {
        return bilateralGenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for otherBudgetTotal.
     * </p>
     * 
     * @return the otherBudgetTotal
     */
    public double getOtherBudgetTotal()
    {
        return otherBudgetTotal;
    }

    /**
     * <p>
     * Getter for otherGenderPrecTotal.
     * </p>
     * 
     * @return the otherGenderPrecTotal
     */
    public double getOtherGenderPrecTotal()
    {
        return otherGenderPrecTotal;
    }

    /**
     * <p>
     * Getter for otherGenderBudgetTotal.
     * </p>
     * 
     * @return the otherGenderBudgetTotal
     */
    public double getOtherGenderBudgetTotal()
    {
        return otherGenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for personnelTotal.
     * </p>
     * 
     * @return the personnelTotal
     */
    public double getPersonnelTotal()
    {
        return personnelTotal;
    }

    /**
     * <p>
     * Getter for operationsTotal.
     * </p>
     * 
     * @return the operationsTotal
     */
    public double getOperationsTotal()
    {
        return operationsTotal;
    }

    /**
     * <p>
     * Getter for indirectCostsTotal.
     * </p>
     * 
     * @return the indirectCostsTotal
     */
    public double getIndirectCostsTotal()
    {
        return indirectCostsTotal;
    }

    /**
     * <p>
     * Getter for w3DonorTotal.
     * </p>
     * 
     * @return the w3DonorTotal
     */
    public double getW3DonorTotal()
    {
        return w3DonorTotal;
    }

    /**
     * <p>
     * Getter for bilateralDonorTotal.
     * </p>
     * 
     * @return the bilateralDonorTotal
     */
    public double getBilateralDonorTotal()
    {
        return bilateralDonorTotal;
    }

    /**
     * <p>
     * Getter for otherDonorTotal.
     * </p>
     * 
     * @return the otherDonorTotal
     */
    public double getOtherDonorTotal()
    {
        return otherDonorTotal;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }
}
