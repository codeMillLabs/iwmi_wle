/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

public class FlagshipReportData {

	private int id;
	private FlagshipFocalRegion flagshipFocalRegion;
	private String cluster;
	private String outputCode;
	private String output;
	private String activityId;
	private String project;
	private String delCode;
	private String deliverable;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the flagshipFocalRegion
	 */
	public FlagshipFocalRegion getFlagshipFocalRegion() {
		return flagshipFocalRegion;
	}

	/**
	 * @param flagshipFocalRegion
	 *            the flagshipFocalRegion to set
	 */
	public void setFlagshipFocalRegion(FlagshipFocalRegion flagshipFocalRegion) {
		this.flagshipFocalRegion = flagshipFocalRegion;
	}

	/**
	 * @return the cluster
	 */
	public String getCluster() {
		return cluster;
	}

	/**
	 * @param cluster the cluster to set
	 */
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	/**
	 * @return the outputCode
	 */
	public String getOutputCode() {
		return outputCode;
	}

	/**
	 * @param outputCode the outputCode to set
	 */
	public void setOutputCode(String outputCode) {
		this.outputCode = outputCode;
	}

	/**
	 * @return the output
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(String output) {
		this.output = output;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            the activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project
	 *            the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the delCode
	 */
	public String getDelCode() {
		return delCode;
	}

	/**
	 * @param delCode
	 *            the delCode to set
	 */
	public void setDelCode(String delCode) {
		this.delCode = delCode;
	}

	/**
	 * @return the deliverable
	 */
	public String getDeliverable() {
		return deliverable;
	}

	/**
	 * @param deliverable
	 *            the deliverable to set
	 */
	public void setDeliverable(String deliverable) {
		this.deliverable = deliverable;
	}
}