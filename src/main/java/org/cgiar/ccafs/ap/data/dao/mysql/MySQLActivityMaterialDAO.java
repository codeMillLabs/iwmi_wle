/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityMaterialDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLActivityMaterialDAO implements ActivityMaterialDAO {

	// Looging
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityMaterialDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLActivityMaterialDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public boolean deleteActivityMaterials(int activityID) {
		LOG.debug(">> deleteActivityMaterials(activityID={})", activityID);
		boolean deleted = false;
		String query = "DELETE FROM activity_materials WHERE activity_id = ?";
		Object[] values = new Object[1];
		values[0] = activityID;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteActivityMaterials() > There was a problem deleting the materials related to the activity {}. \n{}",
						activityID, query);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteActivityMaterials() > There was an error deleting the materials related to the activity {}",
					activityID, e);
		}
		LOG.debug("<< deleteActivityMaterials():{}", deleted);
		return deleted;
	}

	@Override
	public List<Map<String, String>> getActivityMaterials(int activityID) {
		LOG.debug(">> getActivityMaterials(activityID={})", activityID);
		List<Map<String, String>> activityMaterialsDataList = new ArrayList<>();
		String query = " SELECT id, mtype, other_description, brief_description, target_audience, web_link"
				+ "  FROM activity_materials WHERE activity_id = " + activityID;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> activityMaterialsData = new HashMap<>();
				activityMaterialsData.put("id", rs.getString("id"));
				activityMaterialsData.put("mtype", rs.getString("mtype"));
				activityMaterialsData.put("other_description",
						rs.getString("other_description"));
				activityMaterialsData.put("brief_description",
						rs.getString("brief_description"));
				activityMaterialsData.put("target_audience",
						rs.getString("target_audience"));
				activityMaterialsData.put("web_link",
						rs.getString("web_link"));

				activityMaterialsDataList.add(activityMaterialsData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivityMaterials() > There was an error getting the activity materials for activity {}",
					activityID, e);
		}
		LOG.debug(
				"<< getActivityMaterials():activityMaterialsDataList.size={}",
				activityMaterialsDataList.size());
		return activityMaterialsDataList;
	}

	@Override
	public int saveActivityMaterials(Map<String, String> materials,
			int activityID) {
		LOG.debug(">> saveActivityMaterials(materials={}, activityID={})",
				materials, activityID);
		boolean saved = false;
		String query = " INSERT INTO activity_materials "
				+ "( id,  activity_id, mtype, other_description, brief_description, target_audience, web_link)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?) "
				+ " ON DUPLICATE KEY UPDATE mtype = VALUES(mtype), other_description = VAlUES(other_description), "
				+ " activity_id = VALUES(activity_id), brief_description = VALUES(brief_description),"
				+ " target_audience = VALUES(target_audience), web_link = VALUES(web_link)";

		Object[] values = new Object[7];
		values[0] = materials.get("id");
		values[1] = activityID;
		values[2] = materials.get("mtype");
		values[3] = materials.get("other_description");
		values[4] = materials.get("brief_description");
		values[5] = materials.get("target_audience");
		values[6] = materials.get("web_link");

		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);

			return rows;
		} catch (SQLException e) {
			LOG.error(
					"-- saveActivityMaterials() > There was an error saving materials for activity {}.",
					activityID);
		}

		LOG.debug("<< saveActivityMaterials():{}", saved);
		return -1;
	}

}
