/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ActivityObjective implements Difference<ActivityObjective>
{

    private int id;
    private String description;
    private String outcomeType;
    private String researchUser;
    private List<String> researchUserRole;
    private String researchOutcomeChanges;
    private String genderDescription;
    private String intermediaries;
    private String capacityBuilding;
    private String progress;

    public String getDescription()
    {
        return description;
    }

    public int getId()
    {
        return id;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getOutcomeType()
    {
        return outcomeType;
    }

    public void setOutcomeType(String inOutcomeType)
    {
        this.outcomeType = inOutcomeType;
    }

    public String getResearchUser()
    {
        return researchUser;
    }

    public void setResearchUser(String researchUser)
    {
        this.researchUser = researchUser;
    }

    public List<String> getResearchUserRole()
    {
        return researchUserRole;
    }

    public void setResearchUserRole(List<String> researchUserRole)
    {
        this.researchUserRole = researchUserRole;
    }

    public String getResearchOutcomeChanges()
    {
        return researchOutcomeChanges;
    }

    public void setResearchOutcomeChanges(String researchOutcomeChnages)
    {
        this.researchOutcomeChanges = researchOutcomeChnages;
    }

    public String getGenderDescription()
    {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription)
    {
        this.genderDescription = genderDescription;
    }

    public String getIntermediaries()
    {
        return intermediaries;
    }

    public void setIntermediaries(String intermediaries)
    {
        this.intermediaries = intermediaries;
    }

    public String getCapacityBuilding()
    {
        return capacityBuilding;
    }

    public void setCapacityBuilding(String capacityBuilding)
    {
        this.capacityBuilding = capacityBuilding;
    }

    public String getProgress()
    {
        return progress;
    }

    public void setProgress(String progress)
    {
        this.progress = progress;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    private static String TYPE = "Outcome Type";
    private static String DESC = "Description";
    private static String RESEARCH_USERS = "Research Users";
    private static String APP_RESEARCH = "Application of Research";
    private static String PROJECT_OUTPUT = "Tailored Project Outputs";
    private static String GENDER_DESC = "Gender Description";
    private static String INTERMEDIARIES = "Intermediaries";
    private static String BUILDING = "Capacity Building";
    
	@Override
	public DiffReport diff(ActivityObjective a) {
		DiffReport report = new DiffReport();

		if (isNotEqual(this.outcomeType, a.outcomeType)) {
			report.append(TYPE, this.outcomeType, a.outcomeType);
		}
		if (isNotEqual(this.description, a.description)) {
			report.append(DESC, "" + this.description, "" + a.description);
		}
		if (isNotEqual(this.researchUser, a.researchUser)) {
			report.append(RESEARCH_USERS, this.researchUser, a.researchUser);
		}
		if (this.researchUserRole != null &&  a.researchUserRole != null) {
			if(this.researchUserRole.size() != a.researchUserRole.size()) {
				report.append(APP_RESEARCH, String.valueOf(this.researchUserRole), String.valueOf(a.researchUserRole));
			}
		}
		if (isNotEqual(this.researchOutcomeChanges, a.researchOutcomeChanges)) {
			report.append(PROJECT_OUTPUT, this.researchOutcomeChanges,
					a.researchOutcomeChanges);
		}
		if (isNotEqual(this.genderDescription, a.genderDescription)) {
			report.append(GENDER_DESC, this.genderDescription, a.genderDescription);
		}
		if (isNotEqual(this.intermediaries, a.intermediaries)) {
			report.append(INTERMEDIARIES, this.intermediaries,
					a.intermediaries);
		}
		if (isNotEqual(this.capacityBuilding, a.capacityBuilding)) {
			report.append(BUILDING, this.capacityBuilding,
					a.capacityBuilding);
		}
		
		return report;
	}
}
