/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.PartnerDAO;
import org.cgiar.ccafs.ap.data.model.Partner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLPartnerDAO implements PartnerDAO {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLPartnerDAO.class);
	private DAOManager databaseManager;

	@Inject
	public MySQLPartnerDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	private static final String INSERT_PARTNER_SQL = "INSERT INTO partners (name, acronym, country_iso2, "
			+ " partner_type_id, website, status ) VALUES ( ?, ?, ?, ?, ?, ?)";

	@Override
	public int savePartner(Partner partner) {
		LOG.debug(">> savePartner()");
		try (Connection connection = databaseManager.getConnection()) {
			Object[] values = new Object[6];
			values[0] = partner.getName();
			values[1] = partner.getAcronym();
			values[2] = partner.getCountry().getId();
			values[3] = partner.getType().getId();
			values[4] = partner.getWebsite();
			values[5] = partner.getStatus().toString();

			int id = databaseManager.makeChangeSecure(connection,
					INSERT_PARTNER_SQL, values);

			LOG.info(">> Partner saved successfully, [ Id : {}]", id);
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- savePartner() > There was an error getting the save partners.",
					e);
		}
		return -1;
	}

	private static final String UPDATE_PARTNER_SQL = "UPDATE partners SET name = ?, acronym = ?, country_iso2 = ?,"
			+ " partner_type_id = ?, website = ?, status = ?  WHERE id = ?";

	public void updatePartner(Partner partner) {
		LOG.debug(">> savePartner()");
		try (Connection connection = databaseManager.getConnection()) {
			Object[] values = new Object[7];
			values[0] = partner.getName();
			values[1] = partner.getAcronym();
			values[2] = partner.getCountry().getId();
			values[3] = partner.getType().getId();
			values[4] = partner.getWebsite();
			values[5] = partner.getStatus().toString();
			values[6] = partner.getId();

			databaseManager.makeChangeSecure(connection, UPDATE_PARTNER_SQL,
					values);

			LOG.info(">> Partner updated successfully, [ Id : {}]",
					partner.getId());
		} catch (SQLException e) {
			LOG.error(
					"-- updatePartner() > There was an error getting the update partner.",
					e);
		}
	}

	@Override
	public List<Map<String, String>> getAllPartners() {
		LOG.debug(">> getAllPartners()");
		List<Map<String, String>> partners = new ArrayList<>();
		String query = "SELECT p.id, p.acronym, CASE WHEN p.acronym IS NULL THEN CONCAT(p.name, ', ', co.name) ELSE CONCAT(p.acronym, ' - ', p.name, ', ', co.name) END as 'name', pt.id as 'partner_type_id', pt.acronym as 'partner_type_acronym' "
				+ "FROM partners p "
				+ "INNER JOIN partner_types pt ON pt.id = p.partner_type_id "
				+ "INNER JOIN countries co ON p.country_iso2 = co.iso2 "
				+ "WHERE (p.status = 'VERIFIED' OR p.status = 'ACTIVTY' ) "
				+ "ORDER BY p.name ";

		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, connection);
			while (rs.next()) {
				Map<String, String> partnerData = new HashMap<>();
				partnerData.put("id", rs.getString("id"));
				partnerData.put("acronym", rs.getString("acronym"));
				partnerData.put("name", rs.getString("name"));
				partnerData.put("partner_type_id",
						rs.getString("partner_type_id"));
				partnerData.put("partner_type_acronym",
						rs.getString("partner_type_acronym"));

				partners.add(partnerData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getAllPartners() > There was an error getting the list of partners.",
					e);
		}
		LOG.debug("<< getAllPartners():partners.size={}", partners.size());
		return partners;
	}

	@Override
	public Map<String, String> getPartner(int id) {
		LOG.debug(">> getPartner(id={})", id);
		Map<String, String> partnerData = new HashMap<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT p.id, p.acronym, p.name as 'name', p.city, p.website, ");
		query.append("pt.id as 'partner_type_id', pt.acronym as 'partner_type_acronym', pt.name as 'partner_type_name', ");
		query.append("co.iso2 as 'country_iso2', co.name as 'country_name', re.id as 'region_id', ");
		query.append("re.name as 'region_name'");
		query.append("FROM partners p ");
		query.append("INNER JOIN partner_types pt ON pt.id = p.partner_type_id ");
		query.append("INNER JOIN countries co ON p.country_iso2 = co.iso2 ");
		query.append("INNER JOIN regions re ON co.region_id = re.id ");
		query.append("WHERE p.id=" + id + " ");
		query.append("GROUP BY p.id ");
		query.append("ORDER BY p.id ");

		LOG.info(query.toString());

		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				partnerData.put("id", rs.getString("id"));
				partnerData.put("acronym", rs.getString("acronym"));
				partnerData.put("name", rs.getString("name"));
				partnerData.put("city", rs.getString("city"));
				partnerData.put("website", rs.getString("website"));
				partnerData.put("partner_type_id",
						rs.getString("partner_type_id"));
				partnerData.put("partner_type_acronym",
						rs.getString("partner_type_acronym"));
				partnerData.put("partner_type_name",
						rs.getString("partner_type_name"));
				partnerData.put("country_iso2", rs.getString("country_iso2"));
				partnerData.put("country_name", rs.getString("country_name"));
				partnerData.put("region_id", rs.getString("region_id"));
				partnerData.put("region_name", rs.getString("region_name"));
				break;
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPartner() > There was an error getting the partner.",
					e);
		}
		LOG.debug("<< getPartner():{}", partnerData);
		return partnerData;
	}

	@Override
	public List<Map<String, String>> getPartnersByFilter(String countryID,
			String partnerTypeID) {
		LOG.debug(">> getPartnersByFilter(countryID='{}', partnerTypeID='{}')",
				countryID, partnerTypeID);
		List<Map<String, String>> partners = new ArrayList<>();
		String query = "SELECT p.id, p.acronym, CASE WHEN p.acronym IS NULL THEN CONCAT(p.name, ', ', co.name) ELSE CONCAT(p.acronym, ' - ', p.name, ', ', co.name) END as 'name', pt.id as 'partner_type_id', pt.acronym as 'partner_type_acronym' "
				+ "FROM partners p "
				+ "INNER JOIN partner_types pt ON pt.id = p.partner_type_id "
				+ "INNER JOIN countries co ON p.country_iso2 = co.iso2 "
				+ "WHERE (p.status = 'VERIFIED' OR p.status = 'ACTIVTY' ) ";

		if (!countryID.isEmpty() || !partnerTypeID.isEmpty()) {
			query += " AND ";

			// Add the conditions if exists
			query += (!countryID.isEmpty()) ? "p.country_iso2 = '" + countryID
					+ "' " : " 1 ";
			query += (!partnerTypeID.isEmpty()) ? "AND p.partner_type_id = '"
					+ partnerTypeID + "' " : "";
		}

		query += "ORDER BY p.name";
		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, connection);
			while (rs.next()) {
				Map<String, String> partnerData = new HashMap<>();
				partnerData.put("id", rs.getString("id"));
				partnerData.put("acronym", rs.getString("acronym"));
				partnerData.put("name", rs.getString("name"));
				partnerData.put("partner_type_id",
						rs.getString("partner_type_id"));
				partnerData.put("partner_type_acronym",
						rs.getString("partner_type_acronym"));

				partners.add(partnerData);
			}
			rs.close();
		} catch (SQLException e) {
			Object[] errorParams = { countryID, partnerTypeID, e };
			LOG.error(
					"-- getPartnersByFilter() > There was an error getting the partners list for country '{}' and type '{}'",
					errorParams);
		}

		LOG.debug("<< getPartnersByFilter():partners.size={}", partners.size());
		return partners;
	}

	@Override
	public List<Map<String, String>> getPartnersForXML() {
		LOG.debug(">> getPartnersForXML()");
		List<Map<String, String>> partners = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT p.id, p.acronym, p.name as 'name', p.city, p.website, ");
		query.append("pt.id as 'partner_type_id', pt.acronym as 'partner_type_acronym', pt.name as 'partner_type_name', ");
		query.append("co.iso2 as 'country_iso2', co.name as 'country_name', re.id as 'region_id', ");
		query.append("re.name as 'region_name'");
		query.append("FROM partners p ");
		query.append("INNER JOIN partner_types pt ON pt.id = p.partner_type_id ");
		query.append("INNER JOIN countries co ON p.country_iso2 = co.iso2 ");
		query.append("INNER JOIN regions re ON co.region_id = re.id ");
		query.append("WHERE (p.status = 'VERIFIED' OR p.status = 'ACTIVTY' ) ");
		query.append("GROUP BY p.id ");
		query.append("ORDER BY p.id ");

		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> partnerData = new HashMap<>();
				partnerData.put("id", rs.getString("id"));
				partnerData.put("acronym", rs.getString("acronym"));
				partnerData.put("name", rs.getString("name"));
				partnerData.put("city", rs.getString("city"));
				partnerData.put("website", rs.getString("website"));
				partnerData.put("partner_type_id",
						rs.getString("partner_type_id"));
				partnerData.put("partner_type_acronym",
						rs.getString("partner_type_acronym"));
				partnerData.put("partner_type_name",
						rs.getString("partner_type_name"));
				partnerData.put("country_iso2", rs.getString("country_iso2"));
				partnerData.put("country_name", rs.getString("country_name"));
				partnerData.put("region_id", rs.getString("region_id"));
				partnerData.put("region_name", rs.getString("region_name"));
				partners.add(partnerData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPartnersForXML() > There was an error getting the list of partners.",
					e);
		}
		LOG.debug("<< getPartnersForXML():partners.size={}", partners.size());
		return partners;
	}

	@Override
	public List<Map<String, String>> getPartnersList(int activityID) {
		LOG.debug(">> getPartnersList(activityID={})", activityID);
		List<Map<String, String>> statusList = new ArrayList<>();
		String query = "SELECT pa.id, pa.name, pa.acronym, pa.city, co.iso2 as 'country_id', co.name as 'country_name', "
				+ "ap.contact_name, ap.contact_email, pt.id as 'partner_type_id', pt.name as 'partner_type_name' "
				+ "FROM `partners` pa INNER JOIN countries co ON pa.country_iso2 = co.iso2 "
				+ "INNER JOIN activity_partners ap ON ap.partner_id = pa.id INNER JOIN  partner_types pt ON pa.partner_type_id = pt.id "
				+ "WHERE ( pa.status = 'VERIFIED' OR pa.status = 'ACTIVTY' ) AND ap.activity_id="
				+ activityID;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> statusData = new HashMap<>();
				statusData.put("id", rs.getString("id"));
				statusData.put("name", rs.getString("name"));
				statusData.put("acronym", rs.getString("acronym"));
				statusData.put("city", rs.getString("city"));
				statusData.put("country_id", rs.getString("country_id"));
				statusData.put("country_name", rs.getString("country_name"));
				statusData.put("contact_name", rs.getString("contact_name"));
				statusData.put("contact_email", rs.getString("contact_email"));
				statusData.put("partner_type_id",
						rs.getString("partner_type_id"));
				statusData.put("partner_type_name",
						rs.getString("partner_type_name"));
				statusList.add(statusData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPartnersList() > There was an error getting the partners list for activity {}",
					activityID, e);
			return null;
		}

		if (statusList.size() == 0) {
			return null;
		}

		LOG.debug("<< getPartnersList():statusList.size={}", statusList.size());
		return statusList;
	}

	@Override
	public List<Map<String, String>> getAllPatnersForReport() {
		LOG.debug(">> getAllPatnersForReport(activityID={})");
		List<Map<String, String>> partnerList = new ArrayList<>();
		String query = "SELECT "
				+ "ptnrs.name as name,"
				+ "ptnrs.acronym as acronym,"
				+ "ctrs.name as country,"
				+ "ptypes.name as type,"
				+ "aptnrs.clasification as classification,"
				+ "IFNULL(aptnrs.contact_name,\"\") as contact,"
				+ "IFNULL(aptnrs.contact_email,\"\") as email"
				+ " FROM partners ptnrs"
				+ " INNER JOIN countries ctrs ON ctrs.iso2 = ptnrs.country_iso2"
				+ " INNER JOIN  partner_types ptypes ON ptypes.id = ptnrs.partner_type_id"
				+ " INNER JOIN  activity_partners aptnrs ON aptnrs.partner_id = ptnrs.id";

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> statusData = new HashMap<>();
				statusData.put("name", rs.getString("name"));
				statusData.put("acronym", rs.getString("acronym"));
				statusData.put("country", rs.getString("country"));
				statusData.put("type", rs.getString("type"));
				statusData
						.put("classification", rs.getString("classification"));

				String contactName = rs.getString("contact");
				String contactEmail = rs.getString("email");
				if (!contactEmail.equals("")) {
					contactName = contactName + "(" + contactEmail + ")";
				}
				statusData.put("contact", contactName);
				partnerList.add(statusData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getAllPatnersForReport() > There was an error getting the partners list for activity {}",
					e);
			return null;
		}

		if (partnerList.size() == 0) {
			return null;
		}

		LOG.debug("<< getAllPatnersForReport():partnerList.size={}",
				partnerList.size());
		return partnerList;
	}

}