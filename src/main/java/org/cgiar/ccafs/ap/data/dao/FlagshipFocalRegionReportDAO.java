/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLFlagshipFocalRegionReportDAO;

import com.google.inject.ImplementedBy;

@ImplementedBy(MySQLFlagshipFocalRegionReportDAO.class)
public interface FlagshipFocalRegionReportDAO {

	/**
	 * Delete flagship focal regions report.
	 * 
	 * @param id
	 *            - report id
	 * @return true if was successfully deleted. False otherwise
	 */
	public boolean deleteFlagshipFocalRegionReport(int id);

	/**
	 * Delete flagship clusters.
	 * 
	 * @param id
	 *            - report id
	 * @return true if was successfully deleted. False otherwise
	 */
	public boolean deleteFlagshipClusters(int reportId);
	
	/**
	 * Delete cluster details.
	 * 
	 * @param id
	 *            - cluster id
	 * @return true if was successfully deleted. False otherwise
	 */
	public boolean deleteClusterDetails(int clusterId);
	

	/**
	 * Delete output activities.
	 * 
	 * @param detailId
	 *            - detail id
	 * @return true if was successfully deleted. False otherwise
	 */
	public boolean deleteOutputActivities(int detailId);
	
	/**
	 * Delete cluster outputs.
	 * 
	 * @param outputActivityId
	 *            - output activity id
	 * @return true if was successfully deleted. False otherwise
	 */
	public boolean deleteClusterOutputs(int outputActivityId);

	/**
	 * Get the flagship/focal regions report
	 * 
	 * @param flagshipId flagship id
	 * @param year year
	 * @param period period (mid/end)
	 * 
	 * @return a list of flagship/focal regions report
	 */
	public Map<String, String> getFlagshipFocalRegionReport(int flagshipId,
			int year, String period);
	
	
	/**
	 * Get the flagship/focal regions report
	 * 
	 * @param reportId report id
	 * 
	 * @return a list of flagship/focal regions report
	 */
	public Map<String, String> getFlagshipFocalRegionReport(int reportId);

	/**
	 * Get the flagship clusters for given flagship / focal regions report.
	 * 
	 * @param reportId report id
	 * 
	 * @return a list of flagship clusters
	 */
	public List<Map<String, String>> getFlagshipClusters(int reportId);
	
	/**
	 * Get the cluster outputs detail for given cluster id.
	 * 
	 * @param clusterId cluster id
	 * 
	 * @return a list of cluster outputs
	 */
	public List<Map<String, String>> getClusterDetails(int clusterId);
	
	/**
	 * Get the cluster outputs activities for given cluster id.
	 * 
	 * @param detailId detail id
	 * 
	 * @return a list of cluster outputs
	 */
	public List<Map<String, String>> getOutputActivities(int detailId);

	/**
	 * Get the cluster outputs for given cluster detail id.
	 * 
	 * @param outputActivityId cluster output activity id
	 * 
	 * @return a list of cluster outputs
	 */
	public List<Map<String, String>> getClusterOutputs(int outputActivityId);

	/**
	 * save flagship focal region report.
	 * 
	 * @param keyValues
	 *            of report
	 */
	public int saveFlagshipFocalRegionReport(Map<String, String> keyValues);
	
	
	/**
	 * Update flagship focal region report.
	 * 
	 * @param id report id
	 * @param keyValues values
	 * 
	 * @return id
	 */
	public boolean updateFlagshipFocalRegionReport(int id, Map<String, String> keyValues);

	/**
	 * Save the flagship cluster related to given report.
	 * 
	 * @param keyValues
	 *            - The data to save into the DAO
	 * @param reportId
	 *            - report id
	 * @return true if was successfully saved. False otherwise
	 */
	public int saveFlagshipCluster(Map<String, String> keyValues, int reportId);
	
	
	/**
	 * Save the cluster output related to given cluster.
	 * 
	 * @param keyValues
	 *            - The data to save into the DAO
	 * @param clusterId
	 *            - cluster id
	 * @return true if was successfully saved. False otherwise
	 */
	public int saveClusterDetails(Map<String, String> keyValues, int clusterId);
	
	
	/**
	 * Save the cluster output related to given cluster.
	 * 
	 * @param keyValues
	 *            - The data to save into the DAO
	 * @param clusterId
	 *            - detailId id
	 * @return true if was successfully saved. False otherwise
	 */
	public int saveOutputActivities(Map<String, String> keyValues, int detailId);
	
	
	/**
	 * Save the cluster output related to given cluster.
	 * 
	 * @param keyValues
	 *            - The data to save into the DAO
	 * @param detailId
	 *            - cluster detail id
	 * @return true if was successfully saved. False otherwise
	 */
	public int saveClusterOutputs(Map<String, String> keyValues, int outputActivityId);
	
	/**
	 * Update the state of the report.
	 * 
	 * @param reportId report id
	 * @param newState new state
	 * 
	 * @return updated or not boolean value
	 */
	boolean updateState(int reportId, String newState);
	
	/**
	 * Update revision of given report.
	 * 
	 * @param reportId report id
	 * 
	 * @return updated or not boolean value
	 */
	public boolean updateRevision(int reportId);
	
	/**
	 * Get comments for given report id.
	 * 
	 * @param reportId report id
	 * 
	 * @return list of comments
	 */
	public List<Map<String, String>> getComments(int reportId);
	
	/**
	 * Get comments for given report id.
	 * 
	 * @param reportId report id
	 * @param revision revision
	 * 
	 * @return list of comments
	 */
	public List<Map<String, String>> getComments(int reportId, int revision);
	
	/**
	 * Save comment.
	 * 
	 * @param reportId report id
	 * @param keyValues values to be save
	 * 
	 * @return comment id
	 */
	public int saveComment(int reportId, Map<String, String> keyValues);
}
