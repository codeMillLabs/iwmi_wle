/**
 * 
 */
package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityWLEDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.model.ActivityWLEDevelopment;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * 
 * @author Amila Silva
 *
 */
public class MySQLActivityWLEDAO implements ActivityWLEDAO {

	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityKeywordDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLActivityWLEDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public List<ActivityWLEDevelopment> getWLEList(int activityID) {
		List<ActivityWLEDevelopment> welDevs = new ArrayList<ActivityWLEDevelopment>();
		final String WLE_LIST_SQL = " SELECT aw.id , aw.activity_id, w.id, w.display_name, w.value "
				+ " FROM iwmi_activity_wle aw  "
				+ " LEFT JOIN iwmi_wle_developments w ON aw.wle_id = w.id "
				+ " WHERE aw.activity_id = '" + activityID + "' ";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(WLE_LIST_SQL, con);
			while (rs.next()) {
				ActivityWLEDevelopment actWLEDevelopment = new ActivityWLEDevelopment();
				DataLookup dataLookup = new DataLookup();
				actWLEDevelopment.setId(rs.getInt("aw.id"));
				actWLEDevelopment.setActivityId(rs.getInt("aw.activity_id"));
				actWLEDevelopment.setWle(dataLookup);

				dataLookup.setId(rs.getInt("w.id"));
				dataLookup.setDisplayName(rs.getString("w.display_name"));
				dataLookup.setValue(rs.getString("w.value"));

				welDevs.add(actWLEDevelopment);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getWLEList() > There was an error getting activity WLE for activity {}",
					activityID, e);
		}
		LOG.debug("<< getWLEList():{}", welDevs);
		return welDevs;
	}

	@Override
	public boolean removeActivityWLE(int activityID) {
		LOG.debug(">> removeActivityWLE(activityID={})", activityID);

		boolean problem = false;
		String removeQuery = "DELETE FROM iwmi_activity_wle WHERE activity_id = "
				+ activityID;
		try (Connection connection = databaseManager.getConnection()) {
			int rows = databaseManager.makeChange(removeQuery, connection);
			if (rows < 0) {
				LOG.warn(
						"-- removeActivityWLE() > It was tried delete activity WLE from database for activity {} but failed.",
						activityID);
				problem = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- removeActivityWLE() > There was an error deleting the activity WLE related to the activity {}.",
					activityID, e);
		}

		LOG.debug("<< removeActivityWLE():{}", !problem);
		return !problem;
	}

	@Override
	public boolean saveWLE(ActivityWLEDevelopment wleDevelopment, int activistyId) {
		LOG.debug(">> saveWLE(wleDevelopment={})", wleDevelopment);
	    boolean saved = false;
	    String query = "INSERT INTO iwmi_activity_wle (id, activity_id, wle_id)"
	    		+ "  VALUES (?, ?, ?)  ";
	    Object[] values = new Object[3];
	    values[0] = wleDevelopment.getId();
	    values[1] = activistyId;
	    values[2] = wleDevelopment.getWle().getId();

	    try (Connection con = databaseManager.getConnection()) {
	      int rows = databaseManager.makeChangeSecure(con, query, values);
	      if (rows < 0) {
	        LOG
	          .warn("-- saveWLE() > There was an error saving the WLE. \n Query: {}. \n Values: {}", query, values);
	      } else {
	        saved = true;
	      }
	    } catch (SQLException e) {
	      LOG.error("-- saveWLE() > There was an error saving the WLE.", e);
	    }

	    LOG.debug("<< saveKeyword():{}", saved);
	    return saved;
	}

}
