/*
 * FILENAME
 *     MySQLReviewerDAO.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.ReviewerDAO;
import org.cgiar.ccafs.ap.data.model.Reviewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
/**
 * <p>
 * MySQLReviewerDAO.
 * </p>
 *
 *
 * @author Amila Silva
 *
 * @version $Id$
 *
 */
public class MySQLReviewerDAO implements ReviewerDAO
{
    // Loggin
    private static final Logger LOG = LoggerFactory.getLogger(MySQLReviewerDAO.class);
    private DAOManager databaseManager;

    @Inject
    public MySQLReviewerDAO(DAOManager databaseManager)
    {
        this.databaseManager = databaseManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ReviewerDAO#getReviewersByCluster(java.lang.String)
     */
    @Override
    public List<Reviewer> getReviewersByCluster(String clusterName)
    {
        LOG.debug(">> getReviewersByCluster()");
        final String REVIWERS_BY_CLUSTER_SQL =
            "SELECT id, name, flagship, cluster, details" + " FROM IWMI_REVIEWERS WHERE cluster like '" + clusterName
                + "' ";
        List<Reviewer> reviwers = new ArrayList<Reviewer>();

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_BY_CLUSTER_SQL, con);
            while (rs.next())
            {
                Reviewer reviewer = new Reviewer();
                reviewer.setId(rs.getLong("id"));
                reviewer.setName(rs.getString("name"));
                reviewer.setFlagship(rs.getString("flagship"));
                reviewer.setCluster(rs.getString("cluster"));
                reviewer.setDetails(rs.getString("detail"));
                reviwers.add(reviewer);
            }
            rs.close();
        }
        catch (SQLException e)
        {
            LOG.error("-- getReviewersByCluster() > There was an exception trying to load the reviewers list.");
        }

        LOG.debug("<< getReviewersByCluster():reviewers.size={}", reviwers.size());
        return reviwers;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ReviewerDAO#getReviewersById(long)
     */
    @Override
    public Reviewer getReviewersById(long id)
    {
        LOG.debug(">> getReviewersById()");
        final String REVIWERS_BY_ID_SQL =
            "SELECT id, name, flagship, cluster, details" + " FROM IWMI_REVIEWERS WHERE id = " + id + " ";

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_BY_ID_SQL, con);
            if (rs.next())
            {
                Reviewer reviewer = new Reviewer();
                reviewer.setId(rs.getLong("id"));
                reviewer.setName(rs.getString("name"));
                reviewer.setFlagship(rs.getString("flagship"));
                reviewer.setCluster(rs.getString("cluster"));
                reviewer.setDetails(rs.getString("detail"));

                LOG.debug("<< getReviewersById(): reviewer ", reviewer);
                return reviewer;
            }
            rs.close();
        }
        catch (SQLException e)
        {
            LOG.error("-- getReviewersById() > There was an exception trying to load the reviewer By Id.");
        }

        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ReviewerDAO#getAllReviwers()
     */
    @Override
    public List<Reviewer> getAllReviwers()
    {
        LOG.debug(">> getAllReviwers()");
        final String REVIWERS_SQL = "SELECT id, name, flagship, cluster, details" + " FROM IWMI_REVIEWERS ";
        List<Reviewer> reviwers = new ArrayList<Reviewer>();

        try (Connection con = databaseManager.getConnection())
        {
            ResultSet rs = databaseManager.makeQuery(REVIWERS_SQL, con);
            while (rs.next())
            {
                Reviewer reviewer = new Reviewer();
                reviewer.setId(rs.getLong("id"));
                reviewer.setName(rs.getString("name"));
                reviewer.setFlagship(rs.getString("flagship"));
                reviewer.setCluster(rs.getString("cluster"));
                reviewer.setDetails(rs.getString("detail"));
                reviwers.add(reviewer);
            }
            rs.close();
        }
        catch (SQLException e)
        {
            LOG.error("-- getAllReviwers() > There was an exception trying to load the reviewers list.");
        }

        LOG.debug("<< getAllReviwers(): reviewers.size={}", reviwers.size());
        return reviwers;
    }

    private static final String REVIEWERS_INSERT_SQL = "INSERT INTO IWMI_REVIEWERS "
        + " (name, flagship, cluster, detail) " + " VALUES (?, ?, ? ,?)";

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ReviewerDAO#save(org.cgiar.ccafs.ap.data.model.Reviewer)
     */
    @Override
    public void save(Reviewer reviwer)
    {
        LOG.debug(">> save reviewer( Reviewer ={})", reviwer);

        try (Connection con = databaseManager.getConnection())
        {
            Object[] values = new Object[] {
                reviwer.getName(), reviwer.getFlagship(), reviwer.getCluster(), reviwer.getDetails()
            };

            int reviewId = databaseManager.makeChangeSecure(con, REVIEWERS_INSERT_SQL, values);

            if (reviewId < 0)
            {
                LOG.error("There was a problem saving the reviewer data.");
            }

            LOG.debug("<< save(): Reviewer Id : {},", reviewId);
        }
        catch (Exception e)
        {
            LOG.error("-- save() > There was an error saving the reviewer data.");
            LOG.error("Error: ", e);
        }
    }

    private static final String REVIEWERS_UPDATE_SQL = "UPDATE IWMI_REVIEWERS "
        + " SET name =?, flagship =?, cluster = ?, detail =?  WHERE id =?";

    /**
     * {@inheritDoc}
     *
     * @see org.cgiar.ccafs.ap.data.dao.ReviewerDAO#update(org.cgiar.ccafs.ap.data.model.Reviewer)
     */
    @Override
    public void update(Reviewer reviwer)
    {
        LOG.debug(">> save reviewer( Reviewer ={})", reviwer);

        try (Connection con = databaseManager.getConnection())
        {
            Object[] values = new Object[] {
                reviwer.getName(), reviwer.getFlagship(), reviwer.getCluster(), reviwer.getDetails(), reviwer.getId()
            };

            int reviewId = databaseManager.makeChangeSecure(con, REVIEWERS_UPDATE_SQL, values);

            if (reviewId < 0)
            {
                LOG.error("There was a problem updating the reviewer data.");
            }

            LOG.debug("<< updating(): Reviewer Id : {},", reviewId);
        }
        catch (Exception e)
        {
            LOG.error("-- updating() > There was an error updating the budget data.");
            LOG.error("Error: ", e);
        }
    }

}
