/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.model;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * An Enum class to keep static information
 * on LookupData.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public enum LookupInfoEnum {
	
	ACTIVITY_CLUSTER("iwmi_activity_cluster", null, "id", "display_name", "value", "parent_id")
	,FLAGSHIP_SRP("iwmi_flagship_srp", ACTIVITY_CLUSTER, "id", "display_name", "value", "parent_id")
	,CLASIFICATION("iwmi_clasification", null, "id", "display_name", "value", "parent_id")
	,LEADER_CENTER("iwmi_leader_center", null, "id", "display_name", "value", "parent_id")
	,WLE_DEVELOPMENTS("iwmi_wle_developments", null, "id", "display_name", "value", "parent_id")
	,OUTCOME_USER_ROLES("iwmi_outcome_user_roles", null, "id", "display_name", "value", "parent_id")
	,TYPE_OF_DELIVERABLE("iwmi_type_of_deliverable", null, "id", "display_name", "value", "parent_id")
	,TYPE_OF_OUTREACH("iwmi_type_of_outreach", null, "id", "display_name", "value", "parent_id"),
	INDICATOR_SUB("iwmi_indicator_sub", null, "id", "display_name", "value", "parent_id"),
	INDICATOR_MAIN("iwmi_indicator_main", INDICATOR_SUB, "id", "display_name", "value", "parent_id");
	
	private String tableName;
	private List<String> columns;
	private LookupInfoEnum subLevel;
	
	private LookupInfoEnum(String tableName, LookupInfoEnum subLevel, String... columns) {
	
		this.tableName = tableName;
		this.subLevel = subLevel;
		if(columns != null && columns.length > 0) {
		   this.columns = Arrays.asList(columns);
		}
	}
	
	public boolean hasSubLevel() {
		return this.subLevel == null ? false : true;
	}

	public LookupInfoEnum getSubLevel() {
		return subLevel;
	}

	public String getTableName() {
		return tableName;
	}

	public List<String> getColumns() {
		return columns;
	}
}
