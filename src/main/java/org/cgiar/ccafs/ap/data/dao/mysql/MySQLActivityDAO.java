/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.getJSON;
import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.populateObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRatingComment;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.SectionReview;
import org.cgiar.ccafs.ap.data.model.SectionReview.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLActivityDAO implements ActivityDAO {

	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityDAO.class);
	private DAOManager databaseManager;

	@Inject
	public MySQLActivityDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public List<Map<String, String>> getActivities(int year,
			ActivityStatus... activityStatus) {
		LOG.debug(">> getActivities(year={} )", year);
		List<Map<String, String>> activities = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT a.id, a.activity_id, a.revision_id, a.status, a.title, a.start_date, a.end_date, a.description, ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by, ");
		query.append(" (av.activity_id IS NOT NULL) as 'is_validated', m.id as 'milestone_id', ");
		query.append(" m.code as 'milestone_code', th.id as 'theme_id', th.code as 'theme_code', ");
		query.append(" al.id as 'leader_id', al.acronym as 'leader_acronym', al.name as 'leader_name', ");
		query.append(" rating.mid_yr_rating_json FROM activities a ");
		query.append("LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append("LEFT JOIN milestones m ON a.milestone_id = m.id ");
		query.append("LEFT JOIN outputs op ON m.output_id = op.id ");
		query.append("LEFT JOIN objectives ob ON op.objective_id = ob.id ");
		query.append("LEFT JOIN themes th ON ob.theme_id = th.id ");
		query.append("LEFT JOIN logframes l ON th.logframe_id = l.id ");
		query.append("LEFT JOIN activity_review_ratings rating ON a.activity_id = rating.activity_id ");
		query.append("INNER JOIN activity_leaders al ON a.activity_leader_id = al.id ");
		query.append("WHERE a.is_deleted = '0' AND a.year = ");
		query.append(year);

		if (activityStatus != null && activityStatus.length > 0) {
			boolean isFirst = true;
			query.append(" AND ");
			query.append(" ( ");
			for (ActivityStatus status : activityStatus) {
				if (!isFirst) {
					query.append(" OR ");
				}
				query.append("  a.status = '" + status.getName() + "' ");
			}
			query.append(" ) ");
		}

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(), con);
			while (rs.next()) {
				Map<String, String> activity = new HashMap<>();
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("revision_id", rs.getString("revision_id"));
				activity.put("status", rs.getString("status"));
				activity.put("title", rs.getString("title"));
				activity.put("year", rs.getString("year"));
				activity.put("start_date", rs.getString("start_date"));
				activity.put("end_date", rs.getString("end_date"));
				activity.put("description", rs.getString("description"));
				activity.put("is_validated", rs.getString("is_validated"));
				activity.put("project_name", rs.getString("project_name"));
				activity.put("gender_research", rs.getString("gender_research"));
				activity.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activity.put("flagship", rs.getString("flagship"));
				activity.put("activity_cluster",
						rs.getString("activity_cluster"));
				activity.put("lead_center", rs.getString("lead_center"));
				activity.put("milestone_id", rs.getString("milestone_id"));
				activity.put("milestone_code", rs.getString("milestone_code"));
				activity.put("theme_id", rs.getString("theme_id"));
				activity.put("theme_code", rs.getString("theme_code"));
				activity.put("leader_id", rs.getString("leader_id"));
				activity.put("leader_acronym", rs.getString("leader_acronym"));
				activity.put("leader_name", rs.getString("leader_name"));
				activity.put("budget_total", rs.getString("budget_total"));
				activity.put("budget_window1", rs.getString("budget_window1"));
				activity.put("budget_window2", rs.getString("budget_window2"));
				activity.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activity.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activity.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activity.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activity.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activity.put("created_by", rs.getString("created_by"));
				activity.put("mid_yr_rating_json", rs.getString("mid_yr_rating_json"));
				activities.add(activity);
			}
		} catch (SQLException e) {
			LOG.error(
					"--  getActivities() > There was an error getting the activity list for year {}.",
					year, e);
			return null;
		}

		LOG.debug("<< getActivities():activities.size={}", activities.size());
		return activities;
	}

	@Override
	public List<Map<String, String>> getActivities(int year,
			int leaderTypeCode, String createdBy, String flagshipValue, ActivityStatus... activityStatus) {
		LOG.debug(">> getActivities(year={}, leaderTypeCode={})", year,
				leaderTypeCode);

		List<Map<String, String>> activities = new ArrayList<>();
		StringBuilder query = new StringBuilder();

		query.append(" SELECT distinct(a.activity_id), a.id, a.revision_id, a.status, a.title, a.start_date, a.end_date, a.description, ");
		query.append(" (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.created_by, ast.id as 'status_id', ast.name as 'status_name', ");
		query.append(" al.id as 'leader_id', al.acronym as 'leader_acronym', al.name as 'leader_name', ");
		query.append(" a.status_description, rating.mid_yr_rating_json FROM activities a ");
		query.append(" LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append(" INNER JOIN activity_leaders al ON a.activity_leader_id = al.id ");
		query.append(" LEFT JOIN activity_status ast ON a.activity_status_id = ast.id ");
		query.append(" LEFT JOIN activity_review_ratings rating ON a.activity_id = rating.activity_id ");
		query.append(" WHERE  a.is_deleted = '0' ");

		if (year != 0) {
			query.append(" AND a.year = ");
			query.append(year);
		}

		if (leaderTypeCode != 0) {
			query.append(" AND al.id = ");
			query.append(leaderTypeCode);
		}
		
		if(null != createdBy && null != flagshipValue)
		{
			query.append(" AND (a.created_by = '");
			query.append(createdBy);
			query.append("' OR ");
			query.append(" a.flagship = '");
			query.append(flagshipValue);
			query.append("') ");
		}
		else if (null != createdBy)
		{
			query.append(" AND a.created_by = '");
			query.append(createdBy);
			query.append("' ");
		}
		else if(null != flagshipValue)
		{
			query.append(" AND a.flagship = '");
			query.append(flagshipValue);
			query.append("' ");
		}

		if (activityStatus != null && activityStatus.length > 0) {
			boolean isFirst = true;
			query.append(" AND ");
			query.append(" ( ");
			for (ActivityStatus status : activityStatus) {
				if (!isFirst) {
					query.append(" OR ");
				}
				query.append("  a.status = '" + status.getName() + "' ");
				isFirst = false;
			}
			query.append(" ) ");
		}
		
		LOG.debug("Query :" + query);

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(), con);
			while (rs.next()) {
				Map<String, String> activity = new HashMap<>();
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("revision_id", rs.getString("revision_id"));
				activity.put("status", rs.getString("status"));
				activity.put("title", rs.getString("title"));
				activity.put("year", rs.getString("year"));
				activity.put("start_date", rs.getString("start_date"));
				activity.put("end_date", rs.getString("end_date"));
				activity.put("description", rs.getString("description"));
				activity.put("is_validated", rs.getString("is_validated"));
				activity.put("project_name", rs.getString("project_name"));
				activity.put("gender_research", rs.getString("gender_research"));
				activity.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activity.put("flagship", rs.getString("flagship"));
				activity.put("activity_cluster",
						rs.getString("activity_cluster"));
				activity.put("lead_center", rs.getString("lead_center"));
				activity.put("status_id", rs.getString("status_id"));
				activity.put("status_name", rs.getString("status_name"));
				activity.put("leader_id", rs.getString("leader_id"));
				activity.put("leader_acronym", rs.getString("leader_acronym"));
				activity.put("leader_name", rs.getString("leader_name"));
				activity.put("status_description",
						rs.getString("status_description"));
				activity.put("mid_yr_rating_json", rs.getString("mid_yr_rating_json"));
				activity.put("created_by", rs.getString("created_by"));
				activities.add(activity);
			}
			rs.close();
		} catch (SQLException e) {
			Object[] paramArray = { year, leaderTypeCode, e };
			LOG.error(
					"-- getActivities() >There was an error getting the activity list for year {} and activity leader {}.",
					paramArray);
		}

		LOG.debug("<< getActivities():activities.size={}", activities.size());
		return activities;
	}

	@Override
	public List<Map<String, String>> getActivitiesForRSS(int year, int limit) {
		LOG.debug(">> getActivitiesForRSS(year={}, limit={})", year, limit);

		List<Map<String, String>> activities = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT a.id, activity_id, a.revision_id, a.status, a.title, a.start_date, a.end_date, a.description, a.date_added, a.is_global, ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by, ");
		query.append("is_commissioned, m.id as 'milestone_id', m.code as 'milestone_code'");
		query.append("FROM activities a ");
		query.append("INNER JOIN activity_leaders al ON al.id = a.activity_leader_id ");
		query.append("INNER JOIN milestones m ON m.id = a.milestone_id ");
		query.append("WHERE a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" ORDER BY a.id");
		if (limit > 0) {
			query.append(" LIMIT ");
			query.append(limit);
		}
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(), con);
			while (rs.next()) {
				Map<String, String> activity = new HashMap<>();
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("revision_id", rs.getString("revision_id"));
				activity.put("status", rs.getString("status"));
				activity.put("title", rs.getString("title"));
				activity.put("year", rs.getString("year"));
				activity.put("start_date", rs.getString("start_date"));
				activity.put("end_date", rs.getString("end_date"));
				activity.put("description", rs.getString("description"));
				activity.put("is_global", rs.getString("is_global"));
				activity.put("is_commissioned", rs.getString("is_commissioned"));
				activity.put("project_name", rs.getString("project_name"));
				activity.put("gender_research", rs.getString("gender_research"));
				activity.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activity.put("flagship", rs.getString("flagship"));
				activity.put("activity_cluster",
						rs.getString("activity_cluster"));
				activity.put("lead_center", rs.getString("lead_center"));
				activity.put("milestone_id", rs.getString("milestone_id"));
				activity.put("milestone_code", rs.getString("milestone_code"));
				activity.put("date_added", rs.getString("date_added"));
				activity.put("budget_total", rs.getString("budget_total"));
				activity.put("budget_window1", rs.getString("budget_window1"));
				activity.put("budget_window2", rs.getString("budget_window2"));
				activity.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activity.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activity.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activity.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activity.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activity.put("created_by", rs.getString("created_by"));
				activities.add(activity);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivitiesForRSS() >There was an error getting the activity list for year {} with limit {}.",
					year, limit);
		}

		LOG.debug("<< getActivitiesForRSS():activities.size={}",
				activities.size());
		return activities;
	}

	@Override
	public List<Map<String, String>> getActivitiesToContinue(int year) {
		LOG.debug(">> getActivitiesToContinue(year={}) ", year);
		List<Map<String, String>> activitiesToContinue = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT a.id, a.activity_id, a.title, a.created_by  FROM activities a ");
		query.append("LEFT JOIN activities a2 ON a.id =  a2.continuous_activity_id ");
		query.append("WHERE a2.continuous_activity_id IS NULL ");
		query.append("AND  a.is_deleted = '0' AND a.year = ");
		query.append(year);

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(), con);
			while (rs.next()) {
				Map<String, String> activity = new HashMap<>();
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("title", rs.getString("title"));
				activity.put("created_by", rs.getString("created_by"));

				activitiesToContinue.add(activity);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivitiesToContinue() > There was an exception trying to get the activity list.",
					e);
		}

		LOG.debug("<< getActivitiesToContinue():activitiesToContinue.size={} ",
				activitiesToContinue.size());
		return activitiesToContinue;
	}

	@Override
	public List<Map<String, String>> getActivitiesToContinue(int year,
			int leaderID) {
		LOG.debug(">> getActivitiesToContinue(year={}, leader={}) ", year,
				leaderID);
		List<Map<String, String>> activitiesToContinue = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT a.id, a.activity_id, a.title, a.created_by FROM activities a ");
		query.append("LEFT JOIN activities a2 ON a.id =  a2.continuous_activity_id ");
		query.append("WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND a.activity_leader_id = ");
		query.append(leaderID);

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(), con);
			while (rs.next()) {
				Map<String, String> activity = new HashMap<>();
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("title", rs.getString("title"));
				activity.put("created_by", rs.getString("created_by"));

				activitiesToContinue.add(activity);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivitiesToContinue() > There was an exception trying to get the activity list.",
					e);
		}

		LOG.debug("<< getActivitiesToContinue():activitiesToContinue.size={} ",
				activitiesToContinue.size());
		return activitiesToContinue;
	}

	@Override
	public List<Map<String, String>> getActivityListByYear(int year) {
		LOG.debug(">> getActivityListByYear(year={})", year);
		List<Map<String, String>> activitiesData = new ArrayList<>();
		StringBuilder query = new StringBuilder(
				"SELECT a.id, a.title, a.activity_id, a.revision_id, a.status, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by, ");
		query.append(" GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names',");
		query.append(" GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code',");
		query.append(" al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym'");
		query.append(" FROM activities a");
		query.append(" LEFT JOIN contact_person cp ON cp.activity_id = a.id");
		query.append(" LEFT JOIN activity_validations av ON a.id = av.activity_id");
		query.append(" LEFT JOIN milestones m ON m.id = a.milestone_id");
		query.append(" INNER JOIN activity_leaders al ON al.id = a.activity_leader_id");
		query.append(" WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" GROUP BY a.id");
		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("revision_id", rs.getString("revision_id"));
				activityData.put("status", rs.getString("status"));
				activityData.put("title", rs.getString("title"));
				activityData.put("year", rs.getString("year"));
				activityData.put("is_validated", rs.getString("is_validated"));
				activityData.put("project_name", rs.getString("project_name"));
				activityData.put("gender_research",
						rs.getString("gender_research"));
				activityData
						.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activityData.put("flagship", rs.getString("flagship"));
				activityData.put("activity_cluster",
						rs.getString("activity_cluster"));
				activityData.put("lead_center", rs.getString("lead_center"));
				activityData.put("milestone_code",
						rs.getString("milestone_code"));
				activityData.put("contact_person_names",
						rs.getString("contact_person_names"));
				activityData.put("contact_person_emails",
						rs.getString("contact_person_emails"));
				activityData.put("leader_id", rs.getString("leader_id"));
				activityData.put("leader_name", rs.getString("leader_name"));
				activityData.put("leader_acronym",
						rs.getString("leader_acronym"));
				activityData.put("budget_total", rs.getString("budget_total"));
				activityData.put("budget_window1",
						rs.getString("budget_window1"));
				activityData.put("budget_window2",
						rs.getString("budget_window2"));
				activityData.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activityData.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activityData.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activityData.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activityData.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activityData.put("created_by", rs.getString("created_by"));
				activitiesData.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getActivityListByYear() > There was an error getting the list of planning activities for year {}.",
					year);
			e.printStackTrace();
		}

		LOG.debug("<< getActivityListByYear():activitiesData.size={}",
				activitiesData.size());
		return activitiesData;
	}

	@Override
	public Map<String, String> getActivityStatusInfo(int id) {
		LOG.debug(">> getActivityStatusInfo(id={})", id);
		Map<String, String> activity = new HashMap<>();
		String query = "SELECT a.id, a.activity_id, a.lead_center_based_id, a.title, a.revision_id, a.status, a.start_date, a.end_date, a.description, a.status_description, a.is_global, astatus.id as status_id, "
				+ "a.has_partners, astatus.name as status_name, a.is_commissioned, a.continuous_activity_id, a.milestone_id, "
				+ "a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, "
				+ " a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, "
				+ " a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by,  "
				+ "m.code as milestone_code, al.id as 'leader_id', al.acronym as 'leader_acronym', al.name as 'leader_name', "
				+ "g.id as 'gender_id', g.description as 'gender_description', "
				+ "(SELECT a2.activity_id FROM activities a2 WHERE a2.id = a.continuous_activity_id) as 'countinuos_activity_activityID' "
				+ "FROM activities a LEFT JOIN milestones m ON a.milestone_id = m.id "
				+ "LEFT JOIN activity_status astatus ON a.activity_status_id = astatus.id "
				+ "LEFT JOIN activity_leaders al ON a.activity_leader_id = al.id "
				+ "LEFT OUTER JOIN gender_integrations g ON g.activity_id = a.id WHERE  a.is_deleted = '0' AND a.id = "
				+ id;

		return getActivityStatusInfoCommon(String.valueOf(id), activity, query);
	}

	@Override
	public Map<String, String> getActivityStatusInfo(String id) {
		LOG.debug(">> getActivityStatusInfo(id={})", id);
		Map<String, String> activity = new HashMap<>();
		String query = "SELECT a.id, a.activity_id, a.lead_center_based_id, a.title, a.revision_id, a.status, a.start_date, a.end_date, a.description, a.status_description, a.is_global, astatus.id as status_id, "
				+ "a.has_partners, astatus.name as status_name, a.is_commissioned, a.continuous_activity_id, a.milestone_id, "
				+ "a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, "
				+ " a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, "
				+ " a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by,  "
				+ "m.code as milestone_code, al.id as 'leader_id', al.acronym as 'leader_acronym', al.name as 'leader_name', "
				+ "g.id as 'gender_id', g.description as 'gender_description', "
				+ "(SELECT a2.activity_id FROM activities a2 WHERE a2.id = a.continuous_activity_id) as 'countinuos_activity_activityID' "
				+ "FROM activities a LEFT JOIN milestones m ON a.milestone_id = m.id "
				+ "LEFT JOIN activity_status astatus ON a.activity_status_id = astatus.id "
				+ "LEFT JOIN activity_leaders al ON a.activity_leader_id = al.id "
				+ "LEFT OUTER JOIN gender_integrations g ON g.activity_id = a.id WHERE  a.is_deleted = '0' AND a.activity_id = '"
				+ id + "'";

		return getActivityStatusInfoCommon(id, activity, query);
	}
	
	private Map<String, String> getActivityStatusInfoCommon(String id,
			Map<String, String> activity, String query) {
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("lead_center_based_id",
						rs.getString("lead_center_based_id"));
				activity.put("revision_id", rs.getString("revision_id"));
				activity.put("status", rs.getString("status"));
				activity.put("title", rs.getString("title"));
				activity.put("year", rs.getString("year"));
				activity.put("start_date", rs.getString("start_date"));
				activity.put("end_date", rs.getString("end_date"));
				activity.put("description", rs.getString("description"));
				activity.put("is_global", rs.getString("is_global"));
				activity.put("status_description",
						rs.getString("status_description"));
				activity.put("is_commissioned", rs.getString("is_commissioned"));
				activity.put("has_partners", rs.getString("has_partners"));
				activity.put("project_name", rs.getString("project_name"));
				activity.put("gender_research", rs.getString("gender_research"));
				activity.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activity.put("flagship", rs.getString("flagship"));
				activity.put("activity_cluster",
						rs.getString("activity_cluster"));
				activity.put("lead_center", rs.getString("lead_center"));
				activity.put("continuous_activity_id",
						rs.getString("continuous_activity_id"));
				activity.put("countinuos_activity_activityID",
						rs.getString("countinuos_activity_activityID"));
				activity.put("status_id", rs.getString("status_id"));
				activity.put("status_name", rs.getString("status_name"));
				activity.put("milestone_id", rs.getString("milestone_id"));
				activity.put("milestone_code", rs.getString("milestone_code"));
				activity.put("leader_id", rs.getString("leader_id"));
				activity.put("leader_acronym", rs.getString("leader_acronym"));
				activity.put("leader_name", rs.getString("leader_name"));
				activity.put("gender_id", rs.getString("gender_id"));
				activity.put("gender_description",
						rs.getString("gender_description"));
				activity.put("budget_total", rs.getString("budget_total"));
				activity.put("budget_window1", rs.getString("budget_window1"));
				activity.put("budget_window2", rs.getString("budget_window2"));
				activity.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activity.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activity.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activity.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activity.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activity.put("created_by", rs.getString("created_by"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"There was an error getting the status of an activity. \n{}",
					query, e);
		}

		if (activity.isEmpty()) {
			LOG.warn(
					"There was no information fetched from the database for activity {}",
					id);
			LOG.debug("<< getActivityStatusInfo():null");
			return null;
		} else {
//			LOG.debug("<< getActivityStatusInfo():{}", activity);
			return activity;
		}
	}

	@Override
	public int getActivityYear(int activityID) {
		LOG.debug(">> getActivityYear(activityID={})", activityID);
		int year = 0;
		String query = "SELECT a.year FROM activities a"
				+ " WHERE  a.is_deleted = '0' AND a.id = " + activityID;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				year = Integer.parseInt(rs.getString("year"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"--getActivityYear() > There was an error getting the year of the activity {}.",
					activityID, e);
		}

		LOG.debug("<< getActivityYear():{}", year);
		return year;
	}

	@Override
	public List<Map<String, String>> getPlanningActivityList(int year,
			int leaderId, ActivityStatus... activityStatus) {
		LOG.debug(">> getPlanningActivityList(year={}, leaderId={})", year,
				leaderId);
		List<Map<String, String>> activitiesData = new ArrayList<>();
		StringBuilder query = new StringBuilder(
				"SELECT a.id, a.activity_id, a.title, a.revision_id, a.status, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by, ");
		query.append(" GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names',");
		query.append(" GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code',");
		query.append(" al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym', ");
		query.append(" GROUP_CONCAT(r.id SEPARATOR '::') AS 'review_ids', GROUP_CONCAT(r.reviewer_id SEPARATOR '::') AS 'reviewer_ids', GROUP_CONCAT(r.reviewer_name SEPARATOR '::') AS 'reviewer_names' ");
		query.append(" FROM activities a");
		query.append(" LEFT JOIN contact_person cp ON cp.activity_id = a.id");
		query.append(" LEFT JOIN activity_validations av ON a.id = av.activity_id");
		query.append(" LEFT JOIN milestones m ON m.id = a.milestone_id");
		query.append(" LEFT JOIN iwmi_activity_reviewes r ON r.activity_id = a.id");
		query.append(" INNER JOIN activity_leaders al ON al.id = a.activity_leader_id");
		query.append(" WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND al.id = ");
		query.append(leaderId);

		if (activityStatus != null && activityStatus.length > 0) {
			boolean isFirst = true;
			query.append(" AND ");
			query.append(" ( ");
			for (ActivityStatus status : activityStatus) {
				if (!isFirst) {
					query.append(" OR ");
				}
				query.append("  a.status = '" + status.getName() + "' ");
				isFirst = false;
			}
			query.append(" ) ");
		}

		query.append(" GROUP BY a.id");

		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("revision_id", rs.getString("revision_id"));
				activityData.put("status", rs.getString("status"));
				activityData.put("title", rs.getString("title"));
				activityData.put("year", rs.getString("year"));
				activityData.put("is_validated", rs.getString("is_validated"));
				activityData.put("project_name", rs.getString("project_name"));
				activityData.put("gender_research",
						rs.getString("gender_research"));
				activityData
						.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activityData.put("flagship", rs.getString("flagship"));
				activityData.put("activity_cluster",
						rs.getString("activity_cluster"));
				activityData.put("lead_center", rs.getString("lead_center"));
				activityData.put("milestone_code",
						rs.getString("milestone_code"));
				activityData.put("contact_person_names",
						rs.getString("contact_person_names"));
				activityData.put("contact_person_emails",
						rs.getString("contact_person_emails"));
				activityData.put("leader_id", rs.getString("leader_id"));
				activityData.put("leader_name", rs.getString("leader_name"));
				activityData.put("leader_acronym",
						rs.getString("leader_acronym"));
				activityData.put("budget_total", rs.getString("budget_total"));
				activityData.put("budget_window1",
						rs.getString("budget_window1"));
				activityData.put("budget_window2",
						rs.getString("budget_window2"));
				activityData.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activityData.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activityData.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activityData.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activityData.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activityData.put("review_ids", rs.getString("review_ids"));
				activityData.put("reviewer_ids", rs.getString("reviewer_ids"));
				activityData.put("reviewer_names",
						rs.getString("reviewer_names"));
				activityData.put("created_by", rs.getString("created_by"));
				activitiesData.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPlanningActivityList() > There was an error getting the list of planning activities for year {} and leader {}.",
					year, leaderId);
			e.printStackTrace();
		}

		LOG.debug("<< getPlanningActivityList():activitiesData.size={}",
				activitiesData.size());
		return activitiesData;
	}

	@Override
	public List<Map<String, String>> getPlanningActivityListForRPL(int year,
			int leaderId, int regionId, ActivityStatus... activityStatus) {
		LOG.debug(">> getPlanningActivityListForRPL(year={}, leaderId={})",
				year, leaderId);
		List<Map<String, String>> activitiesData = new ArrayList<>();
		StringBuilder query = new StringBuilder("SELECT * FROM ( ");
		query.append("SELECT a.id, a.activity_id, a.title, a.revision_id, a.status, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral, a.created_by, ");
		query.append("GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names', ");
		query.append("GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code', ");
		query.append("al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym' ");
		query.append("FROM activities a ");
		query.append("LEFT JOIN contact_person cp ON cp.activity_id = a.id ");
		query.append("LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append("LEFT JOIN milestones m ON m.id = a.milestone_id ");
		query.append("INNER JOIN activity_leaders al ON al.id = a.activity_leader_id ");
		query.append("LEFT JOIN country_locations cl ON a.id = cl.activity_id ");
		query.append("LEFT JOIN countries co ON cl.country_iso2 = co.iso2 ");
		query.append("LEFT JOIN region_locations rl ON a.id = rl.activity_id ");
		query.append("LEFT JOIN regions r ON rl.region_id = r.id OR co.region_id = r.id ");
		query.append(" WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND r.id = ");
		query.append(regionId);
		query.append(" GROUP BY a.id ");
		query.append("UNION  ");
		query.append("SELECT a.id, a.activity_id, a.title, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append("GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names', ");
		query.append("GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code', ");
		query.append("al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym' ");
		query.append("FROM activities a ");
		query.append("LEFT JOIN contact_person cp ON cp.activity_id = a.id ");
		query.append("LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append("LEFT JOIN milestones m ON m.id = a.milestone_id ");
		query.append("INNER JOIN activity_leaders al ON al.id = a.activity_leader_id ");
		query.append("WHERE a.year = ");
		query.append(year);
		query.append(" AND al.id = ");
		query.append(leaderId);

		if (activityStatus != null && activityStatus.length > 0) {
			boolean isFirst = true;
			query.append(" AND ");
			query.append(" ( ");
			for (ActivityStatus status : activityStatus) {
				if (!isFirst) {
					query.append(" OR ");
				}
				query.append("  a.status = '" + status.getName() + "' ");
				isFirst = false;
			}
			query.append(" ) ");
		}

		query.append(" GROUP BY a.id ");
		query.append(") total GROUP BY id ORDER BY 'leader_id'");
		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("revision_id", rs.getString("revision_id"));
				activityData.put("status", rs.getString("status"));
				activityData.put("title", rs.getString("title"));
				activityData.put("year", rs.getString("year"));
				activityData.put("is_validated", rs.getString("is_validated"));
				activityData.put("project_name", rs.getString("project_name"));
				activityData.put("gender_research",
						rs.getString("gender_research"));
				activityData
						.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activityData.put("flagship", rs.getString("flagship"));
				activityData.put("activity_cluster",
						rs.getString("activity_cluster"));
				activityData.put("lead_center", rs.getString("lead_center"));
				activityData.put("milestone_code",
						rs.getString("milestone_code"));
				activityData.put("contact_person_names",
						rs.getString("contact_person_names"));
				activityData.put("contact_person_emails",
						rs.getString("contact_person_emails"));
				activityData.put("leader_id", rs.getString("leader_id"));
				activityData.put("leader_name", rs.getString("leader_name"));
				activityData.put("leader_acronym",
						rs.getString("leader_acronym"));
				activityData.put("budget_total", rs.getString("budget_total"));
				activityData.put("budget_window1",
						rs.getString("budget_window1"));
				activityData.put("budget_window2",
						rs.getString("budget_window2"));
				activityData.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activityData.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activityData.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activityData.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activityData.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activityData.put("created_by", rs.getString("created_by"));
				activitiesData.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPlanningActivityListForRPL() > There was an error getting the list of planning activities for year {} and leader {}.",
					year, leaderId);
			e.printStackTrace();
		}

		LOG.debug("<< getPlanningActivityListForRPL():activitiesData.size={}",
				activitiesData.size());
		return activitiesData;
	}

	@Override
	public List<Map<String, String>> getPlanningActivityListForTL(int year,
			int leaderId, int themeCode, ActivityStatus... activityStatus) {
		LOG.debug(">> getPlanningActivityListForTL(year={}, themeCode={})",
				year, themeCode);
		List<Map<String, String>> activitiesData = new ArrayList<>();
		// We going to make a UNION of both queries: one to get the activities
		// of leader and other to
		// get the activities under the theme given.

		StringBuilder query = new StringBuilder("SELECT * FROM ( ");
		query.append("SELECT a.id, a.activity_id, a.title, a.revision_id, a.status, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral,  a.created_by, ");
		query.append(" GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names', ");
		query.append(" GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code', ");
		query.append("al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym' ");
		query.append("FROM activities a ");
		query.append("LEFT JOIN contact_person cp ON cp.activity_id = a.id ");
		query.append("LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append("LEFT JOIN milestones m ON m.id = a.milestone_id ");
		query.append("LEFT JOIN outputs op ON m.output_id = op.id ");
		query.append("LEFT JOIN objectives ob ON op.objective_id = ob.id ");
		query.append("LEFT JOIN themes t ON ob.theme_id = t.id ");
		query.append("INNER JOIN activity_leaders al ON al.id = a.activity_leader_id ");
		query.append("WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND t.code = ");
		query.append(themeCode);
		query.append(" GROUP BY a.id ");
		query.append("UNION  ");
		query.append("SELECT a.id, a.activity_id, a.title, a.revision_id, a.status, (av.activity_id IS NOT NULL) as 'is_validated', ");
		query.append(" a.project_name, a.gender_research, a.echo_sys_desc, a.flagship, a.activity_cluster, a.year, a.lead_center, ");
		query.append(" a.budget_total, a.budget_window1 , a.budget_window2, a.budget_bilateral, a.gen_budget_total, ");
		query.append(" a.gen_budget_window1, a.gen_budget_window2 , a.gen_budget_bilateral,  a.created_by, ");
		query.append(" GROUP_CONCAT(cp.name SEPARATOR '::') AS 'contact_person_names', ");
		query.append(" GROUP_CONCAT(cp.email SEPARATOR '::') AS 'contact_person_emails', m.code as 'milestone_code', ");
		query.append("al.id as 'leader_id', al.name as 'leader_name', al.acronym as 'leader_acronym' ");
		query.append("FROM activities a ");
		query.append("LEFT JOIN contact_person cp ON cp.activity_id = a.id ");
		query.append("LEFT JOIN activity_validations av ON a.id = av.activity_id ");
		query.append("LEFT JOIN milestones m ON m.id = a.milestone_id ");
		query.append("LEFT JOIN outputs op ON m.output_id = op.id ");
		query.append("LEFT JOIN objectives ob ON op.objective_id = ob.id ");
		query.append("LEFT JOIN themes t ON ob.theme_id = t.id ");
		query.append("INNER JOIN activity_leaders al ON al.id = a.activity_leader_id ");
		query.append("WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND al.id = ");
		query.append(leaderId);

		if (activityStatus != null && activityStatus.length > 0) {
			boolean isFirst = true;
			query.append(" AND ");
			query.append(" ( ");
			for (ActivityStatus status : activityStatus) {
				if (!isFirst) {
					query.append(" OR ");
				}
				query.append("  a.status = '" + status.getName() + "' ");
				isFirst = false;
			}
			query.append(" ) ");
		}

		query.append(" GROUP BY a.id ");
		query.append(") total GROUP BY id ORDER BY 'leader_id'");

		// LOG.debug(">> getPlanningActivityListForTL(SQL :{})",
		// query.toString());

		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("revision_id", rs.getString("revision_id"));
				activityData.put("status", rs.getString("status"));
				activityData.put("title", rs.getString("title"));
				activityData.put("year", rs.getString("year"));
				activityData.put("is_validated", rs.getString("is_validated"));
				activityData.put("project_name", rs.getString("project_name"));
				activityData.put("gender_research",
						rs.getString("gender_research"));
				activityData
						.put("echo_sys_desc", rs.getString("echo_sys_desc"));
				activityData.put("flagship", rs.getString("flagship"));
				activityData.put("activity_cluster",
						rs.getString("activity_cluster"));
				activityData.put("lead_center", rs.getString("lead_center"));
				activityData.put("milestone_code",
						rs.getString("milestone_code"));
				activityData.put("contact_person_names",
						rs.getString("contact_person_names"));
				activityData.put("contact_person_emails",
						rs.getString("contact_person_emails"));
				activityData.put("leader_id", rs.getString("leader_id"));
				activityData.put("leader_name", rs.getString("leader_name"));
				activityData.put("leader_acronym",
						rs.getString("leader_acronym"));
				activityData.put("budget_total", rs.getString("budget_total"));
				activityData.put("budget_window1",
						rs.getString("budget_window1"));
				activityData.put("budget_window2",
						rs.getString("budget_window2"));
				activityData.put("budget_bilateral",
						rs.getString("budget_bilateral"));
				activityData.put("gen_budget_total",
						rs.getString("gen_budget_total"));
				activityData.put("gen_budget_window1",
						rs.getString("gen_budget_window1"));
				activityData.put("gen_budget_window2",
						rs.getString("gen_budget_window2"));
				activityData.put("gen_budget_bilateral",
						rs.getString("gen_budget_bilateral"));
				activityData.put("created_by", rs.getString("created_by"));
				activitiesData.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getPlanningActivityListForTL() > There was an error getting the list of planning activities for year {} and leader {}.",
					year, themeCode);
			e.printStackTrace();
		}

		LOG.debug("<< getPlanningActivityListForTL():activitiesData.size={}",
				activitiesData.size());
		return activitiesData;
	}

	@Override
	public Map<String, String> getSimpleActivity(int id) {
		LOG.debug(">> getSimpleActivity(id={})", id);

		Map<String, String> activity = new HashMap<>();
		String query = "SELECT a.title, a.id, a.activity_id, a.revision_id, a.status, a.year, a.created_by, al.id as 'leader_id', 'al.name' as 'leader_name', al.acronym as 'leader_acronym' "
				+ "FROM activities a, activity_leaders al "
				+ "WHERE  a.is_deleted = '0' AND a.activity_leader_id = al.id AND a.id = "
				+ id;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				activity.put("title", rs.getString("title"));
				activity.put("id", rs.getString("id"));
				activity.put("activity_id", rs.getString("activity_id"));
				activity.put("revision_id", rs.getString("revision_id"));
				activity.put("status", rs.getString("status"));
				activity.put("year", rs.getString("year"));
				activity.put("leader_id", rs.getString("leader_id"));
				activity.put("leader_name", rs.getString("leader_name"));
				activity.put("leader_acronym", rs.getString("leader_acronym"));
				activity.put("created_by", rs.getString("created_by"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getSimpleActivity() > There was an error getting the basic information for activity {}.",
					id, e);
		}

		if (activity.isEmpty()) {
			LOG.debug("<< getSimpleActivity():null");
			return null;
		} else {
			LOG.debug("<< getSimpleActivity():{}", activity);
			return activity;
		}
	}

	@Override
	public List<Map<String, String>> getTitles(int year) {
		LOG.debug(">> getTitles(year={})", year);
		List<Map<String, String>> activityTitles = new ArrayList<>();
		StringBuilder query = new StringBuilder(
				"SELECT a.id, a.activity_id, a.title, a.created_by ");
		query.append(" FROM activities a");
		query.append(" WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("title", rs.getString("title"));
				activityData.put("created_by", rs.getString("created_by"));
				activityTitles.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getTitles() > There was an error trying to get the list of activity titles that belong to the year {}.",
					year, e);
		}

		LOG.debug("<< getTitles():activityTitles.size={}",
				activityTitles.size());
		return activityTitles;
	}

	@Override
	public List<Map<String, String>> getTitles(int year, int activityLeaderId) {
		LOG.debug(">> getTitles(year={}, activityLeaderId={})", year,
				activityLeaderId);
		List<Map<String, String>> activityTitles = new ArrayList<>();
		StringBuilder query = new StringBuilder(
				"SELECT a.id, a.activity_id, a.title, a.created_by ");
		query.append(" FROM activities a");
		query.append(" WHERE  a.is_deleted = '0' AND a.year = ");
		query.append(year);
		query.append(" AND a.activity_leader_id = ");
		query.append(activityLeaderId);
		try (Connection connection = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query.toString(),
					connection);
			while (rs.next()) {
				Map<String, String> activityData = new HashMap<>();
				activityData.put("id", rs.getString("id"));
				activityData.put("activity_id", rs.getString("activity_id"));
				activityData.put("title", rs.getString("title"));
				activityData.put("created_by", rs.getString("created_by"));
				activityTitles.add(activityData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getTitles() > There was an error trying to get the list of activity titles that belong to the year {}.",
					year, e);
		}

		LOG.debug("<< getTitles():activityTitles.size={}",
				activityTitles.size());
		return activityTitles;
	}

	@Override
	public List<Integer> getYearList() {

		LOG.debug(">> getYearList()");
		List<Integer> yearList = new ArrayList<>();
		String query = "SELECT DISTINCT year FROM activities ORDER BY year";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				yearList.add(rs.getInt("year"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getYearList() > There was an error getting year list.",
					e);
		}

		LOG.debug("<< getYearList():{}", yearList);
		return yearList;
	}

	@Override
	public boolean hasPartners(int activityID) {
		LOG.debug(">> hasPartners(activityID={})", activityID);
		boolean hasPartners = false;
		String query = "SELECT has_partners FROM activities a WHERE  a.is_deleted = '0' AND a.id = "
				+ activityID;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				hasPartners = rs.getBoolean("has_partners");
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- hasPartners() > There was an error checking if the activity {} has partners.",
					activityID, e);
		}

		LOG.debug("<< hasPartners():{}", hasPartners);
		return hasPartners;
	}

	@Override
	public boolean isValidatedActivity(int activityID, String revisionId) {
		LOG.debug(">> isValidatedActivity(activityID={}, revision={})",
				activityID, revisionId);
		boolean isValidated = false;
		String query = "SELECT * FROM activity_validations av INNER JOIN activities WHERE "
				+ " av.activity_id = "
				+ activityID
				+ " AND  av.revision_id = '" + revisionId + "'";

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				isValidated = true;
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- isValidatedActivity() > There was an error checking if an activity was validated.",
					e);
		}

		LOG.debug("<< isValidatedActivity():{}", isValidated);
		return isValidated;
	}

	@Override
	public boolean isValidId(int id) {
		LOG.debug(">> isValidId(id={})", id);
		boolean isValid = false;
		String query = "SELECT id FROM activities WHERE id = " + id;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			isValid = rs.next();
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- isValidId() > There was an error checking if an activity is valid. \n{}",
					query, e);
		}

		LOG.debug("<< isValidId():{}", isValid);
		return isValid;
	}

	@Override
	public boolean saveHasPartners(int activityID, boolean hasPartners) {
		LOG.debug(">> saveHasPartners(activityID = {}, hasPartners = {})",
				activityID, hasPartners);
		boolean saved = false;
		String query = "UPDATE activities SET has_partners = ? WHERE id = ?";
		Object[] values = new Object[2];
		values[0] = hasPartners;
		values[1] = activityID;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- saveHasPartners() > There was a problem updating the attribute has_partners of activity {}.",
						activityID);
			} else {
				saved = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- saveHasPartners() > There was an error updating the attribute has_partners of activity {}.",
					activityID, e);
		}

		LOG.debug("<< saveHasPartners():{}", saved);
		return saved;
	}

	@Override
	public int saveSimpleActivity(Map<String, Object> activityData) {
		LOG.debug(">> saveSimpleActivity(activityData={})", activityData);
		int activityID = -1;
		try (Connection con = databaseManager.getConnection()) {
			String addQuery = "INSERT INTO activities (title, start_date, end_date, year, description, has_partners, "
					+ " activity_leader_id, continuous_activity_id, is_commissioned, milestone_id, "
					+ " project_name, gender_research, echo_sys_desc, flagship, activity_cluster, lead_center, "
					+ " budget_total, budget_window1,  budget_window2, budget_bilateral, gen_budget_total, "
					+ " gen_budget_window1, gen_budget_window2, gen_budget_bilateral, lead_center_based_id, created_by, "
					+ " activity_id) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			Object[] values = new Object[27];
			values[0] = activityData.get("title");
			values[1] = activityData.get("start_date");
			values[2] = activityData.get("end_date");
			values[3] = activityData.get("year");
			values[4] = activityData.get("description");
			values[5] = activityData.get("has_partners");
			values[6] = activityData.get("activity_leader_id");
			values[7] = activityData.get("continuous_activity_id");
			values[8] = activityData.get("is_commissioned");
			values[9] = activityData.get("milestone_id");
			values[10] = activityData.get("project_name");
			values[11] = activityData.get("gender_research");
			values[12] = activityData.get("echo_sys_desc");
			values[13] = activityData.get("flagship");
			values[14] = activityData.get("activity_cluster");
			values[15] = activityData.get("lead_center");
			values[16] = activityData.get("budget_total");
			values[17] = activityData.get("budget_window1");
			values[18] = activityData.get("budget_window2");
			values[19] = activityData.get("budget_bilateral");
			values[20] = activityData.get("gen_budget_total");
			values[21] = activityData.get("gen_budget_window1");
			values[22] = activityData.get("gen_budget_window2");
			values[23] = activityData.get("gen_budget_bilateral");
			values[24] = activityData.get("lead_center_based_id");
			values[25] = activityData.get("created_by");
			values[26] = activityData.get("activity_id");
			int activityAdded = databaseManager.makeChangeSecure(con, addQuery,
					values);
			if (activityAdded > 0) {
				// Get the generated id of the added record.
				ResultSet rs = databaseManager.makeQuery(
						"SELECT LAST_INSERT_ID()", con);
				if (rs.next()) {
					activityID = rs.getInt(1);
				}
				rs.close();
			}
		} catch (SQLException e) {
			LOG.error(
					"-- saveSimpleActivity() > There was an error trying to add a new activity titled: '{}'",
					activityData.get("title"), e);
		}
		LOG.debug("<< saveSimpleActivity():{}", activityID);
		return activityID;
	}

	@Override
	public boolean saveStatus(Map<String, String> activityData) {
		LOG.debug(">> saveStatus(activityData={})", activityData);
		boolean problem = false;
		try (Connection connection = databaseManager.getConnection()) {
			// Update activity.
			Object[] values = new Object[3];
			values[0] = activityData.get("activity_status_id");
			values[1] = activityData.get("status_description");
			values[2] = activityData.get("activity_id");
			String activityQueryPrepared = "UPDATE activities SET activity_status_id = ?, status_description = ? WHERE id = ?";
			int activityUpdatedResult = databaseManager.makeChangeSecure(
					connection, activityQueryPrepared, values);
			// Reporting errors in the previous update.
			if (activityUpdatedResult < 0) {
				problem = true;
				LOG.warn("-- saveStatus() > There was an error trying to update te status of an activity:");
				LOG.warn("  Query: {}", activityQueryPrepared);
				LOG.warn("  Values: {}", Arrays.toString(values));
			}

			// Check if the gender integration records already exist.
			String countQuery = "SELECT count(id) FROM gender_integrations WHERE activity_id = "
					+ activityData.get("activity_id");
			ResultSet rs = databaseManager.makeQuery(countQuery, connection);
			if (rs.next()) {
				int genderExistence = rs.getInt(1);
				if (genderExistence == 1) {
					// if gender exists there must be an update statement.
					String genderUpdatePrepared = "UPDATE gender_integrations SET description = ? WHERE activity_id = ?";
					values = new Object[2];
					values[0] = activityData
							.get("gender_integrations_description");
					values[1] = activityData.get("activity_id");
					int insertedGenderRows = databaseManager.makeChangeSecure(
							connection, genderUpdatePrepared, values);
					if (insertedGenderRows == 1) {
						// record updated
						LOG.debug(
								"-- saveStatus() > Activity {}: Gender integration description successfully updated.",
								activityData.get("activity_id"));
					} else {
						// problem.
						problem = true;
						LOG.warn(
								"-- saveStatus() > Activity {}: Problem trying to update the gender description.",
								activityData.get("activity_id"));
						LOG.error("  Query: {}", genderUpdatePrepared);
						LOG.error("  Values: {}", Arrays.toString(values));
					}
				} else {
					// if gender doesn't exists there must an insert statement.
					String genderInsertPrepared = "INSERT INTO gender_integrations(description, activity_id) VALUES(?, ?)";
					values = new Object[2];
					values[0] = activityData
							.get("gender_integrations_description");
					values[1] = activityData.get("activity_id");
					int insertedGenderRows = databaseManager.makeChangeSecure(
							connection, genderInsertPrepared, values);
					if (insertedGenderRows == 1) {
						// record added
						LOG.debug(
								"-- saveStatus() > Activity {}: Gender integration description successfully added.",
								activityData.get("activity_id"));
					} else {
						// problem.
						problem = true;
						LOG.warn(
								"-- saveStatus() > Activity {}: Problem trying to add the gender description.",
								activityData.get("activity_id"));
						LOG.warn("  Query: {}", genderInsertPrepared);
						LOG.warn("  Values: {}", Arrays.toString(values));
					}
				}
			}
			rs.close();

		} catch (SQLException e) {
			LOG.error(
					"-- saveStatus() > There was an error saving an activity status. \n{}",
					e);
		}

		LOG.debug("<< saveStatus():{}", !problem);
		return !problem;
	}

	@Override
	public boolean updateGlobalAttribute(int activityID, boolean isGlobal) {
		LOG.debug(">> updateGlobalAttribute(activityID = {}, isGlobal = {})",
				activityID, isGlobal);
		boolean saved = false;
		String query = "UPDATE activities SET is_global = ? WHERE id = ?";
		Object[] values = new Object[2];
		values[0] = isGlobal;
		values[1] = activityID;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- updateGlobalAttribute() > There was a problem updating the attribute is global of activity {}.",
						activityID);
			} else {
				saved = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- updateGlobalAttribute() > There was an error updating the attribute is global of activity {}.",
					activityID, e);
		}

		LOG.debug("<< updateGlobalAttribute():{}", saved);
		return saved;
	}

	@Override
	public boolean deleteActivity(Long activityID) {
		LOG.debug(">> deleteActivity(activityID = {} )", activityID);
		boolean saved = false;
		String query = "UPDATE activities SET is_deleted = ? WHERE id = ?";
		Object[] values = new Object[2];
		values[0] = 1;
		values[1] = activityID;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				LOG.warn(
						"-- deleteActivity() > There was a problem updating the attribute is_deleted of activity {}.",
						activityID);
			} else {
				saved = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteActivity() > There was an error updating the attribute is_deleted of activity {}.",
					activityID, e);
		}

		LOG.debug("<< deleteActivity():{}", saved);
		return saved;
	}

	public boolean updateActivitStatus(int activityId,
			ActivityStatus activityStatus, String revisionId) {
		LOG.debug(
				">> updateActivitStatus(activity id={}, activity status = {})",
				activityId, activityStatus);
		boolean added = false;
		Object[] values = new Object[3];
		values[0] = activityStatus.getName();
		values[1] = revisionId;
		values[2] = activityId;

		String query = "UPDATE activities SET status = ?, revision_id = ?  WHERE id = ? ";

		try (Connection con = databaseManager.getConnection()) {
			int updatedResult = databaseManager.makeChangeSecure(con, query,
					values);
			if (updatedResult < 0) {
				LOG.warn(
						"-- updateActivitStatus() > There was a problem updating the status for an activity. See query below. \n{}",
						query);
				LOG.warn("  Query: {}", query);
				LOG.warn("  Values: {}", Arrays.toString(values));
			} else
				added = true;

		} catch (SQLException e) {
			LOG.error(
					"-- updateActivitStatus() > There was an error updating the staus for an activity.",
					e);
			LOG.error("  Query: {}", query);
			LOG.error("  Values: {}", Arrays.toString(values));
		}

		LOG.debug("<< updateActivitStatus():{}", added);
		return added;
	}

	@Override
	public boolean updateMainInformation(Map<String, String> activityData) {
		LOG.debug(">> updateMainInformation(activityData={})", activityData);
		boolean added = false;
		Object[] values = new Object[21];
		values[0] = activityData.get("title");
		values[1] = activityData.get("year");
		values[2] = activityData.get("description");
		values[3] = activityData.get("start_date");
		values[4] = activityData.get("end_date");
		values[5] = activityData.get("milestone_id");
		values[6] = activityData.get("project_name");
		values[7] = activityData.get("gender_research");
		values[8] = activityData.get("echo_sys_desc");
		values[9] = activityData.get("flagship");
		values[10] = activityData.get("activity_cluster");
		values[11] = activityData.get("lead_center");
		values[12] = activityData.get("budget_total");
		values[13] = activityData.get("budget_window1");
		values[14] = activityData.get("budget_window2");
		values[15] = activityData.get("budget_bilateral");
		values[16] = activityData.get("gen_budget_total");
		values[17] = activityData.get("gen_budget_window1");
		values[18] = activityData.get("gen_budget_window2");
		values[19] = activityData.get("gen_budget_bilateral");
		values[20] = activityData.get("id");

		String query = "UPDATE activities SET title = ?, year = ?, description = ?, start_date = ?, end_date = ?,"
				+ " milestone_id = ?, project_name = ?, gender_research = ?, echo_sys_desc = ?,"
				+ " flagship = ?, activity_cluster = ?, lead_center = ?,  "
				+ " budget_total = ?, budget_window1 = ?,  budget_window2 = ?, budget_bilateral = ?, "
				+ " gen_budget_total = ?, gen_budget_window1 = ?, gen_budget_window2 = ?, gen_budget_bilateral = ? "
				+ " WHERE id = ? ";

		try (Connection con = databaseManager.getConnection()) {
			int updatedResult = databaseManager.makeChangeSecure(con, query,
					values);
			if (updatedResult < 0) {
				LOG.warn(
						"-- updateMainInformation() > There was a problem updating the main information for an activity. See query below. \n{}",
						query);
				LOG.warn("  Query: {}", query);
				LOG.warn("  Values: {}", Arrays.toString(values));
			} else {
				added = true;
				// If the activity was updated successfully save the gender
				// integration description
				// Delete the record in the database
				query = "DELETE FROM gender_integrations WHERE `activity_id` = ?";
				int rowsDeleted = databaseManager.makeChangeSecure(con, query,
						new Object[] { activityData.get("id") });
				if (rowsDeleted < -1) {
					LOG.warn(
							"-- updateMainInformation() > There was a problem deleting the gender integration description for activity {}",
							activityData.get("id"));
				}
				if (activityData.get("genderDescription") != null) {
					values = new Object[2];
					values[0] = activityData.get("genderDescription");
					values[1] = activityData.get("id");
					query = "INSERT INTO gender_integrations (description, activity_id) VALUES (?, ?) "
							+ "ON DUPLICATE KEY UPDATE description = VALUES(description)";
					int rowsAffected = databaseManager.makeChangeSecure(con,
							query, values);
					if (rowsAffected < 0) {
						LOG.warn("-- updateMainInformation() > There was a problem saving the gender integration description.");
						LOG.warn("Query: {}", query);
						LOG.warn("Values: {}", values);
					}
				}
			}
		} catch (SQLException e) {
			LOG.error(
					"-- updateMainInformation() > There was an error updating the main information for an activity.",
					e);
			LOG.error("  Query: {}", query);
			LOG.error("  Values: {}", Arrays.toString(values));
		}

		LOG.debug("<< updateMainInformation():{}", added);
		return added;
	}

	@Override
	public boolean validateActivity(int activityID, String revisionId,
			boolean isValidated) {
		LOG.debug(
				">> validateActivity(activityID = {}, revision = {}, isValidated={} )",
				new Object[] { activityID, revisionId, isValidated });
		boolean saved = false;
		String query;

		if (isValidated) {
			query = "INSERT INTO activity_validations (`activity_id`, `revision_id`) VALUES (?, ?)";
		} else {
			query = "DELETE FROM activity_validations WHERE `activity_id` = ? AND `revision_id` = ? ; ";
		}

		Object[] values = new Object[2];
		values[0] = activityID;
		values[1] = revisionId;
		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				String message = "-- validateActivity() > There was a problem inserting a record into the table activity_validations for activity {}.";
				LOG.warn(message, activityID);
			} else {
				saved = true;
			}
		} catch (SQLException e) {
			String message = "-- validateActivity() > There was an error inserting a record into the table activity_validations for activity {}.";
			LOG.error(message, activityID, e);
		}

		LOG.debug("<< validateActivity():{}", saved);
		return saved;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.cgiar.ccafs.ap.data.dao.ActivityDAO#updateActivityRevisionId(int,
	 *      java.lang.String)
	 */
	@Override
	public boolean updateActivityRevisionId(int activityId, String revisionId) {
		LOG.debug(
				">> updateActivityRevisionId(activityID = {}, revisionId={} )",
				activityId, revisionId);
		boolean updated = false;
		String query = " UPDATE activities SET revision_id = ? WHERE id = ?";

		Object[] values = new Object[2];
		values[0] = revisionId;
		values[1] = activityId;

		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				String message = "-- updateActivityRevisionId() > There was a problem updating an activity revision.";
				LOG.warn(message, activityId);
			} else {
				updated = true;
			}
		} catch (SQLException e) {
			String message = "-- updateActivityRevisionId() > There was a problem updating an activity revision{}.";
			LOG.error(message, activityId, e);
		}

		LOG.debug("<< updateActivityRevisionId():{}", updated);
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cgiar.ccafs.ap.data.dao.ActivityDAO#fetchReview(int,
	 * java.lang.String, org.cgiar.ccafs.ap.data.model.SectionReview.Type,
	 * org.cgiar.ccafs.ap.data.model.SectionReview)
	 */
	@Override
	public SectionReview fetchReview(int activityId, String revisionId,
			Category reviewType) {
		LOG.debug(
				">> fetchReview(activityID={}, Revision Id ={}, Review Type= {})",
				new Object[] { activityId, revisionId, reviewType });

		SectionReview review = new SectionReview();
		String query = "SELECT " + reviewType.getColumnName()
				+ " FROM activities a WHERE  a.id = " + activityId
				+ " AND  a.revision_id = '" + revisionId + "' ";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				String reviewComment = rs.getString(reviewType.getColumnName());
				if (reviewComment != null) {
					review = SectionReview.getReview(reviewComment);
					review.setActivityId(activityId);
					review.setCurrentRevision(revisionId);
				}
			}
			rs.close();
		} catch (Exception e) {
			LOG.error(
					"-- fetchReview(activityID={}, Revision Id ={}, Review Type= {})",
					new Object[] { activityId, revisionId, reviewType }, e);
		}

		LOG.debug(">> fetchReview ", review.getReviews());
		return review;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cgiar.ccafs.ap.data.dao.ActivityDAO#fetchAllReviews(int,
	 * java.lang.String, org.cgiar.ccafs.ap.data.model.SectionReview.Type,
	 * org.cgiar.ccafs.ap.data.model.SectionReview)
	 */
	@Override
	public List<SectionReview> fetchAllReviews(int activityId,
			String revisionId, Category reviewType) {
		LOG.debug(
				">> fetchAllReviews(activityID={}, Revision Id ={}, Review Type= {})",
				new Object[] { activityId, revisionId, reviewType });

		List<SectionReview> reviews = new ArrayList<SectionReview>();

		String query = "SELECT " + reviewType.getColumnName()
				+ " FROM activities a WHERE  a.id = " + activityId
				+ " AND  a.revision_id = '" + revisionId + "' ";

		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				String reviewComment = rs.getString(reviewType.getColumnName());
				if (reviewComment != null) {
					SectionReview review = SectionReview
							.getReview(reviewComment);
					review.setActivityId(activityId);
					review.setCurrentRevision(revisionId);
					reviews.add(review);
				}
			}
			rs.close();
		} catch (Exception e) {
			LOG.error(
					"-- fetchAllReviews(activityID={}, Revision Id ={}, Review Type= {})",
					new Object[] { activityId, revisionId, reviewType }, e);
		}

		LOG.debug(">> fetchAllReviews ", reviews.size());
		return reviews;
	}

	@Override
	public boolean updateReview(int activityId, String revisionId,
			Category reviewType, SectionReview review) {
		LOG.debug(">> updateReview(activityID = {}, revisionId={} )",
				activityId, revisionId);
		boolean updated = false;
		String query = " UPDATE activities SET " + reviewType.getColumnName()
				+ " = ? WHERE id = ? and revision_id = ? ";

		Object[] values = new Object[3];
		values[0] = SectionReview.getJSON(review);
		values[1] = activityId;
		values[2] = revisionId;

		LOG.debug("<< updateReview(): QUERY :{}, values : " + values[0] + "",
				query);

		try (Connection con = databaseManager.getConnection()) {
			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				String message = "-- updateReview() > There was a problem updating an activity review.";
				LOG.warn(message, activityId);
			} else {
				updated = true;
			}
		} catch (SQLException e) {
			String message = "-- updateReview() > There was a problem updating an activity review{}.";
			LOG.error(message, activityId, e);
		}

		LOG.debug("<< updateReview():{}", updated);
		return updated;
	}

	@Override
	public boolean saveOrUdatetMidYearRating(String activityId,
			ActivityReviewRating rating) {
		LOG.debug(">> savetMidYearRating(activityID = {})", activityId);
		boolean updated = false;

		try (Connection con = databaseManager.getConnection()) {

			String query = "";
			Object[] values = null;

			if (rating.getId() < 1) {
				query = " INSERT INTO activity_review_ratings (activity_id, mid_year_rate, mid_yr_rating_json) VALUES ( ?, ?, ?)";

				values = new Object[3];
				values[0] = activityId;
				values[1] = rating.getMidYearReviewRating();
				values[2] = getJSON(rating);
			} else {

				query = " UPDATE activity_review_ratings set mid_year_rate = ?, mid_yr_rating_json = ? WHERE id = ?";

				values = new Object[3];
				values[0] = rating.getMidYearReviewRating();
				values[1] = getJSON(rating);
				values[2] = rating.getId();
			}

			LOG.debug("<< savetMidYearRating(): QUERY :{}, values : "
					+ values[0] + "", query);

			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				String message = "-- savetMidYearRating() > There was a problem updating an activity mid year ratings.";
				LOG.warn(message, activityId);
			} else {
				updated = true;
			}
		} catch (Exception e) {
			String message = "-- savetMidYearRating() > There was a problem updating an activity  mid year ratings. {}.";
			LOG.error(message, activityId, e);
		}

		LOG.debug("<< savetMidYearRating():{}", updated);
		return updated;
	}

	@Override
	public ActivityReviewRating getMidYearRating(String activityId) {
		LOG.debug(">> getMidYearRating(activityID={}",
				new Object[] { activityId });

		String query = "SELECT id, activity_id, mid_year_rate, mid_yr_rating_json "
				+ " FROM activity_review_ratings a WHERE  a.activity_id = '" + activityId + "' ";

		LOG.debug(">> getMidYearRating : Query :{}", query);
		ActivityReviewRating rate = null;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				rate = new ActivityReviewRating();
				rate = populateObject(rs.getString("mid_yr_rating_json"));
				rate.setId(rs.getInt("id"));
				rate.setActivityId(rs.getString("activity_id"));
				rate.setMidYearReviewRating(rs.getInt("mid_year_rate"));
			}
			rs.close();
		} catch (Exception e) {
			LOG.error("-- getMidYearRating (activityID={})",
					new Object[] { activityId }, e);
		}

		LOG.debug(">> getMidYearRating ", rate);
		return rate;
	}

	
	
	@Override
	public boolean saveMidYearReviewRatingComment(int reviewRatingId,
			ActivityReviewRatingComment ratingComment) {
		
		LOG.debug(">> saveMidYearReviewRatingComment(review Rating Id = {})", reviewRatingId);
		boolean updated = false;

		List<ActivityReviewRatingComment> comments = getMidYearReviewRatingComment(reviewRatingId);
		try (Connection con = databaseManager.getConnection()) {
			comments.add(ratingComment);
			
			String query = " UPDATE activity_review_ratings set mid_yr_comments = ? WHERE id = ?";

			Object[]  values = new Object[2];
				values[0] = ActivityReviewRatingComment.getJSON(comments);
				values[1] = reviewRatingId;

			LOG.debug("<< saveMidYearReviewRatingComment(): QUERY :{}, values : {}", query,  values[0]);

			int rows = databaseManager.makeChangeSecure(con, query, values);
			if (rows < 0) {
				String message = "-- saveMidYearReviewRatingComment() > There was a problem updating an activity mid year ratings comments.";
				LOG.warn(message, reviewRatingId);
			} else {
				updated = true;
			}
		} catch (Exception e) {
			String message = "-- saveMidYearReviewRatingComment() > There was a problem updating an activity  mid year ratings. {}.";
			LOG.error(message, reviewRatingId, e);
		}

		LOG.debug("<< saveMidYearReviewRatingComment():{}", updated);
		return updated;
	}

	@Override
	public List<ActivityReviewRatingComment> getMidYearReviewRatingComment(int ratingId) {
		LOG.debug(">> getMidYearReviewRatingComment(activity Rating ID={}",
				new Object[] { ratingId });

		String query = "SELECT id, mid_yr_comments "
				+ " FROM activity_review_ratings a WHERE  a.id = "
				+ ratingId;

		List<ActivityReviewRatingComment> comments = new ArrayList<ActivityReviewRatingComment>();
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				comments = ActivityReviewRatingComment.populateObject(rs.getString("mid_yr_comments"));
			}
			rs.close();
		} catch (Exception e) {
			LOG.error("-- getMidYearReviewRatingComment (activity Rating ID={})",
					new Object[] { ratingId }, e);
		}

		LOG.debug(">> getMidYearReviewRatingComment :", comments.size());
		return comments;
	}
	
	@Override
	public int getActivityCount(String cluster) {
		LOG.debug(">> getActivityCount(cluster={})",
				new Object[] { cluster });

		String query = "SELECT count(*) "
				+ " FROM activities WHERE "
				+ " activity_cluster = '" + cluster + "'";

		LOG.debug(">> getActivityCount : Query :{}", query);
		int count = 0;
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			if (rs.next()) {
				count = rs.getInt(1);
			}
			rs.close();
		} catch (Exception e) {
			LOG.error("-- getActivityCount(cluster={})",
					new Object[] { cluster }, e);
		}

		LOG.debug(">> getActivityCount ", count);
		return count;
	}

	/**
	 * <p>
	 * Setting value for databaseManager.
	 * </p>
	 * 
	 * @param databaseManager
	 *            the databaseManager to set
	 */
	public void setDatabaseManager(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	/**
	 * <p>
	 * Getter for databaseManager.
	 * </p>
	 * 
	 * @return the databaseManager
	 */
	public DAOManager getDatabaseManager() {
		return databaseManager;
	}
}
