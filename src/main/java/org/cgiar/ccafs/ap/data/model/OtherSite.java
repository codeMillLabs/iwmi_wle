/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class OtherSite implements Difference<OtherSite>{

  private int id;
  private double latitude;
  private double longitude;
  private String details;
  private Country country;

  public Country getCountry() {
    return country;
  }

  public String getDetails() {
    return details;
  }

  public int getId() {
    return id;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
  
  private static String LATITUDE = "Latitude";
  private static String LONGITUDE = "Longitude";
  private static String DETAILS = "Details";
  private static String COUNTRY = "Country";

	@Override
	public DiffReport diff(OtherSite t) {
		DiffReport report = new DiffReport();

		if (isNotEqual(this.getCountry().getName(), t.getCountry().getName())) {
			report.append(COUNTRY, this.getCountry().getName(), t.getCountry()
					.getName());
		}

		if (isNotEqual(this.details, t.details)) {
			report.append(DETAILS, this.details, t.details);
		}

		if (this.latitude != t.latitude) {
			report.append(LATITUDE, "" + this.latitude, "" + t.latitude);
		}

		if (this.longitude != t.longitude) {
			report.append(LONGITUDE, "" + this.longitude, "" + t.longitude);
		}
		return report;
	}

@Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
