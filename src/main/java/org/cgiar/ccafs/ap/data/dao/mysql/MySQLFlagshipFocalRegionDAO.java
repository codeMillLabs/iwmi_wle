/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.FlagshipFocalRegionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLFlagshipFocalRegionDAO implements FlagshipFocalRegionDAO {

	// Looging
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLFlagshipFocalRegionDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLFlagshipFocalRegionDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public List<Map<String, String>> getFlagshipFocalRegions() {
		LOG.debug(">> getFlagshipFocalRegions()");
		List<Map<String, String>> flagshipFocalRegionsDataList = new ArrayList<>();
		String query = " SELECT id, fs_fr_no, name, leadera, leadera_id, leaderb, leaderb_id, long_name, org_name"
				+ "  FROM flagship_focalregion ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> flagshipFocalRegionsData = new HashMap<>();
				flagshipFocalRegionsData.put("id", rs.getString("id"));
				flagshipFocalRegionsData.put("fs_fr_no",
						rs.getString("fs_fr_no"));
				flagshipFocalRegionsData.put("name", rs.getString("name"));
				flagshipFocalRegionsData
						.put("leadera", rs.getString("leadera"));
				flagshipFocalRegionsData.put("leadera_id",
						rs.getString("leadera_id"));
				flagshipFocalRegionsData
						.put("leaderb", rs.getString("leaderb"));
				flagshipFocalRegionsData.put("leaderb_id",
						rs.getString("leaderb_id"));
				flagshipFocalRegionsData.put("long_name",
						rs.getString("long_name"));
				flagshipFocalRegionsData.put("org_name",
						rs.getString("org_name"));
				flagshipFocalRegionsDataList.add(flagshipFocalRegionsData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipFocalRegions() > There was an error getting flagship / focal regions",
					e);
		}
		LOG.debug(
				"<< getFlagshipFocalRegions():flagshipFocalRegionsDataList.size={}",
				flagshipFocalRegionsDataList.size());
		return flagshipFocalRegionsDataList;
	}

	@Override
	public List<Integer> getClusterIds(int flagshipId) {
		LOG.debug(">> getClusterIds({})", flagshipId);
		List<Integer> clusterIds = new ArrayList<>();
		String query = " SELECT cluster_id "
				+ "  FROM rpt_flagship_cluster WHERE flagship_id=" + flagshipId
				+ " ORDER BY cast(cluster_id as unsigned)";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				clusterIds.add(Integer.parseInt(rs.getString("cluster_id")));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getClusterIds() > There was an error getting clusters for flagship id {}",
					flagshipId, e);
		}
		LOG.debug("<< getClusterIds():clusterIds.size={}", clusterIds.size());
		return clusterIds;
	}

	@Override
	public List<Map<String, String>> getFlagshipReportData(int year,
			int flagshipId, String cluster) {
		LOG.debug(">> getFlagshipReportData(flagshipId={}, cluster={})",
				flagshipId, cluster);
		List<Map<String, String>> flagshipFocalRegionsClusterDataList = new ArrayList<>();
		String query = " SELECT id, output_code, output, activity_id, project, del_code, del FROM flfr_cluster_data WHERE activity_id like '"
				+ year
				+ "%' AND fs_id="
				+ flagshipId
				+ " AND cluster='"
				+ cluster + "' ORDER BY fs_id,cluster,output_code";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> flagshipFocalRegionClusterData = new HashMap<>();
				flagshipFocalRegionClusterData.put("id", rs.getString("id"));
				flagshipFocalRegionClusterData.put("output_code",
						rs.getString("output_code"));
				flagshipFocalRegionClusterData.put("output",
						rs.getString("output"));
				flagshipFocalRegionClusterData.put("activity_id",
						rs.getString("activity_id"));
				flagshipFocalRegionClusterData.put("project",
						rs.getString("project"));
				flagshipFocalRegionClusterData.put("del_code",
						rs.getString("del_code"));
				flagshipFocalRegionClusterData.put("del", rs.getString("del"));
				flagshipFocalRegionsClusterDataList
						.add(flagshipFocalRegionClusterData);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipReportData() > There was an error getting flagship / focal region cluster data",
					e);
		}
		LOG.debug(
				"<< getFlagshipReportData():flagshipFocalRegionsClusterDataList.size={}",
				flagshipFocalRegionsClusterDataList.size());
		return flagshipFocalRegionsClusterDataList;
	}

	@Override
	public Map<String, String> getFlagshipFocalRegion(int id) {
		LOG.debug(">> getFlagshipFocalRegions()");
		Map<String, String> flagshipFocalRegionsData = new HashMap<>();
		String query = " SELECT id, fs_fr_no, name, leadera, leadera_id, leaderb, leaderb_id, long_name"
				+ "  FROM flagship_focalregion WHERE id=" + id + " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				flagshipFocalRegionsData.put("id", rs.getString("id"));
				flagshipFocalRegionsData.put("fs_fr_no",
						rs.getString("fs_fr_no"));
				flagshipFocalRegionsData.put("name", rs.getString("name"));
				flagshipFocalRegionsData
						.put("leadera", rs.getString("leadera"));
				flagshipFocalRegionsData.put("leadera_id",
						rs.getString("leadera_id"));
				flagshipFocalRegionsData
						.put("leaderb", rs.getString("leaderb"));
				flagshipFocalRegionsData.put("leaderb_id",
						rs.getString("leaderb_id"));
				flagshipFocalRegionsData.put("long_name",
						rs.getString("long_name"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlagshipFocalRegions() > There was an error getting flagship / focal region data",
					e);
		}
		LOG.debug(
				"<< getFlagshipFocalRegion():flagshipFocalRegionsData.size={}",
				flagshipFocalRegionsData.size());
		return flagshipFocalRegionsData;
	}

	@Override
	public int saveFlagshipRptStatus(Map<String, String> keyValues) {
		LOG.debug(
				">> saveFlagshipRptStatus( Flagship / Focal Regions report status : {})",
				keyValues);

		String query = "INSERT INTO fl_fr_rpt_status ( year, flagship_id, period) VALUES ( ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[3];
			values[0] = keyValues.get("year");
			values[1] = keyValues.get("flagship_id");
			values[2] = keyValues.get("period");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(
						">> Flagship / Focal Regions report status saved ( Flagship / Focal Regions report status id : {})",
						id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveFlagshipRptStatus() > There was an error trying to add new Flagship / Focal Regions report status : '{}'",
					keyValues.get("flagship_id"), e);
		}
		return -1;
	}

	@Override
	public boolean checkReportAvailable(int year, int flagshipId, String period) {
		LOG.debug(">> checkReportAvailable(year,flagshipId,period {})", year
				+ "," + flagshipId + "," + period);
		boolean available = false;
		String query = " SELECT count(*)"
				+ "  FROM fl_fr_rpt_status WHERE year=" + year
				+ " AND flagship_id=" + flagshipId + " AND period='" + period
				+ "'";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			rs.next();
			int count = rs.getInt(1);
			if (count > 0) {
				available = true;
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- checkReportAvailable() > There was an error checking flagship report availability",
					e);
		}
		LOG.debug("<< checkReportAvailable():available={}", available);
		return available;
	}

	@Override
	public List<Integer> getFlaghipIdsByActivity(String activityId) {
		LOG.debug(">> getFlaghipIdsByActivity(activityId={})", activityId);
		List<Integer> flagshipIds = new ArrayList<>();
		String query = " SELECT DISTINCT fs_id "
				+ "  FROM flfr_cluster_data WHERE activity_id='" + activityId
				+ "' ORDER BY fs_id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				flagshipIds.add(Integer.parseInt(rs.getString("fs_id")));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getFlaghipIdsByActivity() > There was an error getting flagships for activity id {}",
					activityId, e);
		}
		LOG.debug("<< getFlaghipIdsByActivity():flagshipIds.size={}",
				flagshipIds.size());
		return flagshipIds;
	}

	@Override
	public int saveMainPageFirstSection(Map<String, String> keyValues) {
		LOG.debug(">> saveMainPageFirstSection(Main page first section : {})",
				keyValues);

		String query = "INSERT INTO score_card_cache_main (cluster_green, cluster_yellow, cluster_red,"
				+ " project_green, project_yellow, project_red, output_green, output_yellow, output_red, spent,"
				+ " not_spent, cluster_count, project_count, output_count, cluster_output_count)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[15];
			values[0] = keyValues.get("cluster_green");
			values[1] = keyValues.get("cluster_yellow");
			values[2] = keyValues.get("cluster_red");
			values[3] = keyValues.get("project_green");
			values[4] = keyValues.get("project_yellow");
			values[5] = keyValues.get("project_red");
			values[6] = keyValues.get("output_green");
			values[7] = keyValues.get("output_yellow");
			values[8] = keyValues.get("output_red");
			values[9] = keyValues.get("spent");
			values[10] = keyValues.get("not_spent");
			values[11] = keyValues.get("cluster_count");
			values[12] = keyValues.get("project_count");
			values[13] = keyValues.get("output_count");
			values[14] = keyValues.get("cluster_output_count");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Main page first section saved {}", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveMainPageFirstSection() > There was an error trying to saving main page first section",
					e);
		}
		return -1;
	}

	@Override
	public Map<String, String> getScoreCardMainPageFirstSection() {
		LOG.debug(">> getMainPageFirstSection()");
		Map<String, String> mainPageMap = new HashMap<>();
		String query = " SELECT * FROM score_card_cache_main";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				mainPageMap.put("id", rs.getString("id"));
				mainPageMap.put("cluster_green", rs.getString("cluster_green"));
				mainPageMap.put("cluster_yellow",
						rs.getString("cluster_yellow"));
				mainPageMap.put("cluster_red", rs.getString("cluster_red"));
				mainPageMap.put("project_green", rs.getString("project_green"));
				mainPageMap.put("project_yellow",
						rs.getString("project_yellow"));
				mainPageMap.put("project_red", rs.getString("project_red"));
				mainPageMap.put("output_green", rs.getString("output_green"));
				mainPageMap.put("output_yellow", rs.getString("output_yellow"));
				mainPageMap.put("output_red", rs.getString("output_red"));
				mainPageMap.put("spent", rs.getString("spent"));
				mainPageMap.put("not_spent", rs.getString("not_spent"));
				mainPageMap.put("cluster_count", rs.getString("cluster_count"));
				mainPageMap.put("project_count", rs.getString("project_count"));
				mainPageMap.put("output_count", rs.getString("output_count"));
				mainPageMap.put("cluster_output_count",
						rs.getString("cluster_output_count"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error("-- getMainPageFirstSection() > Main page fisrt section",
					e);
		}
		LOG.debug("<< getMainPageFirstSection():mainPageMap.size={}",
				mainPageMap.size());
		return mainPageMap;
	}

	@Override
	public boolean deleteScoreCardCache() {
		LOG.debug(">> deleteScoreCardCache()");
		boolean deleted = false;
		String queryMain = "DELETE FROM score_card_cache_main";
		String queryFlagship = "DELETE FROM score_card_cache_main_flagship";
		String queryCluster = "DELETE FROM score_card_cache_second_page";
		String queryOutput = "DELETE FROM score_card_cache_third_page_output";
		String queryProject = "DELETE FROM score_card_cache_third_page_project";
		try (Connection con = databaseManager.getConnection()) {
			int rowsMain = databaseManager.makeChange(queryMain, con);
			int rowsFlagship = databaseManager.makeChange(queryFlagship, con);
			int rowsCluster = databaseManager.makeChange(queryCluster, con);
			int rowsOutput = databaseManager.makeChange(queryOutput, con);
			int rowsProject = databaseManager.makeChange(queryProject, con);

			if (rowsMain < 0) {
				LOG.warn(
						"-- deleteScoreCardCache() > there was a problem in delete score card main page {}",
						queryMain);
			} else if (rowsFlagship < 0) {
				LOG.warn(
						"-- deleteScoreCardCache() > there was a problem in delete score card flagships {}",
						queryFlagship);
			} else if (rowsCluster < 0) {
				LOG.warn(
						"-- deleteScoreCardCache() > there was a problem in delete score card second page {}",
						queryFlagship);
			} else if (rowsOutput < 0) {
				LOG.warn(
						"-- deleteScoreCardCache() > there was a problem in delete score card third page outputs {}",
						queryOutput);
			} else if (rowsProject < 0) {
				LOG.warn(
						"-- deleteScoreCardCache() > there was a problem in delete score card third page projects {}",
						queryProject);
			} else {
				deleted = true;
			}
		} catch (SQLException e) {
			LOG.error(
					"-- deleteScoreCardCache() > there was a problem in delete score card main page",
					e);
		}
		LOG.debug("<< deleteScoreCardCache():{}", deleted);
		return deleted;
	}

	@Override
	public int saveScoreCardMainPageFlagship(Map<String, String> keyValues) {
		LOG.debug(">> saveMainPageFlagship(Main page flagship section : {})",
				keyValues);

		String query = "INSERT INTO score_card_cache_main_flagship (green, yellow, red,"
				+ " name, long_name, tooltip, spent, not_spent)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[8];
			values[0] = keyValues.get("green");
			values[1] = keyValues.get("yellow");
			values[2] = keyValues.get("red");
			values[3] = keyValues.get("name");
			values[4] = keyValues.get("long_name");
			values[5] = keyValues.get("tooltip");
			values[6] = keyValues.get("spent");
			values[7] = keyValues.get("not_spent");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Main page flagship saved {}", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveMainPageFlagship() > There was an error trying to saving main page flagship",
					e);
		}
		return -1;
	}

	@Override
	public List<Map<String, String>> getScoreCardMainPageFlagships() {
		LOG.debug(">> getScoreCardMainPageFlagships()");
		List<Map<String, String>> scoreCardMainPageFlagshipData = new ArrayList<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_main_flagship ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> row = new HashMap<>();
				row.put("id", rs.getString("id"));
				row.put("green", rs.getString("green"));
				row.put("yellow", rs.getString("yellow"));
				row.put("red", rs.getString("red"));
				row.put("name", rs.getString("name"));
				row.put("long_name", rs.getString("long_name"));
				row.put("tooltip", rs.getString("tooltip"));
				row.put("spent", rs.getString("spent"));
				row.put("not_spent", rs.getString("not_spent"));
				scoreCardMainPageFlagshipData.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardMainPageFlagships() > There was an error getting score card main page flagships",
					e);
		}
		LOG.debug(
				"<< getScoreCardMainPageFlagships():scoreCardMainPageFlagshipData.size={}",
				scoreCardMainPageFlagshipData.size());
		return scoreCardMainPageFlagshipData;
	}

	@Override
	public Map<String, String> getScoreCardMainPageFlagship(int id) {
		LOG.debug(">> getScoreCardMainPageFlagship(id={})", id);
		Map<String, String> scoreCardMainPageFlagshipData = new HashMap<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_main_flagship WHERE id=" + id
				+ " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				scoreCardMainPageFlagshipData.put("id", rs.getString("id"));
				scoreCardMainPageFlagshipData.put("green",
						rs.getString("green"));
				scoreCardMainPageFlagshipData.put("yellow",
						rs.getString("yellow"));
				scoreCardMainPageFlagshipData.put("red", rs.getString("red"));
				scoreCardMainPageFlagshipData.put("name", rs.getString("name"));
				scoreCardMainPageFlagshipData.put("long_name",
						rs.getString("long_name"));
				scoreCardMainPageFlagshipData.put("tooltip",
						rs.getString("tooltip"));
				scoreCardMainPageFlagshipData.put("spent",
						rs.getString("spent"));
				scoreCardMainPageFlagshipData.put("not_spent",
						rs.getString("not_spent"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardMainPageFlagship() > There was an error getting score card main page flagship",
					e);
		}
		LOG.debug(
				"<< getScoreCardMainPageFlagship():scoreCardMainPageFlagshipData.size={}",
				scoreCardMainPageFlagshipData.size());
		return scoreCardMainPageFlagshipData;
	}

	@Override
	public int saveScoreCardSecondPage(Map<String, String> keyValues) {
		LOG.debug(">> saveScoreCardSecondPage(Score card second page: {})",
				keyValues);
		String query = "INSERT INTO score_card_cache_second_page "
				+ "(flagship_id, project_green, project_yellow, project_red,"
				+ " output_green, output_yellow, output_red, rating, name, spent, not_spent, comment)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[12];
			values[0] = keyValues.get("flagship_id");
			values[1] = keyValues.get("project_green");
			values[2] = keyValues.get("project_yellow");
			values[3] = keyValues.get("project_red");
			values[4] = keyValues.get("output_green");
			values[5] = keyValues.get("output_yellow");
			values[6] = keyValues.get("output_red");
			values[7] = keyValues.get("rating");
			values[8] = keyValues.get("name");
			values[9] = keyValues.get("spent");
			values[10] = keyValues.get("not_spent");
			values[11] = keyValues.get("comment");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Score card second page saved {}", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveScoreCardSecondPage() > There was an error trying to saving score card second page",
					e);
		}
		return -1;
	}

	@Override
	public List<Map<String, String>> getScoreCardSecondPages(int flagshipId) {
		LOG.debug(">> getScoreCardSecondPages(flagshipId={})", flagshipId);
		List<Map<String, String>> scoreCardSecondPageData = new ArrayList<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_second_page where flagship_id="
				+ flagshipId + " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> row = new HashMap<>();
				row.put("id", rs.getString("id"));
				row.put("project_green", rs.getString("project_green"));
				row.put("project_yellow", rs.getString("project_yellow"));
				row.put("project_red", rs.getString("project_red"));
				row.put("output_green", rs.getString("output_green"));
				row.put("output_yellow", rs.getString("output_yellow"));
				row.put("output_red", rs.getString("output_red"));
				row.put("rating", rs.getString("rating"));
				row.put("name", rs.getString("name"));
				row.put("spent", rs.getString("spent"));
				row.put("not_spent", rs.getString("not_spent"));
				row.put("comment", rs.getString("comment"));
				scoreCardSecondPageData.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardSecondPages() > There was an error getting score card second page",
					e);
		}
		LOG.debug(
				"<< getScoreCardSecondPages():scoreCardSecondPageData.size={}",
				scoreCardSecondPageData.size());
		return scoreCardSecondPageData;
	}

	@Override
	public int saveScoreCardThirdPageOutput(Map<String, String> keyValues) {
		LOG.debug(">> saveScoreCardThirdPageOutput(Score card third page: {})",
				keyValues);
		String query = "INSERT INTO score_card_cache_third_page_output "
				+ "(project_id, output_green, output_yellow, output_red, name, comment)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[6];
			values[0] = keyValues.get("project_id");
			values[1] = keyValues.get("output_green");
			values[2] = keyValues.get("output_yellow");
			values[3] = keyValues.get("output_red");
			values[4] = keyValues.get("name");
			values[5] = keyValues.get("comment");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Score card third page output saved {}", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveScoreCardThirdPageOutput() > There was an error trying to saving score card third page output",
					e);
		}
		return -1;
	}

	@Override
	public List<Map<String, String>> getScoreCardThirdPageOutputs(
			String projectId) {
		LOG.debug(">> getScoreCardThirdPageOutputs(projectId={})", projectId);
		List<Map<String, String>> scoreCardThirdPageOutputData = new ArrayList<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_third_page_output where project_id='"
				+ projectId + "' ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> row = new HashMap<>();
				row.put("id", rs.getString("id"));
				row.put("output_green", rs.getString("output_green"));
				row.put("output_yellow", rs.getString("output_yellow"));
				row.put("output_red", rs.getString("output_red"));
				row.put("name", rs.getString("name"));
				row.put("comment", rs.getString("comment"));
				scoreCardThirdPageOutputData.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardThirdPageOutputs() > There was an error getting score card third page output",
					e);
		}
		LOG.debug(
				"<< getScoreCardThirdPageOutputs():scoreCardThirdPageOutputData.size={}",
				scoreCardThirdPageOutputData.size());
		return scoreCardThirdPageOutputData;
	}

	@Override
	public int saveScoreCardThirdPageProject(Map<String, String> keyValues) {
		LOG.debug(
				">> saveScoreCardThirdPageProject(Score card third page project: {})",
				keyValues);
		String query = "INSERT INTO score_card_cache_third_page_project "
				+ "(flagship_id, project_id, title, fl_short_name)" + " VALUES ( ?, ?, ?, ?)";
		try (Connection con = databaseManager.getConnection()) {
			Object[] values = new Object[4];
			values[0] = keyValues.get("flagship_id");
			values[1] = keyValues.get("project_id");
			values[2] = keyValues.get("title");
			values[3] = keyValues.get("fl_short_name");

			int id = databaseManager.makeChangeSecure(con, query, values);

			if (id > 0) {
				LOG.info(">> Score card third page project saved {}", id);
			}
			return id;
		} catch (SQLException e) {
			LOG.error(
					"-- saveScoreCardThirdPage() > There was an error trying to saving score card third page project",
					e);
		}
		return -1;
	}

	@Override
	public List<Map<String, String>> getScoreCardThirdPageProjects(
			int flagshipId) {
		LOG.debug(">> getScoreCardThirdPageProjects(flagshipId={})", flagshipId);
		List<Map<String, String>> scoreCardThirdPageProjectData = new ArrayList<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_third_page_project where flagship_id="
				+ flagshipId + " ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				Map<String, String> row = new HashMap<>();
				row.put("id", rs.getString("id"));
				row.put("project_id", rs.getString("project_id"));
				row.put("title", rs.getString("title"));
				row.put("fl_short_name", rs.getString("fl_short_name"));
				scoreCardThirdPageProjectData.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardThirdPageProjects() > There was an error getting score card third page projects",
					e);
		}
		LOG.debug(
				"<< getScoreCardThirdPageOutputs():scoreCardThirdPageProjectData.size={}",
				scoreCardThirdPageProjectData.size());
		return scoreCardThirdPageProjectData;
	}

	@Override
	public Map<String, String> getScoreCardThirdPageProject(String projectId) {
		LOG.debug(">> getScoreCardThirdPageProject(projectId={})", projectId);
		Map<String, String> scoreCardThirdPageProjectData = new HashMap<>();
		String query = " SELECT *"
				+ "  FROM score_card_cache_third_page_project where project_id='"
				+ projectId + "' ORDER BY id";
		try (Connection con = databaseManager.getConnection()) {
			ResultSet rs = databaseManager.makeQuery(query, con);
			while (rs.next()) {
				scoreCardThirdPageProjectData.put("id", rs.getString("id"));
				scoreCardThirdPageProjectData.put("flagship_id", rs.getString("flagship_id"));
				scoreCardThirdPageProjectData.put("title", rs.getString("title"));
				scoreCardThirdPageProjectData.put("fl_short_name", rs.getString("fl_short_name"));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error(
					"-- getScoreCardThirdPageProject() > There was an error getting score card third page project",
					e);
		}
		LOG.debug(
				"<< getScoreCardThirdPageProject():scoreCardThirdPageProjectData.size={}",
				scoreCardThirdPageProjectData.size());
		return scoreCardThirdPageProjectData;
	}
}
