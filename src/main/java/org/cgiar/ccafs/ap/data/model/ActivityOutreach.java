/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

/**
 * @author Manuja
 */
public class ActivityOutreach {

	private int id;
	private String type;
	private String groups;
	private String paricipants;
	private String femaleParticipants;
	private String dateAndLocation;
	private String comments;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the groups
	 */
	public String getGroups() {
		return groups;
	}
	
	/**
	 * @param groups the groups to set
	 */
	public void setGroups(String groups) {
		this.groups = groups;
	}
	
	/**
	 * @return the paricipants
	 */
	public String getParicipants() {
		return paricipants;
	}
	
	/**
	 * @param paricipants the paricipants to set
	 */
	public void setParicipants(String paricipants) {
		this.paricipants = paricipants;
	}
	
	/**
	 * @return the femaleParticipants
	 */
	public String getFemaleParticipants() {
		return femaleParticipants;
	}
	
	/**
	 * @param femaleParticipants the femaleParticipants to set
	 */
	public void setFemaleParticipants(String femaleParticipants) {
		this.femaleParticipants = femaleParticipants;
	}
	
	/**
	 * @return the dateAndLocation
	 */
	public String getDateAndLocation() {
		return dateAndLocation;
	}

	/**
	 * @param dateAndLocation the dateAndLocation to set
	 */
	public void setDateAndLocation(String dateAndLocation) {
		this.dateAndLocation = dateAndLocation;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
}
