/*
 * FILENAME
 *     RegionalBudgetDetails.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Regional Budget details.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegionalBudgetDetails implements Serializable, Difference<RegionalBudgetDetails>
{
    private static final long serialVersionUID = 8509820494959566135L;

    public static final String SOURCE_W1W2 = "W1/W2";
    public static final String SOURCE_W3 = "W3";
    public static final String SOURCE_BILATERAL = "Bilateral";
    public static final String SOURCE_OTHER = "Other";

    private String sourceType = "";
    private double personnel;
    private double operations;
    private double indirectCosts;

    @JsonIgnore
    private double mainBudgetTotal;

    @JsonIgnore
    private double total;

    @Override
	public DiffReport diff(RegionalBudgetDetails t) {
		DiffReport report = new DiffReport();

		if (personnel != t.personnel) {
			report.append(sourceType + ", Presonnel :", "" + personnel, "" + t.personnel);
		}
		if (operations != t.operations) {
			report.append(sourceType + ", Opertations :", "" + operations, "" + t.operations);
		}
		if (indirectCosts != t.indirectCosts) {
			report.append(sourceType + ", Indirect Costs :", "" + indirectCosts, ""
					+ t.indirectCosts);
		}
		
		return report;
	}
    
    /**
     * <p>
     * Getter for sourceType.
     * </p>
     * 
     * @return the sourceType
     */
    public String getSourceType()
    {
        return sourceType;
    }

    /**
     * <p>
     * Setting value for sourceType.
     * </p>
     * 
     * @param sourceType
     *            the sourceType to set
     */
    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    /**
     * <p>
     * Getter for personnel.
     * </p>
     * 
     * @return the personnel
     */
    public double getPersonnel()
    {
        return personnel;
    }

    /**
     * <p>
     * Setting value for personnel.
     * </p>
     * 
     * @param personnel
     *            the personnel to set
     */
    public void setPersonnel(double personnel)
    {
        this.personnel = personnel;
    }

    /**
     * <p>
     * Getter for operations.
     * </p>
     * 
     * @return the operations
     */
    public double getOperations()
    {
        return operations;
    }

    /**
     * <p>
     * Setting value for operations.
     * </p>
     * 
     * @param operations
     *            the operations to set
     */
    public void setOperations(double operations)
    {
        this.operations = operations;
    }

    /**
     * <p>
     * Getter for indirectCosts.
     * </p>
     * 
     * @return the indirectCosts
     */
    public double getIndirectCosts()
    {
        return indirectCosts;
    }

    /**
     * <p>
     * Setting value for indirectCosts.
     * </p>
     * 
     * @param indirectCosts
     *            the indirectCosts to set
     */
    public void setIndirectCosts(double indirectCosts)
    {
        this.indirectCosts = indirectCosts;
    }

    /**
     * <p>
     * Getter for total.
     * </p>
     * 
     * @return the total
     */
    public double getTotal()
    {
        return (this.personnel + this.operations + this.indirectCosts);
    }

    /**
     * <p>
     * Setting value for total.
     * </p>
     * 
     * @param total
     *            the total to set
     */
    public void setTotal(double total)
    {
        this.total = total;
    }

    /**
     * <p>
     * Getter for mainBudgetTotal.
     * </p>
     * 
     * @return the mainBudgetTotal
     */
    public double getMainBudgetTotal()
    {
        return mainBudgetTotal;
    }

    /**
     * <p>
     * Setting value for mainBudgetTotal.
     * </p>
     * 
     * @param mainBudgetTotal
     *            the mainBudgetTotal to set
     */
    public void setMainBudgetTotal(double mainBudgetTotal)
    {
        this.mainBudgetTotal = mainBudgetTotal;
    }

    /**
     * <p>
     * Check budget values validity
     * </p>
     *
     * @return boolean
     *
     */
    @JsonIgnore
    public boolean validTotal()
    {
        return this.mainBudgetTotal == this.getTotal();
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

}
