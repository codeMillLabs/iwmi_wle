/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FlagshipCluster {
	
	private int id;
	private int clusterId;
	private String spentByJune;
	private double draftExpenditure;
	private String overallStatus;
	private String rating;
	private Map<String, ClusterOutputDetail> clusterOutputMap = new TreeMap<String, ClusterOutputDetail>();
	
	private List<CommentDto> outputComments = new ArrayList<>();
	
	private String name;
	private int activityCount;
	private double budget;
	private String managerComment;
	private String directorComment;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the clusterId
	 */
	public int getClusterId() {
		return clusterId;
	}

	/**
	 * @param clusterId the clusterId to set
	 */
	public void setClusterId(int clusterId) {
		this.clusterId = clusterId;
	}

	/**
	 * @return the spentByJune
	 */
	public String getSpentByJune() {
		return spentByJune;
	}
	
	/**
	 * @param spentByJune the spentByJune to set
	 */
	public void setSpentByJune(String spentByJune) {
		this.spentByJune = spentByJune;
	}
	
	/**
	 * @return the overallStatus
	 */
	public String getOverallStatus() {
		return overallStatus;
	}
	/**
	 * @param overallStatus the overallStatus to set
	 */
	public void setOverallStatus(String overallStatus) {
		this.overallStatus = overallStatus;
	}
	
	/**
	 * @return the rating
	 */
	public String getRating() {
		return rating;
	}
	
	/**
	 * @param rating the rating to set
	 */
	public void setRating(String rating) {
		this.rating = rating;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the activityCount
	 */
	public int getActivityCount() {
		return activityCount;
	}

	/**
	 * @param activityCount the activityCount to set
	 */
	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
	}

	/**
	 * @return the budget
	 */
	public double getBudget() {
		return budget;
	}

	/**
	 * @param budget the budget to set
	 */
	public void setBudget(double budget) {
		this.budget = budget;
	}

	/**
	 * @return the clusterOutputMap
	 */
	public Map<String, ClusterOutputDetail> getClusterOutputMap() {
		return clusterOutputMap;
	}

	/**
	 * @param clusterOutputMap the clusterOutputMap to set
	 */
	public void setClusterOutputMap(
			Map<String, ClusterOutputDetail> clusterOutputMap) {
		this.clusterOutputMap = clusterOutputMap;
	}
	
	/**
	 * @param clusterOutputMap the clusterOutputMap to set
	 */
	public void addClusterOutputDetail(
			ClusterOutputDetail clusterOutputDetail) {
		this.clusterOutputMap.put(clusterOutputDetail.getCode(), clusterOutputDetail);
	}

	/**
	 * @return the outputComment
	 */
	public List<CommentDto> getOutputComments() {
		return outputComments;
	}

	/**
	 * @param outputComment the outputComment to set
	 */
	public void setOutputComments(List<CommentDto> outputComments) {
		this.outputComments = outputComments;
	}
	
	/**
	 * @param outputComment the outputComment to set
	 */
	public void addOutputComment(CommentDto outputComment) {
		this.outputComments.add(outputComment);
	}

	/**
	 * @return the managerComment
	 */
	public String getManagerComment() {
		return managerComment;
	}

	/**
	 * @param managerComment the managerComment to set
	 */
	public void setManagerComment(String managerComment) {
		this.managerComment = managerComment;
	}

	/**
	 * @return the directorComment
	 */
	public String getDirectorComment() {
		return directorComment;
	}

	/**
	 * @param directorComment the directorComment to set
	 */
	public void setDirectorComment(String directorComment) {
		this.directorComment = directorComment;
	}

	/**
	 * @return the draftExpenditure
	 */
	public double getDraftExpenditure() {
		return draftExpenditure;
	}

	/**
	 * @param draftExpenditure the draftExpenditure to set
	 */
	public void setDraftExpenditure(double draftExpenditure) {
		this.draftExpenditure = draftExpenditure;
	}
}