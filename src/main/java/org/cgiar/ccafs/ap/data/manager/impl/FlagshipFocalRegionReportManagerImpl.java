/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cgiar.ccafs.ap.data.dao.FlagshipFocalRegionReportDAO;
import org.cgiar.ccafs.ap.data.dao.UserDAO;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionReportManager;
import org.cgiar.ccafs.ap.data.model.ActivityClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutputDetail;
import org.cgiar.ccafs.ap.data.model.CommentDto;
import org.cgiar.ccafs.ap.data.model.FSFLRReportComment;
import org.cgiar.ccafs.ap.data.model.FlagshipCluster;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegionReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class FlagshipFocalRegionReportManagerImpl implements
		FlagshipFocalRegionReportManager {

	private static final Logger LOG = LoggerFactory
			.getLogger(FlagshipFocalRegionReportManagerImpl.class);

	private FlagshipFocalRegionReportDAO flagshipFocalRegionReportDAO;
	private UserDAO userDAO;

	@Inject
	public FlagshipFocalRegionReportManagerImpl(
			FlagshipFocalRegionReportDAO flagshipFocalRegionReportDAO,
			UserDAO userDAO) {
		this.flagshipFocalRegionReportDAO = flagshipFocalRegionReportDAO;
		this.userDAO = userDAO;
	}

	@Override
	public FlagshipFocalRegionReport getReport(final int flagshipId,
			final int year, final String period) {
		Map<String, String> flagshipReportData = flagshipFocalRegionReportDAO
				.getFlagshipFocalRegionReport(flagshipId, year, period);
		if (!flagshipReportData.isEmpty()) {

			FlagshipFocalRegionReport fsfrReport = new FlagshipFocalRegionReport();
			fsfrReport.setId(Integer.parseInt(flagshipReportData.get("id")));
			fsfrReport.setFlagshipId(flagshipId);
			fsfrReport.setYear(year);
			fsfrReport.setPeriod(period);
			fsfrReport.setState(flagshipReportData.get("state"));
			fsfrReport.setRevision(Integer.parseInt(flagshipReportData
					.get("revision")));
			fsfrReport.setOverallDescription(flagshipReportData
					.get("description"));
			fsfrReport
					.setManagerComment(flagshipReportData.get("mngr_comment"));
			fsfrReport
					.setDirectorComment(flagshipReportData.get("dir_comment"));
			fsfrReport.setManagerReviewDone(Integer.parseInt(flagshipReportData
					.get("mngr_done")));
			fsfrReport.setDirectorReviewDone(Integer
					.parseInt(flagshipReportData.get("dir_done")));

			List<Map<String, String>> flagshipClusters = flagshipFocalRegionReportDAO
					.getFlagshipClusters(fsfrReport.getId());
			for (Map<String, String> flagshipClusterMap : flagshipClusters) {
				FlagshipCluster flagshipCluster = new FlagshipCluster();
				flagshipCluster.setId(Integer.parseInt(flagshipClusterMap
						.get("id")));
				flagshipCluster.setSpentByJune(flagshipClusterMap.get("spent"));
				flagshipCluster.setRating(flagshipClusterMap.get("rating"));
				flagshipCluster.setOverallStatus(flagshipClusterMap
						.get("status"));
				flagshipCluster.setName(flagshipClusterMap.get("name"));
				flagshipCluster.setActivityCount(Integer
						.parseInt(flagshipClusterMap.get("act_count")));
				flagshipCluster.setBudget(Double.parseDouble(flagshipClusterMap
						.get("budget")));
				flagshipCluster.setManagerComment(flagshipClusterMap
						.get("mngr_comment"));
				flagshipCluster.setDirectorComment(flagshipClusterMap
						.get("dir_comment"));
				flagshipCluster.setDraftExpenditure(Double
						.parseDouble(flagshipClusterMap.get("exp")));
				flagshipCluster.setClusterId(Integer
						.parseInt(flagshipClusterMap.get("clus_id")));

				fsfrReport.addFlagshipCluster(flagshipCluster);

				List<Map<String, String>> clusterOutputDetails = flagshipFocalRegionReportDAO
						.getClusterDetails(flagshipCluster.getId());
				for (Map<String, String> clusterOutputDetailMap : clusterOutputDetails) {
					ClusterOutputDetail clusterOutputDetail = new ClusterOutputDetail();
					clusterOutputDetail.setId(Integer
							.parseInt(clusterOutputDetailMap.get("id")));
					clusterOutputDetail.setCode(clusterOutputDetailMap
							.get("code"));

					CommentDto comment = new CommentDto();
					comment.setCode(clusterOutputDetail.getCode());
					comment.setComment(clusterOutputDetailMap.get("comment"));
					comment.setManagerComment(clusterOutputDetailMap
							.get("mngr_comment"));
					comment.setDirectorComment(clusterOutputDetailMap
							.get("dir_comment"));
					flagshipCluster.addOutputComment(comment);

					clusterOutputDetail.setComment(clusterOutputDetailMap
							.get("comment"));
					clusterOutputDetail.setDescription(clusterOutputDetailMap
							.get("description"));
					clusterOutputDetail
							.setManagerComment(clusterOutputDetailMap
									.get("mngr_comment"));
					clusterOutputDetail
							.setDirectorComment(clusterOutputDetailMap
									.get("dir_comment"));
					flagshipCluster.addClusterOutputDetail(clusterOutputDetail);

					List<Map<String, String>> outputActivities = flagshipFocalRegionReportDAO
							.getOutputActivities(clusterOutputDetail.getId());
					for (Map<String, String> outputActivityMap : outputActivities) {
						ActivityClusterOutput actClusterOutput = new ActivityClusterOutput();
						actClusterOutput.setId(Integer
								.parseInt(outputActivityMap.get("id")));
						actClusterOutput.setActivityId(outputActivityMap
								.get("activity_id"));
						actClusterOutput.setActivityName(outputActivityMap
								.get("activity_name"));
						clusterOutputDetail
								.addActivityCluster(actClusterOutput);

						List<Map<String, String>> clusterOutputs = flagshipFocalRegionReportDAO
								.getClusterOutputs(actClusterOutput.getId());
						for (Map<String, String> clusterOutputMap : clusterOutputs) {
							ClusterOutput clusterOutput = new ClusterOutput();
							clusterOutput.setId(Integer
									.parseInt(clusterOutputMap.get("id")));
							clusterOutput.setActivityId(clusterOutputMap
									.get("activity_id"));
							clusterOutput.setActivityName(clusterOutputMap
									.get("activity_name"));
							clusterOutput.setProjectId(clusterOutputMap
									.get("project_id"));
							clusterOutput.setProjectName(clusterOutputMap
									.get("project_name"));
							clusterOutput.setProgress(clusterOutputMap
									.get("progress"));
							clusterOutput.setStatus(clusterOutputMap
									.get("status"));
							clusterOutput.setRating(clusterOutputMap
									.get("rating"));
							actClusterOutput.addClusterOutput(clusterOutput);
						}
					}
				}
			}

			return fsfrReport;
		}

		return null;
	}

	@Override
	public FlagshipFocalRegionReport getReport(final int reportId) {
		Map<String, String> flagshipReportData = flagshipFocalRegionReportDAO
				.getFlagshipFocalRegionReport(reportId);
		if (!flagshipReportData.isEmpty()) {

			FlagshipFocalRegionReport fsfrReport = new FlagshipFocalRegionReport();
			fsfrReport.setId(Integer.parseInt(flagshipReportData.get("id")));
			fsfrReport.setFlagshipId(Integer.parseInt(flagshipReportData.get("flagship_id")));
			fsfrReport.setYear(Integer.parseInt(flagshipReportData.get("year")));
			fsfrReport.setPeriod(flagshipReportData.get("period"));
			fsfrReport.setState(flagshipReportData.get("state"));
			fsfrReport.setRevision(Integer.parseInt(flagshipReportData
					.get("revision")));
			fsfrReport.setOverallDescription(flagshipReportData
					.get("description"));
			fsfrReport
					.setManagerComment(flagshipReportData.get("mngr_comment"));
			fsfrReport
					.setDirectorComment(flagshipReportData.get("dir_comment"));
			fsfrReport.setManagerReviewDone(Integer.parseInt(flagshipReportData
					.get("mngr_done")));
			fsfrReport.setDirectorReviewDone(Integer
					.parseInt(flagshipReportData.get("dir_done")));

			List<Map<String, String>> flagshipClusters = flagshipFocalRegionReportDAO
					.getFlagshipClusters(fsfrReport.getId());
			for (Map<String, String> flagshipClusterMap : flagshipClusters) {
				FlagshipCluster flagshipCluster = new FlagshipCluster();
				flagshipCluster.setId(Integer.parseInt(flagshipClusterMap
						.get("id")));
				flagshipCluster.setSpentByJune(flagshipClusterMap.get("spent"));
				flagshipCluster.setRating(flagshipClusterMap.get("rating"));
				flagshipCluster.setOverallStatus(flagshipClusterMap
						.get("status"));
				flagshipCluster.setName(flagshipClusterMap.get("name"));
				flagshipCluster.setActivityCount(Integer
						.parseInt(flagshipClusterMap.get("act_count")));
				flagshipCluster.setBudget(Double.parseDouble(flagshipClusterMap
						.get("budget")));
				flagshipCluster.setManagerComment(flagshipClusterMap
						.get("mngr_comment"));
				flagshipCluster.setDirectorComment(flagshipClusterMap
						.get("dir_comment"));
				flagshipCluster.setDraftExpenditure(Double
						.parseDouble(flagshipClusterMap.get("exp")));
				flagshipCluster.setClusterId(Integer
						.parseInt(flagshipClusterMap.get("clus_id")));

				fsfrReport.addFlagshipCluster(flagshipCluster);

				List<Map<String, String>> clusterOutputDetails = flagshipFocalRegionReportDAO
						.getClusterDetails(flagshipCluster.getId());
				for (Map<String, String> clusterOutputDetailMap : clusterOutputDetails) {
					ClusterOutputDetail clusterOutputDetail = new ClusterOutputDetail();
					clusterOutputDetail.setId(Integer
							.parseInt(clusterOutputDetailMap.get("id")));
					clusterOutputDetail.setCode(clusterOutputDetailMap
							.get("code"));

					CommentDto comment = new CommentDto();
					comment.setCode(clusterOutputDetail.getCode());
					comment.setComment(clusterOutputDetailMap.get("comment"));
					comment.setManagerComment(clusterOutputDetailMap
							.get("mngr_comment"));
					comment.setDirectorComment(clusterOutputDetailMap
							.get("dir_comment"));
					flagshipCluster.addOutputComment(comment);

					clusterOutputDetail.setComment(clusterOutputDetailMap
							.get("comment"));
					clusterOutputDetail.setDescription(clusterOutputDetailMap
							.get("description"));
					clusterOutputDetail
							.setManagerComment(clusterOutputDetailMap
									.get("mngr_comment"));
					clusterOutputDetail
							.setDirectorComment(clusterOutputDetailMap
									.get("dir_comment"));
					flagshipCluster.addClusterOutputDetail(clusterOutputDetail);

					List<Map<String, String>> outputActivities = flagshipFocalRegionReportDAO
							.getOutputActivities(clusterOutputDetail.getId());
					for (Map<String, String> outputActivityMap : outputActivities) {
						ActivityClusterOutput actClusterOutput = new ActivityClusterOutput();
						actClusterOutput.setId(Integer
								.parseInt(outputActivityMap.get("id")));
						actClusterOutput.setActivityId(outputActivityMap
								.get("activity_id"));
						actClusterOutput.setActivityName(outputActivityMap
								.get("activity_name"));
						clusterOutputDetail
								.addActivityCluster(actClusterOutput);

						List<Map<String, String>> clusterOutputs = flagshipFocalRegionReportDAO
								.getClusterOutputs(actClusterOutput.getId());
						for (Map<String, String> clusterOutputMap : clusterOutputs) {
							ClusterOutput clusterOutput = new ClusterOutput();
							clusterOutput.setId(Integer
									.parseInt(clusterOutputMap.get("id")));
							clusterOutput.setActivityId(clusterOutputMap
									.get("activity_id"));
							clusterOutput.setActivityName(clusterOutputMap
									.get("activity_name"));
							clusterOutput.setProjectId(clusterOutputMap
									.get("project_id"));
							clusterOutput.setProjectName(clusterOutputMap
									.get("project_name"));
							clusterOutput.setProgress(clusterOutputMap
									.get("progress"));
							clusterOutput.setStatus(clusterOutputMap
									.get("status"));
							clusterOutput.setRating(clusterOutputMap
									.get("rating"));
							actClusterOutput.addClusterOutput(clusterOutput);
						}
					}
				}
			}

			return fsfrReport;
		}

		return null;
	}

	@Override
	public int saveReport(
			final FlagshipFocalRegionReport flagshipFocalRegionReport) {
		deleteReport(flagshipFocalRegionReport);

		Map<String, String> flagshipReportData = new HashMap<String, String>();
		flagshipReportData.put("year",
				String.valueOf(flagshipFocalRegionReport.getYear()));
		flagshipReportData.put("flagship_id",
				String.valueOf(flagshipFocalRegionReport.getFlagshipId()));
		flagshipReportData.put("period", flagshipFocalRegionReport.getPeriod());
		flagshipReportData.put("description",
				flagshipFocalRegionReport.getOverallDescription());
		flagshipReportData.put("state", flagshipFocalRegionReport.getState());
		flagshipReportData.put("revision",
				String.valueOf(flagshipFocalRegionReport.getRevision()));
		flagshipReportData.put("mngr_comment",
				flagshipFocalRegionReport.getManagerComment());
		flagshipReportData.put("dir_comment",
				flagshipFocalRegionReport.getDirectorComment());
		flagshipReportData.put("mngr_done", String
				.valueOf(flagshipFocalRegionReport.getManagerReviewDone()));
		flagshipReportData.put("dir_done", String
				.valueOf(flagshipFocalRegionReport.getDirectorReviewDone()));

		int reportId = flagshipFocalRegionReport.getId();
		if (reportId <= 0) {

			reportId = flagshipFocalRegionReportDAO
					.saveFlagshipFocalRegionReport(flagshipReportData);
		} else {
			flagshipFocalRegionReportDAO.updateFlagshipFocalRegionReport(
					reportId, flagshipReportData);
		}

		for (FlagshipCluster flagshipCluster : flagshipFocalRegionReport
				.getFlagshipClusters()) {
			Map<String, String> flagshipClusterData = new HashMap<String, String>();
			flagshipClusterData.put("spent", flagshipCluster.getSpentByJune());
			flagshipClusterData.put("status",
					flagshipCluster.getOverallStatus());
			flagshipClusterData.put("rating", flagshipCluster.getRating());
			flagshipClusterData.put("name", flagshipCluster.getName());
			flagshipClusterData.put("act_count",
					String.valueOf(flagshipCluster.getActivityCount()));
			flagshipClusterData.put("budget",
					String.valueOf(flagshipCluster.getBudget()));
			flagshipClusterData.put("mngr_comment",
					flagshipCluster.getManagerComment());
			flagshipClusterData.put("dir_comment",
					flagshipCluster.getDirectorComment());
			flagshipClusterData.put("exp",
					String.valueOf(flagshipCluster.getDraftExpenditure()));
			flagshipClusterData.put("clus_id",
					String.valueOf(flagshipCluster.getClusterId()));

			int clusterId = flagshipFocalRegionReportDAO.saveFlagshipCluster(
					flagshipClusterData, reportId);

			for (Entry<String, ClusterOutputDetail> entry : flagshipCluster
					.getClusterOutputMap().entrySet()) {

				ClusterOutputDetail outputDetail = entry.getValue();
				Map<String, String> outputDetailData = new HashMap<String, String>();
				outputDetailData.put("code", outputDetail.getCode());
				outputDetailData.put("description",
						outputDetail.getDescription());
				outputDetailData.put("mngr_comment",
						outputDetail.getManagerComment());
				outputDetailData.put("dir_comment",
						outputDetail.getDirectorComment());

				for (CommentDto comment : flagshipCluster.getOutputComments()) {
					if (comment.getCode().equals(outputDetail.getCode())) {
						outputDetailData.put("comment", comment.getComment());
						outputDetailData.put("mngr_comment",
								comment.getManagerComment());
						outputDetailData.put("dir_comment",
								comment.getDirectorComment());
						break;
					}
				}

				int detailId = flagshipFocalRegionReportDAO.saveClusterDetails(
						outputDetailData, clusterId);

				for (Entry<String, ActivityClusterOutput> actEntry : outputDetail
						.getActivityMap().entrySet()) {

					ActivityClusterOutput outputActivity = actEntry.getValue();
					Map<String, String> outputActivityData = new HashMap<String, String>();
					outputActivityData.put("activity_id",
							outputActivity.getActivityId());
					outputActivityData.put("activity_name",
							outputActivity.getActivityName());
					int outputActivityId = flagshipFocalRegionReportDAO
							.saveOutputActivities(outputActivityData, detailId);

					for (ClusterOutput clusterOutput : outputActivity
							.getClusterOutputs()) {
						Map<String, String> clusterOutputData = new HashMap<String, String>();
						clusterOutputData.put("activity_id",
								clusterOutput.getActivityId());
						clusterOutputData.put("activity_name",
								clusterOutput.getActivityName());
						clusterOutputData.put("project_id",
								clusterOutput.getProjectId());
						clusterOutputData.put("project_name",
								clusterOutput.getProjectName());
						clusterOutputData.put("progress",
								clusterOutput.getProgress());
						clusterOutputData.put("status",
								clusterOutput.getStatus());
						clusterOutputData.put("rating",
								clusterOutput.getRating());
						flagshipFocalRegionReportDAO.saveClusterOutputs(
								clusterOutputData, outputActivityId);
					}
				}
			}
		}

		return reportId;
	}

	private boolean deleteReport(
			final FlagshipFocalRegionReport flagshipFocalRegionReport) {
		for (FlagshipCluster flagshipCluster : flagshipFocalRegionReport
				.getFlagshipClusters()) {
			for (Entry<String, ClusterOutputDetail> entry : flagshipCluster
					.getClusterOutputMap().entrySet()) {
				ClusterOutputDetail outputDetail = entry.getValue();
				for (Entry<String, ActivityClusterOutput> actEntry : outputDetail
						.getActivityMap().entrySet()) {
					ActivityClusterOutput outputActivity = actEntry.getValue();

					flagshipFocalRegionReportDAO
							.deleteClusterOutputs(outputActivity.getId());
					flagshipFocalRegionReportDAO
							.deleteOutputActivities(outputDetail.getId());
				}
				flagshipFocalRegionReportDAO
						.deleteClusterDetails(flagshipCluster.getId());
			}
			flagshipFocalRegionReportDAO
					.deleteFlagshipClusters(flagshipFocalRegionReport.getId());
		}
		return true;
	}

	@Override
	public boolean updateState(int reportId, String newState) {
		return flagshipFocalRegionReportDAO.updateState(reportId, newState);
	}

	@Override
	public boolean updateRevision(int reportId) {
		return flagshipFocalRegionReportDAO.updateRevision(reportId);
	}

	@Override
	public List<FSFLRReportComment> getComments(int reportId) {
		List<FSFLRReportComment> comments = new ArrayList<>();
		List<Map<String, String>> flagshipReportCommentData = flagshipFocalRegionReportDAO
				.getComments(reportId);
		for (Map<String, String> commentMap : flagshipReportCommentData) {
			FSFLRReportComment fsflrReportComment = new FSFLRReportComment();
			fsflrReportComment.setId(Integer.parseInt(commentMap.get("id")));
			fsflrReportComment.setComment(commentMap.get("comment"));
			fsflrReportComment.setRevision(Integer.parseInt(commentMap
					.get("revision")));
			fsflrReportComment.setUser(userDAO.getById(Integer
					.parseInt(commentMap.get("user_id"))));
			comments.add(fsflrReportComment);
		}
		return comments;
	}

	@Override
	public List<FSFLRReportComment> getComments(int reportId, int revision) {
		List<FSFLRReportComment> comments = new ArrayList<>();
		List<Map<String, String>> flagshipReportCommentData = flagshipFocalRegionReportDAO
				.getComments(reportId, revision);
		for (Map<String, String> commentMap : flagshipReportCommentData) {
			FSFLRReportComment fsflrReportComment = new FSFLRReportComment();
			fsflrReportComment.setId(Integer.parseInt(commentMap.get("id")));
			fsflrReportComment.setComment(commentMap.get("comment"));
			fsflrReportComment.setRevision(Integer.parseInt(commentMap
					.get("revision")));
			fsflrReportComment.setUser(userDAO.getById(Integer
					.parseInt(commentMap.get("user_id"))));
			comments.add(fsflrReportComment);
		}
		return comments;
	}

	@Override
	public int saveComment(int reportId, FSFLRReportComment comment) {
		Map<String, String> commentDataMap = new HashMap<String, String>();
		commentDataMap
				.put("user_id", String.valueOf(comment.getUser().getId()));
		commentDataMap.put("comment", comment.getComment());
		commentDataMap.put("revision", String.valueOf(comment.getRevision()));
		return flagshipFocalRegionReportDAO.saveComment(reportId,
				commentDataMap);
	}
}