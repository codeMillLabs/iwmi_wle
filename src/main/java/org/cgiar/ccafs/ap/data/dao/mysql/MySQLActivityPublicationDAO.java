/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.ActivityPublicationDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MySQLActivityPublicationDAO implements ActivityPublicationDAO {

	// Looging
	private static final Logger LOG = LoggerFactory
			.getLogger(MySQLActivityObjectiveDAO.class);
	DAOManager databaseManager;

	@Inject
	public MySQLActivityPublicationDAO(DAOManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public boolean deleteActivityPublications(int activityID) {
		 LOG.debug(">> deleteActivityPublications(activityID={})", activityID);
		    boolean deleted = false;
		    String query = "DELETE FROM activity_publications WHERE activity_id = ?";
		    Object[] values = new Object[1];
		    values[0] = activityID;
		    try (Connection con = databaseManager.getConnection()) {
		      int rows = databaseManager.makeChangeSecure(con, query, values);
		      if (rows < 0) {
		        LOG
		          .warn(
		            "-- deleteActivityPublications() > There was a problem deleting the publications related to the activity {}. \n{}",
		            activityID, query);
		      } else {
		        deleted = true;
		      }
		    } catch (SQLException e) {
		      LOG.error(
		        "-- deleteActivityPublications() > There was an error deleting the publications related to the activity {}",
		        activityID, e);
		    }
		    LOG.debug("<< deleteActivityPublications():{}", deleted);
		    return deleted;
	}

	@Override
	public List<Map<String, String>> getActivityPublications(int activityID) {
		 LOG.debug(">> getActivityPublications(activityID={})", activityID);
		    List<Map<String, String>> activityPublicationsDataList = new ArrayList<>();
		    String query = " SELECT id, publication_reference, isi_journal, peer_reviewed, web_link"
		    		+ "  FROM activity_publications WHERE activity_id = " + activityID;
		    try (Connection con = databaseManager.getConnection()) {
		      ResultSet rs = databaseManager.makeQuery(query, con);
		      while (rs.next()) {
		        Map<String, String> activityPublicationsData = new HashMap<>();
		        activityPublicationsData.put("id", rs.getString("id"));
		        activityPublicationsData.put("publication_reference", rs.getString("publication_reference"));
		        activityPublicationsData.put("isi_journal", rs.getString("isi_journal"));
		        activityPublicationsData.put("peer_reviewed", rs.getString("peer_reviewed"));
		        activityPublicationsData.put("web_link", rs.getString("web_link"));
		        
		        activityPublicationsDataList.add(activityPublicationsData);
		      }
		      rs.close();
		    } catch (SQLException e) {
		      LOG.error("-- getActivityPublications() > There was an error getting the activity publications for activity {}",
		        activityID, e);
		    }
		    LOG.debug("<< getActivityPublications():activityPublicationsDataList.size={}", activityPublicationsDataList.size());
		    return activityPublicationsDataList;
	}

	@Override
	public int saveActivityPublications(Map<String, String> publications,
			int activityID) {
		LOG.debug(">> saveActivityObjectives(publications={}, activityID={})", publications, activityID);
	    boolean saved = false;
	    String query =
	      " INSERT INTO activity_publications "
	      + "( id,  activity_id, publication_reference, isi_journal, peer_reviewed, web_link)"
	      + " VALUES (?, ?, ?, ?, ?, ?) "
	      + " ON DUPLICATE KEY UPDATE publication_reference = VALUES(publication_reference), isi_journal = VAlUES(isi_journal), "
	      + " activity_id = VALUES(activity_id), peer_reviewed = VALUES(peer_reviewed), web_link = VALUES(web_link)";
	    
	    Object[] values = new Object[6];
	    values[0] = publications.get("id");
	    values[1] = activityID;
	    values[2] = publications.get("publication_reference");
	    values[3] = publications.get("isi_journal");
	    values[4] = publications.get("peer_reviewed");
	    values[5] = publications.get("web_link");
	  
	    try (Connection con = databaseManager.getConnection()) {
	      int rows = databaseManager.makeChangeSecure(con, query, values);
	      
	      return rows;
	    } catch (SQLException e) {
	      LOG.error("-- saveActivityPublications() > There was an error saving publications for activity {}.", activityID);
	    }

	    LOG.debug("<< saveActivityPublications():{}", saved);
	    return -1;
	}

}
