/*
 * FILENAME
 *     ActivityReviewerDAO.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.User;

import com.google.inject.ImplementedBy;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
@ImplementedBy(MySQLActivityReviewerDAO.class)
public interface ActivityReviewerDAO
{
    /**
     * Save reviewers information into the DAO
     * 
     * @param budgetData
     *            - data to be saved
     * @return true if the data was successfully saved, false otherwise.
     */
    public void saveActivityReviewers(int activityId, String revisionId, List<ActivityReview> reviewers);

    /**
     * update reviewers information into the DAO
     * 
     * @param budgetData
     *            - data to be updated
     */
    public void addActivityReviewers(int activityId, String revisionId, String type, List<ActivityReview> reviewers);
    
    /**
     * update reviewers information into the DAO
     * 
     * @param budgetData
     *            - data to be updated
     */
    public void updateActivityReviewerStatus(int activityId, String revisionId, String type, User user, ActivityReview reviewer);

    /**
     * 
     * <p>
     * Retrieve activity reviewers by activity id;
     * </p>
     * 
     * @param activityId
     * @return
     *
     */
    public List<ActivityReview> getReviewers(int activityId, String revisionId, String type);
    
    public List<ActivityReview> hasAnyPendingReviews(int activityId, String revisionId, String type);
    
    public List<ActivityReview> hasPendingReviewsForUser(int activityId, String revisionId, String type, User user);

}
