/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.data.model.User.UserRole.Admin;
import static org.cgiar.ccafs.ap.data.model.User.UserRole.CA;
import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Activity implements Comparable<Activity>, Difference<Activity>
{
    private int id;
    private String activityId;
    private int leadCenterBasedId;
    private int year;
    private String title;
    private Date startDate;
    private Date endDate;
    private String description;
    private Milestone milestone;
    private Leader leader;
    private boolean isEditable;
    private boolean isPlanning;
    private boolean isGlobal;
    private boolean isValidated;
    private boolean hasPartners;
    private List<ContactPerson> contactPersons;
    private String genderIntegrationsDescription;
    private Status status;
    private ActivityBudget budget;
    private String statusDescription;
    private List<Deliverable> deliverables;
    private List<ActivityPartner> activityPartners;
    private Date dateAdded;
    private List<ActivityObjective> objectives;
    private List<Resource> resources;
    private List<ActivityKeyword> keywords;
    private List<Country> countries;
    private List<Region> regions;
    private List<BenchmarkSite> bsLocations;
    private List<OtherSite> otherLocations;
    private boolean commissioned;
    private Activity continuousActivity;
    private List<ActivityWLEDevelopment> wles;
    private List<ActivityReview> internalReviewers = new ArrayList<>();
    private List<ActivityReview> externalReviewers = new ArrayList<>();
    private List<Integer> activityInternalReviewers;
    private List<Integer> activityExternalReviewers;

    private String leadCenter;
    private String projectName;
    private String genderResearchDesc;
    private String ecoSystemsDesc;
    private String flagShipSRP = "";
    private String activityCluster = "";
    private double budgetTotal;
    private double budgetWindow1;
    private double budgetWindow2;
    private double budgetBiLateral;

    private double genderBudgetTotal;
    private double genderBudgetWindow1;
    private double genderBudgetWindow2;
    private double genderBudgetBiLateral;

    private ActivityStatus activityStatus;
    private String revisionId;
    private String createdUser;

    // current logged in user
    private User loggedInUser;

    private boolean canDelete = true;
    private boolean pendingReview = false;
    private boolean toBeSubmittedToCA = false;
    private boolean reviewsCompleted = false;

    private List<String> donors = new ArrayList<>();

    private List<ActivityIndicator> indicators;

    private List<ActivityIndicator> ktdIndicators;
    private List<ActivityIndicator> ceaipIndicators;
    private List<ActivityIndicator> tpivsdIndicators;
    private List<ActivityIndicator> pvsdIndicators;
    private List<ActivityIndicator> otgIndicators;

    private List<ActivityPublication> publications;
    private List<ActivityMaterial> materials;
    private List<ActivityOutreach> outreachs;

    private ActivityReviewRating midYearReviews;

    public Activity()
    {
        donors = Arrays.asList(new String[] {
            "", "", "", "", "", "", "", ""
        });
    }

    private static final String LEAD_CENTER = "Lead Center";
    private static final String YEAR = "Year";
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Description";
    private static final String PROJECT_NAME_IN_CENTER = "Corresponding Project name in the center";
    private static final String FLAGSHIP = "Flagship";
    private static final String ACTIVITY_CLUSTER = "Activity Cluster";
    private static final String DESC_ECO_SERVICES = "Description of Ecosystems Services";
    private static final String START_DATE = "Start Date";
    private static final String END_DATE = "End Date";
    private static final String GENDER_INTEG = "Gender Integration";
    private static final String GENDER_INTEG_DESC = "Description of Gender Research";
    private static final String INTERNAL_REVIEWS = "Internal Reviewers";
    private static final String EXTERNAL_REVIEWS = "External Reviewers";

    @Override
    public DiffReport diff(Activity a)
    {
        DiffReport report = new DiffReport();

        if (isNotEqual(this.leadCenter, a.leadCenter))
        {
            report.append(LEAD_CENTER, this.leadCenter, a.leadCenter);
        }
        if (this.year != a.year)
        {
            report.append(YEAR, "" + this.year, "" + a.year);
        }
        if (isNotEqual(this.title, a.title))
        {
            report.append(TITLE, this.title, a.title);
        }
        if (isNotEqual(this.description, a.description))
        {
            report.append(DESCRIPTION, this.description, a.description);
        }
        if (isNotEqual(this.projectName, a.projectName))
        {
            report.append(PROJECT_NAME_IN_CENTER, this.projectName, a.projectName);
        }
        if (isNotEqual(this.flagShipSRP, a.flagShipSRP))
        {
            report.append(FLAGSHIP, this.flagShipSRP, a.flagShipSRP);
        }
        if (isNotEqual(this.activityCluster, a.activityCluster))
        {
            report.append(ACTIVITY_CLUSTER, this.activityCluster, a.activityCluster);
        }
        if (isNotEqual(this.ecoSystemsDesc, a.ecoSystemsDesc))
        {
            report.append(DESC_ECO_SERVICES, this.ecoSystemsDesc, a.ecoSystemsDesc);
        }
        if (isNotEqual(this.startDate, a.startDate))
        {
            report.append(START_DATE, "" + this.startDate, "" + "" + a.startDate);
        }
        if (isNotEqual(this.endDate, a.endDate))
        {
            report.append(END_DATE, "" + this.endDate, "" + a.endDate);
        }
        if (isNotEqual(this.genderIntegrationsDescription, a.genderIntegrationsDescription))
        {
            report.append(GENDER_INTEG, this.genderIntegrationsDescription, a.genderIntegrationsDescription);
        }
        if (isNotEqual(this.genderResearchDesc, a.genderResearchDesc))
        {
            report.append(GENDER_INTEG_DESC, this.genderResearchDesc, a.genderResearchDesc);
        }

        if (this.contactPersons != null && a.contactPersons != null)
        {
            for (int i = 0; i < a.contactPersons.size(); i++)
            {
                if (i >= this.contactPersons.size())
                {
                    ContactPerson newContact = a.contactPersons.get(i);
                    report.append("New Contact added, Contact :" + newContact.getName() + ", Email :"
                        + newContact.getEmail());
                }
                else
                {
                    ContactPerson prev = this.contactPersons.get(i);
                    ContactPerson current = a.contactPersons.get(i);
                    DiffReport diff = prev.diff(current);
                    report.append(diff);
                }
            }
        }
        if (this.internalReviewers.size() != a.internalReviewers.size())
        {
            report.append(INTERNAL_REVIEWS, getReviewersNameForDiff(internalReviewers),
                getReviewersNameForDiff(a.internalReviewers));
        }
        if (this.externalReviewers.size() != a.externalReviewers.size())
        {
            report.append(EXTERNAL_REVIEWS, getReviewersNameForDiff(externalReviewers),
                getReviewersNameForDiff(a.externalReviewers));
        }

        return report;
    }

    private String getReviewersNameForDiff(List<ActivityReview> reviewUsers)
    {
        String data = "";
        for (ActivityReview r : reviewUsers)
        {
            data += r.getReviewerName() + ", ";
        }
        return data;
    }

    public boolean needValidation()
    {
        ActivityStatus status = this.getActivityStatus();
        switch (status)
        {
            case NEW:
                return true;
            case IN_PROGRESS:
                return true;

            case VALIDATED:
            case INTERNAL_REVIEWING:
            case INTERNAL_CORRECTIONS:
            case SUBMITTED_TO_CA:
            case REVIEWING:
            case REVIEWS_COMPLETED:
            case REVISION:
            case SUBMITTED:
            case COMPLETED:
                return false;
        }
        return false;
    }

    public boolean isReviewer(User user)
    {
        int id = user.getId();
        for (ActivityReview review : getInternalReviewers())
        {
            if (review.getReviewerId() == id)
            {
                return true;
            }
        }

        for (ActivityReview review : getExternalReviewers())
        {
            if (review.getReviewerId() == id)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>
     * Getter for isEditable.
     * </p>
     * 
     * @return the isEditable
     */
    public boolean isEditable()
    {
        if (ActivityStatus.IN_PROGRESS.equals(activityStatus)
            || ActivityStatus.INTERNAL_CORRECTIONS.equals(activityStatus) || ActivityStatus.NEW.equals(activityStatus))
        {
            if (loggedInUser != null)
            {
                isEditable = (createdUser.equals(loggedInUser.getEmail())) ? true : false;
                return isEditable;
            }
        }
        else if (ActivityStatus.INTERNAL_REVIEWING.equals(activityStatus))
        {
            if (loggedInUser != null && hasIntReviewer(loggedInUser))
            {
                isEditable = true;
                return isEditable;
            }
        }
        else if (ActivityStatus.SUBMITTED_TO_CA.equals(activityStatus)
            || ActivityStatus.SUBMITTED.equals(activityStatus))
        {
            if (loggedInUser != null && loggedInUser.hasRole(CA))
            {
                isEditable = true;
                return isEditable;
            }
        }
        else if (ActivityStatus.REVIEWING.equals(activityStatus))
        {
            if (loggedInUser != null && hasExtReviewer(loggedInUser))
            {
                isEditable = true;
                return isEditable;
            }
        }
        else if (ActivityStatus.REVIEWS_COMPLETED.equals(activityStatus))
        {
            if (loggedInUser != null && loggedInUser.hasRole(Admin))
            {
                isEditable = false;
                return isEditable;
            }
        }
        else if (ActivityStatus.SUBMITTED.equals(activityStatus))
        {
            if (loggedInUser != null)
            {
                isEditable = loggedInUser.isAdmin();
                return isEditable;
            }
        }
        return isEditable;
    }

    public boolean hasReviewer(User loggedInUser)
    {
        boolean isValidReviewer = hasIntReviewer(loggedInUser) ? true : hasExtReviewer(loggedInUser);
        return isValidReviewer;
    }

    public boolean hasExtReviewer(User loggedInUser)
    {
        for (ActivityReview reviewer : getExternalReviewers())
        {
            if (reviewer.getReviewerId() == loggedInUser.getId())
            {
                return true;
            }
        }
        return false;
    }

    public boolean hasIntReviewer(User loggedInUser)
    {
        for (ActivityReview reviewer : getInternalReviewers())
        {
            if (reviewer.getReviewerId() == loggedInUser.getId())
            {
                return true;
            }
        }
        return false;
    }

    public List<Integer> getActivityInternalReviewers()
    {
        if (activityInternalReviewers == null)
        {
            activityInternalReviewers = new ArrayList<Integer>();
        }
        return activityInternalReviewers;
    }

    public List<Integer> getActivityExternalReviewers()
    {
        if (activityExternalReviewers == null)
        {
            activityExternalReviewers = new ArrayList<Integer>();
        }
        return activityExternalReviewers;
    }

    public void setActivityExternalReviewers(List<Integer> activityExternalReviewers)
    {
        this.activityExternalReviewers = activityExternalReviewers;
    }

    public void setActivityInternalReviewers(List<Integer> activityInternalReviewers)
    {
        this.activityInternalReviewers = activityInternalReviewers;
    }

    public String getActivityId()
    {
        return activityId;
    }

    public List<ActivityPartner> getActivityPartners()
    {
        return activityPartners;
    }

    public List<String> getBenchmarkSitesIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getBsLocations().size(); c++)
        {
            ids.add(getBsLocations().get(c).getId() + "");
        }
        return ids;
    }

    public List<BenchmarkSite> getBsLocations()
    {
        return bsLocations;
    }

    public ActivityBudget getBudget()
    {
        return budget;
    }

    public List<ContactPerson> getContactPersons()
    {
        return contactPersons;
    }

    public Activity getContinuousActivity()
    {
        return continuousActivity;
    }

    public List<Country> getCountries()
    {
        return countries;
    }

    public List<String> getCountriesIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getCountries().size(); c++)
        {
            ids.add(getCountries().get(c).getId() + "");
        }
        return ids;
    }

    public List<String> getCountriesIdsByRegion(int regionId)
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getCountries().size(); c++)
        {
            if (getCountries().get(c).getRegion().getId() == regionId)
            {
                ids.add(getCountries().get(c).getId() + "");
            }
        }
        return ids;
    }

    public List<String> getCountriesNamesByRegion(int regionId)
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getCountries().size(); c++)
        {
            if (getCountries().get(c).getRegion().getId() == regionId)
            {
                ids.add(getCountries().get(c).getName() + "");
            }
        }
        return ids;
    }

    public Date getDateAdded()
    {
        return dateAdded;
    }

    public List<Deliverable> getDeliverables()
    {
        return deliverables;
    }

    public String getDescription()
    {
        return description;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public String getGenderIntegrationsDescription()
    {
        return genderIntegrationsDescription;
    }

    public int getId()
    {
        return id;
    }

    public List<ActivityKeyword> getKeywords()
    {
        return keywords;
    }

    public List<String> getKeywordsIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getKeywords().size(); c++)
        {
            if (getKeywords().get(c).getKeyword() != null)
            {
                ids.add(getKeywords().get(c).getKeyword().getId() + "");
            }
        }
        return ids;
    }

    public List<ActivityWLEDevelopment> getWles()
    {
        if (wles == null)
            wles = new ArrayList<ActivityWLEDevelopment>();
        return wles;
    }

    public void setWles(List<ActivityWLEDevelopment> wles)
    {
        this.wles = wles;
    }

    public List<String> getWLEDevIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getWles().size(); c++)
        {
            if (getWles().get(c).getWle() != null)
            {
                ids.add("" + getWles().get(c).getWle().getId());
            }
        }
        return ids;
    }

    /**
     * @return the internalReviewers
     */
    public List<ActivityReview> getInternalReviewers()
    {
        if (internalReviewers == null)
            internalReviewers = new ArrayList<ActivityReview>();
        return internalReviewers;
    }

    /**
     * @param internalReviewers
     *            the internalReviewers to set
     */
    public void setInternalReviewers(List<ActivityReview> internalReviewers)
    {
        this.internalReviewers = internalReviewers;
    }

    /**
     * @return the externalReviewers
     */
    public List<ActivityReview> getExternalReviewers()
    {
        if (externalReviewers == null)
            externalReviewers = new ArrayList<ActivityReview>();
        return externalReviewers;
    }

    /**
     * @param externalReviewers
     *            the externalReviewers to set
     */
    public void setExternalReviewers(List<ActivityReview> externalReviewers)
    {
        this.externalReviewers = externalReviewers;
    }

    public List<String> getInternalReviewerIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (ActivityReview reviewer : getInternalReviewers())
        {
            ids.add("" + reviewer.getReviewerId());
        }
        return ids;
    }

    public List<String> getExternalReviewerIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (ActivityReview reviewer : getExternalReviewers())
        {
            ids.add("" + reviewer.getReviewerId());
        }
        return ids;
    }

    public Leader getLeader()
    {
        return leader;
    }

    public Milestone getMilestone()
    {
        return milestone;
    }

    public List<ActivityObjective> getObjectives()
    {
        return objectives;
    }

    public List<OtherSite> getOtherLocations()
    {
        return otherLocations;
    }

    public List<Region> getRegions()
    {
        return regions;
    }

    public List<String> getRegionsIds()
    {
        ArrayList<String> ids = new ArrayList<>();
        for (int c = 0; c < getRegions().size(); c++)
        {
            ids.add(getRegions().get(c).getId() + "");
        }
        return ids;
    }

    public List<Resource> getResources()
    {
        return resources;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public Status getStatus()
    {
        return status;
    }

    public String getStatusDescription()
    {
        return statusDescription;
    }

    public String getTitle()
    {
        return title;
    }

    public int getYear()
    {
        return year;
    }

    public boolean isCommissioned()
    {
        return commissioned;
    }

    public boolean isGlobal()
    {
        return isGlobal;
    }

    public boolean isHasPartners()
    {
        return hasPartners;
    }

    public boolean isPlanning()
    {
        return isPlanning;
    }

    public boolean isValidated()
    {
        return isValidated;
    }

    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }

    public void setActivityPartners(List<ActivityPartner> activityPartners)
    {
        this.activityPartners = activityPartners;
    }

    public void setBsLocations(List<BenchmarkSite> bsLocations)
    {
        this.bsLocations = bsLocations;
    }

    public void setBudget(ActivityBudget budget)
    {
        this.budget = budget;
    }

    public void setCommissioned(boolean isCommissioned)
    {
        this.commissioned = isCommissioned;
    }

    public void setContactPersons(List<ContactPerson> contactPersons)
    {
        this.contactPersons = contactPersons;
    }

    public void setContinuousActivity(Activity continuousActivity)
    {
        this.continuousActivity = continuousActivity;
    }

    public void setCountries(List<Country> countries)
    {
        this.countries = countries;
    }

    public void setDateAdded(Date dateAdded)
    {
        this.dateAdded = dateAdded;
    }

    public void setDeliverables(List<Deliverable> deliverables)
    {
        this.deliverables = deliverables;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setGenderIntegrationsDescription(String genderIntegrationsDescription)
    {
        this.genderIntegrationsDescription = genderIntegrationsDescription;
    }

    public void setGlobal(boolean isGlobal)
    {
        this.isGlobal = isGlobal;
    }

    public void setHasPartners(boolean hasPartners)
    {
        this.hasPartners = hasPartners;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setKeywords(List<ActivityKeyword> keywords)
    {
        this.keywords = keywords;
    }

    public void setLeader(Leader leader)
    {
        this.leader = leader;
    }

    public void setMilestone(Milestone milestone)
    {
        this.milestone = milestone;
    }

    public void setObjectives(List<ActivityObjective> objectives)
    {
        this.objectives = objectives;
    }

    public void setOtherLocations(List<OtherSite> otherLocations)
    {
        this.otherLocations = otherLocations;
    }

    public void setPlanning(boolean isPlanning)
    {
        this.isPlanning = isPlanning;
    }

    public void setRegions(List<Region> regions)
    {
        this.regions = regions;
    }

    public void setResources(List<Resource> resources)
    {
        this.resources = resources;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public void setStatusDescription(String statusDescription)
    {
        this.statusDescription = statusDescription;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setValidated(boolean isValidated)
    {
        this.isValidated = isValidated;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public String getLeadCenter()
    {
        return leadCenter;
    }

    /**
     * @param leadCenter
     *            the leadCenter to set
     */
    public void setLeadCenter(String leadCenter)
    {
        this.leadCenter = leadCenter;
    }

    /**
     * @return the projectName
     */
    public String getProjectName()
    {
        return projectName;
    }

    /**
     * @param projectName
     *            the projectName to set
     */
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    /**
     * @return the genderResearchDesc
     */
    public String getGenderResearchDesc()
    {
        return genderResearchDesc;
    }

    /**
     * @param genderResearchDesc
     *            the genderResearchDesc to set
     */
    public void setGenderResearchDesc(String genderResearchDesc)
    {
        this.genderResearchDesc = genderResearchDesc;
    }

    /**
     * @return the ecoSystemsDesc
     */
    public String getEcoSystemsDesc()
    {
        return ecoSystemsDesc;
    }

    /**
     * @param ecoSystemsDesc
     *            the ecoSystemsDesc to set
     */
    public void setEcoSystemsDesc(String ecoSystemsDesc)
    {
        this.ecoSystemsDesc = ecoSystemsDesc;
    }

    /**
     * @return the flagShipSRP
     */
    public String getFlagShipSRP()
    {
        if (flagShipSRP == null)
            flagShipSRP = "";
        return flagShipSRP;
    }

    /**
     * @param flagShipSRP
     *            the flagShipSRP to set
     */
    public void setFlagShipSRP(String flagShipSRP)
    {
        this.flagShipSRP = flagShipSRP;
    }

    /**
     * @return the activityCluster
     */
    public String getActivityCluster()
    {
        if (activityCluster == null)
            activityCluster = "";
        return activityCluster;
    }

    /**
     * @param activityCluster
     *            the activityCluster to set
     */
    public void setActivityCluster(String activityCluster)
    {
        this.activityCluster = activityCluster;
    }

    /**
     * @return the budgetTotal
     */
    public double getBudgetTotal()
    {
        return budgetTotal;
    }

    /**
     * @param budgetTotal
     *            the budgetTotal to set
     */
    public void setBudgetTotal(double budgetTotal)
    {
        this.budgetTotal = budgetTotal;
    }

    /**
     * @return the budgetWindow1
     */
    public double getBudgetWindow1()
    {
        return budgetWindow1;
    }

    /**
     * @param budgetWindow1
     *            the budgetWindow1 to set
     */
    public void setBudgetWindow1(double budgetWindow1)
    {
        this.budgetWindow1 = budgetWindow1;
    }

    /**
     * @return the budgetWindow2
     */
    public double getBudgetWindow2()
    {
        return budgetWindow2;
    }

    /**
     * @param budgetWindow2
     *            the budgetWindow2 to set
     */
    public void setBudgetWindow2(double budgetWindow2)
    {
        this.budgetWindow2 = budgetWindow2;
    }

    /**
     * @return the budgetBiLateral
     */
    public double getBudgetBiLateral()
    {
        return budgetBiLateral;
    }

    /**
     * @param budgetBiLateral
     *            the budgetBiLateral to set
     */
    public void setBudgetBiLateral(double budgetBiLateral)
    {
        this.budgetBiLateral = budgetBiLateral;
    }

    /**
     * @return the genderBudgetTotal
     */
    public double getGenderBudgetTotal()
    {
        return genderBudgetTotal;
    }

    /**
     * @param genderBudgetTotal
     *            the genderBudgetTotal to set
     */
    public void setGenderBudgetTotal(double genderBudgetTotal)
    {
        this.genderBudgetTotal = genderBudgetTotal;
    }

    /**
     * @return the genderBudgetWindow1
     */
    public double getGenderBudgetWindow1()
    {
        return genderBudgetWindow1;
    }

    /**
     * @param genderBudgetWindow1
     *            the genderBudgetWindow1 to set
     */
    public void setGenderBudgetWindow1(double genderBudgetWindow1)
    {
        this.genderBudgetWindow1 = genderBudgetWindow1;
    }

    /**
     * @return the genderBudgetWindow2
     */
    public double getGenderBudgetWindow2()
    {
        return genderBudgetWindow2;
    }

    /**
     * @param genderBudgetWindow2
     *            the genderBudgetWindow2 to set
     */
    public void setGenderBudgetWindow2(double genderBudgetWindow2)
    {
        this.genderBudgetWindow2 = genderBudgetWindow2;
    }

    /**
     * @return the genderBudgetBiLateral
     */
    public double getGenderBudgetBiLateral()
    {
        return genderBudgetBiLateral;
    }

    /**
     * @param genderBudgetBiLateral
     *            the genderBudgetBiLateral to set
     */
    public void setGenderBudgetBiLateral(double genderBudgetBiLateral)
    {
        this.genderBudgetBiLateral = genderBudgetBiLateral;
    }

    /**
     * @return the donors
     */
    public List<String> getDonors()
    {
        return donors;
    }

    /**
     * @param donors
     *            the donors to set
     */
    public void setDonors(List<String> donors)
    {
        this.donors = donors;
    }

    /**
     * <p>
     * Getter method for createdUser
     * </p>
     * 
     * @return the createdUser
     */
    public String getCreatedUser()
    {
        return createdUser;
    }

    /**
     * <p>
     * Setter method for createdUser
     * </p>
     *
     * @param createdUser
     *            the createdUser to set
     */
    public void setCreatedUser(String createdUser)
    {
        this.createdUser = createdUser;
    }

    /**
     * <p>
     * Getter for activityStatus.
     * </p>
     * 
     * @return the activityStatus
     */
    public ActivityStatus getActivityStatus()
    {
        return activityStatus;
    }

    /**
     * <p>
     * Setting value for activityStatus.
     * </p>
     * 
     * @param activityStatus
     *            the activityStatus to set
     */
    public void setActivityStatus(ActivityStatus activityStatus)
    {
        this.activityStatus = activityStatus;
    }

    /**
     * <p>
     * Getter for revisionId.
     * </p>
     * 
     * @return the revisionId
     */
    public String getRevisionId()
    {
        return revisionId;
    }

    /**
     * <p>
     * Getter for loggedInUser.
     * </p>
     * 
     * @return the loggedInUser
     */
    public User getLoggedInUser()
    {
        return loggedInUser;
    }

    /**
     * <p>
     * Setting value for loggedInUser.
     * </p>
     * 
     * @param loggedInUser
     *            the loggedInUser to set
     */
    public void setLoggedInUser(User loggedInUser)
    {
        this.loggedInUser = loggedInUser;
    }

    /**
     * <p>
     * Setting value for revisionId.
     * </p>
     * 
     * @param revisionId
     *            the revisionId to set
     */
    public void setRevisionId(String revisionId)
    {
        this.revisionId = revisionId;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public boolean isPendingReview()
    {
        return pendingReview;
    }

    public void setPendingReview(boolean pendingReview)
    {
        this.pendingReview = pendingReview;
    }

    public boolean isReviewsCompleted()
    {
        return reviewsCompleted;
    }

    public void setReviewsCompleted(boolean reviewsCompleted)
    {
        this.reviewsCompleted = reviewsCompleted;
    }

    public int getLeadCenterBasedId()
    {
        return leadCenterBasedId;
    }

    public void setLeadCenterBasedId(int leadCenterBasedId)
    {
        this.leadCenterBasedId = leadCenterBasedId;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(Activity o)
    {
        return getFormattedId().compareTo(o.getFormattedId());
    }

    public String getFormattedId()
    {
        String[] idSplittedArray = activityId.split("-");
        String idString = idSplittedArray[0] + "-" + idSplittedArray[1] + "-" + formatId(idSplittedArray[2]);
        return idString;
    }

    private String formatId(String idString)
    {
        idString = idString.trim();
        int size = idString.length();
        for (int x = 3; x > size; x--)
        {
            idString = "0" + idString;
        }
        return idString;
    }

    /**
     * @return the ktdIndicators
     */
    public List<ActivityIndicator> getKtdIndicators()
    {
        return ktdIndicators;
    }

    /**
     * @param ktdIndicators
     *            the ktdIndicators to set
     */
    public void setKtdIndicators(List<ActivityIndicator> ktdIndicators)
    {
        this.ktdIndicators = ktdIndicators;
    }

    /**
     * @return the ceaipIndicators
     */
    public List<ActivityIndicator> getCeaipIndicators()
    {
        return ceaipIndicators;
    }

    /**
     * @param ceaipIndicators
     *            the ceaipIndicators to set
     */
    public void setCeaipIndicators(List<ActivityIndicator> ceaipIndicators)
    {
        this.ceaipIndicators = ceaipIndicators;
    }

    /**
     * @return the tpivsdIndicators
     */
    public List<ActivityIndicator> getTpivsdIndicators()
    {
        return tpivsdIndicators;
    }

    /**
     * @param tpivsdIndicators
     *            the tpivsdIndicators to set
     */
    public void setTpivsdIndicators(List<ActivityIndicator> tpivsdIndicators)
    {
        this.tpivsdIndicators = tpivsdIndicators;
    }

    /**
     * @return the pvsdIndicators
     */
    public List<ActivityIndicator> getPvsdIndicators()
    {
        return pvsdIndicators;
    }

    /**
     * @param pvsdIndicators
     *            the pvsdIndicators to set
     */
    public void setPvsdIndicators(List<ActivityIndicator> pvsdIndicators)
    {
        this.pvsdIndicators = pvsdIndicators;
    }

    /**
     * @return the otgIndicators
     */
    public List<ActivityIndicator> getOtgIndicators()
    {
        return otgIndicators;
    }

    /**
     * @param otgIndicators
     *            the otgIndicators to set
     */
    public void setOtgIndicators(List<ActivityIndicator> otgIndicators)
    {
        this.otgIndicators = otgIndicators;
    }

    /**
     * @param isEditable
     *            the isEditable to set
     */
    public void setEditable(boolean isEditable)
    {
        this.isEditable = isEditable;
    }

    /**
     * @return the publications
     */
    public List<ActivityPublication> getPublications()
    {
        return publications;
    }

    /**
     * @param publications
     *            the publications to set
     */
    public void setPublications(List<ActivityPublication> publications)
    {
        this.publications = publications;
    }

    /**
     * @return the materials
     */
    public List<ActivityMaterial> getMaterials()
    {
        return materials;
    }

    /**
     * @param materials
     *            the materials to set
     */
    public void setMaterials(List<ActivityMaterial> materials)
    {
        this.materials = materials;
    }

    /**
     * @return the outreachs
     */
    public List<ActivityOutreach> getOutreachs()
    {
        return outreachs;
    }

    /**
     * @param outreachs
     *            the outreachs to set
     */
    public void setOutreachs(List<ActivityOutreach> outreachs)
    {
        this.outreachs = outreachs;
    }

    public boolean isToBeSubmittedToCA()
    {
        return toBeSubmittedToCA;
    }

    public void setToBeSubmittedToCA(boolean toBeSubmittedToCA)
    {
        this.toBeSubmittedToCA = toBeSubmittedToCA;
    }

    public List<ActivityIndicator> getIndicators()
    {
        return indicators;
    }

    public void setIndicators(List<ActivityIndicator> indicators)
    {
        this.indicators = indicators;
    }

	public ActivityReviewRating getMidYearReviews() {
		return midYearReviews;
	}

	public void setMidYearReviews(ActivityReviewRating midYearReviews) {
		this.midYearReviews = midYearReviews;
	}
}
