/*
 * FILENAME
 *     ActivityMainBudget.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Main budget informations.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainBudget implements Serializable, Comparable<MainBudget>, Difference<MainBudget>
{
    private static final long serialVersionUID = 1265486343312411535L;
    private String id;
    private String regionId;
    private String regionName;

    private List<MainBudgetDetail> details = new ArrayList<MainBudgetDetail>();

	@JsonIgnore
    private double w1w2BudgetTotal = 0;
    @JsonIgnore
    private double w1w2GenderPrecTotal = 0;
    @JsonIgnore
    private double w1w2GenderBudgetTotal = 0;

    @JsonIgnore
    private double w3BudgetTotal = 0;
    @JsonIgnore
    private double w3GenderPrecTotal = 0;
    @JsonIgnore
    private double w3GenderBudgetTotal = 0;

    @JsonIgnore
    private double bilateralBudgetTotal = 0;
    @JsonIgnore
    private double bilateralGenderPrecTotal = 0;
    @JsonIgnore
    private double bilateralGenderBudgetTotal = 0;

    @JsonIgnore
    private double otherBudgetTotal = 0;
    @JsonIgnore
    private double otherGenderPrecTotal = 0;
    @JsonIgnore
    private double otherGenderBudgetTotal = 0;
    
    
    @Override
	public DiffReport diff(MainBudget t) {
		DiffReport report = new DiffReport();
		
		for(int i = 0; i < t.getDetails().size(); i++) {
			MainBudgetDetail prev = this.details.get(i);			
			MainBudgetDetail current = t.details.get(i);
			DiffReport bDiff = prev.diff(current);
			
			if( bDiff.hasDifferent()) {
				report.getDifferentReport().append("Main Budget, Region :" + regionName + ",\n\t\t\t").append(bDiff.getDifferentReport()).append("\n");
			}
		}
		return report;
	}

    /**
     * <p>
     * Calculate the Total of the main budget.
     * </p>
     */
    @JsonIgnore
    public void calculateTotals()
    {
        double budgetPrecentCounter = 0.0;
        for (MainBudgetDetail budgetDetails : details)
        {
            if (budgetDetails.getType().equals(MainBudgetDetail.BUDGET_TYPE))
            {
                this.w1w2BudgetTotal += budgetDetails.getW1w2();
                this.w3BudgetTotal += budgetDetails.getW3();
                this.bilateralBudgetTotal += budgetDetails.getBilateral();
                this.otherBudgetTotal += budgetDetails.getOther();
            }
            else if (budgetDetails.getType().equals(MainBudgetDetail.GENDER_PRECNT_TYPE))
            {
                this.w1w2GenderPrecTotal += budgetDetails.getW1w2();
                this.w3GenderPrecTotal += budgetDetails.getW3();
                this.bilateralGenderPrecTotal += budgetDetails.getBilateral();
                this.otherGenderPrecTotal += budgetDetails.getOther();
                budgetPrecentCounter += 1;
            }
            else if (budgetDetails.getType().equals(MainBudgetDetail.GENDER_BUDGET_TYPE))
            {
               
            }
        }

        // Get percentage out of all budget details
        this.w1w2GenderPrecTotal = (this.w1w2GenderPrecTotal / budgetPrecentCounter);
        this.w3GenderPrecTotal = (this.w3GenderPrecTotal / budgetPrecentCounter);
        this.bilateralGenderPrecTotal = (this.bilateralGenderPrecTotal / budgetPrecentCounter);
        this.otherGenderPrecTotal = (this.otherGenderPrecTotal / budgetPrecentCounter);

        this.w1w2GenderBudgetTotal = ((this.w1w2BudgetTotal * this.w1w2GenderPrecTotal) / 100);
        this.w3GenderBudgetTotal = ((this.w3BudgetTotal * this.w3GenderPrecTotal) / 100);
        this.bilateralGenderBudgetTotal = ((this.bilateralBudgetTotal * this.bilateralGenderPrecTotal) / 100);
        this.otherGenderBudgetTotal = ((this.otherBudgetTotal * this.otherGenderPrecTotal) / 100);
    }

    public double getRowTotalOfGenderPrecentage()
    {
        return roundUp(getRowTotalOfGenderBudget()/ getRowTotalOfBudget() * 100);
    }
    
    public double getRowTotalOfBudget()
    {
        return this.w1w2BudgetTotal + this.w3BudgetTotal + this.bilateralBudgetTotal
            + this.otherBudgetTotal;
    }
    
    public double getRowTotalOfGenderBudget()
    {
        return this.w1w2GenderBudgetTotal + this.w3GenderBudgetTotal + this.bilateralGenderBudgetTotal
            + this.otherGenderBudgetTotal;
    }

    private double roundUp(double value)
    {
        value = Math.round(value * 100);
        value = value / 100;
        return value;
    }

    /**
     * <p>
     * Getter for w1w2BudgetTotal.
     * </p>
     * 
     * @return the w1w2BudgetTotal
     */
    @JsonIgnore
    public double getW1w2BudgetTotal()
    {
        return Double.isNaN(w1w2BudgetTotal) ? 0.0 :w1w2BudgetTotal;
    }

    /**
     * <p>
     * Getter for w1w2GenderPrecTotal.
     * </p>
     * 
     * @return the w1w2GenderPrecTotal
     */
    @JsonIgnore
    public double getW1w2GenderPrecTotal()
    {
        return Double.isNaN(w1w2GenderPrecTotal) ? 0.0 : w1w2GenderPrecTotal;
    }

    /**
     * <p>
     * Getter for w1w2GenderBudgetTotal.
     * </p>
     * 
     * @return the w1w2GenderBudgetTotal
     */
    @JsonIgnore
    public double getW1w2GenderBudgetTotal()
    {
        return Double.isNaN(w1w2GenderBudgetTotal) ? 0.0 : w1w2GenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for w3BudgetTotal.
     * </p>
     * 
     * @return the w3BudgetTotal
     */
    @JsonIgnore
    public double getW3BudgetTotal()
    {
        return Double.isNaN(w3BudgetTotal) ? 0.0 : w3BudgetTotal;
    }

    /**
     * <p>
     * Getter for w3GenderPrecTotal.
     * </p>
     * 
     * @return the w3GenderPrecTotal
     */
    @JsonIgnore
    public double getW3GenderPrecTotal()
    {
        return Double.isNaN(w3GenderPrecTotal) ? 0.0 : w3GenderPrecTotal;
    }

    /**
     * <p>
     * Getter for w3GenderBudgetTotal.
     * </p>
     * 
     * @return the w3GenderBudgetTotal
     */
    @JsonIgnore
    public double getW3GenderBudgetTotal()
    {
        return Double.isNaN(w3GenderBudgetTotal) ? 0.0 : w3GenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for bilateralBudgetTotal.
     * </p>
     * 
     * @return the bilateralBudgetTotal
     */
    @JsonIgnore
    public double getBilateralBudgetTotal()
    {
        return Double.isNaN(bilateralBudgetTotal) ? 0.0 : bilateralBudgetTotal;
    }

    /**
     * <p>
     * Getter for bilateralGenderPrecTotal.
     * </p>
     * 
     * @return the bilateralGenderPrecTotal
     */
    @JsonIgnore
    public double getBilateralGenderPrecTotal()
    {
        return Double.isNaN(bilateralGenderPrecTotal) ? 0.0 : bilateralGenderPrecTotal;
    }

    /**
     * <p>
     * Getter for bilateralGenderBudgetTotal.
     * </p>
     * 
     * @return the bilateralGenderBudgetTotal
     */
    @JsonIgnore
    public double getBilateralGenderBudgetTotal()
    {
        return Double.isNaN(bilateralGenderBudgetTotal) ? 0.0 : bilateralGenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for otherBudgetTotal.
     * </p>
     * 
     * @return the otherBudgetTotal
     */
    @JsonIgnore
    public double getOtherBudgetTotal()
    {
        return Double.isNaN(otherBudgetTotal) ? 0.0 : otherBudgetTotal;
    }

    /**
     * <p>
     * Getter for otherGenderPrecTotal.
     * </p>
     * 
     * @return the otherGenderPrecTotal
     */
    @JsonIgnore
    public double getOtherGenderPrecTotal()
    {
        return Double.isNaN(otherGenderPrecTotal) ? 0.0 : otherGenderPrecTotal;
    }

    /**
     * <p>
     * Getter for otherGenderBudgetTotal.
     * </p>
     * 
     * @return the otherGenderBudgetTotal
     */
    @JsonIgnore
    public double getOtherGenderBudgetTotal()
    {
        return Double.isNaN(otherGenderBudgetTotal) ? 0.0 : otherGenderBudgetTotal;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for regionName.
     * </p>
     * 
     * @return the regionName
     */
    public String getRegionName()
    {
        return regionName;
    }

    /**
     * <p>
     * Setting value for regionName.
     * </p>
     * 
     * @param regionName
     *            the regionName to set
     */
    public void setRegionName(String regionName)
    {
        this.regionName = regionName;
    }

    /**
     * <p>
     * Getter for regionId.
     * </p>
     * 
     * @return the regionId
     */
    public String getRegionId()
    {
        return regionId;
    }

    /**
     * <p>
     * Setting value for regionId.
     * </p>
     * 
     * @param regionId
     *            the regionId to set
     */
    public void setRegionId(String regionId)
    {
        this.regionId = regionId;
    }

    /**
     * <p>
     * Getter for details.
     * </p>
     * 
     * @return the details
     */
    public List<MainBudgetDetail> getDetails()
    {
        return details;
    }

    /**
     * <p>
     * Setting value for details.
     * </p>
     * 
     * @param details
     *            the details to set
     */
    public void setDetails(List<MainBudgetDetail> details)
    {
        this.details = details;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(MainBudget o)
    {
        return this.regionName.compareToIgnoreCase(o.regionName);
    }

}
