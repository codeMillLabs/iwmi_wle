/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.dao;

import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLFlagshipFocalRegionDAO;

import com.google.inject.ImplementedBy;

@ImplementedBy(MySQLFlagshipFocalRegionDAO.class)
public interface FlagshipFocalRegionDAO {

	/**
	 * Get the flagship/focal regions.
	 * 
	 * @return a list of flagship/focal regions
	 */
	public List<Map<String, String>> getFlagshipFocalRegions();
	
	/**
	 * Get the flagship/focal region by id.
	 * 
	 * @param id flagship / focal region id
	 * 
	 * @return a a flagship/focal region
	 */
	public Map<String, String> getFlagshipFocalRegion(int id);
	
	
	/**
	 * Get cluster id list for given flagship id.
	 * 
	 * @param flagshipId flag ship id
	 * 
	 * @return list of cluster ids
	 */
	public List<Integer> getClusterIds(int flagshipId);
	
	
	/**
	 * Get the flagship cluster data.
	 * 
	 * @param year year
	 * @param flagshipId flagship id
	 * @param cluster cluster code
	 * 
	 * @return a list of flagship/focal cluster data.
	 */
	public List<Map<String, String>>  getFlagshipReportData(int year, int flagshipId, String cluster);
	
	/**
	 * save flagship focal region report status.
	 * 
	 * @param keyValues
	 *            of report
	 */
	public int saveFlagshipRptStatus(Map<String, String> keyValues);
	
	/**
	 * Check report availability.
	 * 
	 * @param year year
	 * @param flagshipId flagship id
	 * @param period period (mid,end)
	 * 
	 * @return boolean value whether available or not
	 */
	boolean checkReportAvailable(int year, int flagshipId, String period);
	
	/**
	 * Get flagship ids for given activity id.
	 * 
	 * @param activityId activity id
	 * 
	 * @return flagship ids
	 */
	public List<Integer> getFlaghipIdsByActivity(String activityId);
	
	
	/**
	 * save main page first section.
	 * 
	 * @param keyValues
	 *            of first section
	 */
	public int saveMainPageFirstSection(Map<String, String> keyValues);
	
	/**
	 * Get main page first section.
	 * 
	 * @return key value map
	 */
	public Map<String, String> getScoreCardMainPageFirstSection();
	
	/**
	 * @return
	 */
	public boolean deleteScoreCardCache();
	
	/**
	 * save main page flagship.
	 * 
	 * @param keyValues
	 *            of flagship section
	 */
	public int saveScoreCardMainPageFlagship(Map<String, String> keyValues);
	
	/**
	 * @return list of key value pairs.
	 */
	public List<Map<String, String>>  getScoreCardMainPageFlagships();
	
	/**
	 * @return list of key value pairs.
	 */
	public Map<String, String>  getScoreCardMainPageFlagship(int id);
	
	/**
	 * save score card second page.
	 * 
	 * @param keyValues
	 *            of second page
	 */
	public int saveScoreCardSecondPage(Map<String, String> keyValues);
	
	/**
	 * Get score card second pages.
	 * 
	 * @return list of key value pairs.
	 */
	public List<Map<String, String>>  getScoreCardSecondPages(int flagshipId);
	
	
	/**
	 * save score card third page output.
	 * 
	 * @param keyValues
	 *            of third page
	 */
	public int saveScoreCardThirdPageOutput(Map<String, String> keyValues);
	
	/**
	 * Get score card third page outputs.
	 * 
	 * @return list of key value pairs.
	 */
	public List<Map<String, String>>  getScoreCardThirdPageOutputs(String projectId);
	
	/**
	 * save score card third page project.
	 * 
	 * @param keyValues
	 *            of third page project
	 */
	public int saveScoreCardThirdPageProject(Map<String, String> keyValues);
	
	/**
	 * Get score card third page projects.
	 * 
	 * @return list of key value pairs.
	 */
	public List<Map<String, String>>  getScoreCardThirdPageProjects(int flagshipId);
	
	/**
	 * Get score card third page project.
	 * 
	 * @return list of key value pairs.
	 */
	public Map<String, String>  getScoreCardThirdPageProject(String projectId);
}