/**
 * 
 */
package org.cgiar.ccafs.ap.data.dao;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityWLEDAO;
import org.cgiar.ccafs.ap.data.model.ActivityWLEDevelopment;

import com.google.inject.ImplementedBy;

/**
 * @author Amila Silva
 *
 */
@ImplementedBy(MySQLActivityWLEDAO.class)
public interface ActivityWLEDAO {

	/**
	   * Get the list of WLE related to the activity given
	   * 
	   * @param activityID - the activity identifier
	   * @return a list of maps with the information
	   */
	  public List<ActivityWLEDevelopment> getWLEList(int activityID);

	  /**
	   * Remove all activity WLE related to the activity given.
	   * 
	   * @param activityID - Activity identifier
	   * @return true if the records were successfully saved. False otherwise.
	   */
	  public boolean removeActivityWLE(int activityID);

	  /**
	   * Save the wleDevelopment information into the database
	   * 
	   * @param activityID - Activity identifier
	   * @param wleDevelopment - wleDevelopment identifier
	   * @return true if the information was successfully saved. False otherwise
	   */
	  public boolean saveWLE(ActivityWLEDevelopment wleDevelopment, int activityId);
}
