/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.manager;

import java.util.List;

import org.cgiar.ccafs.ap.data.manager.impl.ActivityOutcomeManagerImpl;
import org.cgiar.ccafs.ap.data.model.ActivityOutcome;

import com.google.inject.ImplementedBy;

/**
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
@ImplementedBy(ActivityOutcomeManagerImpl.class)
public interface ActivityOutcomeManager {

	/**
	 * Save into the DAO the outcome data corresponding to the given activity
	 * 
	 * @param activityOutcome
	 *            outcome object with the information to save
	 * @return true if was successfully saved, false otherwise
	 */
	public int saveOutcome(ActivityOutcome activityOutcome);

	/**
	 * update into the DAO the outcome data corresponding to the given activity
	 * 
	 * @param activityOutcome
	 *            outcome object with the information to update
	 */
	public void updateOutcome(ActivityOutcome activityOutcome);

	/**
	 * Get the outcomes for a given activity.
	 * 
	 * @param activityID
	 *            - activity identifier.
	 * @return a list of outcomes for the given activity
	 */
	public List<ActivityOutcome> getActivityOutcomeByActivity(int activityId);

}
