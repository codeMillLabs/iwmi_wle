/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import static org.cgiar.ccafs.ap.util.WleAppUtil.isNotEqual;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.cgiar.ccafs.ap.util.WleAppUtil;

public class Country implements Difference<Country> {

	private String id;
	private String name;
	private Region region;
	private String capitalName;
	private String capitalLong;
	private String capitalLat;

	public Country() {
	}

	public Country(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Country(String id, String name, String capitalName,
			String capitalLong, String capitalLat) {
		this.id = id;
		this.name = name;
		this.capitalName = capitalName;
		this.capitalLong = capitalLong;
		this.capitalLat = capitalLat;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Region getRegion() {
		return region;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getCapitalName() {
		return capitalName;
	}

	public void setCapitalName(String capitalName) {
		this.capitalName = capitalName;
	}

	public String getCapitalLong() {
		return capitalLong;
	}

	public void setCapitalLong(String capitalLong) {
		this.capitalLong = capitalLong;
	}

	public String getCapitalLat() {
		return capitalLat;
	}

	public void setCapitalLat(String capitalLat) {
		this.capitalLat = capitalLat;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	private static String NAME = "Name";
	
	@Override
	public DiffReport diff(Country t) {
		DiffReport report = new DiffReport();
		if(isNotEqual(this.getName(), t.getName())) {
			report.append(NAME, this.name, t.getName());
		}
		return report;
	}

}
