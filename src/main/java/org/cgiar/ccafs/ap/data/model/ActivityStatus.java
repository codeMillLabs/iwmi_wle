/*
 * FILENAME
 *     ActivityStatus.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * 
 * <p>
 * Activity Status.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 *
 */
public enum ActivityStatus
{
    NEW("New"),
    IN_PROGRESS("In-Progress"),
    VALIDATED("Validated"),
    INTERNAL_REVIEWING("Internal Reviewing"),
    INTERNAL_CORRECTIONS("Internal Corrections"),
    SUBMITTED_TO_CA("Submitted To CA"),
    REVIEWING("Reviewing"),
    REVISION("Revision"),
    SUBMITTED("Submitted"),
    REVIEWS_COMPLETED("Reviews Completed"),
    COMPLETED("Completed");

    private String name;

    private ActivityStatus(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    

    public static ActivityStatus getValue(String name)
    {
        if (name == null || name.isEmpty())
            return NEW;
        for (ActivityStatus status : ActivityStatus.values())
        {
            if (status.getName().equals(name))
            {
                return status;
            }
        }
        return NEW;
    }
}
