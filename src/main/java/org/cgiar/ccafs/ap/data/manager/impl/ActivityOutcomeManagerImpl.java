/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.data.manager.impl;

import java.util.List;

import org.cgiar.ccafs.ap.data.dao.ActivityOutcomeDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityOutcomeManager;
import org.cgiar.ccafs.ap.data.model.ActivityOutcome;

import com.google.inject.Inject;

/**
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
public class ActivityOutcomeManagerImpl implements ActivityOutcomeManager {

	private ActivityOutcomeDAO outcomeDAO;

	@Inject
	public ActivityOutcomeManagerImpl(ActivityOutcomeDAO outcomeDAO) {
		this.outcomeDAO = outcomeDAO;
	}

	@Override
	public int saveOutcome(ActivityOutcome activityOutcome) {
		return outcomeDAO.saveOutcome(activityOutcome);
	}

	@Override
	public void updateOutcome(ActivityOutcome activityOutcome) {
		outcomeDAO.updateOutcome(activityOutcome);
	}

	@Override
	public List<ActivityOutcome> getActivityOutcomeByActivity(int activityId) {
		return outcomeDAO.getActivityOutcomeByActivity(activityId);
	}

}
