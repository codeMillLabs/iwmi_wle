/**
 * 
 */
package org.cgiar.ccafs.ap.data.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Review comments saved here
 * </p>
 * 
 * 
 * @author asilva
 * @email amila@hashcodesys.com
 */
public class SectionReview implements Serializable
{

    private static final long serialVersionUID = 7234890195319162140L;

    public enum Category
    {
        SUMMARY("Summary", "summary_review"),
        OUTCOMES("Outcomes", "outcomes_review"),
        DELIVERABLES("Deliverables", "deliverables_review"),
        PARTNERS("Partners", "partners_review"),
        GEO_LOCATIONS("Geo-Locations", "geo_review"),
        BUDGET("Budget", "budget_review"),
        ADDITIONAL_INFO("Additional Info", "add_info_review"),
        OUTREACH_ACTION("Outreach", "activity_outreach"),
        MATERIAL_ACTION("Material", "activity_material"),
        PUBLICATION_ACTION("Publication", "activity_publication");

        private String columnName;
        private String display;

        private Category(String display, String columnName)
        {
            this.columnName = columnName;
            this.display = display;
        }

        @Override
        public String toString()
        {
            return this.columnName;
        }

        public String getColumnName()
        {
            return columnName;
        }

        public String getDisplay()
        {
            return display;
        }

        public static Category getValue(String name)
        {

            for (Category category : Category.values())
            {
                if (category.getColumnName().equals(name))
                    return category;
            }
            return SUMMARY;
        }
    }

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(SectionReview.class);

    private int activityId;
    private String currentRevision;
    private List<Review> reviews = new ArrayList<Review>();

    /**
     * <p>
     * Get JSON from the Section Review data.
     * </p>
     *
     * @return JSON data
     * @throws Exception
     *
     */
    public static String getJSON(SectionReview review)
    {
        ObjectMapper mapper = new ObjectMapper();
        String jsonStr = null;
        try
        {
            jsonStr = mapper.writeValueAsString(review);
        }
        catch (Exception e)
        {
            LOG.error("Error occurred during the Get JSON from Object ", e);
            jsonStr = null;
        }
        return jsonStr;
    }

    /**
     * <p>
     * Get Section Review from json string.
     * </p>
     * 
     *
     * @param jsonStr
     * @return
     * @throws Exception
     *
     */
    public static SectionReview getReview(String jsonStr)
    {
        ObjectMapper mapper = new ObjectMapper();
        SectionReview sectionReview = null;
        try
        {
            sectionReview = mapper.readValue(jsonStr, SectionReview.class);
        }
        catch (IOException e)
        {
            LOG.error("Error occurred during the Get Review from json string ", e);
        }
        return sectionReview;
    }

    /**
     * Add Review
     * 
     * @param review
     */
    public void addReview(Review review)
    {
        getReviews().add(review);
    }

    /**
     * @return the activityId
     */
    public int getActivityId()
    {
        return activityId;
    }

    /**
     * @param activityId
     *            the activityId to set
     */
    public void setActivityId(int activityId)
    {
        this.activityId = activityId;
    }

    /**
     * @return the currentRevision
     */
    public String getCurrentRevision()
    {
        return currentRevision;
    }

    /**
     * @param currentRevision
     *            the currentRevision to set
     */
    public void setCurrentRevision(String currentRevision)
    {
        this.currentRevision = currentRevision;
    }

    /**
     * @return the reviews
     */
    public List<Review> getReviews()
    {
        if (reviews == null)
        {
            reviews = new ArrayList<Review>();
        }
        return reviews;
    }

    /**
     * @param reviews
     *            the reviews to set
     */
    public void setReviews(List<Review> reviews)
    {
        this.reviews = reviews;
    }

}
