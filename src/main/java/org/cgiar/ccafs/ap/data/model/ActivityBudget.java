/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityBudget implements Serializable, Difference<ActivityBudget>
{

    private static final long serialVersionUID = 644255125487490483L;
    private long id;
    private int activityId;
    private List<MainBudget> mainBudgets = new ArrayList<MainBudget>();
    private List<RegionalBudget> regBudgets = new ArrayList<RegionalBudget>();
    private List<DonorBudget> donorsBudget = new ArrayList<DonorBudget>();

    private transient Activity activity;
    
    

	@Override
	public DiffReport diff(ActivityBudget t) {
		DiffReport report = new DiffReport();
		report.setTitle("Budget");
		
		for(int i = 0; i < t.getMainBudgets().size(); i++) {
			if(i >= this.mainBudgets.size()) {
				MainBudget main = t.getMainBudgets().get(i);
				report.getDifferentReport().append("New Main Budget, Region :" + main.getRegionName()).append("\n");
				report.setHasDifferent(true);
			} else {
				DiffReport mainDiff = mainBudgets.get(i).diff(t.getMainBudgets().get(i));
				report.append(mainDiff);
			}
		}
		
		for(int i = 0; i < t.getRegBudgets().size(); i++) {
			if(i >= this.regBudgets.size()) {
				RegionalBudget regional = t.getRegBudgets().get(i);
				report.getDifferentReport().append("New Regional Budget, Region :" + regional.getRegionName()).append("\n");
				report.setHasDifferent(true);
			} else {
				DiffReport mainDiff = regBudgets.get(i).diff(t.getRegBudgets().get(i));
				report.append(mainDiff);
			}
		}
		
		for(int i = 0; i < t.getDonorsBudget().size(); i++) {
			if(i >= this.donorsBudget.size()) {
				DonorBudget donor = t.getDonorsBudget().get(i);
				report.getDifferentReport().append("New Donor Budget, Donor Name :" + donor.getDonorName()).append("\n");
				report.setHasDifferent(true);
			} else {
				DiffReport donorDiff = donorsBudget.get(i).diff(t.getDonorsBudget().get(i));
				report.append(donorDiff);
			}
		}
		return report;
	}

	/**
     * <p>
     * Sum all activity related budget totals.
     * </p>
     *
     * @return {@link ActivityBudgetTotals}
     *
     */
    public ActivityBudgetTotals sumAll()
    {
        ActivityBudgetTotals totalCal = new ActivityBudgetTotals(this);
        totalCal.sumActivityBudget();

        return totalCal;
    }

    /**
     * <p>
     * Get JSON from the activity budget data.
     * </p>
     *
     * @return JSON data
     * @throws Exception
     *
     */
    public static String getJSON(ActivityBudget budget) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        String jsonStr = mapper.writeValueAsString(budget);

        return jsonStr;
    }

    /**
     * <p>
     * Get Activity Budget from json string.
     * </p>
     * 
     *
     * @param jsonStr
     * @return
     * @throws Exception
     *
     */
    public static ActivityBudget getBudget(String jsonStr) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        ActivityBudget activityBudget = mapper.readValue(jsonStr, ActivityBudget.class);
        return activityBudget;
    }

    /**
     * <p>
     * Add main budget
     * </p>
     *
     * @param mainBudget
     *
     */
    public void addMainBudget(MainBudget mainBudget)
    {
        mainBudgets.add(mainBudget);
    }

    /**
     * <p>
     * Add Regional Budget
     * </p>
     *
     * @param mainBudget
     *
     */
    public void addRegBudget(RegionalBudget regionalBudget)
    {
        regBudgets.add(regionalBudget);
    }

    /**
     * <p>
     * Add Regional Budget
     * </p>
     *
     * @param mainBudget
     *
     */
    public void addDonorBudget(DonorBudget donorBudget)
    {
        this.donorsBudget.add(donorBudget);
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for activityId.
     * </p>
     * 
     * @return the activityId
     */
    public int getActivityId()
    {
        return activityId;
    }

    /**
     * <p>
     * Setting value for activityId.
     * </p>
     * 
     * @param activityId
     *            the activityId to set
     */
    public void setActivityId(int activityId)
    {
        this.activityId = activityId;
    }

    /**
     * <p>
     * Getter for mainBudgets.
     * </p>
     * 
     * @return the mainBudgets
     */
    public List<MainBudget> getMainBudgets()
    {
        return mainBudgets;
    }

    /**
     * <p>
     * Setting value for mainBudgets.
     * </p>
     * 
     * @param mainBudgets
     *            the mainBudgets to set
     */
    public void setMainBudgets(List<MainBudget> mainBudgets)
    {
        this.mainBudgets = mainBudgets;
    }

    /**
     * <p>
     * Getter for regBudgets.
     * </p>
     * 
     * @return the regBudgets
     */
    public List<RegionalBudget> getRegBudgets()
    {
        return regBudgets;
    }

    /**
     * <p>
     * Setting value for regBudgets.
     * </p>
     * 
     * @param regBudgets
     *            the regBudgets to set
     */
    public void setRegBudgets(List<RegionalBudget> regBudgets)
    {
        this.regBudgets = regBudgets;
    }

    /**
     * <p>
     * Getter for donorBudget.
     * </p>
     * 
     * @return the donorBudget
     */
    public List<DonorBudget> getDonorsBudget()
    {
        return donorsBudget;
    }

    /**
     * <p>
     * Setting value for donorBudget.
     * </p>
     * 
     * @param donorBudget
     *            the donorBudget to set
     */
    public void setDonorsBudget(List<DonorBudget> donorsBudget)
    {
        this.donorsBudget = donorsBudget;
    }

    /**
     * <p>
     * Getter for activity.
     * </p>
     * 
     * @return the activity
     */
    public Activity getActivity()
    {
        return activity;
    }

    /**
     * <p>
     * Setting value for activity.
     * </p>
     * 
     * @param activity
     *            the activity to set
     */
    public void setActivity(Activity activity)
    {
        this.activity = activity;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

}
