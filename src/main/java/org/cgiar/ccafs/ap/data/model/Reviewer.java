/*
 * FILENAME
 *     Reviewer.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.data.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Reviewer Data model
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
public class Reviewer implements Serializable
{
    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    private static final long serialVersionUID = 3825424711635259667L;

    private long id;
    private String name;
    private String flagship;
    private String cluster;
    private String details;

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * <p>
     * Setting value for name.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for flagship.
     * </p>
     * 
     * @return the flagship
     */
    public String getFlagship()
    {
        return flagship;
    }

    /**
     * <p>
     * Setting value for flagship.
     * </p>
     * 
     * @param flagship
     *            the flagship to set
     */
    public void setFlagship(String flagship)
    {
        this.flagship = flagship;
    }

    /**
     * <p>
     * Getter for cluster.
     * </p>
     * 
     * @return the cluster
     */
    public String getCluster()
    {
        return cluster;
    }

    /**
     * <p>
     * Setting value for cluster.
     * </p>
     * 
     * @param cluster
     *            the cluster to set
     */
    public void setCluster(String cluster)
    {
        this.cluster = cluster;
    }

    /**
     * <p>
     * Getter for details.
     * </p>
     * 
     * @return the details
     */
    public String getDetails()
    {
        return details;
    }

    /**
     * <p>
     * Setting value for details.
     * </p>
     * 
     * @param details
     *            the details to set
     */
    public void setDetails(String details)
    {
        this.details = details;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

}
