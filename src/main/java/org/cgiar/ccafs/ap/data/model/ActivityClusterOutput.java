package org.cgiar.ccafs.ap.data.model;

import java.util.ArrayList;
import java.util.List;

public class ActivityClusterOutput {
	
	private int id;
	private String activityId;
	private String activityName;
	private List<ClusterOutput> clusterOutputs = new ArrayList<>();

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}
	
	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	/**
	 * @return the activityName
	 */
	public String getActivityName() {
		return activityName;
	}
	
	/**
	 * @param activityName the activityName to set
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	/**
	 * @return the clusterOutputs
	 */
	public List<ClusterOutput> getClusterOutputs() {
		return clusterOutputs;
	}
	
	/**
	 * @param clusterOutputs the clusterOutputs to set
	 */
	public void setClusterOutputs(List<ClusterOutput> clusterOutputs) {
		this.clusterOutputs = clusterOutputs;
	}		
	
	/**
	 * @param clusterOutputs the clusterOutputs to add
	 */
	public void addClusterOutput(ClusterOutput clusterOutput) {
		this.clusterOutputs.add(clusterOutput);
	}	
}
