/*
 * FILENAME
 *     BudgetDetailsCSVReportService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.reportengine.planning;

import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.ACTIVITY_CLUSTER;
import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.FLAGSHIP_SRP;
import static org.cgiar.ccafs.ap.data.model.MainBudgetDetail.BUDGET_TYPE;
import static org.cgiar.ccafs.ap.data.model.MainBudgetDetail.GENDER_BUDGET_TYPE;
import static org.cgiar.ccafs.ap.data.model.MainBudgetDetail.GENDER_PRECNT_TYPE;
import static org.cgiar.ccafs.ap.data.model.RegionalBudgetDetails.SOURCE_W1W2;
import static org.cgiar.ccafs.ap.data.model.RegionalBudgetDetails.SOURCE_W3;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityBudgetDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLDAOManager;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLDataLookUpDao;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityBudgetManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.DataLookupManagerImpl;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityBudgetTotals;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.DonorBudget;
import org.cgiar.ccafs.ap.data.model.MainBudget;
import org.cgiar.ccafs.ap.data.model.MainBudgetDetail;
import org.cgiar.ccafs.ap.data.model.RegionalBudget;
import org.cgiar.ccafs.ap.data.model.RegionalBudgetDetails;
import org.cgiar.ccafs.ap.util.PropertiesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Create CSV report data for Budget details.
 * </p>
 *
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
public class BudgetDetailsCSVReportService
{
    private static final Logger log = LoggerFactory.getLogger(BudgetDetailsCSVReportService.class);

    private ActivityBudgetManager activityBudgetManager = null;
    private DataLookUpManager dataLookUpManager = null;
    private DecimalFormat df = new DecimalFormat("####0.00");

    /**
     * <p>
     * This method will create CSV data for the budget detail report.
     * </p>
     *
     *
     * @param year
     *            year
     * @param activityId
     *            activityId
     * @param leadCenter
     *            activity lead Center
     * @param flagship
     *            flagship
     * @param cluster
     *            cluster
     * @return csv data
     *
     */
    public List<String[]> getCSVData(int year, int activityId, String leadCenter, String flagship, String cluster)
    {

        log.debug("Going to create data for Budget detail CSV ");

        List<String[]> csvData = new ArrayList<String[]>();

        List<ActivityBudget> activitybudgets =
            getBudgetManager().getActivityBudgetsByCriteria(year, activityId, leadCenter, cluster, flagship);

        if (leadCenter != null)
        {
            csvData = getCSVForGroupedLeadcenter(activitybudgets);
        }
        else if (flagship != null && cluster != null)
        {
            csvData = getCSVForGroupedFlagshipAndCluster(activitybudgets);
        }
        else if (flagship != null && cluster == null)
        {
            csvData = getCSVForGroupedFlagship(activitybudgets);
        }
        else if (flagship == null || cluster == null)
        {
            csvData = getCSVForActivity(activitybudgets, false);
        }
        log.info("Budget Detail CSV data pupulated successfully");
        return csvData;
    }

    private List<String[]> getCSVForGroupedLeadcenter(List<ActivityBudget> activitybudgets)
    {
        log.info("Budget Detail CSV data handler starts for Group by lead center");
        List<String[]> data = new ArrayList<>();
        Map<String, List<ActivityBudget>> leadCenterGrpMap = new HashMap<String, List<ActivityBudget>>();

        for (ActivityBudget budget : activitybudgets)
        {
            // Group by Lead Centers
            String leadCenter = budget.getActivity().getLeadCenter();
            List<ActivityBudget> budgets = leadCenterGrpMap.get(leadCenter);

            if (budgets == null)
            {
                budgets = new ArrayList<ActivityBudget>();
                leadCenterGrpMap.put(leadCenter, budgets);
            }
            budgets.add(budget);
        }
        log.debug("Activity Budgets grouped By LeadCenter - Done [ No of Lead Centers to Process : {}]",
            leadCenterGrpMap.size());

        // Grouped By location in each cluster group
        Map<String, List<ActivityBudget>> groupedActivityBudgetMap = groupedActivityBudgetsToCategory(leadCenterGrpMap);

        // Generate CSV
        log.info("Data has been proccessed and going to generate CSV for Clusters");
        data = getCSVForLeadCenterGroup(groupedActivityBudgetMap);
        return data;
    }

    private List<String[]> getCSVForGroupedFlagshipAndCluster(List<ActivityBudget> activitybudgets)
    {
        log.info("Budget Detail CSV data handler starts for Group by flagship and cluster");
        List<String[]> data = new ArrayList<>();
        Map<String, List<ActivityBudget>> clusteredGrpMap = new HashMap<String, List<ActivityBudget>>();

        for (ActivityBudget budget : activitybudgets)
        {
            // Group by clusters
            String cluster = budget.getActivity().getFlagShipSRP() + "@" + budget.getActivity().getActivityCluster();
            List<ActivityBudget> budgets = clusteredGrpMap.get(cluster);
            if (budgets == null)
            {
                budgets = new ArrayList<ActivityBudget>();
                clusteredGrpMap.put(cluster, budgets);
            }
            budgets.add(budget);
        }
        log.debug("Activity Budgets grouped By Clusters - Done [ No of Clusters to Process : {}]",
            clusteredGrpMap.size());

        // Grouped By location in each cluster group
        Map<String, List<ActivityBudget>> groupedActivityBudgetMap = groupedActivityBudgetsToCategory(clusteredGrpMap);

        // Generate CSV
        log.info("Data has been proccessed and going to generate CSV for Clusters");
        data = getCSVForFlagshipAndClusterGroup(groupedActivityBudgetMap);
        return data;
    }

    private List<String[]> getCSVForGroupedFlagship(List<ActivityBudget> activitybudgets)
    {
        log.info("Budget Detail CSV data handler starts for Group by Flagship");
        List<String[]> data = new ArrayList<>();
        Map<String, List<ActivityBudget>> flagshipGrpMap = new HashMap<String, List<ActivityBudget>>();

        for (ActivityBudget budget : activitybudgets)
        {
            // Group by flagships
            String flagship = budget.getActivity().getFlagShipSRP();
            List<ActivityBudget> budgets = flagshipGrpMap.get(flagship);
            if (budgets == null)
            {
                budgets = new ArrayList<ActivityBudget>();
                flagshipGrpMap.put(flagship, budgets);
            }
            budgets.add(budget);
        }
        log.debug("Activity Budgets grouped By Flagships - Done [ No of Flagships to Process : {}]",
            flagshipGrpMap.size());

        // Grouped By location in each flagship group
        Map<String, List<ActivityBudget>> groupedActivityBudgetMap = groupedActivityBudgetsToCategory(flagshipGrpMap);

        // Generate CSV
        data = getCSVForFlagshipGroup(groupedActivityBudgetMap);

        return data;
    }

    private Map<String, List<ActivityBudget>> groupedActivityBudgetsToCategory(
        Map<String, List<ActivityBudget>> categoryGrpMap)
    {
        Map<String, List<ActivityBudget>> groupedActivityBudgetValuesMap =
            groupAndTotalActivityBudgetsByCategory(categoryGrpMap);
        return groupedActivityBudgetValuesMap;
    }

    private Map<String, List<ActivityBudget>> groupAndTotalActivityBudgetsByCategory(
        Map<String, List<ActivityBudget>> categoryGrpMap)
    {
        Map<String, List<ActivityBudget>> groupedActivityBudgetValuesMap = new HashMap<String, List<ActivityBudget>>();

        for (Map.Entry<String, List<ActivityBudget>> entry : categoryGrpMap.entrySet())
        {
            String groupedCategory = entry.getKey();
            List<ActivityBudget> budgets = entry.getValue();

            // Activity Budget Total values goes here
            ActivityBudget groupedActivityBudget = new ActivityBudget();

            // add Grouped Activity Budget into grouped list
            List<ActivityBudget> groupedBudgets = groupedActivityBudgetValuesMap.get(groupedCategory);
            if (groupedBudgets == null)
            {
                groupedBudgets = new ArrayList<ActivityBudget>();
                groupedActivityBudgetValuesMap.put(groupedCategory, groupedBudgets);
            }

            groupedBudgets.add(groupedActivityBudget);

            Map<String, List<MainBudget>> mainBudgetGroupByLocationMap = new HashMap<String, List<MainBudget>>();
            Map<String, List<RegionalBudget>> regionalGroupByLocationMap = new HashMap<String, List<RegionalBudget>>();
            Map<String, List<DonorBudget>> donorBudgetGroupByNameMap = new HashMap<String, List<DonorBudget>>();

            for (ActivityBudget budget : budgets)
            {
                // Group main budgets by location
                groupMainBudgetByLocation(mainBudgetGroupByLocationMap, budget);

                // Group regional budgets by location
                groupRegionsByLocation(regionalGroupByLocationMap, budget);

                // Group donors budgets by source
                groupedDonorBudgetBySource(donorBudgetGroupByNameMap, budget);
            }

            log.debug(
                "Activity Budgets grouped in to each category by location, Main : {}, Regional : {}, Donors : {}",
                new Object[] {
                    mainBudgetGroupByLocationMap.size(),
                    regionalGroupByLocationMap.size(),
                    donorBudgetGroupByNameMap.size()
                });

            // Total each grouped categories values
            // Main Budget
            totalMainBudget(groupedActivityBudget, mainBudgetGroupByLocationMap);

            // Regional Budget
            totalRegionalBudget(groupedActivityBudget, regionalGroupByLocationMap);

            // Donor Budget
            totalDonorBudget(groupedActivityBudget, donorBudgetGroupByNameMap);
        }
        log.debug("All activities Group in to categorized single ActivityBudget : Key : {}, Size :{}",
            groupedActivityBudgetValuesMap.keySet(), groupedActivityBudgetValuesMap.size());
        return groupedActivityBudgetValuesMap;
    }

    private void totalDonorBudget(ActivityBudget groupedActivityBudget,
        Map<String, List<DonorBudget>> donorBudgetGroupByNameMap)
    {
        for (Map.Entry<String, List<DonorBudget>> donorBudgetEntry : donorBudgetGroupByNameMap.entrySet())
        {
            List<DonorBudget> dornorBudgets = donorBudgetEntry.getValue();

            DonorBudget groupedDonorBudget = new DonorBudget();

            double w3 = 0;
            double bilateral = 0;
            double other = 0;

            for (DonorBudget donor : dornorBudgets)
            {
                w3 += donor.getW3();
                bilateral += donor.getBilateral();
                other += donor.getOther();
            }
            groupedDonorBudget.setDonorName(donorBudgetEntry.getKey());
            groupedDonorBudget.setW3(w3);
            groupedDonorBudget.setBilateral(bilateral);
            groupedDonorBudget.setOther(other);

            groupedActivityBudget.addDonorBudget(groupedDonorBudget);
        }
    }

    private void totalRegionalBudget(ActivityBudget groupedActivityBudget,
        Map<String, List<RegionalBudget>> regionalGroupByLocationMap)
    {
        for (Map.Entry<String, List<RegionalBudget>> regBudgetEntry : regionalGroupByLocationMap.entrySet())
        {
            List<RegionalBudget> regBudgets = regBudgetEntry.getValue();

            RegionalBudget groupedRegBudget = new RegionalBudget();
            Map<String, List<RegionalBudgetDetails>> regBudgetDetailsMap =
                groupRegionalBudgetsByLocation(regBudgets, groupedRegBudget);

            // Total all regional budget data
            totalAllGroupedRegionalBudgetData(groupedRegBudget, regBudgetDetailsMap);
            groupedActivityBudget.addRegBudget(groupedRegBudget);
        }
    }

    private void totalAllGroupedRegionalBudgetData(RegionalBudget groupedRegBudget,
        Map<String, List<RegionalBudgetDetails>> regBudgetDetailsMap)
    {
        for (Map.Entry<String, List<RegionalBudgetDetails>> dtlEntry : regBudgetDetailsMap.entrySet())
        {

            RegionalBudgetDetails regBudgetDtl = new RegionalBudgetDetails();
            regBudgetDtl.setSourceType(dtlEntry.getKey());
            double mainBudgetTotal = 0;
            double personnel = 0;
            double operations = 0;
            double indirectCosts = 0;

            for (RegionalBudgetDetails d : dtlEntry.getValue())
            {
                mainBudgetTotal += d.getMainBudgetTotal();
                personnel += d.getPersonnel();
                operations += d.getOperations();
                indirectCosts += d.getIndirectCosts();
            }

            regBudgetDtl.setMainBudgetTotal(mainBudgetTotal);
            regBudgetDtl.setPersonnel(personnel);
            regBudgetDtl.setOperations(operations);
            regBudgetDtl.setIndirectCosts(indirectCosts);

            groupedRegBudget.getDetails().add(regBudgetDtl);
        }
    }

    private Map<String, List<RegionalBudgetDetails>> groupRegionalBudgetsByLocation(List<RegionalBudget> regBudgets,
        RegionalBudget groupedRegBudget)
    {
        Map<String, List<RegionalBudgetDetails>> regBudgetDetailsMap =
            new HashMap<String, List<RegionalBudgetDetails>>();

        for (RegionalBudget reg : regBudgets)
        {
            groupedRegBudget.setRegionId(reg.getRegionId());
            groupedRegBudget.setRegionName(reg.getRegionName());

            for (RegionalBudgetDetails detail : reg.getDetails())
            {
                // By Type grouping
                List<RegionalBudgetDetails> details = regBudgetDetailsMap.get(detail.getSourceType());
                if (details == null)
                {
                    details = new ArrayList<RegionalBudgetDetails>();
                    regBudgetDetailsMap.put(detail.getSourceType(), details);
                }
                details.add(detail);
            }
        }
        return regBudgetDetailsMap;
    }

    private void totalMainBudget(ActivityBudget groupedActivityBudget,
        Map<String, List<MainBudget>> mainBudgetGroupByLocationMap)
    {
        for (Map.Entry<String, List<MainBudget>> mainBudgetEntry : mainBudgetGroupByLocationMap.entrySet())
        {
            List<MainBudget> mainBudgets = mainBudgetEntry.getValue();

            MainBudget groupedMainBudget = new MainBudget();
            groupedMainBudget.setRegionId(mainBudgetEntry.getKey());
            groupedMainBudget.setRegionName(mainBudgetEntry.getKey());

            // Budget details by type
            Map<String, List<MainBudgetDetail>> mainBudgetDetailsByTypeMap = groupMainBudgetsByLocation(mainBudgets);

            // Total all main budget data
            totalAllGroupedMainBudgetData(groupedMainBudget, mainBudgetDetailsByTypeMap);
            groupedActivityBudget.addMainBudget(groupedMainBudget);
        }
    }

    private void totalAllGroupedMainBudgetData(MainBudget groupedMainBudget,
        Map<String, List<MainBudgetDetail>> mainBudgetDetailsMap)
    {
        for (Map.Entry<String, List<MainBudgetDetail>> dtlEntry : mainBudgetDetailsMap.entrySet())
        {
            String budgetType = dtlEntry.getKey();
            if (GENDER_PRECNT_TYPE.equals(budgetType))
            {
                continue;
            }
            MainBudgetDetail budgetDtl = new MainBudgetDetail();
            budgetDtl.setType(budgetType);
            double w1w2 = 0;
            double w3 = 0;
            double bilateral = 0;
            double other = 0;

            for (MainBudgetDetail d : dtlEntry.getValue())
            {
                if (GENDER_PRECNT_TYPE.equals(d.getType()))
                {
                    continue;
                }
                w1w2 += d.getW1w2();
                w3 += d.getW3();
                bilateral += d.getBilateral();
                other += d.getOther();
            }
            budgetDtl.setW1w2(w1w2);
            budgetDtl.setW3(w3);
            budgetDtl.setBilateral(bilateral);
            budgetDtl.setOther(other);
            groupedMainBudget.getDetails().add(budgetDtl);
        }

        // Handle total percentage calculations for main budget details
        double w1w2Budget = 0, w1w2Gender = 0;
        double w3Budget = 0, w3Gender = 0;
        double bilateralBudget = 0, bilateralGender = 0;
        double otherBudget = 0, otherGender = 0;

        for (MainBudgetDetail detail : groupedMainBudget.getDetails())
        {
            if (BUDGET_TYPE.equals(detail.getType()))
            {
                w1w2Budget += detail.getW1w2();
                w3Budget += detail.getW3();
                bilateralBudget += detail.getBilateral();
                otherBudget += detail.getOther();
            }
            else if (GENDER_BUDGET_TYPE.equals(detail.getType()))
            {
                w1w2Gender += detail.getW1w2();
                w3Gender += detail.getW3();
                bilateralGender += detail.getBilateral();
                otherGender += detail.getOther();
            }
        }
        MainBudgetDetail budgetPrecentage = new MainBudgetDetail();
        budgetPrecentage.setType(GENDER_PRECNT_TYPE);
        budgetPrecentage.setW1w2((w1w2Gender * 100) / w1w2Budget);
        budgetPrecentage.setW3((w3Gender * 100) / w3Budget);
        budgetPrecentage.setBilateral((bilateralGender * 100) / bilateralBudget);
        budgetPrecentage.setOther((otherGender * 100) / otherBudget);
        groupedMainBudget.getDetails().add(budgetPrecentage);
    }

    private Map<String, List<MainBudgetDetail>> groupMainBudgetsByLocation(List<MainBudget> mainBudgets)
    {
        Map<String, List<MainBudgetDetail>> mainBudgetDetailsMap = new HashMap<String, List<MainBudgetDetail>>();
        List<MainBudgetDetail> genderBudgets = new ArrayList<MainBudgetDetail>();

        for (MainBudget main : mainBudgets)
        {
            main.calculateTotals();

            for (MainBudgetDetail detail : main.getDetails())
            {
                if (detail.getType().equals(GENDER_BUDGET_TYPE))
                    continue;

                // Group all budget details By Type
                List<MainBudgetDetail> details = mainBudgetDetailsMap.get(detail.getType());
                if (details == null)
                {
                    details = new ArrayList<MainBudgetDetail>();
                    mainBudgetDetailsMap.put(detail.getType(), details);
                }
                details.add(detail);
            }

            MainBudgetDetail genderBudget = new MainBudgetDetail();
            genderBudget.setType(GENDER_BUDGET_TYPE);
            genderBudget.setW1w2(main.getW1w2GenderBudgetTotal());
            genderBudget.setW3(main.getW3GenderBudgetTotal());
            genderBudget.setBilateral(main.getBilateralGenderBudgetTotal());
            genderBudget.setOther(main.getOtherGenderBudgetTotal());
            genderBudgets.add(genderBudget);
        }

        mainBudgetDetailsMap.put(GENDER_BUDGET_TYPE, genderBudgets);
        return mainBudgetDetailsMap;
    }

    private Map<String, List<DonorBudget>> groupedDonorBudgetBySource(
        Map<String, List<DonorBudget>> donorBudgetGroupByNameMap, ActivityBudget budget)
    {
        for (DonorBudget donorBudget : budget.getDonorsBudget())
        {
            String source = donorBudget.getDonorName();
            if (source == null || source.isEmpty())
            {
                continue;
            }
            List<DonorBudget> donorBudgets = donorBudgetGroupByNameMap.get(source);

            if (donorBudgets == null)
            {
                donorBudgets = new ArrayList<DonorBudget>();
                donorBudgetGroupByNameMap.put(source, donorBudgets);
            }
            donorBudgets.add(donorBudget);
        }
        return donorBudgetGroupByNameMap;
    }

    private Map<String, List<RegionalBudget>> groupRegionsByLocation(
        Map<String, List<RegionalBudget>> regionalGroupByLocationMap, ActivityBudget budget)
    {
        for (RegionalBudget regBudget : budget.getRegBudgets())
        {
            String locationName = regBudget.getRegionName();
            List<RegionalBudget> regBudgets = regionalGroupByLocationMap.get(locationName);

            if (regBudgets == null)
            {
                regBudgets = new ArrayList<RegionalBudget>();
                regionalGroupByLocationMap.put(locationName, regBudgets);
            }
            regBudgets.add(regBudget);
        }
        return regionalGroupByLocationMap;
    }

    private Map<String, List<MainBudget>> groupMainBudgetByLocation(
        Map<String, List<MainBudget>> mainBudgetGroupByLocationMap, ActivityBudget budget)
    {
        for (MainBudget mainBudget : budget.getMainBudgets())
        {
            String locationName = mainBudget.getRegionName();
            List<MainBudget> mainBudgets = mainBudgetGroupByLocationMap.get(locationName);

            if (mainBudgets == null)
            {
                mainBudgets = new ArrayList<MainBudget>();
                mainBudgetGroupByLocationMap.put(locationName, mainBudgets);
            }
            mainBudgets.add(mainBudget);
        }
        return mainBudgetGroupByLocationMap;
    }

    private List<String[]> getCSVForLeadCenterGroup(Map<String, List<ActivityBudget>> groupedMap)
    {
        List<String[]> data = new ArrayList<>();

        log.info("Budget Detail CSV data handler starts for Lead center");
        data.add(csvRow("Activity Budgets By Lead Center"));
        for (Map.Entry<String, List<ActivityBudget>> entry : groupedMap.entrySet())
        {
            String leadCenter = entry.getKey();

            data.add(csvRow(""));
            data.add(csvRow("Lead Center ", leadCenter));
            data.add(csvRow(""));
            getCSVForActivity(entry.getValue(), data, true);
        }
        return data;
    }

    private List<String[]> getCSVForFlagshipAndClusterGroup(Map<String, List<ActivityBudget>> groupedMap)
    {
        List<String[]> data = new ArrayList<>();

        log.info("Budget Detail CSV data handler starts for Flagship And Cluster Groups");
        data.add(csvRow("Activity Budgets By Clusters"));
        for (Map.Entry<String, List<ActivityBudget>> entry : groupedMap.entrySet())
        {
            String[] categories = entry.getKey().split("@", -1);
            DataLookup flagShipLookup = getLookupManager().getLookUpdataByValue(FLAGSHIP_SRP, categories[0], false);
            DataLookup clusterLookup = getLookupManager().getLookUpdataByValue(ACTIVITY_CLUSTER, categories[1], false);
            String flagship = (flagShipLookup != null) ? flagShipLookup.getDisplayName() : "N/A";
            String activityCluster = (clusterLookup != null) ? clusterLookup.getDisplayName() : "N/A";

            data.add(csvRow(""));
            data.add(csvRow("Flagship ", flagship));
            data.add(csvRow("Activity Cluster ", activityCluster));
            data.add(csvRow(""));
            getCSVForActivity(entry.getValue(), data, true);
        }
        return data;
    }

    private List<String[]> getCSVForFlagshipGroup(Map<String, List<ActivityBudget>> groupedMap)
    {
        List<String[]> data = new ArrayList<>();

        log.info("Budget Detail CSV data handler starts for Flagship Groups");
        data.add(csvRow("Activity Budgets By Flagship"));
        for (Map.Entry<String, List<ActivityBudget>> entry : groupedMap.entrySet())
        {
            DataLookup flagShipLookup = getLookupManager().getLookUpdataByValue(FLAGSHIP_SRP, entry.getKey(), false);
            String flagship = (flagShipLookup != null) ? flagShipLookup.getDisplayName() : "N/A";

            data.add(csvRow(""));
            data.add(csvRow("Flagship ", flagship));
            data.add(csvRow(""));
            getCSVForActivity(entry.getValue(), data, true);
        }
        return data;
    }

    private List<String[]> getCSVForActivity(List<ActivityBudget> budgets, boolean isGroupedCategory)
    {
        List<String[]> data = new ArrayList<>();
        return getCSVForActivity(budgets, data, false);
    }

    private List<String[]> getCSVForActivity(List<ActivityBudget> budgets, List<String[]> data,
        boolean isGroupedCategory)
    {
        log.info("Budget Detail CSV data handler starts for Activities");

        for (ActivityBudget budget : budgets)
        {
            ActivityBudgetTotals totals = budget.sumAll();

            if (!isGroupedCategory)
            {
                data.add(csvRow(""));
                data.add(csvRow("Activity Id ", "" + budget.getActivityId()));
                data.add(csvRow("Activity Title", budget.getActivity().getTitle()));
                DataLookup flagShipLookup =
                    getLookupManager().getLookUpdataByValue(FLAGSHIP_SRP, budget.getActivity().getFlagShipSRP(), false);
                DataLookup clusterLookup =
                    getLookupManager().getLookUpdataByValue(ACTIVITY_CLUSTER,
                        budget.getActivity().getActivityCluster(), false);
                String flagship = (flagShipLookup != null) ? flagShipLookup.getDisplayName() : "N/A";
                String activityCluster = (clusterLookup != null) ? clusterLookup.getDisplayName() : "N/A";

                data.add(csvRow("Activity Flagship ", flagship, ""));
                data.add(csvRow("Activity Cluster ", activityCluster, ""));
            }
            data.add(csvRow(""));

            if (!budget.getMainBudgets().isEmpty())
            {
                populateDataForActivityMainBudget(data, budget, totals);
            }

            if (!budget.getRegBudgets().isEmpty())
            {
                populateDataForActivityRegionBudget(data, budget);
            }

            if (!budget.getDonorsBudget().isEmpty())
            {
                populateDataForActivityDonorBudget(data, budget, totals);
            }
            data.add(csvRow(""));
        }
        return data;
    }

    private void populateDataForActivityDonorBudget(List<String[]> data, ActivityBudget budget,
        ActivityBudgetTotals totals)
    {
        data.add(csvRow("Donor wise budget"));
        data.add(csvRow("Source", "W3", "Bilateral", "Other", "Total"));

        for (DonorBudget donor : budget.getDonorsBudget())
        {
            data.add(csvRow(donor.getDonorName(), df.format(donor.getW3()), df.format(donor.getBilateral()),
                df.format(donor.getOther()), df.format(donor.getTotal())));
        }
        data.add(csvRow("Total", df.format(totals.getW3DonorTotal()), df.format(totals.getBilateralDonorTotal()),
            df.format(totals.getOtherDonorTotal()), df.format(totals.getGrandDonorTotal())));

        data.add(csvRow(""));
    }

    private void populateDataForActivityRegionBudget(List<String[]> data, ActivityBudget budget)
    {
        data.add(csvRow("Region wise budget"));

        for (RegionalBudget region : budget.getRegBudgets())
        {
            data.add(csvRow("", "", region.getRegionName()));// Region Name
            data.add(csvRow("Source", "Total Budget", "Personnel", "Operations", "Indirect Costs", "Total"));
            String[] w1w2Row = {}, w3Row = {}, bilateralRow = {}, otherRow = {};
            for (RegionalBudgetDetails dtl : region.getDetails())
            {
                if (dtl.getSourceType().equalsIgnoreCase(SOURCE_W1W2))
                {
                    w1w2Row =
                        csvRow(dtl.getSourceType(), df.format(dtl.getMainBudgetTotal()), df.format(dtl.getPersonnel()),
                            df.format(dtl.getOperations()), df.format(dtl.getIndirectCosts()),
                            df.format(dtl.getTotal()));
                }
                else if (dtl.getSourceType().equalsIgnoreCase(SOURCE_W3))
                {
                    w3Row =
                        csvRow(dtl.getSourceType(), df.format(dtl.getMainBudgetTotal()), df.format(dtl.getPersonnel()),
                            df.format(dtl.getOperations()), df.format(dtl.getIndirectCosts()),
                            df.format(dtl.getTotal()));
                }
                else if (dtl.getSourceType().equalsIgnoreCase(RegionalBudgetDetails.SOURCE_BILATERAL))
                {
                    bilateralRow =
                        csvRow(dtl.getSourceType(), df.format(dtl.getMainBudgetTotal()), df.format(dtl.getPersonnel()),
                            df.format(dtl.getOperations()), df.format(dtl.getIndirectCosts()),
                            df.format(dtl.getTotal()));
                }
                else
                {
                    otherRow =
                        csvRow(dtl.getSourceType(), df.format(dtl.getMainBudgetTotal()), df.format(dtl.getPersonnel()),
                            df.format(dtl.getOperations()), df.format(dtl.getIndirectCosts()),
                            df.format(dtl.getTotal()));
                }
            }
            data.add(w1w2Row);
            data.add(w3Row);
            data.add(bilateralRow);
            data.add(otherRow);

            data.add(csvRow("Total", df.format(region.getTotalMainBudgetTotal()),
                df.format(region.getPersonnelTotal()), df.format(region.getOperationsTotal()),
                df.format(region.getIndirectCostsTotal()), df.format(region.getTotalRegionSum())));

            data.add(csvRow(""));
        }
        data.add(csvRow(""));
    }

    private void populateDataForActivityMainBudget(List<String[]> data, ActivityBudget budget,
        ActivityBudgetTotals totals)
    {
        data.add(csvRow("Main budget"));
        data.add(csvRow("", "W1/W2", "W3", "Bilateral", "Other", "Total"));

        for (MainBudget mainBudget : budget.getMainBudgets())
        {
            data.add(csvRow("", "", mainBudget.getRegionName()));// Region Name

            String[] budgetTypeRow = {}, genderPrecentRow = {}, genderBudget = {};

            for (MainBudgetDetail dtl : mainBudget.getDetails())
            {
                if (dtl.getType().equals(BUDGET_TYPE))
                {
                    budgetTypeRow =
                        csvRow(dtl.getType(), df.format(dtl.getW1w2()), df.format(dtl.getW3()),
                            df.format(dtl.getBilateral()), df.format(dtl.getOther()), df.format(dtl.getTotal()));
                }
                else if (dtl.getType().equals(GENDER_PRECNT_TYPE))
                {
                    genderPrecentRow =
                        csvRow(dtl.getType(), df.format(dtl.getW1w2()), df.format(dtl.getW3()),
                            df.format(dtl.getBilateral()), df.format(dtl.getOther()),
                            df.format(mainBudget.getRowTotalOfGenderPrecentage()));
                }
                else if (dtl.getType().equals(MainBudgetDetail.GENDER_BUDGET_TYPE))
                {
                    genderBudget =
                        csvRow(dtl.getType(), df.format(mainBudget.getW1w2GenderBudgetTotal()),
                            df.format(mainBudget.getW3GenderBudgetTotal()),
                            df.format(mainBudget.getBilateralGenderBudgetTotal()),
                            df.format(mainBudget.getOtherGenderBudgetTotal()),
                            df.format(mainBudget.getRowTotalOfGenderBudget()));
                }
            }
            data.add(budgetTypeRow);
            data.add(genderPrecentRow);
            data.add(genderBudget);
        }

        // Totals
        data.add(csvRow("Total per source of funding", df.format(totals.getW1w2BudgetTotal()),
            df.format(totals.getW3BudgetTotal()), df.format(totals.getBilateralBudgetTotal()),
            df.format(totals.getOtherBudgetTotal()), df.format(totals.getGrandTotalOfFunding())));

        data.add(csvRow("Total Gender %", df.format(totals.getW1w2GenderPrecTotal()),
            df.format(totals.getW3GenderPrecTotal()), df.format(totals.getBilateralGenderPrecTotal()),
            df.format(totals.getOtherGenderPrecTotal()), df.format(totals.getTotalGenderPrecentage())));

        data.add(csvRow("Total Gender Budget", df.format(totals.getW1w2GenderBudgetTotal()),
            df.format(totals.getW3GenderBudgetTotal()), df.format(totals.getBilateralGenderBudgetTotal()),
            df.format(totals.getOtherGenderBudgetTotal()), df.format(totals.getGrandTotalGenderBudget())));

        data.add(csvRow(""));
    }

    private String[] csvRow(String... values)
    {
        return values;
    }

    private ActivityBudgetManager getBudgetManager()
    {
        if (activityBudgetManager == null)
        {
            DAOManager databaseManager = new MySQLDAOManager(new PropertiesManager());
            activityBudgetManager = new ActivityBudgetManagerImpl(new MySQLActivityBudgetDAO(databaseManager));
        }

        return activityBudgetManager;
    }

    private DataLookUpManager getLookupManager()
    {
        if (dataLookUpManager == null)
        {
            DAOManager databaseManager = new MySQLDAOManager(new PropertiesManager());
            dataLookUpManager = new DataLookupManagerImpl(new MySQLDataLookUpDao(databaseManager));
        }

        return dataLookUpManager;
    }
}
