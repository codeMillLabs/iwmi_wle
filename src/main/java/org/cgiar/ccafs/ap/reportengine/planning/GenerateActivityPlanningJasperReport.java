/*
 * Created on Nov 26, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package org.cgiar.ccafs.ap.reportengine.planning;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * GenerateActivityPlanningJasperReport
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class GenerateActivityPlanningJasperReport
{
    // Logger
    private static final Logger log = LoggerFactory.getLogger(GenerateActivityPlanningJasperReport.class);

    private String reportFileLocation;

    /**
     * <p>
     * Build report generator
     * </p>
     *
     * @param reportFileLocation
     * @return
     *
     */
    public static GenerateActivityPlanningJasperReport build(String reportFileLocation)
    {
        GenerateActivityPlanningJasperReport rpt = new GenerateActivityPlanningJasperReport();
        rpt.reportFileLocation = reportFileLocation;

        return rpt;
    }

    /**
     * <p>
     * Generate report for given activity with requested pages
     * </p>
     *
     * @param activityReportId
     * @param reportPages
     * @return
     */
    public String generateReport(long activityReportId, List<ActivityReportEnum> rptPages)
    {
        try
        {
            log.info("Requested Report Generation will start soon, [ Activity Id : {}, No Of pages : {}] ",
                activityReportId, rptPages);

            List<ActivityReportEnum> reportPages = sortPages(rptPages);
            List<JasperPrint> printPages = new ArrayList<JasperPrint>();

            for (ReportPages reportPage : reportPages)
            {

                switch ((ActivityReportEnum) reportPage)
                {
                    case ACTIVITY_COVER_PAGE:
                        printPages.add(getCoverPagePrint(reportPage));
                        break;

                    case ACTIVITY_SUMMARY:
                        printPages.add(getSummaryPagePrint(reportPage));
                        break;
                    case ACTIVITY_OUTCOMES:
                        printPages.add(getOutcomePagePrint(reportPage));
                        break;

                    case ACTIVITY_DELIVERABLES:
                        printPages.add(getDeliverablesPagePrint(reportPage));
                        break;

                    case ACTIVITY_PARTNERS:
                        printPages.add(getPartnersPagePrint(reportPage));
                        break;

                    case ACTIVITY_GEO_LOCATIONS:
                        printPages.add(getGEOLocationsPagePrint(reportPage));
                        break;

                    case ACTIVITY_BUDGET:
                        printPages.add(getBudgetPagePrint(reportPage));
                        break;

                    case ACTIVITY_ADDITIONAL_INFO:
                        printPages.add(getAdditionalInfoPagePrint(reportPage));
                        break;

                    default:
                        break;
                }
            }

            JasperPrint mainDocumentToPrint = null;

            for (JasperPrint contentPrint : printPages)
            {

                if (mainDocumentToPrint == null)
                {
                    mainDocumentToPrint = contentPrint;
                    continue;
                }
                for (JRPrintPage page : contentPrint.getPages())
                {
                    mainDocumentToPrint.addPage(page);
                }
            }

            if (mainDocumentToPrint != null)
            {
                String destFilePath =
                    reportFileLocation + "exports" + File.separator + "Activity_Details_"
                        + mainDocumentToPrint.getName() + "_" + System.currentTimeMillis() + ".pdf";

                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(mainDocumentToPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFilePath));
                exporter.exportReport();

                return destFilePath;
            }

        }
        catch (Exception e)
        {
            Log.error("Severe Error occurred during the Print copy generation,", e);
        }
        return null;
    }

    private JasperPrint getCoverPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;

    }

    private JasperPrint getSummaryPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getOutcomePagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getDeliverablesPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getPartnersPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getGEOLocationsPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getBudgetPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private JasperPrint getAdditionalInfoPagePrint(ReportPages page) throws Exception
    {
        //TODO -MANUJA Data population in reports
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(page.getReportPath());

        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);

        return jasperPrint;
    }

    private List<ActivityReportEnum> sortPages(List<ActivityReportEnum> pages)
    {
        Comparator<ActivityReportEnum> orderReportPages = new Comparator<ActivityReportEnum>()
        {

            @Override
            public int compare(ActivityReportEnum p1, ActivityReportEnum p2)
            {
                return p1.getOrder() == p2.getOrder() ? 0 : ((p1.getOrder() < p2.getOrder()) ? -1 : 1);
            }

        };

        Collections.sort(pages, orderReportPages);
        return pages;
    }

}
