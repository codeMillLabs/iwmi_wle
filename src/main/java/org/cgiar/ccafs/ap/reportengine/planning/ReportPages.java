/*
 * Created on Nov 26, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package org.cgiar.ccafs.ap.reportengine.planning;

/**
 * <p>
 *  ReportEnum
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public interface ReportPages {
	
	public String getReportPath();

	public int getOrder();
}
