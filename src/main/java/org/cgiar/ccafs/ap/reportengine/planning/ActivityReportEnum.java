/*
 * Created on Nov 26, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package org.cgiar.ccafs.ap.reportengine.planning;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * ActivityReportEnum to have the Activity related jasper report names
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public enum ActivityReportEnum implements ReportPages
{

    ACTIVITY_COVER_PAGE("activity_planning/cover_page.jrxml", 1),
    ACTIVITY_SUMMARY("activity_planning/activity_summary.jasper", 2),
    ACTIVITY_OUTCOMES("activity_planning/outcomes.jasper", 3),
    ACTIVITY_DELIVERABLES("activity_planning/deliverables.jasper", 4),
    ACTIVITY_PARTNERS("activity_planning/partners.jasper", 5),
    ACTIVITY_GEO_LOCATIONS("activity_planning/geo_locations.jasper", 6),
    ACTIVITY_BUDGET("activity_planning/budget.jasper", 7),
    ACTIVITY_ADDITIONAL_INFO("activity_planning/additional_info.jasper", 8);

    private String reportPath;
    private int order;

    private ActivityReportEnum(String path, int order)
    {
        this.reportPath = path;
        this.order = order;
    }

    /**
     * <p>
     * Getter method for reportPath
     * </p>
     * 
     * @return the reportPath
     */
    public String getReportPath()
    {
        return reportPath;
    }

    /**
     * <p>
     * Setter method for reportPath
     * </p>
     *
     * @param reportPath
     *            the reportPath to set
     */
    public void setReportPath(String reportPath)
    {
        this.reportPath = reportPath;
    }

    /**
     * <p>
     * Getter method for order
     * </p>
     * 
     * @return the order
     */
    public int getOrder()
    {
        return order;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return this.name();
    }

    public static List<ActivityReportEnum> getValueFromName(String[] pageNames)
    {
        List<ActivityReportEnum> enums = new ArrayList<ActivityReportEnum>();

        for (String name : pageNames)
        {
            enums.add(ActivityReportEnum.valueOf(name));
        }

        return enums;
    }

}
