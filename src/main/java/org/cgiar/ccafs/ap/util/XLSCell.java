package org.cgiar.ccafs.ap.util;

import jxl.format.Colour;
import jxl.write.WritableFont;

/**
 * XLS Cell.
 * 
 * @author Manuja
 */
public class XLSCell {

	private WritableFont font;
	private Colour background;
	int x;
	int y;
	String value;
	
	public XLSCell(WritableFont font, Colour background, int x, int y, String value)
	{
		this.font = font;
		this.background = background;
		this.x = x;
		this.y = y;
		this.value = value;
	}

	/**
	 * @return the font
	 */
	public WritableFont getFont() {
		return font;
	}

	/**
	 * @return the background
	 */
	public Colour getBackground() {
		return background;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}