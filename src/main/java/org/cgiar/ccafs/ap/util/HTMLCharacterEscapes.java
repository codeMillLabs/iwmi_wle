package org.cgiar.ccafs.ap.util;

import org.codehaus.jackson.SerializableString;
import org.codehaus.jackson.io.CharacterEscapes;

//First, definition of what to escape
public class HTMLCharacterEscapes extends CharacterEscapes {
	private final int[] asciiEscapes;

	public HTMLCharacterEscapes() {
		// start with set of characters known to require escaping (double-quote,
		// backslash etc)
		int[] esc = CharacterEscapes.standardAsciiEscapesForJSON();
		// and force escaping of a few others:
		esc['<'] = CharacterEscapes.ESCAPE_STANDARD;
		esc['>'] = CharacterEscapes.ESCAPE_STANDARD;
		esc['&'] = CharacterEscapes.ESCAPE_STANDARD;
		esc['\''] = CharacterEscapes.ESCAPE_CUSTOM;
		esc['\"'] = CharacterEscapes.ESCAPE_CUSTOM;
		asciiEscapes = esc;
	}

	// this method gets called for character codes 0 - 127
	@Override
	public int[] getEscapeCodesForAscii() {
		return asciiEscapes;
	}

	// and this for others; we don't need anything special here
	@Override
	public SerializableString getEscapeSequence(int ch) {
		System.out.println(":::::::::::::;  AS " + (char) ch);
		switch (ch) {
		case '\"':
			return new SerializableString() {

				@Override
				public String getValue() {
					return "&quot;";
				}

				@Override
				public int charLength() {
					return 2;
				}

				@Override
				public char[] asQuotedChars() {
					return null;
				}

				@Override
				public byte[] asQuotedUTF8() {
					return null;
				}

				@Override
				public byte[] asUnquotedUTF8() {
					return null;
				}
			};

		case '\'':
			return new SerializableString() {

				@Override
				public String getValue() {
					return "&#39;";
				}

				@Override
				public int charLength() {
					return 2;
				}

				@Override
				public char[] asQuotedChars() {
					return null;
				}

				@Override
				public byte[] asQuotedUTF8() {
					return null;
				}

				@Override
				public byte[] asUnquotedUTF8() {
					return null;
				}
			};
		default:
			return null;
		}
	}

}
