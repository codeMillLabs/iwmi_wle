package org.cgiar.ccafs.ap.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.data.model.DiffReport;
import org.cgiar.ccafs.ap.data.model.User;
import org.jfree.util.Log;

/**
 * Wle App Utils.
 * 
 * @author Manuja
 */
public final class WleAppUtil {

	private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle("admin");

	// Excel formatting configurations
	public static final Colour XLS_ODD_ROW_COLOUR = Colour.WHITE;
	public static final Colour XLS_EVEN_ROW_COLOUR = Colour.WHITE;
	public static final WritableFont XLS_HEADER_FONT = new WritableFont(
			WritableFont.ARIAL, 10, WritableFont.BOLD, false,
			UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
	public static final WritableFont XLS_DEFAULT_FONT = new WritableFont(
			WritableFont.ARIAL);
	public static final Colour XLS_HEADER_BACKGROUND_COLOUR = Colour.WHITE;
	
	public static final String MID_YEAR_TYPE = "mid";
	public static final String YEAR_END_TYPE = "end";

	private WleAppUtil() {

	}

	/**
	 * Get resource bundle.
	 * 
	 * @return resource bundle instance
	 */
	public static ResourceBundle getResourceBundle() {
		return RESOURCE_BUNDLE;
	}

	public static String generateCommaSeperatedStringForReport(
			final List<String> stringList) {
		StringBuffer result = new StringBuffer();
		for (String stringValue : stringList) {
			if (result.length() > 0) {
				result.append(", ");
			}

			result.append(stringValue);
		}

		return result.toString();
	}

	public static String generateCommaSeperatedStringForDB(
			final List<String> stringList) {
		StringBuffer result = new StringBuffer();
		for (String stringValue : stringList) {
			if (result.length() > 0) {
				result.append(",");
			}

			result.append(stringValue);
		}

		return result.toString();
	}

	public static List<String> generateListFromCommaSeperatedString(
			final String css) {
		List<String> stringList = new ArrayList<>();

		if (css.length() > 0) {
			stringList = Arrays.asList(css.split(","));
		}

		return stringList;
	}

	public static boolean isNotEqual(Object value1, Object value2) {

		if (value1 == null && value2 == null)
			return false;

		if (value1 == null && value2 != null)
			return true;

		if (value1 != null && value2 == null)
			return true;

		if (value1 instanceof String) {
			return !(((String) value1).equalsIgnoreCase((String) value2));
		} else if (value1 instanceof Date) {
			return ((Date) value1).getTime() != ((Date) value2).getTime();
		} else {
			return !(value1.equals(value2));
		}
	}

	public static void sendContentChangedNotification(APConfig config,
			DiffReport diffReport, String activityId, User... users) {

		if (diffReport.hasDifferent()) {
			Log.info(">>>> Some Activity Content has been changed, \n\t\t"
					+ diffReport.getDifferentReport().toString());

			// for(User user : users) {
			String subject = "Activity " + activityId + ", "
					+ diffReport.getTitle() + " Change Alert";
			String message = diffReport.getDifferentReport().toString();
			//
			// SendMail email = new SendMail(config);
			// email.send(user.getEmail(), subject, message);
			// }
		} else {
			Log.info(">>>> No Content change or modification found.");
		}
	}

	public static void createWritableWorkbook(String path, String sheetName,
			List<XLSCell> data, int maxColumnIndex) throws WriteException,
			IOException {
		WorkbookSettings workbookSettings = new WorkbookSettings();
		workbookSettings.setRationalization(false);
		WritableWorkbook workbook = Workbook.createWorkbook(new File(path), workbookSettings);
		WritableSheet sheet = workbook.createSheet(sheetName, 0);

		for (XLSCell cellData : data) {
			addCell(sheet, Border.ALL, BorderLineStyle.THIN, cellData.getX(),
					cellData.getY(), cellData.getValue(), cellData.getFont(),
					cellData.getBackground());
		}

		for (int x = 0; x < maxColumnIndex; x++) {
			CellView cell = sheet.getColumnView(x);
			cell.setAutosize(true);
			sheet.setColumnView(x, cell);
		}

		workbook.write();
		workbook.close();
	}

	private static void addCell(WritableSheet sheet, Border border,
			BorderLineStyle borderLineStyle, int col, int row, String desc,
			WritableFont font, Colour color) throws WriteException {
		WritableCellFormat cellFormat = new WritableCellFormat(font);
		cellFormat.setBorder(border, borderLineStyle);
		cellFormat.setBackground(color);

		Label label = new Label(col, row, desc, cellFormat);
		sheet.addCell(label);
	}

	public static XLSCell createHeaderCell(int x, int y, String value) {
		return new XLSCell(XLS_HEADER_FONT, XLS_HEADER_BACKGROUND_COLOUR, x, y,
				value);
	}

	public static XLSCell createOddCell(int x, int y, String value) {
		return new XLSCell(XLS_DEFAULT_FONT, XLS_ODD_ROW_COLOUR, x, y, value);
	}

	public static XLSCell createEvenCell(int x, int y, String value) {
		return new XLSCell(XLS_DEFAULT_FONT, XLS_EVEN_ROW_COLOUR, x, y, value);
	}
}
