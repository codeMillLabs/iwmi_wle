package org.cgiar.ccafs.ap.util;

import java.math.BigInteger;
import java.security.SecureRandom;

public final class SystemTokenGenerator {

	private static SecureRandom random = new SecureRandom();

	public static String getToken() {
		return new BigInteger(130, random).toString(32).toUpperCase();
	}
	
	public static String getAction(String action) {
		String[] splittedAction = action.split(";");
		String[] actionType = splittedAction[0].split("=");
		return actionType[1];
	}

	public static String getPartnerId(String action) {
		String[] splittedAction = action.split(";");
		String[] id = splittedAction[1].split("=");
		return id[1];
	}

	public static String[] getKeywordIds(String action) {
		String[] splittedAction = action.split(";");
		String[] id = splittedAction[1].split("=");
		return id[1].split(",");
	}
}
