package org.cgiar.ccafs.ap.action.reporting.scorecard;

public class ClusterSummaryCard {

	private int clusterId;
	private String clusterName;
	
	private StatusChartData outputStatus;
	private StatusChartData projectStatus;
	private SpentChartData expenditure;

	private String comment;
	private String clusterRating;

	/**
	 * @return the clusterId
	 */
	public int getClusterId() {
		return clusterId;
	}

	/**
	 * @param clusterId the clusterId to set
	 */
	public void setClusterId(int clusterId) {
		this.clusterId = clusterId;
	}

	/**
	 * @return the clusterName
	 */
	public String getClusterName() {
		return clusterName;
	}

	/**
	 * @param clusterName the clusterName to set
	 */
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	
	/**
	 * @return the outputStatus
	 */
	public StatusChartData getOutputStatus() {
		return outputStatus;
	}

	/**
	 * @param outputStatus the outputStatus to set
	 */
	public void setOutputStatus(StatusChartData outputStatus) {
		this.outputStatus = outputStatus;
	}
	
	/**
	 * @return the projectStatus
	 */
	public StatusChartData getProjectStatus() {
		return projectStatus;
	}

	/**
	 * @param projectStatus the projectStatus to set
	 */
	public void setProjectStatus(StatusChartData projectStatus) {
		this.projectStatus = projectStatus;
	}

	/**
	 * @return the expenditure
	 */
	public SpentChartData getExpenditure() {
		return expenditure;
	}

	/**
	 * @param expenditure the expenditure to set
	 */
	public void setExpenditure(SpentChartData expenditure) {
		this.expenditure = expenditure;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the clusterRating
	 */
	public String getClusterRating() {
		return clusterRating;
	}

	/**
	 * @param clusterRating the clusterRating to set
	 */
	public void setClusterRating(String clusterRating) {
		this.clusterRating = clusterRating;
	}
}