package org.cgiar.ccafs.ap.action.planning;

import static org.cgiar.ccafs.ap.data.model.ActivityReview.EXTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.INTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.COMPLETED;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.INTERNAL_CORRECTIONS;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.INTERNAL_REVIEWING;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.REVIEWING;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.dao.ActivityDAO;
import org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityBenchmarkSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityCountryManager;
import org.cgiar.ccafs.ap.data.manager.ActivityKeywordManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.manager.ActivityOtherSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityPartnerManager;
import org.cgiar.ccafs.ap.data.manager.ActivityRegionManager;
import org.cgiar.ccafs.ap.data.manager.ActivityWLEManager;
import org.cgiar.ccafs.ap.data.manager.ContactPersonManager;
import org.cgiar.ccafs.ap.data.manager.DeliverableManager;
import org.cgiar.ccafs.ap.data.manager.LeaderManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityKeyword;
import org.cgiar.ccafs.ap.data.model.ActivityObjective;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.ActivityWLEDevelopment;
import org.cgiar.ccafs.ap.data.model.BenchmarkSite;
import org.cgiar.ccafs.ap.data.model.ContactPerson;
import org.cgiar.ccafs.ap.data.model.Country;
import org.cgiar.ccafs.ap.data.model.Deliverable;
import org.cgiar.ccafs.ap.data.model.OtherSite;
import org.cgiar.ccafs.ap.data.model.Region;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.util.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * Activity comment action.
 * 
 * @author Amila Silva
 */
public class ActivityCommentAction extends BaseAction
{

    private static final long serialVersionUID = -4648705728840872410L;

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(ActivityCommentAction.class);

    // Managers
    private ActivityManager activityManager;
    private ActivityWLEManager activityWLEManager;
    private ActivityObjectiveManager activityObjectiveManager;
    private DeliverableManager deliverableManager;
    private ContactPersonManager contactPersonManager;
    private ActivityPartnerManager activityPartnerManager;
    private ActivityCountryManager activityCountryManager;
    private ActivityRegionManager activityRegionManager;
    private ActivityBenchmarkSiteManager activityBenchmarkSiteManager;
    private ActivityOtherSiteManager activityOtherSiteManager;
    private ActivityBudgetManager activityBudgetManager;
    private ActivityKeywordManager activityKeywordManager;
    private SubmissionManager submissionManager;
    private LeaderManager leadManager;
    private UserManager userManager;

    private ActivityReviewerDAO reviewerDao;
    private ActivityDAO activityDAO;

    // Model
    private int activityID;
    private Activity activity;
    private List<ActivityReview> intReviews = new ArrayList<>();
    private List<ActivityReview> extReviews = new ArrayList<>();
    private boolean satisfied = false;
    private boolean reviewCompleted = false;
    private boolean internalReviewCompleted = true;
    private boolean activityCompleted;
    private boolean canSubmit;
    private StringBuilder validationMessage;
    private Map<Boolean, String> yesNoOptions;

    @Inject
    public ActivityCommentAction(APConfig config, LogframeManager logframeManager, ActivityManager activityManager,
        ActivityWLEManager activityWLEManager, ActivityObjectiveManager activityObjectiveManager,
        ActivityPartnerManager activityPartnerManager, DeliverableManager deliverableManager,
        ContactPersonManager contactPersonManager, ActivityCountryManager activityCountryManager,
        ActivityRegionManager activityRegionManager, ActivityBenchmarkSiteManager activityBenchmarkSiteManager,
        ActivityOtherSiteManager activityOtherSiteManager, ActivityBudgetManager activityBudgetManager,
        ActivityKeywordManager activityKeywordManager, SubmissionManager submissionManager, UserManager userManager,
        ActivityReviewerDAO reviewerDao, ActivityDAO activityDAO, LeaderManager leadManager)
    {
        super(config, logframeManager);
        this.activityManager = activityManager;
        this.activityWLEManager = activityWLEManager;
        this.activityObjectiveManager = activityObjectiveManager;
        this.activityPartnerManager = activityPartnerManager;
        this.contactPersonManager = contactPersonManager;
        this.deliverableManager = deliverableManager;
        this.activityCountryManager = activityCountryManager;
        this.activityRegionManager = activityRegionManager;
        this.activityBenchmarkSiteManager = activityBenchmarkSiteManager;
        this.activityOtherSiteManager = activityOtherSiteManager;
        this.activityBudgetManager = activityBudgetManager;
        this.activityKeywordManager = activityKeywordManager;
        this.submissionManager = submissionManager;
        this.leadManager = leadManager;
        this.userManager = userManager;
        this.reviewerDao = reviewerDao;
        this.activityDAO = activityDAO;

        this.yesNoOptions = new LinkedHashMap<>();

        validationMessage = new StringBuilder();
        yesNoOptions.put(true, getText("form.options.yes"));
        yesNoOptions.put(false, getText("form.options.no"));
    }

    public Activity getActivity()
    {
        return activity;
    }

    @Override
    public String next()
    {
        save();
        return super.next();
    }

    public boolean isCanSubmit()
    {
        return canSubmit;
    }

    @Override
    public void prepare() throws Exception
    {
        super.prepare();

        validationMessage = new StringBuilder();

        String activityStringID = StringUtils.trim(this.getRequest().getParameter(APConstants.ACTIVITY_REQUEST_ID));
        try
        {
            activityID = Integer.parseInt(activityStringID);
        }
        catch (NumberFormatException e)
        {
            LOG.error("-- prepare() > There was an error parsing the activity identifier '{}'.", activityStringID, e);
        }

        LOG.info("-- prepare() > User {} load the comments for activity {} in planing section", getCurrentUser()
            .getEmail(), activityID);

        // Get the basic information about the activity
        activity = activityManager.getActivity(activityID);
        activity.setLoggedInUser(getCurrentUser());
        intReviews = reviewerDao.getReviewers(activity.getId(), activity.getRevisionId(), INTERNAL_TYPE);
        extReviews = reviewerDao.getReviewers(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE);

        activity.setInternalReviewers(intReviews);
        activity.setExternalReviewers(extReviews);

        if (activity.getActivityStatus().equals(COMPLETED))
        {
            setActivityCompleted(true);
        }
        else
        {
            setActivityCompleted(false);
        }

        if (activity.getActivityStatus().equals(ActivityStatus.SUBMITTED_TO_CA))
        {
            setInternalReviewCompleted(false);
        }

        if (activity.isReviewer(getCurrentUser()))
        {
            List<ActivityReview> pendingReviews =
                reviewerDao.hasPendingReviewsForUser(activity.getId(), activity.getRevisionId(), INTERNAL_TYPE,
                    getCurrentUser());

            if (leadManager.isInternalReviewEnabled(activity.getLeadCenter()) && !pendingReviews.isEmpty())
            {
                setSatisfied(true);
                setReviewCompleted(true);
            }
            else
            {
                pendingReviews =
                    reviewerDao.hasPendingReviewsForUser(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE,
                        getCurrentUser());
                if (pendingReviews.isEmpty())
                {
                    setSatisfied(true);
                    setReviewCompleted(true);
                }
                else
                {
                    setSatisfied(false);
                    setReviewCompleted(pendingReviews.get(0).isRoundCompleted());
                }
            }
        }

        // information
        Submission submission =
            submissionManager.getSubmission(getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
                APConstants.PLANNING_SECTION);
        canSubmit = (submission == null) ? true : false;
    }

    @Override
    public String save()
    {
        LOG.info("save activity status, Activity Id : " + activityID);

        if (completeActivity())
        {
            return SUCCESS;
        }

        if (ActivityStatus.SUBMITTED_TO_CA.equals(activity.getActivityStatus()) && isInternalReviewCompleted())
        {
            activityManager.updateActivitStatus(activityID, REVIEWING, String.valueOf(activity.getRevisionId()));
            sendNotificationsToExtReviewers(activity);
            return SUCCESS;

        }
        
        updateSatisfiedFlags();
        Activity activity = activityManager.getSimpleActivity(activityID);

        if (INTERNAL_REVIEWING.equals(activity.getActivityStatus()))
        {
            // check for pending reviews and submit a job
            List<ActivityReview> internalReviews =
                reviewerDao.hasAnyPendingReviews(activityID, activity.getRevisionId(), INTERNAL_TYPE);
            if (internalReviews.isEmpty())
            {
                activityManager.updateActivitStatus(activityID, INTERNAL_CORRECTIONS,
                    String.valueOf(activity.getRevisionId()));
                sendNotificationsToActivityOwner(activity);
            }
        }
        else if (REVIEWING.equals(activity.getActivityStatus()))
        {
            List<ActivityReview> externalReviews =
                reviewerDao.hasAnyPendingReviews(activityID, activity.getRevisionId(), EXTERNAL_TYPE);

            if (externalReviews.isEmpty())
            {
                LOG.info("No Pending external reviews, Make new Revision, Activity = {}, Revision={}",
                    activity.getActivityId(), activity.getRevisionId());
                // load all activity data -Save as Revision
                saveActivityAsNewRevision();
            }
            else
            {
                activityManager.updateActivitStatus(activityID, REVIEWING, activity.getRevisionId());
            }
        }
        return SUCCESS;
    }

    private boolean completeActivity()
    {
        if (isActivityCompleted())
        {
            activityManager.updateActivitStatus(activityID, COMPLETED, activity.getRevisionId());

            String subject = getText("planning.activity.done.subject", new String[] {
                activity.getActivityId(),
            });

            String directAccessUrl =
                config.getBaseUrl() + "/planning/mainInformation.do?activityID=" + getActivity().getId();

            User createdUser = userManager.getUser(activity.getCreatedUser());
            if (createdUser != null)
            {
                String message = getText("planning.review.done.text", new String[] {
                    createdUser.getName(), directAccessUrl
                });

                SendMail emailMessage = new SendMail(config);
                emailMessage.send(createdUser.getEmail(), subject, message);
            }
            return true;
        }
        return false;
    }

    private void updateSatisfiedFlags()
    {
        List<ActivityReview> pendingReviews =
            reviewerDao.hasPendingReviewsForUser(activity.getId(), activity.getRevisionId(), INTERNAL_TYPE,
                getCurrentUser());

        if (leadManager.isInternalReviewEnabled(activity.getLeadCenter()) && !pendingReviews.isEmpty())
        {
            ActivityReview review = pendingReviews.get(0);
            if (isReviewCompleted())
            {
                review.setSatisfied(true);
                review.setRoundCompleted(true);
            }
            reviewerDao.updateActivityReviewerStatus(activity.getId(), activity.getRevisionId(), INTERNAL_TYPE,
                getCurrentUser(), review);
        }
        else
        {
            pendingReviews =
                reviewerDao.hasPendingReviewsForUser(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE,
                    getCurrentUser());

            if (!pendingReviews.isEmpty())
            {
                ActivityReview review = pendingReviews.get(0);
                review.setSatisfied(isSatisfied());
                review.setRoundCompleted(isReviewCompleted());
                reviewerDao.updateActivityReviewerStatus(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE,
                    getCurrentUser(), review);
            }
        }
    }

    private void sendNotificationsToActivityOwner(Activity activity)
    {
        String directAccessURL =
            config.getBaseUrl() + "/planning/mainInformation.do?activityID=" + activity.getId();

        String subject = getText("planning.review.correction.subject", new String[] {
            activity.getActivityId()
        });

        User user = userManager.getUser(activity.getCreatedUser());
        if (user == null)
            return;

        String message = getText("planning.review.correction.text", new String[] {
            user.getName(), directAccessURL
        });

        SendMail emailMessage = new SendMail(config);
        emailMessage.send(user.getEmail(), subject, message);
    }

    private void sendNotificationsToExtReviewers(Activity activity)
    {
        String directAccessURL =
            config.getBaseUrl() + "/planning/mainInformation.do?activityID=" + activity.getId();

        String subject = getText("planning.review.available.subject", new String[] {
            activity.getActivityId()
        });

        // get assigned reviewers
        List<ActivityReview> reviewers =
            reviewerDao.getReviewers(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE);

        for (ActivityReview reviewer : reviewers)
        {

            User user = userManager.getById((int) reviewer.getReviewerId());
            if (user == null)
                continue;

            String message = getText("planning.review.available.text", new String[] {
                user.getName(), directAccessURL
            });

            SendMail emailMessage = new SendMail(config);
            emailMessage.send(user.getEmail(), subject, message);
        }
    }

    private void saveActivityAsNewRevision()
    {
        Activity activity = activityManager.getActivity(activityID);
        Activity revisionActivity = activity;
        revisionActivity.setId(0);

        // save activity in revision table
        int revisionActId = activityManager.saveNewActivity(revisionActivity);

        if (revisionActId != 1)
        {
            activityManager.updateActivitStatus(revisionActId, ActivityStatus.REVISION,
                String.valueOf(activity.getRevisionId()));

            // Objectives
            List<ActivityWLEDevelopment> wles = activityWLEManager.getWLEList(activityID);
            activityWLEManager.saveWLEList(wles, revisionActId);

            // Contact Persons
            List<ContactPerson> contactPersons = contactPersonManager.getContactPersons(activityID);
            for (ContactPerson cp : contactPersons)
            {
                cp.setId(-1);
            }
            contactPersonManager.saveContactPersons(contactPersons, revisionActId);

            // Objectives
            List<ActivityObjective> objectives = activityObjectiveManager.getActivityObjectives(activityID);
            for (ActivityObjective objective : objectives)
            {
                objective.setId(-1);
            }
            activityObjectiveManager.saveActivityObjectives(objectives, revisionActId);

            // Deliverables
            List<Deliverable> deliverables = deliverableManager.getDeliverables(activityID);
            for (Deliverable deliverable : deliverables)
            {
                deliverable.setId(-1);
            }
            deliverableManager.saveDeliverables(deliverables, revisionActId);

            // Partners
            List<ActivityPartner> partners = activityPartnerManager.getActivityPartners(activityID);
            for (ActivityPartner partner : partners)
            {
                partner.setId(-1);
            }
            activityPartnerManager.saveActivityPartners(partners, revisionActId);

            // Geo location
            List<Country> countries = activityCountryManager.getActvitiyCountries(activityID);
            activityCountryManager.saveActivityCountries(countries, revisionActId);

            List<Region> regions = activityRegionManager.getActvitiyRegions(activityID);
            activityRegionManager.saveActivityRegions(regions, revisionActId);

            List<BenchmarkSite> benchmarkSites = activityBenchmarkSiteManager.getActivityBenchmarkSites(activityID);
            activityBenchmarkSiteManager.saveActivityBenchmarkSites(benchmarkSites, revisionActId);

            List<OtherSite> otherSites = activityOtherSiteManager.getActivityOtherSites(activityID);
            for(OtherSite othersite : otherSites) {
                othersite.setId(-1);
            }
            activityOtherSiteManager.saveActivityOtherSites(otherSites, revisionActId);

            // Budget
            ActivityBudget budget = activityBudgetManager.getBudget(activityID);
            budget.setActivityId(revisionActId);
            activityBudgetManager.saveBudget(budget);

            // Additional Info
            List<ActivityKeyword> keywords = activityKeywordManager.getKeywordList(activityID);
            for (ActivityKeyword k : keywords)
            {
                k.setId(-1);
            }
            activityKeywordManager.saveKeywordList(keywords, revisionActId);

            // update the revision in main activity
            int newRevisionId = activityManager.updateActivityRevisionId(activityID);

            LOG.debug("::::::::::::::::::-->>>>>> NEW Revision  ID :" + newRevisionId);
            // save reviewers for next revision
            List<ActivityReview> nextRevisionReviewers = new ArrayList<ActivityReview>();
            List<ActivityReview> intReviews =
                            reviewerDao.getReviewers(activityID, "" + (newRevisionId - 1), ActivityReview.INTERNAL_TYPE);

            for (ActivityReview review : intReviews)
            {
               review.setSatisfied(true);
               review.setRoundCompleted(true);
               nextRevisionReviewers.add(review);
            }
            
            List<ActivityReview> extReviews =
                reviewerDao.getReviewers(activityID, "" + (newRevisionId - 1), ActivityReview.EXTERNAL_TYPE);

            for (ActivityReview review : extReviews)
            {
                if (review.isSatisfied())
                {
                    review.setSatisfied(true);
                    review.setRoundCompleted(true);
                } else {
                    review.setSatisfied(false);
                    review.setRoundCompleted(false);
                }
                nextRevisionReviewers.add(review);
            }
            reviewerDao.saveActivityReviewers(activityID, String.valueOf(newRevisionId), nextRevisionReviewers);

            // update current activity with new revision id
            activityManager.updateActivitStatus(activityID, ActivityStatus.IN_PROGRESS, String.valueOf(newRevisionId));

            activityDAO.validateActivity(activityID, String.valueOf(newRevisionId), false);

            String subject = getText("planning.review.done.subject", new String[] {
                activity.getActivityId(), String.valueOf(newRevisionId)
            });

            String directAccessUrl =
                config.getBaseUrl() + "/planning/mainInformation.do"
                    + "?activityID=" + getActivity().getId();

            User user = userManager.getUser(activity.getCreatedUser());
            if (user != null)
            {
                String message = getText("planning.review.done.text", new String[] {
                    user.getName(), directAccessUrl
                });

                SendMail emailMessage = new SendMail(config);
                emailMessage.send(user.getEmail(), subject, message);
            }
            LOG.info("Main activity revision updated, [ Activity Id : {}, New Revision :{}]", activityID, newRevisionId);
        }
        else
        {
            LOG.warn("Failed to save new Revision of the activity{}", activityID);
        }
    }

    /**
     * <p>
     * Getter for satisfied.
     * </p>
     * 
     * @return the satisfied
     */
    public boolean isSatisfied()
    {
        return satisfied;
    }

    /**
     * <p>
     * Setting value for satisfied.
     * </p>
     * 
     * @param satisfied
     *            the satisfied to set
     */
    public void setSatisfied(boolean satisfied)
    {
        this.satisfied = satisfied;
    }

    public boolean isReviewCompleted()
    {
        return reviewCompleted;
    }

    public void setReviewCompleted(boolean reviewCompleted)
    {
        this.reviewCompleted = reviewCompleted;
    }

    /**
     * <p>
     * Getter for activityCompleted.
     * </p>
     * 
     * @return the activityCompleted
     */
    public boolean isActivityCompleted()
    {
        return activityCompleted;
    }

    /**
     * <p>
     * Setting value for activityCompleted.
     * </p>
     * 
     * @param activityCompleted
     *            the activityCompleted to set
     */
    public void setActivityCompleted(boolean activityCompleted)
    {
        this.activityCompleted = activityCompleted;
    }

    public boolean isInternalReviewCompleted()
    {
        return internalReviewCompleted;
    }

    public void setInternalReviewCompleted(boolean internalReviewCompleted)
    {
        this.internalReviewCompleted = internalReviewCompleted;
    }

    @Override
    public void validate()
    {

    }

    public Map<Boolean, String> getYesNoOptions()
    {
        return yesNoOptions;
    }

    public boolean isCanEdit()
    {
        return getCurrentUser().isAdmin() || activity.isReviewer(getCurrentUser()) || activity.isEditable();
    }

    public String getActivityRequestParameter()
    {
        return APConstants.ACTIVITY_REQUEST_ID;
    }
}
