package org.cgiar.ccafs.ap.action.reporting.scorecard;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardSecondPage;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageProject;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScorecardMainPageFlagship;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;

import com.google.inject.Inject;

public class ScoreCardPageTwoAction extends BaseAction {

	private static final long serialVersionUID = -7857609929831308228L;

	private FlagshipFocalRegionManager flagshipFocalRegionManager;

	private int flagshipId;
	private String flagshipLongName;
	private List<ClusterSummaryCard> clusterDataSummaries;
	private List<String> activityIds;
	private ScorecardMainPageFlagship scorecardMainPageFlagship;

	@Inject
	public ScoreCardPageTwoAction(APConfig config,
			LogframeManager logframeManager,
			FlagshipFocalRegionManager flagshipFocalRegionManager) {
		super(config, logframeManager);

		this.flagshipFocalRegionManager = flagshipFocalRegionManager;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		flagshipId = Integer.parseInt(StringUtils.trim(this.getRequest()
				.getParameter("flagshipId")));
		clusterDataSummaries = new ArrayList<ClusterSummaryCard>();
		List<ScoreCardSecondPage> scoreCardSecondPages = flagshipFocalRegionManager
				.getScoreCardSecondPages(flagshipId);
		scorecardMainPageFlagship = flagshipFocalRegionManager
				.getScorecardMainPageFlagships(flagshipId);
		for (ScoreCardSecondPage scoreCardSecondPage : scoreCardSecondPages) {
			ClusterSummaryCard clusterSummaryCard = new ClusterSummaryCard();
			clusterSummaryCard.setClusterName(scoreCardSecondPage.getName());
			StatusChartData outputStatus = new StatusChartData(
					scoreCardSecondPage.getOutputGreen(),
					scoreCardSecondPage.getOutputYellow(),
					scoreCardSecondPage.getOutputRed());
			clusterSummaryCard.setOutputStatus(outputStatus);
			StatusChartData projectStatus = new StatusChartData(
					scoreCardSecondPage.getProjectGreen(),
					scoreCardSecondPage.getProjectYellow(),
					scoreCardSecondPage.getProjectRed());
			clusterSummaryCard.setProjectStatus(projectStatus);
			SpentChartData expenditure = new SpentChartData(
					scoreCardSecondPage.getSpent(),
					scoreCardSecondPage.getNotSpent());
			clusterSummaryCard.setExpenditure(expenditure);
			clusterSummaryCard
					.setClusterRating(scoreCardSecondPage.getRating());

			clusterSummaryCard.setComment(scoreCardSecondPage.getComment());
			clusterDataSummaries.add(clusterSummaryCard);
		}
		flagshipLongName = scorecardMainPageFlagship.getLongName();
		
		activityIds = new ArrayList<>();
		List<ScoreCardThirdPageProject> scoreCardThirdPageProjects = flagshipFocalRegionManager.getScoreCardThirdPageProjects(flagshipId);
		for(ScoreCardThirdPageProject scoreCardThirdPageProject : scoreCardThirdPageProjects)
		{
			activityIds.add(scoreCardThirdPageProject.getProjectId());
		}
	}

	public String getFlagshipLongName() {
		return flagshipLongName;
	}

	public List<ClusterSummaryCard> getClusterDataSummaries() {
		return clusterDataSummaries;
	}

	public void setClusterDataSummaries(
			List<ClusterSummaryCard> clusterDataSummaries) {
		this.clusterDataSummaries = clusterDataSummaries;
	}

	public List<String> getActivityIds() {
		return activityIds;
	}

	public ScorecardMainPageFlagship getScorecardMainPageFlagship() {
		return scorecardMainPageFlagship;
	}
}