/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.reporting.activities;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.action.reporting.scorecard.FlagshipSummaryCard;
import org.cgiar.ccafs.ap.action.reporting.scorecard.ScoreCardPageOne;
import org.cgiar.ccafs.ap.action.reporting.scorecard.SpentChartData;
import org.cgiar.ccafs.ap.action.reporting.scorecard.StatusChartData;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ClusterDto;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.FlagshipDto;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ProjectDto;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.RatingDto;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardMainPageFirstSection;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardSecondPage;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageOutput;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageProject;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScorecardMainPageFlagship;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionReportManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityBudgetTotals;
import org.cgiar.ccafs.ap.data.model.ActivityClusterOutput;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.ClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutputDetail;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.Deliverable;
import org.cgiar.ccafs.ap.data.model.DeliverableReviewRating;
import org.cgiar.ccafs.ap.data.model.FlagshipCluster;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegionReport;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class HomeReportingAction extends BaseAction {
	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(HomeReportingAction.class);
	private static final long serialVersionUID = 9001775749549472317L;

	// Managers
	protected ActivityManager activityManager;
	private SubmissionManager submissionManager;
	private DataLookUpManager dataLookUpManager;
	private FlagshipFocalRegionManager flagshipFocalRegionManager;
	private FlagshipFocalRegionReportManager flagshipFocalRegionReportManager;
	private ActivityBudgetManager activityBudgetManager;

	// Model
	private int year;
	private List<Activity> yearActivities;
	private boolean canSubmit;
	private List<FlagshipFocalRegionStatus> flagshipFocalRegions;

	private boolean flagshipLeader;
	private String redirectionURL;

	private ScoreCardPageOne scoreCardPageOne;

	@Inject
	public HomeReportingAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			SubmissionManager submissionManager,
			FlagshipFocalRegionManager flagshipFocalRegionManager,
			FlagshipFocalRegionReportManager flagshipFocalRegionReportManager,
			DataLookUpManager dataLookUpManager,
			ActivityBudgetManager activityBudgetManager) {
		super(config, logframeManager);
		this.activityManager = activityManager;
		this.submissionManager = submissionManager;
		this.dataLookUpManager = dataLookUpManager;
		year = config.getReportingCurrentYear();
		this.flagshipFocalRegionManager = flagshipFocalRegionManager;
		this.flagshipFocalRegionReportManager = flagshipFocalRegionReportManager;
		this.activityBudgetManager = activityBudgetManager;
	}

	@Override
	public void prepare() throws Exception {
		LOG.debug("Home Reporting Action -> prepare()");
		super.prepare();
		User user = getCurrentUser();

		ActivityStatus[] activityStatus = null;

		activityStatus = new ActivityStatus[] { ActivityStatus.NEW,
				ActivityStatus.IN_PROGRESS, ActivityStatus.VALIDATED,
				ActivityStatus.INTERNAL_REVIEWING,
				ActivityStatus.INTERNAL_CORRECTIONS,
				ActivityStatus.SUBMITTED_TO_CA, ActivityStatus.REVIEWING,
				ActivityStatus.REVIEWS_COMPLETED, ActivityStatus.SUBMITTED,
				ActivityStatus.COMPLETED };

		List<Activity> activitiesLst = new LinkedList<Activity>();
		Activity[] activities = new Activity[0];
		yearActivities = new ArrayList<>();

		loadActivities(activityStatus, activitiesLst);
		loadFlagshipData();

		scoreCardPageOne = prepareScoreCardPageOne();

		/* --------- Checking if the user can submit ------------- */
		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentReportingLogframe(),
				APConstants.REPORTING_SECTION);
		canSubmit = (submission == null) ? true : false;
	}

	@Override
	public String save() {

		flagshipFocalRegionManager.deleteScoreCardCache();

		ActivityStatus[] activityStatus = null;
		activityStatus = new ActivityStatus[] { ActivityStatus.NEW,
				ActivityStatus.IN_PROGRESS, ActivityStatus.VALIDATED,
				ActivityStatus.INTERNAL_REVIEWING,
				ActivityStatus.INTERNAL_CORRECTIONS,
				ActivityStatus.SUBMITTED_TO_CA, ActivityStatus.REVIEWING,
				ActivityStatus.REVIEWS_COMPLETED, ActivityStatus.SUBMITTED,
				ActivityStatus.COMPLETED };

		Activity[] activities = new Activity[0];
		yearActivities = new ArrayList<>();
		activities = activityManager.getActivities(config
				.getReportingCurrentYear(), this.getCurrentUser(), this
				.getCurrentUser().getEmail(), null, activityStatus);

		Map<String, FlagshipDto> activityMap = new HashMap<>();
		int activityClusterCount = 0;
		int projectCount = 0;
		int outputCount = 0;
		int clusterOutputCount = 0;

		int clusterGreen = 0;
		int clusterYellow = 0;
		int clusterRed = 0;

		int projectGreen = 0;
		int projectYellow = 0;
		int projectRed = 0;

		int outputGreen = 0;
		int outputYellow = 0;
		int outputRed = 0;

		double overallSpent = 0;
		double overallUnspent = 0;

		for (Activity activity : activities) {
			String flagship = activity.getFlagShipSRP();
			String cluster = activity.getActivityCluster();

			if (!activityMap.containsKey(flagship)) {
				activityMap.put(flagship, new FlagshipDto());
			}

			FlagshipDto flagshipDto = activityMap.get(flagship);
			Map<String, ClusterDto> clusterMap = flagshipDto.getClusters();
			if (!clusterMap.containsKey(cluster)) {
				clusterMap.put(cluster, new ClusterDto());
				activityClusterCount++;
			}

			ActivityReviewRating midYearReview = activityManager
					.getMidYearRating(activity.getActivityId());
			if (null != midYearReview) {
				ClusterDto clusterDto = clusterMap.get(cluster);
				
				ProjectDto projectDto = new ProjectDto();
				projectDto.setProjectId(activity.getActivityId());
				projectDto.setTitle(activity.getTitle());
				clusterDto.getProjects().put(activity.getActivityId(), projectDto);

				projectCount++;
				outputCount += midYearReview.getDeliverableRating().size();

				if (midYearReview.getMidYearReviewRating() == 0) {
					projectGreen++;
					flagshipDto.incrementGreen();
					clusterDto.incrementProjectGreen();
				} else if (midYearReview.getMidYearReviewRating() == 1) {
					projectYellow++;
					flagshipDto.incrementYellow();
					clusterDto.incrementProjectYellow();
				} else if (midYearReview.getMidYearReviewRating() == 2) {
					projectRed++;
					flagshipDto.incrementRed();
					clusterDto.incrementProjectRed();
				}
				
				Activity fullActivity = activityManager.getActivity(activity.getActivityId());
				for(Deliverable deliverable : fullActivity.getDeliverables())
				{
					RatingDto ratingDto = new RatingDto();
					ratingDto.setName("Output ("
							+ deliverable.getDeliverableCode() + ") : "
							+ deliverable.getDescription());
					ratingDto.setComment("");

					for (DeliverableReviewRating deliverableReviewRating : midYearReview
							.getDeliverableRating()) {

						if (deliverableReviewRating.getDeliverableId() == deliverable
								.getId()) {

							if (deliverableReviewRating
									.getMidYearReviewRating() == 0) {
								outputGreen++;
								clusterDto.incrementOutputGreen();
								ratingDto.incrementGreen();
							} else if (deliverableReviewRating
									.getMidYearReviewRating() == 1) {
								outputYellow++;
								clusterDto.incrementOutputYellow();
								ratingDto.incrementYellow();
							} else if (deliverableReviewRating
									.getMidYearReviewRating() == 2) {
								outputRed++;
								clusterDto.incrementOutputRed();
								ratingDto.incrementRed();
							}

							ratingDto.setComment(deliverableReviewRating
									.getMidYearReviewProgress());
						}
					}
					projectDto.getRatings().add(ratingDto);
				}

				ActivityBudget activityBudget = activityBudgetManager
						.getBudget(activity.getId());
				ActivityBudgetTotals budgetTotals = activityBudget.sumAll();
				double totalFunding = budgetTotals.getGrandTotalOfFunding();
				double unspent = 0;
				double totalSpent = 0;
				if (!Double.isNaN(midYearReview.getTotalSpent())) {
					totalSpent = midYearReview.getTotalSpent();
					overallSpent += totalSpent;
					if ((!Double.isNaN(totalFunding))) {
						unspent = totalFunding - midYearReview.getTotalSpent();
					}
				}
				overallUnspent += unspent;

				flagshipDto.addSpent(totalSpent);
				flagshipDto.addUnspent(unspent);
				clusterDto.addSpent(totalSpent);
				clusterDto.addNotSpent(unspent);
			}
		}

		List<FlagshipFocalRegion> flagshipFocalRegions = flagshipFocalRegionManager
				.getFlagshipFocalRegions();

		for (FlagshipFocalRegion flagship : flagshipFocalRegions) {
			int flagshipId = flagship.getId();

			FlagshipFocalRegionReport flagshipReport = flagshipFocalRegionReportManager
					.getReport(flagshipId, year, "mid");
			ScorecardMainPageFlagship scorecardMainPageFlagship = new ScorecardMainPageFlagship();

			FlagshipDto flagshipDto = activityMap.get(flagship.getOrgName());
			scorecardMainPageFlagship.setGreen(flagshipDto.getGreen());
			scorecardMainPageFlagship.setYellow(flagshipDto.getYellow());
			scorecardMainPageFlagship.setRed(flagshipDto.getRed());
			scorecardMainPageFlagship.setName(flagship.getName());
			scorecardMainPageFlagship.setLongName(flagship.getLongName());
			scorecardMainPageFlagship.setSpent(flagshipDto.getSpent());
			scorecardMainPageFlagship.setNotSpent(flagshipDto.getUnspent());
			if (null != flagshipReport) {
				scorecardMainPageFlagship.setToolTip(flagshipReport
						.getOverallDescription());
			} else {
				scorecardMainPageFlagship.setToolTip("");
			}

			int savedFlagshipId = flagshipFocalRegionManager
					.saveScoreCardMainPageFlagship(scorecardMainPageFlagship);

			Map<String, ClusterDto> clusterDtoMap = flagshipDto.getClusters();
			for (Entry<String, ClusterDto> clusterEntry : clusterDtoMap
					.entrySet()) {
				ClusterDto clusterDto = clusterEntry.getValue();

				ScoreCardSecondPage scoreCardSecondPage = new ScoreCardSecondPage();
				scoreCardSecondPage.setFlagshipId(savedFlagshipId);
				scoreCardSecondPage.setProjectGreen(clusterDto
						.getProjectGreen());
				scoreCardSecondPage.setProjectYellow(clusterDto
						.getProjectYellow());
				scoreCardSecondPage.setProjectRed(clusterDto.getProjectRed());
				scoreCardSecondPage.setOutputGreen(clusterDto.getOutputGreen());
				scoreCardSecondPage.setOutputYellow(clusterDto
						.getOutputYellow());
				scoreCardSecondPage.setOutputRed(clusterDto.getOutputRed());
				scoreCardSecondPage.setSpent(clusterDto.getSpent());
				scoreCardSecondPage.setNotSpent(clusterDto.getNotSpent());

				DataLookup dataLookup = dataLookUpManager.getLookUpdataByValue(
						LookupInfoEnum.ACTIVITY_CLUSTER, clusterEntry.getKey(),
						false);
				scoreCardSecondPage.setRating("-1");
				scoreCardSecondPage.setName(dataLookup.getDisplayName());
				scoreCardSecondPage.setComment("");

				if (null != flagshipReport) {
					for (FlagshipCluster flagshipCluster : flagshipReport
							.getFlagshipClusters()) {
						if (flagshipCluster.getClusterId() == dataLookup.getId()) {
							scoreCardSecondPage.setRating(flagshipCluster
									.getRating());
							scoreCardSecondPage.setComment("Manager : ["
									+ flagshipCluster.getManagerComment()
									+ "], Director : ["
									+ flagshipCluster.getDirectorComment()
									+ "]");
						}
					}
				}
				flagshipFocalRegionManager
				.saveScoreCardSecondPage(scoreCardSecondPage);
				
				
				Map<String, ProjectDto> projectDtoMap = clusterDto.getProjects();
				for (Entry<String, ProjectDto> projectEntry : projectDtoMap
						.entrySet()) {
					ScoreCardThirdPageProject scoreCardThirdPageProject = new ScoreCardThirdPageProject();
					scoreCardThirdPageProject.setFlagshipId(savedFlagshipId);
					ProjectDto projectDto = projectEntry.getValue();
					scoreCardThirdPageProject.setProjectId(projectDto.getProjectId());
					scoreCardThirdPageProject.setTitle(projectDto.getTitle());
					scoreCardThirdPageProject.setFlagshipShortName(flagship.getName());
					flagshipFocalRegionManager
					.saveScoreCardThirdPageProject(scoreCardThirdPageProject);
					for(RatingDto ratingDto : projectDto.getRatings())
					{
						ScoreCardThirdPageOutput scoreCardThirdPageOutput = new ScoreCardThirdPageOutput();
						scoreCardThirdPageOutput.setName(ratingDto.getName());
						scoreCardThirdPageOutput.setComment(ratingDto.getComment());
						scoreCardThirdPageOutput.setOutputGreen(ratingDto.getGreen());
						scoreCardThirdPageOutput.setOutputYellow(ratingDto.getYellow());
						scoreCardThirdPageOutput.setOutputRed(ratingDto.getRed());
						scoreCardThirdPageOutput.setProjectId(projectDto.getProjectId());
						flagshipFocalRegionManager
						.saveScoreCardThirdPageOutput(scoreCardThirdPageOutput);
					}
				}
			}
			
			if (null != flagshipReport) {
				for (FlagshipCluster flagshipCluster : flagshipReport
						.getFlagshipClusters()) {
					for (Entry<String, ClusterOutputDetail> entry : flagshipCluster
							.getClusterOutputMap().entrySet()) {

						ClusterOutputDetail outputDetail = entry.getValue();
						for (Entry<String, ActivityClusterOutput> actEntry : outputDetail
								.getActivityMap().entrySet()) {
							ActivityClusterOutput activityClusterOutput = actEntry
									.getValue();
							for (ClusterOutput clusterOutput : activityClusterOutput
									.getClusterOutputs()) {
								clusterOutputCount++;

								if (clusterOutput.getRating().equals("0")) {
									clusterGreen++;
								} else if (clusterOutput.getRating()
										.equals("1")) {
									clusterYellow++;
								} else if (clusterOutput.getRating()
										.equals("2")) {
									clusterRed++;
								}
							}
						}
					}
				}
			}
		}

		ScoreCardMainPageFirstSection scoreCardMainPageFirstSection = new ScoreCardMainPageFirstSection();
		scoreCardMainPageFirstSection.setClusterGreen(clusterGreen);
		scoreCardMainPageFirstSection.setClusterYellow(clusterYellow);
		scoreCardMainPageFirstSection.setClusterRed(clusterRed);
		scoreCardMainPageFirstSection.setProjectGreen(projectGreen);
		scoreCardMainPageFirstSection.setProjectYellow(projectYellow);
		scoreCardMainPageFirstSection.setProjectRed(projectRed);
		scoreCardMainPageFirstSection.setOutputGreen(outputGreen);
		scoreCardMainPageFirstSection.setOutputYellow(outputYellow);
		scoreCardMainPageFirstSection.setOutputRed(outputRed);
		scoreCardMainPageFirstSection.setSpent(overallSpent);
		scoreCardMainPageFirstSection.setNotSpent(overallUnspent);
		scoreCardMainPageFirstSection.setClusterCount(activityClusterCount);
		scoreCardMainPageFirstSection.setProjectCount(projectCount);
		scoreCardMainPageFirstSection.setOutputCount(outputCount);
		scoreCardMainPageFirstSection.setClusterOutputCount(clusterOutputCount);
		flagshipFocalRegionManager
				.saveMainPageFirstSection(scoreCardMainPageFirstSection);

		return SUCCESS;
	}

	private void loadActivities(ActivityStatus[] activityStatus,
			List<Activity> activitiesLst) {
		Activity[] activities;
		boolean hasRole = false;

		if (getCurrentUser().hasRole(UserRole.FL)) {
			String flagshipValue = dataLookUpManager
					.getFlagshipByUser(getCurrentUser().getEmail());
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					null, flagshipValue, activityStatus);
			hasRole = true;
			activitiesLst.addAll(asList(activities));
		}

		if (getCurrentUser().hasRole(UserRole.PL)) {
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					this.getCurrentUser().getEmail(), null, activityStatus);
			hasRole = true;
			activitiesLst.addAll(asList(activities));
		}

		if (!hasRole) {
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					this.getCurrentUser().getEmail(), null, activityStatus);
			activitiesLst.addAll(asList(activities));
		}

		Map<String, Activity> uniqueActivityMap = new HashMap<String, Activity>();

		for (Activity activity : activitiesLst) {
			if (activity.getLeader().getId() == getCurrentUser().getLeader()
					.getId()) {
				activity.setLoggedInUser(getCurrentUser());
			}

			if (uniqueActivityMap.get(activity.getActivityId()) == null) {
				uniqueActivityMap.put(activity.getActivityId(), activity);
			}
		}

		for (Activity act : uniqueActivityMap.values())
			yearActivities.add(act);

		sortActivitesByFlagships();
	}

	private void loadFlagshipData() {
		List<FlagshipFocalRegion> flagshipFocalRegionsIntemediate = flagshipFocalRegionManager
				.getFlagshipFocalRegions();
		List<FlagshipFocalRegionStatus> flagshipRegionsWithStatus = new ArrayList<>();
		int currentUserId = getCurrentUser().getId();
		flagshipLeader = false;
		redirectionURL = "";

		for (FlagshipFocalRegion ffR : flagshipFocalRegionsIntemediate) {
			FlagshipFocalRegionStatus flfrstatus = new FlagshipFocalRegionStatus();
			flfrstatus.setFlagshipFocalRegion(ffR);
			FlagshipFocalRegionReport flagshipReport = flagshipFocalRegionReportManager
					.getReport(ffR.getId(), year, "mid");
			if (null == flagshipReport) {
				flfrstatus.setStatus("Report not yet submitted");
			} else {
				flfrstatus.setStatus(flagshipReport.getStatus());
			}
			flagshipRegionsWithStatus.add(flfrstatus);

			if ((ffR.getLeaderAId() == currentUserId || ffR.getLeaderBId() == currentUserId)) {
				flagshipLeader = true;
				redirectionURL = config.getBaseUrl()
						+ "/reporting/flagshipReport.do?flagship="
						+ ffR.getId() + "&period=mid" + "&year=" + year;
			}
		}

		flagshipFocalRegions = new ArrayList<>();

		if (getCurrentUser().isWLEM() || getCurrentUser().isWLME()
				|| getCurrentUser().isWLED() || getCurrentUser().isAdmin()) {
			flagshipFocalRegions = flagshipRegionsWithStatus;
		} else {
			for (FlagshipFocalRegionStatus ffR : flagshipRegionsWithStatus) {
				if ((ffR.getFlagshipFocalRegion().getLeaderAId() == currentUserId || ffR
						.getFlagshipFocalRegion().getLeaderBId() == currentUserId)
						&& flagshipFocalRegionManager.checkReportAvailable(
								year, ffR.getFlagshipFocalRegion().getId(),
								"mid")) {
					flagshipFocalRegions.add(ffR);
				}
			}
		}
	}

	private void sortActivitesByFlagships() {
		Collections.sort(yearActivities, new Comparator<Activity>() {

			@Override
			public int compare(Activity o1, Activity o2) {
				if (o1.getFlagShipSRP() == null || o2.getFlagShipSRP() == null)
					return 0;
				else
					return o1.getFlagShipSRP().compareTo(o2.getFlagShipSRP());
			}

		});
	}

	public List<Activity> getYearActivities() {
		return yearActivities;
	}

	public List<DataLookup> getYearLookup() {
		List<DataLookup> lookups = new ArrayList<DataLookup>();
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);

		for (int i = -10; i < 5; i++) {
			String yearString = "" + (year + i);
			DataLookup d = new DataLookup();
			d.setDisplayName(yearString);
			d.setValue(yearString);
			lookups.add(d);
		}
		return lookups;
	}

	public DataLookup getCurrentYear() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		String yearString = String.valueOf(year);
		DataLookup d = new DataLookup();
		d.setDisplayName(yearString);
		d.setValue(yearString);

		return d;
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public String getReviewMode() {
		return APConstants.REVIEW_MODE;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	/**
	 * <p>
	 * Getter for year.
	 * </p>
	 * 
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <p>
	 * Setting value for year.
	 * </p>
	 * 
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the flagshipFocalRegions
	 */
	public List<FlagshipFocalRegionStatus> getFlagshipFocalRegions() {
		return flagshipFocalRegions;
	}

	public class FlagshipFocalRegionStatus {
		private FlagshipFocalRegion flagshipFocalRegion;
		private String status;

		/**
		 * @return the flagshipFocalRegion
		 */
		public FlagshipFocalRegion getFlagshipFocalRegion() {
			return flagshipFocalRegion;
		}

		/**
		 * @param flagshipFocalRegion
		 *            the flagshipFocalRegion to set
		 */
		public void setFlagshipFocalRegion(
				FlagshipFocalRegion flagshipFocalRegion) {
			this.flagshipFocalRegion = flagshipFocalRegion;
		}

		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}

		/**
		 * @param status
		 *            the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}
	}

	private ScoreCardPageOne prepareScoreCardPageOne() {
		ScoreCardPageOne scoreCardPageOne = new ScoreCardPageOne();
		ScoreCardMainPageFirstSection scoreCardMainPageFirstSection = flagshipFocalRegionManager
				.getScoreCardMainPageFirstSection();

		scoreCardPageOne.setActivityClusterCount(scoreCardMainPageFirstSection
				.getClusterCount());
		scoreCardPageOne.setActivityCount(scoreCardMainPageFirstSection
				.getProjectCount());
		scoreCardPageOne.setOutputCount(scoreCardMainPageFirstSection
				.getOutputCount());
		scoreCardPageOne.setClusterOutputCount(scoreCardMainPageFirstSection
				.getClusterOutputCount());

		StatusChartData clusterStatus = new StatusChartData(
				scoreCardMainPageFirstSection.getProjectGreen(),
				scoreCardMainPageFirstSection.getProjectYellow(),
				scoreCardMainPageFirstSection.getProjectRed());
		StatusChartData projectStatus = new StatusChartData(
				scoreCardMainPageFirstSection.getOutputGreen(),
				scoreCardMainPageFirstSection.getOutputYellow(),
				scoreCardMainPageFirstSection.getOutputRed());

		SpentChartData overallExpenditure = new SpentChartData(
				scoreCardMainPageFirstSection.getSpent(),
				scoreCardMainPageFirstSection.getNotSpent());
		scoreCardPageOne.setExpenditure(overallExpenditure);
		scoreCardPageOne.setClusterStatus(clusterStatus);
		scoreCardPageOne.setProjectStatus(projectStatus);

		List<ScorecardMainPageFlagship> scorecardMainPageFlagships = flagshipFocalRegionManager
				.getScorecardMainPageFlagships();
		for (ScorecardMainPageFlagship scorecardMainPageFlagship : scorecardMainPageFlagships) {
			StatusChartData status = new StatusChartData(
					scorecardMainPageFlagship.getGreen(),
					scorecardMainPageFlagship.getYellow(),
					scorecardMainPageFlagship.getRed());
			SpentChartData expenditure = new SpentChartData(
					scorecardMainPageFlagship.getSpent(),
					scorecardMainPageFlagship.getNotSpent());
			FlagshipSummaryCard flagshipSummaryCard = new FlagshipSummaryCard(
					scorecardMainPageFlagship.getId(),
					scorecardMainPageFlagship.getName(), status, expenditure,
					true, scorecardMainPageFlagship.getToolTip());
			scoreCardPageOne.getFlagshipSummaries().add(flagshipSummaryCard);
		}

		return scoreCardPageOne;
	}

	/**
	 * @return the scoreCardPageOne
	 */
	public ScoreCardPageOne getScoreCardPageOne() {
		return scoreCardPageOne;
	}

	/**
	 * @return the flagshipLeader
	 */
	public boolean isFlagshipLeader() {
		return flagshipLeader;
	}

	/**
	 * @return the redirectionURL
	 */
	public String getRedirectionURL() {
		return redirectionURL;
	}
}