package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

import java.util.Map;
import java.util.TreeMap;

public class FlagshipDto {

	private double spent;
	private double unspent;
	private int green;
	private int yellow;
	private int red;
	
	private Map<String, ClusterDto> clusters = new TreeMap<>();

	public double getSpent() {
		return spent;
	}

	public void setSpent(double spent) {
		this.spent = spent;
	}
	
	public void addSpent(double spent)
	{
		this.spent = this.spent + spent;
	}

	public double getUnspent() {
		return unspent;
	}

	public void setUnspent(double unspent) {
		this.unspent = unspent;
	}
	
	public void addUnspent(double spent)
	{
		this.unspent = this.unspent + spent;
	}


	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}
	
	public void incrementGreen()
	{
		this.green ++;
	}

	public int getYellow() {
		return yellow;
	}

	public void setYellow(int yellow) {
		this.yellow = yellow;
	}
	
	public void incrementYellow()
	{
		this.yellow ++;
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}
	
	public void incrementRed()
	{
		this.red ++;
	}

	public Map<String, ClusterDto> getClusters() {
		return clusters;
	}

	public void setClusters(Map<String, ClusterDto> clusters) {
		this.clusters = clusters;
	}
}