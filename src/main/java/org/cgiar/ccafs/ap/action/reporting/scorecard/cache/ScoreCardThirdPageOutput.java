package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class ScoreCardThirdPageOutput {

	private int id;
	private String projectId;
	private int outputGreen;
	private int outputYellow;
	private int outputRed;
	private String name;
	private String comment;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public int getOutputGreen() {
		return outputGreen;
	}
	
	public void setOutputGreen(int outputGreen) {
		this.outputGreen = outputGreen;
	}
	
	public int getOutputYellow() {
		return outputYellow;
	}
	
	public void setOutputYellow(int outputYellow) {
		this.outputYellow = outputYellow;
	}
	
	public int getOutputRed() {
		return outputRed;
	}
	
	public void setOutputRed(int outputRed) {
		this.outputRed = outputRed;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
}