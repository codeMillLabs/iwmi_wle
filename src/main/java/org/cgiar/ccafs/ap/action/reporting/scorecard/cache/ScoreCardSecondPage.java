package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class ScoreCardSecondPage {

	private int id;
	private int flagshipId;
	private int projectGreen;
	private int projectYellow;
	private int projectRed;
	private int outputGreen;
	private int outputYellow;
	private int outputRed;
	private String rating;
	private String name;
	private double spent;
	private double notSpent;
	private String comment;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getFlagshipId() {
		return flagshipId;
	}
	
	public void setFlagshipId(int flagshipId) {
		this.flagshipId = flagshipId;
	}
	
	public int getProjectGreen() {
		return projectGreen;
	}
	
	public void setProjectGreen(int projectGreen) {
		this.projectGreen = projectGreen;
	}
	
	public int getProjectYellow() {
		return projectYellow;
	}
	
	public void setProjectYellow(int projectYellow) {
		this.projectYellow = projectYellow;
	}
	
	public int getProjectRed() {
		return projectRed;
	}
	
	public void setProjectRed(int projectRed) {
		this.projectRed = projectRed;
	}
	
	public int getOutputGreen() {
		return outputGreen;
	}
	
	public void setOutputGreen(int outputGreen) {
		this.outputGreen = outputGreen;
	}
	
	public int getOutputYellow() {
		return outputYellow;
	}
	
	public void setOutputYellow(int outputYellow) {
		this.outputYellow = outputYellow;
	}
	
	public int getOutputRed() {
		return outputRed;
	}
	
	public void setOutputRed(int outputRed) {
		this.outputRed = outputRed;
	}
	
	public String getRating() {
		return rating;
	}
	
	public void setRating(String rating) {
		this.rating = rating;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getSpent() {
		return spent;
	}
	
	public void setSpent(double spent) {
		this.spent = spent;
	}
	
	public double getNotSpent() {
		return notSpent;
	}
	
	public void setNotSpent(double notSpent) {
		this.notSpent = notSpent;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}