/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.planning;

import static org.cgiar.ccafs.ap.data.model.ActivityReview.EXTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.INTERNAL_TYPE;
import static org.cgiar.ccafs.ap.util.WleAppUtil.sendContentChangedNotification;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityWLEManager;
import org.cgiar.ccafs.ap.data.manager.BudgetPercentageManager;
import org.cgiar.ccafs.ap.data.manager.ContactPersonManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.MilestoneManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.BudgetPercentage;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.DiffReport;
import org.cgiar.ccafs.ap.data.model.Logframe;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.cgiar.ccafs.ap.data.model.Milestone;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.cgiar.ccafs.ap.util.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MainInformationPlanningAction extends BaseAction
{

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(MainInformationPlanningAction.class);
    private static final long serialVersionUID = -20327742862730172L;

    // Managers
    ActivityManager activityManager;
    ActivityWLEManager activityWLEManager;
    BudgetPercentageManager budgetPercentageManager;
    ContactPersonManager contactPersonManager;
    MilestoneManager milestoneManager;
    SubmissionManager submissionManager;
    DataLookUpManager dataLookUpManager;
    UserManager userManager;

    // Model
    private int activityID;
    private StringBuilder validationMessage;
    private List<BudgetPercentage> budgetPercentages;
    private Activity activity;
    private Milestone[] milestones;
    private Map<String, String> genderOptions;
    private String genderIntegrationOption;
    private boolean canSubmit;
    private List<DataLookup> yearLookup;
    private List<DataLookup> flagshipLookup;
    private List<DataLookup> activityClusterLookup;
    private List<DataLookup> leadCentersLookup;
    private List<DataLookup> wleDevelopments;
    private String logFramDocPath;
    private List<User> extReviewers;
    private List<User> intReviewers;

    @Inject
    public MainInformationPlanningAction(APConfig config, LogframeManager logframeManager,
        ActivityManager activityManager, ActivityWLEManager activityWLEManager,
        BudgetPercentageManager budgetPercentageManager, ContactPersonManager contactPersonManager,
        MilestoneManager milestoneManager, SubmissionManager submissionManager, DataLookUpManager dataLookUpManager,
        UserManager userManager)
    {

        super(config, logframeManager);
        this.activityWLEManager = activityWLEManager;
        this.activityManager = activityManager;
        this.budgetPercentageManager = budgetPercentageManager;
        this.contactPersonManager = contactPersonManager;
        this.milestoneManager = milestoneManager;
        this.submissionManager = submissionManager;
        this.dataLookUpManager = dataLookUpManager;
        this.userManager = userManager;

        this.genderOptions = new LinkedHashMap<>();
        genderOptions.put("1", getText("form.options.yes"));
        genderOptions.put("0", getText("form.options.no"));
    }

    public Activity getActivity()
    {
        return activity;
    }

    public String getActivityRequestParameter()
    {
        return APConstants.ACTIVITY_REQUEST_ID;
    }

    public List<BudgetPercentage> getBudgetPercentages()
    {
        return budgetPercentages;
    }

    public int getCurrentYear()
    {
        return config.getPlanningCurrentYear();
        //	   return Calendar.getInstance().get(Calendar.YEAR);
    }

    public int getEndYear()
    {
        return config.getEndYear();
    }

    public String getGenderIntegrationOption()
    {
        return genderIntegrationOption;
    }

    public Map<String, String> getGenderOptions()
    {
        return genderOptions;
    }

    public boolean getHasGender()
    {
        if (this.getActivity().getGenderIntegrationsDescription() != null)
        {
            if (this.getGenderIntegrationOption() == null || this.getGenderIntegrationOption().equals("1"))
            {
                return true;
            }
        }
        return false;
    }

    public Milestone[] getMilestones()
    {
        return milestones;
    }

    public List<User> getExtReviewers()
    {
        return extReviewers;
    }

    public void setExtReviewers(List<User> extReviewers)
    {
        this.extReviewers = extReviewers;
    }

    public List<User> getIntReviewers()
    {
        return intReviewers;
    }

    public void setIntReviewers(List<User> intReviewers)
    {
        this.intReviewers = intReviewers;
    }

    public String getPublicActivtyRequestParameter()
    {
        return APConstants.PUBLIC_ACTIVITY_ID;
    }

    public int getStartYear()
    {
        return config.getStartYear();
    }

    public List<DataLookup> getYearLookup()
    {
        List<DataLookup> lookups = new ArrayList<DataLookup>();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);

        for (int i = -10; i < 10; i++)
        {
            String yearString = "" + (year + i);
            DataLookup d = new DataLookup();
            d.setDisplayName(yearString);
            d.setValue(yearString);
            lookups.add(d);
        }
        return lookups;
    }

    public void setYearLookup(List<DataLookup> yearLookup)
    {
        this.yearLookup = yearLookup;
    }

    public boolean isCanSubmit()
    {
        return canSubmit;
    }

    @Override
    public String next()
    {
        save();
        return super.next();
    }

    public List<DataLookup> getFlagshipLookup()
    {
        return flagshipLookup;
    }

    public void setFlagshipLookup(List<DataLookup> flagshipLookup)
    {
        this.flagshipLookup = flagshipLookup;
    }

    public List<DataLookup> getActivityClusterLookup()
    {
        return activityClusterLookup;
    }

    public void setActivityClusterLookup(List<DataLookup> activityClusterLookup)
    {
        this.activityClusterLookup = activityClusterLookup;
    }

    public List<DataLookup> getLeadCentersLookup()
    {
        return leadCentersLookup;
    }

    public void setLeadCentersLookup(List<DataLookup> leadCentersLookup)
    {
        this.leadCentersLookup = leadCentersLookup;
    }

    public List<DataLookup> getWleDevelopments()
    {
        return wleDevelopments;
    }

    public void setWleDevelopments(List<DataLookup> wleDevelopments)
    {
        this.wleDevelopments = wleDevelopments;
    }

    @Override
    public void prepare() throws Exception
    {
        super.prepare();

        validationMessage = new StringBuilder();

        String activityStringID = StringUtils.trim(this.getRequest().getParameter(APConstants.ACTIVITY_REQUEST_ID));
        try
        {
            activityID = Integer.parseInt(activityStringID);
        }
        catch (NumberFormatException e)
        {
            LOG.error("-- prepare() > There was an error parsing the activity identifier '{}'.", activityStringID, e);
        }

        LOG.info("-- prepare() > User {} load the main information for activity {} in planing section",
            getCurrentUser().getEmail(), activityID);

        DataLookup dummyLookup = createDummyLookup();

        activityClusterLookup = new ArrayList<DataLookup>();
        activityClusterLookup.add(dummyLookup);

        flagshipLookup = new ArrayList<DataLookup>();
        flagshipLookup.add(dummyLookup);
        flagshipLookup.addAll(dataLookUpManager.getAllLookUpdata(LookupInfoEnum.FLAGSHIP_SRP));

        leadCentersLookup = dataLookUpManager.getAllLookUpdata(LookupInfoEnum.LEADER_CENTER);
        wleDevelopments = dataLookUpManager.getAllLookUpdata(LookupInfoEnum.WLE_DEVELOPMENTS);
        //		activityClusterLookup = dataLookUpManager.getAllLookUpdata(LookupInfoEnum.ACTIVITY_CLUSTER);

        Logframe logFrame = getCurrentPlanningLogframe();
        milestones = milestoneManager.getMilestoneList(logFrame);
        logFramDocPath = "../resources/documents/" + logFrame.getName() + ".pdf";

        // Get the basic information about the activity
        activity = activityManager.getActivityStatusInfo(activityID);
        activity.setLoggedInUser(getCurrentUser());

        activity.setWles(activityWLEManager.getWLEList(activityID));

        // reviewers
        Set<User> internalReviewerUsers = getInternalReviewUsers();
        intReviewers = new ArrayList<>(internalReviewerUsers);
        extReviewers = userManager.getUsersByRole(UserRole.PI);

        // Set contact persons
        activity.setContactPersons(contactPersonManager.getContactPersons(activityID));

        String revision = activity.getRevisionId().equals("0") ? "0" : "" + Integer.valueOf(activity.getRevisionId());
        List<ActivityReview> internalReviewers = activityManager.getReviewers(activityID, revision, INTERNAL_TYPE);
        List<ActivityReview> externalReviewers = activityManager.getReviewers(activityID, revision, EXTERNAL_TYPE);

        for (ActivityReview actReview : internalReviewers)
        {
            activity.getActivityInternalReviewers().add((int) actReview.getReviewerId());
        }

        for (ActivityReview actReview : externalReviewers)
        {
            activity.getActivityExternalReviewers().add((int) actReview.getReviewerId());
        }

        // If the workplan was submitted before the user can't save new
        // information
        Submission submission =
            submissionManager.getSubmission(getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
                APConstants.PLANNING_SECTION);
        canSubmit = (submission == null) ? true : false;

        // Remove all contact persons in case user clicked on submit button
        if (activity.getContactPersons() != null)
        {
            if (this.getRequest().getMethod().equalsIgnoreCase("post"))
            {
                activity.getContactPersons().clear();
                LOG.debug("-- prepare() > All the case studies related to the leader {} was deleted from model",
                    getCurrentUser().getLeader().getId());
            }
        }
    }

    private Set<User> getInternalReviewUsers()
    {
        Set<User> internalReviewers = new TreeSet<User>();
        for (User tlUser : userManager.getUsersByRole(UserRole.TL))
        {
            internalReviewers.add(tlUser);
        }

//        for (User plUser : userManager.getUsersByRole(UserRole.PL))
//        {
//            internalReviewers.add(plUser);
//        }

        for (User spUser : userManager.getUsersByRole(UserRole.SP))
        {
            internalReviewers.add(spUser);
        }
        return internalReviewers;
    }

    private DataLookup createDummyLookup()
    {
        DataLookup dummyLookup = new DataLookup();
        dummyLookup.setId(-1);
        dummyLookup.setDisplayName("");
        dummyLookup.setValue("-1");
        return dummyLookup;
    }

    @Override
    public String save()
    {
        boolean success = true;
        String finalMessage;

        Activity prevActivity = activityManager.getActivity(activityID);
        DiffReport diffReport = diffCheck(prevActivity, activity);
        sendContentChangedNotification(config, diffReport, activity.getFormattedId(), getCurrentUser());

        if (activityManager.updateMainInformation(activity))
        {

            if (!contactPersonManager.saveContactPersons(activity.getContactPersons(), activityID))
            {
                LOG.warn("-- save() > There was a problem saving the contact persons for activity {}", activity.getId());
                success = false;
            }

            if (success)
            {
                activityWLEManager.removeActivityWLEs(activityID);
                activityWLEManager.saveWLEList(activity.getWles(), activityID);
                updateReviewers();
                activityManager.updateActivitStatus(activityID, activity.getActivityStatus(), activity.getRevisionId());
            }

            // As there were changes in the activity we should mark the
            // validation as false
            if (activity.needValidation())
            {
                activity.setValidated(false);
                activityManager.validateActivity(activity);
            }
            else
            {
                activity.setValidated(true);
            }

        }
        else
        {
            success = false;
        }

        if (success)
        {

            // Check if there is a validation message.
            if (validationMessage.toString().isEmpty())
            {
                addActionMessage(getText("saving.success", new String[] {
                    getText("planning.mainInformation")
                }));
                LOG.info("-- save() > The user {} saved the main information of the activity {} successfully.",
                    getCurrentUser().getEmail(), activityID);
            }
            else
            {
                // If there were validation messages show them in a warning
                // message.
                finalMessage = getText("saving.success", new String[] {
                    getText("planning.mainInformation")
                });
                finalMessage += getText("saving.missingFields", new String[] {
                    validationMessage.toString()
                });

                addActionWarning(Capitalize.capitalizeString(finalMessage));
                LOG.info("-- save() > The user {} saved the main information of the activity {} with empty fields.",
                    getCurrentUser().getEmail(), activityID);
            }

            return SUCCESS;
        }
        else
        {
            LOG.warn("-- save() > The user {} had problems to save the main information of the activity {}.",
                getCurrentUser().getEmail(), activityID);
            finalMessage = getText("saving.problem");
            addActionError(finalMessage);
            return INPUT;
        }
    }

    private void updateReviewers()
    {
        List<User> internalReviewers = userManager.getUsersById(activity.getActivityInternalReviewers());
        List<User> externalReviewers = userManager.getUsersById(activity.getActivityExternalReviewers());

        for (User user : internalReviewers)
        {
            ActivityReview review = new ActivityReview();
            review.setActivityId(activity.getId());
            review.setActivityRevision(activity.getRevisionId());
            review.setReviewerId(user.getId());
            review.setReviewerName(user.getName());
            review.setType(ActivityReview.INTERNAL_TYPE);

            activity.getInternalReviewers().add(review);
        }

        List<ActivityReview> extReviews =
            activityManager.getReviewers(activity.getId(), activity.getRevisionId(), ActivityReview.EXTERNAL_TYPE);
        for (User user : externalReviewers)
        {
            ActivityReview review = new ActivityReview();
            review.setActivityId(activity.getId());
            review.setActivityRevision(activity.getRevisionId());
            review.setReviewerId(user.getId());
            review.setReviewerName(user.getName());
            review.setType(ActivityReview.EXTERNAL_TYPE);

            if (!extReviews.contains(review))
            {
                activity.getExternalReviewers().add(review);
            }
        }

        activityManager.addActivtyReviewers(activity);
    }

    public void setActivity(Activity activity)
    {
        this.activity = activity;
    }

    @Override
    public void validate()
    {
        boolean problem = false;

        if (save)
        {

            if (activity.getTitle().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.title") + ", ");
                problem = true;
            }

            if (activity.getDescription().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.descripition") + ", ");
                problem = true;
            }

            if (activity.getProjectName().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.projectName") + ", ");
                problem = true;
            }

            if (activity.getLeadCenter().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.leadCenter") + ", ");
                problem = true;
            }

            if (activity.getActivityCluster().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.activityCluster") + ", ");
                problem = true;
            }

            if (activity.getFlagShipSRP().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.flagShipSRP") + ", ");
                problem = true;
            }

            // Validate if there is at least one contact person, if there is a
            // contact person
            // without name nor email remove it from the list.
            if (activity.getContactPersons() != null)
            {
                if (!activity.getContactPersons().isEmpty())
                {
                    boolean invalidEmail = false;
                    for (int c = 0; c < activity.getContactPersons().size(); c++)
                    {
                        // If there is a contact email, check if it is valid
                        if (!activity.getContactPersons().get(c).getEmail().isEmpty())
                        {
                            if (!EmailValidator.isValidEmail(activity.getContactPersons().get(c).getEmail()))
                            {
                                invalidEmail = true;
                                problem = true;
                            }
                        }
                        else
                        {
                            if (activity.getContactPersons().get(c).getName().isEmpty())
                            {
                                activity.getContactPersons().remove(c);
                                c--;
                            }
                        }
                    }

                    if (invalidEmail)
                    {
                        validationMessage.append(getText("planning.mainInformation.contactEmail") + ", ");
                    }
                }
            }

            // The list could be empty after the last validation
            if (activity.getContactPersons().isEmpty())
            {
                validationMessage.append(getText("planning.mainInformation.validation.contactPerson") + ", ");
                problem = true;
            }

            if (activity.getStartDate() == null)
            {
                validationMessage.append(getText("planning.mainInformation.startDate") + ", ");
                problem = true;
            }

            if (activity.getEndDate() == null)
            {
                validationMessage.append(getText("planning.mainInformation.endDate") + ", ");
                problem = true;
            }

            // Change the last colon by a period
            if (problem == true)
            {
                validationMessage.setCharAt(validationMessage.lastIndexOf(","), '.');
            }
        }

    }

    private DiffReport diffCheck(Activity prev, Activity current)
    {
        if (prev == null)
            return null;

        DiffReport report = prev.diff(current);
        report.setTitle("Activity Summary");
        return report;
    }

    public String getLogFramDocPath()
    {
        return logFramDocPath;
    }

    public void setLogFramDocPath(String logFramDocPath)
    {
        this.logFramDocPath = logFramDocPath;
    }
}
