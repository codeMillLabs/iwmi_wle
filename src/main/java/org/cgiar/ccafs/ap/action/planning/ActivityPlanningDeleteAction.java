/*
 * FILENAME
 *     ActivityPlanningDeleteAction.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.action.planning;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Delete activity planing action.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
public class ActivityPlanningDeleteAction extends BaseAction
{
    private static final long serialVersionUID = 6522690026048042173L;
    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(ActivityPlanningDeleteAction.class);
    
    private long activityId;

    // Managers
    private ActivityManager activityManager;
    
    @Inject
    public ActivityPlanningDeleteAction(APConfig config, LogframeManager logframeManager,
        ActivityManager activityManager)
    {
        super(config, logframeManager);
        this.activityManager = activityManager;
    }
    
    @Override
    public String execute() throws Exception {
       
        activityManager.deleteActivity(activityId);
        
        LOG.info("Activity marked as deleted. [ User : {}, ActivityId : {} ]", getCurrentUser().getEmail(), activityId);
        
        return SUCCESS;
    }
    
    @Override
    public void prepare() throws Exception {

        // Verify if there is a activityID parameter
        // If there is a flagshipValue take its values
        String idValue = StringUtils.trim(this.getRequest().getParameter(
                APConstants.ACTIVITY_REQUEST_ID));
        activityId = (idValue == null) ? -1L : Long.valueOf(idValue);
    }

}
