/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.reporting.activities;

import static org.cgiar.ccafs.ap.util.WleAppUtil.MID_YEAR_TYPE;
import static org.cgiar.ccafs.ap.util.WleAppUtil.YEAR_END_TYPE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.DeliverableManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionReportManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityBudgetTotals;
import org.cgiar.ccafs.ap.data.model.ActivityClusterOutput;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutputDetail;
import org.cgiar.ccafs.ap.data.model.CommentDto;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.Deliverable;
import org.cgiar.ccafs.ap.data.model.DeliverableReviewRating;
import org.cgiar.ccafs.ap.data.model.FlagshipCluster;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegionReport;
import org.cgiar.ccafs.ap.data.model.FlagshipReportData;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.cgiar.ccafs.ap.util.SendMail;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlagshipReportingAction extends BaseAction {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(FlagshipReportingAction.class);
	private static final long serialVersionUID = 9001775749549472317L;

	// Managers
	protected ActivityManager activityManager;
	private SubmissionManager submissionManager;
	private FlagshipFocalRegionReportManager flagshipFocalRegionReportManager;
	private FlagshipFocalRegionManager flagshipFocalRegionManager;
	private DataLookUpManager dataLookUpManager;
	private UserManager userManager;
	private ActivityBudgetManager budgetManager;
	private DeliverableManager deliverableManager;

	// Model
	private boolean canSubmit;
	private int flagshipId;
	private String periodStart;
	private String periodEnd;
	private int year;
	private Map<Integer, FlagshipFocalRegion> flagshipFocalRegionMap = new HashMap<Integer, FlagshipFocalRegion>();
	private FlagshipFocalRegionReport flagshipReport;
	private FlagshipFocalRegion flagshipFocalRegion;
	
	private boolean reportAvaialble = false;

	@Inject
	public FlagshipReportingAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			SubmissionManager submissionManager,
			FlagshipFocalRegionReportManager flagshipFocalRegionReportManager,
			FlagshipFocalRegionManager flagshipFocalRegionManager,
			DataLookUpManager dataLookUpManager, UserManager userManager,
			ActivityBudgetManager budgetManager,
			DeliverableManager deliverableManager) {
		super(config, logframeManager);
		this.activityManager = activityManager;
		this.submissionManager = submissionManager;
		this.flagshipFocalRegionReportManager = flagshipFocalRegionReportManager;
		this.flagshipFocalRegionManager = flagshipFocalRegionManager;
		this.dataLookUpManager = dataLookUpManager;
		this.userManager = userManager;
		this.budgetManager = budgetManager;
		this.deliverableManager = deliverableManager;

		List<FlagshipFocalRegion> focalRegions = this.flagshipFocalRegionManager
				.getFlagshipFocalRegions();
		for (FlagshipFocalRegion focalRegion : focalRegions) {
			flagshipFocalRegionMap.put(focalRegion.getId(), focalRegion);
		}
	}

	@Override
	public String save() {
		flagshipFocalRegionReportManager.saveReport(flagshipReport);

		addActionMessage("Flagship report saved successfully");
		return SUCCESS;
	}

	@Override
	public String sendDraft() {
		StringBuilder error = validateReport();
		if (error.length() > 0) {
			addActionWarning(Capitalize.capitalizeString(error.toString()));
			return INPUT;
		}

		if(flagshipReport.getDirectorReviewDone() == 1 && flagshipReport.getManagerReviewDone() ==1)
		{
			flagshipReport.setState(FlagshipFocalRegionReport.APPROVED_STATE);
			flagshipFocalRegionReportManager.saveReport(flagshipReport);
			 sendReportOnPublishNotification();
				addActionMessage("Flagship Report has been successfully published");

		}
		else
		{
			flagshipReport.setState(FlagshipFocalRegionReport.PENDING_STATE);
			flagshipFocalRegionReportManager.saveReport(flagshipReport);
			sendReportSubmissionNotification();
			addActionMessage("Flagship Report has been sent to the WLE Director, And WLE M&E Manager successfully");
		}
		return SUCCESS;
	}

	@Override
	public String confirm() {

		if (getCurrentUser().isWLME()) {
			flagshipReport.setManagerReviewDone(1);
		}

		if (getCurrentUser().isWLED()) {
			flagshipReport.setDirectorReviewDone(1);
		}

		if (flagshipReport.getManagerReviewDone() == 1
				&& flagshipReport.getDirectorReviewDone() == 1) {
			flagshipReport.setState(FlagshipFocalRegionReport.REVIEWED_STATE);
			sendReportReviewedNotification();
			addActionMessage("Flagship Report reviewed successfully and been sent to the Flagship Leader");
		}

		flagshipFocalRegionReportManager.saveReport(flagshipReport);
		return SUCCESS;
	}

	private StringBuilder validateReport() {
		StringBuilder error = new StringBuilder();

		for (FlagshipCluster flagshipCluster : flagshipReport
				.getFlagshipClusters()) {
			if (isEmpty(flagshipCluster.getOverallStatus())) {
				error.append("[" + flagshipCluster.getName()
						+ "] Overall status is empty, ");
			}

			if (flagshipCluster.getRating().equals("-1")) {
				error.append("[" + flagshipCluster.getName()
						+ "] Overall reating is not selected, ");
			}

			for (Entry<String, ClusterOutputDetail> entry : flagshipCluster
					.getClusterOutputMap().entrySet()) {

				ClusterOutputDetail outputDetail = entry.getValue();
				Map<String, String> outputDetailData = new HashMap<String, String>();
				outputDetailData.put("code", outputDetail.getCode());
				outputDetailData.put("description",
						outputDetail.getDescription());
				outputDetailData.put("mngr_comment",
						outputDetail.getManagerComment());
				outputDetailData.put("dir_comment",
						outputDetail.getDirectorComment());

				for (CommentDto comment : flagshipCluster.getOutputComments()) {
					if (comment.getCode().equals(outputDetail.getCode())) {
						outputDetailData.put("comment", comment.getComment());

						if (isEmpty(comment.getComment())) {
							error.append("["
									+ outputDetail.getCode()
									+ "] Comment on Cluster Output Progress is empty, ");
						}
						break;
					}
				}
			}
		}

		if (isEmpty(flagshipReport.getOverallDescription())) {
			error.append("Overall Flagship Status is empty");
		}

		return error;
	}

	private boolean isEmpty(String value) {
		return value == null || value.trim().isEmpty();
	}

	private void sendReportSubmissionNotification() {
		FlagshipFocalRegion fsfr = flagshipFocalRegionMap.get(flagshipReport
				.getFlagshipId());

		String subject = "[WLE Reporting] : Flagship/Focal Region Report Flagship : ["
				+ fsfr.getName()
				+ "], Year : ["
				+ flagshipReport.getYear()
				+ "] submitted";

		String emailBody = "Flagship/Focal Region Report Flagship : ["
				+ fsfr.getName()
				+ "], Year : ["
				+ flagshipReport.getYear()
				+ "] submitted for your review / approval. Please click below link to review. \n " 
				+ getReportLink()
				+ " \n\nKind regards, WLE Team\n";

		
		List<User> wleManagers = userManager.getUsersByRole(UserRole.WLME);
		List<User> wleDirectors = userManager.getUsersByRole(UserRole.WLED);

		List<String> recipients = new ArrayList<>();
		for (User wleManager : wleManagers) {
			recipients.add(wleManager.getEmail());
		}
		for (User wleDirector : wleDirectors) {
			recipients.add(wleDirector.getEmail());

		}
		SendMail sendMail = new SendMail(this.config);
		for(String recipient : recipients)
		{
			sendMail.send(recipient, subject, emailBody);
			LOG.info(
					"Sending flagship report submission notification TO : {}, Message : {}",
					recipient, emailBody);
		}
	}
	
	private String getReportLink()
	{
		return config.getBaseUrl() + "/reporting/flagshipReport.do?flagship="
				+flagshipReport.getFlagshipId()
				+"&period="+flagshipReport.getPeriod()
				+"&year="+flagshipReport.getYear();  
	}

	private void sendReportReviewedNotification() {
		FlagshipFocalRegion fsfr = flagshipFocalRegionMap.get(flagshipReport
				.getFlagshipId());

		String subject = "[WLE Reporting] : Flagship/Focal Region Report Flagship : ["
				+ fsfr.getName()
				+ "], Year : ["
				+ flagshipReport.getYear()
				+ "] reviewed";

		String emailBody = "Action required: \n\n"
				+ "WLE Director and M&E Manager have reviewed your flagship report,"
				+ " please click below to read and address comments as appropriate." 
				+ "\n"+getReportLink()+""
				+ " \n\nKind regards, WLE Team\n";

		
		List<User> leaders = new ArrayList<>();
		int leaderAId = flagshipFocalRegion.getLeaderAId();
		if (0 != leaderAId) {
			leaders.add(userManager.getById(leaderAId));
		}
		int leaderBId = flagshipFocalRegion.getLeaderBId();
		if (0 != leaderBId) {
			leaders.add(userManager.getById(leaderBId));
		}
		SendMail sendMail = new SendMail(this.config);
		for (User leader : leaders) {
			sendMail.send(leader.getEmail(), subject, emailBody);
			LOG.info(
					"Sending flagship report reviewed notification TO : {}, Message : {}",
					leader.getEmail(), emailBody);
		}
	}
	
	
	private void sendReportOnPublishNotification() {

		List<User> users = new ArrayList<>();
		int leaderAId = flagshipFocalRegion.getLeaderAId();
		if (0 != leaderAId) {
			users.add(userManager.getById(leaderAId));
		}
		int leaderBId = flagshipFocalRegion.getLeaderBId();
		if (0 != leaderBId) {
			users.add(userManager.getById(leaderBId));
		}
		
		List<User> wleManagers = userManager.getUsersByRole(UserRole.WLME);
		List<User> wleDirectors = userManager.getUsersByRole(UserRole.WLED);

		for (User wleManager : wleManagers) {
			users.add(wleManager);
		}

		for (User wleDirector : wleDirectors) {
			users.add(wleDirector);
		}

		FlagshipFocalRegion fsfr = flagshipFocalRegionMap.get(flagshipReport
				.getFlagshipId());

		String subject = "[WLE Reporting] : Flagship/Focal Region Report Flagship : ["
				+ fsfr.getName()
				+ "], Year : ["
				+ flagshipReport.getYear()
				+ "] published";

		String emailBody = "Flagship [ "
				+ flagshipFocalRegion.getLongName() 
				+ "] Report has been published. Please go to the below link to view the report." 
				+ "\n"+getReportLink()+""
				+ " \n\nKind regards, WLE Team\n";

		SendMail sendMail = new SendMail(this.config);
		for (User user : users) {
			sendMail.send(user.getEmail(), subject, emailBody);
			LOG.info(
					"Sending flagship published notification TO : {}, Message : {}",
					user.getEmail(), emailBody);
		}
	}

	@Override
	public String approve() {

		flagshipFocalRegionReportManager.updateState(flagshipReport.getId(),
				FlagshipFocalRegionReport.APPROVED_STATE);
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		flagshipId = Integer.parseInt(StringUtils.trim(this.getRequest()
				.getParameter("flagship")));
		year = Integer.parseInt(StringUtils.trim(
				this.getRequest().getParameter("year")).replace(",", ""));
		String period = StringUtils.trim(this.getRequest().getParameter(
				"period"));
		
		reportAvaialble = flagshipFocalRegionManager.checkReportAvailable(year,
				flagshipId, period);

		if (period.equals("mid")) {
			periodStart = "01 January " + year;
			periodEnd = "30 June " + year;
			period = MID_YEAR_TYPE;
		} else {
			periodStart = "01 July " + year;
			periodEnd = "31 December " + year;
			period = YEAR_END_TYPE;
		}

		flagshipFocalRegion = flagshipFocalRegionMap.get(flagshipId);

		LOG.info(
				"Flagship / Focal Regions report request received for flagship id {} and year {}",
				flagshipId, year);

		flagshipReport = flagshipFocalRegionReportManager.getReport(flagshipId,
				year, period);
		if (null == flagshipReport) {
			LOG.info(
					"Creating sample report for for flagship id {} and year {}",
					flagshipId, year);
			flagshipReport = createSampleReport(year, flagshipId);
			flagshipReport.setFlagshipId(flagshipId);
			flagshipReport.setYear(year);
			flagshipReport.setPeriod(period);
		}

		/* --------- Checking if the user can submit ------------- */
		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentReportingLogframe(),
				APConstants.REPORTING_SECTION);
		canSubmit = (submission == null) ? true : false;
		
		if(!reportAvaialble)
		{
			addActionWarning("This is report is not available to edit because of some of the related activity reports are not published");
		}
	}

	private FlagshipFocalRegionReport createSampleReport(final int year,
			final int flagshipId) {
		FlagshipFocalRegionReport flagshipFocalRegionReport = new FlagshipFocalRegionReport();
		flagshipFocalRegionReport.setManagerComment("");
		flagshipFocalRegionReport.setDirectorComment("");

		List<Integer> clusterIds = flagshipFocalRegionManager
				.getClusterIds(flagshipId);
		for (Integer clusterId : clusterIds) {
			DataLookup cluster = dataLookUpManager.getLookUpdataById(
					LookupInfoEnum.ACTIVITY_CLUSTER, clusterId.longValue());
			
			Map<String, Boolean> activityCacheMap = new HashMap<>();

			FlagshipCluster flagshipCluster = new FlagshipCluster();
			flagshipCluster.setName(cluster.getDisplayName());
			flagshipCluster.setManagerComment("");
			flagshipCluster.setDirectorComment("");
			flagshipCluster.setClusterId(clusterId);
			flagshipCluster.setSpentByJune("0");

			Map<String, ClusterOutputDetail> clusterOutputMap = new TreeMap<>();

			List<FlagshipReportData> reportDataList = flagshipFocalRegionManager
					.getFlagshipReportData(year, flagshipId, cluster.getValue());
			for (FlagshipReportData reportData : reportDataList) {
				String activityId = reportData.getActivityId();
				String outputCode = reportData.getOutputCode();
				ClusterOutput clusterOutput = new ClusterOutput();

				ClusterOutputDetail clstDetail = clusterOutputMap
						.get(outputCode);
				if (clstDetail == null) {
					clstDetail = new ClusterOutputDetail();
					clstDetail.setCode(outputCode);
					clstDetail.setDescription(reportData.getOutput());
					clusterOutputMap.put(outputCode, clstDetail);
					clstDetail.setManagerComment("");
					clstDetail.setDirectorComment("");
					CommentDto comment = new CommentDto();
					comment.setCode(outputCode);
					comment.setComment("");
					comment.setManagerComment("");
					comment.setDirectorComment("");
					flagshipCluster.addOutputComment(comment);
				}

				ActivityClusterOutput actOpt = clstDetail.getActivityMap().get(
						activityId);
				if (actOpt == null) {
					actOpt = new ActivityClusterOutput();
					actOpt.setActivityId(activityId);
					actOpt.setActivityName(reportData.getProject());
					clstDetail.getActivityMap().put(activityId, actOpt);
				}

				clusterOutput.setProjectId(reportData.getDelCode());
				clusterOutput.setProjectName(reportData.getDeliverable());
				clusterOutput.setActivityId(activityId);
				clusterOutput.setActivityName(reportData.getProject());
				clusterOutput.setStatus("");
				clusterOutput.setProgress("");
				clusterOutput.setRating("-1");

				Activity activity = activityManager.getActivity(activityId);
				ActivityReviewRating midYearReview = activityManager
						.getMidYearRating(activityId);

				if (midYearReview != null) {
					List<Deliverable> deliverables = deliverableManager
							.getDeliverables(activity.getId());
					Map<String, Deliverable> deliverableMap = new HashMap<>();
					for (Deliverable deliverable : deliverables) {
						deliverableMap.put(deliverable.getDeliverableCode(),
								deliverable);
					}

					String delCode = reportData.getDelCode();
					Deliverable deliverable = deliverableMap.get(delCode);

					List<DeliverableReviewRating> deliverableRatings = midYearReview
							.getDeliverableRating();
					for (DeliverableReviewRating delRating : deliverableRatings) {
						if (null != delRating
								&& null != deliverable && delRating.getDeliverableId() == deliverable
										.getId()) {
							clusterOutput
									.setStatus(getOutputTypeDetails(delRating));
							clusterOutput.setProgress(delRating
									.getMidYearReviewProgress());
							clusterOutput.setRating(String.valueOf(delRating
									.getMidYearReviewRating()));
							
							
							if (!activityCacheMap.containsKey(activityId)) {

								double expenditure = flagshipCluster
										.getDraftExpenditure()
										+ midYearReview.getTotalSpent();
								flagshipCluster
										.setDraftExpenditure(expenditure);
								flagshipCluster.setSpentByJune(String.valueOf(expenditure).replace(".0", ""));
								activityCacheMap.put(activityId, true);
							}
							break;
						}
					}
				}

				actOpt.addClusterOutput(clusterOutput);
			}

			/*flagshipCluster.setActivityCount(activityManager
					.getActivityCount(cluster.getValue()));*/
			double budgetValue = 0;
			List<ActivityBudget> budgets = budgetManager
					.getActivityBudgetsByCriteria(year, -1, null,
							cluster.getValue(), null);
			for (ActivityBudget budget : budgets) {
				ActivityBudgetTotals totals = budget.sumAll();
				budgetValue = budgetValue + totals.getW1w2BudgetTotal()
						+ totals.getW3BudgetTotal()
						+ totals.getBilateralBudgetTotal()
						+ totals.getOtherBudgetTotal();
			}

			flagshipCluster.setBudget(budgetValue);
			flagshipCluster.setRating("-1");
			flagshipFocalRegionReport.addFlagshipCluster(flagshipCluster);
			flagshipCluster.setClusterOutputMap(clusterOutputMap);
		}

		return flagshipFocalRegionReport;
	}

	public boolean isAuthorizedToSave() {
		return isAuthorizedLeader()
				&& (flagshipReport.getState().equals(
						FlagshipFocalRegionReport.INITIAL_STATE) || flagshipReport
						.getState().equals(
								FlagshipFocalRegionReport.REVIEWED_STATE)) && reportAvaialble;
	}

	public boolean isAuthorizedToConfirm() {
		return isAuthorizedLeader()
				&& (flagshipReport.getState().equals(
						FlagshipFocalRegionReport.INITIAL_STATE) || flagshipReport
						.getState().equals(
								FlagshipFocalRegionReport.REVIEWED_STATE)) && reportAvaialble;
	}

	public boolean isSaveButtonEnabled() {
		
		return (isAuthorizedLeader() && (flagshipReport.getState().equals(
				FlagshipFocalRegionReport.INITIAL_STATE) || flagshipReport
				.getState().equals(FlagshipFocalRegionReport.REVIEWED_STATE)) && reportAvaialble)
				|| (flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE) && getCurrentUser()
						.isWLED() && flagshipReport.getDirectorReviewDone() == 0)
				|| (flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE) && getCurrentUser()
						.isWLME() && flagshipReport.getManagerReviewDone() == 0)
				|| ((flagshipReport.getState().equals(
						FlagshipFocalRegionReport.REVIEWED_STATE) && getCurrentUser()
						.isAdmin()));
	}

	private boolean isAuthorizedLeader() {
		int leaderAId = flagshipFocalRegion.getLeaderAId();
		boolean authorizedLeader = false;
		if (leaderAId > 0)
			authorizedLeader = userManager.getById(leaderAId).getId() == getCurrentUser()
					.getId();

		int leaderBId = flagshipFocalRegion.getLeaderBId();
		if (!authorizedLeader && leaderBId > 0)
			authorizedLeader = userManager.getById(leaderBId).getId() == getCurrentUser()
					.getId();
		return authorizedLeader;
	}

	public boolean isAuthorizedToApprove() {
		return getCurrentUser().isWLED()
				&& flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE);
	}

	public boolean isAuthorizedToCommentManager() {
		return getCurrentUser().isWLME()
				&& flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE)
				&& flagshipReport.getManagerReviewDone() == 0;
	}

	public boolean isAuthorizedToCommentDirector() {
		return getCurrentUser().isWLED()
				&& flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE)
				&& flagshipReport.getDirectorReviewDone() == 0;
	}

	public boolean isAuthorizedToView() {
		return isAuthorizedLeader()
				|| isAuthorizedToApprove()
				|| isAuthorizedToCommentManager()
				|| isAuthorizedToCommentDirector()
				|| flagshipReport.getState().equals(
						FlagshipFocalRegionReport.APPROVED_STATE)
				|| !(flagshipReport.getState().equals(
						FlagshipFocalRegionReport.INITIAL_STATE)  && (getCurrentUser()
						.isWLED() || getCurrentUser().isWLME()));
	}

	public boolean isAuthorizedToCancel() {
		return !flagshipReport.getState().equals(
				FlagshipFocalRegionReport.APPROVED_STATE);
	}

	public boolean isReviewComment() {
		return !flagshipReport.getState().equals(
				FlagshipFocalRegionReport.INITIAL_STATE)
				&& (isAuthorizedLeader() || getCurrentUser().isWLME() || getCurrentUser()
						.isWLED());
	}

	public boolean isAuthorizedToreviewSubmit() {
		return flagshipReport.getState().equals(
				FlagshipFocalRegionReport.PENDING_STATE)
				&& ((getCurrentUser().isWLME() && flagshipReport
						.getManagerReviewDone() == 0) || (getCurrentUser()
						.isWLED() && flagshipReport.getDirectorReviewDone() == 0));
	}
	
	public boolean isPrintable() {
		return flagshipReport.getId() > 0
				&& (isAuthorizedLeader() || getCurrentUser().isWLME() || getCurrentUser()
						.isWLED());
	}
	
	public boolean isFinalSubmit()
	{
		return flagshipReport.getState().equals(
				FlagshipFocalRegionReport.REVIEWED_STATE);
	}
	
	public boolean isAuthorizedToSaveByFinance() {
		return isAuthorizedToSave()
				|| (flagshipReport.getState().equals(
						FlagshipFocalRegionReport.PENDING_STATE)
						&& flagshipReport.getDirectorReviewDone() == 1
						&& flagshipReport.getManagerReviewDone() == 1 && getCurrentUser()
						.isAdmin());
	}

	/**
	 * @return the periodStart
	 */
	public String getPeriodStart() {
		return periodStart;
	}

	/**
	 * @return the periodEnd
	 */
	public String getPeriodEnd() {
		return periodEnd;
	}

	/**
	 * Getter for flagship focal region report.
	 * 
	 * @return
	 */
	public FlagshipFocalRegionReport getFlagshipReport() {
		return flagshipReport;
	}

	/**
	 * @return the flagshipFocalRegion
	 */
	public FlagshipFocalRegion getFlagshipFocalRegion() {
		return flagshipFocalRegion;
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	private String getOutputTypeDetails(DeliverableReviewRating delRating) {
		String outputType = "";
		String outputSubType = "";
		
		if (null != delRating.getOutputType()) {
			switch (delRating.getOutputType()) {

			case "tool":
				outputType = "<b>Tool</b><br/>";
				outputSubType = "Type of Tool: <i>" + delRating.getToolType()
						+ "</i>";
				break;
			case "policy progress":
				outputType = "<b>Policy Process</b><br/>";
				outputSubType = "Name of policy regulation or procedure : <i>"
						+ delRating.getNameOfPolicyProcedure()
						+ "</i>"
						+ "<br/><br/>Stage of development ? Has the policy been : <i>"
						+ delRating.getStageOfDevelopement() + "</i>";
				break;
			case "isi publication":
				outputType = "<b>Publication</b><br/>";
				outputSubType = "Is the publication :Draft? Published? : <i>"
						+ delRating.getPubType()
						+ "</i>"
						+ (delRating.getPubType().equals("Published") ? "<br/><br/>Weblink : <i>"
								+ delRating.getPubWebLink()
								+ "</i>"
								+ "<br/><br/>ublication Reference in Full : <i>"
								+ delRating.getPubReference()
								+ "</i>"
								+ "<br/><br/>Is it open access ? : <i>"
								+ delRating.getPubOpenAccess()
								+ "</i>"
								+ "<br/><br/>Published in ISI Journal ? : <i>"
								+ delRating.getPublishedInISI()
								+ "</i>"
								+ "<br/><br/>Has it been Peer Reviewed ? : <i>"
								+ delRating.getJournalPeerReviewed()
								+ "</i>"
								+ "Was this publication included in last year’s report ? : <i>"
								+ delRating.getWasThisInLastYearReport()
								+ "</i>"
								: "");

				break;
			case "platform":
				outputType = "<b>Multi-Stakeholder Platform</b><br/>";
				outputSubType = "Name of Stakeholder Platform? : <i>"
						+ delRating.getNameOfStakeholderPlatform() + "</i>";
				break;
			case "database":
				outputType = "<b>Database</b><br/>";
				outputSubType = "Is it Open Access ? : <i>"
						+ delRating.getDatabaseOpenAccess()
						+ "</i>"
						+ (delRating.getDatabaseOpenAccess().equals("yes") ? "<br/><br/>Link : <i>"
								+ delRating.getDatabaseLink() + "</i>"
								: "");
				break;
			case "dataset":
				outputType = "<b>Dataset</b><br/>";
				outputSubType = "Is it Open Access ? : <i>"
						+ delRating.getDataSetOpenAccess()
						+ "</i>"
						+ (delRating.getDataSetOpenAccess().equals("yes") ? "<br/><br/>Link : <i>"
								+ delRating.getDataSetLink() + "</i>"
								: "");
				break;
			case "capacity development":
				outputType = "<b>Capacity Development</b><br/>";
				outputSubType = "Was/Is the Capacity Development of : <i>"
						+ delRating.getTypeCapacityDev()
						+ "</i>"
						+ (delRating.getTypeCapacityDev().equals("Individuals") ? "<br/><br/>Training Method : <i>"
								+ delRating.getTrainingMethod() + "</i>"
								: "<br/><br/>Training Method : <i>"
										+ delRating.getTrainingMethod()
										+ "</i>"
										+ "<br/><br/>Has the training already taken place/been completed ? : <i>"
										+ delRating.getTrainingCompleted()
										+ "</i>");
				break;
			case "technology":
				outputType = "<b>Technology/Practice</b><br/>";
				outputSubType = "Type of Technology/Practice ? : <i>"
						+ delRating.getTechType()
						+ "</i>"
						+ (delRating.getTechType().equals("Other") ? "Description : <i>"
								+ delRating.getTechOther() + "</i>"
								: "")
						+ "<br/><br/>Stage of development ? : <i>"
						+ delRating.getStageOfDevelopment() + "</i>";
				break;
			case "communications":
				outputType = "<b>Communications</b><br/>"; // TODO
				outputSubType = "Type of Communications product? : <i>"
						+ delRating.getTypeOfCommunication()
						+ "</i>"
						+ (delRating.getTypeOfCommunication().equals("Other") ? "Description : <i>"
								+ delRating.getTypeOfOutreachedActivity()
								+ "</i>"
								: "");
				break;
			case "other":
				outputType = "<b>Other</b><br/>";
				outputSubType = "Description : <i>" + delRating.getOtherDesc()
						+ "</i>";
				break;
			default:
				outputType = "Not defined";
				outputSubType = "";
				break;

			}
		}
		

		return outputType + outputSubType;
	}
}