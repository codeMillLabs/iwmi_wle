package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

import java.util.Map;
import java.util.TreeMap;

public class ClusterDto {
	
	private int projectGreen;
	private int projectYellow;
	private int projectRed;
	private int outputGreen;
	private int outputYellow;
	private int outputRed;
	private double spent;
	private double notSpent;
	
	private Map<String, ProjectDto> projects = new TreeMap<>();
	
	public int getProjectGreen() {
		return projectGreen;
	}
	
	public void setProjectGreen(int projectGreen) {
		this.projectGreen = projectGreen;
	}
	
	public void incrementProjectGreen() {
		this.projectGreen++;
	}
	
	public int getProjectYellow() {
		return projectYellow;
	}
	
	public void setProjectYellow(int projectYellow) {
		this.projectYellow = projectYellow;
	}
	
	public void incrementProjectYellow()
	{
		this.projectYellow ++;
	}
	
	public int getProjectRed() {
		return projectRed;
	}
	
	public void setProjectRed(int projectRed) {
		this.projectRed = projectRed;
	}
	
	public void incrementProjectRed()
	{
		this.projectRed ++;
	}
	
	public int getOutputGreen() {
		return outputGreen;
	}
	
	public void setOutputGreen(int outputGreen) {
		this.outputGreen = outputGreen;
	}
	
	public void incrementOutputGreen()
	{
		this.outputGreen ++;
	}
	
	public int getOutputYellow() {
		return outputYellow;
	}
		
	public void setOutputYellow(int outputYellow) {
		this.outputYellow = outputYellow;
	}
	
	public void incrementOutputYellow()
	{
		this.outputYellow ++;
	}
	
	public int getOutputRed() {
		return outputRed;
	}
	
	public void setOutputRed(int outputRed) {
		this.outputRed = outputRed;
	}
	
	public void incrementOutputRed()
	{
		this.outputRed ++;
	}
	
	public double getSpent() {
		return spent;
	}
	
	public void setSpent(double spent) {
		this.spent = spent;
	}
	
	public void addSpent(double spent)
	{
		this.spent = this.spent + spent;
	}
	
	public double getNotSpent() {
		return notSpent;
	}
	
	public void setNotSpent(double notSpent) {
		this.notSpent = notSpent;
	}
	
	public void addNotSpent(double notSpent)
	{
		this.notSpent = this.notSpent + notSpent;
	}

	public Map<String, ProjectDto> getProjects() {
		return projects;
	}

	public void setProjects(Map<String, ProjectDto> projects) {
		this.projects = projects;
	}
}