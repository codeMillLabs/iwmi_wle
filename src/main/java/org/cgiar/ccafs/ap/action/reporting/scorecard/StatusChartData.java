package org.cgiar.ccafs.ap.action.reporting.scorecard;

public class StatusChartData {

	private double green;
	private double yellow;
	private double red;
	
	public StatusChartData(double green, double yellow, double red)
	{
		this.green = green;
		this.yellow = yellow;
		this.red = red;
	}
	
	/**
	 * @return the green
	 */
	public double getGreen() {
		return green;
	}

	/**
	 * @return the yellow
	 */
	public double getYellow() {
		return yellow;
	}

	/**
	 * @return the red
	 */
	public double getRed() {
		return red;
	}
}