package org.cgiar.ccafs.ap.action.reporting.scorecard;

public class SpentChartData {

	private double spent;
	private double unspent;
	
	public SpentChartData(double spent, double unspent)
	{
		this.spent = spent;
		this.unspent = unspent;
	}

	/**
	 * @return the spent
	 */
	public double getSpent() {
		return spent;
	}

	/**
	 * @return the unspent
	 */
	public double getUnspent() {
		return unspent;
	}
}