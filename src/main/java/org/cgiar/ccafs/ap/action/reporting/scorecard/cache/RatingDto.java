package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class RatingDto {
	
	private int green;
	private int yellow;
	private int red;
	private String name;
	private String comment;
	
	public int getGreen() {
		return green;
	}
	
	public void setGreen(int green) {
		this.green = green;
	}
	
	public void incrementGreen()
	{
		this.green ++;
	}
	
	public int getYellow() {
		return yellow;
	}
	
	public void setYellow(int yellow) {
		this.yellow = yellow;
	}
	
	public void incrementYellow()
	{
		this.yellow ++;
	}
	
	public int getRed() {
		return red;
	}
	
	public void setRed(int red) {
		this.red = red;
	}
	
	public void incrementRed()
	{
		this.red ++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}