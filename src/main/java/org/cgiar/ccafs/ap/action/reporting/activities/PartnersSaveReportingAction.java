/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.reporting.activities;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.CountryManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.PartnerManager;
import org.cgiar.ccafs.ap.data.manager.PartnerTypeManager;
import org.cgiar.ccafs.ap.data.manager.TokenActionManager;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.Country;
import org.cgiar.ccafs.ap.data.model.Partner;
import org.cgiar.ccafs.ap.data.model.PartnerType;
import org.cgiar.ccafs.ap.data.model.RowStatus;
import org.cgiar.ccafs.ap.data.model.TokenAction;
import org.cgiar.ccafs.ap.util.SendMail;
import org.cgiar.ccafs.ap.util.SystemTokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class PartnersSaveReportingAction extends BaseAction {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(PartnersSaveReportingAction.class);
	private static final long serialVersionUID = -5598244190394106332L;

	// Managers
	private CountryManager countryManager;
	private PartnerTypeManager partnerTypeManager;
	private PartnerManager partnerManager;
	private TokenActionManager tokenActionManager;

	// Model
	private Country[] countriesList;
	private PartnerType[] partnerTypesList;
	private ActivityPartner activityPartner;

	private String partnerWebPage;
	private int activityID;

	@Inject
	public PartnersSaveReportingAction(APConfig config,
			LogframeManager logframeManager, CountryManager countryManager,
			PartnerTypeManager partnerTypeManager,
			PartnerManager partnerManager, TokenActionManager tokenActionManager) {
		super(config, logframeManager);
		this.countryManager = countryManager;
		this.partnerTypeManager = partnerTypeManager;
		this.partnerManager = partnerManager;
		this.tokenActionManager = tokenActionManager;
	}

	public int getActivityID() {
		return activityID;
	}

	public ActivityPartner getActivityPartner() {
		return activityPartner;
	}

	public Country[] getCountriesList() {
		return countriesList;
	}

	public PartnerType[] getPartnerTypesList() {
		return partnerTypesList;
	}

	public String getPartnerWebPage() {
		return partnerWebPage;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		// Take the activity id only the first time the page loads

		if (this.getRequest().getParameter(APConstants.ACTIVITY_REQUEST_ID) != null) {
			activityID = Integer.parseInt(StringUtils.trim(this.getRequest()
					.getParameter(APConstants.ACTIVITY_REQUEST_ID)));
		}
		LOG.info(
				"The user {} load the request partner section related to the activity {}.",
				getCurrentUser().getEmail(), activityID);
		this.countriesList = countryManager.getCountryList();
		this.partnerTypesList = partnerTypeManager.getPartnerTypeList();
	}

	@Override
	public String save() {
		String partnerName, partnerAcronym, partnerTypeName, countryId, countryName;
		String createdBy;

		int partnerTypeId;

		// Take the values to create the message
		partnerName = activityPartner.getPartner().getName();
		partnerAcronym = activityPartner.getPartner().getAcronym();
		partnerTypeId = activityPartner.getPartner().getType().getId();
		countryId = activityPartner.getPartner().getCountry().getId();
		createdBy = activityPartner.getPartner().getCreatedBy();

		// Get the country name
		countryName = countryManager.getCountry(countryId).getName();

		// Get the partner type name
		partnerTypeName = "";
		for (PartnerType pt : partnerTypesList) {
			if (pt.getId() == partnerTypeId) {
				partnerTypeName = pt.getName();
			}
		}

		Partner partner = new Partner();
		partner.setName(partnerName);
		partner.setAcronym(partnerAcronym);
		partner.setType(activityPartner.getPartner().getType());
		partner.setCountry(activityPartner.getPartner().getCountry());
		partner.setWebsite(activityPartner.getPartner().getWebsite());
		partner.setCreatedBy(activityPartner.getPartner().getCreatedBy());
		partner.setStatus(RowStatus.VERIFIED);

		int id = partnerManager.savePartner(partner);

		sendNewPartnerEmail(id, partnerName, partnerAcronym, partnerTypeName,
				countryName, createdBy);
		LOG.info(
				"The user {} send a message requesting add partners to the activity {}",
				getCurrentUser().getEmail(), activityID);
		return SUCCESS;
	}

	private void sendNewPartnerEmail(int partnertId, String partnerName,
			String partnerAcronym, String partnerTypeName, String countryName,
			String createdBy) {

		List<String> placeHolderValues = new ArrayList<String>();

		StringBuilder partnerDetails = new StringBuilder();
		partnerDetails.append("Name :" + partnerName + "\n\t\t\t");
		partnerDetails.append("Acronym :" + partnerAcronym + "\n\t\t\t");
		partnerDetails.append("Type :" + partnerTypeName + "\n\t\t\t");
		partnerDetails.append("Country :" + countryName + "\n\t\t\t");
		partnerDetails.append("Created By :" + createdBy + "\n\t\t\t");

		String token = SystemTokenGenerator.getToken();

		placeHolderValues.add(config.getPartnerVerifyOfficerName());
		placeHolderValues.add(partnerDetails.toString());
		placeHolderValues.add(config.getSystemTokenPath() + token);

		String subject = getText("reporting.PartnersSave.verify.email.subject",
				new String[] { partnerName });
		String message = getText("reporting.PartnersSave.verify.email",
				placeHolderValues);

		String action = "action=partner;id=" + partnertId;
		// Save token
		TokenAction tokenAction = new TokenAction();
		tokenAction.setTokenId(token);
		tokenAction.setAction(action);
		tokenAction.setUsed(false);
		tokenActionManager.saveToken(tokenAction);

		SendMail sendMail = new SendMail(this.config);
		sendMail.send(config.getPartnerVerifyToEmailAddress(), subject, message);
		LOG.info(
				"The user {} send a message requesting add partners to the activity {}",
				getCurrentUser().getEmail(), activityID);
	}

	public void setActivityID(int activityID) {
		this.activityID = activityID;
	}

	public void setActivityPartner(ActivityPartner activityPartner) {
		this.activityPartner = activityPartner;
	}

	public void setPartnerWebPage(String partnerWebPage) {
		this.partnerWebPage = partnerWebPage;
	}

	@Override
	public void validate() {
		boolean anyError = false;

		// If the page is loading don't validate
		if (getRequest().getMethod().equalsIgnoreCase("post")) {

			// Check the partner name
			if (activityPartner.getPartner().getName().isEmpty()) {
				addFieldError("activityPartner.partner.name",
						getText("validation.field.required"));
				anyError = true;
			}

			if (anyError) {
				addActionError(getText("saving.fields.required"));
			}
		}
		super.validate();
	}
}