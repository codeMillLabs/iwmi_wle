package org.cgiar.ccafs.ap.action.reporting.scorecard;

public class FlagshipSummaryCard {

	private int flagshipId;
	private String shortName;
	private StatusChartData status;
	private SpentChartData expenditure;
	private boolean available;
	private String comment;

	public FlagshipSummaryCard(int flagshipId, String shortName,
			StatusChartData status, SpentChartData expenditure,
			boolean available, String comment) {
		this.flagshipId = flagshipId;
		this.shortName = shortName;
		this.status = status;
		this.expenditure = expenditure;
		this.available = available;
		this.comment = comment;
	}

	public int getFlagshipId() {
		return flagshipId;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return the status
	 */
	public StatusChartData getStatus() {
		return status;
	}

	/**
	 * @return the expenditure
	 */
	public SpentChartData getExpenditure() {
		return expenditure;
	}

	/**
	 * @return the available
	 */
	public boolean isAvailable() {
		return available;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
}
