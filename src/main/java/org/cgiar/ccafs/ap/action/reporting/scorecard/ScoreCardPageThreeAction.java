package org.cgiar.ccafs.ap.action.reporting.scorecard;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageOutput;
import org.cgiar.ccafs.ap.action.reporting.scorecard.cache.ScoreCardThirdPageProject;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;

import com.google.inject.Inject;

public class ScoreCardPageThreeAction extends BaseAction {

	private static final long serialVersionUID = 7440586646145402906L;

	private FlagshipFocalRegionManager flagshipFocalRegionManager;

	private int reportId;
	private String activityId;
	private String projectTitle;
	private String flagshipShortName;
	private int flagshipId;
	List<OutputSummaryCard> outputSummaries;

	@Inject
	public ScoreCardPageThreeAction(APConfig config,
			LogframeManager logframeManager,
			FlagshipFocalRegionManager flagshipFocalRegionManager) {
		super(config, logframeManager);
		this.flagshipFocalRegionManager = flagshipFocalRegionManager;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		activityId = StringUtils.trim(this.getRequest().getParameter(
				"activityId"));
		outputSummaries = new ArrayList<OutputSummaryCard>();
		
		ScoreCardThirdPageProject scoreCardThirdPageProject = flagshipFocalRegionManager.getScoreCardThirdPageProject(activityId);
		List<ScoreCardThirdPageOutput> scoreCardThirdPageOutputs = flagshipFocalRegionManager.getScoreCardThirdPageOutputs(activityId);
		projectTitle = scoreCardThirdPageProject.getTitle();
		flagshipShortName = scoreCardThirdPageProject.getFlagshipShortName();
		flagshipId = scoreCardThirdPageProject.getFlagshipId();
		for(ScoreCardThirdPageOutput scoreCardThirdPageOutput : scoreCardThirdPageOutputs)
		{
			OutputSummaryCard outputSummaryCard = new OutputSummaryCard();
			StatusChartData status = new StatusChartData(scoreCardThirdPageOutput.getOutputGreen(), scoreCardThirdPageOutput.getOutputYellow(), scoreCardThirdPageOutput.getOutputRed());;
			outputSummaryCard.setStatus(status);
			outputSummaryCard.setComment(scoreCardThirdPageOutput.getComment());
			outputSummaryCard.setOutputName(scoreCardThirdPageOutput.getName());
			outputSummaries.add(outputSummaryCard);
		}
	}

	/**
	 * @return the reportId
	 */
	public int getReportId() {
		return reportId;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}
	
	/**
	 * @return the projectTitle
	 */
	public String getProjectTitle() {
		return projectTitle;
	}

	/**
	 * @return the outputSummaries
	 */
	public List<OutputSummaryCard> getOutputSummaries() {
		return outputSummaries;
	}

	/**
	 * @return the flagshipShortName
	 */
	public String getFlagshipShortName() {
		return flagshipShortName;
	}

	public int getFlagshipId() {
		return flagshipId;
	}

	public void setFlagshipId(int flagshipId) {
		this.flagshipId = flagshipId;
	}
}