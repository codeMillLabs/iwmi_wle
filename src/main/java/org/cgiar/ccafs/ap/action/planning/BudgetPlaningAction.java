/**
 * 
 */
package org.cgiar.ccafs.ap.action.planning;

import static org.cgiar.ccafs.ap.util.WleAppUtil.sendContentChangedNotification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityCountryManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityRegionManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.RegionManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.DiffReport;
import org.cgiar.ccafs.ap.data.model.MainBudget;
import org.cgiar.ccafs.ap.data.model.Region;
import org.cgiar.ccafs.ap.data.model.RegionalBudget;
import org.cgiar.ccafs.ap.data.model.RegionalBudgetDetails;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * Action class for budget planing
 * 
 * @author Amila Silva
 * @email amilsilva88@gmail.com
 * 
 */
public class BudgetPlaningAction extends BaseAction
{

    private static final long serialVersionUID = -2543929376002715673L;

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(BudgetPlaningAction.class);

    // Managers
    private ActivityManager activityManager;
    private ActivityBudgetManager activityBudgetManager;
    private SubmissionManager submissionManager;
    private StringBuilder validationMessage;
    private ActivityRegionManager activityRegionManager;
    private ActivityCountryManager activityCountryManager;
    private List<Region> activeRegions;
    private Region[] regions;
    private RegionManager regionManager;

    // Model
    private int activityID;
    private Activity activity;
    private boolean canSubmit;

    @Inject
    public BudgetPlaningAction(APConfig config, LogframeManager logframeManager, SubmissionManager submissionManager,
        ActivityManager activityManager, ActivityBudgetManager activityBudgetManager,
        ActivityRegionManager activityRegionManager, RegionManager regionManager,
        ActivityCountryManager activityCountryManager)
    {
        super(config, logframeManager);
        this.submissionManager = submissionManager;
        this.activityManager = activityManager;
        this.activityBudgetManager = activityBudgetManager;
        this.activityRegionManager = activityRegionManager;
        this.regionManager = regionManager;
        this.activityCountryManager = activityCountryManager;
    }

    @Override
    public String next()
    {
        save();
        return super.next();
    }

    @Override
    public void prepare() throws Exception
    {
        super.prepare();

        validationMessage = new StringBuilder();

        LOG.info("-- prepare() > User {} load the budget planning for leader {} in planing section", getCurrentUser()
            .getEmail(), getCurrentUser().getLeader().getId());

        String activityStringID = StringUtils.trim(this.getRequest().getParameter(APConstants.ACTIVITY_REQUEST_ID));
        try
        {
            activityID = Integer.parseInt(activityStringID);
        }
        catch (NumberFormatException e)
        {
            LOG.error("-- prepare() > There was an error getting the activity identifier '{}' from URL.",
                activityStringID, e);
        }

        // Get the basic information about the activity
        activity = activityManager.getActivity(activityID);
        activity.setLoggedInUser(getCurrentUser());

        // Set activity countries
        activity.setCountries(activityCountryManager.getActvitiyCountries(activityID));

        activity.setRegions(activityRegionManager.getActvitiyRegions(activityID));

        ActivityBudget activityBudget = activityBudgetManager.getBudget(activityID);
        activityBudget.setActivityId(activityID);
        // activityBudget.getBudgetByRegion().setActivityId(activityID);
        activity.setBudget(activityBudget);

        regions = regionManager.getRegionList();

        // A region is active if the region is selected or if there are
        // selected countries in that region.
        activeRegions = new ArrayList<>();
        for (Region region : regions)
        {
            if (activity.getRegionsIds().contains(String.valueOf(region.getId()))
                || (activity.getCountriesIdsByRegion(region.getId()).size() > 0))
            {
                activeRegions.add(region);
            }
        }
        Collections.sort(activeRegions);

        if (activeRegions.isEmpty())
        {
            Region region = new Region();
            region.setName("Global");
            activeRegions.add(region);
        }

        populateBudgetWithRegions(activeRegions, activityBudget);

        // If the workplan was submitted before the user can't save new
        // information
        Submission submission =
            submissionManager.getSubmission(getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
                APConstants.PLANNING_SECTION);
        canSubmit = (submission == null) ? true : false;
    }

    private Map<String, MainBudget> getMainBudgetMap(ActivityBudget activityBudget)
    {
        Map<String, MainBudget> mainBudgetMap = new HashMap<>();
        for (MainBudget budget : activityBudget.getMainBudgets())
        {
            mainBudgetMap.put(budget.getRegionName(), budget);
        }
        return mainBudgetMap;
    }

    private Map<String, RegionalBudget> getRegionalBudgetMap(ActivityBudget activityBudget)
    {
        Map<String, RegionalBudget> regionalBudgetMap = new HashMap<>();
        for (RegionalBudget budget : activityBudget.getRegBudgets())
        {
            regionalBudgetMap.put(budget.getRegionName(), budget);
        }
        return regionalBudgetMap;
    }

    private ActivityBudget populateBudgetWithRegions(List<Region> regions, ActivityBudget activityBudget)
    {

        Map<String, MainBudget> mainBudgetMap = getMainBudgetMap(activityBudget);
        Map<String, RegionalBudget> regionalBudgetMap = getRegionalBudgetMap(activityBudget);

        int count = 1;
        for (Region region : regions)
        {
            MainBudget mainBudget = mainBudgetMap.get(region.getName());
            if (mainBudget == null)
            {
                MainBudget main = new MainBudget();
                main.setId("" + count);
                main.setRegionId("" + region.getId());
                main.setRegionName(region.getName());
                mainBudgetMap.put(region.getName(), main);
            }

            RegionalBudget regionBudget = regionalBudgetMap.get(region.getName());
            if (regionBudget == null)
            {
                RegionalBudget reginal = new RegionalBudget();
                reginal.setId("" + count);
                reginal.setRegionId("" + region.getId());
                reginal.setRegionName(region.getName());
                regionalBudgetMap.put(region.getName(), reginal);
            }
            count++;
        }

        List<MainBudget> mainBudgetList = new ArrayList<MainBudget>(mainBudgetMap.values());
        Collections.sort(mainBudgetList);
        activityBudget.setMainBudgets(mainBudgetList);

        List<RegionalBudget> regionBudgetList = new ArrayList<RegionalBudget>(regionalBudgetMap.values());
        Collections.sort(regionBudgetList);
        activityBudget.setRegBudgets(regionBudgetList);

        return activityBudget;
    }

    @Override
    public String save()
    {
        LOG.info("-- Save() > Budget planning, Activity : {} ", activityID);
        boolean valueSaved = false;
        ActivityBudget activityBudget = activity.getBudget();
        
        ActivityBudget prev = activityBudgetManager.getBudget(activityID);
        DiffReport diffReport = diffCheck(prev, activityBudget);
        sendContentChangedNotification(config, diffReport, activity.getFormattedId(),  getCurrentUser());

        if (activityBudget.getId() > 0)
        {
            activityBudgetManager.updateBudget(activityBudget);
            valueSaved = true;
        }
        else
        {
            int id = activityBudgetManager.saveBudget(activityBudget);
            if (id > 0)
                valueSaved = true;
        }

        if (valueSaved)
        {
            if (validationMessage.toString().isEmpty())
            {
                addActionMessage(getText("saving.success", new String[] {
                    getText("planning.budget")
                }));
            }
            else
            {
                String finalMessage = getText("saving.success", new String[] {
                    getText("planning.budget")
                });
                finalMessage += getText("saving.keepInMind", new String[] {
                    validationMessage.toString()
                });
                addActionWarning(Capitalize.capitalizeString(finalMessage));
            }

            return SUCCESS;
        }
        else
        {
            addActionError(getText("saving.problem"));
            return INPUT;
        }
    }
    
    private DiffReport diffCheck(ActivityBudget prev, ActivityBudget current) {
    	return prev.diff(current);
	}

    @Override
    public void validate()
    {
        if (save)
        {
            if (activity.getBudget() != null && !activity.getBudget().getRegBudgets().isEmpty())
            {
                for (RegionalBudget regionalBudget : activity.getBudget().getRegBudgets())
                {
                    for (RegionalBudgetDetails detail : regionalBudget.getDetails())
                    {
                        if (!detail.validTotal())
                        {
                            validationMessage.append(getText("planning.budget.validation.total", new String[] {
                                regionalBudget.getRegionName(), detail.getSourceType()
                            }));
                        }
                    }
                }
            }
        }
    }

    public void setActivityID(int activityID)
    {
        this.activityID = activityID;
    }

    public void setActivity(Activity activity)
    {
        this.activity = activity;
    }

    public int getActivityID()
    {
        return activityID;
    }

    public Activity getActivity()
    {
        return activity;
    }

    public List<Region> getActiveRegions()
    {
        return activeRegions;
    }

    public String getActivityRequestParameter()
    {
        return APConstants.ACTIVITY_REQUEST_ID;
    }

    public boolean isCanSubmit()
    {
        return canSubmit;
    }

}
