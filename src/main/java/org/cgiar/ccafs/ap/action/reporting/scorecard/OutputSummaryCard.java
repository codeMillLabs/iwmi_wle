package org.cgiar.ccafs.ap.action.reporting.scorecard;

public class OutputSummaryCard {
	
	private StatusChartData status;
	
	private String outputName;
	private String outputCode;
	
	private String comment;
	
	/**
	 * @return the status
	 */
	public StatusChartData getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(StatusChartData status) {
		this.status = status;
	}
	
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the outputName
	 */
	public String getOutputName() {
		return outputName;
	}

	/**
	 * @param outputName the outputName to set
	 */
	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	/**
	 * @return the outputCode
	 */
	public String getOutputCode() {
		return outputCode;
	}

	/**
	 * @param outputCode the outputCode to set
	 */
	public void setOutputCode(String outputCode) {
		this.outputCode = outputCode;
	}
}