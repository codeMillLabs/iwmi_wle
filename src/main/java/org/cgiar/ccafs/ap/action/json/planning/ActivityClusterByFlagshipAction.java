/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cgiar.ccafs.ap.action.json.planning;

import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.FLAGSHIP_SRP;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public class ActivityClusterByFlagshipAction extends BaseAction
{

    private static final long serialVersionUID = 1L;

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(ActivityClusterByFlagshipAction.class);

    // Model
    private String flagshipValue;
    private DataLookup[] activityClusters;
    private List<DataLookup> flagships = new ArrayList<DataLookup>();

    // Managers
    private DataLookUpManager dataLookUpManager;

    @Inject
    public ActivityClusterByFlagshipAction(APConfig config, LogframeManager logframeManager,
        DataLookUpManager dataLookUpManager)
    {
        super(config, logframeManager);
        this.dataLookUpManager = dataLookUpManager;

        flagships = dataLookUpManager.getAllLookUpdata(FLAGSHIP_SRP);
    }

    @Override
    public String execute() throws Exception
    {

        for (DataLookup flagship : flagships)
        {

            if (flagship.getValue().equalsIgnoreCase(flagshipValue) && !flagship.getSubLevel().isEmpty())
            {
                activityClusters = new DataLookup[flagship.getSubLevel().size()];
                for (int i = 0; i < flagship.getSubLevel().size(); i++)
                {
                    activityClusters[i] = flagship.getSubLevel().get(i);
                }
            }
        }
        LOG.info("-- execute() > The list of  Acivity clusters  '{}' was loaded.", activityClusters.length);
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception
    {

        // Verify if there is a activityID parameter
        // If there is a flagshipValue take its values
        flagshipValue = StringUtils.trim(this.getRequest().getParameter(APConstants.FLAGSHIP_NAME));
        flagshipValue = (flagshipValue == null) ? "" : flagshipValue;

    }

    public String getFlagshipValue()
    {
        return flagshipValue;
    }

    public void setFlagshipValue(String flagshipValue)
    {
        this.flagshipValue = flagshipValue;
    }

    public DataLookup[] getActivityClusters()
    {
        return activityClusters;
    }

    public void setActivityClusters(DataLookup[] activityClusters)
    {
        this.activityClusters = activityClusters;
    }

}
