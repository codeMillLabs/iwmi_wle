/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.reporting.activities;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.Deliverable;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class ActivitiesReportingAction extends BaseAction {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivitiesReportingAction.class);
	private static final long serialVersionUID = 9001775749549472317L;

	// Managers
	protected ActivityManager activityManager;
	private SubmissionManager submissionManager;
	private DataLookUpManager dataLookupManager;

	// Model
	private Activity[] currentActivities;
	private String[] activityStatuses;
	private boolean canSubmit;

	@Inject
	public ActivitiesReportingAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			SubmissionManager submissionManager,
			DataLookUpManager dataLookupManager) {
		super(config, logframeManager);
		this.activityManager = activityManager;
		this.submissionManager = submissionManager;
		this.dataLookupManager = dataLookupManager;
	}

	/**
	 * This method validates if a specific activity is completely filled or not.
	 * It validates at first step if the activity status and the description has
	 * some content; at second step if the activity has at least one deliverable
	 * without be reported; and finally it validates if the activity has at
	 * least one partner associated without all the contact information filled.
	 */
	private String calculateStatus(Activity activity) {
		String problemDescription = "";

		/* Activity Status */
		boolean problem = false;
		if (activity.getStatusDescription() == null
				|| activity.getStatusDescription().isEmpty()) {
			problemDescription += getText("reporting.activityList.missingStatus");
		}

		/* Deliverables */
		List<Deliverable> deliverables = activity.getDeliverables();
		if (activity.getDeliverables() != null) {
			Deliverable deliverable;
			for (int c = 0; !problem && c < deliverables.size(); c++) {
				deliverable = deliverables.get(c);
				if (deliverable.getType().getId() == 1
						|| deliverable.getType().getId() == 4) {
					if (deliverable.getFileFormats().size() == 0) {
						problem = true;
						problemDescription += getText("reporting.activityList.missingDeliverable");
					}
				}
			}
		}

		/* Partners */
		problem = false;
		List<ActivityPartner> activityPartners = activity.getActivityPartners();
		if (activityPartners != null) {
			ActivityPartner activityPartner;
			for (int c = 0; !problem && c < activityPartners.size(); c++) {
				activityPartner = activityPartners.get(c);
				if (activityPartner.getContactName() == null
						|| activityPartner.getContactName().isEmpty()
						|| activityPartner.getContactEmail() == null
						|| activityPartner.getContactEmail().isEmpty()) {
					problem = true;
					problemDescription += getText("reporting.activityList.missingPartner");
				}
			}
		}
		return problemDescription.isEmpty() ? null : problemDescription;
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public String[] getActivityStatuses() {
		return activityStatuses;
	}

	public Activity[] getCurrentActivities() {
		return currentActivities;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	ActivityStatus[] activityStatus = new ActivityStatus[] {
			ActivityStatus.NEW, ActivityStatus.IN_PROGRESS,
			ActivityStatus.VALIDATED, ActivityStatus.INTERNAL_REVIEWING,
			ActivityStatus.INTERNAL_CORRECTIONS,
			ActivityStatus.SUBMITTED_TO_CA, ActivityStatus.REVIEWING,
			ActivityStatus.REVIEWS_COMPLETED, ActivityStatus.SUBMITTED,
			ActivityStatus.COMPLETED };

	@Override
	public void prepare() throws Exception {
		super.prepare();
		List<Activity> activityList = new ArrayList<Activity>();
		Activity[] activities = new Activity[0];
		
       boolean hasRole = false;
		
		if (getCurrentUser().hasRole(UserRole.FL)) {
			String flagshipValue = dataLookupManager
					.getFlagshipByUser(getCurrentUser().getEmail());
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					null, flagshipValue, activityStatus);
			hasRole = true;
			activityList.addAll(asList(activities));
		} 
			
	    if (getCurrentUser().hasRole(UserRole.PL)) {
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					this.getCurrentUser().getEmail(), null, activityStatus);
			hasRole = true;
			activityList.addAll(asList(activities));
		} 
	    
	    if(!hasRole) {
			activities = activityManager.getActivities(
					config.getReportingCurrentYear(), this.getCurrentUser(),
					this.getCurrentUser().getEmail(), null, activityStatus);
			activityList.addAll(asList(activities));
		}

	    Map<String, Activity> uniqueActivityMap = new HashMap<String, Activity>();
	    
	    for (Activity activity : activityList) {
			if (activity.getLeader().getId() == getCurrentUser().getLeader().getId()) {
				activity.setLoggedInUser(getCurrentUser());
			}
			
			if (uniqueActivityMap.get(activity.getActivityId()) == null) {
				uniqueActivityMap.put(activity.getActivityId(), activity);
			}
		}
	    
		currentActivities = (Activity[]) uniqueActivityMap.values().toArray();

		LOG.info("User {} charge the activity list for the year {}", this
				.getCurrentUser().getEmail(), String.valueOf(config
				.getReportingCurrentYear()));
		activityStatuses = new String[currentActivities.length];
		// Calculate the status of each activity.
		for (int c = 0; c < currentActivities.length; c++) {
			activityStatuses[c] = calculateStatus(currentActivities[c]);
		}

		/* --------- Checking if the user can submit ------------- */
		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentReportingLogframe(),
				APConstants.REPORTING_SECTION);
		canSubmit = (submission == null) ? true : false;
	}

	public void setCurrentActivities(Activity[] currentActivities) {
		this.currentActivities = currentActivities;
	}

}