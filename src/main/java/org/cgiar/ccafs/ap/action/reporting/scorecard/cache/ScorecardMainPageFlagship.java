package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class ScorecardMainPageFlagship {

	private int id;
	private int green;
	private int yellow;
	private int red;
	private String name;
	private String longName;
	private String toolTip;
	private double spent;
	private double notSpent;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the green
	 */
	public int getGreen() {
		return green;
	}
	
	/**
	 * @param green the green to set
	 */
	public void setGreen(int green) {
		this.green = green;
	}
	
	/**
	 * @return the yellow
	 */
	public int getYellow() {
		return yellow;
	}
	
	/**
	 * @param yellow the yellow to set
	 */
	public void setYellow(int yellow) {
		this.yellow = yellow;
	}
	
	/**
	 * @return the red
	 */
	public int getRed() {
		return red;
	}
	
	/**
	 * @param red the red to set
	 */
	public void setRed(int red) {
		this.red = red;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the longName
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * @param longName the longName to set
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}

	/**
	 * @return the toolTip
	 */
	public String getToolTip() {
		return toolTip;
	}
	
	/**
	 * @param toolTip the toolTip to set
	 */
	public void setToolTip(String toolTip) {
		this.toolTip = toolTip;
	}
	
	/**
	 * @return the spent
	 */
	public double getSpent() {
		return spent;
	}
	
	/**
	 * @param spent the spent to set
	 */
	public void setSpent(double spent) {
		this.spent = spent;
	}
	
	/**
	 * @return the notSpent
	 */
	public double getNotSpent() {
		return notSpent;
	}
	
	/**
	 * @param notSpent the notSpent to set
	 */
	public void setNotSpent(double notSpent) {
		this.notSpent = notSpent;
	}
}