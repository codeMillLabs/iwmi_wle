/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.planning;

import static java.lang.Integer.valueOf;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.EXTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.INTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.INTERNAL_CORRECTIONS;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.INTERNAL_REVIEWING;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.REVIEWING;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.REVIEWS_COMPLETED;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.REVISION;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.SUBMITTED_TO_CA;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.VALIDATED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityBenchmarkSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityCountryManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.manager.ActivityOtherSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityPartnerManager;
import org.cgiar.ccafs.ap.data.manager.ActivityRegionManager;
import org.cgiar.ccafs.ap.data.manager.ContactPersonManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.DeliverableManager;
import org.cgiar.ccafs.ap.data.manager.LeaderManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityReview;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.util.ActivityValidator;
import org.cgiar.ccafs.ap.util.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class ActivitiesPlanningAction extends BaseAction
{

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(ActivitiesPlanningAction.class);
    private static final long serialVersionUID = 610099339078512575L;

    // Managers
    private ActivityManager activityManager;
    private LeaderManager leaderManager;
    private ContactPersonManager contactPersonManager;
    private DeliverableManager deliverableManager;
    private ActivityPartnerManager activityPartnerManager;
    private ActivityObjectiveManager activityObjectiveManager;
    private ActivityCountryManager activityCountryManager;
    private ActivityRegionManager activityRegionManager;
    private ActivityBenchmarkSiteManager activityBenchmarkSiteManager;
    private ActivityOtherSiteManager activityOtherSiteManager;
    private SubmissionManager submissionManager;
    private DataLookUpManager dataLookUpManager;
    private UserManager userManager;

    private ActivityReviewerDAO reviewerDao;

    // Model
    private Submission submission;
    private Activity activity;
    private Map<Integer, Activity[]> previousActivities, futureActivities, revisionHistory;
    private List<Activity> currentActivities, relatedActivities, pendingReviews;
    private String[] activityStatuses;
    private boolean workplanSubmitted, canSubmit;
    private int activityIndex, activityID, activitiesFilled;
    private int submitToCA;
    private boolean submitToCARequest = false;
    private String activityStringID, revisionId;

    private List<String> yearSelection;

    private List<DataLookup> flagshipLookup;
    private List<DataLookup> activityClusterLookup;
    private List<DataLookup> leadCentersLookup;

    @Inject
    public ActivitiesPlanningAction(APConfig config, LogframeManager logframeManager, ActivityManager activityManager,
        LeaderManager leaderManager, ContactPersonManager contactPersonManager, DeliverableManager deliverableManager,
        ActivityPartnerManager activityPartnerManager, ActivityObjectiveManager activityObjectiveManager,
        ActivityCountryManager activityCountryManager, ActivityRegionManager activityRegionManager,
        ActivityBenchmarkSiteManager activityBenchmarkSiteManager, ActivityOtherSiteManager activityOtherSiteManager,
        SubmissionManager submissionManager, DataLookUpManager dataLookUpManager, ActivityReviewerDAO reviewerDao,
        UserManager userManager)
    {
        super(config, logframeManager);
        this.activityManager = activityManager;
        this.leaderManager = leaderManager;
        this.contactPersonManager = contactPersonManager;
        this.deliverableManager = deliverableManager;
        this.activityPartnerManager = activityPartnerManager;
        this.activityObjectiveManager = activityObjectiveManager;
        this.activityCountryManager = activityCountryManager;
        this.activityRegionManager = activityRegionManager;
        this.activityBenchmarkSiteManager = activityBenchmarkSiteManager;
        this.activityOtherSiteManager = activityOtherSiteManager;
        this.submissionManager = submissionManager;
        this.dataLookUpManager = dataLookUpManager;
        this.userManager = userManager;
        this.reviewerDao = reviewerDao;

        createData();
    }

    private void createData()
    {
        List<Integer> yearList = activityManager.getYearList();
        yearSelection = new ArrayList<>();
        for (int year : yearList)
        {
            yearSelection.add(String.valueOf(year));
        }

        DataLookup dummyLookup = createDummyLookup();

        activityClusterLookup = new ArrayList<DataLookup>();
        activityClusterLookup.add(dummyLookup);

        flagshipLookup = new ArrayList<DataLookup>();
        flagshipLookup.add(dummyLookup);
        flagshipLookup.addAll(dataLookUpManager.getAllLookUpdata(LookupInfoEnum.FLAGSHIP_SRP));

        leadCentersLookup = new ArrayList<DataLookup>();
        leadCentersLookup.add(dummyLookup);
        leadCentersLookup.addAll(dataLookUpManager.getAllLookUpdata(LookupInfoEnum.LEADER_CENTER));
    }

    private DataLookup createDummyLookup()
    {
        DataLookup dummyLookup = new DataLookup();
        dummyLookup.setId(-1);
        dummyLookup.setDisplayName("");
        dummyLookup.setValue("");
        return dummyLookup;
    }

    public int getActivityID()
    {
        return activityID;
    }

    public String getRevisionId()
    {
        return revisionId;
    }

    public String getActivityRequestParameter()
    {
        return APConstants.ACTIVITY_REQUEST_ID;
    }

    public String[] getActivityStatuses()
    {
        return activityStatuses;
    }

    public String getActivityYearRequest()
    {
        return APConstants.ACTIVITY_YEAR_REQUEST;
    }

    public List<Activity> getCurrentActivities()
    {
        return currentActivities;
    }

    public List<Activity> getPendingReviews()
    {
        return pendingReviews;
    }

    public Map<Integer, Activity[]> getRevisionHistory()
    {
        return revisionHistory;
    }

    public int getCurrentYear()
    {
        return config.getPlanningCurrentYear();
        // return Calendar.getInstance().get(Calendar.YEAR);
    }

    public Map<Integer, Activity[]> getFutureActivities()
    {
        return futureActivities;
    }

    public List<Activity> getOthersActivities()
    {
        return relatedActivities;
    }

    public List<Activity> getOwnActivities()
    {
        return currentActivities;
    }

    public Map<Integer, Activity[]> getPreviousActivities()
    {
        return previousActivities;
    }

    public String getPublicActivityRequestParameter()
    {
        return APConstants.PUBLIC_ACTIVITY_ID;
    }

    public List<Activity> getRelatedActivities()
    {
        return relatedActivities;
    }

    public boolean isCanSubmit()
    {
        return canSubmit;
    }

    public boolean isFuturePlanningActive()
    {
        return config.isPlanningForFutureYearsActive();
    }

    public boolean isRevisionHistoryActive()
    {
        return true;
    }

    /**
     * @return the yearSelection
     */
    public List<String> getYearSelection()
    {
        return yearSelection;
    }

    public List<DataLookup> getFlagshipLookup()
    {
        return flagshipLookup;
    }

    public List<DataLookup> getActivityClusterLookup()
    {
        return activityClusterLookup;
    }

    public List<DataLookup> getLeadCentersLookup()
    {
        return leadCentersLookup;
    }

    public int getSubmitToCA()
    {
        return submitToCA;
    }

    public void setSubmitToCA(int submitToCA)
    {
        this.submitToCA = submitToCA;
    }

    /**
     * This method checks if the activity to submit is valid, checking its owner, if it is from current year and if the
     * activity isn't submitted yet.
     */
    private boolean isValidActivity()
    {
        boolean isValidActivity = false;

        // Check if the activity is valid
        if (activityManager.isValidId(activityID))
        {
            // Check if the activity is from current year
            if (activityManager.isActiveActivity(activityID, getCurrentYear()))
            {
                // Check if the current user owns the activity to submit
                if (leaderManager.getActivityLeader(activityID).getId() == getCurrentUser().getLeader().getId())
                {
                    // Check if the activity hasn't been submitted yet
                    if (!activityManager.isValidatedActivity(activityID, revisionId))
                    {
                        isValidActivity = true;
                    }
                }
            }
        }
        return isValidActivity;
    }

    public boolean isWorkplanSubmitted()
    {
        return workplanSubmitted;
    }

    /**
     * This method is called only after press the validate button if the activity is valid.
     */
    private void loadActivityInformation()
    {
        // Load the activity data to validate if it is complete.
        activity = new Activity();

        // Get the basic information about the activity
        activity = activityManager.getActivityStatusInfo(activityID);
        // Set contact persons
        activity.setContactPersons(contactPersonManager.getContactPersons(activityID));
        // Set the deliverables
        activity.setDeliverables(deliverableManager.getDeliverables(activityID));
        // Set activity partners
        activity.setActivityPartners(activityPartnerManager.getActivityPartners(activityID));
        // Set the activity objectives
        activity.setObjectives(activityObjectiveManager.getActivityObjectives(activityID));
        // Set activity countries
        activity.setCountries(activityCountryManager.getActvitiyCountries(activityID));
        // Set activity regions
        activity.setRegions(activityRegionManager.getActvitiyRegions(activityID));
        // Set activity benchmark sites
        activity.setBsLocations(activityBenchmarkSiteManager.getActivityBenchmarkSites(activityID));
        // Set activity other sites
        activity.setOtherLocations(activityOtherSiteManager.getActivityOtherSites(activityID));
    }

    @Override
    public void prepare() throws Exception
    {
        super.prepare();
        LOG.info("User {} load the list of activities for leader {} in planing section", getCurrentUser().getEmail(),
            getCurrentUser().getLeader().getId());

        /* --------- Getting all the activities ------------- */
        previousActivities = new TreeMap<>();
        futureActivities = new TreeMap<>();
        revisionHistory = new TreeMap<>();
        currentActivities = new ArrayList<>();
        relatedActivities = new ArrayList<>();
        pendingReviews = new ArrayList<>();
        activitiesFilled = 0;

        User user = getCurrentUser();
        ActivityStatus[] activityStatus = null;

        //      if (user.isPI()) {
        //          activityStatus = new ActivityStatus[] { ActivityStatus.REVIEWING };
        //      } else {
        activityStatus =
            new ActivityStatus[] {
                ActivityStatus.NEW,
                ActivityStatus.IN_PROGRESS,
                ActivityStatus.VALIDATED,
                ActivityStatus.INTERNAL_REVIEWING,
                ActivityStatus.INTERNAL_CORRECTIONS,
                ActivityStatus.SUBMITTED_TO_CA,
                ActivityStatus.REVIEWING,
                ActivityStatus.REVIEWS_COMPLETED,
                ActivityStatus.SUBMITTED,
                ActivityStatus.COMPLETED
            };
        //      }

        // For TL and RPL this list will contain the own activities and the
        // activities related to their programmes.
        Activity[] activities = activityManager.getPlanningActivityList(getCurrentYear(), user, activityStatus);

        Arrays.sort(activities);

        for (Activity activity : activities)
        {
            activity.setLoggedInUser(getCurrentUser());

            if (activity.getLeader().getId() == user.getLeader().getId())
            {
                // Check if the activity is filled
                activitiesFilled += (activity.isValidated()) ? 1 : 0;

                if (user.isPI())
                {
                    activity.setCanDelete(false);
                    if(activity.getActivityStatus() == REVIEWING || activity.getActivityStatus() == INTERNAL_REVIEWING)
                    {
                        activity.setPendingReview(true);
                    }
                }

                // check if activity in review state
                if (user.isPI() && activity.isReviewer(user))
                    currentActivities.add(activity);
                else
                    currentActivities.add(activity);
                
                if(activity.getActivityStatus() == INTERNAL_CORRECTIONS) {
                    activity.setCanDelete(false);
                    activity.setPendingReview(false);
                    activity.setToBeSubmittedToCA(true);
                }
                
                if(activity.getActivityStatus() == REVIEWS_COMPLETED) {
                    activity.setCanDelete(false);
                    activity.setPendingReview(false);
                    activity.setToBeSubmittedToCA(false);
                    activity.setReviewsCompleted(true);
                }
                
                if(activity.getActivityStatus() == REVIEWING || activity.getActivityStatus() == INTERNAL_REVIEWING) {
                    activity.setCanDelete(false);
                    activity.setPendingReview(true);
                    if (activity.isEditable())
                    {
                        pendingReviews.add(activity);
                    }
                }
            }
            else
            {
                relatedActivities.add(activity);
            }
        }

        // Load activities from previous years.
        for (int year = 2012; year < getCurrentYear(); year++)
        {
            activities = activityManager.getPlanningActivityList(year, user, activityStatus);
            if (user.isPI())
            {
                Activity[] prevActs = new Activity[activities.length];
                int prevActCount = 0;
                for (int i = 0; i < activities.length; i++)
                {
                    if (activities[i].isReviewer(user))
                    {
                        prevActs[prevActCount] = activities[i];
                        prevActCount++;
                    }
                }
                previousActivities.put(year, prevActs);
            }
            else
            {
                previousActivities.put(year, activities);
            }
        }

        // Load activities from futures years.
        if (isFuturePlanningActive())
        {
            int endYear = getCurrentYear() + config.getFuturePlanningYears();
            for (int year = getCurrentYear() + 1; year <= endYear; year++)
            {
                activities = activityManager.getPlanningActivityList(year, user, activityStatus);
                if (user.isPI())
                {
                    Activity[] futureActs = new Activity[activities.length];
                    int futureActCount = 0;
                    for (int i = 0; i < activities.length; i++)
                    {
                        if (activities[i].isReviewer(user))
                        {
                            futureActs[futureActCount] = activities[i];
                            futureActCount++;
                        }
                    }
                    futureActivities.put(year, futureActs);
                }
                else
                {
                    futureActivities.put(year, activities);
                }
            }
        }

        // load revision
        for (int year = config.getStartYear(); year <= getCurrentYear() + 1; year++)
        {
            activities = activityManager.getPlanningActivityList(year, user, REVISION);

            //            if (user.isPI())
            //            {
            //                Activity[] prevActs = new Activity[activities.length];
            //                int prevActCount = 0;
            //                for (int i = 0; i < activities.length; i++)
            //                {
            //                    if (activities[i].isReviewer(user))
            //                    {
            //                        prevActs[prevActCount] = activities[i];
            //                        prevActCount++;
            //                    }
            //                }
            //                revisionHistory.put(year, prevActs);
            //            }
            //            else
            //            {
            revisionHistory.put(year, activities);
            //            }
        }

        /* --------- Checking if the user can submit ------------- */
        // First, check if the workplan is already submitted.
        submission =
            submissionManager.getSubmission(getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
                APConstants.PLANNING_SECTION);

        if (submission == null)
        {
            workplanSubmitted = false;

            // If the workplan wasn't submitted yet, the user can do it if
            // all activities are completely filled.
            canSubmit = (currentActivities.size() == activitiesFilled);
        }
        else
        {
            workplanSubmitted = true;
            // If the user already did the submission thus can't do it again.
            canSubmit = false;
        }
    }

    @Override
    public String save()
    {
        boolean validated = false;
        
        if(submitToCARequest) {
            submitToCARequest = false;
            return INPUT;
        }
        currentActivities.get(activityIndex).setValidated(true);
        validated = activityManager.validateActivity(currentActivities.get(activityIndex));

        if (validated)
        {
            activitiesFilled++;
            // After make the activity validation we must
            // check if now the user can submit
            if (submission == null)
            {
                canSubmit = (currentActivities.size() == activitiesFilled);
            }
            addActionMessage(getText("planning.activityList.validation.success", new String[] {
                activityStringID
            }));
        }
        else
        {
            addActionError(getText("saving.problem"));
        }

        return INPUT;
    }
    
    private void sendNotificationsToCA(Activity activity)
    {
        String directAccessURL =
            config.getBaseUrl() + "/planning/mainInformation.do?activityID=" + activity.getId();

        String subject = getText("planning.correction.submit.subject", new String[] {
            activity.getActivityId()
        });

        List<User> users = leaderManager.getLeadCenterAdmins(activity.getLeadCenter());
        
        for(User user : users) {
            String message = getText("planning.correction.submit.text", new String[] {
                user.getName(), directAccessURL
            });

            SendMail emailMessage = new SendMail(config);
            emailMessage.send(user.getEmail(), subject, message);
        }
       
    }

    public void setActivityIndex(int activityIndex)
    {
        this.activityIndex = activityIndex;
    }

    public String submit()
    {
        boolean submitted;

        if (submission != null)
        {
            addActionWarning("Workplan already submitted");
            return INPUT;
        }

        submission = new Submission();
        submission.setLeader(getCurrentUser().getLeader());
        submission.setLogframe(getCurrentPlanningLogframe());
        submission.setSection(APConstants.PLANNING_SECTION);

        submitted = submissionManager.submit(submission);
        if (submitted)
        {
            // Now the user can't submit again.
            workplanSubmitted = true;
            canSubmit = false;

            if (!config.getBaseUrl().contains("localhost") && !config.getBaseUrl().contains("/test"))
            {
                //                sendConfirmationMessage();
            }

            addActionMessage(getText("submit.success"));
            activityManager.updateActivitStatus(activity.getId(), ActivityStatus.SUBMITTED, activity.getRevisionId());
        }
        else
        {
            addActionError(getText("submit.error"));
        }
        return INPUT;
    }

    @Override
    public void validate()
    {
        // After validate that all information is complete, set the activity as
        // submitted
        if(submitToCA == 1) {
        	
            submitToCARequest = true;
            Activity activity = currentActivities.get(activityIndex);
            List<User> cadmins = leaderManager.getLeadCenterAdmins(activity.getLeadCenter());
            if(cadmins.isEmpty()) {
            	activityManager.updateActivitStatus(activity.getId(), REVIEWING, activity.getRevisionId());
            	List<ActivityReview> extReviewers = reviewerDao.getReviewers(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE);
            	sendNotificationMessage(activity.getId(), activity.getRevisionId(), extReviewers);
            	
            } else {
            	 activityManager.updateActivitStatus(activity.getId(), SUBMITTED_TO_CA, activity.getRevisionId());
                 activity.setToBeSubmittedToCA(false);
                 activity.setValidated(true);
                 
                 // notify CA
                 sendNotificationsToCA(activity);
                 
                 addActionMessage(getText("planning.activityList.submit.to.ca.success", new String[] {
                     activity.getActivityId()}));
            }
           
            return;
        } 
        
        if (save)
        {
            String result;
            activityID = currentActivities.get(activityIndex).getId();
            revisionId = currentActivities.get(activityIndex).getRevisionId();
            activityStringID = currentActivities.get(activityIndex).getFormattedId();
            // Check if the user can submit the activity
            if (!isValidActivity())
            {
                addActionError(getText("planning.activityList.validation.noValidActivity", new String[] {
                    String.valueOf(activityID)
                }));
                return;
            }

            loadActivityInformation();
            // Load the information to validate

            // Validate process
            ActivityValidator av = new ActivityValidator();
            result = av.validateActivityPlanning(activity);

            if (!result.isEmpty())
            {
                addActionError(result);
            }
            else if (activity.getYear() < getCurrentYear())
            {
                activityManager.updateActivitStatus(activity.getId(), VALIDATED, activity.getRevisionId());
            }
            else
            {
                // get assigned reviewers
                List<ActivityReview> intReviewers = Collections.emptyList();
                List<ActivityReview> extReviewers = Collections.emptyList();
                boolean hasInternalReviewers = false;
                if (leaderManager.isInternalReviewEnabled(activity.getLeadCenter())  && valueOf(revisionId) < 1)
                {
                    intReviewers = reviewerDao.hasAnyPendingReviews(activity.getId(), activity.getRevisionId(), INTERNAL_TYPE);
                    hasInternalReviewers = !intReviewers.isEmpty();
                }
                else
                {
                    extReviewers = reviewerDao.hasAnyPendingReviews(activity.getId(), activity.getRevisionId(), EXTERNAL_TYPE);
                }
                
                // Notification sending
                if (leaderManager.isInternalReviewEnabled(activity.getLeadCenter()) && valueOf(revisionId) < 1)
                {
                    if (hasInternalReviewers)
                    {
                        activityManager.updateActivitStatus(activity.getId(), INTERNAL_REVIEWING,
                            activity.getRevisionId());
                        sendNotificationMessage(activity.getId(), activity.getRevisionId(), intReviewers);
                    }
                    else
                    {
                        activityManager.updateActivitStatus(activity.getId(), SUBMITTED_TO_CA,
                            activity.getRevisionId());
                        activity.setToBeSubmittedToCA(false);
                        activity.setValidated(true);
                        // notify CA
                        sendNotificationsToCA(activity);
                    }
                }
                else if(!extReviewers.isEmpty())
                {
                    activityManager.updateActivitStatus(activity.getId(), REVIEWING, activity.getRevisionId());
                    sendNotificationMessage(activity.getId(), activity.getRevisionId(), extReviewers);
                } else {
                    activityManager.updateActivitStatus(activity.getId(), REVIEWS_COMPLETED, activity.getRevisionId());
                    sendNotificationMessage(activity.getId(), activity.getRevisionId(), extReviewers);
                }
            }
        }
    }

    private void sendNotificationMessage(int activityId, String revisionId, List<ActivityReview> reviewers)
    {
        Activity activity = activityManager.getSimpleActivity(activityId);

        String directAccessURL =
            config.getBaseUrl() + "/planning/mainInformation.do?activityID=" + activity.getId();

        String subject = getText("planning.review.available.subject", new String[] {
            activity.getActivityId()
        });

        for (ActivityReview reviewer : reviewers)
        {

            User user = userManager.getById((int) reviewer.getReviewerId());
            if(user == null) continue;
            
            String message = getText("planning.review.available.text", new String[] {
                user.getName(), directAccessURL
            });

            SendMail emailMessage = new SendMail(config);
            LOG.debug("Send E-MAIL to reviewers : \n\tUser : {},\n\t\t Subject : {},\n\t\t Content: " + message + "",
                user.getName(), subject);
            emailMessage.send(user.getEmail(), subject, message);

        }
    }
}
