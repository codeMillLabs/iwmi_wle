/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.planning;

import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.OUTCOME_USER_ROLES;
import static org.cgiar.ccafs.ap.util.WleAppUtil.sendContentChangedNotification;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityObjective;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.DiffReport;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class ObjectivesPlanningAction extends BaseAction {

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(ObjectivesPlanningAction.class);
	private static final long serialVersionUID = -8764463530270764535L;

	// Managers
	private ActivityManager activityManager;
	private ActivityObjectiveManager activityObjectiveManager;
	private SubmissionManager submissionManager;
	private DataLookUpManager dataLookUpManager;

	// Model
	private int activityID;
	private Activity activity;
	private StringBuilder validationMessage;
	private Map<String, String> outcomeTypes;
	private List<DataLookup> researchRoles;
	private boolean canSubmit;

	@Inject
	public ObjectivesPlanningAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			ActivityObjectiveManager activityObjectiveManager,
			SubmissionManager submissionManager,
			DataLookUpManager dataLookUpManager) {
		super(config, logframeManager);
		this.activityManager = activityManager;
		this.activityObjectiveManager = activityObjectiveManager;
		this.submissionManager = submissionManager;
		this.dataLookUpManager = dataLookUpManager;

		this.outcomeTypes = new LinkedHashMap<>();
		outcomeTypes.put("0", getText("planning.option.research.outcomes"));
		outcomeTypes.put("1",
				getText("planning.option.gender.research.outcomes"));
	}

	public Activity getActivity() {
		return activity;
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public Map<String, String> getOutcomeTypes() {
		return outcomeTypes;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	@Override
	public String next() {
		save();
		return super.next();
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		validationMessage = new StringBuilder();

		String activityStringID = StringUtils.trim(this.getRequest()
				.getParameter(APConstants.ACTIVITY_REQUEST_ID));
		try {
			activityID = Integer.parseInt(activityStringID);
		} catch (NumberFormatException e) {
			LOG.error(
					"-- prepare() > There was an error parsing the activity identifier '{}'.",
					activityStringID, e);
		}

		LOG.info(
				"-- prepare() > User {} load the objectives for activity {} in planing section",
				getCurrentUser().getEmail(), activityID);

		// Get the basic information about the activity
		activity = activityManager.getActivity(activityID);
		activity.setLoggedInUser(getCurrentUser());

		// Get the activity objectives
		activity.setObjectives(activityObjectiveManager
				.getActivityObjectives(activityID));

		researchRoles = dataLookUpManager.getAllLookUpdata(OUTCOME_USER_ROLES);

		// If the workplan was submitted before the user can't save new
		// information
		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
				APConstants.PLANNING_SECTION);
		canSubmit = (submission == null) ? true : false;

		if (getRequest().getMethod().equalsIgnoreCase("post")) {
			activity.getObjectives().clear();
		}
	}

	@Override
	public String save() {
		int saved = -1;

		List<ActivityObjective> prevObjectives = activityObjectiveManager
				.getActivityObjectives(activityID);
		DiffReport diffReport = diffCheck(prevObjectives,
				activity.getObjectives());
		sendContentChangedNotification(config, diffReport, activity.getFormattedId(),  getCurrentUser());

		// First delete all the objectives from the DAO
		activityObjectiveManager.deleteActivityObjectives(activityID);
		// Save the new objectives
		saved = activityObjectiveManager.saveActivityObjectives(
				activity.getObjectives(), activityID);
		if (saved > 0) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(getText("saving.success",
						new String[] { getText("planning.objectives") }));
			} else {
				String finalMessage = getText("saving.success",
						new String[] { getText("planning.objectives") });
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			// As there were changes in the activity we should mark the
			// validation as false
            if (activity.needValidation())
            {
                activity.setValidated(false);
                activityManager.validateActivity(activity);
            } else {
                activity.setValidated(true);
            }

			LOG.info(
					"-- save() > User {} save successfully the objectives for activity {}",
					this.getCurrentUser().getEmail(), activityID);
			return SUCCESS;
		} else {
			LOG.info(
					"-- save() > User {} had problems to save the objectives for activity {}",
					this.getCurrentUser().getEmail(), activityID);
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public List<DataLookup> getResearchRoles() {
		return researchRoles;
	}

	public void setResearchRoles(List<DataLookup> researchRoles) {
		this.researchRoles = researchRoles;
	}

	private DiffReport diffCheck(List<ActivityObjective> prev,
			List<ActivityObjective> current) {
		DiffReport report = new DiffReport();
		report.setTitle("Outcomes");

		if (current.size() < prev.size()) {
			report.append((prev.size() - current.size())
					+ " Activity objectives removed");
		}

		for (int i = 0; i < current.size(); i++) {
			if (i >= prev.size()) {
				report.append("New Objective added. Details : "
						+ current.get(i).getDescription());
			} else {
				ActivityObjective old = prev.get(i);
				ActivityObjective newKeyword = current.get(i);
				report.append(old.diff(newKeyword));
			}
		}

		return report;
	}

	@Override
	public void validate() {
		if (save) {
			// Remove the empty objectives
			for (int c = 0; c < activity.getObjectives().size(); c++) {
				if (activity.getObjectives().get(c).getDescription() == null
						|| activity.getObjectives().get(c).getDescription()
								.isEmpty()) {
					activity.getObjectives().remove(c);
					c--;
				} else {
					if (activity.getObjectives().get(c).getOutcomeType() == null) {
						validationMessage
								.append(getText("planning.outcome.type"));
					}
				}
			}

			// Activity must have at least one objective
			if (activity.getObjectives().isEmpty()) {
				validationMessage
						.append(getText("planning.objectives.validation.atLeastOne"));
			}
		}
	}

}
