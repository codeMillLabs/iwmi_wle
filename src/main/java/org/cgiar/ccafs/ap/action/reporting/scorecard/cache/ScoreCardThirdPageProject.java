package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class ScoreCardThirdPageProject {

	private int id;
	private int flagshipId;
	private String projectId;
	private String title;
	private String flagshipShortName;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getFlagshipId() {
		return flagshipId;
	}
	
	public void setFlagshipId(int flagshipId) {
		this.flagshipId = flagshipId;
	}
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getFlagshipShortName() {
		return flagshipShortName;
	}

	public void setFlagshipShortName(String flagshipShortName) {
		this.flagshipShortName = flagshipShortName;
	}
}