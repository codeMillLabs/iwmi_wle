/*
 * FILENAME
 *     ActivityClusterReportingAction.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package org.cgiar.ccafs.ap.action.reporting.activities;

import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.CORRECTIONS;
import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.STAGE_1;
import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.STAGE_2;
import static org.cgiar.ccafs.ap.data.model.ActivityReviewRating.STAGE_FINALIZED;
import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.ACTIVITY_CLUSTER;
import static org.cgiar.ccafs.ap.data.model.LookupInfoEnum.FLAGSHIP_SRP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.DataLookUpManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityBudgetTotals;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.DataLookup;
import org.cgiar.ccafs.ap.data.model.DeliverableReviewRating;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipReportData;
import org.cgiar.ccafs.ap.data.model.FlagshipRptStatus;
import org.cgiar.ccafs.ap.data.model.LookupInfoEnum;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.cgiar.ccafs.ap.util.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Activity cluster based report for reviews.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
public class ActivityClusterReportingAction extends BaseAction {

	private static final long serialVersionUID = -2319298647460588550L;

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityClusterReportingAction.class);

	// Managers
	private ActivityManager activityManager;
	private ActivityBudgetManager activityBudgetManager;
	private SubmissionManager submissionManager;
	private StringBuilder validationMessage;
	private UserManager userManager;
	private DataLookUpManager dataLookUpManager;
	private FlagshipFocalRegionManager flagshipFocalRegionManager;

	private Map<Integer, FlagshipFocalRegion> flagshipFocalRegionMap = new HashMap<Integer, FlagshipFocalRegion>();
	
	// Model
	private int activityID;
	private Activity activity;
	private boolean canSubmit = false;
	private String periodStart, periodEnd;
	private boolean reviewSubmit = false;
	private boolean canSendDraft = false;
	private boolean canPublish = false;
	private boolean published = false;
	private String buttonAction = "";

	@Inject
	public ActivityClusterReportingAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			ActivityBudgetManager activityBudgetManager,
			SubmissionManager submissionManager,
			DataLookUpManager dataLookUpManager,
			FlagshipFocalRegionManager flagshipFocalRegionManager, UserManager userManager) {
		super(config, logframeManager);
		this.activityManager = activityManager;
		this.activityBudgetManager = activityBudgetManager;
		this.submissionManager = submissionManager;
		this.dataLookUpManager = dataLookUpManager;
		this.userManager = userManager;
		this.flagshipFocalRegionManager = flagshipFocalRegionManager;

		List<FlagshipFocalRegion> focalRegions = this.flagshipFocalRegionManager
				.getFlagshipFocalRegions();
		for (FlagshipFocalRegion focalRegion : focalRegions) {
			flagshipFocalRegionMap.put(focalRegion.getId(), focalRegion);
		}
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		validationMessage = new StringBuilder();

		LOG.info(
				"-- prepare() > User {} load the Activity cluster reporting {} in reporting section",
				getCurrentUser().getEmail(), getCurrentUser().getLeader()
						.getId());
		String reviewMode = getRequest().getParameter("review_mode");

		String activityStringID = StringUtils.trim(this.getRequest()
				.getParameter(APConstants.ACTIVITY_REQUEST_ID));
		try {
			activityID = Integer.parseInt(activityStringID);
		} catch (NumberFormatException e) {
			LOG.error(
					"-- prepare() > There was an error getting the activity identifier '{}' from URL.",
					activityStringID, e);
		}

		// Get the basic information about the activity
		activity = activityManager.getActivity(activityID);
		activity.setLoggedInUser(getCurrentUser());

		DataLookup flagshipLookup = dataLookUpManager.getLookUpdataByValue(FLAGSHIP_SRP, activity.getFlagShipSRP(), false);
		DataLookup activityClusterLookup = dataLookUpManager.getLookUpdataByValue(ACTIVITY_CLUSTER, activity.getActivityCluster(), false);
		String activityCluster = activityClusterLookup.getDisplayName();
		activity.setFlagShipSRP(flagshipLookup.getDisplayName());
		activity.setActivityCluster(activityCluster);
		
		if ("mid".equals(reviewMode)) {
			periodStart = "01 January " + activity.getYear();
			periodEnd = "30 June " + activity.getYear();
		} else {
			periodStart = "01 July " + activity.getYear();
			periodEnd = "31 December " + activity.getYear();
		}

		ActivityBudget activityBudget = activityBudgetManager
				.getBudget(activityID);
		ActivityBudgetTotals budgetTotals = activityBudget.sumAll();
		double totalFunding = budgetTotals.getGrandTotalOfFunding();
//		totalFunding += budgetTotals.getGrandDonorTotal();

		// Set activity midYearReview
		ActivityReviewRating midYearReview = activityManager
				.getMidYearRating(activity.getActivityId());

		if (midYearReview == null) {
			midYearReview = new ActivityReviewRating();
			midYearReview.setStage(STAGE_1);
			midYearReview.setActivityId(activity.getActivityId());
			canSendDraft = true;
		}

		midYearReview.setActivityCreatedBy(activity.getCreatedUser());
		midYearReview.setLoggedInUser(getCurrentUser());

		midYearReview.setTotalFunding(totalFunding);
		activity.setMidYearReviews(midYearReview);

		if ((midYearReview.getStage() == STAGE_1 || midYearReview.getStage() == CORRECTIONS)
				&& midYearReview.isEditable()) {
			canSendDraft = true;
		}

		// check for review submit button
		if (midYearReview.getStage() == STAGE_2
				&& getCurrentUser().hasRole(UserRole.FL)) {
			reviewSubmit = true;
		} else if (midYearReview.getStage() == CORRECTIONS
				&& activity.getCreatedUser().equalsIgnoreCase(
						getCurrentUser().getEmail())) {
			canPublish = true;
			canSendDraft = false;
		} else if(midYearReview.getStage() == STAGE_FINALIZED) {
			published = true;
			canPublish = false;
			canSendDraft = false;
		}

		/* --------- Checking if the user can submit ------------- */
		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentReportingLogframe(),
				APConstants.REPORTING_SECTION);
		canSubmit = (submission == null) ? true : false;
	}

	@Override
	public String save() {
		LOG.info("-- Save() > Cluster Reporting, Activity : {}, Action :{} ", activityID, buttonAction);
		boolean valueSaved = false;

		String saveMessage = "Project report saved successfully";

		ActivityReviewRating midYearReview = activity.getMidYearReviews();
		midYearReview.setActivityId(activity.getActivityId());
		
		if (midYearReview.getStage() == STAGE_1) {
		   midYearReview.setCreatedBy(getCurrentUser().getEmail());
		} else if (midYearReview.getStage() == STAGE_2) {
			saveMessage = "Project report review feedbacks saved successfully";
		}
		
		valueSaved = activityManager.saveOrUdatetMidYearRating(
				activity.getActivityId(), midYearReview);

		LOG.debug("-- Save() > Cluster Reporting, Activity : Saved {} ",
				valueSaved);

		if (valueSaved) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(saveMessage);
			} else {
				String finalMessage = getText(saveMessage);
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			return SUCCESS;
		} else {
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}

	@Override
	public String confirm() {
		LOG.info("-- Review Confirm() > Cluster Reporting, Activity : {}, ",
				activityID);
		boolean valueSaved = false;

		String saveMessage = "Project report reviewed successfully and been sent to the Project Leader";

		ActivityReviewRating midYearReview = activity.getMidYearReviews();
		midYearReview.setActivityId(activity.getActivityId());
		midYearReview.setStage(CORRECTIONS);

		sendEmailToOwner(midYearReview.getCreatedBy());
		valueSaved = activityManager.saveOrUdatetMidYearRating(
				activity.getActivityId(), midYearReview);

		LOG.debug(
				"-- Review Confirm() > Cluster Reporting, Activity : Saved {} ",
				valueSaved);

		if (valueSaved) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(saveMessage);
			} else {
				String finalMessage = getText(saveMessage);
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			return SUCCESS;
		} else {
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}

	@Override
	public String sendDraft() {
		LOG.info("-- sendDraft() > Cluster Reporting, Activity : {}, ",
				activityID);
		boolean valueSaved = false;

		String saveMessage = "Project Report has been sent to the Flagship Leader successfully";

		ActivityReviewRating midYearReview = activity.getMidYearReviews();
		midYearReview.setActivityId(activity.getActivityId());
		midYearReview.setCreatedBy(getCurrentUser().getEmail());
		
		StringBuilder errorMsg = new StringBuilder();
		if (!isValidToProcced(midYearReview, errorMsg)) {
			addActionWarning(Capitalize.capitalizeString(errorMsg.toString()));
			return INPUT;
		}

		if (midYearReview.getStage() == STAGE_1
				|| midYearReview.getStage() == CORRECTIONS) {
			midYearReview.setStage(STAGE_2);
			sendEmailToFlagshipLeader();
		}
		valueSaved = activityManager.saveOrUdatetMidYearRating(
				activity.getActivityId(), midYearReview);

		LOG.debug("-- sendDraft() > Cluster Reporting, Activity : Saved {} ",
				valueSaved);

		if (valueSaved) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(saveMessage);
			} else {
				String finalMessage = getText(saveMessage);
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			return SUCCESS;
		} else {
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}

	@Override
	public String publish() {
		LOG.info("-- publish() > Cluster Reporting, Activity : {}, ",
				activityID);
		boolean valueSaved = false;

		String saveMessage = "Your project report has been published successfully. No further action is required";

		ActivityReviewRating midYearReview = activity.getMidYearReviews();
		midYearReview.setActivityId(activity.getActivityId());
		midYearReview.setStage(ActivityReviewRating.STAGE_FINALIZED);
		
		StringBuilder errorMsg = new StringBuilder();
		if (!isValidToProcced(midYearReview, errorMsg)) {
			addActionWarning(Capitalize.capitalizeString(errorMsg.toString()));
			return INPUT;
		}

		valueSaved = activityManager.saveOrUdatetMidYearRating(
				activity.getActivityId(), midYearReview);
		
		sendPublishedEmailToFlagshipLeader();

		LOG.debug("-- publish() > Cluster Reporting, Activity : Saved {} ",
				valueSaved);

		if (valueSaved) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(saveMessage);
			} else {
				String finalMessage = getText(saveMessage);
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			checkFlagshipReportReadyness(activity.getActivityId());
			return SUCCESS;
		} else {
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}
	
	private boolean isValidToProcced(ActivityReviewRating reviewRating, StringBuilder errorMessage) {
		boolean isValid = true;
		if (isEmpty(reviewRating.getMidYearReviewProgress())) {
			errorMessage.append("Overall Project Status is empty, ");
			isValid = false;
		}
		
		if (reviewRating.getMidYearReviewRating() < 0) {
			errorMessage.append("Overall Project Rating not selected, ");
			isValid = false;
		}
		
		if(reviewRating.getTotalSpent() < 0) {
			errorMessage.append("SPENT BY END JUNE not specified, ");
			isValid = false;
		}
		
		int i = 1;
		for(DeliverableReviewRating dr : reviewRating.getDeliverableRating()) {
			String em = "";
			if(isEmpty(dr.getOutputType())) {
			    em += " Output Type, ";
			} 
			
			if(dr.getMidYearReviewRating() < 0) {
			    em += "Output Project Rating not selected, ";
			} 
			if(isEmpty(dr.getMidYearReviewProgress())) {
				em += " Progress towards output at mid-year point, ";
			}
			if(!em.isEmpty()) {
			  errorMessage.append(" Deliverable Item [" + i + "]" + em + " are missing, ");
			  isValid = false;
			}
			++i;
		}
		
		return isValid;
	}
	
	private boolean isEmpty(String value){
		return value == null || value.trim().isEmpty();
	}
	
	private void sendPublishedEmailToFlagshipLeader() {
		Activity tempActivity = activityManager.getActivity(activity.getId());
		List<String> emails = dataLookUpManager.flagshipLeaderEmail(tempActivity
				.getFlagShipSRP());
		
		String subject = getText(
				"reporting.send.notification.to.published.subject",
				new String[] { activity.getActivityId(), });

		String directAccessUrl = config.getBaseUrl()
				+ "/reporting/activityClusterReport.do?activityID="
				+ getActivity().getId() + "&review_mode=mid";

		for (String userEmail : emails) {
			User flagshipLeader = userManager.getUser(userEmail);
			if (flagshipLeader != null) {
				String message = getText(
						"reporting.send.notification.to.published.text",
						new String[] { flagshipLeader.getName(),
								activity.getTitle(), directAccessUrl });

				SendMail emailMessage = new SendMail(config);
				emailMessage.send(flagshipLeader.getEmail(), subject, message);
			}
		}
	}

	private void sendEmailToFlagshipLeader() {
		Activity tempActivity = activityManager.getActivity(activity.getId());
		List<String> emails = dataLookUpManager.flagshipLeaderEmail(tempActivity
				.getFlagShipSRP());
		String subject = getText(
				"reporting.send.notification.to.flagship.leader.subject",
				new String[] { activity.getActivityId(), });

		String directAccessUrl = config.getBaseUrl()
				+ "/reporting/activityClusterReport.do?activityID="
				+ getActivity().getId() + "&review_mode=mid";

		for (String userEmail : emails) {
			User flagshipLeader = userManager.getUser(userEmail);
			if (flagshipLeader != null) {
				String message = getText(
						"reporting.send.notification.to.flagship.leader.text",
						new String[] { flagshipLeader.getName(),
								activity.getTitle(), directAccessUrl });

				SendMail emailMessage = new SendMail(config);
				emailMessage.send(flagshipLeader.getEmail(), subject, message);
			}
		}
	}

	private void sendEmailToOwner(String email) {

		String subject = getText(
				"reporting.send.notification.to.reviewed.subject",
				new String[] { activity.getActivityId(), });

		String directAccessUrl = config.getBaseUrl()
				+ "/reporting/activityClusterReport.do?activityID="
				+ getActivity().getId() + "&review_mode=mid";

		User owner = userManager.getUser(email);
		if (owner != null) {
			String message = getText(
					"reporting.send.notification.to.reviewed.text",
					new String[] { owner.getName(), directAccessUrl });

			SendMail emailMessage = new SendMail(config);
			emailMessage.send(owner.getEmail(), subject, message);
		}
	}
	
	private void checkFlagshipReportReadyness(String actId)
	{
		List<Integer> flagshipIds = flagshipFocalRegionManager.getFlaghipIdsByActivity(actId);
		LOG.debug("Checking flagships for report generation readyness");
		for(Integer flagshipId : flagshipIds)
		{
			LOG.debug("Checking flagship id [{}]", flagshipId);
			Map<String, Boolean> activityCacheMap = new HashMap<>();
			int year = config.getReportingCurrentYear();
			String period = "mid";
			
			if (!flagshipFocalRegionManager.checkReportAvailable(year,
					flagshipId, period)) {
				
				LOG.debug("Retriving cluster ids for flagship [{}]", flagshipId);
				boolean canInsert = true;
				List<Integer> clusterIds = flagshipFocalRegionManager
						.getClusterIds(flagshipId);
				for (Integer clusterId : clusterIds) {
					
					LOG.debug("Checking cluster id [{}]", clusterId);
					DataLookup cluster = dataLookUpManager.getLookUpdataById(
							LookupInfoEnum.ACTIVITY_CLUSTER,
							clusterId.longValue());

					LOG.debug(
							"Retriving data list for year [{}], flagship [{}], cluster [{}]",
							new String[] { String.valueOf(year),
									String.valueOf(flagshipId),
									cluster.getValue() });
					List<FlagshipReportData> reportDataList = flagshipFocalRegionManager
							.getFlagshipReportData(year, flagshipId,
									cluster.getValue());
					for (FlagshipReportData reportData : reportDataList) {
						String activityId = reportData.getActivityId();
						if (!activityCacheMap.containsKey(activityId)) {
							activityCacheMap.put(activityId, true);
							
							ActivityReviewRating midYearReview = activityManager
									.getMidYearRating(activityId);
							if(null == midYearReview || midYearReview.getStage() != ActivityReviewRating.STAGE_FINALIZED )
							{
								LOG.debug("Activity check failed because of report 1 not avilable for activity id [{}]", activityId);
								canInsert = false;
							}
						}
					}
				}

				if (canInsert) {
					
					LOG.debug("Activity check success for flagship id [{}]", flagshipId);
					FlagshipRptStatus flagshipRptStatus = new FlagshipRptStatus();
					flagshipRptStatus.setFlagshipId(flagshipId);
					flagshipRptStatus.setPeriod(period);
					flagshipRptStatus.setYear(year);
					flagshipFocalRegionManager
							.saveFlagshipRptStatus(flagshipRptStatus);
					sendFlagshipReportNotification(flagshipId);
				}
			} else 
			{
				LOG.debug("Reoport is already available for flagship id [{}]", flagshipId);
			}
		}
	}
	
	private void sendFlagshipReportNotification(int flagshipId) {

		FlagshipFocalRegion flagshipFocalRegion = flagshipFocalRegionMap.get(flagshipId);
		String subject = "[WLE Reporting] : Flaghip report available ["
				+ flagshipFocalRegion.getLongName() + "]";
		
		String reportUrl =  config.getBaseUrl() + "/reporting/flagshipReport.do?flagship="
				+ flagshipFocalRegion.getId()
				+"&period=mid"
				+"&year="+config.getReportingCurrentYear();

		String emailBody = "Flagship report available to save and submit."
				+ " please click below to open the report." 
				+ "\n" + reportUrl
				+ " \n\nKind regards, WLE Team\n";

		
		List<User> users = new ArrayList<>();
		int leaderAId = flagshipFocalRegion.getLeaderAId();
		if (0 != leaderAId) {
			users.add(userManager.getById(leaderAId));
		}
		int leaderBId = flagshipFocalRegion.getLeaderBId();
		if (0 != leaderBId) {
			users.add(userManager.getById(leaderBId));
		}
		SendMail sendMail = new SendMail(config);
		for (User user : users) {
			sendMail.send(user.getEmail(), subject, emailBody);
			LOG.info(
					"Sending flagship report available notification TO : {}, Message : {}",
					user, emailBody);
		}
		
		sendMail.send("D.RiderSmith@cgiar.org", subject, emailBody);
		LOG.info(
				"Sending flagship report available notification TO : {}, Message : {}",
				"D.RiderSmith@cgiar.org", emailBody);
	}

	/**
	 * <p>
	 * Getter for activityID.
	 * </p>
	 * 
	 * @return the activityID
	 */
	public int getActivityID() {
		return activityID;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public boolean isReviewSubmit() {
		return reviewSubmit;
	}

	public boolean isCanPublish() {
		return canPublish;
	}

	public boolean isCanSendDraft() {
		return canSendDraft;
	}

	public boolean isPublished() {
		return published;
	}

	public String getButtonAction() {
		return buttonAction;
	}

	public void setButtonAction(String buttonAction) {
		this.buttonAction = buttonAction;
	}

}
