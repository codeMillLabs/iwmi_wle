package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

import java.util.ArrayList;
import java.util.List;

public class ProjectDto {

	private String projectId;
	private String title;
	
	private List<RatingDto> ratings = new ArrayList<RatingDto>();
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public List<RatingDto> getRatings() {
		return ratings;
	}

	public void setRatings(List<RatingDto> ratings) {
		this.ratings = ratings;
	}
}
