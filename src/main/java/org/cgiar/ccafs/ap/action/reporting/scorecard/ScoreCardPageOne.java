package org.cgiar.ccafs.ap.action.reporting.scorecard;

import java.util.ArrayList;
import java.util.List;

public class ScoreCardPageOne {
	
	private StatusChartData clusterStatus;
	private StatusChartData projectStatus;
	
	private SpentChartData expenditure;
	
	private int activityClusterCount;
	private int activityCount;
	private int outputCount;
	private int clusterOutputCount;
	
	List<FlagshipSummaryCard> flagshipSummaries = new ArrayList<FlagshipSummaryCard>();

	/**
	 * @return the activityClusterStatus
	 */
	public StatusChartData getClusterStatus() {
		return clusterStatus;
	}

	/**
	 * @param clusterStatus the clusterStatus to set
	 */
	public void setClusterStatus(StatusChartData clusterStatus) {
		this.clusterStatus = clusterStatus;
	}

	/**
	 * @return the projectStatus
	 */
	public StatusChartData getProjectStatus() {
		return projectStatus;
	}

	/**
	 * @param projectStatus the projectStatus to set
	 */
	public void setProjectStatus(StatusChartData projectStatus) {
		this.projectStatus = projectStatus;
	}

	/**
	 * @return the expenditure
	 */
	public SpentChartData getExpenditure() {
		return expenditure;
	}

	/**
	 * @param expenditure the expenditure to set
	 */
	public void setExpenditure(SpentChartData expenditure) {
		this.expenditure = expenditure;
	}

	/**
	 * @return the activityClusterCount
	 */
	public int getActivityClusterCount() {
		return activityClusterCount;
	}

	/**
	 * @param activityClusterCount the activityClusterCount to set
	 */
	public void setActivityClusterCount(int activityClusterCount) {
		this.activityClusterCount = activityClusterCount;
	}

	/**
	 * @return the activityCount
	 */
	public int getActivityCount() {
		return activityCount;
	}

	/**
	 * @param activityCount the activityCount to set
	 */
	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
	}

	/**
	 * @return the outputCount
	 */
	public int getOutputCount() {
		return outputCount;
	}

	/**
	 * @param outputCount the outputCount to set
	 */
	public void setOutputCount(int outputCount) {
		this.outputCount = outputCount;
	}

	/**
	 * @return the flagshipSummaries
	 */
	public List<FlagshipSummaryCard> getFlagshipSummaries() {
		return flagshipSummaries;
	}

	/**
	 * @param flagshipSummaries the flagshipSummaries to set
	 */
	public void setFlagshipSummaries(List<FlagshipSummaryCard> flagshipSummaries) {
		this.flagshipSummaries = flagshipSummaries;
	}

	/**
	 * @return the clusterOutputCount
	 */
	public int getClusterOutputCount() {
		return clusterOutputCount;
	}

	/**
	 * @param clusterOutputCount the clusterOutputCount to set
	 */
	public void setClusterOutputCount(int clusterOutputCount) {
		this.clusterOutputCount = clusterOutputCount;
	}
}