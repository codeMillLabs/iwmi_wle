package org.cgiar.ccafs.ap.action.json.planning;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.CountryManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class CountryByISOAction  extends BaseAction  {

	private static final long serialVersionUID = -269112198169563443L;

	  // Logger
	  private static final Logger LOG = LoggerFactory.getLogger(CountryByISOAction.class);

	  // Model
	  private String iso;
	  private Country country;

	  // Managers
	  private CountryManager countryManager;

	  @Inject
	  public CountryByISOAction(APConfig config, LogframeManager logframeManager,
	    CountryManager countryManager) {
	    super(config, logframeManager);
	    this.countryManager = countryManager;
	  }

	  @Override
	  public String execute() throws Exception {
	    country = countryManager.getCountry(iso);

	    LOG.info("-- execute() > Country for iso '{}' was loaded.", iso);
	    return SUCCESS;
	  }

	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	  public void prepare() throws Exception {

	    // Verify if there is a activityID parameter
	    if (this.getRequest().getParameter(APConstants.ISO) == null) {
	    	iso = "";
	      return;
	    }

	    // If there is a parameter take its values
	    iso = StringUtils.trim(this.getRequest().getParameter(APConstants.ISO));
	  }
}
