/*
 * This file is part of CCAFS Planning and Reporting Platform.
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see <http://www.gnu.org/licenses/>.
 */

package org.cgiar.ccafs.ap.action.planning;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cgiar.ccafs.ap.action.BaseAction;
import org.cgiar.ccafs.ap.config.APConfig;
import org.cgiar.ccafs.ap.config.APConstants;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityPublicationManager;
import org.cgiar.ccafs.ap.data.manager.LogframeManager;
import org.cgiar.ccafs.ap.data.manager.SubmissionManager;
import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.Submission;
import org.cgiar.ccafs.ap.util.Capitalize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * @author Manuja
 */
public class PublicationPlanningAction extends BaseAction {

	private static final long serialVersionUID = 3642049826174430790L;

	// Logger
	private static final Logger LOG = LoggerFactory
			.getLogger(PublicationPlanningAction.class);

	// Managers
	private ActivityManager activityManager;
	private ActivityPublicationManager publicationManager;
	private SubmissionManager submissionManager;

	// Model
	private int activityID;
	private Activity activity;
	private boolean canSubmit;
	private StringBuilder validationMessage;
	private Map<String, String> yesNoOptions;

	@Inject
	public PublicationPlanningAction(APConfig config,
			LogframeManager logframeManager, ActivityManager activityManager,
			ActivityPublicationManager publicationManager,
			SubmissionManager submissionManager) {
		super(config, logframeManager);

		this.activityManager = activityManager;
		this.publicationManager = publicationManager;
		this.submissionManager = submissionManager;

		validationMessage = new StringBuilder();

		this.yesNoOptions = new LinkedHashMap<>();
		yesNoOptions.put("0", getText("form.options.yes"));
		yesNoOptions.put("1", getText("form.options.no"));
	}

	@Override
	public String next() {
		save();
		return super.next();
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();

		validationMessage = new StringBuilder();

		String activityStringID = StringUtils.trim(this.getRequest()
				.getParameter(APConstants.ACTIVITY_REQUEST_ID));
		try {
			activityID = Integer.parseInt(activityStringID);
		} catch (NumberFormatException e) {
			LOG.error(
					"-- prepare() > There was an error parsing the activity identifier '{}'.",
					activityStringID, e);
		}

		LOG.info(
				"-- prepare() > User {} load the materials for activity {} in planing section",
				getCurrentUser().getEmail(), activityID);

		activity = activityManager.getActivity(activityID);
		activity.setLoggedInUser(getCurrentUser());

		activity.setPublications(publicationManager
				.getActivityPublications(activityID));

		Submission submission = submissionManager.getSubmission(
				getCurrentUser().getLeader(), getCurrentPlanningLogframe(),
				APConstants.PLANNING_SECTION);
		canSubmit = (submission == null) ? true : false;

		if (getRequest().getMethod().equalsIgnoreCase("post")) {
			activity.getPublications().clear();
		}
	}

	@Override
	public String save() {
		int saved = -1;
		publicationManager.deleteActivityPublications(activityID);
		saved = publicationManager.saveActivityPublications(
				activity.getPublications(), activityID);
		if (saved > 0) {
			if (validationMessage.toString().isEmpty()) {
				addActionMessage(getText("saving.success",
						new String[] { getText("planning.activityPublications") }));
			} else {
				String finalMessage = getText("saving.success",
						new String[] { getText("planning.activityPublications") });
				finalMessage += getText("saving.keepInMind",
						new String[] { validationMessage.toString() });
				addActionWarning(Capitalize.capitalizeString(finalMessage));
			}

			// As there were changes in the activity we should mark the
			// validation as false
            if (activity.needValidation())
            {
                activity.setValidated(false);
                activityManager.validateActivity(activity);
            }
            else
            {
                activity.setValidated(true);
            }

			LOG.info(
					"-- save() > User {} save successfully the publications for activity {}",
					this.getCurrentUser().getEmail(), activityID);
			return SUCCESS;
		} else {
			LOG.info(
					"-- save() > User {} had problems to save the publications for activity {}",
					this.getCurrentUser().getEmail(), activityID);
			addActionError(getText("saving.problem"));
			return INPUT;
		}
	}

	@Override
	public void validate() {
		if (save) {
			// Remove the empty publications
			for (int c = 0; c < activity.getPublications().size(); c++) {
				if (activity.getPublications().get(c).getPublicationReference() == null || 
						activity.getPublications().get(c).getPublicationReference().isEmpty()) {
					activity.getPublications().remove(c);
					c--;
				} else {
					if (activity.getPublications().get(c).getPublicationReference() == null) {
						validationMessage
								.append(getText("planning.activityPublications.publicationReference"));
					}
				}
			}

			// Activity must have at least one publication
			if (activity.getPublications().isEmpty()) {
				validationMessage
						.append(getText("planning.activityPublications.validation.atLeastOne"));
			}
		}
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public boolean isCanSubmit() {
		return canSubmit;
	}

	public String getActivityRequestParameter() {
		return APConstants.ACTIVITY_REQUEST_ID;
	}

	public Map<String, String> getYesNoOptions() {
		return yesNoOptions;
	}

	public void setYesNoOptions(Map<String, String> yesNoOptions) {
		this.yesNoOptions = yesNoOptions;
	}
}
