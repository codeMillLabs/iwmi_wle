package org.cgiar.ccafs.ap.action.reporting.scorecard.cache;

public class ScoreCardMainPageFirstSection {

	private int id;
	private int clusterGreen;
	private int clusterYellow;
	private int clusterRed;
	private int projectGreen;
	private int projectYellow;
	private int projectRed;
	private int outputGreen;
	private int outputYellow;
	private int outputRed;
	private double spent;
	private double notSpent;
	private int clusterCount;
	private int projectCount;
	private int outputCount;
	private int clusterOutputCount;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getClusterGreen() {
		return clusterGreen;
	}
	
	public void setClusterGreen(int clusterGreen) {
		this.clusterGreen = clusterGreen;
	}
	
	public int getClusterYellow() {
		return clusterYellow;
	}
	
	public void setClusterYellow(int clusterYellow) {
		this.clusterYellow = clusterYellow;
	}
	
	public int getClusterRed() {
		return clusterRed;
	}
	
	public void setClusterRed(int clusterRed) {
		this.clusterRed = clusterRed;
	}
	
	public int getProjectGreen() {
		return projectGreen;
	}
	
	public void setProjectGreen(int projectGreen) {
		this.projectGreen = projectGreen;
	}
	
	public int getProjectYellow() {
		return projectYellow;
	}
	
	public void setProjectYellow(int projectYellow) {
		this.projectYellow = projectYellow;
	}
	
	public int getProjectRed() {
		return projectRed;
	}
	
	public void setProjectRed(int projectRed) {
		this.projectRed = projectRed;
	}
	
	public int getOutputGreen() {
		return outputGreen;
	}
	
	public void setOutputGreen(int outputGreen) {
		this.outputGreen = outputGreen;
	}
	
	public int getOutputYellow() {
		return outputYellow;
	}
	
	public void setOutputYellow(int outputYellow) {
		this.outputYellow = outputYellow;
	}
	
	public int getOutputRed() {
		return outputRed;
	}
	
	public void setOutputRed(int outputRed) {
		this.outputRed = outputRed;
	}
	
	public double getSpent() {
		return spent;
	}
	
	public void setSpent(double spent) {
		this.spent = spent;
	}
	
	public double getNotSpent() {
		return notSpent;
	}
	
	public void setNotSpent(double notSpent) {
		this.notSpent = notSpent;
	}
	
	public int getClusterCount() {
		return clusterCount;
	}
	
	public void setClusterCount(int clusterCount) {
		this.clusterCount = clusterCount;
	}
	
	public int getProjectCount() {
		return projectCount;
	}
	
	public void setProjectCount(int projectCount) {
		this.projectCount = projectCount;
	}
	
	public int getOutputCount() {
		return outputCount;
	}
	
	public void setOutputCount(int outputCount) {
		this.outputCount = outputCount;
	}
	
	public int getClusterOutputCount() {
		return clusterOutputCount;
	}
	
	public void setClusterOutputCount(int clusterOutputCount) {
		this.clusterOutputCount = clusterOutputCount;
	}
}