package org.cgiar.ccafs.ap.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.ActivityReviewRating;
import org.cgiar.ccafs.ap.data.model.ActivityReviewRatingComment;
import org.cgiar.ccafs.ap.data.model.User;
import org.cgiar.ccafs.ap.data.model.User.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivityClusterReportCommentServlet extends AbstractHttpServlet {
	private static final long serialVersionUID = -6947679951012127272L;
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityClusterReportCommentServlet.class);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Load data for the UI;
		String activityId = request.getParameter("activityId");
		String email = request.getParameter("user");
		LOG.info("Request came to load the Activity cluster Report review : [ activityId : "
				+ activityId + "," + " user :" + email + "]");

		loadData(request, activityId, email);

		request.getRequestDispatcher("./jsp/cluster-review-comments.jsp")
				.forward(request, response);
	}

	private void loadData(HttpServletRequest request, String activityId,
			String email) {
		User loggedInUser = userManager.getUser(email);
		boolean editable = false;

		ActivityReviewRating rating = activityManager
				.getMidYearRating(activityId);
		List<ActivityReviewRatingComment> comments = new ArrayList<ActivityReviewRatingComment>();
		if (rating != null) {
			rating.setLoggedInUser(loggedInUser);
			comments = activityManager.getMidYearReviewRatingComment(rating
					.getId());
			editable = (rating.getStage() == 2 && loggedInUser
					.hasRole(UserRole.FL)) ? true : false;
			request.setAttribute("reportId", rating.getId());
			
			Collections.sort(comments);
		}
		request.setAttribute("comments", comments);
		request.setAttribute("email", loggedInUser.getEmail());
		request.setAttribute("name", loggedInUser.getName());
		request.setAttribute("editable", editable);
		request.setAttribute("activityId", activityId);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String activityId = request.getParameter("activityId");
		int reportId = Integer.valueOf(request.getParameter("reportId"));
		String email = request.getParameter("email");
		String commentStr = request.getParameter("comment");

		LOG.debug("Request came to save a MidYearReviewRatingComment : [ Report Id : "
				+ reportId
				+ " user :"
				+ email
				+ ", Comment :"
				+ commentStr
				+ "]");
		ActivityReviewRatingComment comment = new ActivityReviewRatingComment();
		comment.setDate(new Date());
		comment.setReviewer(email);
		comment.setComment(commentStr);

		activityManager.saveMidYearReviewRatingComment(reportId, comment);

		LOG.debug(" MidYearReviewRatingComment saved: [ Report Id : "
				+ reportId + " user :" + email + ", Comment :" + commentStr
				+ "]");

		loadData(request, activityId, email);
		request.getRequestDispatcher("./jsp/cluster-review-comments.jsp")
				.forward(request, response);
	}
}
