package org.cgiar.ccafs.ap.servlet;

import static org.cgiar.ccafs.ap.util.WleAppUtil.createEvenCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createHeaderCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createOddCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createWritableWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.write.WriteException;

import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityKeyword;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.ContactPerson;
import org.cgiar.ccafs.ap.data.model.Region;
import org.cgiar.ccafs.ap.util.WleAppUtil;
import org.cgiar.ccafs.ap.util.XLSCell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivityAllSummaryRptServlet extends AbstractHttpServlet {

	private static final long serialVersionUID = -5168961613445179385L;
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityAllSummaryRptServlet.class);

	private static final String csvPath = WleAppUtil.getResourceBundle()
			.getString("csv.file.location");

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {

			int year = (request.getParameter("year") != null) ? Integer
					.parseInt(request.getParameter("year")) : -1;

			if (year != -1) {
				Activity[] activityList = activityManager
						.getActivityListByYear(year);

				if (activityList == null || activityList.length == 0)
					return;
				
				String fileName = generateWorkBook(activityList);

				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=\""
						+ fileName + "\"");

				OutputStream out = response.getOutputStream();
				FileInputStream in = new FileInputStream(csvPath + fileName);
				byte[] buffer = new byte[4096];
				int length;
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			} else {
				return;
			}

		} catch (Exception e) {
			LOG.error(
					" generateReport() -> Error occurred in Report Generation,",
					e);
		}
	}

	private String generateWorkBook(Activity[] activityList)
			throws WriteException, IOException {
		List<XLSCell> xlsData = new ArrayList<>();
		xlsData.add(createHeaderCell(0,0, "ID"));
		xlsData.add(createHeaderCell(1,0, "Lead Center"));
		xlsData.add(createHeaderCell(2,0, "Title"));
		xlsData.add(createHeaderCell(3,0, "Description"));
		xlsData.add(createHeaderCell(4,0, "Flagship"));
		xlsData.add(createHeaderCell(5,0, "Start Date"));
		xlsData.add(createHeaderCell(6,0, "End Date"));
		xlsData.add(createHeaderCell(7,0, "Contact Persons"));
		xlsData.add(createHeaderCell(8,0, "Partners"));
		xlsData.add(createHeaderCell(9,0, "Regions"));
		xlsData.add(createHeaderCell(10,0, "Other Keywords"));
		
		int index = 1;
		for (Activity activity : activityList) {
			int activityId = activity.getId();
			activity = activityManager
					.getActivityStatusInfo(activityId);

			String formattedId = checkNull(activity.getFormattedId());
			String leadCenter = checkNull(activity.getLeadCenter());
			String title = checkNull(activity.getTitle());
			String description = checkNull(activity.getDescription());
			String flagshipSRP = checkNull(activity.getFlagShipSRP());
			String startDate = formatDate(activity.getStartDate());
			String endDate = formatDate(activity.getEndDate());
			String contactPersonList = replaceNull(createContactPersionList(activityId));
			String partnerList = replaceNull(createPartnersList(activityId));
			String regionData = replaceNull(createRegionData(activity));
			String otherKeywordList = replaceNull(createOtherKeywordList(activityId));
			
			if(index % 2 == 0)
			{
				xlsData.add(createOddCell(0,index, formattedId));
				xlsData.add(createOddCell(1,index, leadCenter));
				xlsData.add(createOddCell(2,index, title));
				xlsData.add(createOddCell(3,index, description));
				xlsData.add(createOddCell(4,index, flagshipSRP));
				xlsData.add(createOddCell(5,index, startDate));
				xlsData.add(createOddCell(6,index, endDate));
				xlsData.add(createOddCell(7,index, contactPersonList));
				xlsData.add(createOddCell(8,index, partnerList));
				xlsData.add(createOddCell(9,index, regionData));
				xlsData.add(createOddCell(10,index, otherKeywordList));
			}
			else
			{
				xlsData.add(createEvenCell(0,index, formattedId));
				xlsData.add(createEvenCell(1,index, leadCenter));
				xlsData.add(createEvenCell(2,index, title));
				xlsData.add(createEvenCell(3,index, description));
				xlsData.add(createEvenCell(4,index, flagshipSRP));
				xlsData.add(createEvenCell(5,index, startDate));
				xlsData.add(createEvenCell(6,index, endDate));
				xlsData.add(createEvenCell(7,index, contactPersonList));
				xlsData.add(createEvenCell(8,index, partnerList));
				xlsData.add(createEvenCell(9,index, regionData));
				xlsData.add(createEvenCell(10,index, otherKeywordList));
			}
			
			index ++;
		}

		String fileName = "All_Activity_Details_Summary_"
				+ sdf.format(Calendar.getInstance().getTime()) + ".xls";
		String excel = csvPath + fileName;
		createWritableWorkbook(excel, "Activity Summary", xlsData, 11);
		return fileName;
	}

	private String createContactPersionList(int activityId) {
		StringBuffer contactDetailsString = new StringBuffer();
		for (ContactPerson cp : contactPersonManager
				.getContactPersons(activityId)) {
			if (contactDetailsString.length() > 0) {
				contactDetailsString.append(",");
			}

			contactDetailsString.append(cp.getName());
			contactDetailsString.append("(");
			contactDetailsString.append(cp.getEmail());
			contactDetailsString.append(")");
		}
		return contactDetailsString.toString();
	}

	private String createOtherKeywordList(final int activityId) {
		StringBuffer otherKeywordString = new StringBuffer();
		for (ActivityKeyword activityKeyword : activityKeywordManager
				.getKeywordList(activityId)) {
			if (activityKeyword.getOther() != null) {
				if (otherKeywordString.length() > 0) {
					otherKeywordString.append(",");
				}

				otherKeywordString.append(activityKeyword.getOther());
			}
		}
		return otherKeywordString.toString();
	}

	private String createPartnersList(final int activityId) {
		StringBuffer partnerString = new StringBuffer();
		for (ActivityPartner activityPartner : activityPartnerManager
				.getActivityPartners(activityId)) {

			if (partnerString.length() > 0) {
				partnerString.append(",");
			}

			partnerString.append(activityPartner.getContactName());
		}
		return partnerString.toString();
	}

	private String createRegionData(Activity activity) {
		int activityId = activity.getId();
		activity.setWles(activityWLEManager.getWLEList(activityId));
		activity.setCountries(activityCountryManager
				.getActvitiyCountries(activityId));
		activity.setRegions(activityRegionManager
				.getActvitiyRegions(activityId));
		activity.setBsLocations(activityBenchmarkSiteManager
				.getActivityBenchmarkSites(activityId));
		activity.setOtherLocations(activityOtherSiteManager
				.getActivityOtherSites(activityId));
		Region[] regions = regionManager.getRegionList();
		StringBuffer regionsList = new StringBuffer();

		for (Region region : regions) {
			if (activity.getRegionsIds().contains(
					String.valueOf(region.getId()))) {
				if (regionsList.length() > 0) {
					regionsList.append(", ");
				}
				regionsList.append(region.getName());
			} else if ((activity.getCountriesIdsByRegion(region.getId()).size() > 0)) {
				if (regionsList.length() > 0) {
					regionsList.append(", ");
				}
				regionsList.append(region.getName());

				StringBuffer countries = new StringBuffer();
				for (String country : activity.getCountriesNamesByRegion(region
						.getId())) {
					if (countries.length() > 0) {
						countries.append(", ");
					}

					countries.append(country);
				}
				regionsList.append("(");
				regionsList.append(countries.toString());
				regionsList.append(")");
			}
		}
		return regionsList.toString();
	}

	private String formatDate(final Date date) {
		if (null != date) {
			return sdfCSV.format(date);
		}

		return "";
	}

	private String checkNull(final String stringValue) {
		if (null != stringValue) {
			return stringValue;
		}

		return "";
	}

	private String replaceNull(final String stringValue) {
		return stringValue.replace("null", "");
	}

}
