package org.cgiar.ccafs.ap.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.Keyword;
import org.cgiar.ccafs.ap.data.model.RowStatus;
import org.cgiar.ccafs.ap.data.model.TokenAction;

public class ApproveKeywordServlet extends AbstractHttpServlet {
	private static final long serialVersionUID = -6947679951012127272L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String[] keywordIds = request.getParameterValues("keywordList");
		String tokenId = request.getParameter("tokenId");

		for (String keywordId : keywordIds) {
			Keyword keyword = keywordManager.getKeyword(keywordId);
			keyword.setStatus(RowStatus.VERIFIED);

			keywordManager.updateKeyword(keyword);
		}

		TokenAction tokenAction = tokenActionManager.getTokenAction(tokenId);
		tokenAction.setUsed(true);
		tokenActionManager.updateToken(tokenAction);

		request.getRequestDispatcher("./jsp/approve-keyword-success.jsp")
				.forward(request, response);
	}
}
