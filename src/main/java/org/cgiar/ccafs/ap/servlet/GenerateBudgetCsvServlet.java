package org.cgiar.ccafs.ap.servlet;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.reportengine.planning.BudgetDetailsCSVReportService;
import org.cgiar.ccafs.ap.util.WleAppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * <p>
 * Generate Budget CSV Servlet.
 * </p>
 * 
 * @author Manuja
 */
public class GenerateBudgetCsvServlet extends HttpServlet
{
    private static final long serialVersionUID = -5552888699163367726L;
    private static final Logger LOG = LoggerFactory.getLogger(GenerateBudgetCsvServlet.class);
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final String csvPath = WleAppUtil.getResourceBundle().getString("csv.file.location");
    private BudgetDetailsCSVReportService csvReportService = null;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            LOG.debug("Request came to generate CSV ");

            int year = (request.getParameter("year") != null) ? Integer.parseInt(request.getParameter("year")) : -1;
            int activityId =
                (request.getParameter("activityId") != null) ? Integer.parseInt(request.getParameter("activityId"))
                    : -1;
            String flagship = request.getParameter("flagShipSRP");
            String cluster = request.getParameter("activityCluster");
            String leadCenter = request.getParameter("leadCenter");

            flagship = (flagship == null || flagship.isEmpty()) ? null : flagship;
            cluster = (cluster == null || cluster.isEmpty()) ? null : cluster;
            leadCenter = (leadCenter == null || leadCenter.isEmpty()) ? null : leadCenter;
            
			LOG.info("Request [Year : {" + year + "}, Activity Id : {"
					+ activityId + "}, Lead Center : {"  + leadCenter + "}, Flagship : {" + flagship
					+ "}, Activity Cluster : {" + cluster + "}]");

			List<String[]> data = getCSVReportService().getCSVData(year,
					activityId, leadCenter, flagship, cluster);

            String fileNamePrefix = "Activity_Budget_";
            if(flagship != null || cluster != null) {
                fileNamePrefix = "Activity_Grouped_Summary_";
            }
            String fileName = writeCSVData(fileNamePrefix, data);

            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            OutputStream out = response.getOutputStream();
            FileInputStream in = new FileInputStream(csvPath + fileName);
            byte[] buffer = new byte[4096];
            int length;
            while ((length = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, length);
            }
            in.close();
            out.flush();
        }
        catch (Exception e)
        {
            LOG.error("Error occurred while generating csv file,", e);
        }
    }

    private BudgetDetailsCSVReportService getCSVReportService()
    {
        if (csvReportService == null)
        {
            csvReportService = new BudgetDetailsCSVReportService();
        }
        return csvReportService;
    }

    private String writeCSVData(String filenamePrefix, List<String[]> data) throws IOException
    {
        String fileName = filenamePrefix + sdf.format(Calendar.getInstance().getTime()) + ".csv";
        String csv = csvPath + fileName;
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csv), ',');
        csvWriter.writeAll(data);
        csvWriter.close();
        return fileName;
    }
}
