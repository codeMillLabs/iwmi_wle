package org.cgiar.ccafs.ap.servlet;

import static org.cgiar.ccafs.ap.util.WleAppUtil.createHeaderCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createOddCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createEvenCell;
import static org.cgiar.ccafs.ap.util.WleAppUtil.createWritableWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jxl.write.WriteException;

import org.cgiar.ccafs.ap.data.model.PartnerRptDto;
import org.cgiar.ccafs.ap.util.WleAppUtil;
import org.cgiar.ccafs.ap.util.XLSCell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Reporting Viewer Servlet.
 * </p>
 * 
 * @author Manuja
 */
public class PartnerReportServlet extends AbstractHttpServlet {

	private static final long serialVersionUID = 6730204279357258587L;
	private static final Logger LOG = LoggerFactory
			.getLogger(PartnerReportServlet.class);

	private static final String csvPath = WleAppUtil.getResourceBundle()
			.getString("csv.file.location");

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		try {
			String fileName = generateWorkBook();

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ fileName + "\"");

			OutputStream out = response.getOutputStream();
			FileInputStream in = new FileInputStream(csvPath + fileName);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();
		} catch (Exception e) {
			LOG.error(
					" generateReport() -> Error occurred in Report Generation,",
					e);
		}
	}

	private String generateWorkBook() throws IOException, WriteException {
		String fileName = "Partner_Summary_"
				+ sdf.format(Calendar.getInstance().getTime()) + ".xls";
		String excel = csvPath + fileName;
		
		PartnerRptDto[] partners = partnerManager.getAllPatnersForReport();
		List<XLSCell> xlsData = new ArrayList<>();
		xlsData.add(createHeaderCell(0,0, "Partner Name"));
		xlsData.add(createHeaderCell(1,0, "Acronym"));
		xlsData.add(createHeaderCell(2,0, "Country"));
		xlsData.add(createHeaderCell(3,0, "Type"));
		xlsData.add(createHeaderCell(4,0, "Classification"));
		xlsData.add(createHeaderCell(5,0, "Contact"));
		
		int index = 1;
		for(PartnerRptDto partnerRptDto : partners)
		{
			String name = partnerRptDto.getName();
			String acronym = partnerRptDto.getAcronym();
			String country = partnerRptDto.getCountry();
			String type = partnerRptDto.getType();
			String classification = partnerRptDto.getClassification();
			String contact = partnerRptDto.getContact();
			
			if(index % 2 == 0)
			{
				xlsData.add(createOddCell(0,index, name));
				xlsData.add(createOddCell(1,index, acronym));
				xlsData.add(createOddCell(2,index, country));
				xlsData.add(createOddCell(3,index, type));
				xlsData.add(createOddCell(4,index, classification));
				xlsData.add(createOddCell(5,index, contact));
			}
			else
			{
				xlsData.add(createEvenCell(0,index, name));
				xlsData.add(createEvenCell(1,index, acronym));
				xlsData.add(createEvenCell(2,index, country));
				xlsData.add(createEvenCell(3,index, type));
				xlsData.add(createEvenCell(4,index, classification));
				xlsData.add(createEvenCell(5,index, contact));
			}
			index ++;
		}
		createWritableWorkbook(excel, "Partners", xlsData, 6);
		return fileName;
	}
}
