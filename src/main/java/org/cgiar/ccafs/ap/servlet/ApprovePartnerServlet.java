package org.cgiar.ccafs.ap.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.Country;
import org.cgiar.ccafs.ap.data.model.Partner;
import org.cgiar.ccafs.ap.data.model.PartnerType;
import org.cgiar.ccafs.ap.data.model.RowStatus;
import org.cgiar.ccafs.ap.data.model.TokenAction;

public class ApprovePartnerServlet extends AbstractHttpServlet {
	private static final long serialVersionUID = -4124095067240870398L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("partnerId");
		String name = request.getParameter("partnerName");
		String acronym = request.getParameter("acronym");
		String partnerTypeId = request.getParameter("partnerTypeId");
		String countryId = request.getParameter("countryId");
		String website = request.getParameter("partnerWebPage");
		String createdBy = request.getParameter("createdBy");
		String tokenId = request.getParameter("tokenId");

		Partner partner = new Partner();
		partner.setId(Integer.parseInt(id));
		partner.setName(name);
		partner.setAcronym(acronym);
		partner.setWebsite(website);
		partner.setCreatedBy(createdBy);
		partner.setStatus(RowStatus.VERIFIED);

		PartnerType partnerType = new PartnerType();
		partnerType.setId(Integer.parseInt(partnerTypeId));
		partner.setType(partnerType);

		Country country = new Country();
		country.setId(countryId);
		partner.setCountry(country);
		partnerManager.udpatePartner(partner);

		TokenAction tokenAction = tokenActionManager.getTokenAction(tokenId);
		tokenAction.setUsed(true);
		tokenActionManager.updateToken(tokenAction);

		request.getRequestDispatcher("./jsp/approve-partner-success.jsp")
				.forward(request, response);
	}
}
