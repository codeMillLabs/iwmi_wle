/**
 * 
 */
package org.cgiar.ccafs.ap.servlet;

import static org.cgiar.ccafs.ap.data.model.ActivityReview.EXTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityReview.INTERNAL_TYPE;
import static org.cgiar.ccafs.ap.data.model.ActivityStatus.INTERNAL_REVIEWING;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityStatus;
import org.cgiar.ccafs.ap.data.model.Review;
import org.cgiar.ccafs.ap.data.model.Review.Type;
import org.cgiar.ccafs.ap.data.model.SectionReview;
import org.cgiar.ccafs.ap.data.model.SectionReview.Category;
import org.cgiar.ccafs.ap.data.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Amila Silva
 *
 */
public class ReviewRequestServlet extends AbstractHttpServlet
{
    private static final long serialVersionUID = -6947679951012127272L;
    private static final Logger LOG = LoggerFactory.getLogger(ReviewRequestServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        // Load data for the UI;
        int id = Integer.valueOf(request.getParameter("cid"));
        String email = request.getParameter("cemail");
        Category category = Category.getValue(request.getParameter("ccategory"));

        LOG.info("Request came to load the Activity reviews : [ Activty Id : " + id + "," + " user :" + email
            + ", Category :" + category + "]");

        loadData(request, id, email, category);

        request.getRequestDispatcher("./jsp/activity-comments.jsp").forward(request, response);
    }

    private void loadData(HttpServletRequest request, int id, String email, Category category)
    {
        User user = userManager.getUser(email);

        Activity activity = activityManager.getSimpleActivity(id);
        activity.setLoggedInUser(user);

        LOG.debug("Request came to load the Activity reviews : [ Activty Id : " + id + "," + " user :" + email
            + ", Revision Id :" + activity.getRevisionId() + "]");
        
        activity.setInternalReviewers(activityReviewerDAO.getReviewers(id, activity.getRevisionId(), INTERNAL_TYPE));
        activity.setExternalReviewers(activityReviewerDAO.getReviewers(id, activity.getRevisionId(), EXTERNAL_TYPE));

        boolean isEditable = true;
        boolean showIntReviews = true;

        switch (activity.getActivityStatus())
        {
            case INTERNAL_REVIEWING: {
                showIntReviews = true;
                isEditable = activity.isEditable();
                LOG.info(" REQUEST INTERNAL_REVIEWING ACCCESS  INTERNAL_REVIEWING : [ Activty Id : " + id + "," + " isEditable  :" + isEditable);
                break;
            }
            case REVIEWING: {
                showIntReviews = false;
                LOG.info(" REQUEST REVIEWING ACCCESS  REVIEWING : [ Activty Id : " + id + "," + " isEditable  :" + isEditable);
                isEditable = activity.isEditable();
                break;
            }
            case IN_PROGRESS:
            case VALIDATED:
            case SUBMITTED:
            case REVISION:
            case COMPLETED: {
                isEditable = false;
                showIntReviews = true;
                break;
            }

            default: {
                showIntReviews = true;
                isEditable = false;
            }
        }

        SectionReview sectionReview = activityManager.fetchReview(id, activity.getRevisionId(), category);
        sectionReview.setActivityId(id);
        sectionReview.setCurrentRevision(activity.getRevisionId());

        request.setAttribute("sectionReview", sectionReview);
        request.setAttribute("editable", isEditable);
        request.setAttribute("showIntReviews", showIntReviews);
        request.setAttribute("email", email);
        request.setAttribute("category_display", category.getDisplay());
        request.setAttribute("category_name", category.getColumnName());
        request.setAttribute("status", activity.getActivityStatus().getName());

        LOG.info("<< Attributes to Load review popup :  [ Activty Id : " + id + ", Revision : "
            + activity.getRevisionId() + "," + " user :" + email + ", Category :" + category + ", Can Edit : "
            + isEditable + ", showIntReviews = " + showIntReviews + "]");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
        IOException
    {

        int id = Integer.valueOf(request.getParameter("id"));
        String revisionId = request.getParameter("revisionId");
        String email = request.getParameter("email");
        Category category = Category.getValue(request.getParameter("category"));
        //        String commentId = request.getParameter("commentId");
        String comment = request.getParameter("comment");
        ActivityStatus status = ActivityStatus.getValue(request.getParameter("status"));

        LOG.debug("Request came to save a review : [ Activty Id : " + id + ", Revision : " + revisionId + ","
            + " user :" + email + ", Category :" + category + ", Comment :" + comment + ", Status :" + status  + "]");

        SectionReview sectionReview = activityManager.fetchReview(id, revisionId, category);
        User user = userManager.getUser(email);

        Review review = new Review();
        review.setRevisionId(revisionId);
        review.setReviewerId(user.getId());
        review.setReviewerName(user.getName());
        review.setComment(comment);
        review.setDate(Calendar.getInstance().getTime());
        Type type = (status == INTERNAL_REVIEWING) ? Review.Type.INTERNAL : Review.Type.EXTERNAL;
        review.setType(type);

        sectionReview.getReviews().add(review);
        Collections.sort(sectionReview.getReviews());

        int i = 1;
        for (Review rev : sectionReview.getReviews())
        {
            rev.setId(i++);
        }
        activityManager.updateReview(id, revisionId, category, sectionReview);

        LOG.info("Review data has been updated : [ Activty Id : " + id + ", Revision : " + revisionId + "," + " user :"
            + email + ", Category :" + category + "]");

        loadData(request, id, email, category);
        request.getRequestDispatcher("./jsp/activity-comments.jsp").forward(request, response);
    }
}
