/**
 * 
 */
package org.cgiar.ccafs.ap.servlet;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServlet;

import org.cgiar.ccafs.ap.data.dao.ActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.dao.DAOManager;
import org.cgiar.ccafs.ap.data.dao.mysql.MYSQLTokenActionDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityBenchmarkSiteDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityBudgetDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityCountryDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityKeywordDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityObjectiveDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityOtherSiteDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityPartnerDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityRegionDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityReviewerDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLActivityWLEDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLContactPersonDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLDAOManager;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLDeliverableDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLFileFormatDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLFlagshipFocalRegionDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLFlagshipFocalRegionReportDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLKeywordDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLLeaderDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLMilestoneDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLPartnerDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLRegionDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLResourceDAO;
import org.cgiar.ccafs.ap.data.dao.mysql.MySQLUserDAO;
import org.cgiar.ccafs.ap.data.manager.ActivityBenchmarkSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityBudgetManager;
import org.cgiar.ccafs.ap.data.manager.ActivityCountryManager;
import org.cgiar.ccafs.ap.data.manager.ActivityKeywordManager;
import org.cgiar.ccafs.ap.data.manager.ActivityManager;
import org.cgiar.ccafs.ap.data.manager.ActivityObjectiveManager;
import org.cgiar.ccafs.ap.data.manager.ActivityOtherSiteManager;
import org.cgiar.ccafs.ap.data.manager.ActivityPartnerManager;
import org.cgiar.ccafs.ap.data.manager.ActivityRegionManager;
import org.cgiar.ccafs.ap.data.manager.ActivityWLEManager;
import org.cgiar.ccafs.ap.data.manager.ContactPersonManager;
import org.cgiar.ccafs.ap.data.manager.DeliverableManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionManager;
import org.cgiar.ccafs.ap.data.manager.FlagshipFocalRegionReportManager;
import org.cgiar.ccafs.ap.data.manager.KeywordManager;
import org.cgiar.ccafs.ap.data.manager.LeaderManager;
import org.cgiar.ccafs.ap.data.manager.MilestoneManager;
import org.cgiar.ccafs.ap.data.manager.PartnerManager;
import org.cgiar.ccafs.ap.data.manager.RegionManager;
import org.cgiar.ccafs.ap.data.manager.ResourceManager;
import org.cgiar.ccafs.ap.data.manager.TokenActionManager;
import org.cgiar.ccafs.ap.data.manager.UserManager;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityBenchmarkSiteManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityBudgetManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityCountryManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityKeywordManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityObjectiveManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityOtherSiteManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityPartnerManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityRegionManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ActivityWLEManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ContactPersonManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.DeliverableManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.FlagshipFocalRegionManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.FlagshipFocalRegionReportManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.KeywordManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.LeaderManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.MilestoneManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.PartnerManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.RegionManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.ResourceManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.TokenActionManagerImpl;
import org.cgiar.ccafs.ap.data.manager.impl.UserManagerImp;
import org.cgiar.ccafs.ap.util.PropertiesManager;

/**
 * @author asilva
 * 
 */
public abstract class AbstractHttpServlet extends HttpServlet {

	private static final long serialVersionUID = 3729241798118399834L;

	protected DAOManager databaseManager = new MySQLDAOManager(
			new PropertiesManager());

	protected DeliverableManager deliverableManager = new DeliverableManagerImpl(
			new MySQLDeliverableDAO(databaseManager), new MySQLFileFormatDAO(
					databaseManager));

	protected ActivityPartnerManager activityPartnerManager = new ActivityPartnerManagerImpl(
			new MySQLActivityPartnerDAO(databaseManager));

	protected ContactPersonManager contactPersonManager = new ContactPersonManagerImpl(
			new MySQLContactPersonDAO(databaseManager));

	protected MilestoneManager milestoneManager = new MilestoneManagerImpl(
			new MySQLMilestoneDAO(databaseManager));

	protected ActivityObjectiveManager activityObjectiveManager = new ActivityObjectiveManagerImpl(
			new MySQLActivityObjectiveDAO(databaseManager));

	protected ActivityCountryManager activityCountryManager = new ActivityCountryManagerImpl(
			new MySQLActivityCountryDAO(databaseManager));

	protected ActivityBenchmarkSiteManager activityBenchmarkSiteManager = new ActivityBenchmarkSiteManagerImpl(
			new MySQLActivityBenchmarkSiteDAO(databaseManager));

	protected ActivityOtherSiteManager activityOtherSiteManager = new ActivityOtherSiteManagerImpl(
			new MySQLActivityOtherSiteDAO(databaseManager));

	protected UserManager userManager = new UserManagerImp(new MySQLUserDAO(
			databaseManager));

	protected LeaderManager leaderManager = new LeaderManagerImpl(
			new MySQLLeaderDAO(databaseManager), userManager);

	protected ActivityReviewerDAO activityReviewerDAO = new MySQLActivityReviewerDAO(
			databaseManager);

	protected ActivityBudgetManager activityBudgetManager = new ActivityBudgetManagerImpl(
			new MySQLActivityBudgetDAO(databaseManager));

	protected ActivityManager activityManager = new ActivityManagerImpl(
			new MySQLActivityDAO(databaseManager), deliverableManager,
			activityPartnerManager, contactPersonManager, milestoneManager,
			activityObjectiveManager, leaderManager, activityCountryManager,
			activityBenchmarkSiteManager, activityOtherSiteManager,
			activityReviewerDAO);

	protected ActivityKeywordManager activityKeywordManager = new ActivityKeywordManagerImpl(
			new MySQLActivityKeywordDAO(databaseManager));

	protected ActivityWLEManager activityWLEManager = new ActivityWLEManagerImpl(
			new MySQLActivityWLEDAO(databaseManager));

	protected ActivityRegionManager activityRegionManager = new ActivityRegionManagerImpl(
			new MySQLActivityRegionDAO(databaseManager));

	protected RegionManager regionManager = new RegionManagerImpl(
			new MySQLRegionDAO(databaseManager));

	protected TokenActionManager tokenActionManager = new TokenActionManagerImpl(
			new MYSQLTokenActionDAO(databaseManager));

	protected KeywordManager keywordManager = new KeywordManagerImpl(
			new MySQLKeywordDAO(databaseManager));

	protected PartnerManager partnerManager = new PartnerManagerImpl(
			new MySQLPartnerDAO(databaseManager), new MySQLActivityPartnerDAO(
					databaseManager));

	protected ResourceManager resourceManager = new ResourceManagerImpl(
			new MySQLResourceDAO(databaseManager));

	protected FlagshipFocalRegionReportManager flagshipReportManager = new FlagshipFocalRegionReportManagerImpl(
			new MySQLFlagshipFocalRegionReportDAO(databaseManager),
			new MySQLUserDAO(databaseManager));
	
	protected FlagshipFocalRegionManager flagshipFocalRegionManager = new FlagshipFocalRegionManagerImpl(
			new MySQLFlagshipFocalRegionDAO(databaseManager));

	protected static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	protected static final SimpleDateFormat sdfCSV = new SimpleDateFormat(
			"yyyy-MM-dd");
}
