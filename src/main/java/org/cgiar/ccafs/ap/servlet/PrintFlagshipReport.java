package org.cgiar.ccafs.ap.servlet;

import static org.cgiar.ccafs.ap.util.WleAppUtil.MID_YEAR_TYPE;
import static org.cgiar.ccafs.ap.util.WleAppUtil.YEAR_END_TYPE;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.ActivityClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutput;
import org.cgiar.ccafs.ap.data.model.ClusterOutputDetail;
import org.cgiar.ccafs.ap.data.model.FlagshipCluster;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegion;
import org.cgiar.ccafs.ap.data.model.FlagshipFocalRegionReport;
import org.cgiar.ccafs.ap.util.WleAppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

/**
 * <p>
 * Generate Budget CSV Servlet.
 * </p>
 * 
 * @author Manuja
 */
public class PrintFlagshipReport extends AbstractHttpServlet {
	private static final long serialVersionUID = 8962632372126915205L;
	private static final Logger LOG = LoggerFactory
			.getLogger(PrintFlagshipReport.class);
	private Map<Integer, FlagshipFocalRegion> flagshipFocalRegionMap = new HashMap<Integer, FlagshipFocalRegion>();
	private static final String csvPath = WleAppUtil.getResourceBundle()
			.getString("csv.file.location");

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			LOG.debug("Request received to print flagship report");

			int year = (request.getParameter("year") != null) ? Integer
					.parseInt(request.getParameter("year")) : -1;
			int flagshipId = (request.getParameter("flagshipId") != null) ? Integer
					.parseInt(request.getParameter("flagshipId")) : -1;

			String period = request.getParameter("period") != null ? request
					.getParameter("period") : "";

			FlagshipFocalRegionReport flagshipReport = flagshipReportManager
					.getReport(flagshipId, year, period);

			List<FlagshipFocalRegion> focalRegions = this.flagshipFocalRegionManager
					.getFlagshipFocalRegions();
			for (FlagshipFocalRegion focalRegion : focalRegions) {
				flagshipFocalRegionMap.put(focalRegion.getId(), focalRegion);
			}

			FlagshipFocalRegion flagshipFocalRegion = focalRegions
					.get(flagshipReport.getFlagshipId());

			String periodStart = "";
			String periodEnd = "";

			if (period.equals("mid")) {
				periodStart = "01 January " + year;
				periodEnd = "30 June " + year;
				period = MID_YEAR_TYPE;
			} else {
				periodStart = "01 July " + year;
				periodEnd = "31 December " + year;
				period = YEAR_END_TYPE;
			}

			String timeStamp = String.valueOf(System.currentTimeMillis());
			String filePath = csvPath + timeStamp + ".html";
			String fileName = timeStamp + ".pdf";
			PrintWriter out = new PrintWriter(filePath, "UTF-8");
			// PrintWriter out = new PrintWriter(response.getOutputStream());
			out.println("<html>");

			// #### HEAD #### //
			out.println("<head>");
			out.println("<style>");
			out.println(".flagship-first_col{background:#d0dbe9;}");
			out.println(".autofil_bg{background: #ADD6FF;}");
			out.println("textarea{width: 100%;height: 70px;margin: 3px 10px 3px 10px;}");
			out.println("table{border:#000 1px solid;margin-bottom:20px;border-collapse: collapse;}");
			out.println("td{border:#000 1px solid;padding:5px;}");
			out.println("th{border:#000 1px solid;padding:5px;}");
			out.println("tr{border:#000 1px solid;}");
			out.println(".flagship-radio_box{width:40px;height:30px}");
			out.println(".flagship-green{background:#00ff00}");
			out.println(".flagship-yellow{background:#ffff00}");
			out.println(".flagship-red{background:#ff0000}");
			out.println(".flagship-output-progress-col{background:#978a63;}");
			out.println(".flagship-title{font-weight:bold}");
			out.println(".topic-color{background:#e5b8b7}");
			out.println(".outcome-color{background:#978a63}");
			out.println(".activity-color{background:#FE9A2E}");

			out.println("</style>");
			out.println("</head>");

			out.println("<body>");

			// #### TABLE 01 #### //
			out.println("<table style=\"table-layout: fixed;\" width=\"100%\">");
			out.println("<tbody>");
			out.println("<tr>");
			out.println("<th class=\"topic-color\" colspan=\"12\">SECTION 1.  FLAGSHIP / FOCAL REGION DETAILS</th>");
			out.println("</tr>");
			out.println("<tr>");
			out.println("<td colspan=\"3\" class=\"flagship-first_col\">Flagship</td>");
			out.println("<td colspan=\"9\" class=\"autofil_bg\">"
					+ flagshipFocalRegion.getLongName() + "</td>");
			out.println("</tr>");

			out.println("<tr>");
			out.println("<td colspan=\"3\" class=\"flagship-first_col\">Period Covered:</td>");
			out.println("<td colspan=\"4\" class=\"autofil_bg\">" + periodStart
					+ "</td>");
			out.println("<td colspan=\"5\" class=\"autofil_bg\">" + periodEnd
					+ "</td>");
			out.println("</tr>");
			out.println("</tbody>");
			out.println("</table>");
			out.println("<br/>");

			boolean headerPrint = false;
			for (FlagshipCluster flagshipCluster : flagshipReport
					.getFlagshipClusters()) {
				out.println("<table style=\"table-layout: fixed;\" width=\"100%\">");
				out.println("<tbody>");
				if (!headerPrint) {
					out.println("<tr>");
					out.println("<th class=\"topic-color\" colspan=\"12\">SECTION 2.  ACTIVITY CLUSTER DETAILS AND OVERALL PROGRESS</th>");
					out.println("</tr>");
					headerPrint = true;

				}

				out.println("<tr>");
				out.println("<td colspan=\"3\" class=\"flagship-first_col bold-title\">Activity Cluster</td>");
				out.println("<td colspan=\"9\" class=\"autofil_bg bold-title\">"
						+ flagshipCluster.getName() + "</td>");
				out.println("</tr>");
				out.println("<tr>");
				out.println("<td colspan=\"2\" class=\"flagship-first_col title\">Activity Cluster Budget for Year</td>");
				out.println("<td colspan=\"2\" class=\"autofil_bg\">"
						+ flagshipCluster.getBudget() + "</td>");
				out.println("<td colspan=\"2\" class=\"flagship-first_col title\">Draft Expenditure by end June</td>");
				out.println("<td class=\"fsfr-rpt autofil_bg\" colspan=\"2\">"
						+ flagshipCluster.getDraftExpenditure() + "</td>");
				out.println("<td colspan=\"2\" class=\"flagship-first_col title\">Final Expenditure by end June</td>");
				out.println("<td class=\"fsfr-rpt autofil_bg\" colspan=\"2\">"
						+ flagshipCluster.getSpentByJune() + "</td>");
				out.println("</tr>");
				out.println("<tr>");
				out.println("<td class=\"flagship-first_col\" colspan=\"12\"><span class=\"flagship-title\">Mid-Year Status</span>(based on aggregate cluster outputs identified in the assessment frameworks at flagship / focal region level) </td>");
				out.println("</tr>");

				for (Entry<String, ClusterOutputDetail> clusterOutputDetailEntry : flagshipCluster
						.getClusterOutputMap().entrySet()) {

					ClusterOutputDetail clusterOutputDetail = clusterOutputDetailEntry
							.getValue();

					out.println("<tr>");
					out.println("<td class=\"title outcome-color\" colspan=\"12\">");
					out.println("<font color=\"white\"><span class=\"flagship-title\">Cluster Output<br/>"
							+ clusterOutputDetail.getCode()
							+ " - "
							+ clusterOutputDetail.getDescription()
							+ "</span></font><br/>");
					out.println("</td>");
					out.println("</tr>");

					for (Entry<String, ActivityClusterOutput> activityClusterOutputEntry : clusterOutputDetail
							.getActivityMap().entrySet()) {

						ActivityClusterOutput activityClusterOutput = activityClusterOutputEntry
								.getValue();
						out.println("<tr>");
						out.println("<td class=\"title activity-color\" colspan=\"12\">");
						out.println("<font color=\"white\"><span class=\"flagship-title\">Contributory Activity No : "
								+ activityClusterOutput.getActivityId()
								+ "<br/>Contributory Activity Name : "
								+ activityClusterOutput.getActivityName()
								+ "</span></font><br/>");
						out.println("</td>");
						out.println("</tr>");

						for (ClusterOutput clusterOutput : activityClusterOutput
								.getClusterOutputs()) {
							out.println("<tr>");
							out.println("<td colspan=\"2\" class=\"flagship-first_col title\">Project Output #</td>");
							out.println("<td colspan=\"6\" class=\"flagship-first_col title\">Project Output Name</td>");
							out.println("<td colspan=\"4\" class=\"flagship-first_col title\">Project Output Type/Subtype</td>");
							out.println("</tr>");
							out.println("<tr>");
							out.println("<td colspan=\"2\" class=\"autofil_bg\">"
									+ clusterOutput.getProjectId() + "</td>");
							out.println("<td colspan=\"6\" class=\"autofil_bg\">"
									+ clusterOutput.getProjectName() + "</td>");
							out.println("<td colspan=\"4\" class=\"autofil_bg\">"
									+ clusterOutput.getStatus() + "</td>");
							out.println("</tr>");
							out.println("<tr>");
							out.println("<td colspan=\"10\" class=\"flagship-first_col title\">Progress towards delivery of contributory output</td>");
							out.println("<td colspan=\"2\" class=\"flagship-first_col title\">Status at Mid-Year</td>");
							out.println("</tr>");
							out.println("<tr>");
							out.println("<td colspan=\"10\" class=\"autofil_bg\">"
									+ clusterOutput.getProgress() + "</td>");

							String ratingHtml = "";
							String rating = clusterOutput.getRating();
							if (rating.equals("0")) {
								ratingHtml = "<div><div class=\"flagship-radio_box flagship-green\"></div> (Fully on track to be delivered by year-end or already delivered)</div>";
							} else if (rating.equals("1")) {
								ratingHtml = "<div><div class=\"flagship-radio_box flagship-yellow\"></div> (Likely to be delivered by year-end at current trajectory, but may need a bit more attention)</div>";
							} else if (rating.equals("2")) {
								ratingHtml = "<div><div class=\"flagship-radio_box flagship-red\"></div> (Unlikely to be delivered by year-end at current trajectory)</div>";
							}

							out.println("<td colspan=\"2\" class=\"autofil_bg\">"
									+ ratingHtml + "</td>");
							out.println("</tr>");
						}
					}

					out.println("<tr>");
					out.println("<td colspan=\"2\" class=\"flagship-output-progress-col\"><font color=\"white\">Comment on Cluster Output Progress</font></td>");
					out.println("<td colspan=\"10\" class=\"flagship-output-progress-col\"><font color=\"white\">"
							+ clusterOutputDetail.getComment() + "</font></td>");
					out.println("</tr>");
					out.println("<tr><td colspan=\"12\"></td></tr>");
				}

				out.println("<tr>");
				out.println("<td colspan=\"2\" class=\"flagship-first_col\">Overall status of Cluster</td>");
				out.println("<td colspan=\"10\" class=\"flagship-first_col\">"
						+ flagshipCluster.getOverallStatus() + "</td>");
				out.println("</tr>");

				String ratingHtml = "";
				String rating = flagshipCluster.getRating();
				if (rating.equals("0")) {
					ratingHtml = "<div><div class=\"flagship-radio_box flagship-green\"></div>On track against all deliverables</div>";
				} else if (rating.equals("1")) {
					ratingHtml = "<div><div class=\"flagship-radio_box flagship-yellow\"></div>On track with most but not all deliverables</div>";
				} else if (rating.equals("2")) {
					ratingHtml = "<div><div class=\"flagship-radio_box flagship-red\"></div>Off track with most deliverables</div>";
				}

				out.println("<tr><td colspan=\"12\" class=\"flagship-first_col\"><span class=\"flagship-title\">Overall Activity Cluster Rating (Please tick the one of the boxes):</span>");
				out.println(ratingHtml + "</td>");
				out.println("</tr>");
				out.println("</tbody>");
				out.println("</table>");
				out.println("<br/>");
			}

			// #### TABLE 03 #### //
			out.println("<table style=\"table-layout: fixed;\" width=\"100%\">");
			out.println("<tbody>");
			out.println("<tr>");
			out.println("<th class=\"topic-color\" colspan=\"12\">SECTION 3.  OVERALL FLAGSHIP / FOCAL REGION PROGRESS AT MID-YEAR</th>");
			out.println("</tr>");
			out.println("<tr>");
			out.println("<td class=\"flagship-first_col\" colspan=\"12\"><span class=\"flagship-flagship-title\">Overall Flagship Status at mid-year: </span> How is the flagship performing purely in terms of output delivery and spending. Highlight any risks or issues regarding likely trajectory for second half of year. Indicate contribution of flagship leader / focal region manager.</td>");
			out.println("</tr>");
			out.println("<tr>");
			out.println("<td colspan=\"12\" class=\"flagship-first_col\">");
			out.println("<div class=\"textArea\">");
			out.println("" + flagshipReport.getOverallDescription());
			out.println("</div>");
			out.println("</td>");
			out.println("</tr>");
			out.println("");
			out.println("</tbody>");
			out.println("</table>");
			out.println("<br/>");

			out.println("</body>");
			out.println("</html>");
			out.close();
			
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ fileName + "\"");

			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document,
					response.getOutputStream());
			document.open();
			XMLWorkerHelper.getInstance().parseXHtml(writer, document,
					new FileInputStream(filePath));
			document.close();

		} catch (Exception e) {
			LOG.error("Error while printing flagship report,", e);
		}
	}
}
