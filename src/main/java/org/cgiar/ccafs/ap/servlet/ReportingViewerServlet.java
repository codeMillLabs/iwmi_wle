package org.cgiar.ccafs.ap.servlet;

import static java.io.File.separator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import org.cgiar.ccafs.ap.data.model.Activity;
import org.cgiar.ccafs.ap.data.model.ActivityBudget;
import org.cgiar.ccafs.ap.data.model.ActivityBudgetTotals;
import org.cgiar.ccafs.ap.data.model.ActivityKeyword;
import org.cgiar.ccafs.ap.data.model.ActivityObjective;
import org.cgiar.ccafs.ap.data.model.ActivityPartner;
import org.cgiar.ccafs.ap.data.model.ContactPerson;
import org.cgiar.ccafs.ap.data.model.Deliverable;
import org.cgiar.ccafs.ap.data.model.DonorBudget;
import org.cgiar.ccafs.ap.data.model.MainBudget;
import org.cgiar.ccafs.ap.data.model.OtherSite;
import org.cgiar.ccafs.ap.data.model.Region;
import org.cgiar.ccafs.ap.data.model.RegionalBudget;
import org.cgiar.ccafs.ap.data.model.RegionalBudgetDetails;
import org.cgiar.ccafs.ap.data.model.Resource;
import org.cgiar.ccafs.ap.util.WleAppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Reporting Viewer Servlet.
 * </p>
 * 
 * @author Manuja
 */
public class ReportingViewerServlet extends AbstractHttpServlet {

	private static final long serialVersionUID = 6730204279357258587L;
	private static final Logger LOG = LoggerFactory
			.getLogger(ReportingViewerServlet.class);
	private static final String reportFolder = WleAppUtil.getResourceBundle()
			.getString("report.file.location");
	private Set<String> fileList = new HashSet<String>();

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			fileList = new HashSet<String>();
			int year = (request.getParameter("year") != null) ? Integer
					.parseInt(request.getParameter("year")) : -1;
			int activityId = (request.getParameter("activityId") != null) ? Integer
					.parseInt(request.getParameter("activityId")) : -1;

			String folderName = String.valueOf(System.currentTimeMillis());
			String folderPath = reportFolder + folderName;
			(new File(folderPath)).mkdirs();
			ServletContext context = request.getSession().getServletContext();
			LOG.info(" generateReport() --> context PATH :" + request.getContextPath());

			File destFile = null;
			String contentType = null;

			if (activityId != -1) {
				Activity activity = activityManager.getActivity(activityId);
				destFile = generateReport(folderPath, activity, context);
				contentType = "application/pdf";
			}

			else if (year != -1) {
				Activity[] activityList = activityManager
						.getActivityListByYear(year);

				if (activityList == null || activityList.length == 0)
					return;

				LOG.info("This Report export will have {} Activity outputs",
						activityList.length);
				for (Activity activity : activityList) {
					generateReport(folderPath, activity, context);
				}
				generateFileList(folderPath, new File(folderPath));
				String zipFileName = "Activity-Report-" + year + ".zip";
				zipIt(folderPath, zipFileName);
				LOG.info(
						"All Activity Reportes have been exported to PDF, Zip Ready to download, Zip Name :{}",
						zipFileName);
				destFile = new File(folderPath + File.separator + zipFileName);
				contentType = "application/zip";
			} else {
				return;
			}

			performTask(request, response, destFile, contentType);
		} catch (Exception e) {
			LOG.error(
					" generateReport() -> Error occurred in Report Generation,",
					e);
		}
	}

	private File generateReport(String folderPath, Activity activity,
			ServletContext context) throws JRException {
		LOG.debug(" generateReport() --> started");
		String reportFilePath = context.getRealPath("") + File.separator + "reports" + File.separator + "activity-summary-rpt.jasper";
		LOG.info(" generateReport() --> context :" + reportFilePath);
		
		File reportFile = new File(reportFilePath);
		if (!reportFile.exists())
			throw new JRRuntimeException(
					"File activity-summary-rpt not found. The report design must be compiled first.");

		JasperReport jasperReport = (JasperReport) JRLoader
				.loadObjectFromFile(reportFile.getPath());
		ReportingDataSource reportingDataSource = new ReportingDataSource(
				activity.getId());

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("BaseDir", reportFile.getParentFile());
		parameters.put("title", reportingDataSource.getTitle());
		parameters.put("leadCenter", reportingDataSource.getLeadCenter());

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
				parameters, reportingDataSource);

		File destFile = new File(folderPath + separator
				+ activity.getFormattedId() + ".pdf");
		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				destFile));
		exporter.exportReport();

		LOG.debug(" generateReport() -> jasper report generated and exported PDF");
		return destFile;
	}

	private void performTask(HttpServletRequest request,
			HttpServletResponse response, File destFile, String contentType)
			throws ServletException, IOException {

		response.setContentType(contentType);
		response.addHeader("Content-Disposition", "attachment; filename="
				+ destFile);
		response.setContentLength((int) destFile.length());

		@SuppressWarnings("resource")
		FileInputStream fileInputStream = new FileInputStream(destFile);
		OutputStream responseOutputStream = response.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
		}
	}

	public class ReportingDataSource implements JRDataSource {

		private Activity activity;
		private List<CaptionValue> reportData;

		private int index = -1;

		public ReportingDataSource(int activityId) {

			// Main Information
			activity = activityManager.getActivityStatusInfo(activityId);
			reportData = new ArrayList<>();
			getMainInformation(activityId);

			// Outcomes
			activity.setObjectives(activityObjectiveManager
					.getActivityObjectives(activityId));
			getObjectiveData();

			// Deliverables
			activity.setDeliverables(deliverableManager
					.getDeliverables(activityId));
			getDeliverableData();

			// Partners
			activity.setActivityPartners(activityPartnerManager
					.getActivityPartners(activityId));

			getPartnerData();

			// Regions
			getRegionData(activityId);

			// Locations
			getLocationData();

			// Budget
			ActivityBudget activityBudget = activityBudgetManager
					.getBudget(activityId);

			ActivityBudgetTotals activityBudgetTotals = activityBudget.sumAll();

			getActivityBudgetData(activityBudget, activityBudgetTotals);

			// Additional Information
			activity.setKeywords(activityKeywordManager
					.getKeywordList(activityId));

			getKeywordData(activityId);
		}

		private void getKeywordData(int activityId) {
			int index;
			if (null != activity.getKeywords()
					&& !activity.getKeywords().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Keywords", "-"));

				index = 1;
				for (ActivityKeyword keyword : activity.getKeywords()) {
					if (null != keyword.getKeyword()) {
						reportData.add(new CaptionValue("", String
								.valueOf(index)
								+ ". "
								+ keyword.getKeyword().getName()));
						index++;
					}
				}
			}

			StringBuilder otherKeywords = new StringBuilder();
			for (int i = 0; i < activity.getKeywords().size(); i++) {
				if (activity.getKeywords().get(i).getOther() != null) {
					if (i > 0) {
						otherKeywords.append(", ");
					}

					otherKeywords.append(activity.getKeywords().get(i)
							.getOther().trim());
				}
			}

			String otherKeywordsString = otherKeywords.toString();
			if (null != otherKeywordsString && !otherKeywordsString.equals("")) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Other Keywords",
						otherKeywordsString));
			}

			activity.setResources(resourceManager.getResources(activityId));
			if (null != activity.getResources()
					&& !activity.getResources().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Resources", "-"));

				index = 1;
				for (Resource resource : activity.getResources()) {
					reportData.add(new CaptionValue("", String.valueOf(index)
							+ ". " + resource.getName()));
					index++;
				}
			}
		}

		private void getActivityBudgetData(ActivityBudget activityBudget,
				ActivityBudgetTotals activityBudgetTotals) {
			int index;
			if (!activityBudget.getMainBudgets().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Main budget", "-"));

				index = 1;
				for (MainBudget mainBudget : activityBudget.getMainBudgets()) {

					String budgetString = String.valueOf(mainBudget
							.getW1w2BudgetTotal())
							+ getTab(mainBudget.getW1w2BudgetTotal())
							+ String.valueOf(mainBudget.getW3BudgetTotal())
							+ getTab(mainBudget.getW3BudgetTotal())
							+ String.valueOf(mainBudget
									.getBilateralBudgetTotal())
							+ getTab(mainBudget.getBilateralBudgetTotal())
							+ String.valueOf(mainBudget.getOtherBudgetTotal())
							+ getTab(mainBudget.getOtherBudgetTotal())
							+ String.valueOf(mainBudget.getRowTotalOfBudget());

					String genderString = String.valueOf(mainBudget
							.getW1w2GenderPrecTotal())
							+ getTab(mainBudget.getW1w2GenderPrecTotal())
							+ String.valueOf(mainBudget.getW3GenderPrecTotal())
							+ getTab(mainBudget.getW3GenderPrecTotal())
							+ String.valueOf(mainBudget
									.getBilateralGenderPrecTotal())
							+ getTab(mainBudget.getBilateralGenderPrecTotal())
							+ String.valueOf(mainBudget
									.getOtherGenderPrecTotal())
							+ getTab(mainBudget.getOtherGenderPrecTotal())
							+ String.valueOf(mainBudget
									.getRowTotalOfGenderPrecentage());

					String genderBudgetString = String.valueOf(mainBudget
							.getW1w2GenderBudgetTotal())
							+ getTab(mainBudget.getW1w2GenderBudgetTotal())
							+ String.valueOf(mainBudget
									.getW3GenderBudgetTotal())
							+ getTab(mainBudget.getW3GenderBudgetTotal())
							+ String.valueOf(mainBudget
									.getBilateralGenderBudgetTotal())
							+ getTab(mainBudget.getBilateralGenderBudgetTotal())
							+ String.valueOf(mainBudget
									.getOtherGenderBudgetTotal())
							+ getTab(mainBudget.getOtherGenderBudgetTotal())
							+ String.valueOf(mainBudget
									.getRowTotalOfGenderBudget());

					reportData.add(new CaptionValue(String.valueOf(index)
							+ "). " + mainBudget.getRegionName(),
							"W1/W2\t\tW3\t\tBilateral\t\tOther\t\tTotal"));
					reportData.add(new CaptionValue("Budget", budgetString));
					reportData.add(new CaptionValue("Gender %", genderString));
					reportData.add(new CaptionValue("Gender Budget",
							genderBudgetString));
					index++;
				}

				String tpsof = String.valueOf(activityBudgetTotals
						.getW1w2BudgetTotal())
						+ getTab(activityBudgetTotals.getW1w2BudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getW3BudgetTotal())
						+ getTab(activityBudgetTotals.getW3BudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getBilateralBudgetTotal())
						+ getTab(activityBudgetTotals.getBilateralBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getOtherBudgetTotal())
						+ getTab(activityBudgetTotals.getOtherBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getGrandTotalOfFunding());

				String gbp = String.valueOf(activityBudgetTotals
						.getW1w2GenderPrecTotal())
						+ getTab(activityBudgetTotals.getW1w2GenderPrecTotal())
						+ String.valueOf(activityBudgetTotals
								.getW3GenderPrecTotal())
						+ getTab(activityBudgetTotals.getW3GenderPrecTotal())
						+ String.valueOf(activityBudgetTotals
								.getBilateralGenderPrecTotal())
						+ getTab(activityBudgetTotals
								.getBilateralGenderPrecTotal())
						+ String.valueOf(activityBudgetTotals
								.getOtherGenderPrecTotal())
						+ getTab(activityBudgetTotals.getOtherGenderPrecTotal())
						+ String.valueOf(activityBudgetTotals
								.getTotalGenderPrecentage());

				String tgb = String.valueOf(activityBudgetTotals
						.getW1w2GenderBudgetTotal())
						+ getTab(activityBudgetTotals
								.getW1w2GenderBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getW3GenderBudgetTotal())
						+ getTab(activityBudgetTotals.getW3GenderBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getBilateralGenderBudgetTotal())
						+ getTab(activityBudgetTotals
								.getBilateralGenderBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getOtherGenderBudgetTotal())
						+ getTab(activityBudgetTotals
								.getOtherGenderBudgetTotal())
						+ String.valueOf(activityBudgetTotals
								.getGrandTotalGenderBudget());

				reportData.add(new CaptionValue("Total per source of funding",
						tpsof));
				reportData.add(new CaptionValue("Total Gender %", gbp));
				reportData.add(new CaptionValue("Total Gender Budget", tgb));

				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Region wise budget", "-"));
				index = 1;
				for (RegionalBudget regionalBudget : activityBudget
						.getRegBudgets()) {

					String w1w2String = "";
					String w3String = "";
					String bilateralString = "";
					String otherString = "";

					for (RegionalBudgetDetails regionalBudgetDetails : regionalBudget
							.getDetails()) {

						double mt = regionalBudgetDetails.getMainBudgetTotal();
						double per = regionalBudgetDetails.getPersonnel();
						double oper = regionalBudgetDetails.getOperations();
						double indir = regionalBudgetDetails.getIndirectCosts();
						double tot = regionalBudgetDetails.getTotal();

						String valueString = String.valueOf(mt) + getTab(mt)
								+ String.valueOf(per) + getTab(per)
								+ String.valueOf(oper) + getTab(oper)
								+ String.valueOf(indir) + getTab(indir)
								+ String.valueOf(tot);

						if (regionalBudgetDetails.getSourceType().equals(
								RegionalBudgetDetails.SOURCE_W1W2)) {
							w1w2String = valueString;
						} else if (regionalBudgetDetails.getSourceType()
								.equals(RegionalBudgetDetails.SOURCE_W3)) {
							w3String = valueString;
						} else if (regionalBudgetDetails.getSourceType()
								.equals(RegionalBudgetDetails.SOURCE_BILATERAL)) {
							bilateralString = valueString;
						} else if (regionalBudgetDetails.getSourceType()
								.equals(RegionalBudgetDetails.SOURCE_OTHER)) {
							otherString = valueString;
						}
					}

					String totalRow = String.valueOf(regionalBudget
							.getTotalMainBudgetTotal())
							+ getTab(regionalBudget.getTotalMainBudgetTotal())
							+ String.valueOf(regionalBudget.getPersonnelTotal())
							+ getTab(regionalBudget.getPersonnelTotal())
							+ String.valueOf(regionalBudget
									.getOperationsTotal())
							+ getTab(regionalBudget.getOperationsTotal())
							+ String.valueOf(regionalBudget
									.getIndirectCostsTotal())
							+ getTab(regionalBudget.getIndirectCostsTotal())
							+ String.valueOf(regionalBudget.getTotalRegionSum());

					reportData
							.add(new CaptionValue(String.valueOf(index) + "). "
									+ regionalBudget.getRegionName(),
									"Total Budget\tPersonnel\tOperations\tIndirect Costs\tTotal"));
					reportData.add(new CaptionValue("W1/W2", w1w2String));
					reportData.add(new CaptionValue("W3", w3String));
					reportData.add(new CaptionValue("Bilateral",
							bilateralString));
					reportData.add(new CaptionValue("Other", otherString));
					reportData.add(new CaptionValue("Total", totalRow));

					index++;
				}

				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Donor wise budget ", "-"));

				reportData.add(new CaptionValue("Source",
						"W3\t\tBilateral\t\tOther\t\tTotal"));

				for (DonorBudget donorBudget : activityBudget.getDonorsBudget()) {
					double w3 = donorBudget.getW3();
					double bil = donorBudget.getBilateral();
					double don = donorBudget.getOther();

					String row = String.valueOf(w3) + getTab(w3)
							+ String.valueOf(bil) + getTab(bil)
							+ String.valueOf(don) + getTab(don)
							+ String.valueOf(donorBudget.getTotal());

					reportData.add(new CaptionValue(donorBudget.getDonorName(),
							row));
				}

				String row = String.valueOf(activityBudgetTotals
						.getW3DonorTotal())
						+ getTab(activityBudgetTotals.getW3DonorTotal())
						+ String.valueOf(activityBudgetTotals
								.getBilateralDonorTotal())
						+ getTab(activityBudgetTotals.getBilateralDonorTotal())
						+ String.valueOf(activityBudgetTotals
								.getOtherDonorTotal())
						+ getTab(activityBudgetTotals.getOtherDonorTotal())
						+ String.valueOf(activityBudgetTotals
								.getBilateralBudgetTotal());

				reportData.add(new CaptionValue("Total", row));
			}
		}

		private void getLocationData() {
			int index;
			if (null != activity.getOtherLocations()
					&& !activity.getOtherLocations().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Site Details", "-"));

				index = 1;
				for (OtherSite otherSite : activity.getOtherLocations()) {
					reportData.add(new CaptionValue("Site Detail #", String
							.valueOf(index)));
					reportData.add(new CaptionValue("Country", otherSite
							.getCountry().getName()));
					reportData.add(new CaptionValue("Details", otherSite
							.getDetails()));
					reportData.add(new CaptionValue("Latitude", String
							.valueOf(otherSite.getLatitude())));
					reportData.add(new CaptionValue("Longitude", String
							.valueOf(otherSite.getLongitude())));
					reportData.add(new CaptionValue("", ""));
					index++;
				}
			}
		}

		private void getRegionData(int activityId) {
			int index;
			activity.setWles(activityWLEManager.getWLEList(activityId));
			activity.setCountries(activityCountryManager
					.getActvitiyCountries(activityId));
			activity.setRegions(activityRegionManager
					.getActvitiyRegions(activityId));
			activity.setBsLocations(activityBenchmarkSiteManager
					.getActivityBenchmarkSites(activityId));
			activity.setOtherLocations(activityOtherSiteManager
					.getActivityOtherSites(activityId));
			Region[] regions = regionManager.getRegionList();

			index = 1;
			for (Region region : regions) {
				if (activity.getRegionsIds().contains(
						String.valueOf(region.getId()))) {

					if (index == 1) {
						reportData.add(new CaptionValue("", ""));
						reportData.add(new CaptionValue("Regions", "-"));
					}
					reportData.add(new CaptionValue("Region #", String
							.valueOf(index)));
					reportData.add(new CaptionValue("Name", region.getName()));
					reportData.add(new CaptionValue("Countries", "All"));
					reportData.add(new CaptionValue("", ""));
					index++;
				} else if ((activity.getCountriesIdsByRegion(region.getId())
						.size() > 0)) {
					if (index == 1) {
						reportData.add(new CaptionValue("", ""));
						reportData.add(new CaptionValue("Regions", "-"));
					}
					reportData.add(new CaptionValue("Regions #", String
							.valueOf(index)));
					reportData.add(new CaptionValue("Name", region.getName()));
					String countries = null;
					for (String country : activity
							.getCountriesNamesByRegion(region.getId())) {
						if (null != countries) {
							countries = countries + ", ";
						} else {
							countries = "";
						}

						countries = countries + country;
					}
					reportData.add(new CaptionValue("Countries", countries));
					reportData.add(new CaptionValue("", ""));
					index++;
				}
			}
		}

		private void getPartnerData() {
			int index;
			if (null != activity.getActivityPartners()
					&& !activity.getActivityPartners().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Partners", "-"));
				index = 1;
				for (ActivityPartner partner : activity.getActivityPartners()) {
					reportData.add(new CaptionValue("Partner #", String
							.valueOf(index)));

					if (null != partner.getPartner()) {
						reportData.add(new CaptionValue("Type", partner
								.getPartner().getType().getName()));
						reportData.add(new CaptionValue("Country", partner
								.getPartner().getCountry().getName()));
						reportData.add(new CaptionValue("Name", partner
								.getPartner().getName()));
					}
					reportData.add(new CaptionValue("Contact name", partner
							.getContactName()));
					reportData.add(new CaptionValue("Contact email", partner
							.getContactEmail()));
					reportData.add(new CaptionValue("Classification", partner
							.getClasification()));
					reportData.add(new CaptionValue("Budget to partner", String
							.valueOf(partner.getBudget())));
					reportData.add(new CaptionValue("", ""));
					index++;
				}
			}
		}

		private void getDeliverableData() {
			int index;
			if (null != activity.getDeliverables()
					&& !activity.getDeliverables().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Deliverables", "-"));

				index = 1;
				for (Deliverable deliverable : activity.getDeliverables()) {
					reportData.add(new CaptionValue("Deliverable #", String
							.valueOf(index)));

					if (null != deliverable.getIndicator()) {
						reportData
								.add(new CaptionValue(
										"Indicators",
										deliverable.getIndicator().equals("0") ? "Other Deliverables" 
												: "Gender-related Deliverables"));
					}
					reportData.add(new CaptionValue("Description", deliverable
							.getDescription()));
					reportData.add(new CaptionValue("Type", deliverable
							.getType().getName()));
					reportData.add(new CaptionValue("", ""));
					index++;
				}
			}
		}

		private void getObjectiveData() {
			int index;
			if (null != activity.getObjectives()
					&& !activity.getObjectives().isEmpty()) {
				reportData.add(new CaptionValue("", ""));
				reportData.add(new CaptionValue("Outcomes", "-"));

				index = 1;
				for (ActivityObjective outcome : activity.getObjectives()) {
					reportData.add(new CaptionValue("Outcome #", String
							.valueOf(index)));
					if (null != outcome.getOutcomeType()) {
						reportData
								.add(new CaptionValue(
										"Outcome Type",
										outcome.getOutcomeType().equals("0") ? "Research outcomes"
												: "Gender research outcomes"));
					}
					reportData.add(new CaptionValue("Description", outcome
							.getDescription()));
					reportData.add(new CaptionValue("Research Users", outcome
							.getResearchUser()));
					reportData
							.add(new CaptionValue(
									"Application of Research",
									WleAppUtil
											.generateCommaSeperatedStringForReport(outcome
													.getResearchUserRole())));
					reportData.add(new CaptionValue("Tailored Project Outputs",
							outcome.getResearchOutcomeChanges()));
					reportData.add(new CaptionValue("Gender Description ",
							outcome.getGenderDescription()));
					reportData.add(new CaptionValue("Intermediaries", outcome
							.getIntermediaries()));
					reportData.add(new CaptionValue("Capacity Building",
							outcome.getCapacityBuilding()));
					reportData.add(new CaptionValue(
							"Expected Progress this year ", outcome
									.getProgress()));
					reportData.add(new CaptionValue("", ""));
					index++;
				}
			}
		}

		@SuppressWarnings("deprecation")
		private void getMainInformation(int activityId) {
			reportData.add(new CaptionValue("Main Information", "-"));
			reportData.add(new CaptionValue("Lead Center", activity
					.getLeadCenter()));
			reportData.add(new CaptionValue("Title", activity.getTitle()));
			reportData.add(new CaptionValue("Description", activity
					.getDescription()));
			reportData.add(new CaptionValue(
					"Corresponding Project name in the center", activity
							.getProjectName()));
			reportData.add(new CaptionValue("Flagship", activity
					.getFlagShipSRP()));
			reportData.add(new CaptionValue("Activity Cluster", activity
					.getActivityCluster()));
			reportData.add(new CaptionValue(
					"Description of Ecosystems Services", activity
							.getEcoSystemsDesc()));
			reportData.add(new CaptionValue("Start Date", activity
					.getStartDate() != null ? activity.getStartDate()
					.toLocaleString() : ""));
			reportData.add(new CaptionValue("End Date",
					activity.getEndDate() != null ? activity.getEndDate()
							.toLocaleString() : ""));
			reportData.add(new CaptionValue("Gender Integration", activity
					.getGenderIntegrationsDescription()));
			int index = 1;
			for (ContactPerson cp : contactPersonManager
					.getContactPersons(activityId)) {
				reportData.add(new CaptionValue("Contact Person "
						+ String.valueOf(index), cp.getName() + "("
						+ cp.getEmail() + ")"));
				index++;
			}
		}

		public boolean next() throws JRException {
			index++;
			return (index < reportData.size());
		}

		public Object getFieldValue(JRField field) throws JRException {
			Object value = null;

			String fieldName = field.getName();

			if ("Caption".equals(fieldName)) {
				value = reportData.get(index).getCaption();
			} else if ("Value".equals(fieldName)) {
				value = reportData.get(index).getValue();
			}

			return value;
		}

		public String getTitle() {
			return this.activity.getTitle();
		}

		public String getLeadCenter() {
			return this.activity.getLeadCenter();
		}

		private String getTab(double value) {
			if (String.valueOf(value).length() > 7) {
				return "\t";
			} else {
				return "\t\t";
			}
		}
	}

	public class CaptionValue {
		private String caption;
		private String value;

		public CaptionValue(String caption, String value) {
			this.caption = caption;
			this.value = value;
		}

		/**
		 * @return the caption
		 */
		public String getCaption() {
			return caption;
		}

		/**
		 * @param caption
		 *            the caption to set
		 */
		public void setCaption(String caption) {
			this.caption = caption;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}
	}

	public void zipIt(String sourceFolder, String zipFile) {
		String source = "";
		try (FileOutputStream fos = new FileOutputStream(sourceFolder
				+ File.separator + zipFile);
				ZipOutputStream zos = new ZipOutputStream(fos);) {
			try {
				source = sourceFolder.substring(
						sourceFolder.lastIndexOf("/") + 1,
						sourceFolder.length());
			} catch (Exception e) {
				source = sourceFolder;
			}

			for (String file : this.fileList) {
				byte[] buffer = new byte[1024 * 4];
				ZipEntry ze = new ZipEntry(source + File.separator + file);
				zos.putNextEntry(ze);
				File reportFile = new File(sourceFolder + File.separator + file);
				if (!reportFile.exists()) {
					continue;
				}

				try (FileInputStream in = new FileInputStream(reportFile)) {
					int len;
					while ((len = in.read(buffer)) > 0) {
						zos.write(buffer, 0, len);
					}
				}
			}
			zos.flush();
			zos.closeEntry();
		} catch (Exception e) {
			LOG.error("Error occurred in Generating ZIP File", e);
		}
	}

	public void generateFileList(String sourceFolder, File node) {
		// add file only
		if (node.isFile()) {
			fileList.add(generateZipEntry(sourceFolder, node.toString()));
		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(sourceFolder, new File(node, filename));
			}
		}
	}

	private String generateZipEntry(String sourceFolder, String file) {
		return file.substring(sourceFolder.length() + 1, file.length());
	}
}
