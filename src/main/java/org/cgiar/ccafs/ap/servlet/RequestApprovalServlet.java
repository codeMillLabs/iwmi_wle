package org.cgiar.ccafs.ap.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cgiar.ccafs.ap.data.model.Keyword;
import org.cgiar.ccafs.ap.data.model.Partner;
import org.cgiar.ccafs.ap.data.model.TokenAction;
import org.cgiar.ccafs.ap.util.SystemTokenGenerator;

public class RequestApprovalServlet extends AbstractHttpServlet {
	private static final long serialVersionUID = 3718739883962090302L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String token = request.getParameter("token");
		boolean error = false;
		if (null == token || token.equals("")) {
			error = true;
		} else {
			TokenAction tokenAction = tokenActionManager.getTokenAction(token);
			if (null != tokenAction) {

				if (SystemTokenGenerator.getAction(tokenAction.getAction())
						.equals("partner")) {
					String id = SystemTokenGenerator.getPartnerId(tokenAction
							.getAction());
					Partner partner = partnerManager.getPartner(Integer
							.parseInt(id));
					request.setAttribute("id", partner.getId());
					request.setAttribute("tokenId", tokenAction.getTokenId());
					request.setAttribute("name", partner.getName());
					request.setAttribute("acronym", partner.getAcronym());
					request.setAttribute("partnerType", partner.getType()
							.getId());
					request.setAttribute("partnerWeb", partner.getWebsite());
					request.setAttribute("countryId", partner.getCountry()
							.getId());
					request.setAttribute("createdBy", partner.getCreatedBy());

					request.getRequestDispatcher("./jsp/approve-partner.jsp")
							.forward(request, response);
				} else if (SystemTokenGenerator.getAction(
						tokenAction.getAction()).equals("keyword")) {
					String[] ids = SystemTokenGenerator
							.getKeywordIds(tokenAction.getAction());
					List<Keyword> keywords = new ArrayList<>();
					for (String id : ids)
						keywords.add(keywordManager.getKeyword(id));
					request.setAttribute("tokenId", tokenAction.getTokenId());
					request.setAttribute("keywords", keywords);
					request.getRequestDispatcher("./jsp/approve-keyword.jsp")
							.forward(request, response);
				}
			} else {
				error = true;
			}
		}

		if (error)
			request.getRequestDispatcher("./jsp/invalid-token.jsp").forward(
					request, response);
	}
}
