<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html lang="en">
<%@include file='/jsp/review-header-top.jsp'%>
<style>
.topic {
	width: 15%;
	float: left;
	text-align: left;
	margin-left: 20px
}

;
.r-content {
	float: left;
	text-align: left;
}

.r-content input {
	width: 50%
}

.r-header {
	float: left;
	font-weight: bold;
}

.clear {
	clear: both
}

.label {
	float: left;
	width: 80px;
	margin: 5px 0;
	text-align: left;
}

.field {
	float: left;
	width: 500px;
	margin: 5px 0
}

.r-button {
	-moz-border-bottom-colors: none;
	-moz-border-left-colors: none;
	-moz-border-right-colors: none;
	-moz-border-top-colors: none;
	background-color: #f5f5f5;
	border-color: #eee #dedede #dedede #eee;
	border-image: none;
	border-right: 1px solid #dedede;
	border-style: solid;
	border-width: 1px;
	color: #565656;
	cursor: pointer;
	font-size: 12px;
	margin: 0 7px 0 0;
	padding: 5px 10px 6px 7px;
	float: left;
	margin-top: 8px;
}

.add-comment-caption {
	font-weight: bold;
	float: left;
	margin-top: 8px
}
</style>
<body>
	<div>
		<section>
			<article class="content">
				<h1>Review Comments - ${category_display}</h1>
				<div>
					<c:if test="${editable}">
						<form id="commentSave" name="commentSave"
							action="/wle-ap/review-request" method="post">
							<div class="clear">
								<div class="add-comment-caption">Comment :</div>
								<div class="field">
									<input type="hidden" name="id"
										value="${sectionReview.activityId}" /> <input type="hidden"
										name="revisionId" value="${sectionReview.currentRevision}" />
									<input type="hidden" name="email" value="${email}" /> <input
										type="hidden" name="category" value="${category_name}" /> <input
										type="hidden" name="status" value="${status}" />
									<textarea rows="4" cols="50" name="comment"></textarea>
								</div>
							</div>
							<input class="r-button" type="submit" value="Add" />
						</form>
					</c:if>
					<div class="clear">&nbsp;</div>
					<c:set var="count" value="1" scope="page" />
					<c:forEach var="review" items="${sectionReview.reviews}">
						<c:choose>
							<c:when test="${showIntReviews && review.type == 'INTERNAL'}">
							   <div class="r-header">${count}).
									Revision No :${review.revisionId},&nbsp; Date :
									<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${review.date}" />
									,&nbsp; Reviewer :${review.reviewerName} - (Internal)
								</div>
								<div class="clear">
									<div class="label">&nbsp;&nbsp;&nbsp;&nbsp;Comment :</div>
									<div class="field">
										<textarea rows="4" cols="50" name="reviewComment"
											readonly="readonly">${review.comment}</textarea>
									</div>
								</div>
								<div class="clear">&nbsp;</div>
								<c:set var="count" value="${count + 1}" scope="page" />
							</c:when>
							<c:when test="${review.type == 'EXTERNAL'}">
							   <div class="r-header">${count}).
									Revision No :${review.revisionId},&nbsp; Date :
									<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${review.date}" />
									,&nbsp; Reviewer :${review.reviewerName} - (External)
								</div>
								<div class="clear">
									<div class="label">&nbsp;&nbsp;&nbsp;&nbsp;Comment :</div>
									<div class="field">
										<textarea rows="4" cols="50" name="reviewComment"
											readonly="readonly">${review.comment}</textarea>
									</div>
								</div>
								<div class="clear">&nbsp;</div>
								<c:set var="count" value="${count + 1}" scope="page" />
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>

						
					</c:forEach>
					<div class="clear">&nbsp;</div>
				</div>
			</article>
		</section>
	</div>
</body>