<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<%@include file='/jsp/header-top.jsp'%>
<body>
	<div class="outside_popup_container">
		<%@include file='/jsp/header-bottom.jsp'%>
		<section>
			<article class="content">
				<h1>Approve Keywords</h1>

				<form id="keywordSave" name="keywordSave" action="/ccafs-ap/keyword"
					method="post">
					<c:forEach var="keyword" items="${keywords}">
						<input type="checkbox" name="keywordList" value="${keyword.id}">&nbsp;<b>${keyword.name}</b>
						<br/>
					</c:forEach>
					<input type="hidden" id="token.id" name="tokenId" value="${tokenId}" />
					<br/>
					<input type="submit" id="partnerSave_save" value="Approve" />
				</form>
			</article>
		</section>
	</div>
</body>