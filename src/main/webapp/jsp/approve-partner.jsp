<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<%@include file='/jsp/header-top.jsp'%>
<body>
	<div class="outside_popup_container">
		<%@include file='/jsp/header-bottom.jsp'%>
		<section>
			<article class="content">
				<h1>Approve a partner</h1>
				<form id="partnerSave" name="partnerSave" action="/ccafs-ap/partner"
					method="post">

					<div class="fullBlock">
						<div class="input">
							<h6>
								<label for="activityPartner.partner.name">Name </label>
							</h6>
							<input type="text" id="activityPartner.partner.name"
								name="partnerName" value="${name}" />
						</div>
					</div>

					<div class="fullBlock">
						<div class="input">
							<h6>
								<label for="activityPartner.partner.acronym">Acronym </label>
							</h6>
							<input type="text" id="activityPartner.partner.acronym"
								name="acronym" value="${acronym}" />
						</div>
					</div>

					<div class="halfPartBlock">
						<div class="select">
							<h6>Partner type:</h6>
							<div class="selectList">
								<select name="partnerTypeId"
									id="activityPartner.partner.type.id" class="">
									<c:choose>
										<c:when test='${partnerType == "1"}'>
											<option value="1" selected="selected">Academic
												Institution</option>
										</c:when>
										<c:otherwise>
											<option value="1">Academic Institution</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "2"}'>
											<option value="2" selected="selected">Advanced
												Research Institution</option>
										</c:when>
										<c:otherwise>
											<option value="2">Advanced Research Institution</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "3"}'>
											<option value="3" selected="selected">CGIAR Center</option>
										</c:when>
										<c:otherwise>
											<option value="3">CGIAR Center</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "4"}'>
											<option value="4" selected="selected">Challenge
												Research Program</option>
										</c:when>
										<c:otherwise>
											<option value="4">Challenge Research Program</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "5"}'>
											<option value="5" selected="selected">Donors</option>
										</c:when>
										<c:otherwise>
											<option value="5">Donors</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "6"}'>
											<option value="6" selected="selected">End users</option>
										</c:when>
										<c:otherwise>
											<option value="6">End users</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "7"}'>
											<option value="7" selected="selected">Government
												office/department</option>
										</c:when>
										<c:otherwise>
											<option value="7">Government office/department</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "8"}'>
											<option value="8" selected="selected">National
												agricultural research and extension services</option>
										</c:when>
										<c:otherwise>
											<option value="8">National agricultural research and
												extension services</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "9"}'>
											<option value="9" selected="selected">Non-governmental
												organization/Development organization</option>
										</c:when>
										<c:otherwise>
											<option value="9">Non-governmental
												organization/Development organization</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "10"}'>
											<option value="10" selected="selected">Private
												Research Institution</option>
										</c:when>
										<c:otherwise>
											<option value="10">Private Research Institution</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "11"}'>
											<option value="11" selected="selected">Regional
												Organization</option>
										</c:when>
										<c:otherwise>
											<option value="11">Regional Organization</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "12"}'>
											<option value="12" selected="selected">Research
												network</option>
										</c:when>
										<c:otherwise>
											<option value="12">Research network</option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test='${partnerType == "18"}'>
											<option value="18" selected="selected">Other</option>
										</c:when>
										<c:otherwise>
											<option value="18">Other</option>
										</c:otherwise>
									</c:choose>
								</select>
							</div>
						</div>
					</div>

					<div class="halfPartBlock">
						<div class="select">
							<h6>Country:</h6>
							<div class="selectList">
								<select name="countryId" id="country" class="">
									<c:choose>
										<c:when test='${countryId == "af"}'>
											<option value="af" selected="selected">Afghanistan</option>
										</c:when>
										<c:otherwise>
											<option value="af">Afghanistan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "al"}'>
											<option value="al" selected="selected">Albania</option>
										</c:when>
										<c:otherwise>
											<option value="al">Albania</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "dz"}'>
											<option value="dz" selected="selected">Algeria</option>
										</c:when>
										<c:otherwise>
											<option value="dz">Algeria</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "as"}'>
											<option value="as" selected="selected">American
												Samoa</option>
										</c:when>
										<c:otherwise>
											<option value="as">American Samoa</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ad"}'>
											<option value="ad" selected="selected">Andorra</option>
										</c:when>
										<c:otherwise>
											<option value="ad">Andorra</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ao"}'>
											<option value="ao" selected="selected">Angola</option>
										</c:when>
										<c:otherwise>
											<option value="ao">Angola</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ai"}'>
											<option value="ai" selected="selected">Anguilla</option>
										</c:when>
										<c:otherwise>
											<option value="ai">Anguilla</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "aq"}'>
											<option value="aq" selected="selected">Antarctica</option>
										</c:when>
										<c:otherwise>
											<option value="aq">Antarctica</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ag"}'>
											<option value="ag" selected="selected">Antigua and
												Barbuda</option>
										</c:when>
										<c:otherwise>
											<option value="ag">Antigua and Barbuda</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ar"}'>
											<option value="ar" selected="selected">Argentina</option>
										</c:when>
										<c:otherwise>
											<option value="ar">Argentina</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "am"}'>
											<option value="am" selected="selected">Armenia</option>
										</c:when>
										<c:otherwise>
											<option value="am">Armenia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "aw"}'>
											<option value="aw" selected="selected">Aruba</option>
										</c:when>
										<c:otherwise>
											<option value="aw">Aruba</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "au"}'>
											<option value="au" selected="selected">Australia</option>
										</c:when>
										<c:otherwise>
											<option value="au">Australia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "at"}'>
											<option value="at" selected="selected">Austria</option>
										</c:when>
										<c:otherwise>
											<option value="at">Austria</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "az"}'>
											<option value="az" selected="selected">Azerbaijan</option>
										</c:when>
										<c:otherwise>
											<option value="az">Azerbaijan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bs"}'>
											<option value="bs" selected="selected">Bahamas</option>
										</c:when>
										<c:otherwise>
											<option value="bs">Bahamas</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bh"}'>
											<option value="bh" selected="selected">Bahrain</option>
										</c:when>
										<c:otherwise>
											<option value="bh">Bahrain</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bd"}'>
											<option value="bd" selected="selected">Bangladesh</option>
										</c:when>
										<c:otherwise>
											<option value="bd">Bangladesh</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bb"}'>
											<option value="bb" selected="selected">Barbados</option>
										</c:when>
										<c:otherwise>
											<option value="bb">Barbados</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "by"}'>
											<option value="by" selected="selected">Belarus</option>
										</c:when>
										<c:otherwise>
											<option value="by">Belarus</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "be"}'>
											<option value="be" selected="selected">Belgium</option>
										</c:when>
										<c:otherwise>
											<option value="be">Belgium</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bz"}'>
											<option value="bz" selected="selected">Belize</option>
										</c:when>
										<c:otherwise>
											<option value="bz">Belize</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bj"}'>
											<option value="bj" selected="selected">Benin</option>
										</c:when>
										<c:otherwise>
											<option value="bj">Benin</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bm"}'>
											<option value="bm" selected="selected">Bermuda</option>
										</c:when>
										<c:otherwise>
											<option value="bm">Bermuda</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bt"}'>
											<option value="bt" selected="selected">Bhutan</option>
										</c:when>
										<c:otherwise>
											<option value="bt">Bhutan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bo"}'>
											<option value="bo" selected="selected">Bolivia</option>
										</c:when>
										<c:otherwise>
											<option value="bo">Bolivia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ba"}'>
											<option value="ba" selected="selected">Bosnia and
												Herzegovina</option>
										</c:when>
										<c:otherwise>
											<option value="ba">Bosnia and Herzegovina</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bw"}'>
											<option value="bw" selected="selected">Botswana</option>
										</c:when>
										<c:otherwise>
											<option value="bw">Botswana</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "br"}'>
											<option value="br" selected="selected">Brazil</option>
										</c:when>
										<c:otherwise>
											<option value="br">Brazil</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "io"}'>
											<option value="io" selected="selected">British
												Indian Ocean Territory</option>
										</c:when>
										<c:otherwise>
											<option value="io">British Indian Ocean Territory</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "vg"}'>
											<option value="vg" selected="selected">British
												Virgin Islands</option>
										</c:when>
										<c:otherwise>
											<option value="vg">British Virgin Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bn"}'>
											<option value="bn" selected="selected">Brunei</option>
										</c:when>
										<c:otherwise>
											<option value="bn">Brunei</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bg"}'>
											<option value="bg" selected="selected">Bulgaria</option>
										</c:when>
										<c:otherwise>
											<option value="bg">Bulgaria</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bf"}'>
											<option value="bf" selected="selected">Burkina Faso</option>
										</c:when>
										<c:otherwise>
											<option value="bf">Burkina Faso</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mm"}'>
											<option value="mm" selected="selected">Burma</option>
										</c:when>
										<c:otherwise>
											<option value="mm">Burma</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bi"}'>
											<option value="bi" selected="selected">Burundi</option>
										</c:when>
										<c:otherwise>
											<option value="bi">Burundi</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kh"}'>
											<option value="kh" selected="selected">Cambodia</option>
										</c:when>
										<c:otherwise>
											<option value="kh">Cambodia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cm"}'>
											<option value="cm" selected="selected">Cameroon</option>
										</c:when>
										<c:otherwise>
											<option value="cm">Cameroon</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ca"}'>
											<option value="ca" selected="selected">Canada</option>
										</c:when>
										<c:otherwise>
											<option value="ca">Canada</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cv"}'>
											<option value="cv" selected="selected">Cape Verde</option>
										</c:when>
										<c:otherwise>
											<option value="cv">Cape Verde</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ky"}'>
											<option value="ky" selected="selected">Cayman
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="ky">Cayman Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cf"}'>
											<option value="cf" selected="selected">Central
												African Republic</option>
										</c:when>
										<c:otherwise>
											<option value="cf">Central African Republic</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "td"}'>
											<option value="td" selected="selected">Chad</option>
										</c:when>
										<c:otherwise>
											<option value="td">Chad</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cl"}'>
											<option value="cl" selected="selected">Chile</option>
										</c:when>
										<c:otherwise>
											<option value="cl">Chile</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cn"}'>
											<option value="cn" selected="selected">China</option>
										</c:when>
										<c:otherwise>
											<option value="cn">China</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cx"}'>
											<option value="cx" selected="selected">Christmas
												Island</option>
										</c:when>
										<c:otherwise>
											<option value="cx">Christmas Island</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cc"}'>
											<option value="cc" selected="selected">Cocos
												(Keeling) Islands</option>
										</c:when>
										<c:otherwise>
											<option value="cc">Cocos (Keeling) Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "co"}'>
											<option value="co" selected="selected">Colombia</option>
										</c:when>
										<c:otherwise>
											<option value="co">Colombia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "km"}'>
											<option value="km" selected="selected">Comoros</option>
										</c:when>
										<c:otherwise>
											<option value="km">Comoros</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ck"}'>
											<option value="ck" selected="selected">Cook Islands</option>
										</c:when>
										<c:otherwise>
											<option value="ck">Cook Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cr"}'>
											<option value="cr" selected="selected">Costa Rica</option>
										</c:when>
										<c:otherwise>
											<option value="cr">Costa Rica</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "hr"}'>
											<option value="hr" selected="selected">Croatia</option>
										</c:when>
										<c:otherwise>
											<option value="hr">Croatia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cu"}'>
											<option value="cu" selected="selected">Cuba</option>
										</c:when>
										<c:otherwise>
											<option value="cu">Cuba</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cy"}'>
											<option value="cy" selected="selected">Cyprus</option>
										</c:when>
										<c:otherwise>
											<option value="cy">Cyprus</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cz"}'>
											<option value="cz" selected="selected">Czech
												Republic</option>
										</c:when>
										<c:otherwise>
											<option value="cz">Czech Republic</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cd"}'>
											<option value="cd" selected="selected">Democratic
												Republic of the Congo</option>
										</c:when>
										<c:otherwise>
											<option value="cd">Democratic Republic of the Congo</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "dk"}'>
											<option value="dk" selected="selected">Denmark</option>
										</c:when>
										<c:otherwise>
											<option value="dk">Denmark</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "dj"}'>
											<option value="dj" selected="selected">Djibouti</option>
										</c:when>
										<c:otherwise>
											<option value="dj">Djibouti</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "dm"}'>
											<option value="dm" selected="selected">Dominica</option>
										</c:when>
										<c:otherwise>
											<option value="dm">Dominica</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "do"}'>
											<option value="do" selected="selected">Dominican
												Republic</option>
										</c:when>
										<c:otherwise>
											<option value="do">Dominican Republic</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tl"}'>
											<option value="tl" selected="selected">East Timor</option>
										</c:when>
										<c:otherwise>
											<option value="tl">East Timor</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ec"}'>
											<option value="ec" selected="selected">Ecuador</option>
										</c:when>
										<c:otherwise>
											<option value="ec">Ecuador</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "eg"}'>
											<option value="eg" selected="selected">Egypt</option>
										</c:when>
										<c:otherwise>
											<option value="eg">Egypt</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sv"}'>
											<option value="sv" selected="selected">El Salvador</option>
										</c:when>
										<c:otherwise>
											<option value="sv">El Salvador</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gq"}'>
											<option value="gq" selected="selected">Equatorial
												Guinea</option>
										</c:when>
										<c:otherwise>
											<option value="gq">Equatorial Guinea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "er"}'>
											<option value="er" selected="selected">Eritrea</option>
										</c:when>
										<c:otherwise>
											<option value="er">Eritrea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ee"}'>
											<option value="ee" selected="selected">Estonia</option>
										</c:when>
										<c:otherwise>
											<option value="ee">Estonia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "et"}'>
											<option value="et" selected="selected">Ethiopia</option>
										</c:when>
										<c:otherwise>
											<option value="et">Ethiopia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fk"}'>
											<option value="fk" selected="selected">Falkland
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="fk">Falkland Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fo"}'>
											<option value="fo" selected="selected">Faroe Islands</option>
										</c:when>
										<c:otherwise>
											<option value="fo">Faroe Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fj"}'>
											<option value="fj" selected="selected">Fiji</option>
										</c:when>
										<c:otherwise>
											<option value="fj">Fiji</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fi"}'>
											<option value="fi" selected="selected">Finland</option>
										</c:when>
										<c:otherwise>
											<option value="fi">Finland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fr"}'>
											<option value="fr" selected="selected">France</option>
										</c:when>
										<c:otherwise>
											<option value="fr">France</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pf"}'>
											<option value="pf" selected="selected">French
												Polynesia</option>
										</c:when>
										<c:otherwise>
											<option value="pf">French Polynesia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ga"}'>
											<option value="ga" selected="selected">Gabon</option>
										</c:when>
										<c:otherwise>
											<option value="ga">Gabon</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gm"}'>
											<option value="gm" selected="selected">Gambia</option>
										</c:when>
										<c:otherwise>
											<option value="gm">Gambia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ge"}'>
											<option value="ge" selected="selected">Georgia</option>
										</c:when>
										<c:otherwise>
											<option value="ge">Georgia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "de"}'>
											<option value="de" selected="selected">Germany</option>
										</c:when>
										<c:otherwise>
											<option value="de">Germany</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gh"}'>
											<option value="gh" selected="selected">Ghana</option>
										</c:when>
										<c:otherwise>
											<option value="gh">Ghana</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gi"}'>
											<option value="gi" selected="selected">Gibraltar</option>
										</c:when>
										<c:otherwise>
											<option value="gi">Gibraltar</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gr"}'>
											<option value="gr" selected="selected">Greece</option>
										</c:when>
										<c:otherwise>
											<option value="gr">Greece</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gl"}'>
											<option value="gl" selected="selected">Greenland</option>
										</c:when>
										<c:otherwise>
											<option value="gl">Greenland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gd"}'>
											<option value="gd" selected="selected">Grenada</option>
										</c:when>
										<c:otherwise>
											<option value="gd">Grenada</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gu"}'>
											<option value="gu" selected="selected">Guam</option>
										</c:when>
										<c:otherwise>
											<option value="gu">Guam</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gt"}'>
											<option value="gt" selected="selected">Guatemala</option>
										</c:when>
										<c:otherwise>
											<option value="gt">Guatemala</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gn"}'>
											<option value="gn" selected="selected">Guinea</option>
										</c:when>
										<c:otherwise>
											<option value="gn">Guinea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gw"}'>
											<option value="gw" selected="selected">Guinea-Bissau</option>
										</c:when>
										<c:otherwise>
											<option value="gw">Guinea-Bissau</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gy"}'>
											<option value="gy" selected="selected">Guyana</option>
										</c:when>
										<c:otherwise>
											<option value="gy">Guyana</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ht"}'>
											<option value="ht" selected="selected">Haiti</option>
										</c:when>
										<c:otherwise>
											<option value="ht">Haiti</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "va"}'>
											<option value="va" selected="selected">Holy See
												(Vatican City)</option>
										</c:when>
										<c:otherwise>
											<option value="va">Holy See (Vatican City)</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "hn"}'>
											<option value="hn" selected="selected">Honduras</option>
										</c:when>
										<c:otherwise>
											<option value="hn">Honduras</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "hk"}'>
											<option value="hk" selected="selected">Hong Kong</option>
										</c:when>
										<c:otherwise>
											<option value="hk">Hong Kong</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "hu"}'>
											<option value="hu" selected="selected">Hungary</option>
										</c:when>
										<c:otherwise>
											<option value="hu">Hungary</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "is"}'>
											<option value="is" selected="selected">Iceland</option>
										</c:when>
										<c:otherwise>
											<option value="is">Iceland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "in"}'>
											<option value="in" selected="selected">India</option>
										</c:when>
										<c:otherwise>
											<option value="in">India</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "id"}'>
											<option value="id" selected="selected">Indonesia</option>
										</c:when>
										<c:otherwise>
											<option value="id">Indonesia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ir"}'>
											<option value="ir" selected="selected">Iran</option>
										</c:when>
										<c:otherwise>
											<option value="ir">Iran</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "iq"}'>
											<option value="iq" selected="selected">Iraq</option>
										</c:when>
										<c:otherwise>
											<option value="iq">Iraq</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ie"}'>
											<option value="ie" selected="selected">Ireland</option>
										</c:when>
										<c:otherwise>
											<option value="ie">Ireland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "im"}'>
											<option value="im" selected="selected">Isle of Man</option>
										</c:when>
										<c:otherwise>
											<option value="im">Isle of Man</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "il"}'>
											<option value="il" selected="selected">Israel</option>
										</c:when>
										<c:otherwise>
											<option value="il">Israel</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "it"}'>
											<option value="it" selected="selected">Italy</option>
										</c:when>
										<c:otherwise>
											<option value="it">Italy</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ci"}'>
											<option value="ci" selected="selected">Ivory Coast</option>
										</c:when>
										<c:otherwise>
											<option value="ci">Ivory Coast</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "jm"}'>
											<option value="jm" selected="selected">Jamaica</option>
										</c:when>
										<c:otherwise>
											<option value="jm">Jamaica</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "jp"}'>
											<option value="jp" selected="selected">Japan</option>
										</c:when>
										<c:otherwise>
											<option value="jp">Japan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "je"}'>
											<option value="je" selected="selected">Jersey</option>
										</c:when>
										<c:otherwise>
											<option value="je">Jersey</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "jo"}'>
											<option value="jo" selected="selected">Jordan</option>
										</c:when>
										<c:otherwise>
											<option value="jo">Jordan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kz"}'>
											<option value="kz" selected="selected">Kazakhstan</option>
										</c:when>
										<c:otherwise>
											<option value="kz">Kazakhstan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ke"}'>
											<option value="ke" selected="selected">Kenya</option>
										</c:when>
										<c:otherwise>
											<option value="ke">Kenya</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ki"}'>
											<option value="ki" selected="selected">Kiribati</option>
										</c:when>
										<c:otherwise>
											<option value="ki">Kiribati</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kw"}'>
											<option value="kw" selected="selected">Kuwait</option>
										</c:when>
										<c:otherwise>
											<option value="kw">Kuwait</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kg"}'>
											<option value="kg" selected="selected">Kyrgyzstan</option>
										</c:when>
										<c:otherwise>
											<option value="kg">Kyrgyzstan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "la"}'>
											<option value="la" selected="selected">Laos</option>
										</c:when>
										<c:otherwise>
											<option value="la">Laos</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lb"}'>
											<option value="lb" selected="selected">Lebanon</option>
										</c:when>
										<c:otherwise>
											<option value="lb">Lebanon</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ls"}'>
											<option value="ls" selected="selected">Lesotho</option>
										</c:when>
										<c:otherwise>
											<option value="ls">Lesotho</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lr"}'>
											<option value="lr" selected="selected">Liberia</option>
										</c:when>
										<c:otherwise>
											<option value="lr">Liberia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ly"}'>
											<option value="ly" selected="selected">Libya</option>
										</c:when>
										<c:otherwise>
											<option value="ly">Libya</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "li"}'>
											<option value="li" selected="selected">Liechtenstein</option>
										</c:when>
										<c:otherwise>
											<option value="li">Liechtenstein</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lt"}'>
											<option value="lt" selected="selected">Lithuania</option>
										</c:when>
										<c:otherwise>
											<option value="lt">Lithuania</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lu"}'>
											<option value="lu" selected="selected">Luxembourg</option>
										</c:when>
										<c:otherwise>
											<option value="lu">Luxembourg</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mo"}'>
											<option value="mo" selected="selected">Macau</option>
										</c:when>
										<c:otherwise>
											<option value="mo">Macau</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mk"}'>
											<option value="mk" selected="selected">Macedonia</option>
										</c:when>
										<c:otherwise>
											<option value="mk">Macedonia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mg"}'>
											<option value="mg" selected="selected">Madagascar</option>
										</c:when>
										<c:otherwise>
											<option value="mg">Madagascar</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mw"}'>
											<option value="mw" selected="selected">Malawi</option>
										</c:when>
										<c:otherwise>
											<option value="mw">Malawi</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "my"}'>
											<option value="my" selected="selected">Malaysia</option>
										</c:when>
										<c:otherwise>
											<option value="my">Malaysia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mv"}'>
											<option value="mv" selected="selected">Maldives</option>
										</c:when>
										<c:otherwise>
											<option value="mv">Maldives</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ml"}'>
											<option value="ml" selected="selected">Mali</option>
										</c:when>
										<c:otherwise>
											<option value="ml">Mali</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mt"}'>
											<option value="mt" selected="selected">Malta</option>
										</c:when>
										<c:otherwise>
											<option value="mt">Malta</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mh"}'>
											<option value="mh" selected="selected">Marshall
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="mh">Marshall Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mr"}'>
											<option value="mr" selected="selected">Mauritania</option>
										</c:when>
										<c:otherwise>
											<option value="mr">Mauritania</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mu"}'>
											<option value="mu" selected="selected">Mauritius</option>
										</c:when>
										<c:otherwise>
											<option value="mu">Mauritius</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "yt"}'>
											<option value="yt" selected="selected">Mayotte</option>
										</c:when>
										<c:otherwise>
											<option value="yt">Mayotte</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mx"}'>
											<option value="mx" selected="selected">Mexico</option>
										</c:when>
										<c:otherwise>
											<option value="mx">Mexico</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "fm"}'>
											<option value="fm" selected="selected">Micronesia</option>
										</c:when>
										<c:otherwise>
											<option value="fm">Micronesia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "md"}'>
											<option value="md" selected="selected">Moldova</option>
										</c:when>
										<c:otherwise>
											<option value="md">Moldova</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mc"}'>
											<option value="mc" selected="selected">Monaco</option>
										</c:when>
										<c:otherwise>
											<option value="mc">Monaco</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mn"}'>
											<option value="mn" selected="selected">Mongolia</option>
										</c:when>
										<c:otherwise>
											<option value="mn">Mongolia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "me"}'>
											<option value="me" selected="selected">Montenegro</option>
										</c:when>
										<c:otherwise>
											<option value="me">Montenegro</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ms"}'>
											<option value="ms" selected="selected">Montserrat</option>
										</c:when>
										<c:otherwise>
											<option value="ms">Montserrat</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ma"}'>
											<option value="ma" selected="selected">Morocco</option>
										</c:when>
										<c:otherwise>
											<option value="ma">Morocco</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mz"}'>
											<option value="mz" selected="selected">Mozambique</option>
										</c:when>
										<c:otherwise>
											<option value="mz">Mozambique</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "na"}'>
											<option value="na" selected="selected">Namibia</option>
										</c:when>
										<c:otherwise>
											<option value="na">Namibia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "nr"}'>
											<option value="nr" selected="selected">Nauru</option>
										</c:when>
										<c:otherwise>
											<option value="nr">Nauru</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "np"}'>
											<option value="np" selected="selected">Nepal</option>
										</c:when>
										<c:otherwise>
											<option value="np">Nepal</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "nl"}'>
											<option value="nl" selected="selected">Netherlands</option>
										</c:when>
										<c:otherwise>
											<option value="nl">Netherlands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "an"}'>
											<option value="an" selected="selected">Netherlands
												Antilles</option>
										</c:when>
										<c:otherwise>
											<option value="an">Netherlands Antilles</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "nc"}'>
											<option value="nc" selected="selected">New Caledonia</option>
										</c:when>
										<c:otherwise>
											<option value="nc">New Caledonia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "nz"}'>
											<option value="nz" selected="selected">New Zealand</option>
										</c:when>
										<c:otherwise>
											<option value="nz">New Zealand</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ni"}'>
											<option value="ni" selected="selected">Nicaragua</option>
										</c:when>
										<c:otherwise>
											<option value="ni">Nicaragua</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ne"}'>
											<option value="ne" selected="selected">Niger</option>
										</c:when>
										<c:otherwise>
											<option value="ne">Niger</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ng"}'>
											<option value="ng" selected="selected">Nigeria</option>
										</c:when>
										<c:otherwise>
											<option value="ng">Nigeria</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "nu"}'>
											<option value="nu" selected="selected">Niue</option>
										</c:when>
										<c:otherwise>
											<option value="nu">Niue</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kp"}'>
											<option value="kp" selected="selected">North Korea</option>
										</c:when>
										<c:otherwise>
											<option value="kp">North Korea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mp"}'>
											<option value="mp" selected="selected">Northern
												Mariana Islands</option>
										</c:when>
										<c:otherwise>
											<option value="mp">Northern Mariana Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "no"}'>
											<option value="no" selected="selected">Norway</option>
										</c:when>
										<c:otherwise>
											<option value="no">Norway</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "om"}'>
											<option value="om" selected="selected">Oman</option>
										</c:when>
										<c:otherwise>
											<option value="om">Oman</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pk"}'>
											<option value="pk" selected="selected">Pakistan</option>
										</c:when>
										<c:otherwise>
											<option value="pk">Pakistan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pw"}'>
											<option value="pw" selected="selected">Palau</option>
										</c:when>
										<c:otherwise>
											<option value="pw">Palau</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pa"}'>
											<option value="pa" selected="selected">Panama</option>
										</c:when>
										<c:otherwise>
											<option value="pa">Panama</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pg"}'>
											<option value="pg" selected="selected">Papua New
												Guinea</option>
										</c:when>
										<c:otherwise>
											<option value="pg">Papua New Guinea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "py"}'>
											<option value="py" selected="selected">Paraguay</option>
										</c:when>
										<c:otherwise>
											<option value="py">Paraguay</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pe"}'>
											<option value="pe" selected="selected">Peru</option>
										</c:when>
										<c:otherwise>
											<option value="pe">Peru</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ph"}'>
											<option value="ph" selected="selected">Philippines</option>
										</c:when>
										<c:otherwise>
											<option value="ph">Philippines</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pn"}'>
											<option value="pn" selected="selected">Pitcairn
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="pn">Pitcairn Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pl"}'>
											<option value="pl" selected="selected">Poland</option>
										</c:when>
										<c:otherwise>
											<option value="pl">Poland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pt"}'>
											<option value="pt" selected="selected">Portugal</option>
										</c:when>
										<c:otherwise>
											<option value="pt">Portugal</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pr"}'>
											<option value="pr" selected="selected">Puerto Rico</option>
										</c:when>
										<c:otherwise>
											<option value="pr">Puerto Rico</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "qa"}'>
											<option value="qa" selected="selected">Qatar</option>
										</c:when>
										<c:otherwise>
											<option value="qa">Qatar</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "cg"}'>
											<option value="cg" selected="selected">Republic of
												the Congo</option>
										</c:when>
										<c:otherwise>
											<option value="cg">Republic of the Congo</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ro"}'>
											<option value="ro" selected="selected">Romania</option>
										</c:when>
										<c:otherwise>
											<option value="ro">Romania</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ru"}'>
											<option value="ru" selected="selected">Russia</option>
										</c:when>
										<c:otherwise>
											<option value="ru">Russia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "rw"}'>
											<option value="rw" selected="selected">Rwanda</option>
										</c:when>
										<c:otherwise>
											<option value="rw">Rwanda</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "bl"}'>
											<option value="bl" selected="selected">Saint
												Barthelemy</option>
										</c:when>
										<c:otherwise>
											<option value="bl">Saint Barthelemy</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sh"}'>
											<option value="sh" selected="selected">Saint Helena</option>
										</c:when>
										<c:otherwise>
											<option value="sh">Saint Helena</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kn"}'>
											<option value="kn" selected="selected">Saint Kitts
												and Nevis</option>
										</c:when>
										<c:otherwise>
											<option value="kn">Saint Kitts and Nevis</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lc"}'>
											<option value="lc" selected="selected">Saint Lucia</option>
										</c:when>
										<c:otherwise>
											<option value="lc">Saint Lucia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "mf"}'>
											<option value="mf" selected="selected">Saint Martin</option>
										</c:when>
										<c:otherwise>
											<option value="mf">Saint Martin</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "pm"}'>
											<option value="pm" selected="selected">Saint Pierre
												and Miquelon</option>
										</c:when>
										<c:otherwise>
											<option value="pm">Saint Pierre and Miquelon</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "vc"}'>
											<option value="vc" selected="selected">Saint Vincent
												and the Grenadines</option>
										</c:when>
										<c:otherwise>
											<option value="vc">Saint Vincent and the Grenadines</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ws"}'>
											<option value="ws" selected="selected">Samoa</option>
										</c:when>
										<c:otherwise>
											<option value="ws">Samoa</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sm"}'>
											<option value="sm" selected="selected">San Marino</option>
										</c:when>
										<c:otherwise>
											<option value="sm">San Marino</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "st"}'>
											<option value="st" selected="selected">Sao Tome and
												Principe</option>
										</c:when>
										<c:otherwise>
											<option value="st">Sao Tome and Principe</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sa"}'>
											<option value="sa" selected="selected">Saudi Arabia</option>
										</c:when>
										<c:otherwise>
											<option value="sa">Saudi Arabia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sn"}'>
											<option value="sn" selected="selected">Senegal</option>
										</c:when>
										<c:otherwise>
											<option value="sn">Senegal</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "rs"}'>
											<option value="rs" selected="selected">Serbia</option>
										</c:when>
										<c:otherwise>
											<option value="rs">Serbia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sc"}'>
											<option value="sc" selected="selected">Seychelles</option>
										</c:when>
										<c:otherwise>
											<option value="sc">Seychelles</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sl"}'>
											<option value="sl" selected="selected">Sierra Leone</option>
										</c:when>
										<c:otherwise>
											<option value="sl">Sierra Leone</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sg"}'>
											<option value="sg" selected="selected">Singapore</option>
										</c:when>
										<c:otherwise>
											<option value="sg">Singapore</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sk"}'>
											<option value="sk" selected="selected">Slovakia</option>
										</c:when>
										<c:otherwise>
											<option value="sk">Slovakia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "si"}'>
											<option value="si" selected="selected">Slovenia</option>
										</c:when>
										<c:otherwise>
											<option value="si">Slovenia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sb"}'>
											<option value="sb" selected="selected">Solomon
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="sb">Solomon Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "so"}'>
											<option value="so" selected="selected">Somalia</option>
										</c:when>
										<c:otherwise>
											<option value="so">Somalia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "za"}'>
											<option value="za" selected="selected">South Africa</option>
										</c:when>
										<c:otherwise>
											<option value="za">South Africa</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "kr"}'>
											<option value="kr" selected="selected">South Korea</option>
										</c:when>
										<c:otherwise>
											<option value="kr">South Korea</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "es"}'>
											<option value="es" selected="selected">Spain</option>
										</c:when>
										<c:otherwise>
											<option value="es">Spain</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "lk"}'>
											<option value="lk" selected="selected">Sri Lanka</option>
										</c:when>
										<c:otherwise>
											<option value="lk">Sri Lanka</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sd"}'>
											<option value="sd" selected="selected">Sudan</option>
										</c:when>
										<c:otherwise>
											<option value="sd">Sudan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sr"}'>
											<option value="sr" selected="selected">Suriname</option>
										</c:when>
										<c:otherwise>
											<option value="sr">Suriname</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sj"}'>
											<option value="sj" selected="selected">Svalbard</option>
										</c:when>
										<c:otherwise>
											<option value="sj">Svalbard</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sz"}'>
											<option value="sz" selected="selected">Swaziland</option>
										</c:when>
										<c:otherwise>
											<option value="sz">Swaziland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "se"}'>
											<option value="se" selected="selected">Sweden</option>
										</c:when>
										<c:otherwise>
											<option value="se">Sweden</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ch"}'>
											<option value="ch" selected="selected">Switzerland</option>
										</c:when>
										<c:otherwise>
											<option value="ch">Switzerland</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "sy"}'>
											<option value="sy" selected="selected">Syria</option>
										</c:when>
										<c:otherwise>
											<option value="sy">Syria</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tw"}'>
											<option value="tw" selected="selected">Taiwan</option>
										</c:when>
										<c:otherwise>
											<option value="tw">Taiwan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tj"}'>
											<option value="tj" selected="selected">Tajikistan</option>
										</c:when>
										<c:otherwise>
											<option value="tj">Tajikistan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tz"}'>
											<option value="tz" selected="selected">Tanzania</option>
										</c:when>
										<c:otherwise>
											<option value="tz">Tanzania</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "th"}'>
											<option value="th" selected="selected">Thailand</option>
										</c:when>
										<c:otherwise>
											<option value="th">Thailand</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tg"}'>
											<option value="tg" selected="selected">Togo</option>
										</c:when>
										<c:otherwise>
											<option value="tg">Togo</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tk"}'>
											<option value="tk" selected="selected">Tokelau</option>
										</c:when>
										<c:otherwise>
											<option value="tk">Tokelau</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "to"}'>
											<option value="to" selected="selected">Tonga</option>
										</c:when>
										<c:otherwise>
											<option value="to">Tonga</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tt"}'>
											<option value="tt" selected="selected">Trinidad and
												Tobago</option>
										</c:when>
										<c:otherwise>
											<option value="tt">Trinidad and Tobago</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tn"}'>
											<option value="tn" selected="selected">Tunisia</option>
										</c:when>
										<c:otherwise>
											<option value="tn">Tunisia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tr"}'>
											<option value="tr" selected="selected">Turkey</option>
										</c:when>
										<c:otherwise>
											<option value="tr">Turkey</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tm"}'>
											<option value="tm" selected="selected">Turkmenistan</option>
										</c:when>
										<c:otherwise>
											<option value="tm">Turkmenistan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tc"}'>
											<option value="tc" selected="selected">Turks and
												Caicos Islands</option>
										</c:when>
										<c:otherwise>
											<option value="tc">Turks and Caicos Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "tv"}'>
											<option value="tv" selected="selected">Tuvalu</option>
										</c:when>
										<c:otherwise>
											<option value="tv">Tuvalu</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ug"}'>
											<option value="ug" selected="selected">Uganda</option>
										</c:when>
										<c:otherwise>
											<option value="ug">Uganda</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ua"}'>
											<option value="ua" selected="selected">Ukraine</option>
										</c:when>
										<c:otherwise>
											<option value="ua">Ukraine</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ae"}'>
											<option value="ae" selected="selected">United Arab
												Emirates</option>
										</c:when>
										<c:otherwise>
											<option value="ae">United Arab Emirates</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "gb"}'>
											<option value="gb" selected="selected">United
												Kingdom</option>
										</c:when>
										<c:otherwise>
											<option value="gb">United Kingdom</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "us"}'>
											<option value="us" selected="selected">United States</option>
										</c:when>
										<c:otherwise>
											<option value="us">United States</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "uy"}'>
											<option value="uy" selected="selected">Uruguay</option>
										</c:when>
										<c:otherwise>
											<option value="uy">Uruguay</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "vi"}'>
											<option value="vi" selected="selected">US Virgin
												Islands</option>
										</c:when>
										<c:otherwise>
											<option value="vi">US Virgin Islands</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "uz"}'>
											<option value="uz" selected="selected">Uzbekistan</option>
										</c:when>
										<c:otherwise>
											<option value="uz">Uzbekistan</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "vu"}'>
											<option value="vu" selected="selected">Vanuatu</option>
										</c:when>
										<c:otherwise>
											<option value="vu">Vanuatu</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ve"}'>
											<option value="ve" selected="selected">Venezuela</option>
										</c:when>
										<c:otherwise>
											<option value="ve">Venezuela</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "vn"}'>
											<option value="vn" selected="selected">Vietnam</option>
										</c:when>
										<c:otherwise>
											<option value="vn">Vietnam</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "wf"}'>
											<option value="wf" selected="selected">Wallis and
												Futuna</option>
										</c:when>
										<c:otherwise>
											<option value="wf">Wallis and Futuna</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "eh"}'>
											<option value="eh" selected="selected">Western
												Sahara</option>
										</c:when>
										<c:otherwise>
											<option value="eh">Western Sahara</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "ye"}'>
											<option value="ye" selected="selected">Yemen</option>
										</c:when>
										<c:otherwise>
											<option value="ye">Yemen</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "zm"}'>
											<option value="zm" selected="selected">Zambia</option>
										</c:when>
										<c:otherwise>
											<option value="zm">Zambia</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test='${countryId == "zw"}'>
											<option value="zw" selected="selected">Zimbabwe</option>
										</c:when>
										<c:otherwise>
											<option value="zw">Zimbabwe</option>
										</c:otherwise>
									</c:choose>



								</select>
							</div>
						</div>
					</div>

					<div class="fullBlock"></div>

					<div class="fullBlock">
						<div class="input">
							<h6>
								<label for="partnerWebPage">If you know the partner web
									page please paste the link below. </label>
							</h6>
							<input type="text" id="partnerWebPage" name="partnerWebPage"
								value="${partnerWeb}" />
						</div>
					</div>

					<div class="halfPartBlock">
						<div class="input">
							<h6>
								<label for="activityPartner.partner.createdBy">Created
									By </label>
							</h6>
							<input type="text" id="activityPartner.partner.createdBy"
								name="createdBy" value="${createdBy}" />
						</div>
					</div>

					<!-- internal parameter -->
					<input type="hidden" id="activityPartner.partner.id"
						name="partnerId" value="${id}" /> <input type="hidden"
						id="token.id" name="tokenId" value="${tokenId}" /> <input
						name="activityID" type="hidden" value="1" /> <input type="submit"
						id="partnerSave_save" value="Approve" />
				</form>
			</article>
		</section>
	</div>
</body>