<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="./images/global/favicon.ico" />

<meta name="google-site-verification"
	content="uDt49ijI7uKxK60GeIDi2N1DedHr3hsomqFzE7ngwqw" />

<title>Add Comment</title>

<link rel="stylesheet" type="text/css" href="./css/global/reset.css" />
<link rel="stylesheet" type="text/css" href="./css/global/global.css" />

<link rel="stylesheet" type="text/css"
	href="./css/reporting/partnersReporting.css" />
</head>