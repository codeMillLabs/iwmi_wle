[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#macro activitiesList activities owned=true canValidate=false canEditActivity=false tableID=""]

  <table class="activityList" id="${tableID}">
  <thead>
    <tr>
      <th id="flagship">[@s.text name="planning.mainInformation.flagShipSRP" /]</th>
      <th id="id" style="width:30px;">[@s.text name="planning.activityList.id" /]</th>
      <th id="activity">[@s.text name="planning.activityList.activity" /]</th>
      [#if owned]
        <th id="contactPerson">[@s.text name="planning.activityList.contactPerson" /]</th>
      [#else]
        <th id="leaderName">[@s.text name="planning.activityList.leader" /]</th>
      [/#if]
      <th id="leadCenter">[@s.text name="planning.activityList.lead.center" /]</th>
     
      <th id="status">[@s.text name="planning.activityList.status" /]</th>
    </tr>
  </thead>
  <tbody>
    [#if activities?has_content]
      [#list activities as activity]
        [#if activity??]
        <tr>
          <td>
          	[#if activity.flagShipSRP?has_content]
          		${activity.flagShipSRP}
          	[#else]
              [@s.text name="planning.activityList.lead.center.undefined" /]
            [/#if]
          </td> 
          <td>
               <a href=" [@s.url action='activityClusterReport' includeParams='get']
                    [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param]
                    [@s.param name='${reviewMode}']mid[/@s.param]
                 [/@s.url]" >
                ${activity.formattedId}
               </a>  
          </td>
          <td class="left">
            [#if activity.title?has_content]
                    <a href=" [@s.url action='activityClusterReport' includeParams='get'] 
                          [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param] 
                          [@s.param name='${reviewMode}']mid[/@s.param]
                         [/@s.url]" >
		              [#if activity.title?length < 70] 
		                    ${activity.title} 
		              [#else] [@utilities.wordCutter string=activity.title maxPos=70 /]... 
		              
		              [/#if]
		              : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
		           </a> 
           [#else]
              [@s.text name="planning.activityList.title.undefined" /]
           [/#if]
          </td>
          <td>
            [#if owned]
              [#if activity.contactPersons?has_content]
                [#if activity.contactPersons[0].email?has_content]
                  <a href="mailto:${activity.contactPersons[0].email}">${activity.contactPersons[0].name}</a>
                [#else]
                  ${activity.contactPersons[0].name}
                [/#if]
              [#else]
                [@s.text name="planning.activityList.contactPerson.empty" /]
              [/#if]
            [#else]
              ${activity.leader.acronym}
            [/#if]
          </td>
          <td>
          	[#if activity.leadCenter?has_content]
          		${activity.leadCenter}
          	[#else]
              [@s.text name="planning.activityList.lead.center.undefined" /]
            [/#if]
          </td> 
          
          <td>
          	[#if activity.midYearReviews?has_content]
          	   	[#if activity.midYearReviews.stage == 1]
          		    Report not yet submitted
          	    [#elseif activity.midYearReviews.stage == 2]
          	        Draft report ready for review
          	    [#elseif activity.midYearReviews.stage == 3]
          	        Draft report reviewed
          	    [#elseif activity.midYearReviews.stage == 4]
          	        Final report submitted
          	    [#else]
          	        
          	    [/#if]
          	[#else]
              
            [/#if]
          </td>                  
        </tr>
        [/#if]
      [/#list]
    [#else]
      <tr>
        <td colspan="4">[@s.text name="planning.activityList.empty" /]</td>
      </tr>
    [/#if]
  </tbody>
</table>
        
[/#macro]