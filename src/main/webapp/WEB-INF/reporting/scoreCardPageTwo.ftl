[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Welcome to WLE Activity Planning" /]
[#assign globalLibs = ["jquery", "dataTable", "noty", "googleAPI"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js", "${baseUrl}/js/reporting/scoreCardPageTwo.js"] /]
[#assign customCSS = [] /]
[#assign currentSection = "reporting" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm /]
<section id="activityListPlanning" class="content">
[@s.form action="flagshipReport"] 

<article  class="fullContent">
  <div class="content">
  	<h1>${flagshipLongName}</h1>
  	<fieldset>
  			<legend class="bold-title">${scorecardMainPageFlagship.name} Flagship Overall Status</legend>
  			<input type="hidden" name="fspent" value="${scorecardMainPageFlagship.spent}"> 	
	      	<input type="hidden" name="funspent" value="${scorecardMainPageFlagship.notSpent}"> 
	      	<input type="hidden" name="fgreen" value="${scorecardMainPageFlagship.green}">
	      	<input type="hidden" name="fyellow" value="${scorecardMainPageFlagship.yellow}">
	      	<input type="hidden" name="fred" value="${scorecardMainPageFlagship.red}">	
  			<div> 
		      	<div style="float:left;width:30%" id="foutput-rating"></div>   
		      	<div style="float:left;width:30%;margin-top:10px" id="fchart_div"></div> 
		      	<div style="float:left;width:35%;margin-top:35px;margin-left:30px">
		      	    <label class="bold-title">Flagship Comment</label>
		      		<textarea readonly="readonly" rows="10" style="height:auto">${scorecardMainPageFlagship.toolTip}</textarea>
		      	</div>
	      	</div> 
  	</fieldset>
  	<br/>
  	<div class="bold-title">Activity Clusters</div><br/>
  	[#assign count = 1] 
  	[#list clusterDataSummaries as clusterDataSummary]
  		<fieldset>
    		<legend class="bold-title">${clusterDataSummary.clusterName}</legend>
	      	  
	      	<input type="hidden" name="spent${count}" value="${clusterDataSummary.expenditure.spent}"> 	
	      	<input type="hidden" name="unspent${count}" value="${clusterDataSummary.expenditure.unspent}"> 
	      	<input type="hidden" name="green${count}" value="${clusterDataSummary.outputStatus.green}">
	      	<input type="hidden" name="yellow${count}" value="${clusterDataSummary.outputStatus.yellow}">
	      	<input type="hidden" name="red${count}" value="${clusterDataSummary.outputStatus.red}">	
	      	<input type="hidden" name="pgreen${count}" value="${clusterDataSummary.projectStatus.green}">
	      	<input type="hidden" name="pyellow${count}" value="${clusterDataSummary.projectStatus.yellow}">
	      	<input type="hidden" name="pred${count}" value="${clusterDataSummary.projectStatus.red}">	
	      	<div> 
	      		[#if clusterDataSummary.clusterRating == "-1"] <div class="bold-title">No cluster rating defined</div>[/#if]	      	
	      		[#if clusterDataSummary.clusterRating == "0"] <div class="scorecard-green bold-title">Cluster Rating : (On track against all deliverables)</div>[/#if]
				[#if clusterDataSummary.clusterRating == "1"] <div class="scorecard-yellow bold-title">Cluster Rating : (On track with most but not all deliverables)</div>[/#if]
				[#if clusterDataSummary.clusterRating == "2"] <div class="scorecard-red bold-title">Cluster Rating : (Off track with most deliverables)</div>[/#if]
	      	</div> 
	      	<div> 
		      	<div style="float:left;width:20%" id="output-rating${count}"></div>   
		      	<div style="float:left;width:20%" id="project-rating${count}"></div>   
		      	<div style="float:left;width:20%;margin-top:10px" id="chart_div${count}"></div> 
		      	<div style="float:left;width:35%;margin-top:35px;margin-left:30px">
		      	    <label class="bold-title">Activity Cluster Review Comments</label>
		      		<textarea readonly="readonly" rows="10" style="height:auto">${clusterDataSummary.comment}</textarea>
		      	</div>
	      	</div>
	      	[#assign count = count + 1] 
      	</fieldset>
  	[/#list]
  	<input type="hidden" name="count" value="${count}">
  	
  	<fieldset>
    		<legend class="bold-title">Projects</legend>
  	[#list activityIds as activityId]
  			<a href="[@s.url action='scoreCardThree'] [@s.param name='activityId']${activityId}[/@s.param] [/@s.url]" title="${activityId}">${activityId}</a> | 
  	[/#list]
  	</fieldset>
  	
  </div>
</article>

 [/@s.form] 


  </section>

[#include "/WEB-INF/global/pages/footer.ftl"]