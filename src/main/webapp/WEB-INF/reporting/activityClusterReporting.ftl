[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Welcome to WLE Activity Planning" /]
[#assign globalLibs = ["jquery", "dataTable", "noty"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js",  "${baseUrl}/js/wz_tooltip.js" "${baseUrl}/js/reporting/activity-list.js", "${baseUrl}/js/reporting/activityClusterReporting.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/libs/dataTables/jquery.dataTables-1.9.4.css", "${baseUrl}/css/global/customDataTable.css", "${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "reporting" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm /]
<section id="activityClusterReport" class="content">
<div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
     [#if canPublish ] 
        <p> [@s.text name="reporting.form.publish.help" /] </p>
     [#else]
        <p> [@s.text name="reporting.form.help" /] </p>
     [/#if]
  </div>
[@s.form action="activityClusterReport"]

[#-- SECTION 1. PROJECT DETAILS AND OVERALL PROGRESS --]

<article  class="fullContent">
  <div>
    <h1>[@s.text name="reporting.introduction.title" /]</h1>
    [#if activity.midYearReviews.viewable ] 
    <div id="activity_cluster_review">
         <table class="review-table" width="100%">
            <tr class="reviewTableHeader">
      			<th colspan="4">
			       <span>SECTION 1.  PROJECT DETAILS</span>
			       <span >
			      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
			      			onmouseover="Tip('The term &quot;Project&quot; is being used for what were formerly known as &quot;Activities&quot;')"
			      			onmouseout="UnTip()"/>
			      </span> 
      			</th>
    		</tr>
    		<tr>
    		 <td class="review_lable_bg"><span class="review_lable">Project Name:</span></td>
    		 <td class="autofil_bg">
    		    [#if activity.title??]  ${activity.title}  [#else] N\A [/#if]
    		 </td>
    		</tr>
    		<tr>
    		 <td class="review_lable_bg"><span class="review_lable">Project Leader:</span></td>
    		 <td class="autofil_bg">
    		    [#if activity.createdUser??]  ${activity.createdUser}  [#else] N\A [/#if]
    		 </td>
    		</tr>
    		<tr>
    		 <td class="review_lable_bg"><span class="review_lable">Flagship:</span></td>
    		 <td class="autofil_bg">
    		    [#if activity.flagShipSRP??]  ${activity.flagShipSRP}  [#else] N\A [/#if]
    		 </td>
    		</tr>
    		<tr>
    		 <td class="review_lable_bg"><span class="review_lable">Activity Cluster:</span></td>
    		  <td class="autofil_bg">
    		    [#if activity.activityCluster??]  ${activity.activityCluster}  [#else] N\A [/#if]
    		  </td>
    		</tr> 
    		<tr>
    		 <td class="review_lable_bg"><span class="review_lable">Reporting Period:</span></td>
    		 <td class="autofil_bg">
    		   <div>
    		    <span>From : </span> ${periodStart}
    		    <span>To : </span> ${periodEnd}
    		   </div>
    		 </td>
    		</tr>
         </table>
         <br/>

[#-- SECTION 2. OUTPUT PROGRESS --]

         <table  class="review-table" width="100%">
            <tr class="reviewTableHeader">
      			<th colspan="4">SECTION 2.  OUTPUT PROGRESS <small>(one table per output)</small></th>
    		</tr>
    		[#if activity.deliverables?has_content]
            	[#list activity.deliverables as deliverable]
                  <tr>

                       <table  class="review-table" width="100%">
            		  	<tr>
            		     <td class="review_lable_bg autofil_bg" colspan = "2">
            		       <span class="review_lable">
            		        Output 	[#if deliverable.deliverableCode??] (${deliverable.deliverableCode}) [/#if]  :
						  </span>
            		      <p>  [#if deliverable.description??]  ${deliverable.description} [#else] N\A [/#if] </p>
            		     </td>
            		    </tr>
            		    <tr>
            		      <td class="review_lable_bg" colspan = "2">
            		           <span class="review_lable">Output Type – <small>Select the most appropriate output type from the following list and complete sub-categories as they appear.</small></span>
							</br>
							</br>
            		        <div id="column-left" class="column-left-grid">
        				     <!-- hidden -->
            		        <input name="activity.midYearReviews.deliverableRating[${deliverable_index}].deliverableId" type="hidden" value="${deliverable.id}" />

            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="tool" id="tool_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "tool" ]
            		                    checked
            		                [/#if]
							     [/#if]>
							     Tool
                                  <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('Tool types are based on CGIAR/USAID indicators and include materials<br/> to support capacity building such as training manuals.  <br/>Blogs, press releases, policy briefs or other types of evidence should be available on the tool.<br/>  Note that a policy brief/blog/press release itself is not considered a tool')"
							      			onmouseout="UnTip()"/>
							      </span> 							   
							 </div>

            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="policy progress" id="policy_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "policy progress" ]
            		                    checked
            		                 [/#if]
							     [/#if]> 
							     Policy Process
							      <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('Includes policies, regulations, guidelines and procedures of a government body or third party<br/> organization which has jurisdiction over resources or a particular region')"
							      			onmouseout="UnTip()"/>
							      </span> 	
							     
							</div>
            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="isi publication"  id="publication_${deliverable_index}"
                                [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "isi publication" ]
            		                    checked
            		                 [/#if]
							     [/#if]> 
							     Publication
							      <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('“Open Access” is broadly defined as the unrestricted access to research results,<br/> and unrestricted rights to reuse those results. For details on the CGIAR Open Access Policy')"
							      			onmouseout="UnTip()"/>
							      </span> 	
							     
							</div>

            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="platform"  id="platform_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                     data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "platform" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Multi-Stakeholder Platform
							     <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('A multi-stakeholder platform may also be known as a forum or<br/> dialogue process which provides a setting for participatory decision<br/> making and management of the differing interests of stakeholders.<br/> A multi stakeholder  platform has inclusive and clear governance mechanisms, and would<br/> result in clear decisions to manage the perspectives of different stakeholders')"
							      			onmouseout="UnTip()"/>
							      </span> 
							</div>
            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="database" id="database_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "database" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Database
							     <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('“Open Access” is broadly defined as the unrestricted access to research results,<br/> and unrestricted rights to reuse those results. For details on the CGIAR Open Access Policy')"
							      			onmouseout="UnTip()"/>
							      </span>      
							</div>
							
							<div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="dataset" id="dataset_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "dataset" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Dataset
							     <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('“Open Access” is broadly defined as the unrestricted access to research results,<br/> and unrestricted rights to reuse those results. For details on the CGIAR Open Access Policy')"
							      			onmouseout="UnTip()"/>
							      </span>      
							</div>
							     
						    <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="capacity development" id="capacity-development_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                     data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "capacity development" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Capacity Development
							     <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('“Degree training” can be support to field research, thesis writing or other types<br/> of support. It does not necessarily refer to the payment of course fees')"
							      			onmouseout="UnTip()"/>
							      </span> 
							</div>	
							     
							     
							<div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="technology" id="technology_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                    data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "technology" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Technology/Practice
							      <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('“Type of Technology” Agriculture-related and NRM-related technologies and innovations. <br/> “Stage of development” Select the stage at which the technology/practice is expected to be<br/> by the end of the current year<br/>“Field testing” means that research has moved from focused development to broader<br/> testing and this testing is underway under conditions intended to duplicate<br/> those encountered by potential users')"
							      			onmouseout="UnTip()"/>
							      </span>              
							</div>
							     
            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="communications" id="communications_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                     data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "communications" ]
            		                    checked
            		                [/#if]
							     [/#if]> 
							     Communications
							      <span >
							      		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
							      			onmouseover="Tip('Communications deliverables eg policy briefs, blog posts etc. and outreach<br/> activities such as stakeholder consultations that have not been reported elsewhere')"
							      			onmouseout="UnTip()"/>
							      </span>     
							</div>

            		        <div class="grid"><input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].outputType" value="other" id="other_${deliverable_index}"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
                                     data="'${activity.midYearReviews.deliverableRating[deliverable_index]}'"
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].outputType?? && activity.midYearReviews.deliverableRating[deliverable_index].outputType?lower_case = "other" ]
            		                    checked
            		                [/#if]
							     [/#if]> Other</div>
            		        
            		        </div>
            		        [#-- OUTPUT TOOL --]
            		        <div id="content_${deliverable_index}" class="column-center-grid"></div>

            		        <br/>
            		      </td>
            		    </tr>
            		    <tr>
            		     <td class="review_lable_bg">
            		       <span class="review_lable">Progress towards output at mid-year point <small>(Max 1000 characters) (including milestones achieved or missed, and explanation for status. Select green, amber or red below)</small></span>
            		     </td>
            		     <td class="review_lable_bg">
            		        <textarea name="activity.midYearReviews.deliverableRating[${deliverable_index}].midYearReviewProgress" rows="4" cols="120" maxlength="1000" >
            		         [#if activity.midYearReviews.deliverableRating?has_content]
            		           [#if activity.midYearReviews.deliverableRating[deliverable_index].deliverableId ==  deliverable.id]
            		              [#if activity.midYearReviews.deliverableRating[deliverable_index].midYearReviewProgress?has_content]
            		                ${activity.midYearReviews.deliverableRating[deliverable_index].midYearReviewProgress}
            		              [/#if]  
            		           [/#if]
							 [/#if]
						    </textarea>
            		     </td>
            		    </tr>
            		    <tr>
            		     <td class="review_lable_bg" colspan = "2">
            		        <div class="column-left">
        			          <div class="review_rating_row">
        			          	<div class="review_rating_color_box review_lable_green">
        			          	<input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].midYearReviewRating" value="0"
                                [#if activity.midYearReviews.deliverableRating?has_content]
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].midYearReviewRating ==  0]
            		                    checked
            		                 [/#if]
							     [/#if]
        			          	>Green</div>
        			            <div> (Fully on track to be delivered by year-end or already delivered)</div>
        			          </div>
        			        </div>
        			        <div class="column-center">
        			          <div  class="review_rating_row">
        			          	<div class="review_rating_color_box review_lable_amber">
        			          	<input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].midYearReviewRating" value="1"
                                [#if activity.midYearReviews.deliverableRating?has_content]
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].midYearReviewRating ==  1]
            		                    checked
            		                 [/#if]
							     [/#if]> Amber</div>
        			            <div> (Likely to be delivered by year-end at current trajectory, but may need a bit more attention)</div>
        			          </div>
        			        </div>
        			         <div class="column-right">
        			          <div  class=" review_rating_row">
        			          	<div class="review_rating_color_box review_lable_red">
        			          	<input type="radio" name="activity.midYearReviews.deliverableRating[${deliverable_index}].midYearReviewRating" value="2"
                                 [#if activity.midYearReviews.deliverableRating?has_content]
            		                [#if activity.midYearReviews.deliverableRating[deliverable_index].midYearReviewRating ==  2]
            		                    checked
            		                 [/#if]
							     [/#if]> Red</div>
        			            <div> (Unlikely to be delivered by year-end at current trajectory) </div>
        			          </div>
        			         </div>
            		     </td>
            		    </tr>
            		    <tr class="review_feedback_block">
			              <td colspan="1" class="review_lable_bg">
			    		    <div>
			    		       <span class="review_lable">Review Comments:
			    		       <small></small>
			    		       </span> 
			    		    </div>
			    		  </td>
						  <td  colspan="3" class="review_lable_bg">
			    		    <div>
			    		        <textarea name="activity.midYearReviews.deliverableRating[${deliverable_index}].comment" rows="4" cols="120" maxlength="1000" class="review_feedback" >
		            		         [#if activity.midYearReviews.deliverableRating?has_content]
		            		           [#if activity.midYearReviews.deliverableRating[deliverable_index].deliverableId ==  deliverable.id]
		            		              [#if activity.midYearReviews.deliverableRating[deliverable_index].comment?has_content]
		            		                ${activity.midYearReviews.deliverableRating[deliverable_index].comment}
		            		              [/#if]  
		            		           [/#if]
									 [/#if]
						    	</textarea>
			    		    </div>
			    		  </td>
			            </tr>
            		  </table>
            		</tr>
            		<br/>

            	[/#list]
            [/#if]

    	 </table>

         <br/>
          <table  class="review-table" width="100%">
            <tr class="reviewTableHeader">
      			<th colspan="4">SECTION 3. SPENDING AND OVERALL PROGRESS</th>
    		</tr>
    		<tr>
    		    <td class="review_lable_bg">
    		       <span class="review_lable">ACTIVITY BUDGET FOR YEAR</span>
    		     </td>
    		     <td class="autofil_bg">[#if activity.midYearReviews.totalFunding??]  ${activity.midYearReviews.totalFunding}  [#else] N\A [/#if]
    		     </td>
    		     <td class="review_lable_bg">
    		       <span class="review_lable">SPENT BY END JUNE</span>
      			    <span>
						<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
								onmouseover="Tip('WLE requests the project leaders provide a draft expenditure figure based on<br/> recorded expenditures to date. This is important to estimate progress.<br/> This is not an official financial report and will not be<br/> used in any financial statement to the CGIAR')"
								onmouseout="UnTip()"/>
				    </span>  
    		     </td>
    		     <td class="review_lable_bg">
    		     	<input type="text" id="activity.midYearReviews.totalSpent" name="activity.midYearReviews.totalSpent" class="totalSpent"
    		     	[#if activity.midYearReviews.totalSpent??]
                        value="${activity.midYearReviews.totalSpent}"
                    [/#if]>
    		     </td>
            </tr>
            <tr>
              <td colspan="2" class="review_lable_bg">
    		    <div>
    		       <span class="review_lable">Overall Project Status:
    		       <small>How is the project performing against the project agreement and/or plan?  Highlight any deviations in cost, timescales, quality, scope, products, risk, or benefits. (Max 2000 characters)</small>
    		       </span> 
    		    </div>
    		  </td>
			  <td  colspan="2" class="review_lable_bg">
    		    <div>
    		       <textarea name="activity.midYearReviews.midYearReviewProgress" rows="4" cols="120"   maxlength="2000" >
    		        [#if activity.midYearReviews.midYearReviewProgress?has_content]  
    		          ${activity.midYearReviews.midYearReviewProgress}
    		        [/#if]
    		       </textarea>
    		    </div>
    		  </td>
            </tr>
            <tr>
               <td class="review_lable_bg" colspan = "4">
                  <div class="review_lable_bg"><span class="review_lable">Overall Project Rating:</span><br/>
                       <small>Please indicate overall status assessment. Select one after completion of output section</small> 
                  </div>
                  <div class="column-left">
                      <div class="review_rating_row">
                      <div class="review_rating_color_box review_lable_green"><input type="radio" name="activity.midYearReviews.midYearReviewRating" value="0"
                        [#if activity.midYearReviews.midYearReviewRating??]
            		       [#if activity.midYearReviews.midYearReviewRating ==  0]
            		            checked
            		       [/#if]
						[/#if]> Green</div>
                    <div> (On track against all outputs)</div>
                    </div>
                  </div>
                  <div class="column-center">
                       <div  class="review_rating_row">
                        <div class="review_rating_color_box review_lable_amber"><input type="radio" name="activity.midYearReviews.midYearReviewRating" value="1"
                        [#if activity.midYearReviews.midYearReviewRating??]
            		       [#if activity.midYearReviews.midYearReviewRating ==  1]
            		            checked
            		       [/#if]
						[/#if]> Amber</div>
                       <div> (On track with most but not all outputs)</div>
                       </div>
                      </div>
                   <div class="column-right">
                    	<div  class="review_rating_row">
                    	 <div class="review_rating_color_box review_lable_red"><input type="radio" name="activity.midYearReviews.midYearReviewRating" value="2"
                        [#if activity.midYearReviews.midYearReviewRating??]
            		       [#if activity.midYearReviews.midYearReviewRating ==  2]
            		            checked
            		       [/#if]
						[/#if]> Red</div>
                    	<div> (Off track with most outputs)</div>
                   </div>
                   </div>
                </td>
             </tr>
             <tr class="review_feedback_block">
	              <td colspan="1" class="review_lable_bg">
	    		    <div>
	    		       <span class="review_lable">Review Comments:
	    		       <small></small>
	    		       </span> 
	    		    </div>
	    		  </td>
				  <td  colspan="3" class="review_lable_bg">
	    		    <div>
	    		        <textarea name="activity.midYearReviews.comment" rows="4" cols="120" maxlength="1000" class="review_feedback" >
	        		           [#if activity.midYearReviews.comment?has_content]
	        		                ${activity.midYearReviews.comment}
	        		           [/#if]
				    	</textarea>
	    		    </div>
	    		  </td>
            </tr>
           </table>
           <div class="clear-both">&nbsp;</div>
			
	
           <!-- internal parameter -->
  		   <input name="activityID" type="hidden" class="hiddenVal" value="${activity.id?c}" />
  		   <input name="activity.midYearReviews.activityId" type="hidden" class="hiddenVal" value="${activity.id?c}" />
  		   <input name="buttonAction" id="buttonAction" type="hidden" class="hiddenVal" value="-1" />
  		   <input name="viewable" id="viewable" type="hidden" class="hiddenVal" value="${activity.midYearReviews.viewable?string}" />
		   <input name="editable" id="editable" type="hidden" class="hiddenVal" value="${activity.midYearReviews.editable?string}" />
		   <input name="reviewable" id="reviewable" type="hidden" class="hiddenVal" value="${activity.midYearReviews.reviewable?string}" />
		   <input name="publishable" id="publishable" type="hidden" class="hiddenVal" value="${activity.midYearReviews.publishable?string}" />
  		   <input name="published" id="published" type="hidden" class="hiddenVal" value="${activity.midYearReviews.published?string}" />
  		  
              <div class="buttons">
                 [#if activity.midYearReviews.editable || reviewSubmit ] 
                   [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
                 [/#if] 
                 [#if canSendDraft ] 
                     [@s.submit type="button" name="sendDraft"][@s.text name="reporting.form.send.darft" /][/@s.submit]
                 [/#if] 
                 [#if reviewSubmit ] 
                     [@s.submit type="button" name="confirm"][@s.text name="reporting.form.review_done" /][/@s.submit]
                 [/#if]  
                 [#if canPublish ] 
                     [@s.submit type="button" name="publish"][@s.text name="reporting.form.publish" /][/@s.submit]
                 [/#if] 
                 [#if published || (!canSendDraft && !reviewSubmit)]
                     [@s.submit type="button" name="cancel"][@s.text name="form.buttons.back" /][/@s.submit]
                 [#else]
                     [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
                 [/#if] 
                  
              </div>
     </div>
     [#else] 
        No permission to view or Still Report is not published
     [/#if]
      
  </div>
</article>
[/@s.form]
  </section>
[#include "/WEB-INF/global/pages/footer.ftl"]