[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Welcome to WLE Activity Planning" /]
[#assign globalLibs = ["jquery", "dataTable", "noty", "googleAPI"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js",  "${baseUrl}/js/wz_tooltip.js" "${baseUrl}/js/reporting/activity-list.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/libs/dataTables/jquery.dataTables-1.9.4.css", "${baseUrl}/css/global/customDataTable.css", "${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "reporting" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/reporting/activitiesList.ftl" as activityList/]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm /]
[@s.form action="introduction"] 
<section id="activityListPlanning" class="content">
<article  class="fullContent">
  <div>
    <h1>[@s.text name="reporting.introduction.title" /]</h1>
     <div id="activityTables">
      <ul>
          <li><a href="#activityTables-1"> [@s.text name="reporting.activityList.midYear" /]</a></li>
          [#--<li><a href="#activityTables-2"> [@s.text name="reporting.activityList.yearEnd" /]</a></li>--]
      </ul>
      
      [#-- Mid Year Report --]      
      <div id="activityTables-1" class="activityTable">
	       	<div id="activityTables2">
     			<ul>
     			    <li><a href="#activityTables-5">Dashboard</a></li>
          			[#-- <li><a href="#activityTables-3"> [@s.text name="reporting.activity.report.caption" /]</a></li>
          			[#if flagshipLeader]
          			   <input type="hidden" name="redirectURL" value="${redirectionURL}" />
          				<li><a href="#activityTables-4" id="report-link">[@s.text name="reporting.flagship.report.caption" /]</a></li>
          			[#else]
          				<li><a href="#activityTables-4"> [@s.text name="reporting.flagship.report.caption" /]</a></li>
          			[/#if]--]
      			</ul>
      			
      		[#-- Activities Table --]    	
      		[#-- <div id="activityTables-3" class="activityTable">
          
               [#if yearActivities?has_content]
                  [@activityList.activitiesList activities=yearActivities canValidate=true canEditActivity=true tableID="yearActivities" /]
                  
               [#else]
		          <div class="noActivities">
		            [@s.text name="planning.activityList.empty" /]
		          </div>
		       [/#if]
      		</div>
      		
      		[#if flagshipLeader]
      			<div id="activityTables-4" class="activityTable">
      				loading flagship report ...
      			</div>
      		[#else]
      			<div id="activityTables-4" class="activityTable">--]
	      			 [#-- Year 
	    			 <div class="halfPartBlock">
	      			 [@customForm.select name="currentYear.displayName1" label="" i18nkey="planning.mainInformation.year" 
	       					listName="yearLookup" keyFieldName="value"  displayFieldName="displayName" value="currentYear.displayName" /] 
	    		     </div>--][#-- 
					<table aria-describedby="currentActivities_info" class="activityList dataTable" id="currentActivities">
					  <thead>
					    <tr role="row">
					    	<th aria-label="ID: activate to sort column descending" aria-sort="ascending" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting_asc" id="id" style="width: 100px;">FS/ FR No. </th>
					        <th aria-label="Activity: activate to sort column ascending" style="width: 210px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="activity">Flagship/Focal Region</th>
					        <th aria-label="Contact Person: activate to sort column ascending" style="width: 120px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="contactPerson">Leader A</th>
					        <th aria-label="Lead Center: activate to sort column ascending" style="width: 120px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="leadCenter">Leader B</th>
					        <th aria-label="Lead Center: activate to sort column ascending" style="width: 120px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="leadCenter">Status</th>
					    </tr>
					  </thead>  
					  <tbody aria-relevant="all" aria-live="polite" role="alert">
					  	[#list flagshipFocalRegions as flagshipFocalRegion]
					  		<tr class="odd">
					          <td class="  sorting_1">${flagshipFocalRegion.flagshipFocalRegion.fsFrNumber}</td>
					          <td class="  sorting_1 fsfr1"><a href=" [@s.url action='flagshipReport' includeParams='get'][@s.param name='flagship']${flagshipFocalRegion.flagshipFocalRegion.id}[/@s.param] [@s.param name='period']mid[/@s.param] [@s.param name='year']${currentYear.displayName}[/@s.param] [/@s.url]">${flagshipFocalRegion.flagshipFocalRegion.name}</a></td>
					          <td class="  sorting_1">${flagshipFocalRegion.flagshipFocalRegion.leaderA}</td>
					          <td class="  sorting_1">${flagshipFocalRegion.flagshipFocalRegion.leaderB}</td>
					          <td class="  sorting_1">${flagshipFocalRegion.status}</td>
					        </tr>
					  	[/#list]
					 	</tbody>
					</table>
      			</div>
      		[/#if]--]
      		
      		[#-- Dashboard --]    	
      		<div id="activityTables-5" class="activityTable">
      			<h1>WLE Half-annual Performance Scorecard</h1>
      			<input type="hidden" name="spent" value="${scoreCardPageOne.expenditure.spent}"> 	
	      		<input type="hidden" name="unspent" value="${scoreCardPageOne.expenditure.unspent}"> 
	      		<input type="hidden" name="green" value="${scoreCardPageOne.clusterStatus.green}">
	      		<input type="hidden" name="yellow" value="${scoreCardPageOne.clusterStatus.yellow}">
	      		<input type="hidden" name="red" value="${scoreCardPageOne.clusterStatus.red}">	
	      		<input type="hidden" name="pgreen" value="${scoreCardPageOne.projectStatus.green}">
	      		<input type="hidden" name="pyellow" value="${scoreCardPageOne.projectStatus.yellow}">
	      		<input type="hidden" name="pred" value="${scoreCardPageOne.projectStatus.red}">	
      			<fieldset style="float:left;width:70%;height:280px">
    				<legend class="bold-title">Overall Summary</legend>
    				<div>
    					<div style="float:left;font-weight:bold;text-decoration:underline;font-size:11px;width:60%">Project and Output Status</div>
    					<div style="float:left;font-weight:bold;text-decoration:underline;font-size:11px;width:40%">Proportion of Annual Budget Spent</div>
    				<div>
    				<div style="float:left;width:30%" id="cluster-rating"></div>  
    				<div style="float:left;width:30%" id="project-rating"></div>   
		      	    <div style="float:left;width:40%" id="chart_div"></div> 
    			</fieldset>
    			<fieldset style="float:left;width:19%;height:280px">
    				<legend class="bold-title">Source Info</legend>
    				<label class="bold-title">Total # Activity Clusters</label><br/>
    				<label><u>${scoreCardPageOne.activityClusterCount}</label></u><br/><br/><br/>
    				<label class="bold-title">Total # Activities </label><br/>
    				<label><u>${scoreCardPageOne.activityCount}</label></u><br/><br/><br/>
    				<label class="bold-title">Total # Outputs </label> <br/>
    				<label><u>${scoreCardPageOne.outputCount}</label></u><br/><br/><br/>
    				<label class="bold-title">Total # Cluster Outputs </label> <br/>
    				<label><u>${scoreCardPageOne.clusterOutputCount}</label></u><br/><br/><br/>
    			</fieldset>
    			<div style="clear:both;"></div>    			
      			<fieldset>
      				<legend class="bold-title">Flagships</legend>
      				<div>
    					<div style="font-weight:bold;text-decoration:underline;font-size:11px;text-align:center">Status of Projects at Half-Year</div>
    				<div>
      				[#assign count = 1] 
	      			[#list scoreCardPageOne.flagshipSummaries as flagshipSummary]
	      				<div>
							<input type="hidden" name="spent${count}" value="${flagshipSummary.expenditure.spent}"> 	
		      				<input type="hidden" name="unspent${count}" value="${flagshipSummary.expenditure.unspent}"> 
		      				<input type="hidden" name="green${count}" value="${flagshipSummary.status.green}">
		      				<input type="hidden" name="yellow${count}" value="${flagshipSummary.status.yellow}">
		      				<input type="hidden" name="red${count}" value="${flagshipSummary.status.red}">	
		      				<input type="hidden" name="short-name${count}" value="${flagshipSummary.shortName}">	
		      				
		      				<div  style="float:left;text-align:center;">
								[#if flagshipSummary.available]
									<div id="rating${count}"></div>
									<a style="text-decoration: underline;" href="[@s.url action='scoreCardTwo' includeParams='get'] [@s.param name='flagshipId']${flagshipSummary.flagshipId?c}[/@s.param][/@s.url]">${flagshipSummary.shortName}</a>
								[#else]
									<div id="rating${count}"></div>
									${flagshipSummary.shortName}
								[/#if]
								
								<div id="chart_div${count}"></div> 
							</div>
						</div>
						[#if count=5]
							<div style="clear:both;"></div>
	      					<div>
    							<div style="font-weight:bold;text-decoration:underline;font-size:11px;text-align:center">Proportion of Annual Budget Spent</div>
    						<div>
						[/#if]
						
	      				[#assign count = count + 1] 
	      			[/#list]
	      			<div style="clear:both;"></div>
	      					<div>
    							<div style="font-weight:bold;text-decoration:underline;font-size:11px;text-align:center">Proportion of Annual Budget Spent</div>
    				<div>
      			</fieldset>
      			<input type="hidden" name="count" value="${count}">
      			<div class="bold-title subhelpMessage1">
      				<ul>
      					<li>Click on the towers to view drilldown report</li>
      					<li>Hover on Tower / Pie Chart to exact percentage</li>
      				</ul>
      			</div>
      		</div>
      		</div>
      		
      </div>
      
      [#-- Year End Report --]      
      [#--<div id="activityTables-2" class="activityTable">
			<div id="activityTables3">
     			<ul>
          			<li><a href="#activityTables-5"> [@s.text name="reporting.activity.report.caption" /]</a></li>
          			<li><a href="#activityTables-6"> [@s.text name="reporting.flagship.report.caption" /]</a></li>
      			</ul>
     			      
      		<div id="activityTables-5" class="activityTable">
        
      		</div>
      		<div id="activityTables-6" class="activityTable">
    			 <div class="halfPartBlock">
      			 [@customForm.select name="currentYear.displayName2" label="" i18nkey="planning.mainInformation.year" 
       					listName="yearLookup" keyFieldName="value"  displayFieldName="displayName" value="currentYear.displayName" /] 
    		     </div>
				<table aria-describedby="currentActivities_info" class="activityList dataTable" id="currentActivities">
				  <thead>
				    <tr role="row">
				    	<th aria-label="ID: activate to sort column descending" aria-sort="ascending" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting_asc" id="id" style="width: 100px;">FS/ FR No. </th>
				        <th aria-label="Activity: activate to sort column ascending" style="width: 210px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="activity">Flagship/Focal Region</th>
				        <th aria-label="Contact Person: activate to sort column ascending" style="width: 120px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="contactPerson">Leader A</th>
				        <th aria-label="Lead Center: activate to sort column ascending" style="width: 120px;" colspan="1" rowspan="1" aria-controls="currentActivities" tabindex="0" role="columnheader" class="sorting" id="leadCenter">Leader B</th>
				    </tr>
				  </thead>  
				  [#list flagshipFocalRegions as flagshipFocalRegion]
				  		<tr class="odd">
				          <td class="  sorting_1">${flagshipFocalRegion.fsFrNumber}</td>
				          <td class="  sorting_1 fsfr2"><a href=" [@s.url action='flagshipReport' includeParams='get'][@s.param name='flagship']${flagshipFocalRegion.id}[/@s.param] [@s.param name='period']end[/@s.param] [@s.param name='year']${currentYear.displayName}[/@s.param] [/@s.url]">${flagshipFocalRegion.name}</a></td>
				          <td class="  sorting_1">${flagshipFocalRegion.leaderA}</td>
				          <td class="  sorting_1">${flagshipFocalRegion.leaderB}</td>
				        </tr>
				  	[/#list]
				 	</tbody>
				</table>
      		</div
      		</div> --]
      </div>
      </div>
    <br>
  </div>
  [@s.submit type="button" name="save"]Cache Scorecard[/@s.submit]
</article>
  </section>
[/@s.form] 
[#include "/WEB-INF/global/pages/footer.ftl"]