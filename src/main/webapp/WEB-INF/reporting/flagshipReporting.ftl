[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Welcome to WLE Activity Planning" /]
[#assign globalLibs = ["jquery", "dataTable", "noty"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js",  "${baseUrl}/js/wz_tooltip.js" "${baseUrl}/js/reporting/flagshipReporting.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/libs/dataTables/jquery.dataTables-1.9.4.css", "${baseUrl}/css/global/customDataTable.css", "${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "reporting" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm /]
<section id="activityListPlanning" class="content">
[@s.form action="flagshipReport"] 

<input name="flagship" value="${flagshipReport.flagshipId}" type="hidden"/>
<input name="year" value="${flagshipReport.year}" type="hidden"/>
<input name="period" value="${flagshipReport.period}" type="hidden"/>

<article  class="fullContent">
  <div class="content">
  <div class="helpMessage">
  	<img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="reporting.flagship.rpt.help.message.part1" /] </p>
  </div>
  <br/>
  <div class="subhelpMessage1">
    <p> [@s.text name="reporting.flagship.rpt.help.message.part2" /] </p>
  </div>
  <br/>
  <div class="subhelpMessage2">
    <p> [@s.text name="reporting.flagship.rpt.help.message.part3" /] </p>
  </div>
  <div class="clear-both">&nbsp</div>
    <h1>[@s.text name="reporting.flagship.rpt.report.header" /]</h1>
    [#if AuthorizedToView]
      [#-- Only the owner of the activity can see the action buttons --]
      <div class="buttons">
        [#if saveButtonEnabled]
        	[@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [/#if]
        [#if authorizedToConfirm]
        	[@s.submit type="button" name="sendDraft"]
        		[#if finalSubmit]
        			[@s.text name="reporting.form.send.final" /]
        		[#else]
        			[@s.text name="reporting.form.send.darft" /]
        		[/#if]        		
        	[/@s.submit]
        [/#if]
        [#if authorizedToreviewSubmit ] 
            [@s.submit type="button" name="confirm"][@s.text name="reporting.form.review_done" /][/@s.submit]
        [/#if]  
        [#if authorizedToApprove]
        	[@s.submit type="button" name="approve"][@s.text name="form.buttons.approve" /][/@s.submit]
        [/#if]
        [#if authorizedToCancel]
        	[@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
        [/#if]
        [#if printable]
        	<a class="export-buttons" href="../flagship-report?flagshipId=${flagshipReport.flagshipId}&period=${flagshipReport.period}&year=${flagshipReport.year?c}">Print</a>
        [/#if]
      </div>
    
    
    <div class="clear-both">&nbsp</div>
    
    <div id="flagship-reporting">
		<form>
  <table style="table-layout: fixed;" width="100%">
    <tr bgcolor="#e5b8b7">
      <th colspan="12">[@s.text name="reporting.flagship.rpt.section.1.topic" /]</th>
    </tr>
    <tr>
      <td colspan="3" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.1.flagship.caption" /]</td>
      <td colspan="9" class="autofil_bg">${flagshipFocalRegion.longName}</td>
    </tr>
    <tr>
      <td colspan="3" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.1.period.covered" /]</td>
      <td colspan="4" class="autofil_bg">${periodStart}</td>
      <td colspan="5" class="autofil_bg">${periodEnd}</td>
    </tr>
  </table>
  <br/>
  
  
    [#assign count = 1] 
     [#list flagshipReport.flagshipClusters as flagshipCluster]
     <table style="table-layout: fixed;" width="100%">
     
     [#if count == 1]
    <tr bgcolor="#e5b8b7">
      <th colspan="12">[@s.text name="reporting.flagship.rpt.section.2.topic" /]</th>
    </tr>
    [/#if]
    
    [#assign count = count + 1] 
    <tr>
      <td colspan="3" class="flagship-first_col bold-title">[@s.text name="reporting.flagship.rpt.section.2.activity.cluster.caption" /]</td>
      <td colspan="9" class="autofil_bg bold-title">${flagshipCluster.name}</td>
    </tr>
    <tr>
      <td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.budget.caption" /]</td>
      <td colspan="2" class="autofil_bg">${flagshipCluster.budget}</td>
      <td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.exp.caption" /]
      	<span >
		  		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
		      			onmouseover="Tip('This figure is generated based on draft expenditure amounts provided by project leaders.  This amount is not based on financial reports and will not be shared with the CGIAR')"
		      			onmouseout="UnTip()"/>
		</span>
      </td>
      <td class="fsfr-rpt autofil_bg" colspan="2" >
      	  ${flagshipCluster.draftExpenditure}    
      </td>
      <td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.spent.caption" /]
      	<span >
		  		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
		      			onmouseover="Tip('Please leave this blank.  This box should be completed only when IWMI finance has provided a report on mid-year flagship expenditure. This will be completed by the WLE Operations Team')"
		      			onmouseout="UnTip()"/>
		</span>
	  </td>
      <td class="fsfr-rpt autofil_bg" colspan="2" >
      [#if authorizedToSaveByFinance]
      	[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].spentByJune" i18nkey="reporting.flagship.rpt.empty.caption"/]
      [#else]
      	[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].spentByJune" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
      [/#if]
      </td>
    </tr>
    <tr>
      <td class="flagship-first_col" colspan="12"><span class="flagship-title">[@s.text name="reporting.flagship.rpt.section.2.mid.year.status.caption" /]</span>[@s.text name="reporting.flagship.rpt.section.2.mid.year.status.description" /] </td>
    </tr>
    
     [#list  flagshipCluster.clusterOutputMap?keys as co]
    	[#assign actMap = flagshipCluster.clusterOutputMap.get(co) ]
    	
        <tr>
      		<td class="title" colspan="12" bgcolor="#978a63">
      			<font color="white"><span class="flagship-title">
      				[@s.text name="reporting.flagship.rpt.section.2.cluster.outputs" /]<br/>
      				${co} - ${actMap.description}
      			</span></font><br/>
	    	</td>
   	  	</tr>
    	
    	[#list actMap.activityMap?keys as aid]
    		[#assign actClust = actMap.activityMap.get(aid) ]
    		    <tr>
      					<td class="title" colspan="12" bgcolor="#FE9A2E"><span class="flagship-title">[@s.text name="reporting.flagship.rpt.section.2.co.activity.number.caption" /] ${actClust.activityId} <br/>
      					 [@s.text name="reporting.flagship.rpt.section.2.co.activity.name.caption" /] ${actClust.activityName}</span></td>
      			</tr>
      			
    			[#list actClust.clusterOutputs as clstOutput]
    			   <tr>
      					<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.project.number.caption" /]</td>
      					<td colspan="6" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.project.name.caption" /]</td>
      					<td colspan="4" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.output.caption" /]</td>
    			    </tr>
    				<tr>
					     <td colspan="2" class="autofil_bg">${clstOutput.projectId}</td>
					     <td colspan="6" class="autofil_bg">${clstOutput.projectName}</td>
					     <td colspan="4" class="autofil_bg">${clstOutput.status}</td>
    				</tr>
    				
    				<tr>
    					<td colspan="10" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.progress.caption" /]</td>
    					<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.status.caption" /]</td>
    					
    				</tr>
    				<tr>
    					<td colspan="10" class="autofil_bg">${clstOutput.progress}
    					<td colspan="2" class="autofil_bg">
					     	<div>
					          	[#if clstOutput.rating == "0"] <div class="flagship-radio_box flagship-green"></div> (Fully on track to be delivered by year-end or already delivered)[/#if]
					          	[#if clstOutput.rating == "1"] <div class="flagship-radio_box flagship-yellow"></div>(Likely to be delivered by year-end at current trajectory, but may need a bit more attention)[/#if]
					         	[#if clstOutput.rating == "2"] <div class="flagship-radio_box flagship-red"></div>(Unlikely to be delivered by year-end at current trajectory) [/#if]
					        </div>
					      </td>
					 </td>
    				</tr>    				
    			[/#list]
    	[/#list]
    	<tr>
			<td colspan="2" class="flagship-output-progress-col"><font color="white">[@s.text name="reporting.flagship.rpt.section.2.co.comment.caption" /]</font>
				<span >
			  		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
			      			onmouseover="Tip('Please comment on the collective progress of the contributory outputs towards the cluster outputs. Identify any weaknesses or issues to follow-up on')"
			      			onmouseout="UnTip()"/>
				</span>
			</td>
			<td colspan="10" class="flagship-output-progress-col">
			
 			[#list flagshipCluster.outputComments as outputComment]
 				[#if outputComment.code == co]
					[#if authorizedToSave]
						[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].outputComments[${outputComment_index}].comment" i18nkey="reporting.flagship.rpt.empty.caption" /]
					[#else]
						[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].outputComments[${outputComment_index}].comment" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
					[/#if]
				[/#if]
			[/#list]
			</td>
		</tr>
		</tr> <td colspan="12"></td><tr>
    [/#list]
    <tr>
      <td colspan="2" class="flagship-first_col">[@s.text name="reporting.flagship.rpt.section.2.cluster.status.caption" /]
      		<span >
			  		<img style="width: 15px;" src="${baseUrl}/images/global/icon-help.png" 
			      			onmouseover="Tip('Please comment on the overall status of the cluster at mid-year. Identify any weaknesses or issues to follow-up on')"
			      			onmouseout="UnTip()"/>
			</span>      
      </td>
      <td colspan="10" class="flagship-first_col">
      [#if authorizedToSave]
		[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].overallStatus" i18nkey="reporting.flagship.rpt.empty.caption" /]
	  [#else]
	  	[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].overallStatus" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
	  [/#if]      
      </td>
    </tr>
    [#if reviewComment]
    <tr>
      	<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.manager.comment.caption" /]</td>
      	<td colspan="4" class="flagship-first_col">
			[#if authorizedToCommentManager]
				[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].managerComment" i18nkey="reporting.flagship.rpt.empty.caption" /]
			[#else]
				[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].managerComment" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
			[/#if]
		</td>
      	<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.director.comment.caption" /]</td>
      	<td colspan="4" class="flagship-first_col">
			[#if authorizedToCommentDirector]
				[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].directorComment" i18nkey="reporting.flagship.rpt.empty.caption" /]
		 	[#else]
				[@customForm.textArea name="flagshipReport.flagshipClusters[${flagshipCluster_index}].directorComment" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
			[/#if]
      	</td>
    </tr>
    [/#if]
    <td colspan="12" class="flagship-first_col"><span class="flagship-title">[@s.text name="reporting.flagship.rpt.section.2.cluster.rating.caption" /]</span>
        <div>
          <div class="flagship-single_row">
           	<div class="flagship-radio_box flagship-green"><input type="radio" name="flagshipReport.flagshipClusters[${flagshipCluster_index}].rating" value="0" [#if flagshipCluster.rating == "0"] checked [/#if]  [#if !authorizedToSave] disabled [/#if]></div>
            <div> On track against all deliverables</div>
          </div>
          <div class="flagship-single_row">
          	<div class="flagship-radio_box flagship-yellow"><input type="radio" name="flagshipReport.flagshipClusters[${flagshipCluster_index}].rating" value="1" [#if flagshipCluster.rating == "1"] checked [/#if] [#if !authorizedToSave] disabled [/#if]></div>
            <div> On track with most but not all deliverables</div>
          </div>
          <div class="flagship-single_row">
          	<div class="flagship-radio_box flagship-red"><input type="radio" name="flagshipReport.flagshipClusters[${flagshipCluster_index}].rating" value="2"  [#if flagshipCluster.rating == "2"] checked [/#if] [#if !authorizedToSave] disabled [/#if]></div>
            <div> Off track with most deliverables</div>
          </div>
        </div>
      </td>
    </table>
    <br/>
   [/#list]
    
  
  <br/>
  <table style="table-layout: fixed;" width="100%">
    <tr bgcolor="#e5b8b7">
      <th  colspan="12">[@s.text name="reporting.flagship.rpt.section.3.topic" /]</th>
    </tr>
    <tr>
      <td class="flagship-first_col" colspan="12"><span class="flagship-flagship-title">[@s.text name="reporting.flagship.rpt.section.3.topic.overall.status.caption" /] </span> [@s.text name="reporting.flagship.rpt.section.3.topic.overall.status.description" /]</td>
    </tr>
    <tr>
      <td colspan="12" class="flagship-first_col">
      [#if authorizedToSave]
      	[@customForm.textArea name="flagshipReport.overallDescription" i18nkey="reporting.flagship.rpt.empty.caption" /]
      [#else]
      	[@customForm.textArea name="flagshipReport.overallDescription" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
      [/#if]
      </td>
    </tr>
    
    [#if reviewComment]
     <tr>
      	<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.manager.comment.caption" /]</td>
      	<td colspan="4" class="flagship-first_col">
			[#if authorizedToCommentManager]
				[@customForm.textArea name="flagshipReport.managerComment" i18nkey="reporting.flagship.rpt.empty.caption" /]
			[#else]
				[@customForm.textArea name="flagshipReport.managerComment" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
			[/#if]
		</td>
      	<td colspan="2" class="flagship-first_col title">[@s.text name="reporting.flagship.rpt.section.2.co.director.comment.caption" /]</td>
      	<td colspan="4" class="flagship-first_col">
			[#if authorizedToCommentDirector]
				[@customForm.textArea name="flagshipReport.directorComment" i18nkey="reporting.flagship.rpt.empty.caption" /]
		 	[#else]
				[@customForm.textArea name="flagshipReport.directorComment" i18nkey="reporting.flagship.rpt.empty.caption" disabled=true/]
			[/#if]
      	</td>
    </tr>
    [/#if]
  </table>
</form>    
</div>
  </div>
  [#-- Only the owner of the activity can see the action buttons --]
      <div class="buttons">
        [#if saveButtonEnabled]
        	[@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [/#if]
        [#if authorizedToConfirm]
        	[@s.submit type="button" name="sendDraft"]
        		[#if finalSubmit]
        			[@s.text name="reporting.form.send.final" /]
        		[#else]
        			[@s.text name="reporting.form.send.darft" /]
        		[/#if]        		
        	[/@s.submit]
        [/#if]
        [#if authorizedToreviewSubmit ] 
            [@s.submit type="button" name="confirm"][@s.text name="reporting.form.review_done" /][/@s.submit]
        [/#if]  
        [#if authorizedToApprove]
        	[@s.submit type="button" name="approve"][@s.text name="form.buttons.approve" /][/@s.submit]
        [/#if]
        [#if authorizedToCancel]
        	[@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
        [/#if]
        [#if printable]
        	<a class="export-buttons" href="../flagship-report?flagshipId=${flagshipReport.flagshipId}&period=${flagshipReport.period}&year=${flagshipReport.year?c}">Print</a>
        [/#if]
      </div>
 [#else]
 	[@s.text name="reporting.flagship.rpt.section.no.permission.message" /] 
 [/#if]
</article>

 [/@s.form] 


  </section>

[#include "/WEB-INF/global/pages/footer.ftl"]