[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Welcome to WLE Activity Planning" /]
[#assign globalLibs = ["jquery", "dataTable", "noty", "googleAPI"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js", "${baseUrl}/js/reporting/scoreCardPageThree.js"] /]
[#assign customCSS = [] /]
[#assign currentSection = "reporting" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm /]
<section id="activityListPlanning" class="content">
[@s.form action="flagshipReport"] 
<article  class="fullContent">
  <div class="content">
  	 <div class="bold-title subhelpMessage1"> 
  	 <a href="[@s.url action='scoreCardTwo'] [@s.param name='flagshipId']${flagshipId?c}[/@s.param][/@s.url]" title="${flagshipShortName}">${flagshipShortName}</a> >
  	 Project <a href="[@s.url action='scoreCardThree'][@s.param name='activityId']${activityId}[/@s.param] [/@s.url]" title="${activityId}">${activityId}</a>
  	 </div> 
  	 <br/>
  	<h1>Project : ${activityId} (${projectTitle})</h1>
  	<div class="bold-title">Project Outputs<div>
  	<br/>
  	[#assign count = 1] 
  	[#list outputSummaries as outputSummary]
  		<input type="hidden" name="green${count}" value="${outputSummary.status.green}">
	    <input type="hidden" name="yellow${count}" value="${outputSummary.status.yellow}">
	    <input type="hidden" name="red${count}" value="${outputSummary.status.red}">	
  		<fieldset>
    		<legend class="bold-title">${outputSummary.outputName}</legend>
    		<div>
  				<div style="float:left;width:30%;" id="rating${count}"></div> 
  				<div style="float:left;width:70%;margin-top:15px">
		      	    <label class="bold-title">Progress towards delivery of contributory output</label>
		      		<textarea readonly="readonly" rows="10" style="height:auto">${outputSummary.comment}</textarea>
		   	 	</div>
    		</div>
    		
  		</fieldset>
  		[#assign count = count + 1]   		
  	[/#list]
  	<input type="hidden" name="count" value="${count}">
  </div>
</article>
 [/@s.form] 
  </section>

[#include "/WEB-INF/global/pages/footer.ftl"]