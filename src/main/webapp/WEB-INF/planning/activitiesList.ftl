[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#macro activitiesList activities owned=true canValidate=false canEditActivity=false tableID=""]

  <table class="activityList" id="${tableID}">
  <thead>
    <tr>
      <th id="id" style="width:35px;">[@s.text name="planning.activityList.id" /]</th>
      <th id="activity">[@s.text name="planning.activityList.activity" /]</th>
      [#if owned]
        <th id="contactPerson">[@s.text name="planning.activityList.contactPerson" /]</th>
      [#else]
        <th id="leaderName">[@s.text name="planning.activityList.leader" /]</th>
      [/#if]
      <th id="leadCenter">[@s.text name="planning.activityList.lead.center" /]</th>
      [#-- <th id="theme">[@s.text name="planning.activityList.milestone" /]</th> --]
      [#if canValidate]
        <th id="validated">[@s.text name="planning.activityList.validated" /]</th>
      [/#if]
      <th id="print" style="width:50px;">[@s.text name="planning.activityList.action" /]</th>
    </tr>
  </thead>
  <tbody>
    [#if activities?has_content]
      [#list activities as activity]
        [#if activity??]
        <tr>
          <td>
               <a href=" [@s.url action='mainInformation' includeParams='get'] [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param] [/@s.url]" >
                ${activity.formattedId}
               </a>  
          </td>
          <td class="left">
            [#if activity.title?has_content]
                    <a href=" [@s.url action='mainInformation' includeParams='get'] [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param] [/@s.url]" >
		              [#if activity.title?length < 70] 
		                    ${activity.title} 
		              [#else] [@utilities.wordCutter string=activity.title maxPos=70 /]... 
		              
		              [/#if]
		              : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
		           </a> 
           [#else]
              [@s.text name="planning.activityList.title.undefined" /]
           [/#if]
          </td>
          <td>
            [#if owned]
              [#if activity.contactPersons?has_content]
                [#if activity.contactPersons[0].email?has_content]
                  <a href="mailto:${activity.contactPersons[0].email}">${activity.contactPersons[0].name}</a>
                [#else]
                  ${activity.contactPersons[0].name}
                [/#if]
              [#else]
                [@s.text name="planning.activityList.contactPerson.empty" /]
              [/#if]
            [#else]
              ${activity.leader.acronym}
            [/#if]
          </td>
          <td>
          	[#if activity.leadCenter?has_content]
          		${activity.leadCenter}
          	[#else]
              [@s.text name="planning.activityList.lead.center.undefined" /]
            [/#if]
          </td>               
         [#--
          <td>
            [#if activity.milestone.code?has_content]
              ${activity.milestone.code}
            [#else]
              [@s.text name="planning.activityList.milestone.undefined" /]
            [/#if]
          </td>
          --]
          [#if canValidate]
            <td>
              [#if activity.pendingReview]
              	   [@s.text name="planning.activityList.pending.review" /]
              [#elseif activity.reviewsCompleted ]
              	   [@s.text name="planning.activityList.review.completed" /]
              [#else]
              
              [#if activity.validated] 
              
                [#if activity.toBeSubmittedToCA]
                    [#if activity.editable]
                        <a class='inline validate-button' href="#submit_to_ca_${activity_index}">Submit To CA</a>
                    [#else]
                       <a class='validate-button' href="#">Submit To CA</a>
                    [/#if]    
                 	  <div style='display:none'>
						<div id='submit_to_ca_${activity_index}' style='padding:10px; background:#fff;'>
						<div><p>Plan will be submitted to Center Admin to notify external reviewers.</p></div>
							[#-- We send the index of the activity in the array, not the activity identifier  --]
                    		[#-- in order find quickly the activity in the array to modify it.  --]
                    		[@s.form action="activities" cssClass="buttons"]
                      			<input name="activityIndex" value="${activity_index}" type="hidden"/>
                      			<input name="submitToCA" value="1" type="hidden"/>
                      			[@s.submit type="button" name="save"]Submit[/@s.submit]
                   			 [/@s.form]  
                   	  </div>
					</div>
                [#else]
                  <div alt="Activity validated" title="Activity validated" ><div class="icon-20" id="i-checkedActivity"></div>Validated </div>
                [/#if]  
              [#else]
                [#-- The PI only can see a notification, they can't validate the activity --]
                [#if currentUser.PI] 
                  <div alt="This activity has not been validated yet" title="This activity has not been validated yet"  ><div class="icon-20" id="i-errorCheckedActivity"></div>Validate</div>
                [#else]
                  [#-- The CP/TL/RPL can validate the activity if needed --]
                  [#if activityID == activity.id]
                    [#-- User tried to submit this activity but there is some missing data. --] 
                    <div alt="There is missing data" title="There is missing data"  ><div class="icon-20" id="i-errorCheckedActivity"></div>Validate</div>
                  [#else]
                     [#if activity.editable]
                        <a class='inline validate-button' href="#validate-activity_${activity_index}">Validate</a>
                     [#else]
                       <a class='validate-button' href="#">Validate</a>
                     [/#if] 
                     
                 	  <div style='display:none'>
						<div id='validate-activity_${activity_index}' style='padding:10px; background:#fff;'>
						<div><p>Once the plan is validated, edits will not be possible until reviewers have provided their comments.</p></div>
							[#-- We send the index of the activity in the array, not the activity identifier  --]
                    		[#-- in order find quickly the activity in the array to modify it.  --]
                    		[@s.form action="activities" cssClass="buttons"]
                      			<input name="activityIndex" value="${activity_index}" type="hidden"/>
                      			[@s.submit type="button" name="save"]Validate[/@s.submit]
                   			 [/@s.form]  
                   	    </div>
						</div>
                  [/#if]
                [/#if]
              [/#if]
              [/#if]
            </td>
          [/#if]
          <td>
             
             [#if canEditActivity]
                [#if activity.editable]
	              <a href=" [@s.url action='mainInformation' includeParams='get'] [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param] [/@s.url]" >
	                 <img src="${baseUrl}/images/global/icon_edit.png"
	                    onmouseover="Tip('Edit Activity Details')"
      			        onmouseout="UnTip()"  />
	              </a> 
	              
	              [#if activity.canDelete]
	               <a id="deleteActivityLink" href="[@s.url action='activityDelete' includeParams='get'] [@s.param name='${activityRequestParameter}']${activity.id}[/@s.param] [/@s.url]" >
	                  <img id="deleteActivityIcon" src="${baseUrl}/images/global/icon_delete.png"
	                            onmouseover="Tip('Delete Activity')" onmouseout="UnTip()"  />
	              </a> 
	              [/#if]
	            
	              <input type="hidden" id="beforeDeleteMessage" value="[@s.text name="delete.beforeDelete.message" /]" />
	            [#else]
	               <a target="_blank" href=" [@s.url action='activity' namespace="/home" includeParams='get'] [@s.param name='${publicActivityRequestParameter}']${activity.id?c}[/@s.param] [/@s.url]" >
	               
	              </a> 
                [/#if]
            [#else]
	              <a target="_blank" href=" [@s.url action='activity' namespace="/home" includeParams='get'] [@s.param name='${publicActivityRequestParameter}']${activity.id?c}[/@s.param] [/@s.url]" >
	                
	              </a>
            [/#if]
          	 <a href="../report?activityId=${activity.id}" target="_blank">
          	   <img src="${baseUrl}/images/global/icon_print.png"
          	        onmouseover="Tip('Print Activity Details')"
      			    onmouseout="UnTip()" />
          	 </a>
          	 <a href="../csv?activityId=${activity.id}" target="_blank">
          	   <img src="${baseUrl}/images/global/icon_budget.png"
          	        onmouseover="Tip('Print Activity Budget Details')"
      			    onmouseout="UnTip()" />
          	 </a>
          </td>
        </tr>
        [/#if]
      [/#list]
    [#else]
      <tr>
        <td colspan="4">[@s.text name="planning.activityList.empty" /]</td>
      </tr>
    [/#if]
  </tbody>
</table>
        
[/#macro]