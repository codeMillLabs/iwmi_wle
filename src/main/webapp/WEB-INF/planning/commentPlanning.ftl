[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Comments" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/wz_tooltip.js"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "comment" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.comments.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  
  [@s.form action="comment"]  
  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
     [#if currentUser.TL || currentUser.PL || currentUser.SP || currentUser.PI || currentUser.admin ]
	    <div class="fullBlock">
	      [@customForm.radioButtonGroup i18nkey="planning.review.complete" label="" name="reviewCompleted" listName="yesNoOptions" value="${reviewCompleted?string('true', 'false')}" /]
	    </div> 
     [/#if]
    
     [#if currentUser.PI || currentUser.admin ]
	    <div class="fullBlock">
	      [@customForm.radioButtonGroup i18nkey="planning.statisfied.review" label="" name="satisfied" listName="yesNoOptions" value="${satisfied?string('true', 'false')}" /]
	    </div> 
     [/#if]
     
     [#if currentUser.CA || currentUser.admin ]
	    <div class="fullBlock">
	      [@customForm.radioButtonGroup i18nkey="planning.request.external.reviews" label="" name="internalReviewCompleted" listName="yesNoOptions" value="${internalReviewCompleted?string('true', 'false')}" /]
	    </div> 
     [/#if]
    
     [#if currentUser.admin ]
	    <div class="fullBlock">
	      [@customForm.radioButtonGroup i18nkey="planning.activity.complete" label="" name="activityCompleted" listName="yesNoOptions" value="${activityCompleted?string('true', 'false')}" /]
	    </div> 
     [/#if]
    
    [#if activity.reviewers?has_content]
      [#list activity.reviewers as review]
        <div id="objective-${review_index}" class="comment">
          [#-- Objective identifier --]
          <input type="hidden" name="activity.objectives[${review_index}].id" value="${review.id?c}" />
          [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.reviewer.comment" /] ${review_index +1}
          </div>
          
          <div class="halfPartBlock">
    			 [@customForm.input name="activity.reviewers[${review_index}].reviewerName"  i18nkey="planning.reviewer.name" disabled=true/]
    	 </div>
    	 
    	 <div class="halfPartBlock">
    			 [@customForm.input name="activity.reviewers[${review_index}].reviewedDate"  i18nkey="planning.reviewer.date" disabled=true/]
    	 </div>
    	 
    	 <div class="halfPartBlock">
    			 [@customForm.input name="activity.reviewers[${review_index}].activityRevision"  i18nkey="planning.reviewer.activity.revision" disabled=true/]
    	 </div>
          
           [#-- Comment --]
      	   <div class="fullBlock">
      	   [#if review.editable]
 		    [@customForm.textArea name="activity.reviewers[${review_index}].comment"  i18nkey="planning.reviewer.comment" /]
      	   [#else]
      	   [@customForm.textArea name="activity.reviewers[${review_index}].comment"  i18nkey="planning.reviewer.comment" disabled=true/]
      	   [/#if]
      	  </div>
      	  
 		   <div class="clear-both"></div> 
          <hr />
        </div>
      [/#list]
    [/#if]

	</fieldset>
    
    
    [#-- Comment template --]
    <div id="commentTemplate" style="display:none">
      [#-- Item index --]
      <div class="itemIndex">
          [@s.text name="planning.reviewer.comment" /]
      </div>
      <div class="clear-both"></div>
      <hr />
    </div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && canEdit]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]