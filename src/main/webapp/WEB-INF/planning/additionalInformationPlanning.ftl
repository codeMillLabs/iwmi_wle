[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Additional Information Planning" /]
[#assign globalLibs = ["jquery", "noty", "chosen"] /]
[#assign customJS = ["${baseUrl}/js/planning/additionalInformationPlanning.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "additionalInformation" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]

<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.additionalInformation.help" /] </p>
  </div>
  
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  [@s.form action="additionalInformation"]
  <article class="halfContent">
    
    [#-- Hidden values --]
    <input id="keywordsDefaultText" value="[@s.text name="planning.additionalInformation.keyword.default" /]" type="hidden">
  
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    <fieldset>
      <legend><h6>[@s.text name="planning.additionalInformation" /]</h6></legend>
    
    [#-- Activity keywords --]
    <div class="keywordsBlock fullBlock">
      <div id="keywordsList">
        [@customForm.select name="activity.keywords" label="" i18nkey="planning.additionalInformation.keywords" 
        listName="keywords" keyFieldName="id"  displayFieldName="name" value="activity.keywordsIds" 
        multiple=true className="keywords" /]
      </div>
    </div>
    
    [#-- Other keywords --]
    <div class="otherKeywordsBlock fullBlock">
      [#-- Title --]
      
      [#-- Other keyword Help --]
      
      
      [#-- Other keywords field --]
     
      <hr />
    </div>
    
    [#-- Resources --]
    <div class="resourcesBlock">
      <div id="resourcesList">
        [#if activity.resources?has_content]
          [#list activity.resources as resource]
            <div id="resource-${resource_index}" class="fullBlock resource">
              <input type="hidden" name="activity.resources[${resource_index}].id" value="${resource.id?c}" />
              [@customForm.input name="activity.resources[${resource_index}].name" type="text" i18nkey="planning.additionalInformation.resources" /]
              <img src="${baseUrl}/images/global/icon-remove.png" class="removeResource" id="removeResource-${resource_index}" />
            </div>
          [/#list]
        [/#if]
      </div>
      <div id="addResourceBlock" class="halfPartBlock">
        <img src="${baseUrl}/images/global/icon-add.png" />
        <a href="" class="addResource" >[@s.text name="planning.additionalInformation.addResource" /]</a>
      </div>
      <hr />
    </div>
    
    [#-- Keyword template --]
    <div id="keywordTemplate" class="thirdPartBlock" style="display:none" >
      <input type="hidden" name="keywordId" class="activityKeywordId" value="-1" />
      [@customForm.select name="id" label="" i18nkey="planning.additionalInformation.keywords" listName="keywords" keyFieldName="id"  displayFieldName="name" className="keywords" /]
    </div>
    
    [#-- Other keyword template --]
    [#--
    <div id="otherKeywordTemplate" class="thirdPartBlock" style="display:none">
      <input type="hidden" class="activityKeywordId" value="-1" />
      [@customForm.input name="other" type="text" i18nkey="planning.additionalInformation.keyword" /]
    </div>
    --]
    
    [#-- Resource template --]
    <div id="resourceTemplate" class="fullBlock" style="display:none">
      <input type="hidden" name="resourceId" value="-1" />
      [@customForm.input name="name" type="text" i18nkey="planning.additionalInformation.resource" /]
      <img src="${baseUrl}/images/global/icon-remove.png" class="removeResource" />
    </div>
    <div class="clear-both">&nbsp;</div>
	   <div class="addLink">
		  <img src="${baseUrl}/images/global/icon-add.png" />
		  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=add_info_review&cstatus=${activity.activityStatus}">
		     [@s.text name="planning.comments.addComment" /]
		  </a>
	 </div>
    
    <!-- internal parameter -->
    <input name="activityID" type="hidden" value="${activity.id?c}" />
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
    </fieldset>
    </article>
  [/@s.form]
 
  </section>
  
[#include "/WEB-INF/global/pages/footer.ftl"]