
[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Partners Planning" /]
[#assign globalLibs = ["jquery", "noty", "chosen"] /]
[#assign customJS = ["${baseUrl}/js/planning/partnersPlanning.js", "${baseUrl}/js/global/utils.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "partners" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]

[#macro partnerSection]
  [#if activity.activityPartners?has_content]
    [#list activity.activityPartners as ap]
      <div id="activityPartner-${ap_index}" class="activityPartner">
        [#-- Partner identifier --]
        <input type="hidden" name="activity.activityPartners[${ap_index}].id" value="${ap.id?c}" />
        
        [#-- Remove link for all partners --]
        <div class="removeLink">
          <img src="${baseUrl}/images/global/icon-remove.png" />
          <a id="removeActivityPartner-${ap_index}" href="" class="removeActivityPartner">[@s.text name="planning.activityPartners.removePartner" /]</a>
        </div>

        [#-- Partner type list --]
        <div class="halfPartBlock partnerTypeName chosen">
          [@customForm.select name="partnerTypeList" label="" i18nkey="planning.activityPartners.partnerType" listName="partnerTypes" keyFieldName="id"  displayFieldName="name" className="partnerTypes" value="activity.activityPartners[${ap_index}].partner.type.id" /]
        </div>

        [#-- Country list --]
        <div class="halfPartBlock countryListBlock chosen">
          [@customForm.select name="countryList" label="" i18nkey="planning.activityPartners.country" listName="countries" keyFieldName="id"  displayFieldName="name" className="countryList" value="activity.activityPartners[${ap_index}].partner.country.id" /]
        </div>

        [#-- Partner Name --]
        <div class="fullBlock  partnerName chosen">
          [@customForm.select name="activity.activityPartners[${ap_index}].partner" label="" i18nkey="planning.activityPartners.partner.name" listName="partners" keyFieldName="id"  displayFieldName="name" /]
        </div>

        [#-- Contact Name --]
        <div class="helpHalfPartBlock">
          [@customForm.input name="activity.activityPartners[${ap_index}].contactName" type="text" i18nkey="planning.activityPartners.contactPersonName" required=true /]
          <div class="helpImg">
      				<img class="margin-img-partner" src="${baseUrl}/images/global/icon-help.png" 
      						onmouseover="Tip('Principle contact person')"
      						onmouseout="UnTip()"/>
      	  </div>
        </div>

        [#-- Contact Email --]
        <div class="halfPartBlock">
          [@customForm.input name="activity.activityPartners[${ap_index}].contactEmail" type="text" i18nkey="planning.activityPartners.contactPersonEmail" required=true /]
        </div> 
        
        [#-- Clasification --]
        <div class="helpHalfPartBlock">
           [@customForm.select name="clasification" 
	           label="" 
	           i18nkey="planning.activityPartners.partner.clasification" 
	           listName="clasificationList" 
	           keyFieldName="value"  
	           displayFieldName="displayName"
	           value="activity.activityPartners[${ap_index}].clasification" /]
        </div> 
        
        [#-- Budget  --]
        <div class="helpHalfPartBlock">
          [@customForm.input name="activity.activityPartners[${ap_index}].budget" 
               type="text" i18nkey="planning.activityPartners.partner.budget" /]
               <div class="helpImg">
      				<img class="margin-img-partner" src="${baseUrl}/images/global/icon-help.png" 
      						onmouseover="Tip('Allocated budget for the selected partner')"
      						onmouseout="UnTip()"/>
      	  </div>
        </div> 
        <div class="clear-both"></div> 
        <hr />
      </div> <!-- End activityPartner-${ap_index} -->
    [/#list]
  [/#if]  
[/#macro]

<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p>[@s.text name="planning.activityPartners.help" /]</p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  [@s.form action="partners"]
  [#assign typeSelectHeadValue ] [@s.text name="planning.activityPartners.selectPartnerType" /] [/#assign]
  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
  
    <div class="fullBlock">
      [@customForm.radioButtonGroup i18nkey="planning.activityPartners.havePartners" label="" name="activity.hasPartners" listName="partnersOptions" value="${activity.hasPartners?string('true', 'false')}" /]
    </div>  
    
    <div id="items" [#if !activity.hasPartners]style="display:none;"[/#if]>
      <fieldset id="activityPartnerGroup" class="group" >
        <legend> <h6> [@s.text name="planning.activityPartners.partners" /] </h6> </legend>
        [@partnerSection /]
        <div class="addLink">
          <img src="${baseUrl}/images/global/icon-add.png" />
          <a href="" class="addActivityPartner">[@s.text name="planning.activityPartners.addNewPartner" /]</a>
        </div>   
      </fieldset>
    
      <p id="addPartnerText">
        [@s.text name="planning.activityPartners.addPartnerMessage.first" /]
        <a class="popup" href="[@s.url action='partnerSave'][@s.param name='${activityRequestParameter}']${activityID?c}[/@s.param][/@s.url]">
          [@s.text name="planning.activityPartners.addPartnerMessage.second" /]
        </a>       
      </p>
    </div>
    
    
    <!-- PARTNERS TEMPLATE -->
    <div id="template">
      <div id="activityPartner-9999" class="activityPartner" style="display: none;">      
        [#-- remove link --]
        <div class="removeLink">
          <img src="${baseUrl}/images/global/icon-remove.png" />
          <a id="removeActivityPartner-9999" href="" class="removeActivityPartner">[@s.text name="planning.activityPartners.removePartner" /]</a>
        </div>
        
        [#-- Partner type list --]
        <div class="halfPartBlock partnerTypeName chosen">
          [@customForm.select name="partnerTypeList" label="" i18nkey="planning.activityPartners.partnerType" listName="partnerTypes" keyFieldName="id"  displayFieldName="name" className="partnerTypes" /]
        </div>
        
        [#-- Country list --]
        <div class="halfPartBlock countryListBlock chosen">
          [@customForm.select name="countryList" label="" i18nkey="planning.activityPartners.country" listName="countries" keyFieldName="id"  displayFieldName="name" className="countryList" /]
        </div>
        
        [#-- Partner identifier --]
        <input type="hidden" name="id" value="-1">
      
        [#-- Partner Name --]
        <div class="fullBlock partnerName chosen">
          [@customForm.select name="__partner" label="" i18nkey="planning.activityPartners.partner.name" listName="partners" keyFieldName="id"  displayFieldName="name" /]
        </div>
        
        [#-- Contact Name --]
        <div class="helpHalfPartBlock">
          [@customForm.input name="contactName" type="text" i18nkey="planning.activityPartners.contactPersonName" required=true /]
          <div class="helpImg">
      				<img class="margin-img-partner" src="${baseUrl}/images/global/icon-help.png" 
      						onmouseover="Tip('Principle contact person')"
      						onmouseout="UnTip()"/>
      	  </div>       
        </div>
      
        [#-- Contact Email --]
        <div class="halfPartBlock">
          [@customForm.input name="contactEmail" type="text" 
             i18nkey="planning.activityPartners.contactPersonEmail" required=true /]
        </div>

        [#-- Clasification --]
        <div class="helpHalfPartBlock">
          [@customForm.select name="clasification" 
          label="" i18nkey="planning.activityPartners.partner.clasification" 
          listName="clasificationList" keyFieldName="value"  displayFieldName="displayName" className="clasificationList" /]
        </div>
        
        
        [#-- Budget  --]
        <div class="helpHalfPartBlock">
          [@customForm.input name="budget" type="text" i18nkey="planning.activityPartners.partner.budget" /]
        	<div class="helpImg">
      				<img class="margin-img-partner" src="${baseUrl}/images/global/icon-help.png" 
      						onmouseover="Tip('Allocated budget for the selected partner')"
      						onmouseout="UnTip()"/>
      	  </div>
        </div> 
       <div class="clear-both"></div>    
        <hr />
      </div> <!-- End partner template -->
    </div> <!-- End template -->
    
    <div class="clear-both">&nbsp;</div>
	<div class="addComment">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=partners_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
    
    [#-- Partner list no result found message --]
    <input id="noResultText" type="hidden" value="[@s.text name="planning.activityPartners.addNewPartner.noResultMatch" /]" />
    <input id="noResultByFilterText" type="hidden" value="[@s.text name="planning.activityPartners.addNewPartner.noResultFilterMatch" /]" />
    <input id="partnerTypeDefault" type="hidden" value="[@s.text name="planning.activityPartners.selectPartnerType" /]" />
    <input id="countryListDefault" type="hidden" value="[@s.text name="planning.activityPartners.selectCountry" /]" />
    
    <!-- internal parameter -->
    <input name="activityID" type="hidden" value="${activity.id?c}" />
    
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
        
    </article>
  [/@s.form]
 
  </section>
  
[#include "/WEB-INF/global/pages/footer.ftl"]