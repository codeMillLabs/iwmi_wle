[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Deliverables Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/deliverablesPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "deliverables" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.activityDeliverables.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="deliverables"]  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="deliverablesBlock">
      <legend> <h6> [@s.text name="planning.activityDeliverables" /] </h6> </legend>
      [#if activity.deliverables?has_content]
        [#list activity.deliverables as deliverable]
          <div id="deliverable-${deliverable_index}" class="deliverable">
            [#-- identifier --]
            <input name="activity.deliverables[${deliverable_index}].id" type="hidden" value="${deliverable.id?c}" />
            
            [#-- Adding remove link only for new deliverables --]
            <div class="removeLink">
              <img src="${baseUrl}/images/global/icon-remove.png" />
              <a id="removeDeliverable-${deliverable_index}" href="" class="removeDeliverable">
                [@s.text name="planning.activityDeliverables.removeDeliverable" /]
              </a>
            </div>
            
            [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.activityDeliverables.deliverable" /] ${deliverable_index +1}
            </div>
            
          	<div class="halfBlock">
    			 [@customForm.input name="activity.deliverables[${deliverable_index}].deliverableCode"  i18nkey="planning.deliverable.code" disabled=true/]
             </div>
            
             [#-- Verifiable Indicator --]
            <div class="fullBlock">
              [@customForm.radioButtonGroup 
                 name="activity.deliverables[${deliverable_index}].indicator" 
                 i18nkey="planning.activityDeliverables.indicators" 
                 label="" 
                 listName="indicatorOptions" value="activity.deliverables[${deliverable_index}].indicator" required=true/]
            </div>
            
            [#-- Description --]
            <div class="helpBlock">
              [@customForm.textArea name="activity.deliverables[${deliverable_index}].description" i18nkey="planning.activityDeliverables.description" required=true /]
              <div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('Describe specific significant  outputs for the year (these may be final outputs if the<br/> activity ends this year) Outputs must be tangible and verifiable. Examples: manuscripts<br/>  published / submitted, workshops held/ documented,  training/capacity building conducted,<br/> computer models developed, stakeholder consultations documented, policy dialogues held<br/>  (with xx participants), policy briefs developed/ circulated, etc.).')"
      					onmouseout="UnTip()"/>
      		  </div>
           </div>
            
             [#-- Type --]
            <div class="helpHalfPartBlock">
              [@customForm.select name="activity.deliverables[${deliverable_index}].type" label="" i18nkey="planning.activityDeliverables.type" listName="deliverableTypesList" keyFieldName="id"  displayFieldName="name" className="deliverableType" /]
            </div>
            
            
          	<div class="clear-both"></div> 
         
            
          </div> <!-- End deliverable-${deliverable_index} -->
          <hr />
        [/#list]
      [/#if]
      
      <div id="addDeliverableBlock" class="addLink">
        <img src="${baseUrl}/images/global/icon-add.png" />
        <a href="" class="addDeliverable" >[@s.text name="planning.activityDeliverables.addDeliverable" /]</a>
      </div>
    
    </fieldset>
    
    [#-- Delvierable template --]
    <div id="delvierableTemplate" style="display:none">
      <div id="deliverable-9999" class="deliverable" style="display: none;">      
        [#-- remove link --]      
        <div class="removeLink">            
            <img src="${baseUrl}/images/global/icon-remove.png" />
            <a id="removeDeliverable-9999" href="" class="removeDeliverable">
              [@s.text name="planning.activityDeliverables.removeDeliverable" /]
            </a>
        </div>
        
        [#-- identifier --]
        <input name="id" type="hidden" value="-1" />
        
        [#-- Item index --]
        <div class="itemIndex">
          [@s.text name="planning.activityDeliverables.deliverable" /]
        </div>
        
        [#-- Verifiable Indicator --]
        <div class="fullBlock">
          [@customForm.radioButtonGroup name="indicator" i18nkey="planning.activityDeliverables.indicators" label="" listName="indicatorOptions" required=true/]
        </div>
        
        [#-- Description --]
        <div class="helpBlock">                      
          [@customForm.textArea name="description" i18nkey="planning.activityDeliverables.description" required=true /]
          <div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('Describe specific significant  outputs for the year (these may be final outputs if the<br/> activity ends this year) Outputs must be tangible and verifiable. Examples: manuscripts<br/>  published / submitted, workshops held/ documented,  training/capacity building conducted,<br/> computer models developed, stakeholder consultations documented, policy dialogues held<br/>  (with xx participants), policy briefs developed/ circulated, etc.).')"
      					onmouseout="UnTip()"/>
      		  </div>
        </div>
        
        [#-- Type --]
        <div class="helpHalfPartBlock">        
          [@customForm.select name="type" i18nkey="planning.activityDeliverables.type" listName="deliverableTypesList" keyFieldName="id"  displayFieldName="name" className="deliverableType" /]
        </div>
        
        <div class="clear-both"></div> 
       
        
        <hr />
      </div> <!-- End deliverable template -->
    </div>
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=deliverables_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]