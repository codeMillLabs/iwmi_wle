[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Budget Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/budgetPlanning.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "budget" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]


<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.budget.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  [@s.form action="budget"]
  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
  
   <fieldset>
      <legend><h6>[@s.text name="planning.budget" /]</h6></legend>
      
      <div id="budget-def-table">
      <table>
      	<tr>
      		<td bgcolor="#c6e0b4" width="50px"></td>
      		<td>User Entered Data</td>
      	</tr>
      	<tr>
      		<td bgcolor="#fff2cc"></td>
      		<td>Calculated Figures, Ex: 50 * 10% = 5</td>
      	</tr>
      	<tr>
      		<td bgcolor="#d9e1f2"></td>
      		<td>Totals, Sums</td>
      	</tr>
      	<tr>
      		<td bgcolor="#aeaaaa"></td>
      		<td>Auto populated on Geo Locations selected on Geo Location Tab</td>
      	</tr>
      </table>
      </div>
      <br/><br/>  
       <h3 class="budget-topics">
       		Main Budget
       </h3>
       <br/>
       <div class="budget-table">
       		<table class="region-related-table">
       			<tr>
       				<th></th>
       				<th>W1/W2</th>
       				<th>W3</th>
       				<th>Bilateral</th>
       				<th>Other</th>
       				<th>Total</th>
       			<tr>
       			[#if activity.budget.mainBudgets?has_content]
        			[#list activity.budget.mainBudgets as region]
        				<tr>
        					<td colspan=6 class="region-row">${region.regionName}</td>
        				</tr>
        				<tr class="main-budget-budget-row">
        				     <!-- hidden -->
        				     <input name="activity.budget.mainBudgets[${region_index}].regionId" type="hidden" value="${region.regionName}" />
        				     <input name="activity.budget.mainBudgets[${region_index}].regionName" type="hidden" value="${region.regionName}" />
        				     <input name="activity.budget.mainBudgets[${region_index}].details[0].id" type="hidden" value="0" />
        				     <input name="activity.budget.mainBudgets[${region_index}].details[0].type" type="hidden" value="Budget" />
        					<td width="170px">Budget</td>
        					<td class="user-data">
        					[@customForm.input className="mainBudgets_${region_index}_0_w1w2" name="activity.budget.mainBudgets[${region_index}].details[0].w1w2" type="text"/]
        					</td>
        					<td class="user-data">
        					[@customForm.input className="mainBudgets_${region_index}_0_w3" name="activity.budget.mainBudgets[${region_index}].details[0].w3" type="text"/]</td>
							<td class="user-data">
							[@customForm.input className="mainBudgets_${region_index}_0_bilateral" name="activity.budget.mainBudgets[${region_index}].details[0].bilateral" type="text"/]</td>        					
        					<td class="user-data">
        					[@customForm.input className="mainBudgets_${region_index}_0_other" name="activity.budget.mainBudgets[${region_index}].details[0].other" type="text"/]</td>
        					<td class="totals">
        						<h6></h6>[@customForm.input className="mainBudgets_${region_index}_0_total" name="activity.budget.mainBudgets[${region_index}].details[0].total" type="text" /]
        					</td>
        				</tr>
        				<tr class="main-budget-gender-per-row">
        				     <!-- hidden -->
        				     <input name="activity.budget.mainBudgets[${region_index}].details[1].id" type="hidden" value="1" />
        				     <input name="activity.budget.mainBudgets[${region_index}].details[1].type" type="hidden" value="Gender %" />
        					<td class="budget-gender-def">Gender %</td>
        					<td class="user-data">
        					[@customForm.input className="gender_${region_index}_1_w1w2" name="activity.budget.mainBudgets[${region_index}].details[1].w1w2" type="text"/]
        					</td>
        					<td class="user-data">
        					[@customForm.input className="gender_${region_index}_1_w3" name="activity.budget.mainBudgets[${region_index}].details[1].w3" type="text"/]</td>
							<td class="user-data">
							[@customForm.input className="gender_${region_index}_1_bilateral" name="activity.budget.mainBudgets[${region_index}].details[1].bilateral" type="text"/]</td>        					
        					<td class="user-data">
        					[@customForm.input className="gender_${region_index}_1_other" name="activity.budget.mainBudgets[${region_index}].details[1].other" type="text"/]</td>
        					<td class="calculated-figures">
        						<h6></h6>[@customForm.input className="gender_${region_index}_1_total" name="activity.budget.mainBudgets[${region_index}].details[1].total" type="text" /]
        					</td>
        				</tr>
        				<tr>
        				     <!-- hidden -->
        				     <input name="activity.budget.mainBudgets[${region_index}].details[2].id" type="hidden" value="2" />
        				     <input name="activity.budget.mainBudgets[${region_index}].details[2].type" type="hidden" value="Gender Budget" />
        					<td class="budget-gender-def">Gender Budget</td>
        					<td class="calculated-figures">
        					<h6></h6><input class="geneder_budget_${region_index}_2_w1w2" name="" type="text" readOnly/>
        					</td>
        					<td class="calculated-figures">
        					<h6></h6><input class="geneder_budget_${region_index}_2_w3" name="" type="text" readOnly/>
							<td class="calculated-figures">
							<h6></h6><input class="geneder_budget_${region_index}_2_bilateral" name="" type="text" readOnly/>
        					<td class="calculated-figures">
        					<h6></h6><input class="geneder_budget_${region_index}_2_other" name="" type="text" readOnly/>
        					<td class="totals">
        					<h6></h6>
        					    [@customForm.input className="gender_${region_index}_2_total" name="activity.budget.mainBudgets[${region_index}].details[2].total" type="text" /]
        					</td>
        				</tr>
      				[/#list]
      			[/#if]	
       			       <tr>
        					<td class="sum-color total-per-source-def">Total per source of funding</td>
        					<td class="totals">
        						<h6></h6><input class="source-total-w1w2" name="" type="text" readOnly/>
        					</td>
        					<td class="totals">
        						<h6></h6><input class="source-total-w3" name="" type="text" readOnly/>
        					</td>
							<td class="totals">
								<h6></h6><input class="source-total-bilateral" name="" type="text" readOnly/>
							</td>        					
        					<td class="totals">
        						<h6></h6><input class="source-total-other" name="" type="text" readOnly/>
        					</td>
        					<td class="totals">
        						<h6></h6><input class="source-total" name="" type="text" readOnly/>
        					</td>
        				</tr>
        				<tr>
        					<td class="sum-color">Total Gender %</td>
        					<td class="calculated-figures">
        						<h6></h6><input class="gender-per-total-w1w2" name="" type="text" readOnly/>
        					</td>
        					<td class="calculated-figures">
        						<h6></h6><input class="gender-per-total-w3" name="" type="text" readOnly/>
        					</td>
							<td class="calculated-figures">
								<h6></h6><input class="gender-per-total-bilateral" name="" type="text" readOnly/>
							</td>        					
        					<td class="calculated-figures">
        						<h6></h6><input class="gender-per-total-other" name="" type="text" readOnly/>
        					</td>
        					<td class="calculated-figures">
        						<h6></h6><input class="gender-per-total" name="" type="text" readOnly/>
        					</td>
        				</tr>
        				<tr>
        					<td class="sum-color">Total Gender Budget</td>
        					<td class="totals">
        						<h6></h6><input class="grand-total-w1w2" name="" type="text" readOnly/>
        					</td>
        					<td class="totals">
        						<h6></h6><input class="grand-total-w3" name="" type="text" readOnly/>
        					</td>
							<td class="totals">
								<h6></h6><input class="grand-total-bilateral" name="" type="text" readOnly/>
							</td>        					
        					<td class="totals">
        						<h6></h6><input class="grand-total-other" name="" type="text" readOnly/>
        					</td>
        					<td class="totals">
        						<h6></h6><input class="grand-total" name="" type="text" readOnly/>
        					</td>
        				</tr>
       		</table>
       </div>
      
      <br/><br/>
      
      
      <div class="budget-table">
       		<table class="region-related-table">
       			[#if activity.budget.regBudgets?has_content]
       			<h3 class="budget-topics">
       				Region wise budget 
      			</h3>
      			<br/>
        			[#list activity.budget.regBudgets as region]
        				<tr>
        					<td colspan=6 class="region-row">${region.regionName}</td>
        				</tr>
        				<tr>
        					<td class="budget-region-header">Source</td>
        					<td class="budget-region-header">Total Budget</td>
        					<td class="budget-region-header">Personnel</td>
							<td class="budget-region-header">Operations</td>        					
        					<td class="budget-region-header">Indirect Costs</td>
        					<td class="budget-region-header"></td>
        				</tr>
        				    <td>W1/W2</td>
        				     <!-- hidden -->
        				     <input name="activity.budget.regBudgets[${region_index}].regionId" type="hidden" value="${region.regionId}" />
        				     <input name="activity.budget.regBudgets[${region_index}].regionName" type="hidden" value="${region.regionName}" />
        				     
        				     <input name="activity.budget.regBudgets[${region_index}].details[0].id" type="hidden" value="1" />
        				     <input name="activity.budget.regBudgets[${region_index}].details[0].sourceType" type="hidden" value="W1/W2" />
        					
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_0_main" name="activity.budget.regBudgets[${region_index}].details[0].mainBudgetTotal" type="text"/]
        					</td>
        					<td class="user-data">
        					  [@customForm.input className="reg_budget_${region_index}_0_personnel" name="activity.budget.regBudgets[${region_index}].details[0].personnel" type="text"/]
        					  </td>
							<td class="user-data">
							  [@customForm.input className="reg_budget_${region_index}_0_operations" name="activity.budget.regBudgets[${region_index}].details[0].operations" type="text"/]
							  </td>        					
        					<td class="user-data">
        					  [@customForm.input className="reg_budget_${region_index}_0_indirectCosts" name="activity.budget.regBudgets[${region_index}].details[0].indirectCosts" type="text"/]
        					  </td>
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_0_row_total" name="activity.budget.regBudgets[${region_index}].details[0].total" type="text"/]
        					</td>
        				<tr>
        					<td>W3</td>
        				     <input name="activity.budget.regBudgets[${region_index}].details[1].id" type="hidden" value="2" />
        				     <input name="activity.budget.regBudgets[${region_index}].details[1].sourceType" type="hidden" value="W3" />
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_1_main" name="activity.budget.regBudgets[${region_index}].details[1].mainBudgetTotal" type="text" /]
        					</td>
        					<td class="user-data">
        					  [@customForm.input className="reg_budget_${region_index}_1_personnel" name="activity.budget.regBudgets[${region_index}].details[1].personnel" type="text"/]
        					 </td>
							<td class="user-data">
							  [@customForm.input className="reg_budget_${region_index}_1_operations" name="activity.budget.regBudgets[${region_index}].details[1].operations" type="text"/]
							  </td>        					
        					<td class="user-data">
        					  [@customForm.input className="reg_budget_${region_index}_1_indirectCosts" name="activity.budget.regBudgets[${region_index}].details[1].indirectCosts" type="text"/]
        					  </td>
        					<td class="calculated-figures">
        					  [@customForm.input className="reg_budget_${region_index}_1_row_total" name="activity.budget.regBudgets[${region_index}].details[1].total" type="text" /]
        					</td>
        				</tr>
        				<tr>
        					<td>Bilateral</td>
        					 <input name="activity.budget.regBudgets[${region_index}].details[2].id" type="hidden" value="3" />
        				     <input name="activity.budget.regBudgets[${region_index}].details[2].sourceType" type="hidden" value="Bilateral" />
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_2_main" name="activity.budget.regBudgets[${region_index}].details[2].mainBudgetTotal" type="text" /]
        					</td>
        					<td class="user-data">
        					  [@customForm.input className="reg_budget_${region_index}_2_personnel" name="activity.budget.regBudgets[${region_index}].details[2].personnel" type="text"/]
        					  </td>
							<td class="user-data">
							  [@customForm.input className="reg_budget_${region_index}_2_operations" name="activity.budget.regBudgets[${region_index}].details[2].operations" type="text"/]
							  </td>        					
        					<td class="user-data">
        					   [@customForm.input className="reg_budget_${region_index}_2_indirectCosts" name="activity.budget.regBudgets[${region_index}].details[2].indirectCosts" type="text"/]
        					  </td>
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_2_row_total" name="activity.budget.regBudgets[${region_index}].details[2].total" type="text" /]
        					</td>
        				</tr>
        				<tr>
        					<td>Other</td>
        					 <input name="activity.budget.regBudgets[${region_index}].details[3].id" type="hidden" value="4" />
        				     <input name="activity.budget.regBudgets[${region_index}].details[3].sourceType" type="hidden" value="Other" />
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_3_main" name="activity.budget.regBudgets[${region_index}].details[3].mainBudgetTotal" type="text" /]
        					</td>
        					<td class="user-data">
        					 [@customForm.input className="reg_budget_${region_index}_3_personnel" name="activity.budget.regBudgets[${region_index}].details[3].personnel" type="text"/]
        					 </td>
							<td class="user-data">
							 [@customForm.input className="reg_budget_${region_index}_3_operations" name="activity.budget.regBudgets[${region_index}].details[3].operations" type="text"/]
							 </td>        					
        					<td class="user-data">
        					 [@customForm.input className="reg_budget_${region_index}_3_indirectCosts" name="activity.budget.regBudgets[${region_index}].details[3].indirectCosts" type="text"/]
        					 </td>
        					<td class="calculated-figures">
        						<h6></h6>
        						[@customForm.input className="reg_budget_${region_index}_3_row_total" name="activity.budget.regBudgets[${region_index}].details[3].total" type="text"/]
        					</td>
        				</tr>
        				<tr>
        					<td class="budget-region-def">Total</td>
        					<td class="calculated-figures">
        						<h6></h6><input class="reg_budget_${region_index}_4_main" name="" type="text" readOnly/>
        					</td>
        					<td class="user-data">
        						<h6></h6><input class="reg_budget_${region_index}_4_personal_total" name="" type="text" readOnly/>
        					</td>
							<td class="user-data">
								<h6></h6><input class="reg_budget_${region_index}_4_operations_total" name="" type="text" readOnly/>
							</td>        					
        					<td class="user-data">
        						<h6></h6><input class="reg_budget_${region_index}_4_indirect_costs_total" name="" type="text" readOnly/>
        					</td>
        					<td class="calculated-figures">
        						<h6></h6><input class="reg_budget_${region_index}_4_row_total" name="" type="text" readOnly/>        
        					</td>
        				</tr>
      				[/#list]
      			[/#if]	
       		</table>
       </div>
      
      <br/><br/>
      <h3 class="budget-topics">
       		Donor wise budget 
      </h3>
      <br/>
      <div class="budget-table">
       		<table class="donor-budget-table">
       			<tr>
       				<th>Source</th>
       				<th>W3</th>
       				<th>Bilateral</th>
       				<th>Other</th>
       				<th>Total</th>
       			</tr>
       			[#if activity.donors?has_content]
        			[#list activity.donors as donor]
		       			<tr class="donor-budget-row">
		       				<td>
		       					[@customForm.input className="donor_budget_${donor_index}_name" name="activity.budget.donorsBudget[${donor_index}].donorName" type="text"/]
		       				</td>
		       				<td class="user-data">
		       					[@customForm.input className="donor_budget_${donor_index}_w3" name="activity.budget.donorsBudget[${donor_index}].w3" type="text"/]
		       				</td>
		       				<td class="user-data">
		       					[@customForm.input className="donor_budget_${donor_index}_bilateral" name="activity.budget.donorsBudget[${donor_index}].bilateral" type="text"/]
		       				</td>
		       				<td class="user-data">
		       					[@customForm.input className="donor_budget_${donor_index}_other" name="activity.budget.donorsBudget[${donor_index}].other" type="text"/]
		       				</td>
		       				<td class="totals budget-donor-total">
		       					<h6></h6><input class="donor_budget_total_${donor_index}" name="" type="text" readOnly/>
		       				</td>
		       			</tr>
		       		[/#list]
      			[/#if]			
       			<tr>
       				<td class="budget-donor-total ">Total</td>
       				<td class="totals budget-donor-total ">
       					<h6></h6><input class="donor_budget_total_w3" name="" type="text" readOnly/>
       				</td>
       				<td class="totals budget-donor-total ">
       					<h6></h6><input class="donor_budget_total_bilateral" name="" type="text" readOnly/>
       				</td>
       				<td class="totals budget-donor-total ">
       					<h6></h6><input class="donor_budget_total_other" name="" type="text" readOnly/>
       				</td>
       				<td class="totals budget-donor-total ">
       					<h6></h6><input class="donor_budget_grand_total" name="" type="text" readOnly/>
       				</td>
       			</tr>
       		</table>
       </div>
       <div class="clear-both">&nbsp;</div>
	   <div class="addLink">
		  <img src="${baseUrl}/images/global/icon-add.png" />
		  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=budget_review&cstatus=${activity.activityStatus}">
		    [@s.text name="planning.comments.addComment" /]
		  </a>
	  </div>
      
      <br/><br/>
   <!-- internal parameter -->
    <input name="activityID" type="hidden" value="${activity.id?c}" />
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
    </fieldset>
  </article>
  [/@s.form]
 
  </section>
  
[#include "/WEB-INF/global/pages/footer.ftl"]