[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Outcomes Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/objectivesPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "objectives" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.objectives.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  
  [@s.form action="objectives"]  
  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="objectivesBlock">
      <legend><h6> [@s.text name="planning.objectives" /]  </h6></legend>
    
    [#if activity.objectives?has_content]
      [#list activity.objectives as objective]
        <div id="objective-${objective_index}" class="objective">
          [#-- Objective identifier --]
          <input type="hidden" name="activity.objectives[${objective_index}].id" value="${objective.id?c}" />
          
          [#-- remove link --]      
          <div class="removeLink">            
              <img src="${baseUrl}/images/global/icon-remove.png" />
              <a id="removeObjective-0" href="" class="removeObjective">
                [@s.text name="planning.objectives.removeObjective" /]
              </a>
          </div>      
          
          [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.activityOutcomes.outcomes" /] ${objective_index +1}
          </div>
          
           <div class="fullBlock">
      		[@customForm.radioButtonGroup name="activity.objectives[${objective_index}].outcomeType" i18nkey="planning.outcome.type" listName="outcomeTypes" label="" value="activity.objectives[${objective_index}].outcomeType" required=true/]
      	  </div>
          
          <div class="helpBlock">    
          [@customForm.textArea name="activity.objectives[${objective_index}].description" i18nkey="planning.outcome.description"  required=true/]
 		  	<div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('A research outcome is the usage of research that results<br/> in changes in knowledge, skills, practice, improved policies, etc.<br/>  Examples include planners using decision support models;<br/> improved capacity of the government to integrate ESS into planning scenarios;<br/> revised policies that integrate gender into irrigation planning.<br/>Specify the timeframe for the expected outcome (expected to be approx. 2-3 years)')"
      					onmouseout="UnTip()"/>
      	    	</div> 
 		  
 		  </div> 
 		  <div class="clear-both"></div> 
 		  
 		  [#-- Research User--]
 		  <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].researchUser"  i18nkey="planning.outcome.users" /]
 		    <div class="helpImg">
      		<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Who will use the research to<br/>achieve the outcome (be specific)')"
      			onmouseout="UnTip()"/>
      		</div> 
      	  </div>
      	  
      	   [#-- Research Roles --]
		  <div class="helpHalfPartBlock">
		   [@customForm.select name="activity.objectives[${objective_index}].researchUserRole" label="" i18nkey="planning.outcome.roles" 
		       listName="researchRoles" keyFieldName="value"  displayFieldName="displayName" value="activity.objectives[${objective_index}].researchUserRole" multiple=true/] 
		  	   <div class="helpImg">
      				<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('How will the specific research users (above) apply the research')"
      					onmouseout="UnTip()"/>
      			</div>
		  </div>
		   <div class="clear-both"></div> 
      	  
      	  
      	  [#-- Research Outcome Changes --]
      	   <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].researchOutcomeChanges"  i18nkey="planning.outcome.changes" /]
      	  	<div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Which project outputs are tailored for the specific research users')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	  [#-- Gender Description --]
      	   <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].genderDescription"  i18nkey="planning.outcome.gender.desc" /]
      	  	<div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('How is gender addressed in selecting research users, understanding their research needs,<br/> and tailored research outputs to fit their needs')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	  [#-- Intermediaries --]
      	   <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].intermediaries"  i18nkey="planning.outcome.intermediaries" /]
      	  	<div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Which intermediaries will the project work with to support research users <br/> (e.g., a policy network, a working group, a donor working group, a policy <br/>liaison champion, a local NGO working with farmers)')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	   [#-- capacityBuilding --]
      	   <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].capacityBuilding"  i18nkey="planning.outcome.capacityBuilding" /]
      	  	<div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Explain any capacity building components in this project')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	   [#-- progress --]
      	   <div class="helpBlock">
 		    [@customForm.input name="activity.objectives[${objective_index}].progress"  i18nkey="planning.outcome.progress" /]
      	  	<div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('What progress do you expect this year toward research usage <br/> (e.g. greater interest, changes in decisions, training completed)?<br/>Detail progress indicators and list the information/data that will<br/> demonstrate progress')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
 		   <div class="clear-both"></div> 
 
          <hr />
        </div>
      [/#list]
    [/#if]

    <div id="addDeliverableBlock" class="addLink">
      <img src="${baseUrl}/images/global/icon-add.png" />
      <a href="" class="addObjective" >[@s.text name="planning.objectives.addObjective" /]</a>
    </div>
    </fieldset>
    
    
    [#-- Objective template --]
    <div id="objectiveTemplate" style="display:none">
      [#-- Objective identifier --]
      <input type="hidden" name="id" value="-1" />
      
      [#-- remove link --]      
      <div class="removeLink">            
        <img src="${baseUrl}/images/global/icon-remove.png" />
        <a id="removeObjective" href="" class="removeObjective">
          [@s.text name="planning.objectives.removeObjective" /]
        </a>
      </div>      
      
      [#-- Item index --]
      <div class="itemIndex">
          [@s.text name="planning.activityOutcomes.outcomes" /]
      </div>
      
      <div class="fullBlock">
      		[@customForm.radioButtonGroup name="outcomeType" i18nkey="planning.outcome.type" listName="outcomeTypes" label="" required=true /]
      </div>  
      
      <div class="helpBlock">    
      [@customForm.textArea name="description" i18nkey="planning.outcome.description" required=true /]
 		  	<div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('A research outcome is the usage of research that results<br/> in changes in knowledge, skills, practice, improved policies, etc.<br/>  Examples include planners using decision support models;<br/> improved capacity of the government to integrate ESS into planning scenarios;<br/> revised policies that integrate gender into irrigation planning.<br/>Specify the timeframe for the expected outcome (expected to be approx. 2-3 years)')"
      					onmouseout="UnTip()"/>
      	    </div> 
 		  
	  </div> 
 	  <div class="clear-both"></div>
 	  
 	      [#-- Research User--]
 		  <div class="helpBlock">
 		    [@customForm.input name="researchUser"  i18nkey="planning.outcome.users" /]
      	    <div class="helpImg">
      	    <img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Who will use the research to<br/>achieve the outcome (be specific)')"
      			onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	   [#-- Research Roles --]
		  <div class="helpHalfPartBlock">
		   [@customForm.select name="researchUserRole" label="" i18nkey="planning.outcome.roles" listName="researchRoles" keyFieldName="value"  displayFieldName="displayName" multiple=true/] 
		   <div class="helpImg">
      				<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('How will the specific research users (above) apply the research')"
      					onmouseout="UnTip()"/>
      			</div>
      		<div class="clear-both"></div>
		  </div>
      	  
      	  [#-- Research Outcome Changes --]
      	   <div class="helpBlock">
 		    [@customForm.input name="researchOutcomeChanges"  i18nkey="planning.outcome.changes" /]
      	    <div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Which project outputs are tailored for the specific research users')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	  [#-- Gender Description --]
      	   <div class="helpBlock">
 		    [@customForm.input name="genderDescription"  i18nkey="planning.outcome.gender.desc" /]
      	    <div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('How is gender addressed in selecting research users, understanding their research needs,<br/> and tailored research outputs to fit their needs')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	  [#-- Intermediaries --]
      	   <div class="helpBlock">
 		    [@customForm.input name="intermediaries"  i18nkey="planning.outcome.intermediaries" /]
      	    <div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Which intermediaries will the project work with to support research users <br/> (e.g., a policy network, a working group, a donor working group, a policy <br/>liaison champion, a local NGO working with farmers)')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	   [#-- capacityBuilding --]
      	   <div class="helpBlock">
 		    [@customForm.input name="capacityBuilding"  i18nkey="planning.outcome.capacityBuilding" /]
      	    <div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('Explain any capacity building components in this project')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      	  
      	   [#-- progress --]
      	   <div class="helpBlock">
 		    [@customForm.input name="progress"  i18nkey="planning.outcome.progress" /]
      	    <div class="helpImg">
      			<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      				onmouseover="Tip('What progress do you expect this year toward research usage <br/> (e.g. greater interest, changes in decisions, training completed)?<br/>Detail progress indicators and list the information/data that will<br/> demonstrate progress')"
      				onmouseout="UnTip()"/>
      		</div>
      	  </div>
      <div class="clear-both"></div>
      <hr />
    </div>
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=outcomes_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]