[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Deliverables Outreachs Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/outreachsPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "outreachs" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.activityOutreachs.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="outreachs"]  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="outreachsBlock">
      <legend> <h6> [@s.text name="planning.activityOutreachs" /] </h6> </legend>
      [#if activity.outreachs?has_content]
        [#list activity.outreachs as outreach]
          <div id="outreach-${outreach_index}" class="outreach">
            [#-- identifier --]
            <input name="activity.outreachs[${outreach_index}].id" type="hidden" value="${outreach.id?c}" />
            
            [#-- Adding remove link only for new outreachs --]
            <div class="removeLink">
              <img src="${baseUrl}/images/global/icon-remove.png" />
              <a id="removeOutreachs-${outreach_index}" href="" class="removeOutreachs">
                [@s.text name="planning.activityOutreachs.removeOutreachs" /]
              </a>
            </div>
            
            [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.activityOutreachs.outreach" /] ${outreach_index +1}
            </div>
            
            [#-- Type of Outreach --]
            <div class="fullBlock">
              [@customForm.select name="activity.outreachs[${outreach_index}].type" i18nkey="planning.activityOutreachs.type" required=true label="" required=true listName="outreachTypes" keyFieldName="value"  displayFieldName="displayName" value="activity.outreachs[${outreach_index}].type"/]
            </div>
            
            [#-- Groups --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.outreachs[${outreach_index}].groups" i18nkey="planning.activityOutreachs.groups"/]
            </div>
            
            [#-- Participants --]
            <div class="fullBlock">
             [@customForm.input name="activity.outreachs[${outreach_index}].paricipants" i18nkey="planning.activityOutreachs.paricipants" required=true /]
            </div>
            
            [#-- Female Participants --]
            <div class="fullBlock">
             [@customForm.input name="activity.outreachs[${outreach_index}].femaleParticipants" i18nkey="planning.activityOutreachs.femaleParticipants" required=true /]
            </div>
            
             [#-- Date & Location --]
             <div class="fullBlock">
              [@customForm.input name="activity.outreachs[${outreach_index}].dateAndLocation" i18nkey="planning.activityOutreachs.dateAndLocation" required=true /]
             </div>
            
            [#-- Comments --]
            <div class="fullBlock">
              [@customForm.input name="activity.outreachs[${outreach_index}].comments" i18nkey="planning.activityOutreachs.comments" required=true /]
            </div>
            
          	<div class="clear-both"></div> 
          </div> <!-- End outreach-${outreach_index} -->
          <hr />
        [/#list]
      [/#if]
      
      <div id="addOutreachsBlock" class="addLink">
        <img src="${baseUrl}/images/global/icon-add.png" />
        <a href="" class="addOutreachs" >[@s.text name="planning.activityOutreachs.addOutreach" /]</a>
      </div>
    
    </fieldset>
    [#-- Outreachs template --]
    <div id="outreachTemplate" style="display:none">
      <div id="outreach-9999" class="outreach" style="display: none;">      
        [#-- remove link --]      
        <div class="removeLink">            
            <img src="${baseUrl}/images/global/icon-remove.png" />
            <a id="removeOutreachs-9999" href="" class="removeOutreachs">
              [@s.text name="planning.activityOutreachs.removeOutreachs" /]
            </a>
        </div>
        
        [#-- identifier --]
        <input name="id" type="hidden" value="-1" />
        
        [#-- Item index --]
        <div class="itemIndex">
          [@s.text name="planning.activityOutreachs.outreach" /]
        </div>
        
        
        [#-- Type of Outreach --]
        <div class="fullBlock">
           [@customForm.select name="type" i18nkey="planning.activityOutreachs.type" required=true label="" required=true listName="outreachTypes" keyFieldName="value"  displayFieldName="displayName"/]
        </div>
          
        [#-- Groups --]
        <div class="fullBlock">
           [@customForm.textArea name="groups" i18nkey="planning.activityOutreachs.groups"/]
        </div>
            
        [#-- Participants --]
        <div class="fullBlock">
           [@customForm.input name="paricipants" i18nkey="planning.activityOutreachs.paricipants" required=true /]
        </div>
            
        [#-- Female Participants --]
        <div class="fullBlock">
          [@customForm.input name="femaleParticipants" i18nkey="planning.activityOutreachs.femaleParticipants" required=true /]
        </div>
            
        [#-- Date & Location --]
        <div class="fullBlock">
          [@customForm.input name="dateAndLocation" i18nkey="planning.activityOutreachs.dateAndLocation" required=true /]
        </div>
           
        [#-- Comments --]
        <div class="fullBlock">
           [@customForm.input name="comments" i18nkey="planning.activityOutreachs.comments" required=true /]
        </div>
        
        <div class="clear-both"></div> 
        <hr />
      </div> <!-- End outreach template -->
    </div>
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=outreachs_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]