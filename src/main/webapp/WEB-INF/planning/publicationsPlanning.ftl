[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Publications Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/publicationsPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "publications" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.activityPublications.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="publications"]  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="publicationsBlock">
      <legend> <h6> [@s.text name="planning.activityPublications" /] </h6> </legend>
      [#if activity.publications?has_content]
        [#list activity.publications as publication]
          <div id="publication-${publication_index}" class="publication">
            [#-- identifier --]
            <input name="activity.publications[${publication_index}].id" type="hidden" value="${publication.id?c}" />
            
            [#-- Adding remove link only for new publications --]
            <div class="removeLink">
              <img src="${baseUrl}/images/global/icon-remove.png" />
              <a id="removePublication-${publication_index}" href="" class="removePublication">
                [@s.text name="planning.activityPublications.removePublication" /]
              </a>
            </div>
            
            [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.activityPublications.publication" /] ${publication_index +1}
            </div>
            
            [#-- Publication Reference --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.publications[${publication_index}].publicationReference" i18nkey="planning.activityPublications.publicationReference" required=true /]
            </div>
            
            [#-- Peer Reviewed --]
            <div class="fullBlock">
              [@customForm.radioButtonGroup name="activity.publications[${publication_index}].peerReviewed" i18nkey="planning.activityPublications.peerReviewed" label="" required=true listName="yesNoOptions"/]
            </div>
            
            [#-- Published in ISI Journal --]
            <div class="fullBlock">
              [@customForm.radioButtonGroup name="activity.publications[${publication_index}].isiJournal" i18nkey="planning.activityPublications.isiJournal" label="" required=true listName="yesNoOptions"/]
            </div>
            
            [#-- Weblink --]
            <div class="fullBlock">
              [@customForm.input name="activity.publications[${publication_index}].webLink" i18nkey="planning.activityPublications.webLink" required=true /]
            </div>
            
          	<div class="clear-both"></div> 
          </div> <!-- End publication-${publication_index} -->
          <hr />
        [/#list]
      [/#if]
      
      <div id="addPublicationBlock" class="addLink">
        <img src="${baseUrl}/images/global/icon-add.png" />
        <a href="" class="addPublication" >[@s.text name="planning.activityPublications.addPublication" /]</a>
      </div>
    
    </fieldset>
    [#-- Publication template --]
    <div id="publicationTemplate" style="display:none">
      <div id="publication-9999" class="publication" style="display: none;">      
        [#-- remove link --]      
        <div class="removeLink">            
            <img src="${baseUrl}/images/global/icon-remove.png" />
            <a id="removePublication-9999" href="" class="removePublication">
              [@s.text name="planning.activityPublications.removePublication" /]
            </a>
        </div>
        
        [#-- identifier --]
        <input name="id" type="hidden" value="-1" />
        
        [#-- Item index --]
        <div class="itemIndex">
          [@s.text name="planning.activityPublications.publication" /]
        </div>
        
        [#-- Publication Reference --]
        <div class="fullBlock">                      
          [@customForm.textArea name="publicationReference" i18nkey="planning.activityPublications.publicationReference" required=true /]
        </div>
        
        [#-- Peer Reviewed --]
        <div class="fullBlock">
          [@customForm.radioButtonGroup name="peerReviewed" i18nkey="planning.activityPublications.peerReviewed" label="" required=true listName="yesNoOptions"/]
        </div>
          
        [#-- Published in ISI Journal --]
        <div class="fullBlock">
         [@customForm.radioButtonGroup name="isiJournal" i18nkey="planning.activityPublications.isiJournal" label="" required=true listName="yesNoOptions"/]
        </div>
          
        [#-- Weblink --]
        <div class="fullBlock">
          [@customForm.input name="webLink" i18nkey="planning.activityPublications.webLink" required=true /]
        </div>
        
        <div class="clear-both"></div> 
       
        
        <hr />
      </div> <!-- End publication template -->
    </div>
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=publications_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]