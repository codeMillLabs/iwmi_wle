[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Indicators Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/indicatorsPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "indicators" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.activityIndicators.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="indicators"]  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="indicatorsBlock">
      <legend> <h6> [@s.text name="planning.activityIndicators" /] </h6> </legend>
      [#if activity.indicators?has_content]
        [#list activity.indicators as indicator]
          <div id="indicator-${indicator_index}" class="indicator">
            [#-- identifier --]
            <input name="activity.indicators[${indicator_index}].id" type="hidden" value="${indicator.id?c}" />
            
            [#-- Item index --]
            <div class="itemIndexLeft">
              [@s.text name="activity.indicators[${indicator_index}].itype.displayName" /] : 	
              [@s.text name="activity.indicators[${indicator_index}].subType.displayName" /]
            </div>
            <div>
            	[@s.text name="activity.indicators[${indicator_index}].subType.value" /]
            </div>
            
            [@customForm.input name="activity.indicators[${indicator_index}].itype.id" display=false/]
            [@customForm.input name="activity.indicators[${indicator_index}].subType.id" display=false/]
            
            [#-- Type of Target --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.indicators[${indicator_index}].target" i18nkey="planning.activityIndicators.target" required=true/]
            </div>
            
            [#-- Type of Actual --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.indicators[${indicator_index}].actual" i18nkey="planning.activityIndicators.actual" required=true/]
            </div>
            
            [#-- Type of Contribution --]
            <div class="helpBlock">
              [@customForm.textArea name="activity.indicators[${indicator_index}].contribution" i18nkey="planning.activityIndicators.contribution" required=true/]
              <div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('Where more than 1 activity contributes, provide a breakdown, eg 1.1 – 3, 1.2 - 4')"
      					onmouseout="UnTip()"/>
      	    	</div> 
            </div>
            
            [#-- Type of Comment --]
            <div class="helpBlock">
              [@customForm.textArea name="activity.indicators[${indicator_index}].comment" i18nkey="planning.activityIndicators.comment" required=true/]
               <div class="helpImg">
      				<img src="${baseUrl}/images/global/icon-help.png" 
      					onmouseover="Tip('include deviation narrative if actual is more than 10% away from target.  Mention any uncertainty related to the indicator')"
      					onmouseout="UnTip()"/>
      	    	</div> 
            </div>
            
          	<div class="clear-both"></div> 
          </div> <!-- End indicator-${indicator_index} -->
          <hr />
        [/#list]
      [/#if]
    </fieldset>
    
     <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=indicators_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]