[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Main information" /]
[#assign globalLibs = ["jquery", "noty", "chosen"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js", "${baseUrl}/js/planning/mainInformation.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "mainInformation" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]    

<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.mainInformation.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="mainInformation"]  
  <article class="halfContent" id="mainInformation">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: 
      [#if activity.commissioned] 
        [@s.text name="planning.mainInformation.commissionedActivity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
      [#else] 
        [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
      [/#if]
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    [#-- Budget identifier --]
    [#--  <input name="activity.budget.id" value="${activity.budget.id?c}" type="hidden"/> --]
    [#-- Budget 'No funds' text --]
    [#-- <input id="activity.budget.noFunds" value="[@s.text name="planning.mainInformation.budget.noFunds" /] " type="hidden"/> --]
    
    
    
    [#-- Hidden values used by js --]
    <input id="minDateValue" value="${startYear?c}-01-01" type="hidden"/>
    <input id="maxDateValue" value="${endYear?c}-12-31" type="hidden"/>
    
    [#-- Commissioned and continuous --]
    [#if activity.continuousActivity?has_content]
      <div class="fullblock">
        <div class="halfPartBlock continuation">
          [#if activity.continuousActivity?has_content]
            [@s.text name="planning.mainInformation.continuationActivity" /] 
            <a href="[@s.url action='activity' namespace="/"][@s.param name='${publicActivtyRequestParameter}']${activity.continuousActivity.id}[/@s.param][/@s.url]" target="_blank"> 
              [@s.text name="planning.mainInformation.activity" /] ${activity.continuousActivity.activityId} 
            </a>
          [/#if]
        </div>
      </div>
    [/#if]
    
    [#-- Lead Center --]
    <div class="halfPartBlock">
      [@customForm.select name="activity.leadCenter" label="" i18nkey="planning.mainInformation.leadCenter" 
       listName="leadCentersLookup" keyFieldName="value" displayFieldName="displayName" value="activity.leadCenter" /] 
    </div>
    
    [#-- Year --]
    <div class="halfPartBlock">
      [@customForm.select name="activity.year" label="" i18nkey="planning.mainInformation.year" 
       listName="yearLookup" keyFieldName="value"  displayFieldName="displayName" value="activity.year" /] 
    </div>
    
    [#-- Title --]
    <div class="fullBlock">
      [@customForm.textArea name="activity.title" i18nkey="planning.mainInformation.title" required=true /]
    </div>
    
    <fieldset class="fullBlock">
        <legend>Contact persons</legend>
        <div id="contactPersonBlock">
          [#if activity.contactPersons?has_content]
            [#-- If there is contact persons show the list --]
            [#list activity.contactPersons as contactPerson]
              
              <div id="contactPerson-${contactPerson_index}" class="contactPerson">
                [#-- Contact person id--]
                <input type="hidden" name="activity.contactPersons[${contactPerson_index}].id" value="${contactPerson.id?c}">
                
                [#-- Contact name --]
                <div class="halfPartBlock">
                  [@customForm.input name="activity.contactPersons[${contactPerson_index}].name" type="text" i18nkey="planning.mainInformation.contactName" required=true /]
                </div>
                
                [#-- Contact email --]
                <div class="halfPartBlock">
                  [@customForm.input name="activity.contactPersons[${contactPerson_index}].email" type="text" i18nkey="planning.mainInformation.contactEmail" required=true /]
                </div>
        
                [#-- Adding remove image --]
                <a href="#" >
                  <img src="${baseUrl}/images/global/icon-remove.png" class="removeContactPerson" />
                </a>
                
              </div>
            [/#list]
          [#else]
            <div class="contactPerson">
              [#-- Contact person id--]
              <input type="hidden" name="activity.contactPersons[0].id" value="-1">
              
              [#-- Contact person --]
              <div class="halfPartBlock">
                [@customForm.input name="activity.contactPersons[0].name" type="text" i18nkey="planning.mainInformation.contactName" value="" /]
              </div>
              
              [#-- Contact email --]
              <div class="halfPartBlock">
                [@customForm.input name="activity.contactPersons[0].email" type="text" i18nkey="planning.mainInformation.contactEmail" value="" /]
              </div>
              
              [#-- Adding remove image --]
              <a href="#" >
                <img src="${baseUrl}/images/global/icon-remove.png" class="removeContactPerson" />
              </a>
            </div>
          [/#if]
        </div>
    
    <div id="addContactPerson" class="addLink">
      <img src="${baseUrl}/images/global/icon-add.png" />
      <a href="" class="addContactPerson" >[@s.text name="planning.mainInformation.addContactPerson" /]</a>
    </div>
    </fieldset>
    
    
    [#-- Contact person template --]
    <div id="contactPersonTemplate" style="display:none;">
      [#-- Contact person id--]
      <input type="hidden" name="id" value="-1">
      
      [#-- Contact name --]
      <div class="halfPartBlock">
        [@customForm.input name="name" type="text" i18nkey="planning.mainInformation.contactName" /]
      </div>
      
      [#-- Contact email --]
      <div class="halfPartBlock">
        [@customForm.input name="email" type="text" i18nkey="planning.mainInformation.contactEmail" /]
      </div>

      [#-- Adding remove image --]
      <a href="#" >
        <img src="${baseUrl}/images/global/icon-remove.png" class="removeContactPerson" />
      </a>
      
      <input id="noResultByFilterText" type="hidden" value="[@s.text name="planning.noResultFilterMatch" /]" />
      
    </div>
    
    [#-- Description --]
    <div class="helpBlock">
      [@customForm.textArea name="activity.description" i18nkey="planning.mainInformation.descripition" required=true /]
      <div class="helpImg">
      		<img src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Short description of the activity including rationale <br/> (Approx. 300 words)')"
      			onmouseout="UnTip()"/>
      </div> 
      <div class="clear-both"></div>      
    </div>
    

    [#-- Project Name --]
    <div class="helpBlock">
      [@customForm.input name="activity.projectName" type="text" i18nkey="planning.mainInformation.projectName" required=true /]
      <div class="helpImg">
      		<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Short title and/or code as used in your Center admin.')"
      			onmouseout="UnTip()"/>
      </div> 
      <div class="clear-both"></div>     
    </div>
    
    [#-- Flagship Cluster --]
     <input id="selectedFlagship" type="hidden" value="${activity.flagShipSRP}"/>
     <div class="helpHalfPartBlock" >
       [@customForm.select name="activity.flagShipSRP" label="" i18nkey="planning.mainInformation.flagShipSRP" 
       listName="flagshipLookup" keyFieldName="value"  displayFieldName="displayName" value="activity.flagShipSRP" className="flagShipSRP" /] 
       <div class="helpImg">
      		<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Select')"
      			onmouseout="UnTip()"/>
      </div>  
    </div>
    
    [#-- Activity Cluster --]
    <input id="selectedActivityCluster" type="hidden" value="${activity.activityCluster}" />
    <div class="helpHalfPartBlock">
     [@customForm.select name="activity.activityCluster" label="" i18nkey="planning.mainInformation.activityCluster" 
       listName="activityClusterLookup" keyFieldName="value"  displayFieldName="displayName" value="activity.activityCluster" className="activityCluster" /] 
       <div class="helpImg">
      		<img class="margin-img" src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Select')"
      			onmouseout="UnTip()"/>
      </div>
      <div class="clear-both"></div>     
    </div>
     
    [#-- Eco System --]
    <div class="helpBlock">
      [@customForm.textArea name="activity.ecoSystemsDesc" i18nkey="planning.mainInformation.ecoSystemsDesc" /]
      <div class="helpImg">
      		<img src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Description of Ecosystems Services impacts being considered by the activity.<br/> Both approaches that consider environmental impact, and/or ecosystem<br/> services are valued and should be highlighted. If you require any advice on<br/> integrating ESS&R into your WLE activity, please contact FabriceDeClerck <br/>(fdeclerck@cgiar.org)')"
      			onmouseout="UnTip()"/>
      </div> 
      <div class="clear-both"></div>     
    </div>
    
    [#-- Milestones --]
    [#--
    <div class="halfPartBlock">
      [@customForm.select name="activity.milestone" label="" i18nkey="planning.mainInformation.milestone" listName="milestones" keyFieldName="id"  displayFieldName="code" value="${activity.milestone.id?c}" className="milestones" required=true /]
    </div> --]
   
   [#-- Logframe link --]
   [#--
    <div class="halfPartBlock">
      <a href="${logFramDocPath}" target="_blank">View Logframe </a> 
    </div> --]
   
    [#-- Start Date --]
    <div class="halfPartBlock">
      [@customForm.input name="activity.startDate" type="text" i18nkey="planning.mainInformation.startDate" required=true /]
    </div>
    
    [#-- End Date --]
    <div class="halfPartBlock">
      [@customForm.input name="activity.endDate" type="text" i18nkey="planning.mainInformation.endDate" required=true /]
    </div>
    
    [#-- Gender integration --]
    <div id="gender">
      <div class="fullBlock">
        [@customForm.radioButtonGroup i18nkey="planning.mainInformation.genderIntegration" label="" name="genderIntegrationOption" listName="genderOptions" value="${hasGender?string('1', '0')}" /]
      </div>
      <div class="helpBlock genderIntegrationsDescription">
        [@customForm.textArea name="activity.genderIntegrationsDescription" i18nkey="planning.mainInformation.genderIntegrationDescription"/]
        <div class="helpImg">
      		<img src="${baseUrl}/images/global/icon-help.png" 
      			onmouseover="Tip('Provide a short description of how you anticipate your activity<br/> to integrate gender (as at least 10% WLE portfolio will be<br/> comprised of gender research). Gender-specific research is<br/> encouraged, as are gender components of wider projects.<br/>  WLE also recognizes that it might not be easy to incorporate<br/> gender in all projects. In case you require any advice on<br/> integrating gender into any of your activities, please contact<br/> Nicoline de Haan (ndehaan@cgiar.org)')"
      			onmouseout="UnTip()"/>
      	</div> 
      	      <div class="clear-both"></div>     
      	
      </div>
    </div>
    
    [#-- Activity Reviewer Selection --]
    <div class="halfPartBlock">
    [@customForm.select name="activity.activityInternalReviewers" label="" i18nkey="planning.internal.reviewers" 
		       listName="intReviewers" keyFieldName="id"  displayFieldName="name" value="activity.activityInternalReviewers" multiple=true/] 
    </div>
    <div class="halfPartBlock">
    </div>
    
    <div class="halfPartBlock">
    [@customForm.select name="activity.activityExternalReviewers" label="" i18nkey="planning.external.reviewers" 
		       listName="extReviewers" keyFieldName="id"  displayFieldName="name" value="activity.activityExternalReviewers" multiple=true/] 
    </div>
    <div class="halfPartBlock">
    </div>
    
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=summary_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
               
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
    <div class="clear-both">&nbsp;</div>
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]