[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity Deliverables Materials Planning" /]
[#assign globalLibs = ["jquery", "noty"] /]
[#assign customJS = ["${baseUrl}/js/planning/materialsPlanning.js", "${baseUrl}/js/wz_tooltip.js", "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]
[#assign currentPlanningSection = "materials" /]

[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]

[#if activity.editable]
	[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
[#else]
	[#import "/WEB-INF/global/macros/forms-noedit.ftl" as customForm/]
[/#if]
    
<section class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.activityMaterials.help" /] </p>
  </div>
  [#include "/WEB-INF/global/pages/planning-secondary-menu.ftl" /]
  
  [@s.form action="materials"]  
  <article class="halfContent">
    <h1 class="contentTitle">
      ${activity.leader.acronym}: [@s.text name="planning.mainInformation.activity" /] ${activity.formattedId} : [@s.text name="planning.mainInformation.revision" /] ${activity.revisionId}
    </h1>
    
    [#-- Activity identifier --]
    <input name="activityID" value="${activity.id?c}" type="hidden"/>
    
    <fieldset id="materialsBlock">
      <legend> <h6> [@s.text name="planning.activityMaterials" /] </h6> </legend>
      [#if activity.materials?has_content]
        [#list activity.materials as material]
          <div id="material-${material_index}" class="material">
            [#-- identifier --]
            <input name="activity.materials[${material_index}].id" type="hidden" value="${material.id?c}" />
            
            [#-- Adding remove link only for new materials --]
            <div class="removeLink">
              <img src="${baseUrl}/images/global/icon-remove.png" />
              <a id="removeMaterials-${material_index}" href="" class="removeMaterials">
                [@s.text name="planning.activityMaterials.removeMaterials" /]
              </a>
            </div>
            
            [#-- Item index --]
            <div class="itemIndex">
              [@s.text name="planning.activityMaterials.material" /] ${material_index +1}
            </div>
            
            [#-- Type of Deliverable --]
            <div class="fullBlock">
              [@customForm.select name="activity.materials[${material_index}].type" i18nkey="planning.activityMaterials.type" required=true label="" required=true listName="materialTypes" keyFieldName="value"  displayFieldName="displayName" value="activity.materials[${material_index}].type"/]
            </div>
            
            [#-- Other Description --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.materials[${material_index}].otherDescription" i18nkey="planning.activityMaterials.otherDescription"/]
            </div>
            
            [#-- Brief Description --]
            <div class="fullBlock">
              [@customForm.textArea name="activity.materials[${material_index}].briefDescription" i18nkey="planning.activityMaterials.briefDescription" required=true /]
            </div>
            
            [#-- Target Audience --]
            <div class="fullBlock">
             [@customForm.input name="activity.materials[${material_index}].targetAudience" i18nkey="planning.activityMaterials.targetAudience" required=true /]
            </div>
            
            [#-- Weblink --]
            <div class="fullBlock">
              [@customForm.input name="activity.materials[${material_index}].webLink" i18nkey="planning.activityMaterials.webLink" required=true /]
            </div>
            
          	<div class="clear-both"></div> 
          </div> <!-- End material-${material_index} -->
          <hr />
        [/#list]
      [/#if]
      
      <div id="addMaterialsBlock" class="addLink">
        <img src="${baseUrl}/images/global/icon-add.png" />
        <a href="" class="addMaterials" >[@s.text name="planning.activityMaterials.addMaterial" /]</a>
      </div>
    
    </fieldset>
    [#-- Materials template --]
    <div id="materialTemplate" style="display:none">
      <div id="material-9999" class="material" style="display: none;">      
        [#-- remove link --]      
        <div class="removeLink">            
            <img src="${baseUrl}/images/global/icon-remove.png" />
            <a id="removeMaterials-9999" href="" class="removeMaterials">
              [@s.text name="planning.activityMaterials.removeMaterials" /]
            </a>
        </div>
        
        [#-- identifier --]
        <input name="id" type="hidden" value="-1" />
        
        [#-- Item index --]
        <div class="itemIndex">
          [@s.text name="planning.activityMaterials.material" /]
        </div>
        
        
		[#-- Type of Deliverable --]
		<div class="fullBlock">
        	 [@customForm.select name="type" i18nkey="planning.activityMaterials.type" required=true label="" required=true listName="materialTypes" keyFieldName="value"  displayFieldName="displayName"/]
        </div>
            
        [#-- Other Description --]
        <div class="fullBlock">
			[@customForm.textArea name="otherDescription" i18nkey="planning.activityMaterials.otherDescription"/]
        </div>
           
        [#-- Brief Description --]
        <div class="fullBlock">
        	[@customForm.textArea name="briefDescription" i18nkey="planning.activityMaterials.briefDescription" required=true /]
        </div>
            
        [#-- Brief Description --]
        <div class="fullBlock">
        	[@customForm.input name="targetAudience" i18nkey="planning.activityMaterials.targetAudience" required=true /]
        </div>
            
        [#-- Weblink --]
        <div class="fullBlock">
        	[@customForm.input name="webLink" i18nkey="planning.activityMaterials.webLink" required=true /]
        </div>
        <div class="clear-both"></div> 
       
        
        <hr />
      </div> <!-- End material template -->
    </div>
    <div class="clear-both">&nbsp;</div>
	<div class="addLink">
	  <img src="${baseUrl}/images/global/icon-add.png" />
	  <a class="iframe" href="/wle-ap/review-request?cid=${activity.id}&cemail=${currentUser.email}&ccategory=materials_review&cstatus=${activity.activityStatus}">
	    [@s.text name="planning.comments.addComment" /]
	  </a>
	</div>
      
    [#-- Only the owner of the activity can see the action buttons --]
    [#if activity.leader.id == currentUser.leader.id && canSubmit && activity.editable]
      <div class="buttons">
        [@s.submit type="button" name="save"][@s.text name="form.buttons.save" /][/@s.submit]
        [@s.submit type="button" name="next"][@s.text name="form.buttons.next" /][/@s.submit]
        [@s.submit type="button" name="cancel"][@s.text name="form.buttons.cancel" /][/@s.submit]
      </div>
    [/#if]
  </article>
  [/@s.form]  
</section>
[#include "/WEB-INF/global/pages/footer.ftl"]