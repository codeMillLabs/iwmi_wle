[#ftl]
[#--
 
 * This file is part of CCAFS Planning and Reporting Platform.
 *
 * CCAFS P&R is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * CCAFS P&R is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCAFS P&R. If not, see 
 * <http://www.gnu.org/licenses/>
  
--]

[#assign title = "Activity List (Planning)" /]
[#assign globalLibs = ["jquery", "dataTable", "noty"] /]
[#assign customJS = ["${baseUrl}/js/global/utils.js", "${baseUrl}/js/planning/activity-list.js",  "${baseUrl}/js/wz_tooltip.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox.js" ,  "${baseUrl}/js/colorbox/jquery.colorbox-min.js"] /]
[#assign customCSS = ["${baseUrl}/css/libs/dataTables/jquery.dataTables-1.9.4.css", "${baseUrl}/css/global/customDataTable.css", "${baseUrl}/css/colorbox/colorbox.css"] /]
[#assign currentSection = "planning" /]


[#include "/WEB-INF/global/pages/header.ftl" /]
[#include "/WEB-INF/global/pages/main-menu.ftl" /]
[#import "/WEB-INF/global/macros/utils.ftl" as utilities/]
[#import "/WEB-INF/planning/activitiesList.ftl" as activityList/]
[#import "/WEB-INF/global/macros/forms.ftl" as customForm/]
    
<section id="activityListPlanning" class="content">
  <div class="helpMessage">
    <img src="${baseUrl}/images/global/icon-help.png" />
    <p> [@s.text name="planning.introduction.part1" /] </p>
    <p> [@s.text name="planning.introduction.part2" /] </p>
  </div>
  
  <article class="fullContent">
    <h1>[#if currentUser.leader??]${currentUser.leader.name}[/#if] [@s.text name="planning.activityList.activities" /] </h1>   
    
    <div class="export-bar">
    	<a class='inline' href="#export-activities">Export Budgets</a>&nbsp;|&nbsp;
    	<a class='inline' href="#export-summary">Export Budget Summary</a>
    	[#if currentUser.isAdmin()]
    	&nbsp;|&nbsp;
    	 <a class='inline' href="#print-activities">Print Activities</a>
    	&nbsp;|&nbsp;
    	 <a class='inline' href="#print-all-activities">Print All Activity Summaries</a>
    	[/#if]&nbsp;|&nbsp;
    	<a class='inline-nor' href="../preport">Partner Summary</a>
    </div>
    <br/>
    <div class="clear-both"></div> 
    <div style='display:none'>
    		
			<div id='export-activities' style='padding:10px; background:#fff;'>
			<form action="../csv">
    			 <div><b>Year : </b><select name="year" class="popup-selection">
    			 	[#list yearSelection as year]
    			 		<option value="${year}">${year}</option>
              		[/#list]
				 </select>
				 <!--<input type="text" name="activityId" value="101">-->
				 <input type="submit" value="Export" class="export-buttons">
				  </div>
    		</form>
    		</div>
    		
    
			
			<div id='export-summary'>
			<div class="query-topic">Query by Center</div>
			<form action="../csv">
    		<div>
    		<h6>Year :</h6><br/> <select name="year">
    			 	[#list yearSelection as year]
    			 		<option value="${year}">${year}</option>
              		[/#list]
				 </select>
			</div>
			<div>
    		<h6>Lead Center :</h6><br/> <select name="leadCenter" class="leadCenter">
    			 	[#list leadCentersLookup as leadCenter]
    			 		<option value="${leadCenter.value}">${leadCenter.displayName}</option>
              		[/#list]
				 </select>
			</div>
			<input type="submit" value="Export" class="export-buttons">
    		 </form>
    		 <br/><hr/><br/>
			<div class="query-topic">Query by Cluster and Flagship</div>
    		<form action="../csv">
    		<div>
    		<h6>Year :</h6><br/> <select name="year">
    			 	[#list yearSelection as year]
    			 		<option value="${year}">${year}</option>
              		[/#list]
				 </select>
			</div>
			<div>
    		<h6>Flagship :</h6><br/> <select name="flagShipSRP" class="flagShipSRP">
    			 	[#list flagshipLookup as flagship]
    			 		<option value="${flagship.value}">${flagship.displayName}</option>
              		[/#list]
				 </select>
			</div>
			<div>
    		<h6>Activity Cluster :</h6><br/> <select name="activityCluster" class="activityCluster">
    			 	[#list activityClusterLookup as activityCluster]
    			 		<option value="${activityCluster.value}">${activityCluster.displayName}</option>
              		[/#list]
				 </select>
			</div>
			 <input type="submit" value="Export" class="export-buttons">
    		 </form>
			</div>
			
			<div id='print-activities' style='padding:10px; background:#fff;'>
			<form action="../report" target="_blank">
    			 <div><b>Year : </b><select name="year" class="popup-selection">
    			 	[#list yearSelection as year]
    			 		<option value="${year}">${year}</option>
              		[/#list]
				 </select>
				 <!--<input type="text" name="activityId" value="101">-->
				 <input type="submit" value="Print" class="export-buttons">
				  </div>
    		</form>
    		</div>
    		
    		<div id='print-all-activities' style='padding:10px; background:#fff;'>
			<form action="../actSum" target="_blank">
    			 <div><b>Year : </b><select name="year" class="popup-selection">
    			 	[#list yearSelection as year]
    			 		<option value="${year}">${year}</option>
              		[/#list]
				 </select>
				 <!--<input type="text" name="activityId" value="101">-->
				 <input type="submit" value="Print" class="export-buttons">
				  </div>
    		</form>
    		</div>
	</div>
    
    <div id="activityTables">
      <ul>
        [#-- Current Activities --]
        [#if currentUser.TL]
          <li><a href="#activityTables-1"> [@s.text name="planning.activityList.themeActivities" /] </a></li>
        [#elseif currentUser.RPL ]
          <li><a href="#activityTables-1"> [@s.text name="planning.activityList.regionActivities" /] </a></li>
        [#else]
          <li><a href="#activityTables-1"> [@s.text name="planning.activityList.activities" /] ${currentYear?c} </a></li>
        [/#if]
        [#-- Future activities --]
        [#if futurePlanningActive]
          <li><a href="#activityTables-2"> [@s.text name="planning.activityList.futureActivities" /] </a></li>
        [/#if]
        [#-- Previous activities --]
        <li><a href="#activityTables-3"> [@s.text name="planning.activityList.previousActivities" /] </a></li>
       
        [#-- Related activities --]
        [#--
        [#if currentUser.TL]
          <li><a href="#activityTables-4"> [@s.text name="planning.activityList.themeLedActivities" ] [@s.param] ${currentUser.leader.theme.code} [/@s.param] [/@s.text] </a></li>
        [#elseif currentUser.RPL ]
          <li><a href="#activityTables-4"> [@s.text name="planning.activityList.regionLedActivities" ] [@s.param] ${currentUser.leader.region.name} [/@s.param] [/@s.text] </a></li>
        [/#if]
        --]
        
        [#-- Pending Review Activities --]
          <li><a href="#activityTables-6"> [@s.text name="planning.activityList.pendingReviews" /] </a></li>
        [#-- Revision history --]
        [#if revisionHistoryActive]
          <li><a href="#activityTables-5"> [@s.text name="planning.activityList.revisionHistory" /] </a></li>
        [/#if]
       
      </ul>

      [#-- Current activities --]      
      <div id="activityTables-1" class="activityTable">
        
        [#if currentActivities?has_content]
          [@activityList.activitiesList activities=currentActivities canValidate=true canEditActivity=true tableID="currentActivities" /]
          
          [#-- If the workplan hasn't been submitted yet, show the button --]
          [#if !workplanSubmitted]
            [#if !currentUser.PI ]
              <div id="submitButtonBlock" class="buttons">
               [#-- 
                [#if canSubmit]
                  [@s.form action="activities" id="submitForm" ]
                    [@s.submit type="button" name="save" method="submit" cssClass="test" ][@s.text name="form.buttons.submit" /][/@s.submit] 
                  [/@s.form]  
                [#else]
                  <button id="submitForm_save" class="disabled" title="[@s.text name="submit.disabled" /]"> [@s.text name="form.buttons.submit" /] </button>
                [/#if]
                --]
              </div>
            [/#if]
          [#else]
            <div id="submitButtonBlock" class="buttons">
              <img src="${baseUrl}/images/global/icon-complete.png" /> [@s.text name="submit.submitted" /]
            </div>
          [/#if]
        
        [#else]
          <div class="noActivities">
            [@s.text name="planning.activityList.empty" /]
          </div>
        [/#if]
        
        [#-- Show the Add activity button if the workplan hasn't been submitted yet--]
        [#if !currentUser.PI]
          <span id="addActivity">
            <a href=" [@s.url action='addActivity' includeParams='get'] [@s.param name='${activityYearRequest}']${currentYear?c}[/@s.param] [/@s.url]" >
             [@s.text name="planning.activityList.addActivity" /]
            </a>
          </span>
        [/#if]
      </div>
  
      [#-- Future activities --]
      [#if futurePlanningActive]
        <div id="activityTables-2" class="activityTable">
          <div id="futureActivities">
            <ul>
              [#list futureActivities?keys as year]
                <li><a href="#futureActivities-${year_index+1}"> ${year?c} </a></li>
              [/#list]
            </ul>
            [#list futureActivities?keys as year]
              [#if futureActivities.get(year)?has_content]
                <div id="futureActivities-${year_index+1}">
                  [#assign listOfActivities = futureActivities.get(year)]
                  [@activityList.activitiesList activities=listOfActivities canValidate=false canEditActivity=true tableID="futureActivities" /]                
                  
                  [#-- Add activity button --]
                  [#if !currentUser.PI]
                    <div id="addActivity">
                      <a href=" [@s.url action='addActivity' includeParams='get'] [@s.param name='${activityYearRequest}']${year?c}[/@s.param] [/@s.url]" >
                        [@s.text name="planning.activityList.addActivity" /]
                      </a>
                    </div>
                  [/#if]
                </div>
              [#else]
                <div id="futureActivities-${year_index+1}">
                  <div class="noActivities">
                    [@s.text name="planning.activityList.empty" /]
                  </div>
                  
                  [#-- Add activity button --]
                  [#if !currentUser.PI]
                    <div id="addActivity">
                      <a href=" [@s.url action='addActivity' includeParams='get'] [@s.param name='${activityYearRequest}']${year?c}[/@s.param] [/@s.url]" >
                        [@s.text name="planning.activityList.addActivity" /]
                      </a>
                    </div>
                  [/#if]
                </div>
              [/#if]
            [/#list]
          </div>
        </div>
      [/#if]
      
      
      [#-- Previous activities --]
        <div id="activityTables-3" class="activityTable">
          <div id="previousActivities">
            <ul>
              [#list previousActivities?keys as year]
                <li><a href="#previousActivities-${year_index+1}"> ${year?c} </a></li>
              [/#list]
            </ul>
            [#list previousActivities?keys as year]
              [#if previousActivities.get(year)?has_content]
                <div id="previousActivities-${year_index+1}">
                  [#assign listOfActivities = previousActivities.get(year)]
                  [@activityList.activitiesList activities=listOfActivities canValidate=true canEditActivity=true tableID="futureActivities" /]                
                  
                  [#-- Add activity button --]
                  [#if !currentUser.PI]
                    <div id="addActivity">
                      <a href=" [@s.url action='addActivity' includeParams='get'] [@s.param name='${activityYearRequest}']${year?c}[/@s.param] [/@s.url]" >
                        [@s.text name="planning.activityList.addActivity" /]
                      </a>
                    </div>
                  [/#if]
                </div>
              [#else]
                <div id="previousActivities-${year_index+1}">
                  <div class="noActivities">
                    [@s.text name="planning.activityList.empty" /]
                  </div>
                  
                  [#-- Add activity button --]
                  [#if !currentUser.PI]
                    <div id="addActivity">
                      <a href=" [@s.url action='addActivity' includeParams='get'] [@s.param name='${activityYearRequest}']${year?c}[/@s.param] [/@s.url]" >
                        [@s.text name="planning.activityList.addActivity" /]
                      </a>
                    </div>
                  [/#if]
                </div>
              [/#if]
            [/#list]
          </div>
        </div>
          
      [#-- Related activities --]      
      [#if currentUser.TL || currentUser.RPL ]
        <div id="activityTables-4" class="activityTable">
          [#if relatedActivities?has_content]
            [@activityList.activitiesList activities=relatedActivities canValidate=false canEditActivity=true owned=false /]
          [#else]
            <div class="noActivities">
              [@s.text name="planning.activityList.empty" /]
            </div>
          [/#if]
        </div>
      [/#if]
      
      [#-- Pending Reviews activities --]      
      <div id="activityTables-6" class="activityTable">
          <div id="pendingReviewsActivities">
        [#if pendingReviews?has_content]
          [@activityList.activitiesList activities=pendingReviews canValidate=false canEditActivity=true tableID="pendingReviewsActivities" /]        
        [#else]
          <div class="noActivities">
            [@s.text name="planning.activityList.pending.review.empty" /]
          </div>
        [/#if]
         </div>
      </div>
      
      [#-- Revision history --]
      [#if revisionHistoryActive]
        <div id="activityTables-5" class="activityTable">
          <div id="revisionHistory">
            <ul>
              [#list revisionHistory?keys as year]
                <li><a href="#revisionHistory-${year_index+1}"> ${year?c} </a></li>
              [/#list]
            </ul>
            [#list revisionHistory?keys as year]
              [#if revisionHistory.get(year)?has_content]
                <div id="revisionHistory-${year_index+1}">
                  [#assign listOfActivities = revisionHistory.get(year)]
                  [@activityList.activitiesList activities=listOfActivities canValidate=false canEditActivity=true tableID="revisionHistory" /]                
                </div>
              [#else]
                <div id="revisionHistory-${year_index+1}">
                  <div class="noActivities">
                    [@s.text name="planning.activityList.empty" /]
                  </div>
                </div>
              [/#if]
              [#if year == currentYear]
              	    <input type="hidden" id="currentYearKey" value="${year_index}" />
              [/#if]
            [/#list]
          </div>
        </div>
      [/#if]
      
      
    </div>
    
    <input type="hidden" id="beforeSubmitMessage" value="[@s.text name="delete.beforeDelete.message" /]" />
    
    <div class="clearfix"></div>
  </article>
  </section>
[#include "/WEB-INF/global/pages/footer.ftl"]