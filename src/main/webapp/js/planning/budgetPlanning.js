$(document).ready(
		function() {
			renameObjectives();

			calculateDonorBudgetTotals();
			calculateMainRegionBudgetTotals();

			$(".region-related-table input[type='text']").keydown(
					function(e) {
						// Allow: backspace, delete, tab, escape, enter and .
						if ($
								.inArray(e.keyCode, [ 46, 8, 9, 27, 13, 110,
										190 ]) !== -1
								||
								// Allow: Ctrl+A
								(e.keyCode == 65 && e.ctrlKey === true) ||
								// Allow: home, end, left, right
								(e.keyCode >= 35 && e.keyCode <= 39)) {
							// let it happen, don't do anything
							return;
						}
						// Ensure that it is a number and stop the keypress
						if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
								&& (e.keyCode < 96 || e.keyCode > 105)) {
							e.preventDefault();
						}
					});
			
			$(".donor-budget-table .user-data input[type='text']").keydown(
					function(e) {
						// Allow: backspace, delete, tab, escape, enter and .
						if ($
								.inArray(e.keyCode, [ 46, 8, 9, 27, 13, 110,
										190 ]) !== -1
								||
								// Allow: Ctrl+A
								(e.keyCode == 65 && e.ctrlKey === true) ||
								// Allow: home, end, left, right
								(e.keyCode >= 35 && e.keyCode <= 39)) {
							// let it happen, don't do anything
							return;
						}
						// Ensure that it is a number and stop the keypress
						if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
								&& (e.keyCode < 96 || e.keyCode > 105)) {
							e.preventDefault();
						}
					});

			 $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});
		});

function renameObjectives() {
}

//$(".input input").mouseout(calculateTotals);


$(".donor-budget-table input").mouseout(calculateDonorBudgetTotals);

function calculateDonorBudgetTotals() {

	var w3Total = 0;
	var bilateralTotal = 0;
	var otherTotal = 0;
	$(".donor-budget-table .donor-budget-row").each(
			function(index) {
				var w3 = $('.donor_budget_' + index + '_w3').val();
				var bilateral = $('.donor_budget_' + index + '_bilateral')
						.val();
				var other = $('.donor_budget_' + index + '_other').val();
				var total = checkNaN(parseFloat(w3)) + checkNaN(parseFloat(bilateral))
						+ checkNaN(parseFloat(other));
				$('.donor_budget_total_' + index).val(checkNaN(total));
				w3Total = checkNaN(parseFloat(w3Total)) + checkNaN(parseFloat(w3));
				bilateralTotal = checkNaN(parseFloat(bilateralTotal))
						+ checkNaN(parseFloat(bilateral));
				otherTotal = checkNaN(parseFloat(otherTotal)) + checkNaN(parseFloat(other));

			});
	
	$('.donor_budget_total_w3').val(checkNaN(w3Total));
	$('.donor_budget_total_bilateral').val(checkNaN(bilateralTotal));
	$('.donor_budget_total_other').val(checkNaN(otherTotal));
	
	
	var grandTotal = checkNaN(parseFloat(w3Total)) + checkNaN(parseFloat(bilateralTotal)) + checkNaN(parseFloat(otherTotal));
	$('.donor_budget_grand_total').val(checkNaN(grandTotal));
	
}

$(".region-related-table input").mouseout(calculateMainRegionBudgetTotals);

function calculateMainRegionBudgetTotals() {
	
	var sourcew1w2 = 0;
	var sourcew3 = 0;
	var sourcebilateral = 0;
	var sourceother = 0;
	var sourcetotal = 0;
	
	var budgetw1w2 = 0;
	var budgetw3 = 0;
	var budgetbilateral = 0;
	var budgetother = 0;
	var budgettotal = 0;
	
	var grandw1w2 = 0;
	var grandw3 = 0;
	var grandbilateral = 0;
	var grandother = 0;
	var grandtotal = 0;
	
	$(".region-related-table .main-budget-budget-row").each(
			function(index) {
				var w1w2 = $('.mainBudgets_' + index + '_0_w1w2').val();
				var w3 = $('.mainBudgets_' + index + '_0_w3').val();
				var bilateral = $('.mainBudgets_' + index + '_0_bilateral')
						.val();
				var other = $('.mainBudgets_' + index + '_0_other').val();

				var total = checkNaN(parseFloat(w1w2)) + checkNaN(parseFloat(w3))
						+ checkNaN(parseFloat(bilateral)) + checkNaN(parseFloat(other));
				$('.mainBudgets_' + index + '_0_total').val(checkNaN(total));
				
				sourcew1w2 = parseFloat(sourcew1w2) + parseFloat(w1w2);
				sourcew3 = parseFloat(sourcew3) + parseFloat(w3);
				sourcebilateral = parseFloat(sourcebilateral) + parseFloat(bilateral);
				sourceother = parseFloat(sourceother) + parseFloat(other);
				sourcetotal = parseFloat(sourcetotal) + parseFloat(total);
				
				var bpw1w2 = $('.gender_' + index + '_1_w1w2').val();
				var bpw3 = $('.gender_' + index + '_1_w3').val();
				var bpbilateral = $('.gender_' + index + '_1_bilateral')
						.val();
				var bpother = $('.gender_' + index + '_1_other').val();

				
				
				var bw1w2 = parseFloat(w1w2) / 100 * parseFloat(bpw1w2);
				var bw3 = parseFloat(w3) / 100 * parseFloat(bpw3);
				var bbilateral = parseFloat(bilateral) / 100 * parseFloat(bpbilateral);
				var bother = parseFloat(other) / 100 * parseFloat(bpother);

				var btotal = checkNaN(parseFloat(bw1w2)) + checkNaN(parseFloat(bw3))
						+ checkNaN(parseFloat(bbilateral)) + checkNaN(parseFloat(bother));
				
				$('.geneder_budget_' + index + '_2_w1w2').val(checkNaN(bw1w2.toFixed(2)));
				$('.geneder_budget_' + index + '_2_w3').val(checkNaN(bw3.toFixed(2)));
				$('.geneder_budget_' + index + '_2_bilateral').val(checkNaN(bbilateral.toFixed(2)));
				$('.geneder_budget_' + index + '_2_other').val(checkNaN(bother.toFixed(2)));
				$('.geneder_budget_' + index + '_2_total').val(checkNaN(btotal.toFixed(2)));
				
				var bptotal = parseFloat(btotal) / parseFloat(total) * 100;				
				$('.gender_' + index + '_1_total').val(checkNaN(bptotal.toFixed(2)));
				
				$('.gender_' + index + '_2_total').val(checkNaN(btotal.toFixed(2)));
				
				grandw1w2 = checkNaN(parseFloat(grandw1w2)) + checkNaN(parseFloat(bw1w2));
				grandw3 = checkNaN(parseFloat(grandw3)) + checkNaN(parseFloat(bw3));
				grandbilateral = checkNaN(parseFloat(grandbilateral)) + checkNaN(parseFloat(bbilateral));
				grandother = checkNaN(parseFloat(grandother)) + checkNaN(parseFloat(bother));
				grandtotal = checkNaN(parseFloat(grandtotal)) + checkNaN(parseFloat(btotal));
				
				$('.reg_budget_' + index + '_0_main').val(checkNaN(w1w2));
				$('.reg_budget_' + index + '_1_main').val(checkNaN(w3));
				$('.reg_budget_' + index + '_2_main').val(checkNaN(bilateral));
				$('.reg_budget_' + index + '_3_main').val(checkNaN(other));
				$('.reg_budget_' + index + '_4_main').val(checkNaN(total));
				
				var personnelw1w2 = $('.reg_budget_'+index+'_0_personnel').val();
				var personnelw3 = $('.reg_budget_'+index+'_1_personnel').val();
				var personnelbiateral = $('.reg_budget_'+index+'_2_personnel').val();
				var personnelother = $('.reg_budget_'+index+'_3_personnel').val();
				
				var operationsw1w2 = $('.reg_budget_'+index+'_0_operations').val();
				var operationsw3 = $('.reg_budget_'+index+'_1_operations').val();
				var operationsbiateral = $('.reg_budget_'+index+'_2_operations').val();
				var operationsother = $('.reg_budget_'+index+'_3_operations').val();
				
				var inderectcostsw1w2 = $('.reg_budget_'+index+'_0_indirectCosts').val();
				var inderectcostsw3 = $('.reg_budget_'+index+'_1_indirectCosts').val();
				var inderectcostsbiateral = $('.reg_budget_'+index+'_2_indirectCosts').val();
				var inderectcostsother = $('.reg_budget_'+index+'_3_indirectCosts').val();
				
				var personaltotal = checkNaN(parseFloat(personnelw1w2)) + checkNaN(parseFloat(personnelw3)) + checkNaN(parseFloat(personnelbiateral)) + checkNaN(parseFloat(personnelother)); 
				var operationstotal = checkNaN(parseFloat(operationsw1w2)) + checkNaN(parseFloat(operationsw3)) + checkNaN(parseFloat(operationsbiateral)) + checkNaN(parseFloat(operationsother));
				var inderectcoststotal = checkNaN(parseFloat(inderectcostsw1w2)) + checkNaN(parseFloat(inderectcostsw3)) + checkNaN(parseFloat(inderectcostsbiateral)) + checkNaN(parseFloat(inderectcostsother));
				
				var regw1w2total = checkNaN(parseFloat(personnelw1w2)) + checkNaN(parseFloat(operationsw1w2)) + checkNaN(parseFloat(inderectcostsw1w2));
				var regw3total = checkNaN(parseFloat(personnelw3)) + checkNaN(parseFloat(operationsw3)) + checkNaN(parseFloat(inderectcostsw3));
				var regbilateraltotal = checkNaN(parseFloat(personnelbiateral)) + checkNaN(parseFloat(operationsbiateral)) + checkNaN(parseFloat(inderectcostsbiateral));
				var reothertotal = checkNaN(parseFloat(personnelother)) + checkNaN(parseFloat(operationsother)) + checkNaN(parseFloat(inderectcostsother));
				var reggrandtotal = checkNaN(parseFloat(regw1w2total)) + checkNaN(parseFloat(regw3total)) + checkNaN(parseFloat(regbilateraltotal)) + checkNaN(parseFloat(reothertotal));
				
				$('.reg_budget_' + index + '_4_personal_total').val(checkNaN(personaltotal));
				$('.reg_budget_' + index + '_4_operations_total').val(checkNaN(operationstotal));
				$('.reg_budget_' + index + '_4_indirect_costs_total').val(checkNaN(inderectcoststotal));
				
				
				$('.reg_budget_' + index + '_0_row_total').val(checkNaN(regw1w2total));
				$('.reg_budget_' + index + '_1_row_total').val(checkNaN(regw3total));
				$('.reg_budget_' + index + '_2_row_total').val(checkNaN(regbilateraltotal));
				$('.reg_budget_' + index + '_3_row_total').val(checkNaN(reothertotal));
				$('.reg_budget_' + index + '_4_row_total').val(checkNaN(reggrandtotal));
				
			});
	
	$('.source-total-w1w2').val(checkNaN(sourcew1w2));
	$('.source-total-w3').val(checkNaN(sourcew3));
	$('.source-total-bilateral').val(checkNaN(sourcebilateral));
	$('.source-total-other').val(checkNaN(sourceother));
	$('.source-total').val(checkNaN(sourcetotal));
	
	budgetw1w2 =  parseFloat(checkNaN(grandw1w2)) / parseFloat(checkNaN(sourcew1w2)) * 100;
	budgetw3 = parseFloat(checkNaN(grandw3)) / parseFloat(checkNaN(sourcew3)) * 100;
	budgetbilateral = parseFloat(checkNaN(grandbilateral)) / parseFloat(checkNaN(sourcebilateral)) * 100;
	budgetother = parseFloat(checkNaN(grandother)) / parseFloat(checkNaN(sourceother)) * 100;
	
	$('.gender-per-total-w1w2').val(checkNaN(budgetw1w2.toFixed(2)));
	$('.gender-per-total-w3').val(checkNaN(budgetw3.toFixed(2)));
	$('.gender-per-total-bilateral').val(checkNaN(budgetbilateral.toFixed(2)));
	$('.gender-per-total-other').val(checkNaN(budgetother.toFixed(2)));
	budgettotal =  parseFloat(grandtotal) /  parseFloat(sourcetotal) * 100;
	$('.gender-per-total').val(checkNaN(budgettotal.toFixed(2)));
	
	$('.grand-total-w1w2').val(checkNaN(grandw1w2.toFixed(2)));
	$('.grand-total-w3').val(checkNaN(grandw3.toFixed(2)));
	$('.grand-total-bilateral').val(checkNaN(grandbilateral.toFixed(2)));
	$('.grand-total-other').val(checkNaN(grandother.toFixed(2)));
	$('.grand-total').val(checkNaN(grandtotal.toFixed(2)));
}

function checkNaN(value)
{
	return isNaN(value) ? 0 : value;
}

$("#budget_save").click(function(e) {
	var budgetTotal = $('.budgetTotal').val();
	var regionsTotal = $('.regionsTotal').val();
	if (regionsTotal != budgetTotal) {
		alert('Budget total and regions total should be equal to save');
		e.preventDefault();
	} 

});

$("#budget_next").click(function(e) {
	var budgetTotal = $('.budgetTotal').val();
	var regionsTotal = $('.regionsTotal').val();
	if (regionsTotal != budgetTotal) {
		alert('Budget total and regions total should be equal to save');
		e.preventDefault();
	} 
});