$(document).ready(function() {
  // Test
  $("#activityTables").tabs({ show: { effect: "slide", direction: "up", duration: 500 }});
  
  $( "#previousActivities" )
    .tabs( )
    .addClass('ui-tabs-vertical ui-helper-clearfix');

  $( "#futureActivities" )
    .tabs(  )
    .addClass('ui-tabs-vertical ui-helper-clearfix');
  
  $( "#revisionHistory" )
  .tabs(  )
  .addClass('ui-tabs-vertical ui-helper-clearfix');
  
  $(".inline").colorbox({inline:true, width:"50%"});
  
  $('table.activityList').dataTable({
    "bPaginate" : true, // This option enable the table pagination
    "bLengthChange" : false, // This option disables the select table size option
    "bFilter" : true, // This option enable the search
    "bSort" : true, // this option enable the sort of contents by columns
    "bAutoWidth" : true, // This option enables the auto adjust columns width
    "iDisplayLength" : 15, // Number of rows to show on the table
    "fnDrawCallback": function(){
      // This function locates the add activity button at left to the filter box
      var table = $(this).parent().find("table");
      if( $(table).attr("id") == "currentActivities"){
        $("#currentActivities_filter").prepend($("#addActivity"));
      }
    }
  });
  
  $("#submitForm").on("submit", function(evt){
    if(confirm($("#beforeSubmitMessage").val())){
      
      return true;
    }else{
      evt.preventDefault();
      return false;
    }
  });
  
  $('#deleteActivityLink').on('click', function () {
      return confirm($("#beforeDeleteMessage").val());
  });
  
  $('.flagShipSRP').change(loadActivityClusters);
  
  var selectedFlashship = $("#selectedFlagship").val();
  $(".flagShipSRP").val(selectedFlashship);  
  loadActivityClusters();
 
  $('#revisionHistory').tabs("option", "active",  $('#currentYearKey').val());
});

function loadActivityClusters() {
	
	var selectedValue = $(".flagShipSRP").val();
	var activityClusterByFilter = '../json/activityClusersByFilter.do?flagship=' + selectedValue;
	 // Return if the element is the template
	  if(selectedValue == '-1' || selectedValue == ''){
	    return;
	  }

	  $.getJSON(activityClusterByFilter, function(data) {
	    var optionsHtml = "";
	    $(".activityCluster option").remove(); 
	    var selectedVal = $("#selectedActivityCluster").val();
	    
	    if(data.activityClusters && data.activityClusters.length <= 0) {
	      optionsHtml += "<option value='-1'>";
		  optionsHtml += $("#noResultByFilterText").val();
		  optionsHtml += "</option>";

		  $(".activityCluster").append(optionsHtml);
	    } else {
	    	  optionsHtml += "<option value=''>";
			  optionsHtml += "";
			  optionsHtml += "</option>";
	    	 var clusters = data.activityClusters;
		      for ( var c = 0; c < clusters.length; c++) {
		        optionsHtml += "<option value='" + clusters[c].value + "' >";
		        optionsHtml += clusters[c].displayName;
		        optionsHtml += "</option>";
		      }
		      $(".activityCluster").append(optionsHtml);   
		      $(".activityCluster").val(selectedVal);   
		      
		      $(".activityCluster").trigger("liszt:updated");
	    }
	  });
	
}
