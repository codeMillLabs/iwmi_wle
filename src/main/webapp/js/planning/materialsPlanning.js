$(document).ready(
    function() {
      $(".addMaterials").click(function(event) {
        event.preventDefault();
        // Cloning tempalte.
        var $newMaterials = $("#material-9999").clone(true);
        $("#addMaterialsBlock").before($newMaterials);
        renameMaterials();
        $newMaterials.fadeIn("slow");
        $newMaterials.find("[id$='type']").focus();
      });

      $('.removeMaterials').click(function(event) {
        event.preventDefault();
        // Getting the id.
        var removeId = $(event.target).attr("id").split("-")[1];
        $("#material-" + removeId).fadeOut("slow");
        // removing division line.
        $("#material-" + removeId).next("hr").hide("slow", function() {
          $(this).remove();
        });
        // removing div.
        $("#material-" + removeId).hide("slow", function() {
          $(this).remove();
          renameMaterials();
        });
      });
      
      $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});

    });

function renameMaterials() {
  //getting the text of the index element.
  var itemText = $("#materialTemplate").find(".itemIndex").text();
  $("#materialsBlock .material").each(
      function(index, material) {
        // Changing attributes of each component in order to match with the array order.
        // Main div.
        $(this).attr("id", "material-" + index);
        // Remove link.
        $(this).find("[id^='removeMaterials-']").attr("id",
            "removeMaterials-" + index);
        // Id
        $(this).find("[name$='id']").attr("name",
            "activity.materials[" + index + "].id");
        //Item index
        $(this).find(".itemIndex").text(itemText + " " + (index + 1));
        
        // Type of Deliverable.
        $(this).find("[id$='type']").attr("id",
            "activity.materials[" + index + "].type");
        $(this).find("[name$='type']").attr("name",
            "activity.materials[" + index + "].type");
        $(this).find("[for$='type']").attr("for",
            "activity.materials[" + index + "].type");
        
        // Other Description.
        $(this).find("[id$='otherDescription']").attr("id",
            "activity.materials[" + index + "].otherDescription");
        $(this).find("[name$='otherDescription']").attr("name",
            "activity.materials[" + index + "].otherDescription");
        $(this).find("[for$='otherDescription']").attr("for",
            "activity.materials[" + index + "].otherDescription");
        
        // Brief Description.
        $(this).find("[id$='briefDescription']").attr("id",
            "activity.materials[" + index + "].briefDescription");
        $(this).find("[name$='briefDescription']").attr("name",
            "activity.materials[" + index + "].briefDescription");
        $(this).find("[for$='briefDescription']").attr("for",
            "activity.materials[" + index + "].briefDescription");
        
        // Target Audience.
        $(this).find("[id$='targetAudience']").attr("id",
            "activity.materials[" + index + "].targetAudience");
        $(this).find("[name$='targetAudience']").attr("name",
            "activity.materials[" + index + "].targetAudience");
        $(this).find("[for$='targetAudience']").attr("for",
            "activity.materials[" + index + "].targetAudience");
        
        // Weblink.
        $(this).find("[id$='webLink']").attr("id",
            "activity.materials[" + index + "].webLink");
        $(this).find("[name$='webLink']").attr("name",
            "activity.materials[" + index + "].webLink");
        $(this).find("[for$='webLink']").attr("for",
            "activity.materials[" + index + "].webLink");
      });
}