$(document).ready(function() {

  $(".removeContactPerson").on("click", function(event) {
    event.preventDefault();
    $(event.target).parent().parent().hide("slow", function() {
      $(event.target).parent().parent().remove();
      renameContactPersons();
    });
  });

  $(".addContactPerson").on("click", function(event) {
    event.preventDefault();
    var newContactPerson = $("#contactPersonTemplate").clone(true);
    $("#contactPersonBlock").append(newContactPerson);
    $(newContactPerson).attr("class", "contactPerson");
    renameContactPersons();
    newContactPerson.show("slow");
  });
  
  // Set text 'No funds' in budget percentage list
  var nofundsMsg = $("#activity\\.budget\\.noFunds").val();
  $("#mainInformation_activity_budget_cgFund option").first().text(nofundsMsg);
  $("#mainInformation_activity_budget_bilateral option").first().text(nofundsMsg);
  
  // Format the budget field
  $("#activity\\.budget\\.usd").on("keypress", isNumber);

  // Add chosen plugin to the milestone select
  $(".milestones").chosen();
 // $(".wleSelect").attr('data-placeholder', $("#wleDefaultText").val());
  $(".wleSelect").chosen();
  
  /*
  $(".flagShipSRP").each(function(){
	    $(this).attr('data-placeholder', $("#selectedFlagship").val());
	    $(this).trigger('change');
	  });
 
 $(".flagShipSRP").chosen({allow_single_deselect:true, search_contains:true});
 $(".activityCluster").chosen({allow_single_deselect:true, search_contains:true});
  */
  $('.flagShipSRP').change(loadActivityClusters);
  
  var selectedFlashship = $("#selectedFlagship").val();
  $(".flagShipSRP").val(selectedFlashship);  
  loadActivityClusters();
  
  datePickerConfig();
  
  $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});
});

function renameContactPersons() {
  $(".contactPerson").each(
      function(index, contactPerson) {
        // Block id
        $(this).attr("id", "contactPerson-" + index);
        // Contact person id
        $(this).find("[name$='id']").attr("id",
            "activity.contactPersons[" + index + "].id");
        $(this).find("[name$='id']").attr("name",
            "activity.contactPersons[" + index + "].id");
        // Contact name
        $(this).find("[id$='name']").attr("id",
            "activity.contactPersons[" + index + "].name");
        $(this).find("[name$='name']").attr("name",
            "activity.contactPersons[" + index + "].name");
        // Contact email
        $(this).find("[id$='email']").attr("id",
            "activity.contactPersons[" + index + "].email");
        $(this).find("[name$='email']").attr("name",
            "activity.contactPersons[" + index + "].email");
      });
}

/**
 * Attach to the date fields the datepicker plugin
 * 
 */
function datePickerConfig() {
  var defaultMinDateValue = $("#minDateValue").val();
  var defaultMaxDateValue = $("#maxDateValue").val();
  var minDateValue = defaultMinDateValue;
  var maxDateValue = defaultMaxDateValue;

  // Start date calendar
  maxDateValue = $("#activity\\.endDate").val();
  
  // Add readonly attribute to prevent inappropriate user input
  $("#activity\\.startDate").attr('readonly', true);
  var finalMaxDate = (maxDateValue != 0) ? maxDateValue : defaultMaxDateValue; 
  $("#activity\\.startDate").datepicker({
    dateFormat : "yy-mm-dd",
    minDate : defaultMinDateValue,
    maxDate : finalMaxDate,
    changeMonth : true,
    changeYear : true,
    defaultDate : null,
    onClose : function(selectedDate) {
      if(selectedDate != ""){
        $("#activity\\.endDate").datepicker("option", "minDate", selectedDate);
      }
    }
  });

  // End date calendar
  minDateValue = $("#activity\\.startDate").val();
  
  // Add readonly attribute to prevent inappropriate user input
  $("#activity\\.endDate").attr('readonly', true);
  var finalMinDate = (minDateValue != 0) ? minDateValue : defaultMinDateValue;
  $("#activity\\.endDate").datepicker({
    dateFormat : "yy-mm-dd",
    minDate : finalMinDate,
    maxDate : defaultMaxDateValue,
    changeMonth : true,
    changeYear : true,
    defaultDate : null,
    onClose : function(selectedDate) {
      if(selectedDate != ""){
        $("#activity\\.startDate").datepicker("option", "maxDate", selectedDate);
      }
    }
  });
}


function loadActivityClusters() {
	
	var selectedValue = $(".flagShipSRP").val();
	var activityClusterByFilter = '../json/activityClusersByFilter.do?flagship=' + selectedValue;
	 // Return if the element is the template
	  if(selectedValue == '-1'){
	    return;
	  }

	  $.getJSON(activityClusterByFilter, function(data) {
	    var optionsHtml = "";
	    $(".activityCluster option").remove(); 
	    var selectedVal = $("#selectedActivityCluster").val();
	    
	    if(data.activityClusters && data.activityClusters.length <= 0) {
	      optionsHtml += "<option value='-1'>";
		  optionsHtml += $("#noResultByFilterText").val();
		  optionsHtml += "</option>";

		  $(".activityCluster").append(optionsHtml);
	    } else {
	    	
	    	 var clusters = data.activityClusters;
		      for ( var c = 0; c < clusters.length; c++) {
		        optionsHtml += "<option value='" + clusters[c].value + "' >";
		        optionsHtml += clusters[c].displayName;
		        optionsHtml += "</option>";
		      }
		      $(".activityCluster").append(optionsHtml);   
		      $(".activityCluster").val(selectedVal);   
		      
		      $(".activityCluster").trigger("liszt:updated");
	    }
	  });
	
}
