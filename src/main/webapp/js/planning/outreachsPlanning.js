$(document).ready(
    function() {
      $(".addOutreachs").click(function(event) {
        event.preventDefault();
        // Cloning tempalte.
        var $newOutreachs = $("#outreach-9999").clone(true);
        $("#addOutreachsBlock").before($newOutreachs);
        renameOutreachs();
        $newOutreachs.fadeIn("slow");
        $newOutreachs.find("[id$='type']").focus();
      });

      $('.removeOutreachs').click(function(event) {
        event.preventDefault();
        // Getting the id.
        var removeId = $(event.target).attr("id").split("-")[1];
        $("#outreach-" + removeId).fadeOut("slow");
        // removing division line.
        $("#outreach-" + removeId).next("hr").hide("slow", function() {
          $(this).remove();
        });
        // removing div.
        $("#outreach-" + removeId).hide("slow", function() {
          $(this).remove();
          renameOutreachs();
        });
      });
      
      $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});

    });

function renameOutreachs() {
  //getting the text of the index element.
  var itemText = $("#outreachTemplate").find(".itemIndex").text();
  $("#outreachsBlock .outreach").each(
      function(index, outreach) {
        // Changing attributes of each component in order to match with the array order.
        // Main div.
        $(this).attr("id", "outreach-" + index);
        // Remove link.
        $(this).find("[id^='removeOutreachs-']").attr("id",
            "removeOutreachs-" + index);
        // Id
        $(this).find("[name$='id']").attr("name",
            "activity.outreachs[" + index + "].id");
        //Item index
        $(this).find(".itemIndex").text(itemText + " " + (index + 1));
        
        // Type of Deliverable.
        $(this).find("[id$='type']").attr("id",
            "activity.outreachs[" + index + "].type");
        $(this).find("[name$='type']").attr("name",
            "activity.outreachs[" + index + "].type");
        $(this).find("[for$='type']").attr("for",
            "activity.outreachs[" + index + "].type");
        
        // Groups.
        $(this).find("[id$='groups']").attr("id",
            "activity.outreachs[" + index + "].groups");
        $(this).find("[name$='groups']").attr("name",
            "activity.outreachs[" + index + "].groups");
        $(this).find("[for$='groups']").attr("for",
            "activity.outreachs[" + index + "].groups");
        
        // Participants.
        $(this).find("[id$='paricipants']").attr("id",
            "activity.outreachs[" + index + "].paricipants");
        $(this).find("[name$='paricipants']").attr("name",
            "activity.outreachs[" + index + "].paricipants");
        $(this).find("[for$='paricipants']").attr("for",
            "activity.outreachs[" + index + "].paricipants");
        
        // Female Participants.
        $(this).find("[id$='femaleParticipants']").attr("id",
            "activity.outreachs[" + index + "].femaleParticipants");
        $(this).find("[name$='femaleParticipants']").attr("name",
            "activity.outreachs[" + index + "].femaleParticipants");
        $(this).find("[for$='femaleParticipants']").attr("for",
            "activity.outreachs[" + index + "].femaleParticipants");
        
        // Date & Location.
        $(this).find("[id$='dateAndLocation']").attr("id",
            "activity.outreachs[" + index + "].dateAndLocation");
        $(this).find("[name$='dateAndLocation']").attr("name",
            "activity.outreachs[" + index + "].dateAndLocation");
        $(this).find("[for$='dateAndLocation']").attr("for",
            "activity.outreachs[" + index + "].dateAndLocation");
        
        // Comments.
        $(this).find("[id$='comments']").attr("id",
            "activity.outreachs[" + index + "].comments");
        $(this).find("[name$='comments']").attr("name",
            "activity.outreachs[" + index + "].comments");
        $(this).find("[for$='comments']").attr("for",
            "activity.outreachs[" + index + "].comments");
      });
}