$(document).ready(function() {
  
  $(".removeObjective").on("click", function(event) {
    event.preventDefault();
    $(event.target).parent().parent().hide("slow", function() {
      $(event.target).parent().parent().remove();
      renameObjectives();
    });
  });

  $(".addObjective").on("click", function(event) {
    event.preventDefault();
    var newObjective = $("#objectiveTemplate").clone(true);
    $("#addDeliverableBlock").before(newObjective);
    $(newObjective).attr("class", "objective");
    renameObjectives();
    newObjective.show("slow");
    $(newObjective).find("[id$='description']").focus();
  });
  
  renameObjectives();

  $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});
});

function renameObjectives() {
  var itemText = $("#objectiveTemplate").find(".itemIndex").text();
  $(".objective").each( function(index, objective) {
    // Block id
    $(this).attr("id", "objective-" + index);
    // remove link
    $(this).find(".removeObjective").attr("id",
        "removeObjective-" + index);
    
    $(this).find(".itemIndex").text(itemText + " " + (index + 1));
    
   // description
    $(this).find("[name$='id']").attr("name",
        "activity.objectives[" + index + "].id");
    
    $(this).find("[name$='description']").attr("id",
        "activity.objectives[" + index + "].description");
    $(this).find("[name$='description']").attr("name",
        "activity.objectives[" + index + "].description");
    
    $(this).find("[name$='outcomeType']").attr("id",
            "activity.objectives[" + index + "].outcomeType");
    $(this).find("[name$='outcomeType']").attr("name",
            "activity.objectives[" + index + "].outcomeType");
    
    $(this).find("[name$='researchUser']").attr("id",
            "activity.objectives[" + index + "].researchUser");
    $(this).find("[name$='researchUser']").attr("name",
            "activity.objectives[" + index + "].researchUser");
    
    $(this).find("[name$='researchUserRole']").attr("id",
            "activity.objectives[" + index + "].researchUserRole");
    $(this).find("[name$='researchUserRole']").attr("name",
            "activity.objectives[" + index + "].researchUserRole");
    
    $(this).find("[name$='researchOutcomeChanges']").attr("id",
            "activity.objectives[" + index + "].researchOutcomeChanges");
    $(this).find("[name$='researchOutcomeChanges']").attr("name",
            "activity.objectives[" + index + "].researchOutcomeChanges");
    
    $(this).find("[name$='genderDescription']").attr("id",
            "activity.objectives[" + index + "].genderDescription");
    $(this).find("[name$='genderDescription']").attr("name",
            "activity.objectives[" + index + "].genderDescription");
    
    $(this).find("[name$='intermediaries']").attr("id",
            "activity.objectives[" + index + "].intermediaries");
    $(this).find("[name$='intermediaries']").attr("name",
            "activity.objectives[" + index + "].intermediaries");
    
    $(this).find("[name$='capacityBuilding']").attr("id",
            "activity.objectives[" + index + "].capacityBuilding");
    $(this).find("[name$='capacityBuilding']").attr("name",
            "activity.objectives[" + index + "].capacityBuilding");
    
    $(this).find("[name$='progress']").attr("id",
            "activity.objectives[" + index + "].progress");
    $(this).find("[name$='progress']").attr("name",
            "activity.objectives[" + index + "].progress");
    
    // Add the word counter
    if( ! $(this).find("[name$='description']").hasClass("wordCounter")){
      applyWordCounter($(this).find("[name$='description']"), 300);
      $(this).find("[name$='description']").addClass("wordCounter");
    }
  });
}