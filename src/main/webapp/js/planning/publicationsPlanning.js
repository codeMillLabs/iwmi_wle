$(document).ready(
    function() {
      $(".addPublication").click(function(event) {
        event.preventDefault();
        // Cloning tempalte.
        var $newPublication = $("#publication-9999").clone(true);
        $("#addPublicationBlock").before($newPublication);
        renamePublications();
        $newPublication.fadeIn("slow");
        $newPublication.find("[id$='publicationReference']").focus();
      });

      $('.removePublication').click(function(event) {
        event.preventDefault();
        // Getting the id.
        var removeId = $(event.target).attr("id").split("-")[1];
        $("#publication-" + removeId).fadeOut("slow");
        // removing division line.
        $("#publication-" + removeId).next("hr").hide("slow", function() {
          $(this).remove();
        });
        // removing div.
        $("#publication-" + removeId).hide("slow", function() {
          $(this).remove();
          renamePublications();
        });
      });
      
      $(".iframe").colorbox({iframe:true, width:"60%", height:"80%"});

    });

function renamePublications() {
  //getting the text of the index element.
  var itemText = $("#publicationTemplate").find(".itemIndex").text();
  $("#publicationsBlock .publication").each(
      function(index, publication) {
        // Changing attributes of each component in order to match with the array order.
        // Main div.
        $(this).attr("id", "publication-" + index);
        // Remove link.
        $(this).find("[id^='removePublication-']").attr("id",
            "removePublication-" + index);
        // Id
        $(this).find("[name$='id']").attr("name",
            "activity.publications[" + index + "].id");
        //Item index
        $(this).find(".itemIndex").text(itemText + " " + (index + 1));
        
        // Publication Reference.
        $(this).find("[id$='publicationReference']").attr("id",
            "activity.publications[" + index + "].publicationReference");
        $(this).find("[name$='publicationReference']").attr("name",
            "activity.publications[" + index + "].publicationReference");
        $(this).find("[for$='publicationReference']").attr("for",
            "activity.publications[" + index + "].publicationReference");
        
        // Peer Reviewed.
        $(this).find("[id$='peerReviewed']").attr("id",
            "activity.publications[" + index + "].peerReviewed");
        $(this).find("[name$='peerReviewed']").attr("name",
            "activity.publications[" + index + "].peerReviewed");
        $(this).find("[for$='peerReviewed']").attr("for",
            "activity.publications[" + index + "].peerReviewed");
        
        // Published in ISI Journal.
        $(this).find("[id$='isiJournal']").attr("id",
            "activity.publications[" + index + "].isiJournal");
        $(this).find("[name$='isiJournal']").attr("name",
            "activity.publications[" + index + "].isiJournal");
        $(this).find("[for$='isiJournal']").attr("for",
            "activity.publications[" + index + "].isiJournal");
        
        // Weblink.
        $(this).find("[id$='webLink']").attr("id",
            "activity.publications[" + index + "].webLink");
        $(this).find("[name$='webLink']").attr("name",
            "activity.publications[" + index + "].webLink");
        $(this).find("[for$='webLink']").attr("for",
            "activity.publications[" + index + "].webLink");
      });
}