$(document)
    .ready(
        function() {

            var viewable = $('#viewable').val();
            var editable = $('#editable').val();
            var reviewable = $('#reviewable').val();
            var publishable = $('#publishable').val();
            var published = $('#published').val();

            //editable
            if (viewable == 'false' && editable == 'false') {
                $("#activityClusterReport :input").attr('disabled', true);
                $("#activityClusterReport :button").attr('disabled', false);
                $("#activityClusterReport .hiddenVal").attr('disabled', false);
            }
            //
            //            // review block
            if(published == 'true') {
            	 $("#activityClusterReport .review_feedback").attr('disabled', true);
            	 $("#activityClusterReport .review_feedback_block").show();
            } else if (reviewable == 'true') {
                $("#activityClusterReport .review_feedback").attr('disabled', false);
                $("#activityClusterReport .review_feedback_block").show();
            } else if (publishable == 'true') {
                $("#activityClusterReport .review_feedback").attr('disabled', true);
                $("#activityClusterReport .review_feedback_block").show();
            } else {
                $("#activityClusterReport .review_feedback").attr('disabled', true);
                $("#activityClusterReport .review_feedback_block").hide();
            }


            //Load Saved Data Set
            $('.column-left-grid input[type=radio]:checked').each(function(i, j) {
                //select radio option
                var toolOption = $(j);
                var id = toolOption.attr('id');
                var data = toolOption.attr('data');
                var stringVal = String(data);

                //split to array
                var object = stringVal.substring(stringVal.lastIndexOf('[') + 1).replace("]'", "");
                var trainingMethodString = stringVal.substring(stringVal.lastIndexOf('trainingMethod='), stringVal.lastIndexOf('trainingCompleted')).replace("trainingMethod=", "");
                var array = object.split(',');
                //                var trainingMethods = trainingMethodString.split(',');

                //create review rating object
                var reviewRating = {};
                for (var key in array) {
                    var value = array[key].split('=');
                    if (value[0] == "trainingMethod") {
                        reviewRating[value[0]] = trainingMethodString;
                    } else {
                        reviewRating[value[0]] = value[1];

                    }
                }
                //load the save data
                loadPreData(id, reviewRating);
            })


            //radio on change
            $('input[type=radio]')
                .change(
                    function() {
                        $('div[id=content_' + index + ']').html("");
                        var id = this.id;
                        var subStr = id.lastIndexOf('_');

                        var name = id.substring(0, subStr);
                        var index = id.substring(subStr + 1);

                        if (name == 'tool') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Type of Tool ?</span></br></br>" +
                                    "<select id=toolType_" + index + "  name='activity.midYearReviews.deliverableRating[" + index + "].toolType' >" +
                                    "<option value='Guideline'>Guideline</option>" + "<option value='Manual'>Manual</option>" +
                                    "<option value='Decision Support Tool'>Decision Support Tool</option>" +
                                    "<option value='Curriculum's>Curriculum</option>" +
                                    "<option value='Games'>Games</option>" +
                                    "<option value='Workbooks'>Workbooks</option>" +
                                    "<option value='Other'>Other</option>" +
                                    "</select>" +
                                    "<div>" +
                                    "<span id=toolOther_" + index + " > <span>");

                        } else if (name == 'policy') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Name of policy regulation or procedure</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].nameOfPolicyProcedure' style='width:120%'></input>" + "</br></br>" + "<span class='tool-label'>Stage of development ? Has the policy been</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopement'>" + "<option value='Analysed'>Analysed</option>" + "<option value='Drafted and Presented for Stakeholder Consultation'>Drafted and Presented for Stakeholder Consultation</option>" + "<option value='Presented for Legislation'>Presented for Legislation</option>" + "<option value='Ready for Implemention'>Ready for Implemention</option>" + "</select>" + "<div>");
                        } else if (name == 'publication') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Is the publication : Draft? Published?</span></br>" + "<select id='pubType_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].pubType' onslec>" + "<option value='draft'>Draft</option>" + "<option value='published'>Published</option>" + "</select>" + "</br></br>" + "<span id=pubTypeContent_" + index + " > <span>" + "<div>");
                        } else if (name == 'platform') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Name of Stakeholder Platform?</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].nameOfStakeholderPlatform' style='width:120%'></input" + "<div>");
                        } else if (name == 'database') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Is it Open Access ?</span>" +
                                    "<select id='databaseOpenAccess_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].databaseOpenAccess'>" +
                                    "<option value='No'>No</option>" +
                                    "<option value='Yes'>Yes</option>" +
                                    "</select></br></br>" +
                                    "<span id=databaseLink_" + index + " > <span>");
                        } else if (name == 'dataset') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Is it Open Access ?</span>" +
                                    "<select id='dataSetOpenAccess_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].dataSetOpenAccess'>" +
                                    "<option value='No'>No</option>" +
                                    "<option value='Yes'>Yes</option>" +
                                    "</select></br></br>" +
                                    "<span id=dataSetLink_" + index + " > <span>");
                        } else if (name == 'communications') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Type of Communications product?</span>" +
                                    "<select id='typeOfCommunication_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].typeOfCommunication'>" +
                                    "<option value='Research Report'>Research Report</option>" +
                                    "<option value='Student Thesis'>student Thesis</option>" +
                                    "<option value='Conference Paper'>Conference Paper</option>" +
                                    "<option value='Poster'>Poster</option>" +
                                    "<option value='Flyer'>Flyer</option>" +
                                    "<option value='Briefing Paper/Policy Brief'>Briefing paper/policy brief</option>" +
                                    "<option value='Social Media Output'>Social Media Output</option>" +
                                    "<option value='Video'>Video</option>" +
                                    "<option value='Media article '>Media Article </option>" +
                                    "<option value='Other'>Other</option>" +
                                    "<option value='Presentation'>Presentation</option>" + "</select>" + "</br></br>" +

                                    "<span id=typeOfCommunicationOther_" + index + " > <span>");
                        } else if (name == 'other') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append("<span class='tool-label'>Description</span>" + "<textarea  type='text' name='activity.midYearReviews.deliverableRating[" + index + "].otherDesc' cols='40' rows='5'></textarea></br></br>");
                        } else if (name == 'capacity-development') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" +
                                    "<span class='tool-label'>Was/Is the Capacity Development of </span></br>" +
                                    "<select id='typeCapacityDev_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].typeCapacityDev'>" +
                                    "<option value='Individuals'>Individuals</option>" +
                                    "<option value='Institutions'>Institutions</option>" +
                                    "</select></br></br>" +
                                    "<span id=typeCapacityDev_" + index + " > <span>");
                        } else if (name == 'technology') {
                            $('div[id=content_' + index + ']')
                                .html("");
                            $('div[id=content_' + index + ']')
                                .append(
                                    "<div class='grid'>" + "<span class='tool-label'>Type of Technology/Practice ?</span></br>" +
                                    "<select id='techType_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].techType'>" +
                                    "<option value='management_cultural_practices'>Management/Cultural Practices</option>" +
                                    "<option value='mechanical_physical'>Mechanical/Physical</option>" +
                                    "<option value='biological'>Biological</option>" +
                                    "<option value='chemical'>Chemical</option>" +
                                    "<option value='other'>Other</option>" +
                                    "</select>" +
                                    "</br></br>" +
                                    "<span id=techTypeOther_" + index + " > <span>" +
                                    "<span class='tool-label'>Stage of development ?</span></br>" +
                                    "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                                    "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");
                        }



                        //select on change
                        $('select')
                            .change(
                                function() {
                                    var id = this.id;

                                    var subStr = id.lastIndexOf('_');
                                    var name = id.substring(0, subStr);
                                    var index = id.substring(subStr + 1);
                                    var value = this.value;

                                    //									console.log("Name :" + name + ", Index :" +  index + ", Value :" + value);

                                    if (name == "techType") {
                                        if (value == 'other') {
                                            $('span[id=techTypeOther_' + index + ']')
                                                .html("");
                                            $('span[id=techTypeOther_' + index + ']')
                                                .append(
                                                    "<textarea  type='text' name='activity.midYearReviews.deliverableRating[" + index + "].techOther' cols='40' rows='5'></textarea></br></br>" +
                                                    "<span class='tool-label'>Stage of development ?</span></br>" +
                                                    "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                                                    "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");

                                        } else {
                                            $('span[id=techTypeOther_' + index + ']').html("<span class='tool-label'>Stage of development ?</span></br>" +
                                                "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                                                "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");
                                        }
                                    } else if (name == "toolType") {

                                        if (value == 'Other') {

                                            $('span[id=toolOther_' + index + ']')
                                                .html("");
                                            $('span[id=toolOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" + "<textarea id='toolOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>");


                                        } else {
                                            $('span[id=toolOther_' + index + ']').html("");
                                        }
                                    } else if (name == "typeOfCommunication") {

                                        if (value == 'Other') {

                                            $('span[id=typeOfCommunicationOther_' + index + ']')
                                                .html("");
                                            $('span[id=typeOfCommunicationOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" +
                                                "<textarea id='typeOfCommunicationOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFemales' cols='40' rows='5'></textarea></br></br>");


                                        } else {
                                            $('span[id=typeOfCommunicationOther_' + index + ']').html("");
                                        }
                                    }
                                    if (name == 'pubType') {

                                        if (value == 'published') {
                                            $('span[id=pubTypeContent_' + index + ']')
                                                .html("");
                                            $('span[id=pubTypeContent_' + index + ']')
                                                .append(
                                                    "<span class='tool-label'>Weblink</span></br>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubWebLink'></input>" + "<span class='tool-label'>Publication Reference in Full</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubReference'></input>" + "<span class='tool-label'>Is it open access ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].pubOpenAccess'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Published in ISI Journal ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].publishedInISI'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Has it been Peer Reviewed ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].journalPeerReviewed'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Was this publication included in last year’s report?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].wasThisInLastYearReport'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>");
                                        } else {
                                            $('span[id=pubTypeContent_' + index + ']').html("");
                                        }
                                    } else if (name == 'typeCapacityDev') {

                                        if (value == 'Institutions') {

                                            $('span[id=typeCapacityDev_' + index + ']')
                                                .html("");
                                            $('span[id=typeCapacityDev_' + index + ']')
                                                .append("<span class='tool-label'>Training Method</span></br>" +
                                                    "<select name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' multiple='multiple' size='11' >" +
                                                    "<option value='Workshop'>Workshop (Participatory)</option>" +
                                                    "<option value='Demonstration'>Demonstration</option>" +
                                                    "<option value='FieldTraining'>Field Training </option>" +
                                                    "<option value='FarmerExchange'>Farmer Exchange</option>" +
                                                    "<option value='FieldTrip'>Field Trip</option>" +
                                                    "<option value='TechnicalClassroom_Instruction'>Technical Classroom Instruction</option>" +
                                                    "<option value='Seminar'>Seminar</option>" +
                                                    "<option value='PolicyDialogue'>Policy Dialogue</option>" +
                                                    "<option value='EngagingDecisionMakersinResearch'>Engaging Decision Makers in Research</option>" +
                                                    "<option value='MultiStakeholderPlatform'>Multi-Stakeholder Platform</option>" +
                                                    "<option value='Other'>Other</option>" +
                                                    "</select></br></br>" +
                                                    "<span class='tool-label'>Has the training already taken place/been completed?</span></br>" +
                                                    "<select id='trainingCompleted_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingCompleted'>" +
                                                    "<option value='No'>No</option>" +
                                                    "<option value='Yes'>Yes</option>" +
                                                    "</select></br></br>");
                                            //                                                    "<span id=trainingCompleted_" + index + " > <span>"




                                            //select on change
                                            $('select')
                                                .change(
                                                    function() {

                                                        var id = this.id;

                                                        var subStr = id.lastIndexOf('_');
                                                        var name = id.substring(0, subStr);
                                                        var index = id.substring(subStr + 1);
                                                        var value = this.value;

                                                        if (name == 'trainingCompleted') {

                                                            if (value == 'Yes') {
                                                                $('span[id=trainingCompleted_' + index + ']')
                                                                    .html("");
                                                                $('span[id=trainingCompleted_' + index + ']')
                                                                    .append("<span class='tool-label'>Title of Training course/workshop etc.</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].titleOfWorkshop'></input></br></br>" +
                                                                        "<span class='tool-label'>Total No. of trainees?</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noOfTrainees'></input></br></br>" +
                                                                        "<span class='tool-label'>Countries of origin of trainees</span>" +
                                                                        "<textarea  type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>" +
                                                                        "<span class='tool-label'>No. trainees from CGIAR</span>" +
                                                                        "<span class='tool-label'>No. Female</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFemales'></input></br></br>" +
                                                                        "<span class='tool-label'>No. from developing countries</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFromDev'></input></br></br>" +
                                                                        "<span class='tool-label'>No. trainees not from CGIAR</span>" +
                                                                        "<span class='tool-label'>No. Female</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFemales'></input></br></br>" +
                                                                        "<span class='tool-label'>No. from NARS</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromNars'></input></br></br>" +
                                                                        "<span class='tool-label'>No. from developing countries</span>" +
                                                                        "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromDev'></input></br></br>" +
                                                                        "<span class='tool-label'>Was there an evaluation of the course by participants ?</span></br>" +
                                                                        "<select name='activity.midYearReviews.deliverableRating[" + index + "].evalutionOfCourse'>" +
                                                                        "<option value='No'>No</option>" +
                                                                        "<option value='Yes'>Yes</option>" +
                                                                        "</select></br></br>" +
                                                                        "<span class='tool-label'>Is there a study planned for future impacts of the course ?</span></br>" +
                                                                        "<select name='activity.midYearReviews.deliverableRating[" + index + "].studyPlanned'>" +
                                                                        "<option value='No'>No</option>" +
                                                                        "<option value='Yes'>Yes</option>" +
                                                                        "</select></br></br>" +
                                                                        "</div>");



                                                            } else {
                                                                $('span[id=trainingCompleted_' + index + ']').html("");
                                                            }
                                                        }
                                                    });

                                        } else {
                                            $('span[id=typeCapacityDev_' + index + ']')
                                                .html("");
                                            $('span[id=typeCapacityDev_' + index + ']')
                                                .append("<span class='tool-label'>Training Method</span></br>" +
                                                    "<select id='trainingMethod_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' >" +
                                                    "<option value='Degree training (PhD or Masters)'>Degree training (PhD or Masters)</option>" +
                                                    "<option value='Internship'>Internship</option>" +
                                                    "<option value='Mentoring'>Mentoring</option>" +
                                                    "<option value='Fellowship'>Fellowship</option>" +
                                                    "<option value='Other'>Other</option>" +
                                                    "</select></br></br>" +
                                                    "<span id=trainingCompleted_" + index + " > <span>");

                                            //select on change
                                            $('select')
                                                .change(
                                                    function() {

                                                        var id = this.id;
                                                        var subStr = id.lastIndexOf('_');
                                                        var name = id.substring(0, subStr);
                                                        var index = id.substring(subStr + 1);
                                                        var value = this.value;

                                                        if (name == 'trainingMethod') {

                                                            if (value == 'Degree training (PhD or Masters)') {
                                                                $('span[id=trainingCompleted_' + index + ']')
                                                                    .html("");
                                                                $('span[id=trainingCompleted_' + index + ']')
                                                                    .append("<span class='tool-label'>Level?</span>" +
                                                                        "<select id='level_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].level' >" +
                                                                        "<option value='MSc'>MSc</option>" +
                                                                        "<option value='Internship'>Internship</option>" +
                                                                        "<option value='PhD'>PhD</option>" +
                                                                        "<option value='Bachelor'>Bachelor’s</option>" +
                                                                        "</select></br></br>");



                                                            } else {
                                                                $('span[id=trainingCompleted_' + index + ']').html("");
                                                            }
                                                        }
                                                    });
                                        }
                                    } else if (name == 'databaseOpenAccess') {

                                        if (value == 'Yes') {
                                            $('span[id=databaseLink_' + index + ']')
                                                .html("");
                                            $('span[id=databaseLink_' + index + ']')
                                                .append(
                                                    "<span class='tool-label'>Provide link</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].databaseLink' style='width:120%'></input>" + "<div>");
                                        } else {
                                            $('span[id=databaseLink_' + index + ']').html("");
                                        }
                                    } else if (name == 'dataSetOpenAccess') {

                                        if (value == 'Yes') {
                                            $('span[id=dataSetLink_' + index + ']')
                                                .html("");
                                            $('span[id=dataSetLink_' + index + ']')
                                                .append(
                                                    "<span class='tool-label'>Provide link</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].dataSetLink' style='width:120%'></input>" + "<div>");
                                        } else {
                                            $('span[id=dataSetLink_' + index + ']').html("");
                                        }
                                    }



                                });
                    });


            $(".popup").colorbox({
                iframe: true,
                width: "60%",
                height: "80%"
            });


            function loadPreData(id, reviewRating) {

                var subStr = id.lastIndexOf('_');
                var name = id.substring(0, subStr).trim();
                var index = id.substring(subStr + 1);

                //				console.log("Name :" +  name +", index :" + index + "");
                if (name == 'tool') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Type of Tool ?</span></br></br>" +
                            "<select id='toolType_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].toolType' >" +
                            "<option value='Guideline'>Guideline</option>" +
                            "<option value='Manual'>Manual</option>" +
                            "<option value='Decision Support Tool'>Decision Support Tool</option>" +
                            "<option value='Curriculum's>Curriculum</option>" +
                            "<option value='Games'>Games</option>" +
                            "<option value='Workbooks'>Workbooks</option>" +
                            "<option value='Other'>Other</option>" + "</select>" +
                            "<span id=toolOther_" + index + " > <span>");

                    if (reviewRating.toolType != null) {
                        document.querySelector('#toolType_' + index + '  [value="' + reviewRating.toolType + '"]').selected = true;
                    }
                    if (reviewRating.toolType == "Other") {

                        $('span[id=toolOther_' + index + ']')
                            .html("");
                        $('span[id=toolOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" + "<textarea id='toolOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>");

                        if (reviewRating.countries != null) {
                            document.querySelector('#toolOtherDesc_' + index).value = reviewRating.countries;

                        }
                    }
                } else if (name == 'policy') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Name of policy regulation or procedure</span>" + "<input id='nameOfPolicyProcedure_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].nameOfPolicyProcedure' style='width:120%'></input>" + "</br></br>" + "<span class='tool-label'>Stage of development ? Has the policy been</span></br>" + "<select id='policy_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopement'>" + "<option value='Analysed'>Analysed</option>" + "<option value='Drafted and Presented for Stakeholder Consultation'>Drafted and Presented for Stakeholder Consultation</option>" + "<option value='Presented for Legislation'>Presented for Legislation</option>" + "<option value='Ready for Implemention'>Ready for Implemention</option>" + "</select>" + "<div>");
                    if (reviewRating.nameOfPolicyProcedure != null) {
                        document.querySelector('#nameOfPolicyProcedure_' + index).value = reviewRating.nameOfPolicyProcedure;
                    }
                    if (reviewRating.stageOfDevelopement != null) {
                        document.querySelector('#' + id + ' [value="' + reviewRating.stageOfDevelopement + '"]').selected = true;
                    }
                } else if (name == 'publication') {

                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Is the publication : Draft? Published?</span></br>" + "<select id='pubType_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].pubType' >" + "<option value='draft'>Draft</option>" + "<option value='published'>Published</option>" + "</select>" + "</br></br>" + "<span id=pubTypeContent_" + index + " > <span>" + "<div>");
                    if (reviewRating.pubType != null) {
                        document.querySelector('#pubType_' + index + ' [value="' + reviewRating.pubType + '"]').selected = true;
                    }
                    if (reviewRating.pubType == "published") {
                        $('span[id=pubTypeContent_' + index + ']').html("");
                        $('span[id=pubTypeContent_' + index + ']').append(
                            "<span class='tool-label'>Weblink</span></br>" +
                            "<input id='pubWebLink_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubWebLink'></input>" + "<span class='tool-label'>Publication Reference in Full</span>" +
                            "<input id='pubReference_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubReference'></input>" + "<span class='tool-label'>Is it open access ?</span></br>" +
                            "<select id='pubOpenAccess_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].pubOpenAccess'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Published in ISI Journal ?</span></br>" +
                            "<select id='publishedInISI_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].publishedInISI'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Has it been Peer Reviewed ?</span></br>" +
                            "<select id='journalPeerReviewed_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].journalPeerReviewed'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Was this publication included in last year’s report?</span></br>" +
                            "<select id='wasThisInLastYearReport_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].wasThisInLastYearReport'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>");
                        if (reviewRating.pubWebLink != null) {
                            document.querySelector('#pubWebLink_' + index).value = reviewRating.pubWebLink;
                        }
                        if (reviewRating.pubReference != null) {
                            document.querySelector('#pubReference_' + index).value = reviewRating.pubReference;
                        }
                        if (reviewRating.pubOpenAccess != null) {
                            document.querySelector('#pubOpenAccess_' + index + ' [value="' + reviewRating.pubOpenAccess + '"]').selected = true;
                        }
                        if (reviewRating.publishedInISI != null) {
                            document.querySelector('#publishedInISI_' + index + ' [value="' + reviewRating.publishedInISI + '"]').selected = true;
                        }
                        if (reviewRating.journalPeerReviewed != null) {
                            document.querySelector('#journalPeerReviewed_' + index + ' [value="' + reviewRating.journalPeerReviewed + '"]').selected = true;
                        }
                        if (reviewRating.wasThisInLastYearReport != null) {
                            document.querySelector('#wasThisInLastYearReport_' + index + ' [value="' + reviewRating.wasThisInLastYearReport + '"]').selected = true;
                        }
                    }
                } else if (name == 'platform') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Name of Stakeholder Platform?</span>" + "<input id='nameOfStakeholderPlatform_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].nameOfStakeholderPlatform' style='width:120%'></input" + "<div>");
                    if (reviewRating.nameOfStakeholderPlatform != null) {
                        document.querySelector('#nameOfStakeholderPlatform_' + index).value = reviewRating.nameOfStakeholderPlatform;
                    }
                } else if (name == 'database') {
                    //					console.log("Inside Database :" + name + " Values : " +  reviewRating.databaseOpenAccess + ":::; Link :" + reviewRating.databaseLink);
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Is it Open Access ?</span>" +
                            "<select id='databaseOpenAccess_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].databaseOpenAccess'>" +
                            "<option value='No'>No</option>" +
                            "<option value='Yes'>Yes</option>" +
                            "</select></br></br>" +
                            "<input id=databaseLink_" + index + " > </input>");

                    if (reviewRating.databaseOpenAccess != null) {
                        //						console.log("Inside Database :" + name + " Values : databaseOpenAccess " +  reviewRating.databaseOpenAccess );
                        document.querySelector('#databaseOpenAccess_' + index + ' [value="' + reviewRating.databaseOpenAccess.trim() + '"]').selected = true;
                    }
                    if (reviewRating.databaseLink != null) {
                        //						console.log("Inside Database :" + name + " Values : databaseLink " +  reviewRating.databaseLink );
                        document.querySelector('#databaseLink_' + index).value = reviewRating.databaseLink;
                    }
                } else if (name == 'dataset') {
                    //					console.log("Inside dataset :" + name + " Values :" +  reviewRating.databaseOpenAccess + ":::; Link :" + reviewRating.databaseLink);
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Is it Open Access ?</span>" +
                            "<select id='dataSetOpenAccess_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].dataSetOpenAccess'>" +
                            "<option value='No'>No</option>" +
                            "<option value='Yes'>Yes</option>" +
                            "</select></br></br>" +
                            "<input id=dataSetLink_" + index + " > </input>");

                    if (reviewRating.dataSetOpenAccess != null) {
                        document.querySelector('#dataSetOpenAccess_' + index + ' [value="' + reviewRating.dataSetOpenAccess.trim() + '"]').selected = true;
                    }
                    if (reviewRating.dataSetLink != null) {
                        document.querySelector('#dataSetLink_' + index).value = reviewRating.dataSetLink;
                    }
                } else if (name == 'communications') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Type of Communications product?</span>" +
                            "<select id='typeOfCommunication_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].typeOfCommunication'>" +
                            "<option value='Research Report'>Research Report</option>" +
                            "<option value='Student Thesis'>student Thesis</option>" +
                            "<option value='Conference Paper'>Conference Paper</option>" +
                            "<option value='Poster'>Poster</option>" +
                            "<option value='Flyer'>Flyer</option>" +
                            "<option value='Briefing Paper/Policy Brief'>Briefing paper/policy brief</option>" +
                            "<option value='Social Media Output'>Social Media Output</option>" +
                            "<option value='Video'>Video</option>" +
                            "<option value='Media article '>Media Article </option>" +
                            "<option value='Other'>Other</option>" +
                            "<option value='Presentation'>Presentation</option>" + "</select>" + "</br></br>" +

                            "<span id=typeOfCommunicationOther_" + index + " > <span>");
                    if (reviewRating.typeOfCommunication != null) {
                        document.querySelector('#typeOfCommunication_' + index + ' [value="' + reviewRating.typeOfCommunication + '"]').selected = true;
                    }

                    if (reviewRating.typeOfCommunication == 'Other') {

                        $('span[id=typeOfCommunicationOther_' + index + ']')
                            .html("");
                        $('span[id=typeOfCommunicationOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" + "<textarea id='typeOfCommunicationOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFemales' cols='40' rows='5'></textarea></br></br>");
                        if (reviewRating.noCgiarFemales != null) {
                            document.querySelector('#typeOfCommunicationOtherDesc_' + index).value = reviewRating.noCgiarFemales;

                        }
                    }

                } else if (name == 'other') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']').append("<span class='tool-label'>Description</span>" + "<textarea id='otherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].otherDesc' cols='40' rows='5'></textarea></br></br>");
                    if (reviewRating.otherDesc != null) {
                        document.querySelector('#otherDesc_' + index).value = reviewRating.otherDesc;
                    }
                } else if (name == 'capacity-development') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" +
                            "<span class='tool-label'>Was/Is the Capacity Development of </span></br>" +
                            "<select id='typeCapacityDev_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].typeCapacityDev'>" +
                            "<option value='Individuals'>Individuals</option>" +
                            "<option value='Institutions'>Institutions</option>" +
                            "</select></br></br>" +
                            "<span id=typeCapacityDev_" + index + " > <span>");
                    if (reviewRating.typeCapacityDev != null) {
                        document.querySelector('#typeCapacityDev_' + index + ' [value="' + reviewRating.typeCapacityDev + '"]').selected = true;
                    }
                    if (reviewRating.typeCapacityDev != null && reviewRating.typeCapacityDev == "Institutions") {
                        $('span[id=typeCapacityDev_' + index + ']')
                            .html("");
                        $('span[id=typeCapacityDev_' + index + ']')
                            .append("<span class='tool-label'>Training Method</span></br>" +
                                "<select id='trainingMethod_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' multiple='multiple' size='11'>" +
                                "<option value='Workshop'>Workshop (Participatory)</option>" +
                                "<option value='Demonstration'>Demonstration</option>" +
                                "<option value='FieldTraining'>Field Training </option>" +
                                "<option value='FarmerExchange'>Farmer Exchange</option>" +
                                "<option value='FieldTrip'>Field Trip</option>" +
                                "<option value='TechnicalClassroom_Instruction'>Technical Classroom Instruction</option>" +
                                "<option value='Seminar'>Seminar</option>" +
                                "<option value='PolicyDialogue'>Policy Dialogue</option>" +
                                "<option value='EngagingDecisionMakersinResearch'>Engaging Decision Makers in Research</option>" +
                                "<option value='MultiStakeholderPlatform'>Multi-Stakeholder Platform</option>" +
                                "<option value='Other'>Other</option>" +
                                "</select></br></br>" +
                                "<span class='tool-label'>Has the training already taken place/been completed?</span></br>" +
                                "<select id='trainingCompleted_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingCompleted'>" +
                                "<option value='No'>No</option>" +
                                "<option value='Yes'>Yes</option>" +
                                "</select></br></br>" +
                                "<span id=trainingCompleted_" + index + " > <span>"
                            );
                        if (reviewRating.trainingMethod != null) {
                            var selectedData = reviewRating.trainingMethod.split(',');
                            for (i = 0; i < selectedData.length; ++i) {
                                if (i != selectedData.length - 1) {

                                    document.querySelector('#trainingMethod_' + index + ' [value="' + selectedData[i].trim() + '"]').selected = true;

                                }
                            }


                        }
                        if (reviewRating.trainingCompleted != null) {
                            document.querySelector('#trainingCompleted_' + index + ' [value="' + reviewRating.trainingCompleted + '"]').selected = true;
                        }

                        //                        if (reviewRating.trainingCompleted != null && reviewRating.trainingCompleted == "Yes") {
                        //                            $('span[id=trainingCompleted_' + index + ']')
                        //                                .html("");
                        //                            $('span[id=trainingCompleted_' + index + ']')
                        //                                .append("<span class='tool-label'>Title of Training course/workshop etc.</span>" +
                        //                                    "<input  id='titleOfWorkshop_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].titleOfWorkshop'></input></br></br>" +
                        //                                    "<span class='tool-label'>Total No. of trainees?</span>" +
                        //                                    "<input id='noOfTrainees_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noOfTrainees'></input></br></br>" +
                        //                                    "<span class='tool-label'>Countries of origin of trainees</span>" +
                        //                                    "<textarea id='countries_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>" +
                        //                                    "<span class='tool-label'>No. trainees from CGIAR</span>" +
                        //                                    "<span class='tool-label'>No. Female</span>" +
                        //                                    "<input id='noCgiarFemales_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFemales'></input></br></br>" +
                        //                                    "<span class='tool-label'>No. from developing countries</span>" +
                        //                                    "<input id='noCgiarFromDev_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFromDev'></input></br></br>" +
                        //                                    "<span class='tool-label'>No. trainees not from CGIAR</span>" +
                        //                                    "<span class='tool-label'>No. Female</span>" +
                        //                                    "<input id='noFemales_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFemales'></input></br></br>" +
                        //                                    "<span class='tool-label'>No. from NARS</span>" +
                        //                                    "<input id='noFromNars_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromNars'></input></br></br>" +
                        //                                    "<span class='tool-label'>No. from developing countries</span>" +
                        //                                    "<input id='noFromDev_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromDev'></input></br></br>" +
                        //                                    "<span class='tool-label'>Was there an evaluation of the course by participants ?</span></br>" +
                        //                                    "<select id='evalutionOfCourse_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].evalutionOfCourse'>" +
                        //                                    "<option value='No'>No</option>" +
                        //                                    "<option value='Yes'>Yes</option>" +
                        //                                    "</select></br></br>" +
                        //                                    "<span class='tool-label'>Is there a study planned for future impacts of the course ?</span></br>" +
                        //                                    "<select id='studyPlanned_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].studyPlanned'>" +
                        //                                    "<option value='No'>No</option>" +
                        //                                    "<option value='Yes'>Yes</option>" +
                        //                                    "</select></br></br>" +
                        //                                    "</div>");
                        //                            if (reviewRating.titleOfWorkshop != null) {
                        //                                document.querySelector('#titleOfWorkshop_' + index).value = reviewRating.titleOfWorkshop;
                        //                            }
                        //                            if (reviewRating.noOfTrainees != null) {
                        //                                document.querySelector('#noOfTrainees_' + index).value = reviewRating.noOfTrainees;
                        //                            }
                        //                            if (reviewRating.countries != null) {
                        //                                document.querySelector('#countries_' + index).value = reviewRating.countries;
                        //                            }
                        //                            if (reviewRating.noCgiarFemales != null) {
                        //                                document.querySelector('#noCgiarFemales_' + index).value = reviewRating.noCgiarFemales;
                        //                            }
                        //                            if (reviewRating.noCgiarFromDev != null) {
                        //                                document.querySelector('#noCgiarFromDev_' + index).value = reviewRating.noCgiarFromDev;
                        //                            }
                        //                            if (reviewRating.noFemales != null) {
                        //                                document.querySelector('#noFemales_' + index).value = reviewRating.noFemales;
                        //                            }
                        //                            if (reviewRating.noFromNars != null) {
                        //                                document.querySelector('#noFromNars_' + index).value = reviewRating.noFromNars;
                        //                            }
                        //                            if (reviewRating.noFromDev != null) {
                        //                                document.querySelector('#noFromDev_' + index).value = reviewRating.noFromDev;
                        //                            }
                        //                            if (reviewRating.evalutionOfCourse != null) {
                        //                                document.querySelector('#evalutionOfCourse_' + index + ' [value="' + reviewRating.evalutionOfCourse + '"]').selected = true;
                        //                            }
                        //                            if (reviewRating.studyPlanned != null) {
                        //                                document.querySelector('#studyPlanned_' + index + ' [value="' + reviewRating.studyPlanned + '"]').selected = true;
                        //                            }
                        //                        }
                    }
                    if (reviewRating.typeCapacityDev != null && reviewRating.typeCapacityDev == "Individuals") {
                        $('span[id=typeCapacityDev_' + index + ']')
                            .html("");
                        $('span[id=typeCapacityDev_' + index + ']')
                            .append("<span class='tool-label'>Training Method</span></br>" +
                                "<select id='trainingMethod_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' >" +
                                "<option value='Degree training (PhD or Masters)'>Degree training (PhD or Masters)</option>" +
                                "<option value='Internship'>Internship</option>" +
                                "<option value='Mentoring'>Mentoring</option>" +
                                "<option value='Fellowship'>Fellowship</option>" +
                                "<option value='Other'>Other</option>" +
                                "</select></br></br>" +
                                "<span id=trainingCompleted_" + index + " > <span>");
                        if (reviewRating.trainingMethod != null) {
                            document.querySelector('#trainingMethod_' + index).value = reviewRating.trainingMethod;

                        }

                        if (reviewRating.trainingMethod != null && reviewRating.trainingMethod == "Degree training (PhD or Masters)") {
                            $('span[id=trainingCompleted_' + index + ']')
                                .html("");
                            $('span[id=trainingCompleted_' + index + ']')
                                .append("<span class='tool-label'>Level?</span>" +
                                    "<select id='level_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].level' >" +
                                    "<option value='MSc'>MSc</option>" +
                                    "<option value='Internship'>Internship</option>" +
                                    "<option value='PhD'>PhD</option>" +
                                    "<option value='Bachelor'>Bachelor’s</option>" +
                                    "</select></br></br>");
                            if (reviewRating.level != null) {
                                document.querySelector('#trainingCompleted_' + index).value = reviewRating.level;

                            }
                        }

                    }

                } else if (name == 'technology') {
                    $('div[id=content_' + index + ']')
                        .html("");
                    $('div[id=content_' + index + ']')
                        .append(
                            "<div class='grid'>" + "<span class='tool-label'>Type of Technology/Practice ?</span></br>" +
                            "<select id='techType_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].techType'>" +
                            "<option value='management_cultural_practices'>Management/Cultural Practices</option>" +
                            "<option value='mechanical_physical'>Mechanical/Physical</option>" +
                            "<option value='biological'>Biological</option>" +
                            "<option value='chemical'>Chemical</option>" +
                            "<option value='other'>Other</option>" +
                            "</select>" +
                            "</br></br>" +
                            "<span id=techTypeOther_" + index + " > <span>" +
                            "<span id=techTypeStageOfDevelopment_" + index + " > <span>");

                    if (reviewRating.techType != null) {
                        document.querySelector('#techType_' + index + ' [value="' + reviewRating.techType + '"]').selected = true;
                    }

                    if (reviewRating.techType == "other") {
                        $('span[id=techTypeOther_' + index + ']').html("");
                        $('span[id=techTypeOther_' + index + ']').html("<textarea  id='techTypeOther_Text_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].techOther' cols='40' rows='5'></textarea></br></br>" + "<span class='tool-label'>Stage of development ?</span></br>" +
                            "<select id='stageOfDevelopment_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                            "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");
                        if (reviewRating.techOther != null) {
                            document.querySelector('#techTypeOther_Text_' + index).value = reviewRating.techOther;

                        }
                        if (reviewRating.stageOfDevelopment != null) {
                            document.querySelector('#stageOfDevelopment_' + index + ' [value="' + reviewRating.stageOfDevelopment + '"]').selected = true;
                        }
                    } else {
                        $('span[id=techTypeStageOfDevelopment_' + index + ']').html("");
                        $('span[id=techTypeStageOfDevelopment_' + index + ']').html("<span class='tool-label'>Stage of development ?</span></br>" +
                            "<select id='stageOfDevelopment_" + index + "'  name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                            "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");
                        if (reviewRating.stageOfDevelopment != null) {
                            document.querySelector('#stageOfDevelopment_' + index + ' [value="' + reviewRating.stageOfDevelopment + '"]').selected = true;
                        }
                    }


                }

                //select on change
                $('select')
                    .change(
                        function() {
                            var id = this.id;

                            var subStr = id.lastIndexOf('_');
                            var name = id.substring(0, subStr);
                            var index = id.substring(subStr + 1);
                            var value = this.value;

                            //						    console.log("Name :" + name + ", Value :" + value + ":");

                            if (name == "techType") {
                                if (value == 'other') {
                                    $('span[id=techTypeOther_' + index + ']')
                                        .html("");
                                    $('span[id=techTypeOther_' + index + ']')
                                        .append(
                                            "<textarea  type='text' name='activity.midYearReviews.deliverableRating[" + index + "].techOther' cols='40' rows='5'></textarea></br></br>" +
                                            "<span class='tool-label'>Stage of development ?</span></br>" +
                                            "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                                            "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");

                                } else {
                                    $('span[id=techTypeOther_' + index + ']').html("<span class='tool-label'>Stage of development ?</span></br>" +
                                        "<select name='activity.midYearReviews.deliverableRating[" + index + "].stageOfDevelopment'>" +
                                        "<option value='under_research'>Under Research</option>" + "<option value='field_tested'>Field Tested</option>" + "</select>" + "<div>");
                                }
                            } else if (name == "toolType") {

                                if (value == 'Other') {

                                    $('span[id=toolOther_' + index + ']')
                                        .html("");
                                    $('span[id=toolOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" + "<textarea id='toolOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>");


                                } else {
                                    $('span[id=toolOther_' + index + ']').html("");
                                }
                            } else if (name == "typeOfCommunication") {

                                if (value == 'Other') {

                                    $('span[id=typeOfCommunicationOther_' + index + ']')
                                        .html("");
                                    $('span[id=typeOfCommunicationOther_' + index + ']').append("</br><span class='tool-label'>Description</span>" + "<textarea id='typeOfCommunicationOtherDesc_" + index + "' type='text' name='activity.midYearReviews.deliverableRating[" + index + "].level' cols='40' rows='5'></textarea></br></br>");


                                } else {
                                    $('span[id=typeOfCommunicationOther_' + index + ']').html("");
                                }
                            }
                            if (name == 'pubType') {

                                if (value == 'published') {
                                    $('span[id=pubTypeContent_' + index + ']')
                                        .html("");
                                    $('span[id=pubTypeContent_' + index + ']')
                                        .append(
                                            "<span class='tool-label'>Weblink</span></br>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubWebLink'></input>" + "<span class='tool-label'>Publication Reference in Full</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].pubReference'></input>" + "<span class='tool-label'>Is it open access ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].pubOpenAccess'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Published in ISI Journal ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].publishedInISI'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Has it been Peer Reviewed ?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].journalPeerReviewed'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>" + "</br></br>" + "<span class='tool-label'>Was this publication included in last year’s report?</span></br>" + "<select name='activity.midYearReviews.deliverableRating[" + index + "].wasThisInLastYearReport'>" + "<option value='yes'>Yes</option>" + "<option value='no'>No</option>" + "</select>");
                                } else {
                                    $('span[id=pubTypeContent_' + index + ']').html("");
                                }

                            } else if (name == 'typeCapacityDev') {

                                if (value == 'Institutions') {
                                    $('span[id=typeCapacityDev_' + index + ']')
                                        .html("");
                                    $('span[id=typeCapacityDev_' + index + ']')
                                        .append("<span class='tool-label'>Training Method</span></br>" +
                                            "<select id='trainingMethod_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' multiple='multiple' size='11'>" +
                                            "<option value='Workshop'>Workshop (Participatory)</option>" +
                                            "<option value='Demonstration'>Demonstration</option>" +
                                            "<option value='FieldTraining'>Field Training </option>" +
                                            "<option value='FarmerExchange'>Farmer Exchange</option>" +
                                            "<option value='FieldTrip'>Field Trip</option>" +
                                            "<option value='TechnicalClassroom_Instruction'>Technical Classroom Instruction</option>" +
                                            "<option value='Seminar'>Seminar</option>" +
                                            "<option value='PolicyDialogue'>Policy Dialogue</option>" +
                                            "<option value='EngagingDecisionMakersinResearch'>Engaging Decision Makers in Research</option>" +
                                            "<option value='MultiStakeholderPlatform'>Multi-Stakeholder Platform</option>" +
                                            "<option value='Other'>Other</option>" +
                                            "</select></br></br>" +
                                            "<span class='tool-label'>Has the training already taken place/been completed?</span></br>" +
                                            "<select id='trainingCompleted_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingCompleted'>" +
                                            "<option value='No'>No</option>" +
                                            "<option value='Yes'>Yes</option>" +
                                            "</select></br></br>" +
                                            "<span id=trainingCompleted_" + index + " > <span>"
                                        );

                                    //select on change
                                    $('select')
                                        .change(
                                            function() {

                                                var id = this.id;
                                                var subStr = id.lastIndexOf('_');
                                                var name = id.substring(0, subStr);
                                                var index = id.substring(subStr + 1);
                                                var value = this.value;

                                                if (name == 'trainingCompleted') {

                                                    if (value == 'Yes') {
                                                        $('span[id=trainingCompleted_' + index + ']')
                                                            .html("");
                                                        $('span[id=trainingCompleted_' + index + ']')
                                                            .append("<span class='tool-label'>Title of Training course/workshop etc.</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].titleOfWorkshop'></input></br></br>" +
                                                                "<span class='tool-label'>Total No. of trainees?</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noOfTrainees'></input></br></br>" +
                                                                "<span class='tool-label'>Countries of origin of trainees</span>" +
                                                                "<textarea  type='text' name='activity.midYearReviews.deliverableRating[" + index + "].countries' cols='40' rows='5'></textarea></br></br>" +
                                                                "<span class='tool-label'>No. trainees from CGIAR</span>" +
                                                                "<span class='tool-label'>No. Female</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFemales'></input></br></br>" +
                                                                "<span class='tool-label'>No. from developing countries</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noCgiarFromDev'></input></br></br>" +
                                                                "<span class='tool-label'>No. trainees not from CGIAR</span>" +
                                                                "<span class='tool-label'>No. Female</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFemales'></input></br></br>" +
                                                                "<span class='tool-label'>No. from NARS</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromNars'></input></br></br>" +
                                                                "<span class='tool-label'>No. from developing countries</span>" +
                                                                "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].noFromDev'></input></br></br>" +
                                                                "<span class='tool-label'>Was there an evaluation of the course by participants ?</span></br>" +
                                                                "<select name='activity.midYearReviews.deliverableRating[" + index + "].evalutionOfCourse'>" +
                                                                "<option value='No'>No</option>" +
                                                                "<option value='Yes'>Yes</option>" +
                                                                "</select></br></br>" +
                                                                "<span class='tool-label'>Is there a study planned for future impacts of the course ?</span></br>" +
                                                                "<select name='activity.midYearReviews.deliverableRating[" + index + "].studyPlanned'>" +
                                                                "<option value='No'>No</option>" +
                                                                "<option value='Yes'>Yes</option>" +
                                                                "</select></br></br>" +
                                                                "</div>");
                                                    } else {
                                                        $('span[id=trainingCompleted_' + index + ']').html("");
                                                    }
                                                }



                                            });


                                } else {
                                    $('span[id=typeCapacityDev_' + index + ']')
                                        .html("");
                                    $('span[id=typeCapacityDev_' + index + ']')
                                        .append("<span class='tool-label'>Training Method</span></br>" +
                                            "<select id='trainingMethod_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].trainingMethod' >" +
                                            "<option value='Degree training (PhD or Masters)'>Degree training (PhD or Masters)</option>" +
                                            "<option value='Internship'>Internship</option>" +
                                            "<option value='Mentoring'>Mentoring</option>" +
                                            "<option value='Fellowship'>Fellowship</option>" +
                                            "<option value='Other'>Other</option>" +
                                            "</select></br></br>" +
                                            "<span id=trainingCompleted_" + index + " > <span>");

                                    //select on change
                                    $('select')
                                        .change(
                                            function() {

                                                var id = this.id;
                                                var subStr = id.lastIndexOf('_');
                                                var name = id.substring(0, subStr);
                                                var index = id.substring(subStr + 1);
                                                var value = this.value;

                                                if (name == 'trainingMethod') {

                                                    if (value == 'Degree training (PhD or Masters)') {
                                                        $('span[id=trainingCompleted_' + index + ']')
                                                            .html("");
                                                        $('span[id=trainingCompleted_' + index + ']')
                                                            .append("<span class='tool-label'>Level?</span>" +
                                                                "<select id='level_" + index + "' name='activity.midYearReviews.deliverableRating[" + index + "].level' >" +
                                                                "<option value='MSc'>MSc</option>" +
                                                                "<option value='Internship'>Internship</option>" +
                                                                "<option value='PhD'>PhD</option>" +
                                                                "<option value='Bachelor'>Bachelor’s</option>" +
                                                                "</select></br></br>");



                                                    } else {
                                                        $('span[id=trainingCompleted_' + index + ']').html("");
                                                    }
                                                }
                                            });
                                }
                            } else if (name == 'databaseOpenAccess') {
                                //                            	console.log(" databaseOpenAccess Name :" + name + ", Value :" + value + ":");
                                if (value == 'Yes') {
                                    $('span[id=databaseLink_' + index + ']')
                                        .html("");
                                    $('span[id=databaseLink_' + index + ']')
                                        .append(
                                            "<span class='tool-label'>Provide link</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].databaseLink' style='width:120%'></input>" + "<div>");
                                } else {
                                    $('span[id=databaseLink_' + index + ']').html("");
                                }
                            } else if (name == 'dataSetOpenAccess') {
                                //                            	console.log(" dataSetOpenAccess Name :" + name + ", Value :" + value + ":");
                                if (value == 'Yes') {
                                    $('span[id=dataSetLink_' + index + ']')
                                        .html("");
                                    $('span[id=dataSetLink_' + index + ']')
                                        .append(
                                            "<span class='tool-label'>Provide link</span>" + "<input type='text' name='activity.midYearReviews.deliverableRating[" + index + "].dataSetLink' style='width:120%'></input>" + "<div>");
                                } else {
                                    $('span[id=dataSetLink_' + index + ']').html("");
                                }
                            }



                        });

                //editable
                if (editable == 'false' || viewable == 'false') {
                    $("#activityClusterReport :input").attr('disabled', true);
                    $("#activityClusterReport :button").attr('disabled', false);
                    $("#activityClusterReport .hiddenVal").attr('disabled', false);
                }

                // review block 
                if(published == 'true') {
                	$("#activityClusterReport .review_feedback").attr('disabled', true);
                    $("#activityClusterReport .review_feedback_block").show();
                } else  if (reviewable == 'true') {
                    $("#activityClusterReport .review_feedback").attr('disabled', false);
                    $("#activityClusterReport .review_feedback_block").show();
                } else if (publishable == 'true') {
                    $("#activityClusterReport .review_feedback").attr('disabled', true);
                    $("#activityClusterReport .review_feedback_block").show();
                } else {
                    $("#activityClusterReport .review_feedback").attr('disabled', true);
                    $("#activityClusterReport .review_feedback_block").hide();
                }
            }


            $("textarea").each(function() {
                this.value = $.trim(this.value);
            });

            $(".totalSpent").each(function() {
                this.value = parseFloat(this.value.replace(/[^\d.-]/g, '')).toFixed(2);
            });

            $(".totalSpent").change(function() {
                this.value = parseFloat(this.value.replace(/[^\d.-]/g, '')).toFixed(2);
            });
        });