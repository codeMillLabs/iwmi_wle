$(document).ready(function() {

});

google.load('visualization', '1.0', {
	'packages' : [ 'corechart' ]
});

google.setOnLoadCallback(drawChart);

function drawChart() {
	var count = $('input[name=count]').val() - 1;
	for (i = 0; i < count; i++) {
		var green = 'green' + (i + 1);
		var greenValue = parseInt($('input[name=' + green + ']').val().replace(/\,/g,''));
		var yellow = 'yellow' + (i + 1);
		var yellowValue = parseInt($('input[name=' + yellow + ']').val().replace(/\,/g,''));
		var red = 'red' + (i + 1);
		var redValue = parseInt($('input[name=' + red + ']').val().replace(/\,/g,''));
		
		 var ratingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',greenValue, yellowValue, redValue, '']
		                                                 ]);

	    var ratingOptoins = {title: 'Output Status',
		                     width: 200,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '70%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var ratingChartDiv = 'rating' + (i + 1);
	    var ratingChart = new google.visualization.ColumnChart(document.getElementById(ratingChartDiv));
	    ratingChart.draw(ratingData, ratingOptoins);


	}
}