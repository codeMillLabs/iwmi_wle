$(document).ready(function() {

});

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {
	'packages' : [ 'corechart' ]
});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

	var count = $('input[name=count]').val() - 1;
	for (i = 0; i < count; i++) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Topping');
		data.addColumn('number', 'Slices');
		var spentName = 'spent' + (i + 1);
		var spentValue = parseInt($('input[name=' + spentName + ']').val().replace(/\,/g,''));
		var unSpentName = 'unspent' + (i + 1);
		var unSpentValue = parseInt($('input[name=' + unSpentName + ']').val().replace(/\,/g,''));

		data.addRows([ [ 'Cluster Expenditure',  spentValue],
				[ 'Unspent',  unSpentValue] ]);

		// Set chart options
		var options = {
			'width' : 170,
			'height' : 170,
			is3D : true,
			colors: ['green', 'red'],
			legend: {position: 'none'},
			tooltip: {textStyle: {fontSize: 12}}
		};

		var chartDiv = 'chart_div' + (i + 1);

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document
				.getElementById(chartDiv));
		chart.draw(data, options);
		
		var green = 'green' + (i + 1);
		var greenValue = parseInt($('input[name=' + green + ']').val().replace(/\,/g,''));
		var yellow = 'yellow' + (i + 1);
		var yellowValue = parseInt($('input[name=' + yellow + ']').val().replace(/\,/g,''));
		var red = 'red' + (i + 1);
		var redValue = parseInt($('input[name=' + red + ']').val().replace(/\,/g,''));
		
		var ratingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',greenValue, yellowValue, redValue, '']
		                                                 ]);

	    var ratingOptoins = {title: 'Cluster Output Status',
		                     width: 150,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '70%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var ratingChartDiv = 'output-rating' + (i + 1);
	    var ratingChart = new google.visualization.ColumnChart(document.getElementById(ratingChartDiv));
	    ratingChart.draw(ratingData, ratingOptoins);
	    
	    var pgreen = 'pgreen' + (i + 1);
		var pgreenValue = parseInt($('input[name=' + pgreen + ']').val().replace(/\,/g,''));
		var pyellow = 'pyellow' + (i + 1);
		var pyellowValue = parseInt($('input[name=' + pyellow + ']').val().replace(/\,/g,''));
		var pred = 'pred' + (i + 1);
		var predValue = parseInt($('input[name=' + pred + ']').val().replace(/\,/g,''));
		
		var pratingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',pgreenValue, pyellowValue, predValue, '']
		                                                 ]);

	    var pratingOptoins = {title: 'Project Status',
		                     width: 150,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '70%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var pratingChartDiv = 'project-rating' + (i + 1);
	    var pratingChart = new google.visualization.ColumnChart(document.getElementById(pratingChartDiv));
	    pratingChart.draw(pratingData, pratingOptoins);
	}
	
	
	
	var fdata = new google.visualization.DataTable();
	fdata.addColumn('string', 'Topping');
	fdata.addColumn('number', 'Slices');
	var fspentName = 'fspent';
	var fspentValue = parseInt($('input[name=' + fspentName + ']').val().replace(/\,/g,''));
	var funSpentName = 'funspent';
	var funSpentValue = parseInt($('input[name=' + funSpentName + ']').val().replace(/\,/g,''));

	fdata.addRows([ [ 'Cluster Expenditure',  fspentValue],
			[ 'Unspent',  funSpentValue] ]);

	// Set chart options
	var foptions = {
		'width' : 230,
		'height' : 230,
		is3D : true,
		colors: ['green', 'red'],
		legend: {position: 'none'},
		tooltip: {textStyle: {fontSize: 12}}
	};

	var fchartDiv = 'fchart_div';

	// Instantiate and draw our chart, passing in some options.
	var fchart = new google.visualization.PieChart(document
			.getElementById(fchartDiv));
	fchart.draw(fdata, foptions);
	
	
	var fgreen = 'fgreen';
	var fgreenValue = parseInt($('input[name=' + fgreen + ']').val().replace(/\,/g,''));
	var fyellow = 'fyellow';
	var fyellowValue = parseInt($('input[name=' + fyellow + ']').val().replace(/\,/g,''));
	var fred = 'fred';
	var fredValue = parseInt($('input[name=' + fred + ']').val().replace(/\,/g,''));
	
	var fratingData = google.visualization.arrayToDataTable([
	                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
	                                                   ['',fgreenValue, fyellowValue, fredValue, '']
	                                                 ]);

    var fratingOptoins = {title: 'Cluster Output Status',
	                     width: 250,
	                     height: 250,
	                     legend: {position: 'none'},
	                     bar: { groupWidth: '50%' },
	                     isStacked: true,
	                     colors: ['green','yellow','red']		                                                   
	                     };
    
    var fratingChartDiv = 'foutput-rating';
    var fratingChart = new google.visualization.ColumnChart(document.getElementById(fratingChartDiv));
    fratingChart.draw(fratingData, fratingOptoins);
}