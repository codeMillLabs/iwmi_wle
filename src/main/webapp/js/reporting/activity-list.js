$(document).ready(function() {

	$("#activityTables").tabs({
		show : {
			effect : "slide",
			direction : "up",
			duration : 500
		}
	});

	$("#activityTables2").tabs({
		show : {
			effect : "slide",
			direction : "up",
			duration : 500
		}
	});

	$("#activityTables3").tabs({
		show : {
			effect : "slide",
			direction : "up",
			duration : 500
		}
	});

	$('table.activityList').dataTable({
		"bPaginate" : true, // This option enable the table pagination
		"bLengthChange" : false, // This option disables the select table size option
		"bFilter" : true, // This option enable the search
		"bSort" : true, // this option enable the sort of contents by columns
		"bAutoWidth" : true, // This option enables the auto adjust columns width
		"iDisplayLength" : 25, // Number of rows to show on the table
		
	});
	
	$( "#currentYear_displayName1" ).change(function() {
		  var selectedValue = $( this ).val();
		  $(".fsfr1" ).each(function() {
			  var htmlString = $(this).html();
			  $(this).html(htmlString.replace(/year=[0-9]*/, "year="+selectedValue));
		  });
		});	
	
	$( "#currentYear_displayName2" ).change(function() {
		  var selectedValue = $( this ).val();
		  $(".fsfr2" ).each(function() {
			  var htmlString = $(this).html();
			  $(this).html(htmlString.replace(/year=[0-9]*/, "year="+selectedValue));
		  });
		});	
	
	$( "#report-link" ).click(function() {
		window.location.href=$("input[name=redirectURL]").val();
	});
	$(".inline").colorbox({inline:true, width:"50%"});

});


google.load('visualization', '1.0', {
	'packages' : [ 'corechart' ]
});

google.setOnLoadCallback(drawChart);

function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Topping');
		data.addColumn('number', 'Slices');
		var spentValue = parseInt($('input[name=spent]').val().replace(/\,/g,''));
		var unSpentValue = parseInt($('input[name=unspent]').val().replace(/\,/g,''));

		data.addRows([ [ 'Total Expenditure',  spentValue],
				[ 'Unspent',  unSpentValue] ]);

		var options = {
			'width' : 250,
			'height' : 250,
			is3D : true,
			colors: ['green', 'red'],
			legend: {position: 'none'},
			tooltip: {textStyle: {fontSize: 12}}
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('chart_div'));
		chart.draw(data, options);
		
		var greenValue = parseInt($('input[name=green]').val().replace(/\,/g,''));
		var yellowValue = parseInt($('input[name=yellow]').val().replace(/\,/g,''));
		var redValue = parseInt($('input[name=red').val().replace(/\,/g,''));
		
		var ratingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',greenValue, yellowValue, redValue, '']
		                                                 ]);
	    var ratingOptoins = {title:'Project Status',
	    					 width: 200,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '50%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var ratingChart = new google.visualization.ColumnChart(document.getElementById('cluster-rating'));
	    ratingChart.draw(ratingData, ratingOptoins);
	    
	    var pGreenValue = parseInt($('input[name=pgreen]').val().replace(/\,/g,''));
		var pYellowValue = parseInt($('input[name=pyellow]').val().replace(/\,/g,''));
		var pRedValue = parseInt($('input[name=pred').val().replace(/\,/g,''));
		
		var pRatingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',pGreenValue, pYellowValue, pRedValue, '']
		                                                 ]);
	    var pRatingOptoins = {title:'Output Status',
	    					 width: 200,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '50%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var pRatingChart = new google.visualization.ColumnChart(document.getElementById('project-rating'));
	    pRatingChart.draw(pRatingData, pRatingOptoins);
	    
	    drawFlagshipChart();
}

function drawFlagshipChart()
{
	var count = $('input[name=count]').val() - 1;
	for (i = 0; i < count; i++) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Topping');
		data.addColumn('number', 'Slices');
		var spentName = 'spent' + (i + 1);
		var spentValue = parseInt($('input[name=' + spentName + ']').val().replace(/\,/g,''));
		var unSpentName = 'unspent' + (i + 1);
		var unSpentValue = parseInt($('input[name=' + unSpentName + ']').val().replace(/\,/g,''));

		data.addRows([ [ 'Flagship Total Expenditure',  spentValue],
				[ 'Unspent',  unSpentValue] ]);

		// Set chart options
		var options = {
			'width' : 140,
			'height' : 140,
			is3D : true,
			colors: ['green', 'red'],
			legend: {position: 'none'},
			tooltip: {textStyle: {fontSize: 12}}
		};

		var chartDiv = 'chart_div' + (i + 1);

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document
				.getElementById(chartDiv));
		chart.draw(data, options);
		
		var green = 'green' + (i + 1);
		var greenValue = parseInt($('input[name=' + green + ']').val().replace(/\,/g,''));
		var yellow = 'yellow' + (i + 1);
		var yellowValue = parseInt($('input[name=' + yellow + ']').val().replace(/\,/g,''));
		var red = 'red' + (i + 1);
		var redValue = parseInt($('input[name=' + red + ']').val().replace(/\,/g,''));
		
		var shortNameDiv = 'short-name' + (i + 1);
		var shortName = $('input[name=' + shortNameDiv + ']').val();
		
		 var ratingData = google.visualization.arrayToDataTable([
		                                                   ['','On track', 'Mostly On track', 'Off track', { role: 'annotation' } ],
		                                                   ['',greenValue, yellowValue, redValue, '']
		                                                 ]);

	    var ratingOptoins = {//title: shortName,
		                     width: 140,
		                     height: 250,
		                     legend: {position: 'none'},
		                     bar: { groupWidth: '70%' },
		                     isStacked: true,
		                     colors: ['green','yellow','red']		                                                   
		                     };
	    
	    var ratingChartDiv = 'rating' + (i + 1);
	    var ratingChart = new google.visualization.ColumnChart(document.getElementById(ratingChartDiv));
	    ratingChart.draw(ratingData, ratingOptoins);


	}	
}
