ALTER TABLE `countries` ADD COLUMN `capital_name` VARCHAR(255) NULL DEFAULT 'Test'  AFTER `region_id`;
ALTER TABLE `countries` ADD COLUMN `capital_long` VARCHAR(255) NULL DEFAULT '0.1234'  AFTER `capital_name`;
ALTER TABLE `countries` ADD COLUMN `capital_lat` VARCHAR(255) NULL DEFAULT '0.9876'  AFTER `capital_long`;

ALTER TABLE `activities` ADD COLUMN `summary_review` TEXT NULL  AFTER `gen_budget_bilateral`;
ALTER TABLE `activities` ADD COLUMN `outcomes_review` TEXT NULL  AFTER `summary_review`;
ALTER TABLE `activities` ADD COLUMN `deliverables_review` TEXT NULL  AFTER `outcomes_review`;
ALTER TABLE `activities` ADD COLUMN `partners_review` TEXT NULL  AFTER `deliverables_review`;
ALTER TABLE `activities` ADD COLUMN `geo_review` TEXT NULL  AFTER `partners_review`;
ALTER TABLE `activities` ADD COLUMN `budget_review` TEXT NULL  AFTER `geo_review`;
ALTER TABLE `activities` ADD COLUMN `add_info_review` TEXT NULL  AFTER `budget_review`;

ALTER TABLE `iwmi_activity_reviewes` ADD COLUMN `reviewer_type` VARCHAR(20) NULL  AFTER `reviewer_name`;
ALTER TABLE `iwmi_activity_reviewes` ADD COLUMN `satisfied` VARCHAR(10) NULL DEFAULT 0  AFTER `reviewer_type`;
ALTER TABLE `iwmi_activity_reviewes` ADD COLUMN `round_completed` VARCHAR(10) NULL DEFAULT 0  AFTER `satisfied`;

ALTER TABLE `iwmi_leader_center` ADD COLUMN `center_admins` VARCHAR(1000) NULL  AFTER `next_activity_id`;
ALTER TABLE `iwmi_leader_center` ADD COLUMN `int_reviews_allowed` VARCHAR(10) NULL  DEFAULT 0 AFTER `center_admins`;

ALTER TABLE `users` CHANGE COLUMN `role` `role` VARCHAR(255) NOT NULL;




