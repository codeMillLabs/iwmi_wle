  CREATE TABLE `iwmi_type_of_deliverable` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `iwmi_type_of_outreach` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));
  
 
ALTER TABLE `activities` ADD COLUMN `activity_outreach` TEXT NULL  AFTER `add_info_review`;
ALTER TABLE `activities` ADD COLUMN `activity_material` TEXT NULL  AFTER `activity_outreach`;
ALTER TABLE `activities` ADD COLUMN `activity_publication` TEXT NULL  AFTER `activity_material`;

--
-- Table structure for table `activity_publications`
--

DROP TABLE IF EXISTS `activity_publications`;
CREATE TABLE `activity_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `publication_reference` varchar(2000) NULL,
  `isi_journal` char(1) NULL,
  `peer_reviewed` char(1) NULL,
  `web_link` varchar(500) NULL,
  PRIMARY KEY (`id`),
  KEY `activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `activity_materials`
--

DROP TABLE IF EXISTS `activity_materials`;
CREATE TABLE `activity_materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `mtype` varchar(100) NULL,
  `other_description` varchar(2000) NULL,
  `brief_description` varchar(2000) NULL,
  `target_audience` varchar(2000) NULL,
  `web_link` varchar(500) NULL,
  PRIMARY KEY (`id`),
  KEY `activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `activity_outreachs`
--

DROP TABLE IF EXISTS `activity_outreachs`;
CREATE TABLE `activity_outreachs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `otype` varchar(100) NULL,
  `groups` varchar(500) NULL,
  `paricipants` varchar(5) NULL,
  `female_participants` varchar(5) NULL,
  `date_and_location` varchar(500) NULL,
  `comments` varchar(2000) NULL,
  PRIMARY KEY (`id`),
  KEY `activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `activity_indicators`
--

DROP TABLE IF EXISTS `activity_indicators`;
CREATE TABLE `activity_indicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `itype` int(11) NOT NULL,
  `sub_type` int(11) NOT NULL,
  `target` varchar(2000) NULL,
  `actual` varchar(2000) NULL,
  `contribution` varchar(2000) NULL,
  `comment` varchar(2000) NULL,
  PRIMARY KEY (`id`),
  KEY `activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



  CREATE TABLE `iwmi_indicator_sub` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(2000) NOT NULL ,
  `value` VARCHAR(2000) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));
  
  
   CREATE TABLE `iwmi_indicator_main` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(2000) NOT NULL ,
  `value` VARCHAR(2000) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));