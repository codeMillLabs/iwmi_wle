-- MySQL dump 10.13  Distrib 5.5.30, for Win64 (x86)
--
-- Host: localhost    Database: ccafs_ap
-- ------------------------------------------------------
-- Server version	5.5.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` varchar(10) DEFAULT NULL,
  `title` text NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `year` int(11) NOT NULL,
  `description` text,
  `milestone_id` int(11) DEFAULT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `is_global` tinyint(1) DEFAULT NULL,
  `has_partners` tinyint(1) DEFAULT NULL,
  `continuous_activity_id` int(11) DEFAULT NULL,
  `activity_status_id` int(11) DEFAULT NULL,
  `status_description` text,
  `is_commissioned` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `milestone_fk` (`milestone_id`),
  KEY `activity_leader_fk` (`activity_leader_id`),
  KEY `continous_activity_fk` (`continuous_activity_id`),
  KEY `status_fk` (`activity_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1;



DROP TRIGGER IF EXISTS `activity_id_insert_trigger`;
DELIMITER //
CREATE TRIGGER `activity_id_insert_trigger` BEFORE INSERT ON `activities`
 FOR EACH ROW IF( NEW.continuous_activity_id IS NULL ) THEN
    SET NEW.activity_id = CONCAT( (SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'ccafs_ap' AND TABLE_NAME = 'activities'), "-", NEW.`year`);
    ELSE
    SET NEW.activity_id = CONCAT( NEW.continuous_activity_id, "-", NEW.`year`);
    END IF
//
DELIMITER ;
DROP TRIGGER IF EXISTS `activity_id_update_trigger`;
DELIMITER //
CREATE TRIGGER `activity_id_update_trigger` BEFORE UPDATE ON `activities`
 FOR EACH ROW IF( NEW.continuous_activity_id IS NULL ) THEN
    SET NEW.activity_id = CONCAT( NEW.id, "-", NEW.`year`);
    ELSE
    SET NEW.activity_id = CONCAT( NEW.continuous_activity_id, "-", NEW.`year`);
    END IF
//
DELIMITER ;

--
-- Table structure for table `activity_budgets`
--

DROP TABLE IF EXISTS `activity_budgets`;
CREATE TABLE `activity_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usd` double NOT NULL,
  `cg_funds` int(11) DEFAULT NULL,
  `bilateral` int(11) DEFAULT NULL,
  `activity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cg_funds_fk` (`cg_funds`),
  KEY `bilateral_fk` (`bilateral`),
  KEY `budgets_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_keywords`
--

DROP TABLE IF EXISTS `activity_keywords`;
CREATE TABLE `activity_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `keyword_id` int(11) DEFAULT NULL,
  `other` text,
  PRIMARY KEY (`id`),
  KEY `ak_activity_fk` (`activity_id`),
  KEY `ak_keyword_fk` (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_leaders`
--

DROP TABLE IF EXISTS `activity_leaders`;
CREATE TABLE `activity_leaders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acronym` varchar(20) NOT NULL DEFAULT 'ACRONYM',
  `name` text NOT NULL,
  `led_activity_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `theme_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `led_activity_fk` (`led_activity_id`),
  KEY `region_id_fk` (`region_id`),
  KEY `theme_id_fk` (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_objectives`
--

DROP TABLE IF EXISTS `activity_objectives`;
CREATE TABLE `activity_objectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `outcome_type` char(1) NULL,
  `users` varchar(2000) NULL,
  `roles` varchar(2000) NULL,
  `changes` varchar(2000) NULL,
  `gender_desc` varchar(2000) NULL,
  `intermediaries` varchar(2000) NULL,
  `capacity` varchar(2000) NULL,
  `progress` varchar(2000) NULL,
  PRIMARY KEY (`id`),
  KEY `activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_partner_roles`
--

DROP TABLE IF EXISTS `activity_partner_roles`;
CREATE TABLE `activity_partner_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_partner_id` int(11) NOT NULL,
  `partner_role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_partner_fk` (`activity_partner_id`),
  KEY `partner_role_fk` (`partner_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_partners`
--

DROP TABLE IF EXISTS `activity_partners`;
CREATE TABLE `activity_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `contact_name` text,
  `contact_email` text,
  PRIMARY KEY (`id`),
  KEY `ap_partner_fk` (`partner_id`),
  KEY `ap_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_status`
--

DROP TABLE IF EXISTS `activity_status`;
CREATE TABLE `activity_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `activity_validations`
--

DROP TABLE IF EXISTS `activity_validations`;
CREATE TABLE `activity_validations` (
  `activity_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `activity_id_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `benchmark_sites`
--

DROP TABLE IF EXISTS `benchmark_sites`;
CREATE TABLE `benchmark_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bs_id` text NOT NULL,
  `name` text NOT NULL,
  `country_iso2` varchar(2) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_fk` (`country_iso2`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Table structure for table `bs_locations`
--

DROP TABLE IF EXISTS `bs_locations`;
CREATE TABLE `bs_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bs_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `details` text,
  PRIMARY KEY (`id`),
  KEY `bs_fk` (`bs_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `budget_percentages`
--

DROP TABLE IF EXISTS `budget_percentages`;
CREATE TABLE `budget_percentages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percentage` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Table structure for table `case_studies`
--

DROP TABLE IF EXISTS `case_studies`;
CREATE TABLE `case_studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `author` text NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `photo` text,
  `objectives` text,
  `description` text,
  `results` text,
  `partners` text,
  `links` text,
  `keywords` text,
  `is_global` tinyint(1) NOT NULL,
  `logframe_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cs_logframe_fk` (`logframe_id`),
  KEY `cs_activity_leader` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `case_study_countries`
--

DROP TABLE IF EXISTS `case_study_countries`;
CREATE TABLE `case_study_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_study_id` int(11) NOT NULL,
  `country_iso2` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `csc_case_study_fk` (`case_study_id`),
  KEY `csc_country_fk` (`country_iso2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `case_study_types`
--

DROP TABLE IF EXISTS `case_study_types`;
CREATE TABLE `case_study_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Table structure for table `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_campaigns` text,
  `blogs` text,
  `websites` text,
  `social_media_campaigns` text,
  `newsletters` text,
  `events` text,
  `videos_multimedia` text,
  `other_communications` text,
  `activity_leader_id` int(11) NOT NULL,
  `logframe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_communications_logframe_id` (`logframe_id`),
  KEY `FK_communications_activity_leader_id` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `contact_person`
--

DROP TABLE IF EXISTS `contact_person`;
CREATE TABLE `contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `email` text,
  `activity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `iso2` varchar(2) NOT NULL,
  `name` text NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`iso2`),
  KEY `region_fk` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `country_locations`
--

DROP TABLE IF EXISTS `country_locations`;
CREATE TABLE `country_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_iso2` varchar(2) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `details` text,
  PRIMARY KEY (`id`),
  KEY `cl_country_fk` (`country_iso2`),
  KEY `cl_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `cs_types`
--

DROP TABLE IF EXISTS `cs_types`;
CREATE TABLE `cs_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_study_id` int(11) NOT NULL,
  `case_study_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_study_id` (`case_study_id`),
  KEY `case_study_type_id` (`case_study_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `deliverable_formats`
--

DROP TABLE IF EXISTS `deliverable_formats`;
CREATE TABLE `deliverable_formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deliverable_id` int(11) NOT NULL,
  `file_format_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_format_id` (`file_format_id`),
  KEY `deliverable_id` (`deliverable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `deliverable_status`
--

DROP TABLE IF EXISTS `deliverable_status`;
CREATE TABLE `deliverable_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `deliverable_types`
--

DROP TABLE IF EXISTS `deliverable_types`;
CREATE TABLE `deliverable_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Table structure for table `deliverables`
--

DROP TABLE IF EXISTS `deliverables`;
CREATE TABLE `deliverables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `year` int(4) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `deliverable_type_id` int(11) NOT NULL,
  `is_expected` tinyint(1) NOT NULL,
  `deliverable_status_id` int(11) NOT NULL,
  `filename` text,
  `description_update` text,
  PRIMARY KEY (`id`),
  KEY `activity_fk2` (`activity_id`),
  KEY `deliverable_type_fk2` (`deliverable_type_id`),
  KEY `deliverable_status_fk2` (`deliverable_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `file_formats`
--

DROP TABLE IF EXISTS `file_formats`;
CREATE TABLE `file_formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Table structure for table `gender_integrations`
--

DROP TABLE IF EXISTS `gender_integrations`;
CREATE TABLE `gender_integrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `activity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gi_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `indicator_reports`
--

DROP TABLE IF EXISTS `indicator_reports`;
CREATE TABLE `indicator_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target` text,
  `next_target` text,
  `actual` text,
  `support_links` text,
  `deviation` text,
  `activity_leader_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_activity_leader_id_fk` (`activity_leader_id`),
  KEY `FK_indicator_id_fk` (`indicator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `indicator_types`
--

DROP TABLE IF EXISTS `indicator_types`;
CREATE TABLE `indicator_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Table structure for table `indicators`
--

DROP TABLE IF EXISTS `indicators`;
CREATE TABLE `indicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varchar(5) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `indicator_type_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `indicator_type_id_fk` (`indicator_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
CREATE TABLE `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Table structure for table `leader_types`
--

DROP TABLE IF EXISTS `leader_types`;
CREATE TABLE `leader_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `leverages`
--

DROP TABLE IF EXISTS `leverages`;
CREATE TABLE `leverages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `budget` double DEFAULT NULL,
  `start_year` int(4) DEFAULT NULL,
  `end_year` int(4) DEFAULT NULL,
  `theme_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_theme_id` (`theme_id`),
  KEY `FK_leader_id` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `logframes`
--

DROP TABLE IF EXISTS `logframes`;
CREATE TABLE `logframes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Table structure for table `milestone_reports`
--

DROP TABLE IF EXISTS `milestone_reports`;
CREATE TABLE `milestone_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `milestone_id` int(11) NOT NULL,
  `milestone_status_id` int(11) DEFAULT NULL,
  `tl_description` text,
  `rpl_description` text,
  PRIMARY KEY (`id`),
  KEY `mr_milestone_fk` (`milestone_id`),
  KEY `mr_milestone_status_fk` (`milestone_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `milestone_status`
--

DROP TABLE IF EXISTS `milestone_status`;
CREATE TABLE `milestone_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `milestones`
--

DROP TABLE IF EXISTS `milestones`;
CREATE TABLE `milestones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `output_id` int(11) NOT NULL,
  `code` text NOT NULL,
  `year` int(4) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `output_fk` (`output_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8;

--
-- Table structure for table `objectives`
--

DROP TABLE IF EXISTS `objectives`;
CREATE TABLE `objectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL,
  `code` text NOT NULL,
  `description` text NOT NULL,
  `outcome_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `theme_fk` (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Table structure for table `open_access`
--

DROP TABLE IF EXISTS `open_access`;
CREATE TABLE `open_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `other_sites`
--

DROP TABLE IF EXISTS `other_sites`;
CREATE TABLE `other_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `country_iso2` varchar(2) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `os_country_fk` (`country_iso2`),
  KEY `os_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `outcome_indicator_reports`
--

DROP TABLE IF EXISTS `outcome_indicator_reports`;
CREATE TABLE `outcome_indicator_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outcome_indicator_id` int(11) NOT NULL,
  `achievements` text,
  `evidence` text,
  `activity_leader_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_outcome_indicator_id` (`outcome_indicator_id`),
  KEY `FK_activity_leader_id` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `outcome_indicators`
--

DROP TABLE IF EXISTS `outcome_indicators`;
CREATE TABLE `outcome_indicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `description` text NOT NULL,
  `theme_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_theme_id` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `outcomes`
--

DROP TABLE IF EXISTS `outcomes`;
CREATE TABLE `outcomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `outcome` text NOT NULL,
  `outputs` text NOT NULL,
  `partners` text NOT NULL,
  `output_user` text NOT NULL,
  `how_used` text NOT NULL,
  `evidence` text NOT NULL,
  `logframe_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `out_logframe_fk` (`logframe_id`),
  KEY `out_activity_leader_fk` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `output_summaries`
--

DROP TABLE IF EXISTS `output_summaries`;
CREATE TABLE `output_summaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `output_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `os_output_fk` (`output_id`),
  KEY `os_activity_leader_fk` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `outputs`
--

DROP TABLE IF EXISTS `outputs`;
CREATE TABLE `outputs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `code` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objective_fk` (`objective_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Table structure for table `partner_roles`
--

DROP TABLE IF EXISTS `partner_roles`;
CREATE TABLE `partner_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Table structure for table `partner_types`
--

DROP TABLE IF EXISTS `partner_types`;
CREATE TABLE `partner_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acronym` varchar(20) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `acronym` text,
  `country_iso2` varchar(2) DEFAULT NULL,
  `city` text,
  `partner_type_id` int(11) NOT NULL,
  `website` text,
  PRIMARY KEY (`id`),
  KEY `type_fk` (`partner_type_id`),
  KEY `country_iso2` (`country_iso2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `publication_themes`
--

DROP TABLE IF EXISTS `publication_themes`;
CREATE TABLE `publication_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Table structure for table `publication_themes_reporting`
--

DROP TABLE IF EXISTS `publication_themes_reporting`;
CREATE TABLE `publication_themes_reporting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) NOT NULL,
  `publication_theme_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_publication_publication_themes_reporting_id` (`publication_id`),
  KEY `FK_publication_theme_publication_themes_reporting_id` (`publication_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `publication_types`
--

DROP TABLE IF EXISTS `publication_types`;
CREATE TABLE `publication_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
CREATE TABLE `publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_type_id` int(11) NOT NULL,
  `identifier` text,
  `citation` text NOT NULL,
  `file_url` text,
  `logframe_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `open_access_id` int(11) DEFAULT NULL,
  `ccafs_acknowledge` tinyint(1) NOT NULL,
  `isi_publication` tinyint(1) DEFAULT NULL,
  `nars_coauthor` tinyint(1) DEFAULT NULL,
  `earth_system_coauthor` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_publication_type_fk` (`publication_type_id`),
  KEY `p_logframe_fk` (`logframe_id`),
  KEY `p_activity_leader_fk` (`activity_leader_id`),
  KEY `open_access_id` (`open_access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `region_locations`
--

DROP TABLE IF EXISTS `region_locations`;
CREATE TABLE `region_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `details` text,
  PRIMARY KEY (`id`),
  KEY `region_id` (`region_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `activity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `res_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `rpl_synthesis_reports`
--

DROP TABLE IF EXISTS `rpl_synthesis_reports`;
CREATE TABLE `rpl_synthesis_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccafs_sites` text NOT NULL,
  `cross_center` text NOT NULL,
  `regional` text NOT NULL,
  `decision_support` text NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `logframe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rplsr_activity_leader_fk` (`activity_leader_id`),
  KEY `rplsr_logframe_fk` (`logframe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `submissions`
--

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE `submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_leader_id` int(11) NOT NULL,
  `logframe_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `section` enum('Planning','Reporting') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_leader_fk_idx` (`activity_leader_id`),
  KEY `logframe_id_fk_idx` (`logframe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
CREATE TABLE `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `description` text,
  `logframe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logframe_fk` (`logframe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Table structure for table `tl_output_summaries`
--

DROP TABLE IF EXISTS `tl_output_summaries`;
CREATE TABLE `tl_output_summaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `output_id` int(11) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tlos_output_fk` (`output_id`),
  KEY `tlos_activity_leader_fk` (`activity_leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activity_leader_id` int(11) NOT NULL,
  `role` enum('Admin','CP','TL','RPL','PI') NOT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_activity_leader_fk` (`activity_leader_id`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

CREATE  TABLE `iwmi_flagship_srp` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(300) NOT NULL ,
  `value` VARCHAR(300) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));

CREATE TABLE `iwmi_activity_cluster` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(300) NOT NULL ,
  `value` VARCHAR(300) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));
  
 CREATE TABLE `iwmi_clasification` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));
  
 CREATE TABLE `iwmi_leader_center` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));  
  
 CREATE TABLE `iwmi_wle_developments` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));  
  
  CREATE TABLE `iwmi_outcome_user_roles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_name` VARCHAR(150) NOT NULL ,
  `value` VARCHAR(150) NOT NULL ,
  `parent_id` INT NULL ,
  PRIMARY KEY (`id`));  
  
 CREATE TABLE `iwmi_token_activity` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `token_id` VARCHAR(150) NOT NULL ,
  `action` VARCHAR(500) NOT NULL ,
  `is_used` VARCHAR(2) NOT NULL ,
  PRIMARY KEY (`id`));
  
 DROP TABLE IF EXISTS `activity_budgets`;  
 CREATE TABLE `activity_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `budget_data_json` VARCHAR(4000) NOT NULL ,
  PRIMARY KEY (`id`),
  KEY `budgets_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 
DROP TABLE IF EXISTS `iwmi_activity_wle`; 
CREATE TABLE `iwmi_activity_wle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `wle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ak_activity_fk` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `iwmi_activity_outcome`; 
CREATE TABLE `iwmi_activity_outcome` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `activity_id` INT NOT NULL ,
  `users` VARCHAR(2000) DEFAULT NULL ,
  `users_role` VARCHAR(2000) DEFAULT NULL ,
  `changes` VARCHAR(2000) DEFAULT NULL ,
  `gen_desc` VARCHAR(2000) DEFAULT NULL ,
  `intermediaries` VARCHAR(2000) DEFAULT NULL ,
  `capacity_building` VARCHAR(2000) DEFAULT NULL ,
  `progress` VARCHAR(2000) DEFAULT NULL ,
  PRIMARY KEY (`id`)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
DROP TABLE IF EXISTS `iwmi_reviewers`;   
CREATE  TABLE `iwmi_reviewers` (
  `id` INT(19) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `flagship` VARCHAR(255) NULL ,
  `cluster` VARCHAR(255) NULL ,
  `details` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
DROP TABLE IF EXISTS `iwmi_activity_reviewer`;  
CREATE  TABLE `iwmi_activity_reviewer` (
  `id` INT(19) NOT NULL AUTO_INCREMENT ,
  `activity_id` INT NULL ,
  `reviewer_id` INT(19) NULL ,
  `reviewer_name` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;  
  

ALTER TABLE `activities` ADD COLUMN `project_name` VARCHAR(255) NULL  AFTER `date_added`;
ALTER TABLE `activities` ADD COLUMN `gender_research` VARCHAR(4000) NULL  AFTER `project_name`;
ALTER TABLE `activities` ADD COLUMN `echo_sys_desc` VARCHAR(4000) NULL  AFTER `gender_research`;
ALTER TABLE `activities` ADD COLUMN `flagship` VARCHAR(300) NULL  AFTER `echo_sys_desc`;
ALTER TABLE `activities` ADD COLUMN `activity_cluster` VARCHAR(300) NULL  AFTER `flagship`;
ALTER TABLE `activities` ADD COLUMN `lead_center` VARCHAR(300) NULL  AFTER `activity_cluster`;
ALTER TABLE `activities` ADD COLUMN `created_by` VARCHAR(60) NULL  AFTER `lead_center`;
ALTER TABLE `activities` ADD COLUMN `is_deleted` tinyint(1) NOT NULL DEFAULT '0'  AFTER `created_by`;



ALTER TABLE `deliverables` ADD COLUMN `indicator_type` CHAR(1) NULL  AFTER `filename`;
ALTER TABLE `activity_partners` ADD COLUMN `clasification` VARCHAR(255) NULL  AFTER `contact_email`;
ALTER TABLE `activity_partners` ADD COLUMN `budget` DECIMAL(20,3) NULL DEFAULT 0.0   AFTER `clasification`;
ALTER TABLE `regions` CHANGE COLUMN `name` `name` VARCHAR(300) NOT NULL  , CHANGE COLUMN `description` `description` VARCHAR(4000) NULL  ;
ALTER TABLE `keywords` ADD COLUMN `status` VARCHAR(45) NULL DEFAULT 'PENDING'  AFTER `name` ;
ALTER TABLE `partners` ADD COLUMN `status` VARCHAR(45) NULL DEFAULT 'PENDING'  AFTER `website` ;

ALTER TABLE `iwmi_token_activity` ADD INDEX `iwmi_token_id_indx` (`token_id` ASC) ;


-- New Activity ID changes Trigger
DROP TRIGGER IF EXISTS `activity_id_insert_trigger`;
DELIMITER //
CREATE TRIGGER `activity_id_insert_trigger` BEFORE INSERT ON `activities`
 FOR EACH ROW 
   BEGIN
     DECLARE next_id INT;
      IF( NEW.activity_id IS NULL ) THEN
		SELECT next_activity_id into next_id FROM iwmi_leader_center WHERE value = NEW.`lead_center` limit 1;
	    UPDATE iwmi_leader_center SET next_activity_id = (next_id +1) WHERE  value = NEW.`lead_center`;
        SET NEW.activity_id = CONCAT( NEW.`year`, "-", (SELECT id FROM iwmi_leader_center WHERE value = NEW.`lead_center`), "-", next_id); 
        SET NEW.lead_center_based_id = next_id;
      ELSE
       SET NEW.activity_id = CONCAT( NEW.`year`, "-", (SELECT id FROM iwmi_leader_center WHERE value = NEW.`lead_center`), "-", NEW.lead_center_based_id);
      END IF;
   END	
//
DELIMITER;


DROP TRIGGER IF EXISTS `activity_id_update_trigger`;
DELIMITER //
CREATE TRIGGER `activity_id_update_trigger` BEFORE UPDATE ON `activities`
 FOR EACH ROW IF( NEW.activity_id IS NULL ) THEN
    SET NEW.activity_id = CONCAT( NEW.`year`, "-", (SELECT id FROM iwmi_leader_center WHERE value = NEW.`lead_center`), "-", NEW.lead_center_based_id);
    ELSE
    SET NEW.activity_id = CONCAT( NEW.`year`, "-", (SELECT id FROM iwmi_leader_center WHERE value = NEW.`lead_center`), "-", NEW.lead_center_based_id);
    END IF
//
DELIMITER;


ALTER TABLE `activities` CHANGE COLUMN `activity_id` `activity_id` VARCHAR(80) NULL DEFAULT NULL  ;
ALTER TABLE `activities` CHANGE COLUMN `activity_id` `activity_id` VARCHAR(80) NULL  , ADD COLUMN `lead_center_based_id` INT(11) NULL DEFAULT NULL  AFTER `id` ;
ALTER TABLE `iwmi_leader_center` ADD COLUMN `next_activity_id` INT(11) NULL DEFAULT 1  AFTER `parent_id`;

ALTER TABLE `activities` ADD COLUMN `revision_id` VARCHAR(45) NULL DEFAULT 0  AFTER `activity_id` , ADD COLUMN `status` VARCHAR(65) NULL DEFAULT 'New'  AFTER `revision_id` ;

DROP TABLE IF EXISTS `iwmi_activity_reviewes`;
CREATE TABLE `iwmi_activity_reviewes` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) DEFAULT NULL,
  `activity_revision` varchar(60) DEFAULT NULL,
  `reviewer_id` int(19) DEFAULT NULL,
  `reviewer_name` varchar(255) DEFAULT NULL,
  `review_comment` varchar(2000) DEFAULT NULL,
  `reviewed_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `activity_validations` ADD COLUMN `revision_id` VARCHAR(45) NULL DEFAULT 0  AFTER `activity_id` ;



