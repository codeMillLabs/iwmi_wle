

DROP TABLE IF EXISTS `activity_review_ratings`;
CREATE TABLE `activity_review_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` varchar(40) NOT NULL,
  `mid_year_rate` varchar(10) NULL,
  `year_end_rate` varchar(10) NULL,
  `mid_yr_rating_json` text NULL,
  `mid_yr_comments` text NULL,
  `yr_end_rating_json` text NULL,
  `yr_end_comments` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `flagship_focalregion`;
CREATE TABLE `flagship_focalregion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_fr_no` varchar(5) NOT NULL,
  `name` varchar(100) NULL,
  `leadera` varchar(150) NULL,
  `leadera_id` int(11) NULL,
  `leaderb` varchar(150) NULL,
  `leaderb_id` int(11) NULL,
  `long_name` varchar(500) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rpt_flagship_cluster`;
CREATE TABLE `rpt_flagship_cluster` (
  `flagship_id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(11) NOT NULL,
   PRIMARY KEY (`flagship_id`,`cluster_id`),
  KEY `flagship_id_fk` (`flagship_id`),
  KEY `cluster_id_fk` (`cluster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `fl_fr_rpt`;
CREATE TABLE `fl_fr_rpt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `flagship_id` int(11) NOT NULL,
  `period` varchar(3) NOT NULL,
  `description` varchar(1000) NULL,
  `state` varchar(20) NULL,
  `revision` int(11) NOT NULL,
  `mngr_comment` varchar(1000) NULL,
  `dir_comment` varchar(1000) NULL,
  `mngr_done` tinyint(4) NOT NULL,
  `dir_done` tinyint(4) NOT NULL,
   PRIMARY KEY (`id`),
  KEY `flagship_id_fk` (`flagship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flfr_cluster`;
CREATE TABLE `flfr_cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rpt_id` int(11) NOT NULL,
  `spent` double NULL,
  `status` varchar(1000) NULL,
  `rating` char(2) NULL,
  `name` varchar(200) NOT NULL,
  `act_count` int(11) NOT NULL,
  `budget` double NOT NULL,
  `mngr_comment` varchar(1000) NULL,
  `dir_comment` varchar(1000) NULL,
  `exp` double NULL,
  `clus_id` int(11) NOT NULL,
   PRIMARY KEY (`id`),
  KEY `rpt_id_fk` (`rpt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flfr_output_detail`;
CREATE TABLE `flfr_output_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(11) NOT NULL,
  `code` varchar(1000) NULL,
  `description` varchar(1000) NULL,
  `comment` varchar(1000) NULL,
  `mngr_comment` varchar(1000) NULL,
  `dir_comment` varchar(1000) NULL,
   PRIMARY KEY (`id`),
  KEY `cluster_id_fk` (`cluster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flfr_output_activity`;
CREATE TABLE `flfr_output_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail_id` int(11) NOT NULL,
  `activity_id` varchar(1000) NULL,
  `activity_name` varchar(1000) NULL,
   PRIMARY KEY (`id`),
  KEY `detail_id_fk` (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flfr_output`;
CREATE TABLE `flfr_output` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oact_id` int(11) NOT NULL,
  `activity_id` varchar(100) NULL,
  `activity_name` varchar(400) NULL,
  `project_id` varchar(100) NULL,
  `project_name` varchar(400) NULL,
  `progress` varchar(1000) NULL,
  `status` varchar(1000) NULL,
  `rating` char(2) NULL,
   PRIMARY KEY (`id`),
  KEY `oact_id_fk` (`oact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flfr_comment`;
CREATE TABLE `flfr_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rpt_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rpt_id_fk` (`rpt_id`),
  KEY `user_id_fk` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `iwmi_flagship_srp` ADD COLUMN `leader_email` VARCHAR(100) NULL AFTER `parent_id`;


DROP TABLE IF EXISTS `flfr_cluster_data`;
CREATE TABLE `flfr_cluster_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_id` int(11) NOT NULL,
  `cluster` varchar(20) NOT NULL,
  `output_code` varchar(10) NOT NULL,
  `output` varchar(1000) NOT NULL,
  `activity_id` varchar(20) NOT NULL,
  `project` varchar(1000) NOT NULL,
  `del_code` varchar(20) NOT NULL,
  `del` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `deliverables` ADD COLUMN `del_code` VARCHAR(100) NULL AFTER `description_update`;

ALTER TABLE `iwmi_flagship_srp` ADD COLUMN `leader_b_email` VARCHAR(100) NULL AFTER `leader_email`;

DELETE FROM flfr_comment;
DELETE FROM flfr_output;
DELETE FROM flfr_output_activity;
DELETE FROM flfr_output_detail;
DELETE FROM flfr_cluster;
DELETE FROM fl_fr_rpt;


DROP TABLE IF EXISTS `fl_fr_rpt_status`;
CREATE TABLE `fl_fr_rpt_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `flagship_id` int(11) NOT NULL,
  `period` varchar(3) NOT NULL,
   PRIMARY KEY (`id`),
  KEY `flagship_id_fk` (`flagship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE fl_fr_rpt MODIFY description VARCHAR(3000);
ALTER TABLE fl_fr_rpt MODIFY mngr_comment VARCHAR(3000);
ALTER TABLE fl_fr_rpt MODIFY dir_comment VARCHAR(3000);


ALTER TABLE flfr_cluster MODIFY mngr_comment VARCHAR(3000);
ALTER TABLE flfr_cluster MODIFY dir_comment VARCHAR(3000);

ALTER TABLE flfr_output_detail MODIFY description VARCHAR(3000);
ALTER TABLE flfr_output_detail MODIFY comment VARCHAR(3000);
ALTER TABLE flfr_output_detail MODIFY mngr_comment VARCHAR(3000);
ALTER TABLE flfr_output_detail MODIFY dir_comment VARCHAR(3000);

ALTER TABLE flfr_output MODIFY project_name VARCHAR(3000);
ALTER TABLE flfr_output MODIFY progress VARCHAR(3000);
ALTER TABLE flfr_output MODIFY status VARCHAR(3000);

ALTER TABLE fl_fr_rpt MODIFY description VARCHAR(10000);


DROP TABLE IF EXISTS `score_card_cache_main`;
CREATE TABLE `score_card_cache_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_green` int(11) NOT NULL,
  `cluster_yellow` int(11) NOT NULL,
  `cluster_red` int(11) NOT NULL,
  `project_green` int(11) NOT NULL,
  `project_yellow` int(11) NOT NULL,
  `project_red` int(11) NOT NULL,
  `output_green` int(11) NOT NULL,
  `output_yellow` int(11) NOT NULL,
  `output_red` int(11) NOT NULL,
  `spent` double NOT NULL,
  `not_spent` double NOT NULL,
  `cluster_count` int(11) NOT NULL,
  `project_count` int(11) NOT NULL,
  `output_count` int(11) NOT NULL,
  `cluster_output_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `score_card_cache_main_flagship`;
CREATE TABLE `score_card_cache_main_flagship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `green` int(11) NOT NULL,
  `yellow` int(11) NOT NULL,
  `red` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `long_name` varchar(500) NOT NULL,
  `tooltip` varchar(10000) NOT NULL,
  `spent` double NOT NULL,
  `not_spent` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `flagship_focalregion` ADD COLUMN `org_name` VARCHAR(50) NULL AFTER `long_name`;

DROP TABLE IF EXISTS `score_card_cache_second_page`;
CREATE TABLE `score_card_cache_second_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flagship_id` int(11) NOT NULL,
  `project_green` int(11) NOT NULL,
  `project_yellow` int(11) NOT NULL,
  `project_red` int(11) NOT NULL,
  `output_green` int(11) NOT NULL,
  `output_yellow` int(11) NOT NULL,
  `output_red` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `spent` double NOT NULL,
  `not_spent` double NOT NULL,
  `comment` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `score_card_cache_third_page_output`;
CREATE TABLE `score_card_cache_third_page_output` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` varchar(20) NOT NULL,
  `output_green` int(11) NOT NULL,
  `output_yellow` int(11) NOT NULL,
  `output_red` int(11) NOT NULL,
  `name` varchar(10000) NOT NULL,
  `comment` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `score_card_cache_third_page_project`;
CREATE TABLE `score_card_cache_third_page_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flagship_id` int(11) NOT NULL,
  `project_id` varchar(20) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `fl_short_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;